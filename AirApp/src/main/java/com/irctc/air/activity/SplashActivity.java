package com.irctc.air.activity;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateUtils;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.irctc.air.Database.AirDatabase;
import com.irctc.air.R;
import com.irctc.air.model.app_version_response.PojoAppVersion;
import com.irctc.air.model.app_version_response.VersionData;
import com.irctc.air.model.flight_icon.PojoFlightIcon;
import com.irctc.air.model.search_airports.PojoAirportList;
import com.irctc.air.networking.Networking;
import com.irctc.air.services.ServiceCreateAirportList;
import com.irctc.air.util.AppKeys;
import com.irctc.air.util.AssetsReader;
import com.irctc.air.util.NetworkingUtils;
import com.irctc.air.util.Pref;
import com.irctc.air.util.ProjectUtil;

import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Rajnikant on 01/04/2018 for version check web service call
 */
public class SplashActivity extends AppCompatActivity {

    ProgressBar lProgressBar;
    String currentVersion, latestVersion;
    private AirDatabase airDatabase;
    private Context context;
    private Gson gson;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        context = getApplicationContext();
        gson = new Gson();
        airDatabase = new AirDatabase(context);
        if(isConnectingToInternet(this)){
            lProgressBar = (ProgressBar) findViewById(R.id.progress);
            try {
                lProgressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#FF797C80"), PorterDuff.Mode.SRC_IN);
            } catch (Exception e) {

            }
            try {
                currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            getAppVersion();
//                new GetVersionCode().execute();
        }else {
            noInternetConnection("NO INTERNET",this.getResources().getString(R.string.INTERNET_DOWN));
//            openAlertDialog("NO INTERNET", "Please Check Your Internet Connection.");
        }
    }




    private void openAlertDialog(String title, String msg){
        AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getAppVersion();
//                        new GetVersionCode().execute();
            }
        });
        builder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.show();
    }

    private void getAppVersion() {

        if(airDatabase.isAirportDataExist()<3950){
            Intent i= new Intent(SplashActivity.this, ServiceCreateAirportList.class);
            context.startService(i);
        }

        Networking.checkAppVersion(new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //NetworkingUtils.dismissProgress();
                Gson gson = new Gson();
                PojoAppVersion pojoAppVersion = gson.fromJson(response.toString(), PojoAppVersion.class);
                if (pojoAppVersion.getStatus().equals("SUCCESS")) {
                    if (pojoAppVersion.getData() !=null) {
                        List<VersionData> data = pojoAppVersion.getData();
                        for (VersionData d: data) {
                            if (d.getVersion() !=null) {
                                latestVersion = d.getVersion();
                            }
                        }
                        if (Float.valueOf(currentVersion) < Float.valueOf(latestVersion)) {
                            //show dialog
                            showForceUpdateDialog();
                        } else {
                            startActivity(new Intent(SplashActivity.this, ActivityMain.class));
                            finish();
                        }

                    }

                } else {
                    startActivity(new Intent(SplashActivity.this, ActivityMain.class));
                    finish();
//                    NetworkingUtils.internalServerIssue(context);
//                    openAlertDialog("Server Error", "Internal Server Error");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkingUtils.dismissProgress();
//                noInternetConnection("Server Error","Internal Server Error");
//                openAlertDialog("Server Error", "Internal Server Error");
                startActivity(new Intent(SplashActivity.this, ActivityMain.class));
                finish();
            }
        });
    }

    public static boolean isConnectingToInternet(Context ctx) {

        boolean NetConnected = false;
        try {
            ConnectivityManager connectivity = (ConnectivityManager) ctx
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity == null) {
//                Logger.info("tag", "couldn't get connectivity manager");
                NetConnected = false;
            } else {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null) {
                    for (int i = 0; i < info.length; i++) {
                        if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                            NetConnected = true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
//            Logger.error("Connectivity Exception",
//                    "Exception AT isInternetConnection");
            NetConnected = false;
        }
        return NetConnected;

    }


    /*private class GetVersionCode extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... voids) {
            if(airDatabase.isAirportDataExist()<3950){
                Intent i= new Intent(SplashActivity.this, ServiceCreateAirportList.class);
                context.startService(i);
            }
            String newVersion = null;
            try {
                String url = "https://play.google.com/store/apps/details?id=" + SplashActivity.this.getPackageName();
                Log.e("URL", "URL: " + url);
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + SplashActivity.this.getPackageName() + "&hl=it")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();
                return newVersion;
            } catch (Exception e) {
                return newVersion;
            }
        }

        @Override
        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                latestVersion = onlineVersion;
                if (Float.valueOf(currentVersion) < Float.valueOf(onlineVersion)) {
                    //show dialog
                    showForceUpdateDialog();
                } else {
                    startActivity(new Intent(SplashActivity.this, ActivityMain.class));
                    finish();
                }
            } else {
                *//*AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
                builder.setTitle("NO INTERNET");
                builder.setMessage("Please Check Your Internet Connection.");
                builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        new GetVersionCode().execute();
                    }
                });
                builder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                builder.show();*//*
                //startActivity(new Intent(SplashActivity.this, ActivityMain.class));
                //finish();
            }
        }

    }*/

    public void showForceUpdateDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SplashActivity.this);

        alertDialogBuilder.setTitle("Update Available");
        alertDialogBuilder.setMessage(getString(R.string.youAreNotUpdatedMessage) + " " + latestVersion + getString(R.string.youAreNotUpdatedMessage1));
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                dialog.cancel();
                finish();
            }
        });
        alertDialogBuilder.show();
    }

    public void noInternetConnection(String title, String msg) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SplashActivity.this);

        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(msg);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                finish();
            }
        });
        alertDialogBuilder.show();
    }
}
