package com.irctc.air.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.irctc.air.Database.AirDatabase;
import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.R;
import com.irctc.air.fragment.FragmentLogin;
import com.irctc.air.fragment.FragmentTicket;
import com.irctc.air.fragment.FragmentPlanner;
import com.irctc.air.header.AirHeader;
import com.irctc.air.model.AirDataModel;
import com.irctc.air.model.flight_icon.PojoFlightIcon;
import com.irctc.air.navigationdrawer.SliderMenu;
import com.irctc.air.session.Waiter;
import com.irctc.air.util.AppKeys;
import com.irctc.air.util.Constant;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.NetworkingUtils;
import com.irctc.air.util.Pref;
import com.irctc.air.util.ProjectUtil;
import com.irctc.air.networking.Networking;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by vivek on 04/13/2016
 */
public class ActivityMain extends BaseActivity {
    public static boolean backStackEnable = false;
    public static boolean paymentPage = false;
    public static boolean ticketPage = false;
    public static boolean ticketDetailPage = false;
    public static boolean boolBackClose = false;
    public static boolean flagIsSliderOpen = false;
    public static Context context;
    public static boolean backstack = false;
    public static boolean backStack2 = false;
    public static boolean soldOutDialog = false;
    // To keep current fragment
    public static int activeFragment = 0;
    // To keep last fragment
    public static int lastActiveFragment = 0;

    public boolean isDepartureStationSelected = false;
    public boolean isOneWaySelected = true;
    public boolean isDepartureDateSelected = true;
    public boolean isEconomyClassSelected = true;

    // Both  flag are used to handle to show default data when coming from side menu as fresh search
    // User selected data will be shown if coming from recent flight list or flight list frag
    public boolean isComingFromRecentFlightSearch = false;

    // This is used to handle if coming from filter fragment
    public boolean isComingFromFilterFrag = false;

    //
    public boolean isComingFromFlightListFrag = false;

    // Flag if coming from Side LTC click
    public static boolean isComingFromSideLTC = false;

    // Flag if coming from Side LTC click
    public boolean isComingFromFareQuote = false;

    // This is used to handle if coming from filter dialog
    public boolean isComingFromRoundFilterDialog = false;

    // get the selected id to search in main holder
    public int userSelectedUniqueIdOfFlight = -1;

    // Coming from pass list page
    public boolean isComingFromPassengerListPage = false;

    // Is air flag for dta in passenger
    public boolean isAirAsia = false;

    // Is coming from GDS FlightSearch Or International flight search
    public boolean isGdsOrInternatioal = false;


    // This variable is used to handl back press from farequote
    public int FARE_QUOTE_BACK_HANDLER_VAR;


    // To hide dialog of BookingHistoryService call
    public static boolean hideDialog = false;


    // This Variable is used to check Calendar month counter
    //public boolean isRounCalanderFirstTimeIncrement = false;
    public boolean isClickedDepartDate = false;
    public boolean isClickedReturnDate = false;


    // this is use for identifying screen in roundtrip for onward_return, round_onward, round_return
    public int SCREEN_TYPE;

    public static int refreshPlanerFragment = 0;
    public static boolean isRunning;
    public ActivityMain() {
        super();
    }

    //public static boolean isSessionExpired = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = ActivityMain.this;
        // Initialize
        initView();


        initializeVariables();

        // set data In Air Data Holder
        setdataInAirDataHolder();
        //Default Fragment Loading first time
        if (savedInstanceState == null) {
            // open when work on add passenger done
            lastActiveFragment = Constant.PLANNER_FRAGMENT;
            ProjectUtil.replaceFragment(ActivityMain.this, new FragmentPlanner(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

            Pref.clearPref(this);
            AirDatabase airDatabase = new AirDatabase(ActivityMain.this);
            airDatabase.removeAuthToken();
            Pref.clearPref(this);
            //Pref.setString(this, AppKeys.USER_DETAIL_FIRST_NAME, "");
        }

        //if (Pref.getBoolean(this)) startThread();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    protected void onPause() {
        super.onPause();
        isRunning = false;
    }

    private void initView() {

        setContentView(R.layout.activity_main);
        setupUI(findViewById(R.id.frame_layout));
        //getToolbar();
        AirHeader.createHeader(ActivityMain.this, getResources().getString(R.string.PLANNER_HEADER_TEXT));

    }

    private void getFlightIconPath() {
        if(Pref.getString(context,AppKeys.FLIGHT_ICON_PATH).equals("")){
        //NetworkingUtils.showProgress(ActivityMain.this);
        Networking.getFileIconPath(new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //NetworkingUtils.dismissProgress();
                Gson gson = new Gson();
                PojoFlightIcon pojoFlightIcon = gson.fromJson(response.toString(), PojoFlightIcon.class);
                if (pojoFlightIcon.getStatus().equals("SUCCESS")) {
                    //Pref.setString(context,AppKeys.FLIGHT_ICON_PATH,pojoFlightIcon.getData().replace("localhost:18081",Networking.BASE_URL_IP));
                    Pref.setString(context, AppKeys.FLIGHT_ICON_PATH, pojoFlightIcon.getData());
                } else {
                    //NetworkingUtils.internalServerIssue(context);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //NetworkingUtils.dismissProgress();
                //NetworkingUtils.noInternetAccess(context);
            }
        });
        }
    }

//    public void getFloatingBtn(boolean isShow){
//        if(isShow) {
//            mFloatinFilterBtn = (FloatingActionButton) findViewById(R.id.FLOATING_FILTER_BTN);
//            mFloatinFilterBtn.setVisibility(View.VISIBLE);
//        }else{
//            mFloatinFilterBtn = (FloatingActionButton) findViewById(R.id.FLOATING_FILTER_BTN);
//            mFloatinFilterBtn.setVisibility(View.GONE);
//        }
//    }


//    public void getToolbar(){
//        // Always cast your custom Toolbar here, and set it as the ActionBar.
//        Toolbar tb = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(tb);
//
//        //getSupportActionBar().hide();
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, tb, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.setDrawerListener(toggle);
//        toggle.syncState();
//
////        // Get the ActionBar here to configure the way it behaves.
////        final ActionBar ab = getSupportActionBar();
////        //ab.setHomeAsUpIndicator(R.drawable.ic_menu); // set a custom icon for the default home button
////        ab.setDisplayShowHomeEnabled(true); // show or hide the default home button
////        ab.setDisplayHomeAsUpEnabled(true);
////        ab.setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout
////        ab.setDisplayShowTitleEnabled(false); // disable the default title element here (for centered title)
////
////        ab.setSubtitle("Sub Title ");
//    }


    public void initializeVariables() {

    }


    // set data In Air Data Holder
    public void setdataInAirDataHolder() {
        AirDataHolder.getListHolder().getList().clear();

        ArrayList<AirDataModel> alAirData = new ArrayList<AirDataModel>();
        AirDataModel alObjAirData = new AirDataModel();
        alObjAirData.setTripType("One way");
        alObjAirData.setFromStation("DEL");
        alObjAirData.setToStation("BOM");
        alObjAirData.setFromStationCity("New Delhi IN");
        alObjAirData.setToStationCity("Mumbai IN");
        Calendar lObjCal = new GregorianCalendar();


        alObjAirData.setDepDate(lObjCal.getTime().toString());
        alObjAirData.setReturnDate(lObjCal.getTime().toString());

        //String curDate = new ServerDateSharedPrefernce(context).getServerDate();//new SimpleDateFormat(DateUtility.lStrGMTDateFormater).format(new Date());
        //String[] dateArray =  curDate.split(" ");
        // String formatedDateWithZeroHourTime = dateArray[0] +" "+dateArray[1]+" "+dateArray[2]+" 00:00:00 "+dateArray[4]+" "+dateArray[5];
        // Date currentDate = DateUtility.getDateInFormat(formatedDateWithZeroHourTime);

        alObjAirData.setAdultPassNum(1);
        alObjAirData.setChildPassNum(0);
        alObjAirData.setInfantPassNum(0);
        alObjAirData.setTravelClass("E");

        alAirData.add(alObjAirData);

        AirDataHolder.getListHolder().setList(alAirData);

    }

    @Override
    public void onBackPressed() {
        if (AirHeader.drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            AirHeader.drawerLayout.closeDrawers();
        } else if (backStackEnable) {
            showAlertDialog();
        } else if (paymentPage) {
            FragmentTransaction transaction;
            transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout, new FragmentTicket());
            transaction.commit();
        } else if (ticketPage) {
            FragmentTransaction transaction;
            transaction = getSupportFragmentManager().beginTransaction();
            FragmentPlanner fragmentPlanner = new FragmentPlanner();
            Bundle bundle = new Bundle();
            bundle.putString("previousData", "Clear");
            if (refreshPlanerFragment == 1) fragmentPlanner.setArguments(bundle);
            transaction.replace(R.id.frame_layout, fragmentPlanner);
            transaction.commit();
        } else if (soldOutDialog) {
            soldOutDialog = false;
            FragmentTransaction transaction;
            transaction = getSupportFragmentManager().beginTransaction();
            FragmentPlanner fragmentPlanner = new FragmentPlanner();
            Bundle bundle = new Bundle();
            bundle.putString("previousData", "Clear");
            if (refreshPlanerFragment == 1) fragmentPlanner.setArguments(bundle);
            transaction.replace(R.id.frame_layout, fragmentPlanner);
            transaction.commit();
        } /*else if (isSessionExpired) {
            sessionExpireDialog();
        }*/ else {
            super.onBackPressed();
        }
    }


    private synchronized void backStack() {
        //if(ActivityMain.backstack){
        //    super.onBackPressed();
        //}else if(!ActivityMain.flagIsSliderOpen) {
        //  new BackStackManagement(ActivityMain.this).initFragment(ActivityMain.activeFragment);
        //} else {
        // ActivityMain.flagIsSliderOpen = true;
        //AirHeader.drawerLayout.closeDrawers();
        //}
    }

    private void showAlertDialog() {

        //  AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

        // Setting Dialog Title
        alertDialog.setTitle(R.string.app_name);

        // Setting Dialog Message
        alertDialog.setMessage(R.string.EXIT);

//      // Setting Icon to Dialog
//      alertDialog.setIcon(R.drawable.adult);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                ((AppCompatActivity) context).finish();
                System.exit(0);
                ///((TBaseActivity) context).onFinish();
            }
        });

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });

        // Showing Alert Message
        ProjectUtil.dialogColorAlert(alertDialog);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isRunning = true;
        getFlightIconPath();
        /*if (isSessionExpired) {
            //backStackEnable = true;
            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            FragmentLogin fragmentLogin = new FragmentLogin();
            Bundle bundle = new Bundle();
            bundle.putString("logout", "logout");
            fragmentLogin.setArguments(bundle);
            fragmentTransaction.replace(R.id.frame_layout, fragmentLogin);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }*/
    }

    private void sessionExpireDialog() {

        //  AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

        // Setting Dialog Title
        alertDialog.setTitle(R.string.app_name);

        // Setting Dialog Message
        alertDialog.setMessage(R.string.EXIT);

//      // Setting Icon to Dialog
//      alertDialog.setIcon(R.drawable.adult);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                AirDatabase airDatabase = new AirDatabase(ActivityMain.this);
                airDatabase.removeAuthToken();
                //isSessionExpired = true;
                SliderMenu.txtUserId.setText("Guest");
                Pref.setString(ActivityMain.this, AppKeys.USER_DETAIL_ADDRESS_1, "");
                Pref.setString(ActivityMain.this, AppKeys.USER_DETAIL_ADDRESS_2, "");
                Pref.setString(ActivityMain.this, AppKeys.USER_DETAIL_FIRST_NAME, "");
                Pref.setString(ActivityMain.this, AppKeys.USER_DETAIL_LAST_NAME, "");
                Pref.setString(ActivityMain.this, AppKeys.USER_DETAIL_CITY, "");
                Pref.setString(ActivityMain.this, AppKeys.USER_DETAIL_STATE, "");
                Pref.setString(ActivityMain.this, AppKeys.USER_DETAIL_COUNTRY, "");
                Pref.setString(ActivityMain.this, AppKeys.USER_DETAIL_PIN_CODE, "");
                Pref.setString(ActivityMain.this, AppKeys.USER_DETAIL_EMAIL, "");
                Pref.setString(ActivityMain.this, AppKeys.USER_DETAIL_MOBILE_NO, "");
                Pref.setBoolean(ActivityMain.this, false);
                ((AppCompatActivity) context).finish();
                finish();
                System.exit(0);
                ///((TBaseActivity) context).onFinish();
            }
        });

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });

        // Showing Alert Message
        ProjectUtil.dialogColorAlert(alertDialog);
    }

    private Waiter waiter;  //Thread which controls idle time

    public void startThread() {
        Log.e("Start", "Start");
        //if (waiter.isAlive())
        //    waiter.interrupt();
        //waiter=new Waiter(15*60*1000); //15 min
        waiter = new Waiter(4 * 60 * 1000); //4 min
        waiter.start();
    }

    public void touch() {
        if (Pref.getBoolean(this)) waiter.touch();
    }

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }*/
}
