package com.irctc.air.activity;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.irctc.air.AppController;

import static com.irctc.air.util.ProjectUtil.hideSoftKeyboard;

/**
 * Created by vivek on 04/13/2016
 */
public class BaseActivity extends AppCompatActivity {


    int READ_PHONE_STATE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void setupUI(View view) {
        //Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {

            view.setOnTouchListener(new View.OnTouchListener() {

                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(BaseActivity.this);
                    return false;
                }

            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {

            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

                View innerView = ((ViewGroup) view).getChildAt(i);

                setupUI(innerView);
            }
        }
    }


    public void removePhoneKeypad(View view) {
        InputMethodManager inputManager = (InputMethodManager) view
                .getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        IBinder binder = view.getWindowToken();
        inputManager.hideSoftInputFromWindow(binder,
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    /*@Override
    public void onUserInteraction() {
        super.onUserInteraction();
        ((ActivityMain) ActivityMain.context).touch();
    }*/


    /*************************************************************************************************
     * MASHMELLOW PERMISSION HANDLING
     ************************************************************************************************/
    /**
     /**
     * Requests the Camera permission.
     * If the permission has been denied previously, a alert will prompt the user to grant the
     * permission, otherwise it is requested directly.
     */
//    public void requestPermission(Activity activity) {
//        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_PHONE_STATE)) {
//
//            READ_PHONE_STATE = ContextCompat.checkSelfPermission(BaseActivity.this,Manifest.permission.READ_PHONE_STATE);
//
//
//            // Provide an additional rationale to the user if the permission was not granted
//            // and the user would benefit from additional context for the use of the permission.
//            // For example if the user has previously denied the permission.
//
//            // Here, thisActivity is the current activity
//            if (ContextCompat.checkSelfPermission(BaseActivity.this,
//                    Manifest.permission.READ_PHONE_STATE)
//                    != PackageManager.PERMISSION_GRANTED) {
//
//                // Should we show an explanation?
//                if (ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this,
//                        Manifest.permission.READ_PHONE_STATE)) {
//
//                    // Show an expanation to the user *asynchronously* -- don't block
//                    // this thread waiting for the user's response! After the user
//                    // sees the explanation, try again to request the permission.
//                    Toast.makeText(BaseActivity.this, "Permission by wish allowed.", Toast.LENGTH_SHORT).show();
//                } else {
//
//                    // No explanation needed, we can request the permission.
//
//                    ActivityCompat.requestPermissions(BaseActivity.this,
//                            new String[]{Manifest.permission.READ_PHONE_STATE},
//                            READ_PHONE_STATE);
//                    Toast.makeText(BaseActivity.this, "Permission forcefully allowed.", Toast.LENGTH_SHORT).show();
//                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
//                    // app-defined int constant. The callback method gets the
//                    // result of the request.
//                }
//            }
//
//
//
//        }
//    }
//
//
//
//    /**
//     * Callback received when a permissions request has been completed.
//     */
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,@NonNull int[] grantResults) {
//
//        if (requestCode == READ_PHONE_STATE) {
//
//            // Received permission result for camera permission.est.");
//            // Check if the only required permission has been granted
//            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                // READ_PHONE_STATE permission has been granted, preview can be displayed
//                Toast.makeText(BaseActivity.this, "Permission allowed.", Toast.LENGTH_SHORT).show();
//            } else {
//                Toast.makeText(BaseActivity.this, "Permission not allowed.", Toast.LENGTH_SHORT).show();
//            }
//        }
//    }


}
