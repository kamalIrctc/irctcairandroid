package com.irctc.air.activity;

import android.os.Bundle;
import android.widget.TextView;

import com.irctc.air.R;

/**
 * Created by tourism on 11/29/2016.
 */

public class UnderMaintenanceActivity extends BaseActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.maintenance_layout);

        TextView msg = (TextView) findViewById(R.id.TXT_MAINTENANCE_MSG);

        msg.setText(getIntent().getStringExtra("DOWN_MSG"));
       // msg.setText("App is Under Maintenance Please Try After sometime");

    }
}
