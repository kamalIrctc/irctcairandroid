package com.irctc.air.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.irctc.air.R;


/**
 * Created by tourism on 8/2/2016.
 */
public class DashboardActivity extends BaseActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_layout);

        initializeVariable();
    }

    private void initializeVariable() {

        ImageView btnAir = (ImageView) findViewById(R.id.BTN_AIR);
        ImageView btnTourism = (ImageView) findViewById(R.id.BTN_TOURISM);

        btnAir.setOnClickListener(this);
        btnTourism.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.BTN_TOURISM:

                // Use package name which we want to check
                boolean isAppInstalled = appInstalledOrNot("com.irctc.tourism");

                if(isAppInstalled) {

                    //This intent will help you to launch if the package is already installed
                    Intent LaunchIntent = getPackageManager().getLaunchIntentForPackage("com.irctc.tourism");
                    startActivity(LaunchIntent);

                }else{
                    // Going to Play store for install the app
                    Uri marketUri = Uri.parse("market://details?id=com.irctc.tourism");
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
                    startActivity(marketIntent);
                }

                //this.finish();
                break;

            case R.id.BTN_AIR:

                startActivity(new Intent(this, ActivityMain.class));
                this.finish();

                break;

            default:
                break;
        }
    }

    private boolean appInstalledOrNot(String uri) {

        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }
}
