package com.irctc.air.util;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.appyvet.rangebar.RangeBar;
import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.fragment.FragmentRoundTripDomestic;
import com.irctc.air.model.FlightFilterBean;
import com.irctc.air.model.FlightOnWardDetailBean;
import com.irctc.air.model.InnerFlightDetailsBeans;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by tourism on 5/12/2016.
 */
public class RoundFilterEventHandler implements View.OnClickListener, RangeBar.OnRangeBarChangeListener, DialogInterface.OnKeyListener {

    ActivityMain mainActivity;
    FlightFilterBean filterBean;
    Dialog view;

    CheckBox zeroStopsCheckBox;
    CheckBox oneStopsCheckBox;
    CheckBox twoStopsMoreCheckBox;

    CheckBox chBoxfilterOnward_1;
    CheckBox chBoxfilterOnward_2;
    CheckBox chBoxfilterOnward_3;
    CheckBox chBoxfilterOnward_4;

    CheckBox chBoxfilterReturn_1;
    CheckBox chBoxfilterReturn_2;
    CheckBox chBoxfilterReturn_3;
    CheckBox chBoxfilterReturn_4;

    private RangeBar roundRangeBar;

    private TextView minSelectedFlightFare;
    private TextView maxSelectedFlightFare;
    private TextView minFlightFare;
    private TextView maxFlightFare;

    ArrayList<FlightOnWardDetailBean> mObjOnwrdClone;
    ArrayList<FlightOnWardDetailBean> mObjReturnClone;
    ListView listViewOnwardFlight;
    ListView listViewReturnFlight;

    ArrayList<FlightOnWardDetailBean> specificFlight;
    ListView listViewRouOnwGdsFlight;

    int SCREEN_TYPE;
    FragmentRoundTripDomestic roundTrip;

    private int minRound;
    private int maxReturn;

    ArrayList<FlightOnWardDetailBean> mSpecificCloneOfClone = new ArrayList<>();
    ArrayList<FlightOnWardDetailBean> mOnwCloneOfClone = new ArrayList<>();
    ArrayList<FlightOnWardDetailBean> mRetCloneOfClone = new ArrayList<>();

    @Override
    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {

            mainActivity.isComingFromRoundFilterDialog = true;
 //           performFilterOPt();
//            resetAllFields();

            view.cancel();
            if(SCREEN_TYPE == 2){

                specificFlight.clear();
                specificFlight.addAll(mSpecificCloneOfClone);
                listViewRouOnwGdsFlight.invalidateViews();
                roundTrip.doPerform(specificFlight, null);

            }else{

                mObjOnwrdClone.clear();
                mObjReturnClone.clear();
                mObjOnwrdClone.addAll(mOnwCloneOfClone);
                mObjReturnClone.addAll(mRetCloneOfClone);
                listViewOnwardFlight.invalidateViews();
                listViewReturnFlight.invalidateViews();
                roundTrip.doPerform(mObjOnwrdClone, mObjReturnClone);
            }
        }
        return true;
    }


    public interface AfterApply{
        void  doPerform(ArrayList<FlightOnWardDetailBean> onwrdClone, ArrayList<FlightOnWardDetailBean> returnClone);
    }

    public RoundFilterEventHandler(FragmentRoundTripDomestic fragmentRoundTrip, ActivityMain mainActivity, Dialog lObjDialogShowFilterOption, ArrayList<FlightOnWardDetailBean> onwrdClone, ArrayList<FlightOnWardDetailBean> returnClone, ListView listViewOnwardFlight, ListView listViewReturnFlight, int SCREEN_NO, FlightFilterBean flightFilterBean){

        // USE FOR THE TWO LIST VIEW
        this.SCREEN_TYPE = SCREEN_NO;
        this.mObjReturnClone = returnClone;
        this.mObjOnwrdClone = onwrdClone;
        this.mainActivity = mainActivity;
        filterBean = flightFilterBean;
        view = lObjDialogShowFilterOption;
        initializeDialogVariable(view);
        roundTrip = fragmentRoundTrip;
        ///////////////////////////////////////
        this.listViewOnwardFlight = listViewOnwardFlight;
        this.listViewReturnFlight = listViewReturnFlight;

        mOnwCloneOfClone.addAll(onwrdClone);
        mRetCloneOfClone.addAll(returnClone);

    }

    public RoundFilterEventHandler(FragmentRoundTripDomestic fragmentRoundTrip, ActivityMain mainActivity, Dialog lObjDialogShowFilterOption, ArrayList<FlightOnWardDetailBean> specificFlight, ListView listRouOnwFlight, int SCREEN_NO, FlightFilterBean flightFilterBean){


        // USE FOR THE ONE LIST VIEW
        this.SCREEN_TYPE = SCREEN_NO;
        this.specificFlight = specificFlight;
        this.mainActivity = mainActivity;
        filterBean = flightFilterBean;

        view = lObjDialogShowFilterOption;
        initializeDialogVariable(view);
        roundTrip = fragmentRoundTrip;

        this.listViewRouOnwGdsFlight = listRouOnwFlight;
        mSpecificCloneOfClone.addAll(specificFlight);
    }

    private void initializeDialogVariable( Dialog view){

            LinearLayout lLReset = (LinearLayout) view.findViewById(R.id.FILTER_RESET_LAY);

            zeroStopsCheckBox = (CheckBox) view.findViewById(R.id.roundFilterCkBoxZeroStops);
            oneStopsCheckBox = (CheckBox) view.findViewById(R.id.roundFilterCkBoxOneStops);
            twoStopsMoreCheckBox = (CheckBox) view.findViewById(R.id.roundFilterCkBoxMoreStops);

            chBoxfilterOnward_1 = (CheckBox) view.findViewById(R.id.roundChBoxfilterOnward_1);
            chBoxfilterOnward_2 = (CheckBox) view.findViewById(R.id.roundChBoxfilterOnward_2);
            chBoxfilterOnward_3 = (CheckBox) view.findViewById(R.id.roundChBoxfilterOnward_3);
            chBoxfilterOnward_4 = (CheckBox) view.findViewById(R.id.roundChBoxfilterOnward_4);

            chBoxfilterReturn_1 = (CheckBox) view.findViewById(R.id.roundChBoxfilterReturn_1);
            chBoxfilterReturn_2 = (CheckBox) view.findViewById(R.id.roundChBoxfilterReturn_2);
            chBoxfilterReturn_3 = (CheckBox) view.findViewById(R.id.roundChBoxfilterReturn_3);
            chBoxfilterReturn_4 = (CheckBox) view.findViewById(R.id.roundChBoxfilterReturn_4);

            minSelectedFlightFare = (TextView) view.findViewById(R.id.txtSelectedRoundFlightFareMin);
            maxSelectedFlightFare = (TextView) view.findViewById(R.id.txtSelectedRoundFlightFareMax);
            minFlightFare = (TextView) view.findViewById(R.id.txtRoundFlightFareMin);
            maxFlightFare = (TextView) view.findViewById(R.id.txtRoundFlightFareMax);

//            roundRangeBar = (RangeBar) view.findViewById(R.id.roundRangebarPrice);
//
//            roundRangeBar.setBarColor(Color.parseColor("#9DA2A3"));
//            roundRangeBar.setPinColor(Color.parseColor("#21A1CE")); // change the background color of pin like drop
//            roundRangeBar.setPinRadius(30);// change the radious of pin like drop
//            roundRangeBar.setTickColor(Color.TRANSPARENT); // background color line
//            roundRangeBar.setConnectingLineColor(Color.parseColor("#21A1CE"));
//            roundRangeBar.setSelectorColor(Color.parseColor("#4F565E"));//
//            roundRangeBar.setConnectingLineWeight(2);

            initRangebar();


           // roundRangeBar.setOnRangeBarChangeListener(this);


//            roundRangeBar.setTickEnd(AirDataHolder.getListHolder().getList().get(0).getOnwFlightFareMax());
//            roundRangeBar.setTickStart(AirDataHolder.getListHolder().getList().get(0).getOnwFlightFareMin());
//            roundRangeBar.setTickInterval(100);



            Button btnFilterApply = (Button)view.findViewById(R.id.roundFilterBtnApply);

            zeroStopsCheckBox.setOnClickListener(this);
            oneStopsCheckBox.setOnClickListener(this);
            twoStopsMoreCheckBox.setOnClickListener(this);

            chBoxfilterOnward_1.setOnClickListener(this);
            chBoxfilterOnward_2.setOnClickListener(this);
            chBoxfilterOnward_3.setOnClickListener(this);
            chBoxfilterOnward_4.setOnClickListener(this);

            chBoxfilterReturn_1.setOnClickListener(this);
            chBoxfilterReturn_2.setOnClickListener(this);
            chBoxfilterReturn_3.setOnClickListener(this);
            chBoxfilterReturn_4.setOnClickListener(this);

            btnFilterApply.setOnClickListener(this);
            lLReset.setOnClickListener(this);

            view.setOnKeyListener(this);

            // it is used to provide previous selection
            providePreviousSelection();

    }

    private void providePreviousSelection() {

        if(filterBean.getStopZero()!= 0)
            zeroStopsCheckBox.setChecked(true);
        if(filterBean.getStopOne()!= 0)
            oneStopsCheckBox.setChecked(true);
        if(filterBean.getStopTwoMore()!= 0)
            twoStopsMoreCheckBox.setChecked(true);

        if(filterBean.getOnwardOption1()!= 0)
            chBoxfilterOnward_1.setChecked(true);
        if(filterBean.getOnwardOption2()!= 0)
            chBoxfilterOnward_2.setChecked(true);
        if(filterBean.getOnwardOption3()!= 0)
            chBoxfilterOnward_3.setChecked(true);
        if(filterBean.getOnwardOption4()!= 0)
            chBoxfilterOnward_4.setChecked(true);

        if(filterBean.getReturnOption1()!= 0)
            chBoxfilterReturn_1.setChecked(true);
        if(filterBean.getReturnOption2()!= 0)
            chBoxfilterReturn_2.setChecked(true);
        if(filterBean.getReturnOption3()!= 0)
            chBoxfilterReturn_3.setChecked(true);
        if(filterBean.getReturnOption4()!= 0)
            chBoxfilterReturn_4.setChecked(true);
    }

    @Override
    public void onClick(View v) {

        /************************************
        * * CLICK HANDLING FOR FILTER DIALOG
        ************************************/

        switch (v.getId()){

                // FOR STOPS HANDLING
                case R.id.roundFilterCkBoxZeroStops:

                    if(zeroStopsCheckBox.isChecked()){
                        // set the checked  value here
                        filterBean.setStopZero(1);
                    }else{
                        // set the unchecked value here
                        filterBean.setStopZero(0);
                    }

                    break;

                case R.id.roundFilterCkBoxOneStops:
                    if(oneStopsCheckBox.isChecked()){
                        // set the checked value here
                        filterBean.setStopOne(1);
                    }else{
                        // set the unchecked value here
                        filterBean.setStopOne(0);
                    }

                    break;

                case R.id.roundFilterCkBoxMoreStops:

                    if(twoStopsMoreCheckBox.isChecked()){
                        // set the checked value here
                        filterBean.setStopTwoMore(1);
                    }else{
                        // set the unchecked value here
                        filterBean.setStopTwoMore(0);
                    }

                    break;

                // FOR ONWARD TIME HANDLING
                case R.id.roundChBoxfilterOnward_1:

                    if(chBoxfilterOnward_1.isChecked()){
                        // set the checked value here
                        filterBean.setOnwardOption1(1);
                    }else{
                        // set the unchecked value here
                        filterBean.setOnwardOption1(0);
                    }
                    break;

                case R.id.roundChBoxfilterOnward_2:

                    if(chBoxfilterOnward_2.isChecked()){
                        // set the checked value here
                        filterBean.setOnwardOption2(1);
                    }else{
                        // set the unchecked value here
                        filterBean.setOnwardOption2(0);
                    }

                    break;
                case R.id.roundChBoxfilterOnward_3:

                    if(chBoxfilterOnward_3.isChecked()){
                        // set the checked value here
                        filterBean.setOnwardOption3(1);
                    }else{
                        // set the unchecked value here
                        filterBean.setOnwardOption3(0);
                    }

                    break;
                case R.id.roundChBoxfilterOnward_4:

                    if(chBoxfilterOnward_4.isChecked()){
                        // set the checked value here
                        filterBean.setOnwardOption4(1);
                    }else{
                        // set the unchecked value here
                        filterBean.setOnwardOption4(0);
                    }

                    break;

                // FOR RETURN TIME HANDLING
                case R.id.roundChBoxfilterReturn_1:

                    if(chBoxfilterReturn_1.isChecked()){
                        // set the checked value here
                        filterBean.setReturnOption1(1);
                    }else{
                        // set the unchecked value here
                        filterBean.setReturnOption1(0);
                    }

                    break;
                case R.id.roundChBoxfilterReturn_2:

                    if(chBoxfilterReturn_2.isChecked()){
                        // set the checked value here
                        filterBean.setReturnOption2(1);
                    }else{
                        // set the unchecked value here
                        filterBean.setReturnOption2(0);
                    }

                    break;
                case R.id.roundChBoxfilterReturn_3:

                    if(chBoxfilterReturn_3.isChecked()){
                        // set the checked value here
                        filterBean.setReturnOption3(1);
                    }else{
                        // set the unchecked value here
                        filterBean.setReturnOption3(0);
                    }

                    break;
                case R.id.roundChBoxfilterReturn_4:

                    if(chBoxfilterReturn_4.isChecked()){
                        // set the checked value here
                        filterBean.setReturnOption4(1);
                    }else{
                        // set the unchecked value here
                        filterBean.setReturnOption4(0);
                    }

                    break;

                case R.id.roundFilterBtnApply:

                    mainActivity.isComingFromRoundFilterDialog = true;
                    performFilterOPt();


                    if(SCREEN_TYPE == 2){

                        if(specificFlight.size() > 0){
                            view.cancel();
                            roundTrip.doPerform(specificFlight, null);

                        }else{

                            specificFlight.clear();
                            specificFlight.addAll(mSpecificCloneOfClone);
                            new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.FLIGHT_FILTER_SEARCH_ERROR_TEXT), mainActivity.getResources().getString(R.string.FLIGHT_SEARCH_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE, null).generateAlert();
                        }


                    }else{

                        if(mObjOnwrdClone.size() > 0 && mObjReturnClone.size() > 0 ){
                            // resetAllFields();
                            view.cancel();
                            roundTrip.doPerform(mObjOnwrdClone, mObjReturnClone);

                        }else{

                            mObjOnwrdClone.clear();
                            mObjReturnClone.clear();
                            mObjOnwrdClone.addAll(mOnwCloneOfClone);
                            mObjReturnClone.addAll(mRetCloneOfClone);
                            new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.FLIGHT_FILTER_SEARCH_ERROR_TEXT), mainActivity.getResources().getString(R.string.FLIGHT_SEARCH_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE, null).generateAlert();
                        }
                    }

                    break;
                case R.id.FILTER_RESET_LAY:

                    resetAllFields();
                    break;

                default:
                    break;

            }
    }

    private void performFilterOPt() {

        switch (SCREEN_TYPE){

            // MAIN SCREEN
            case 1:
                //filterOnUserPreference();

                mObjOnwrdClone = filterBasedOnOnwardStops(mObjOnwrdClone);
                mObjReturnClone  = filterBasedOnReturnStops(mObjReturnClone);
                listViewOnwardFlight.invalidateViews();
                listViewReturnFlight.invalidateViews();
                break;

            // ROUND ONWARD SCREEN
            case 2:
                //filterOnUserPreference();
                specificFlight = filterBasedOnStopsOnGDSFlight(specificFlight);
                listViewRouOnwGdsFlight.invalidateViews();
                break;

            // ROUND RETURN SCREEN
            case 3:
                //filterOnUserPreference();
                assignMinMaxPriceRouRet();
                mObjOnwrdClone = filterBasedRouRetOnOnwardStops(mObjOnwrdClone);
                mObjReturnClone  = filterBasedRouRetOnReturnStops(mObjReturnClone);
                listViewOnwardFlight.invalidateViews();
                listViewReturnFlight.invalidateViews();
                break;
            default:
                break;
        }
    }

    // USE TO FILTER GDS FLIGHTS
    private ArrayList<FlightOnWardDetailBean> filterBasedOnStopsOnGDSFlight(ArrayList<FlightOnWardDetailBean> gdsFlightClone){

        ArrayList<FlightOnWardDetailBean> al = new ArrayList<>();
        al.addAll(gdsFlightClone);

        gdsFlightClone.clear();

        if(filterBean.getStopZero() != 0 || filterBean.getStopOne() != 0 || filterBean.getStopTwoMore() != 0){

            for (int i = 0; i <al.size() ; i++) {

                FlightOnWardDetailBean flightDetailBean =  al.get(i);

                int onwStops = flightDetailBean.getFlightOnwardTotalStops();
                int retStops = flightDetailBean.getFlightreturnTotalStops();

                ArrayList<InnerFlightDetailsBeans> flight  = flightDetailBean.getFlight();

                if(flight.size() > 0) { //3

                    if (filterBean.getStopZero() != 0) { // S

                        if(onwStops == 0 && retStops == 0)
                        {  // Add flight to clone
                            gdsFlightClone.add(flightDetailBean);
                            continue;
                        }
                    }
                    if (filterBean.getStopOne() != 0) { // S
                        if(onwStops == 1 && retStops == 1)
                        {  // Add flight to clone
                            gdsFlightClone.add(flightDetailBean);
                            continue;
                        }
                    }
                    if (filterBean.getStopTwoMore() != 0) { // S
                        if(onwStops > 1 && retStops > 1)
                        {  // Add flight to clone
                            gdsFlightClone.add(flightDetailBean);
                            continue;
                        }
                    }
                }
            }
        }

        if((gdsFlightClone.size() > 0) && (filterBean.getStopZero() != 0 || filterBean.getStopOne() != 0 || filterBean.getStopTwoMore() != 0)){

        }else {
            if ((filterBean.getStopZero() != 0 || filterBean.getStopOne() != 0 || filterBean.getStopTwoMore() != 0)) {

            }else{
                specificFlight = al;//(ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightGDS().clone();
            }
        }

        if(filterBean.getOnwardOption1() != 0 || filterBean.getOnwardOption2() != 0 || filterBean.getOnwardOption3() != 0 || filterBean.getOnwardOption4() != 0){

            if(specificFlight.size() > 0) {
                // filter the clone data on behalf of Onwards time
                filterBasedOnOnwardGDSFlighTime();
            }
        }

        if(filterBean.getReturnOption1() != 0 || filterBean.getReturnOption2() != 0 || filterBean.getReturnOption3() != 0 || filterBean.getReturnOption4() != 0){

            if(specificFlight.size() > 0) {
                // filter the clone data on behalf of Onwards time
                filterBasedOnReturnGDSFlighTime();
            }
        }


        if(specificFlight.size() > 0) {
            if (filterBean.getIsChange() != 0) {
                // filter the clone data on behalf of fare Price
                AppLogger.enable();
                AppLogger.e("MIN Avl=> ", "" + filterBean.getMinPrice());
                AppLogger.e("MAAX Avl=> ", "" + filterBean.getMaxPrice());
                gdsFlightClone = filterBasedOnPrice(specificFlight);
            }
        }


        //filterBean = null;

        AppLogger.enable();
        AppLogger.e("Flight SIZE =>", "" + gdsFlightClone.size());

        // Show alert if filtered list is zero and provide new clone with all data
        if(gdsFlightClone.size() == 0){

            //gdsFlightClone = al;//(ArrayList<FlightOnWardDetailBean>)AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightGDS().clone();
            //new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.FLIGHT_FILTER_SEARCH_ERROR_TEXT), mainActivity.getResources().getString(R.string.FLIGHT_SEARCH_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE, null).generateAlert();
        }

        return gdsFlightClone;
    }

    // FILTER GDS FLIGHT ON BEHALF OF RETURN TIME
    private void filterBasedOnReturnGDSFlighTime() {

        ArrayList<FlightOnWardDetailBean> filteredbasedRet = new ArrayList<FlightOnWardDetailBean>();

        int length =  specificFlight.size();
        for (int i = 0; i < length ; i++) {

            FlightOnWardDetailBean flighBeans = specificFlight.get(i);
            Date dateX =  DateUtility.getBeforeAfterTime(flighBeans.getFlightRetDepartureTime(), 0, 0);

            if (filterBean.getReturnOption1() != 0) {     // S

                Date date1 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getReturnDate(),0,1);
                Date date2 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getReturnDate(),6,1);

                if(compareDateTime(date1,date2,dateX)){
                    // remains the flight in the clone
                    filteredbasedRet.add(flighBeans);
                    //
                }

            }
            if (filterBean.getReturnOption2() != 0) { // S

                Date date1 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getReturnDate(),6,1);
                Date date2 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getReturnDate(),12,1);

                if(compareDateTime(date1,date2,dateX)){
                    // remains the flight in the clone
                    filteredbasedRet.add(flighBeans);
                    //continue;
                }

            }
            if (filterBean.getReturnOption3() != 0) {  // S
                Date date1 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getReturnDate(),12,1);
                Date date2 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getReturnDate(),18,1);

                if(compareDateTime(date1,date2,dateX)){
                    // remains the flight in the clone
                    filteredbasedRet.add(flighBeans);
                    //continue;
                }

            }
            if (filterBean.getReturnOption4() != 0) {  // S

                Date date1 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getReturnDate(),18,1);
                Date date2 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getReturnDate(),24,1);

                if(compareDateTime(date1,date2,dateX)){
                    // remains the flight in the clone
                    filteredbasedRet.add(flighBeans);
                    // continue;
                }

            }
        }
        specificFlight.clear();
        specificFlight.addAll(filteredbasedRet);
    }

    // FILTER GDS FLIGHT ON BEHALF OF ONWARD TIME
    private void filterBasedOnOnwardGDSFlighTime() {

        ArrayList<FlightOnWardDetailBean> filteredbasedRet = new ArrayList<FlightOnWardDetailBean>();

        int length =  specificFlight.size();
        for (int i = 0; i < length ; i++) {

            FlightOnWardDetailBean flighBeans = specificFlight.get(i);
            Date dateX =  DateUtility.getBeforeAfterTime(flighBeans.getFlightOnwDepartureTime(), 0, 0);


            if (filterBean.getOnwardOption1() != 0) {     // S

                Date date1 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(),0,1);
                Date date2 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(),6,1);

                if(compareDateTime(date1,date2,dateX)){
                    // remains the flight in the clone
                    filteredbasedRet.add(flighBeans);
                    //
                }
            }
            if (filterBean.getOnwardOption2() != 0) { // S

                Date date1 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(),6,1);
                Date date2 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(),12,1);

                if(compareDateTime(date1,date2,dateX)){
                    // remains the flight in the clone
                    filteredbasedRet.add(flighBeans);
                    //continue;
                }

            }
            if (filterBean.getOnwardOption3() != 0) {  // S
                Date date1 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(),12,1);
                Date date2 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(),18,1);

                if(compareDateTime(date1,date2,dateX)){
                    // remains the flight in the clone
                    filteredbasedRet.add(flighBeans);
                    //continue;
                }

            }
            if (filterBean.getOnwardOption4() != 0) {  // S

                Date date1 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(),18,1);
                Date date2 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(),24,1);

                if(compareDateTime(date1,date2,dateX)){
                    // remains the flight in the clone
                    filteredbasedRet.add(flighBeans);
                    // continue;
                }

            }
        }
        specificFlight.clear();
        specificFlight.addAll(filteredbasedRet);
    }

    private void assignMinMaxPriceRouRet() {

        // LCC FLIGHT
        int minLCC   = mObjOnwrdClone.get(0).getFlightFare();
        int maxLCC   = mObjOnwrdClone.get(0).getFlightFare();

        for (int i = 0; i < mObjOnwrdClone.size() ; i++) {

            if(i == 0){

                minLCC   = mObjOnwrdClone.get(0).getFlightFare();
                maxLCC   = mObjOnwrdClone.get(0).getFlightFare();
            }
            else{

                if(mObjOnwrdClone.get(i).getFlightFare() < minLCC ){
                    minLCC = mObjOnwrdClone.get(i).getFlightFare();
                }
                if(mObjOnwrdClone.get(i).getFlightFare() > maxLCC ){
                    maxLCC = mObjOnwrdClone.get(i).getFlightFare();
                }
            }


        }

        // ROU RET FLIGHT

        int minRouRet   = mObjReturnClone.get(0).getFlightFare();
        int maxRouRet   = mObjReturnClone.get(0).getFlightFare();

        for (int i = 0; i < mObjReturnClone.size() ; i++) {

            if(i == 0){

                minRouRet   = mObjReturnClone.get(0).getFlightFare();
                maxRouRet   = mObjReturnClone.get(0).getFlightFare();
            }
            else{

                if(mObjReturnClone.get(i).getFlightFare() < minRouRet ){
                    minRouRet = mObjReturnClone.get(i).getFlightFare();
                }
                if(mObjReturnClone.get(i).getFlightFare() > maxRouRet ){
                    maxRouRet = mObjReturnClone.get(i).getFlightFare();
                }
            }
        }

        minRound = minLCC;
        maxReturn = maxLCC;

        if(minRouRet < minRound){
            minRound = minRouRet;
        }
        if( maxRouRet > maxReturn){
            maxReturn = maxRouRet;
        }

    }

    private void resetAllFields() {

        if(zeroStopsCheckBox.isChecked()){
            filterBean.setStopOne(0);
            zeroStopsCheckBox.setChecked(false);
        }
        if(oneStopsCheckBox.isChecked()){
            filterBean.setStopOne(0);
            oneStopsCheckBox.setChecked(false);
        }
        if(twoStopsMoreCheckBox.isChecked()){
            filterBean.setStopTwoMore(0);
            twoStopsMoreCheckBox.setChecked(false);
        }
        if(chBoxfilterOnward_1.isChecked()){
            filterBean.setReturnOption1(0);
            chBoxfilterOnward_1.setChecked(false);
        }
        if(chBoxfilterOnward_2.isChecked()){
            filterBean.setReturnOption2(0);
            chBoxfilterOnward_2.setChecked(false);
        }
        if(chBoxfilterOnward_3.isChecked()){
            filterBean.setReturnOption3(0);
            chBoxfilterOnward_3.setChecked(false);
        }
        if(chBoxfilterOnward_4.isChecked()){
            filterBean.setReturnOption4(0);
            chBoxfilterOnward_4.setChecked(false);
        }
        if(chBoxfilterReturn_1.isChecked()){
            filterBean.setReturnOption1(0);
            chBoxfilterReturn_1.setChecked(false);
        }
        if(chBoxfilterReturn_2.isChecked()){
            filterBean.setReturnOption2(0);
            chBoxfilterReturn_2.setChecked(false);
        }
        if(chBoxfilterReturn_3.isChecked()){
            filterBean.setReturnOption3(0);
            chBoxfilterReturn_3.setChecked(false);
        }
        if(chBoxfilterReturn_4.isChecked()){
            filterBean.setReturnOption4(0);
            chBoxfilterReturn_4.setChecked(false);
        }

        if(filterBean.getIsChange() == 1){

            filterBean.setIsChange(0);
            filterBean.setMaxPrice(maxReturn);
            filterBean.setMinPrice(minRound);
            //initRangebar();
            roundRangeBar.setTickEnd(maxReturn);
            roundRangeBar.setTickStart(minRound);
            roundRangeBar.setTickInterval(1);

            minSelectedFlightFare.setText("\u20B9  " + minRound);
            maxSelectedFlightFare.setText("\u20B9  " + maxReturn);
        }
    }

    @Override
    public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {

        filterBean.setIsChange(1);
        filterBean.setMaxPrice(Integer.parseInt(rangeBar.getRightPinValue()));
        filterBean.setMinPrice(Integer.parseInt(rangeBar.getLeftPinValue()));
        minSelectedFlightFare.setText("\u20B9  " + Integer.parseInt(rangeBar.getLeftPinValue()));
        maxSelectedFlightFare.setText("\u20B9  " + Integer.parseInt(rangeBar.getRightPinValue()));
    }

    private void initRangebar(){

        roundRangeBar = (RangeBar) view.findViewById(R.id.roundRangebarPrice);
        roundRangeBar.setBarColor(Color.parseColor("#9DA2A3"));
        roundRangeBar.setPinColor(Color.parseColor("#21A1CE")); // change the background color of pin like drop
        roundRangeBar.setPinRadius(30);// change the radious of pin like drop
        roundRangeBar.setTickColor(Color.TRANSPARENT); // background color line
        roundRangeBar.setConnectingLineColor(Color.parseColor("#21A1CE"));
        roundRangeBar.setSelectorColor(Color.parseColor("#4F565E"));//
        roundRangeBar.setConnectingLineWeight(2);


        if(SCREEN_TYPE == 1) {

            minRound = AirDataHolder.getListHolder().getList().get(0).getOnwFlightFareMin();
            maxReturn = AirDataHolder.getListHolder().getList().get(0).getOnwFlightFareMax();

            if (AirDataHolder.getListHolder().getList().get(0).getRetFlightFareMin() < minRound) {
                minRound = AirDataHolder.getListHolder().getList().get(0).getRetFlightFareMin();
            }
            if (AirDataHolder.getListHolder().getList().get(0).getRetFlightFareMax() > maxReturn) {
                maxReturn = AirDataHolder.getListHolder().getList().get(0).getRetFlightFareMax();
            }
        }
        if(SCREEN_TYPE == 3){
            assignMinMaxPriceRouRet();
        }

        if(SCREEN_TYPE == 2){
            if(specificFlight.size() != 0){
                assignMinMaxPriceGDS();
            }
        }


        roundRangeBar.setPinTextFormatter(new RangeBar.PinTextFormatter() {
            @Override
            public String getText(String value) {
                return value;
            }
        });

        roundRangeBar.setOnRangeBarChangeListener(this);

        roundRangeBar.setTickEnd(maxReturn);
        roundRangeBar.setTickStart(minRound);
        roundRangeBar.setTickInterval(1);

        minFlightFare.setText("\u20B9  " + minRound);
        maxFlightFare.setText("\u20B9  " + maxReturn);
    }

    private void assignMinMaxPriceGDS() {

        // GDS FLIGHT
        int minGDS   = specificFlight.get(0).getFlightFare();
        int maxGDS   = specificFlight.get(0).getFlightFare();

        for (int i = 0; i < specificFlight.size() ; i++) {

            if(i == 0){

                minGDS   = specificFlight.get(0).getFlightFare();
                maxGDS   = specificFlight.get(0).getFlightFare();
            }
            else{

                if(specificFlight.get(i).getFlightFare() < minGDS ){
                    minGDS = specificFlight.get(i).getFlightFare();
                }
                if(specificFlight.get(i).getFlightFare() > maxGDS ){
                    maxGDS = specificFlight.get(i).getFlightFare();
                }
            }
        }
        minRound = minGDS;
        maxReturn = maxGDS;
    }

    private ArrayList<FlightOnWardDetailBean> filterBasedOnOnwardStops(ArrayList<FlightOnWardDetailBean> onwrdClone) {//

        ArrayList<FlightOnWardDetailBean> al = new ArrayList<>();
        al.addAll(onwrdClone);

        onwrdClone.clear();
        if(filterBean.getStopZero() != 0 || filterBean.getStopOne() != 0 || filterBean.getStopTwoMore() != 0){

            for (int i = 0; i <al.size() ; i++) {

                FlightOnWardDetailBean flightDetailBean =  al.get(i);

                int stops = flightDetailBean.getFlightOnwardTotalStops();

                ArrayList<InnerFlightDetailsBeans> flight  = flightDetailBean.getFlight();

                if(flight.size() > 0) { //3

                    if (filterBean.getStopZero() != 0) { // S

                        if(stops == 0)
                        {  // Add flight to clone
                            onwrdClone.add(flightDetailBean);
                            continue;
                        }
                    }
                    if (filterBean.getStopOne() != 0) { // S
                        if(stops == 1)
                        {  // Add flight to clone
                            onwrdClone.add(flightDetailBean);
                            continue;
                        }
                    }
                    if (filterBean.getStopTwoMore() != 0) { // S
                        if(stops > 1)
                        {  // Add flight to clone
                            onwrdClone.add(flightDetailBean);
                            continue;
                        }
                    }
                }
            }
        }

        if((onwrdClone.size() > 0) && (filterBean.getStopZero() != 0 || filterBean.getStopOne() != 0 || filterBean.getStopTwoMore() != 0)){

        }else {
            if ((filterBean.getStopZero() != 0 || filterBean.getStopOne() != 0 || filterBean.getStopTwoMore() != 0)) {

            }else{
                mObjOnwrdClone = al;//(ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().clone();
            }
        }

        if(filterBean.getOnwardOption1() != 0 || filterBean.getOnwardOption2() != 0 || filterBean.getOnwardOption3() != 0 || filterBean.getOnwardOption4() != 0){

            if(mObjOnwrdClone.size() > 0) {
                // filter the clone data on behalf of Onwards time
                filterBasedOnOnwardsTime();
            }
        }

        if(mObjOnwrdClone.size() > 0) {
            if (filterBean.getIsChange() != 0) {
                // filter the clone data on behalf of fare Price
                AppLogger.enable();
                AppLogger.e("MIN Avl=> ", "" + filterBean.getMinPrice());
                AppLogger.e("MAAX Avl=> ", "" + filterBean.getMaxPrice());
                onwrdClone = filterBasedOnPrice(mObjOnwrdClone);
            }
        }


        //filterBean = null;

        AppLogger.enable();
        AppLogger.e("Flight SIZE =>", "" + onwrdClone.size());

        // Show alert if filtered list is zero and provide new clone with all data
        if(onwrdClone.size() == 0){

           // onwrdClone = al;//(ArrayList<FlightOnWardDetailBean>)AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().clone();
           // new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.FLIGHT_FILTER_SEARCH_ERROR_TEXT), mainActivity.getResources().getString(R.string.FLIGHT_SEARCH_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE, null).generateAlert();
        }

        return onwrdClone;
    }

    private ArrayList<FlightOnWardDetailBean> filterBasedOnReturnStops(ArrayList<FlightOnWardDetailBean> returnClone) {

        ArrayList<FlightOnWardDetailBean> al = new ArrayList<>();
        al.addAll(returnClone);

        returnClone.clear();
        if(filterBean.getStopZero() != 0 || filterBean.getStopOne() != 0 || filterBean.getStopTwoMore() != 0){

            for (int i = 0; i <al.size() ; i++) {

                FlightOnWardDetailBean flightDetailBean =  al.get(i);

                ArrayList<InnerFlightDetailsBeans> flight  = flightDetailBean.getFlight();
                int stops = flightDetailBean.getFlightOnwardTotalStops();

                if(flight.size()>0) { //3

                    if (filterBean.getStopZero() != 0) { // S

                        if(stops == 0)
                        {  // Add flight to clone
                            returnClone.add(flightDetailBean);
                            continue;
                        }
                    }
                    if (filterBean.getStopOne() != 0) { // S
                        if(stops == 1)
                        {  // Add flight to clone
                            returnClone.add(flightDetailBean);
                            continue;
                        }
                    }
                    if (filterBean.getStopTwoMore() != 0) { // S
                        if(stops > 1)
                        {  // Add flight to clone
                            returnClone.add(flightDetailBean);
                            continue;
                        }
                    }
                }
            }
        }

        if((returnClone.size() > 0) && (filterBean.getStopZero() != 0 || filterBean.getStopOne() != 0 || filterBean.getStopTwoMore() != 0)){

        }else {
            if ((filterBean.getStopZero() != 0 || filterBean.getStopOne() != 0 || filterBean.getStopTwoMore() != 0)) {

            }else{
                mObjReturnClone = al;//(ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().clone();
            }
        }

        if(filterBean.getReturnOption1() != 0 || filterBean.getReturnOption2() != 0 || filterBean.getReturnOption3() != 0 || filterBean.getReturnOption4() != 0){

            if(mObjReturnClone.size() > 0) {
                // filter the clone data on behalf of return time
                filterBasedOnReturnTime();
            }
        }

        if(mObjReturnClone.size() > 0) {
            if (filterBean.getIsChange() != 0) {
                // filter the clone data on behalf of fare Price
                AppLogger.enable();
                AppLogger.e("MIN Avl=> ", "" + filterBean.getMinPrice());
                AppLogger.e("MAAX Avl=> ", "" + filterBean.getMaxPrice());
                returnClone = filterBasedOnPrice(mObjReturnClone);
            }
        }

        AppLogger.enable();
        AppLogger.e("Flight SIZE =>", "" + returnClone.size());

        // Show alert if filtered list is zero and provide new clone with all data
        if(returnClone.size() == 0){

           // returnClone = al;//(ArrayList<FlightOnWardDetailBean>)AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().clone();
           // new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.FLIGHT_FILTER_SEARCH_ERROR_TEXT), mainActivity.getResources().getString(R.string.FLIGHT_SEARCH_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE, null).generateAlert();
        }
        return returnClone;
    }

    private ArrayList<FlightOnWardDetailBean> filterBasedOnPrice(ArrayList<FlightOnWardDetailBean> flightClone) {

        ArrayList<FlightOnWardDetailBean> al = new ArrayList<>();
        al.addAll(flightClone);

        flightClone.clear();

        int length = flightClone.size();

        for (int i = 0; i < al.size() ; i++) {

            FlightOnWardDetailBean flightDetailBean =  al.get(i);

            if( flightDetailBean.getFlightFare() >= filterBean.getMinPrice() && flightDetailBean.getFlightFare() <= filterBean.getMaxPrice() ){


                flightClone.add(flightDetailBean);
            }
        }
        al.clear();
       return flightClone;
    }

    private void filterBasedOnOnwardsTime() {

        ArrayList<FlightOnWardDetailBean> filteredbasedQnw = new ArrayList<FlightOnWardDetailBean>();

        int length =  mObjOnwrdClone.size();
        for (int i = 0; i < length ; i++) {

            FlightOnWardDetailBean flighBeans = mObjOnwrdClone.get(i);
           // Date dateX =  DateUtility.getBeforeAfterTime(flighBeans.getFlight().get(0).getFlightDepartureTime(), 0, 0);

            Date dateX =  DateUtility.getBeforeAfterTime(flighBeans.getFlightOnwDepartureTime(), 0, 0);

            if (filterBean.getOnwardOption1() != 0) {     // S

                Date date1 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(),0,1);
                Date date2 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(),6,1);

                if(compareDateTime(date1,date2,dateX)){
                    // remains the flight in the clone
                    filteredbasedQnw.add(flighBeans);
                    //
                }
            }
            if (filterBean.getOnwardOption2() != 0) { // S

                Date date1 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(),6,1);
                Date date2 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(),12,1);

                if(compareDateTime(date1,date2,dateX)){
                    // remains the flight in the clone
                    filteredbasedQnw.add(flighBeans);
                    //continue;
                }

            }
            if (filterBean.getOnwardOption3() != 0) {  // S
                Date date1 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(),12,1);
                Date date2 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(),18,1);

                if(compareDateTime(date1,date2,dateX)){
                    // remains the flight in the clone
                    filteredbasedQnw.add(flighBeans);
                    //continue;
                }

            }
            if (filterBean.getOnwardOption4() != 0) {  // S

                Date date1 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(),18,1);
                Date date2 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(),24,1);

                if(compareDateTime(date1,date2,dateX)){
                    // remains the flight in the clone
                    filteredbasedQnw.add(flighBeans);
                    // continue;
                }

            }
        }
        mObjOnwrdClone.clear();
        mObjOnwrdClone.addAll(filteredbasedQnw);
    }

    private void filterBasedOnReturnTime() {

        ArrayList<FlightOnWardDetailBean> filteredbasedRet = new ArrayList<FlightOnWardDetailBean>();

        int length =  mObjReturnClone.size();
        for (int i = 0; i < length ; i++) {

            FlightOnWardDetailBean flighBeans = mObjReturnClone.get(i);
            Date dateX =  DateUtility.getBeforeAfterTime(flighBeans.getFlightOnwDepartureTime(), 0, 0);

            if (filterBean.getReturnOption1() != 0) {     // S

                Date date1 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getReturnDate(),0,1);
                Date date2 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getReturnDate(),6,1);

                if(compareDateTime(date1,date2,dateX)){
                    // remains the flight in the clone
                    filteredbasedRet.add(flighBeans);
                    //
                }

            }
            if (filterBean.getReturnOption2() != 0) { // S

                Date date1 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getReturnDate(),6,1);
                Date date2 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getReturnDate(),12,1);

                if(compareDateTime(date1,date2,dateX)){
                    // remains the flight in the clone
                    filteredbasedRet.add(flighBeans);
                    //continue;
                }

            }
            if (filterBean.getReturnOption3() != 0) {  // S
                Date date1 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getReturnDate(),12,1);
                Date date2 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getReturnDate(),18,1);

                if(compareDateTime(date1,date2,dateX)){
                    // remains the flight in the clone
                    filteredbasedRet.add(flighBeans);
                    //continue;
                }

            }
            if (filterBean.getReturnOption4() != 0) {  // S

                Date date1 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getReturnDate(),18,1);
                Date date2 =  DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getReturnDate(),24,1);

                if(compareDateTime(date1,date2,dateX)){
                    // remains the flight in the clone
                    filteredbasedRet.add(flighBeans);
                    // continue;
                }

            }
        }
        mObjReturnClone.clear();
        mObjReturnClone.addAll(filteredbasedRet);
    }

    private boolean compareDateTime(Date date1,Date date2,Date dateX){

        try {

            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(date1);

            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(date2);

            Calendar calendar3 = Calendar.getInstance();
            calendar3.setTime(dateX);

            Date x = calendar3.getTime();

            //((x.getTime() == calendar1.getTime() || (calendar2.getTime() == x.getTime()))

            long xdate = x.getTime();
            long xCAl1 = calendar1.getTime().getTime();


            if ( ( (x.getTime() == calendar1.getTime().getTime() ) || (x.getTime() == calendar1.getTime().getTime())) || (x.after(calendar1.getTime()) && x.before(calendar2.getTime()))) {
                //checkes whether the current time is between 14:49:00 and 20:11:13.
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    // FOR ROUND RETURN
    private ArrayList<FlightOnWardDetailBean> filterBasedRouRetOnOnwardStops(ArrayList<FlightOnWardDetailBean> onwrdClone) {//

        ArrayList<FlightOnWardDetailBean> al = new ArrayList<>();
        al.addAll(onwrdClone);

        onwrdClone.clear();
        if(filterBean.getStopZero() != 0 || filterBean.getStopOne() != 0 || filterBean.getStopTwoMore() != 0){

            for (int i = 0; i <al.size() ; i++) {

                FlightOnWardDetailBean flightDetailBean =  al.get(i);

                int stops = flightDetailBean.getFlightOnwardTotalStops();

                ArrayList<InnerFlightDetailsBeans> flight  = flightDetailBean.getFlight();

                if(flight.size() > 0) { //3

                    if (filterBean.getStopZero() != 0) { // S

                        if(stops == 0)
                        {  // Add flight to clone
                            onwrdClone.add(flightDetailBean);
                            continue;
                        }
                    }
                    if (filterBean.getStopOne() != 0) { // S
                        if(stops == 1)
                        {  // Add flight to clone
                            onwrdClone.add(flightDetailBean);
                            continue;
                        }
                    }
                    if (filterBean.getStopTwoMore() != 0) { // S
                        if(stops > 1)
                        {  // Add flight to clone
                            onwrdClone.add(flightDetailBean);
                            continue;
                        }
                    }
                }
            }
        }

        if((onwrdClone.size() > 0) && (filterBean.getStopZero() != 0 || filterBean.getStopOne() != 0 || filterBean.getStopTwoMore() != 0)){

        }else {
            if ((filterBean.getStopZero() != 0 || filterBean.getStopOne() != 0 || filterBean.getStopTwoMore() != 0)) {

            }else{
                mObjOnwrdClone = al;//roundTrip.rouRetOnwardFlight;//(ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightLCC().clone();
            }
        }

        if(filterBean.getOnwardOption1() != 0 || filterBean.getOnwardOption2() != 0 || filterBean.getOnwardOption3() != 0 || filterBean.getOnwardOption4() != 0){

            if(mObjOnwrdClone.size() > 0) {
                // filter the clone data on behalf of Onwards time
                filterBasedOnOnwardsTime();
            }
        }

        if(mObjOnwrdClone.size() > 0) {
            if (filterBean.getIsChange() != 0) {
                // filter the clone data on behalf of fare Price
                AppLogger.enable();
                AppLogger.e("MIN Avl=> ", "" + filterBean.getMinPrice());
                AppLogger.e("MAAX Avl=> ", "" + filterBean.getMaxPrice());
                onwrdClone = filterBasedOnPrice(mObjOnwrdClone);
            }
        }


        //filterBean = null;

        AppLogger.enable();
        AppLogger.e("Flight SIZE =>", "" + onwrdClone.size());

        // Show alert if filtered list is zero and provide new clone with all data
        if(onwrdClone.size() == 0){
           // onwrdClone = al;//roundTrip.rouRetOnwardFlight;//(ArrayList<FlightOnWardDetailBean>)AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightLCC().clone();
           // new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.FLIGHT_FILTER_SEARCH_ERROR_TEXT), mainActivity.getResources().getString(R.string.FLIGHT_SEARCH_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE, null).generateAlert();
        }

        return onwrdClone;
    }

    private ArrayList<FlightOnWardDetailBean> filterBasedRouRetOnReturnStops(ArrayList<FlightOnWardDetailBean> returnClone) {

        ArrayList<FlightOnWardDetailBean> al = new ArrayList<>();
        al.addAll(returnClone);

        returnClone.clear();
        if(filterBean.getStopZero() != 0 || filterBean.getStopOne() != 0 || filterBean.getStopTwoMore() != 0){

            for (int i = 0; i <al.size() ; i++) {

                FlightOnWardDetailBean flightDetailBean =  al.get(i);

                ArrayList<InnerFlightDetailsBeans> flight  = flightDetailBean.getFlight();
                int stops = flightDetailBean.getFlightOnwardTotalStops();

                if(flight.size()>0) { //3

                    if (filterBean.getStopZero() != 0) { // S

                        if(stops == 0)
                        {  // Add flight to clone
                            returnClone.add(flightDetailBean);
                            continue;
                        }
                    }
                    if (filterBean.getStopOne() != 0) { // S
                        if(stops == 1)
                        {  // Add flight to clone
                            returnClone.add(flightDetailBean);
                            continue;
                        }
                    }
                    if (filterBean.getStopTwoMore() != 0) { // S
                        if(stops > 1)
                        {  // Add flight to clone
                            returnClone.add(flightDetailBean);
                            continue;
                        }
                    }
                }
            }
        }


        if((returnClone.size() > 0) && (filterBean.getStopZero() != 0 || filterBean.getStopOne() != 0 || filterBean.getStopTwoMore() != 0)){

        }else {
            if ((filterBean.getStopZero() != 0 || filterBean.getStopOne() != 0 || filterBean.getStopTwoMore() != 0)) {

            }else{
                mObjReturnClone = al;//roundTrip.rouRetReturnFlight;//(ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getRouRetalAllFlightDetails().clone();
            }
        }


        if(filterBean.getReturnOption1() != 0 || filterBean.getReturnOption2() != 0 || filterBean.getReturnOption3() != 0 || filterBean.getReturnOption4() != 0){

            if(mObjReturnClone.size() > 0) {
                // filter the clone data on behalf of return time
                filterBasedOnReturnTime();
            }
        }

        if(mObjReturnClone.size() > 0) {
            if (filterBean.getIsChange() != 0) {
                // filter the clone data on behalf of fare Price
                AppLogger.enable();
                AppLogger.e("MIN Avl=> ", "" + filterBean.getMinPrice());
                AppLogger.e("MAAX Avl=> ", "" + filterBean.getMaxPrice());
                returnClone = filterBasedOnPrice(mObjReturnClone);
            }
        }

        AppLogger.enable();
        AppLogger.e("Flight SIZE =>", "" + returnClone.size());

        // Show alert if filtered list is zero and provide new clone with all data
        if(returnClone.size() == 0){
           // returnClone = al;//roundTrip.rouRetReturnFlight;//(ArrayList<FlightOnWardDetailBean>)AirDataHolder.getListHolder().getList().get(0).getRouRetalAllFlightDetails().clone();
           // new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.FLIGHT_FILTER_SEARCH_ERROR_TEXT), mainActivity.getResources().getString(R.string.FLIGHT_SEARCH_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE, null).generateAlert();
        }
        return returnClone;
    }




}