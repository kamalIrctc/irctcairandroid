package com.irctc.air.util.states;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by Rajnikant Kumar on 9/21/2018.
 */

public class States {
    public static ArrayList<String> getList(Context context){
       ArrayList<String> arrayList=new ArrayList<>();
       String states= "--Select State--," +
                "Andhra Pradesh," +
               "Arunachal Pradesh," +
               "Assam," +
               "Bihar," +
               "Chhattisgarh," +
               "Chandigarh," +
               "Dadra and Nagar Haveli," +
               "Daman and Diu," +
               "Delhi," +
               "Goa," +
               "Gujarat," +
               "Haryana," +
               "Himachal Pradesh," +
               "Jammu and Kashmir," +
               "Jharkhand," +
               "Karnataka," +
               "Kerala," +
               "Madhya Pradesh," +
               "Maharashtra," +
               "Manipur," +
               "Meghalaya," +
               "Mizoram," +
               "Nagaland," +
               "Orissa," +
               "Punjab," +
               "Pondicherry," +
               "Rajasthan," +
               "Sikkim," +
               "Tamil Nadu," +
               "Tripura," +
               "Uttar Pradesh," +
               "Uttarakhand," +
               "West Bengal";
        String[] list=states.split(",");
        for(int i=0;i<list.length;i++){
        arrayList.add(list[i]);
        }
        return arrayList;
    }
}
