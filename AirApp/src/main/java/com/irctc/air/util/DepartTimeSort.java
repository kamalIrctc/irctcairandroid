package com.irctc.air.util;

import com.irctc.air.model.FlightOnWardDetailBean;

import java.util.Comparator;

/**
 * Created by tourism on 4/13/2016.
 */
public class DepartTimeSort implements Comparator<FlightOnWardDetailBean>{

    @Override
    public int compare(FlightOnWardDetailBean obj1, FlightOnWardDetailBean obj2) {

        double departTimeOne = Double.parseDouble(DateUtility.getTimeFromCal(obj1.getFlightOnwDepartureTime()).replace(":", "."));
        double departTimeTwo = Double.parseDouble(DateUtility.getTimeFromCal(obj2.getFlightOnwDepartureTime()).replace(":", "."));

//        double departTimeOne = Double.parseDouble(obj1.getFlight().get(0).getFlightDepartureTime().substring(0, 5).replace(":", "."));
//        double departTimeTwo = Double.parseDouble(obj2.getFlight().get(0).getFlightDepartureTime().substring(0, 5).replace(":", "."));

        if (departTimeOne > departTimeTwo) {
            return 1;
        } else if (departTimeOne < departTimeTwo) {
            return -1;
        } else {
            return 0;
        }
    }

}
