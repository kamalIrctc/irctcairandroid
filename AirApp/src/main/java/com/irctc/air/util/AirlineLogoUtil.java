package com.irctc.air.util;

import com.irctc.air.R;

import java.util.HashMap;

/**
 * Created by tourism on 6/1/2016.
 */
public class AirlineLogoUtil {

    public static HashMap<String,Integer> map = new HashMap<>();

    /*********************************************
    * HERE WE GET THE AIRLINE LOGO ICON
    **********************************************/
    public static Integer getAirlineLogo(String airlineCode){

        if(map.containsKey(airlineCode)){
            return map.get(airlineCode);
        }else{
            return R.drawable.onward_icon;
        }
    }

   /* public  void setAirlineIcon(){

        map.put("0S", R.drawable.spicejet_0s);
        map.put("G8", R.drawable.goair_g8);
       // map.put("IT", R.drawable.airasia);
        map.put("S2", R.drawable.jet_lite_s2);

        map.put("AI", R.drawable.airindia_ai);
        map.put("9W", R.drawable.jetair_9w);
        map.put("6E", R.drawable.indigo_6e);

        map.put("G9", R.drawable.airarebia_g9);
        map.put("MH", R.drawable.malaysia_air_mh);
      //  map.put("9I", R.drawable.airasia);

        map.put("CX", R.drawable.cathay_pacific_cx);

        map.put("SQ", R.drawable.singaporeairlines_sq);
        map.put("EK", R.drawable.emirates_ek);

        map.put("PR", R.drawable.philippineairlines_pr);
        map.put("CZ", R.drawable.china_southern_cz);
       // map.put("KA", R.drawable.airasia);
        map.put("SV", R.drawable.saudi_arabian_sv);
        map.put("SU", R.drawable.aeroflot_su);
        map.put("KU", R.drawable.kuwait_airways_ku);
        map.put("RJ", R.drawable.royal_jordanian_rj);
        map.put("AY", R.drawable.finnair_ay);
        map.put("TK", R.drawable.turkish_airlines_tk);
        map.put("WY", R.drawable.oman_wy);
        map.put("UL", R.drawable.sri_lankan_airlines_ul);
        map.put("LH", R.drawable.lufthansa_lh);
        map.put("LX", R.drawable.swiss_intl_air_lx);
        map.put("OS", R.drawable.austrian_airlines_os);
        map.put("AF", R.drawable.air_france_af);
        map.put("KL", R.drawable.klm_kl);
        map.put("ET", R.drawable.ethiopian_air_et);
        map.put("GF", R.drawable.gulf_air_gf);
        map.put("EY", R.drawable.etihad_airways_ey);
        map.put("BA", R.drawable.british_airways_ba);
        map.put("VS", R.drawable.virgin_atlantic_vs);
        map.put("SN", R.drawable.brussels_airlines_sn);
     //   map.put("BD", R.drawable.airasia);
        map.put("QK", R.drawable.air_canada_qk);
        map.put("UA", R.drawable.united_ua);
    //    map.put("CO", R.drawable.airasia);
        map.put("CA", R.drawable.air_china_ca);
      //  map.put("EG", R.drawable.e);
        map.put("JL", R.drawable.japan_airlines_jl);
        map.put("QR", R.drawable.qatar_qr);
        map.put("DL", R.drawable.delta_dl);
        map.put("MQ", R.drawable.american_airlines_mq);
        map.put("XM", R.drawable.alitalia_xm);
       // map.put("US", R.drawable.airasia);
        map.put("SA", R.drawable.south_african_sa);
      //  map.put("EI", R.drawable.airasia);
        map.put("FI", R.drawable.icelandair_fi);
        map.put("AB", R.drawable.air_berlin_ab);
      //  map.put("EA", R.drawable.airasia);
      //  map.put("GE", R.drawable.airasia);
      //  map.put("IB", R.drawable.airasia);
       // map.put("MU", R.drawable.china_airlines_mu);
        map.put("MU", R.drawable.china_eastern_airlines_mu);
        map.put("GA", R.drawable.garuda_indonesia_ga);
        map.put("NH", R.drawable.nippon_airways_nh);
        map.put("KE", R.drawable.korean_air_lines_ke);
      //  map.put("LO", R.drawable.airasia);
      //  map.put("TP", R.drawable.airasia);
      //  map.put("MS", R.drawable.airasia);
      //  map.put("1K", R.drawable.airasia);
      //  map.put("MB", R.drawable.airasia);
      //  map.put("7H", R.drawable.airasia);
      //  map.put("TZ", R.drawable.airasia);
      //  map.put("2D", R.drawable.airasia);
      //  map.put("SU", R.drawable.airasia);
        map.put("AK", R.drawable.airasia_ak);
        map.put("UK", R.drawable.vistara_uk);
        map.put("OD", R.drawable.malindo_air_od);
        map.put("RA", R.drawable.royalnepal_air_ra);
        map.put("MJ", R.drawable.mihin_lanka_mj);
        map.put("SC", R.drawable.shandong_air_sc);
     //   map.put("I5", R.drawable.airasia);
     //   map.put("D7", R.drawable.airasia);
     //   map.put("AT", R.drawable.airasia);
     //   map.put("key", R.drawable.airasia);
        map.put("TG", R.drawable.thai_airways_tg);
        map.put("OZ", R.drawable.asiana_oz);

        /// new Add
         map.put("AC", R.drawable.air_canada_ac);
         map.put("QF", R.drawable.qantas_qf);
         map.put("PS", R.drawable.ukraine_ps);
         map.put("ZH", R.drawable.shenzhen_ps);
         map.put("MD", R.drawable.madagascare_md);
         map.put("ZI", R.drawable.aigle_azur_zi);
         map.put("ZI", R.drawable.aigle_azur_zi);
         map.put("JP", R.drawable.adria_jp);
         map.put("NX", R.drawable.airmacau_nx);
         map.put("AZ", R.drawable.alitalia_az);
         map.put("AD", R.drawable.azul_ad);
         map.put("PG", R.drawable.bangkok_airways_pg);
         map.put("BG", R.drawable.biman_bg);
         map.put("BR", R.drawable.eva_air_br);
         map.put("FJ", R.drawable.fiji_fz);
         map.put("HR", R.drawable.hahn_air_hr);
         map.put("HU", R.drawable.hainan_airlines_hu);
         map.put("KQ", R.drawable.kenya_airways_kq);
         map.put("PC", R.drawable.pegasus_pc);
         map.put("SK", R.drawable.sas_sk);
         map.put("LO", R.drawable.polishairline_lo);
         map.put("WN", R.drawable.southwest_airlines_wn);
         map.put("VN", R.drawable.vietnam_airline_vn);
         map.put("SZ", R.drawable.somom_air_sz);
         map.put("MK", R.drawable.air_mauritius_mk);





    }*/

}
