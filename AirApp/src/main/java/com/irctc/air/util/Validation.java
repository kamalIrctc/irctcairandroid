package com.irctc.air.util;

import java.util.Calendar;
import java.util.GregorianCalendar;


public class Validation
{

	public static Validation getInstance ()
	{
		Validation mObjVlidation = new Validation();
		return mObjVlidation;
	}


	/*******************************************************************************************************
	 * ADD PASS VALIDATION
	 *******************************************************************************************************/
	public String getFirstNameValidation ( String name )
	{
		String message = "FAILURE";
		String firstLetterAlphabet = "^[a-zA-Z\\s\\-]*$";

		if (!(name.equalsIgnoreCase(""))) {

			if (name.length() > 1) {

				if (name.matches(firstLetterAlphabet)) {
					if(!name.contains(" ")) {
						message = "SUCCESS";
					}else{
						message = "Space is not allowed in first name";
					}
				} else {
					message = "Only alphabets are allowed in first name.";
				}
			} else {
				message = "First name length cannot be less than 2 characters.";
			}
		} else {
			message = "Please enter first name.";
		}

		return message;
	}

	public String getEmpCodeValidation ( String name )
	{
		String message = "FAILURE";


		if (!(name.equalsIgnoreCase(""))) {

			message = "SUCCESS";

		} else {
			message = "Please enter Employee code.";
		}

		return message;
	}



	public String getLastNameValidation ( String name )
	{
		String message = "FAILURE";
		String firstLetterAlphabet = "^[a-zA-Z\\s\\-]*$";

		if (!(name.equalsIgnoreCase(""))) {

			if (name.length() > 1) {

				if (name.matches(firstLetterAlphabet)) {

					if(!name.contains(" ")) {
						message = "SUCCESS";

					}else{
						message = "Space is not allowed in last name";
					}
				} else {
					message = "Only alphabets are allowed in last name.";
				}
			} else {
				message = "Last name length cannot be less than 2 characters.";
			}
		} else {
			message = "Please enter last name.";
		}

		return message;
	}





	public String getUserIdValidation( String name )
	{
		String message = "FAILURE";
		// String alphaNumericRegx = "^[a-zA-Z0-9_]+$";
		// String pattern = ".*__.*";
		// String firstLetterAlphabet = "^[a-zA-Z].*";

		if (!(name.equalsIgnoreCase(""))) {

			if (name.length() <= 10) {

				message = "SUCCESS";

			} else {
				message = "User id length cannot be more than 10 characters.";
			}
		} else {
			message = "Please enter user id.";
		}

		return message;
	}
	public String getHouseNumberValidation ( String name )
	{
		String message = "FAILURE";
		// String alphaNumericRegx = "^[a-zA-Z0-9_]+$";
		// String pattern = ".*__.*";
		// String firstLetterAlphabet = "^[a-zA-Z].*";

		if (!(name.equalsIgnoreCase(""))) {

			if (name.matches("^[a-zA-Z]+$")) {
			message = "SUCCESS";

			} else {
				message = "Please enter valid detail, special case are not allowed.";
			}
		} else {
			message = "Please enter username.";
		}

		return message;
	}













	/*******************************************************************************************************
	 *  LOGIN VALIDATION
	 *******************************************************************************************************/
	public String getEmailValidation ( String email )
	{
		String message = "FAILURE";
		if (!(email.equalsIgnoreCase(""))) {
			if (email.matches("^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$")) {
				message = "SUCCESS";
			} else {
				message = "Please enter valid email Id.";
			}
		} else {
			message = "Please enter email id.";
		}
		return message;
	}


	// DONE
	public String getUserNameValidation ( String name )
	{
		String message = "FAILURE";
		// String alphaNumericRegx = "^[a-zA-Z0-9_]+$";
		// String pattern = ".*__.*";
		// String firstLetterAlphabet = "^[a-zA-Z].*";

		if (!(name.equalsIgnoreCase(""))) {

				if (name.length() <= 30) {

						message = "SUCCESS";

				} else {
					message = "User name length cannot be more than 30 characters.";
				}
		} else {
			message = "Please enter username.";
		}

		return message;
	}


	public String getUserPwdValidation ( String name )
	{
		String message = "FAILURE";

		if (!(name.trim().equalsIgnoreCase(""))) {

				message = "SUCCESS";

		} else {
			message = "Please enter password.";
		}

		return message;
	}

	public String getUserPassportValidation ( String name )
	{
		String message = "FAILURE";

		if (!(name.trim().equalsIgnoreCase(""))) {

			message = "SUCCESS";

		} else {
			message = "Please enter valid passport number.";
		}

		return message;
	}


	// DONE
	public String getMobileNoValidation ( String name )
	{
		String message = "FAILURE";
		if (!(name.equalsIgnoreCase(""))) {
			char lStrChar = '0';
			if ((name.indexOf(lStrChar) != 0)) {
				if (name.length() == 10) {
					message = "SUCCESS";
				} else {
					message = "Mobile number length cannot be less/more than 10 numbers.";
				}
			} else {
				message = "First character of mobile number cannot be zero.";
			}
		} else {
			message = "Please enter mobile number.";
		}
		return message;
	}


	// DONE
	public String getCityValidation ( String name )
	{
		String message = "FAILURE";
		if (!(name.equalsIgnoreCase(""))) {
			if (name.length() >= 1) {
				if (name.length() <= 18) {
					if (name.matches("^[a-zA-Z\\s]*$")) {
						message = "SUCCESS";

					} else {
						message = "Please enter valid city.";
					}
				} else {
					message = "City name cannot be more than 18 characters.";
				}
			} else {
				message = "City name cannot be less than 1 characters.";
			}
		} else {
			message = "Please enter city.";
		}
		return message;
	}


	// DONE
	public String getStateValidation ( String name )
	{
		String message = "FAILURE";
		if (!(name.equalsIgnoreCase(""))) {
			if (name.matches("^[a-zA-Z\\s]*$")) {
				message = "SUCCESS";

			} else {
				message = "Please enter valid state.";
			}
		} else {
			message = "Please enter state.";
		}
		return message;
	}


	// DONE
	public String getCountryValidation ( String name )
	{
		String message = "FAILURE";
		if (!(name.equalsIgnoreCase(""))) {
			if (name.matches("^[a-zA-Z\\s]*$")) {
				message = "SUCCESS";

			} else {
				message = "Please enter valid country.";
			}
		} else {
			message = "Please enter country.";
		}
		return message;
	}

	public static boolean isValidPassportNumber(String name)
	{
		boolean message = false;
		if (!(name.equalsIgnoreCase(""))) {
			if (name.matches("^[\\pL\\pN]+$")) {
				message = true;

			} else {
				message = false;
			}
		} else {
			message = false;
		}
		return message;
	}



	// DONE
	public String getPinCodeValidation ( String name )
	{
		String message = "FAILURE";
		if (!(name.equalsIgnoreCase(""))) {
			if (name.length() == 6) {
				message = "SUCCESS";

			} else {
				message = "Pin Code length cannot be less/more than 6 numbers.";
			}
		} else {
			message = "Please enter valid Pincode.";
		}
		return message;
	}


	// DONE
	public String getAddressValidation ( String mname )
	{

		String name = mname.toString().trim();
		String message = "FAILURE";

		if (!(name.equalsIgnoreCase(""))) {
			if (name.length() >= 1) {
				if (name.length() < 90) {
					if ((name.matches("([a-zA-Z0-9_\\-/, #])*"))) {
						message = "SUCCESS";
					} else {
						message = "Please enter valid address.";
					}
				} else {
					message = "Address length cannot be more than 90 characters.";
				}
			} else {
				message = "Please enter minimum 1 characters.";
			}
		} else {
			message = "Please enter address .";
		}
		return message;
	}


	public boolean getDOBValidation ( int year, int month, int day )
	{
		boolean valid = false;
		Calendar userAge = new GregorianCalendar(year, month, day);
		Calendar minAdultAge = new GregorianCalendar();
		minAdultAge.add(Calendar.YEAR, -18);

		Calendar maxAdultAge = new GregorianCalendar();
		maxAdultAge.add(Calendar.YEAR, +98);

		if ((minAdultAge.before(userAge))) {

			if ((maxAdultAge.after(userAge))) {
				valid = true;
			}
		}

		return valid;
	}



	// DONE
	public String getAddPassAgeValidation ( String name )
	{
		String message = "FAILURE";


		if (!(name.equalsIgnoreCase(""))) {
			if (name.matches("^[0-9]*$")) {
				int age = Integer.parseInt(name);
				if (age <= 125) {
					message = "SUCCESS";

				} else {
					message = "Age cannot be greater than 125.";
				}

			} else {
				message = "Please enter valid age.";
			}

		} else {
			message = "Please enter age.";
		}
		return message;
	}


	// DONE
	public String getAddChildPassAgeValidation ( String name )
	{
		String message = "FAILURE";

		if (!(name.equalsIgnoreCase(""))) {
			if (name.matches("^[0-9]*$")) {
				int age = Integer.parseInt(name);
				if (age <= 4) {
					message = "SUCCESS";

				} else {
					message = "Child passenger age cannot be greater than 4.";
				}

			} else {
				message = "Please enter valid Child passenger age.";
			}

		} else {
			message = "Please enter Child passenger age.";
		}
		return message;
	}


}