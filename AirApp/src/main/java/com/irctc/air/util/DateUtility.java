package com.irctc.air.util;

import android.text.TextUtils;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;


/**
 * Created by vivek on 4/20/2016.
 */
public class DateUtility {

    public static String lStrGMTDateFormater = "EEE MMM d HH:mm:ss z yyyy";
    public static String lStrISTDateFormater = "EEE MMM d HH:mm:ss 'IST' yyyy";
    public static DateFormat dateFormat = new SimpleDateFormat("dd MMM");

    public static String getTodayDate(){
        Date date = new Date();
        return dateFormat.format(date);
    }
    public static String getDateAndMonthFromCal(String datee) {
        // Wed Apr 20 15:58:48 GMT+05:30 2016
//        DateFormat readFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
//        Date date = null;
//
//        try {
//            date = readFormat.parse(datee);
//
//        } catch (java.text.ParseException e) {
//            e.printStackTrace();
//        }


        ////////////////////
        String[] date = datee.split(" ");
        return date[2] + " " + date[1];
    }

    public static String convertDateFormat(String currentDate,
                                           String currentDateFormatString, String reqDateFormat) {
        SimpleDateFormat currentDateFormat = new SimpleDateFormat(
                currentDateFormatString);
        SimpleDateFormat format = new SimpleDateFormat(reqDateFormat);
        try {
            TimeZone istTimeZone = TimeZone.getTimeZone("IST");
            currentDateFormat.setTimeZone(istTimeZone);
            Date d = currentDateFormat.parse(currentDate);
            return format.format(d);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return currentDate;
    }


    public static String getDateDayFromCal(String datee) {
        // Wed Apr 20 15:58:48 GMT+05:30 2016
//        DateFormat readFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
//        Date date = null;
//
//        try {
//            date = readFormat.parse(datee);
//
//        } catch (java.text.ParseException e) {
//            e.printStackTrace();
//        }

        ////////////////////
        String[] date = datee.split(" ");
        return date[2];
    }

    public static String getMonthFromCal(String datee) {
        // Wed Apr 20 15:58:48 GMT+05:30 2016
//        DateFormat readFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
//        Date date = null;
//
//        try {
//            date = readFormat.parse(datee);
//
//        } catch (java.text.ParseException e) {
//            e.printStackTrace();
//        }


        ////////////////////
        String[] date = datee.split(" ");
        return date[1] ;
    }

    public static String getDateNameFromCal(String datee) {

//        DateFormat readFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
//        Date date = null;
//
//        try {
//            date = readFormat.parse(datee);
//
//        } catch (java.text.ParseException e) {
//            e.printStackTrace();
//        }
//
//
//
//        return date.getDay()+"";
        String[] date = datee.split(" ");
        return date[0];

    }

    public static String getYearFromCal(String datee) {

//        DateFormat readFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
//        Date date = null;
//
//        try {
//            date = readFormat.parse(datee);
//
//        } catch (java.text.ParseException e) {
//            e.printStackTrace();
//        }
//
//
//
//        return date.getYear()+"";

        String[] date = datee.split(" ");
        return date[5];
    }

    public static String getTimeFromCal(String datee) {//Wed Jun 27 20:50:00 GMT+05:30 2007

//        DateFormat readFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
//        Date date = null;
//
//        try {
//            date = readFormat.parse(datee);
//
//        } catch (java.text.ParseException e) {
//            e.printStackTrace();
//        }
//
//
//
//        return date.getYear()+"";
        String time= null;
        try {
        String[] date = datee.split(" ");
        time =  date[3].substring(0, 5);


        }catch (Exception e){
        e.printStackTrace();
        }
        return time;
    }

    public static String formatDate(String startDateString) { // 27-06-2007T20:50

        startDateString = containsIndianTimeZone(startDateString);

        startDateString = startDateString.replace('T', ' ');

        // This object can interpret strings representing dates in the format MM/dd/yyyy
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");

        // Convert from String to Date
        Date startDate = null;
        try {
            startDate = df.parse(startDateString); //"27-06-2007 20:50";
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // String date = df.format(startDate);

        return startDate.toString(); // Wed Jun 27 20:50:00 GMT+05:30 2007

    }



    public static Date onlyDate(String lStrDate){

        DateFormat readFormat = new SimpleDateFormat("dd-MMM-yyyy");
        lStrDate = containsIndianTimeZone(lStrDate);
        Date date = null;
        try {
            date = readFormat.parse(lStrDate);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Date convertDate(String lStrDate){// 21/10/2016

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

        Date startDate = null;
        try {
            startDate = df.parse(lStrDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return startDate;
    }

    public static String convertDateForLowFareCalendar(String lStrDate){// 21/10/2016

        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat outputDF = new SimpleDateFormat("dd-MM-yyyy");

        Date startDate = null;
        try {
            startDate = df.parse(lStrDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formattedDate = outputDF.format(startDate);
        return formattedDate;
    }

    public static Date convertStringInDate(String lStrDate){

        TimeZone timeZone = TimeZone.getDefault();
        TimeZone tZ = TimeZone.getTimeZone("Asia/Calcutta");
        tZ.getID();
        tZ.getRawOffset();
        tZ.getDSTSavings();

        Date date = null;
        Date dateInAmerica = null;

        try {

        SimpleDateFormat formatter = new SimpleDateFormat(lStrGMTDateFormater);

        String dateInString = lStrDate;
        date = formatter.parse(dateInString);
        TimeZone tz = TimeZone.getDefault();

        // From TimeZone Asia/Singapore
        System.out.println("TimeZone : " + tz.getID() + " - " + tz.getDisplayName());
        System.out.println("TimeZone : " + tz);
        System.out.println("Date : " + formatter.format(date));

        // To TimeZone America/New_York
        SimpleDateFormat sdfAmerica = new SimpleDateFormat(lStrGMTDateFormater);
        TimeZone tzInAmerica = TimeZone.getTimeZone("Asia/Calcutta");
        sdfAmerica.setTimeZone(tzInAmerica);

        String sDateInAmerica = sdfAmerica.format(date); // Convert to String first


            dateInAmerica = formatter.parse(sDateInAmerica);

        System.out.println("\nTimeZone : " + tzInAmerica.getID() +
                " - " + tzInAmerica.getDisplayName());
        System.out.println("TimeZone : " + tzInAmerica);
        System.out.println("Date (String) : " + sDateInAmerica);
        System.out.println("Date (Object) : " + formatter.format(dateInAmerica));

        } catch (ParseException e) {
            e.printStackTrace();
        }

//        SimpleDateFormat readFormat = new SimpleDateFormat(lStrGMTDateFormater);
//
//        Date date = null;
//        try {
//            date = readFormat.parse(lStrDate);
//
//            String str = readFormat.format(date);
//
//            date = readFormat.parse(str);
//
//        } catch (java.text.ParseException e) {
//            e.printStackTrace();
//        }
        return date;
    }

    public static String getTimeDifferenceInTwoDate(String date1, String date2) {

        DateFormat readGMTFormat = new SimpleDateFormat(lStrGMTDateFormater);
        DateFormat readISTFormat = new SimpleDateFormat(lStrISTDateFormater);

        date1 = containsIndianTimeZone(date1);
        date2 = containsIndianTimeZone(date2);


        Date newerDate = null;
        Date olderDate = null;

        try {

            if(!date1.contains("GMT")) {
                newerDate = readISTFormat.parse(date1);
            }else{
                newerDate = readGMTFormat.parse(date1);
            }

            if(!date2.contains("GMT")) {
                olderDate = readISTFormat.parse(date2);
            }else{
                olderDate = readGMTFormat.parse(date2);
            }
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        long diff = newerDate.getTime() - olderDate.getTime();

        long minutes = (diff / 1000) / 60;

        return (minutes/60)+"h "+(minutes%60)+"m";
    }

    public static Date getBeforeAfterTime(String serverDate, int addValue, int CheckAdd) {

        serverDate = containsIndianTimeZone(serverDate);

        if (CheckAdd == 1) {

            String dateArr[] = serverDate.split(" ");
            dateArr[3] = "00:00:00";
            serverDate = dateArr[0] + " " + dateArr[1] + " " + dateArr[2] + " " + dateArr[3] + " " + dateArr[4] + " " + dateArr[5];
        }
        else{

//
//            String dateArr[] = serverDate.split(" ");
//            dateArr[3] = "07:50:00";
//            serverDate = dateArr[0] + " " + dateArr[1] + " " + dateArr[2] + " " + dateArr[3] + " " + dateArr[4] + " " + dateArr[5];
        }

        DateFormat readFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
        Date date = null;
        Date lObjAdd30Min = null;
        try {
            date = readFormat.parse(serverDate);

            Calendar lObjCal = new GregorianCalendar();
            lObjCal.setTime(date);
            lObjCal.add(Calendar.HOUR, addValue);
            lObjAdd30Min = lObjCal.getTime();

        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        return lObjAdd30Min;
    }

    public static String getDayDateMonthYear(String datee){
        // Wed Apr 20 15:58:48 GMT+05:30 2016
//        DateFormat readFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
//        Date date = null;
//
//        try {
//            date = readFormat.parse(datee);
//
//        } catch (java.text.ParseException e) {
//            e.printStackTrace();
//        }


        ////////////////////
        String[] date = datee.split(" ");
        return date[0] +", "+date[2]+" "+date[1]+" "+date[5];
    }

    public static String getTime(String datee) {
        // Wed Apr 20 15:58:48 GMT+05:30 2016
//        DateFormat readFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
//        Date date = null;
//
//        try {
//            date = readFormat.parse(datee);
//
//        } catch (java.text.ParseException e) {
//            e.printStackTrace();
//        }


        ////////////////////
        String[] date = datee.split(" ");
        return date[3].substring(0,5);
    }


/*
*   Input string                            Pattern
    ------------------------------------    ----------------------------
    2001.07.04 AD at 12:08:56 PDT           yyyy.MM.dd G 'at' HH:mm:ss z
    Wed, Jul 4, '01                         EEE, MMM d, ''yy
    12:08 PM                                h:mm a
    12 o'clock PM, Pacific Daylight Time    hh 'o''clock' a, zzzz
    0:08 PM, PDT                            K:mm a, z
    02001.July.04 AD 12:08 PM               yyyyy.MMMM.dd GGG hh:mm aaa
    Wed, 4 Jul 2001 12:08:56 -0700          EEE, d MMM yyyy HH:mm:ss Z
    010704120856-0700                       yyMMddHHmmssZ
    2001-07-04T12:08:56.235-0700            yyyy-MM-dd'T'HH:mm:ss.SSSZ
    2001-07-04T12:08:56.235-07:00           yyyy-MM-dd'T'HH:mm:ss.SSSXXX
    2001-W27-3                              YYYY-'W'ww-u

 * */


    // Get month in integer
    public static String getMonthInInt(String pDate) {
        int monthInInt = 0;
        pDate = containsIndianTimeZone(pDate);
        try {

            if (pDate.contains("IST")) {
                DateFormat readFormat = new SimpleDateFormat(lStrISTDateFormater, Locale.US);
                Date date = readFormat.parse(pDate);

                monthInInt = date.getMonth();

//                Calendar lObjCal = new GregorianCalendar();
//                lObjCal.setTime(date);
//                monthInInt = lObjCal.get(Calendar.MONTH);

                monthInInt++;
            } else {
                DateFormat readFormat = new SimpleDateFormat(lStrGMTDateFormater, Locale.US);
                Date date = readFormat.parse(pDate);
                monthInInt = date.getMonth();

//                Calendar lObjCal = new GregorianCalendar();
//                lObjCal.setTime(date);
//                monthInInt = lObjCal.get(Calendar.MONTH);

                monthInInt++;
            }
        } catch (java.text.ParseException e) {

//            AppLogger.enable();
//            AppLogger.e("EXCEption occour Date ", e.getCause().toString());
        }

        String month = "";
        if (monthInInt == 10 || monthInInt == 11 || monthInInt == 12) {
            month = String.valueOf(monthInInt);
        } else {
            month = "0" + monthInInt;
        }

        return month;
    }


    public static String getSingleMonthWithZero(int monthInInt){
        String month = "";
        if (monthInInt == 10 || monthInInt == 11 || monthInInt == 12) {
            month = String.valueOf(monthInInt);
        } else {
            month = "0" + monthInInt;
        }
        return month;

    }

    public static String getSingleMonthName(int monthInInt) {
//        try {
////            Date date = new SimpleDateFormat("MMM", Locale.ENGLISH).parse("Feb");
////            Calendar cal = Calendar.getInstance();
////            cal.setTime(date);
////            int month = cal.get(Calendar.MONTH);
////            System.out.println(month == Calendar.FEBRUARY);
//
//
//
//        }catch (Exception e){
//
//        }
        return new DateFormatSymbols().getMonths()[monthInInt-1];
    }

    public static String getSingleDateWithZero(int monthInInt){
        String day = "";
        if (monthInInt == 1 || monthInInt == 2 || monthInInt == 3 || monthInInt == 4 || monthInInt == 5 || monthInInt == 6 || monthInInt == 7 || monthInInt == 8 || monthInInt == 9) {
            day = "0" + monthInInt;
        } else {
            day = String.valueOf(monthInInt);
        }
        return day;
    }



    public static String getDateFromBookingHistoryRes(String datee) {
        // 23-01-2016 16:45:00
        DateFormat readFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        datee = containsIndianTimeZone(datee);

        Date date = null;

        try {
            date = readFormat.parse(datee);

        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        return date.toString();
    }


    public static Date getDateInFormat(String date) {

        DateFormat readFormat;
        if(date.contains("GMT")) {
            //Wed May 25 00:00:00 GMT+05:30 2016
            readFormat = new SimpleDateFormat(lStrGMTDateFormater);
        }else{
            readFormat = new SimpleDateFormat(lStrISTDateFormater);
        }

        date = containsIndianTimeZone(date);
        Date date2 = null;

        try {
            date2 = readFormat.parse(date);

        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        return date2;

    }

    public static Date getDateInFor(String date) {

        DateFormat readFormat= new SimpleDateFormat("yyyy-MM-dd");
        Date date2 = null;
        try {
            date2 = readFormat.parse(date);

        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        return date2;

    }


//    public static Date getConfirmationDateInFormat(String date) {
//        // 08-05-2016 20:20:04
//        DateFormat readFormat = new SimpleDateFormat(lStrGMTDateFormater);
//        Date date2 = null;
//
//        try {
//            date2 = readFormat.parse(date);
//
//        } catch (java.text.ParseException e) {
//            e.printStackTrace();
//        }
//
//        return date2;
//
//    }

//
//    public static String getTimeDifferenceInTwoDate(String date1, String date2) {
//
//        DateFormat readFormat = new SimpleDateFormat(lStrGMTDateFormater);
//        Date newerDate = null;
//        Date olderDate = null;
//
//        try {
//            newerDate = readFormat.parse(date1);
//            olderDate = readFormat.parse(date2);
//
//        } catch (java.text.ParseException e) {
//            e.printStackTrace();
//        }
//
//        long diff = ((newerDate.getTime() - olderDate.getTime())/ (1000 * 60 * 60 * 24));
//
//        return String.valueOf(diff);
//}

        public static String getTimeDifferenceInTwoISTDate(String date1, String date2) {

            DateFormat readGMTFormat = new SimpleDateFormat(lStrGMTDateFormater);
            DateFormat readISTFormat = new SimpleDateFormat(lStrISTDateFormater);

            date1  = containsIndianTimeZone(date1);
            date2  = containsIndianTimeZone(date2);

            Date newerDate = null;
            Date olderDate = null;

            try {

                if(!date1.contains("GMT")) {
                    newerDate = readISTFormat.parse(date1);
                }else{
                    newerDate = readGMTFormat.parse(date1);
                }

                if(!date2.contains("GMT")) {
                    olderDate = readISTFormat.parse(date2);
                }else{
                    olderDate = readGMTFormat.parse(date2);
                }
            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }

            long diff = newerDate.getTime() - olderDate.getTime();

            long minutes = (diff / 1000) / 60;

            return (minutes/60)+"h "+(minutes%60)+"m";
    }


    public static String getTimeInDayMonthYear(String datee) {//Wed Jun 27 20:50:00 GMT+05:30 2007

//        DateFormat readFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
//        Date date = null;
//
//        try {
//            date = readFormat.parse(datee);
//
//        } catch (java.text.ParseException e) {
//            e.printStackTrace();
//        }
//
//
//
//        return date.getYear()+"";

        String[] date = datee.split(" ");
        return date[1] + " " + date[2] + ", " + date[5];
    }




    public static boolean isReturnDateGreaterThenDepDate(String returnDate, String departureDate){

        DateFormat readISTFormat = new SimpleDateFormat(lStrISTDateFormater);
        DateFormat readFormat = new SimpleDateFormat(lStrGMTDateFormater);

        returnDate = containsIndianTimeZone(returnDate);
        departureDate = containsIndianTimeZone(departureDate);

        Date retDate = null;
        Date depDate = null;

        try {
            if(!returnDate.contains("GMT")) {
                retDate = readISTFormat.parse(returnDate);
            }else {
                retDate = readFormat.parse(returnDate);
            }

            if(!departureDate.contains("GMT")){
                depDate = readISTFormat.parse(departureDate);
            }else{
                depDate = readFormat.parse(departureDate);
            }


        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        if(retDate.before(depDate))
        {
            return false;
        }else{
            return true;
        }

    }



    public static boolean isReturnDateSmallerThenDepDate(String depDatee, String retDatee){

        DateFormat readISTFormat = new SimpleDateFormat(lStrISTDateFormater);
        DateFormat readFormat = new SimpleDateFormat(lStrGMTDateFormater);

        depDatee = containsIndianTimeZone(depDatee);
        retDatee = containsIndianTimeZone(retDatee);


        Date retDate = null;
        Date depDate = null;

        try {
            if(!retDatee.contains("GMT")) {
                retDate = readISTFormat.parse(retDatee);
            }else{
                retDate = readFormat.parse(retDatee);
            }
            if(!depDatee.contains("GMT")) {
                depDate = readISTFormat.parse(depDatee);
            }else{
                depDate = readFormat.parse(depDatee);
            }

        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        if(retDate.before(depDate))
        {
            return true;
        }else{
            return false;
        }

    }

    public static String currentTime(long millSecondDate){

        DateFormat readFormat = new SimpleDateFormat(lStrGMTDateFormater);
        String convertDate = null;

        try {
            convertDate = readFormat.format(millSecondDate);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertDate;
    }




    public static Date getEndDate(String dateee, String value){
        Calendar lObjCal = new GregorianCalendar();
        dateee  = containsIndianTimeZone(dateee);
        try {


            if(value.equalsIgnoreCase("Infant")) {
                if (dateee.contains("IST")) {
                    DateFormat readFormat = new SimpleDateFormat(lStrISTDateFormater);
                    Date date = readFormat.parse(dateee);

                    lObjCal.setTime(date);
                   // lObjCal.add(Calendar.MONTH, -24);


                } else {
                    DateFormat readFormat = new SimpleDateFormat(lStrGMTDateFormater);
                    Date date = readFormat.parse(dateee);


                    lObjCal.setTime(date);
                    //lObjCal.add(Calendar.MONTH, -24);


                }
            }else if(value.equalsIgnoreCase("Child")) {
                if (dateee.contains("IST")) {
                    DateFormat readFormat = new SimpleDateFormat(lStrISTDateFormater);
                    Date date = readFormat.parse(dateee);

                    lObjCal.setTime(date);
                    //lObjCal.add(Calendar.YEAR, -2);


                } else {
                    DateFormat readFormat = new SimpleDateFormat(lStrGMTDateFormater);
                    Date date = readFormat.parse(dateee);


                    lObjCal.setTime(date);
                    //lObjCal.add(Calendar.YEAR, -2);


                }
            }else{
                if (dateee.contains("IST")) {
                    DateFormat readFormat = new SimpleDateFormat(lStrISTDateFormater);
                    Date date = readFormat.parse(dateee);

                    lObjCal.setTime(date);
                    //lObjCal.add(Calendar.YEAR, -12);


                } else {
                    DateFormat readFormat = new SimpleDateFormat(lStrGMTDateFormater);
                    Date date = readFormat.parse(dateee);


                    lObjCal.setTime(date);
                    //lObjCal.add(Calendar.YEAR, -12);


                }
            }
        } catch (java.text.ParseException e) {
        }

        // Fri Mar 16 02:13:28 EET 2012

        String arraydate[] = lObjCal.getTime().toString().split(" ");
        Date newDate = lObjCal.getTime();

        return newDate;
    }






    public static String getStarDate(String date){
        //Wed Jun 27 20:50:00 GMT+05:30 2007
        DateFormat readISTFormat = new SimpleDateFormat(lStrISTDateFormater);
        DateFormat readFormat = new SimpleDateFormat(lStrGMTDateFormater);
        Date retDate = null;

        try {
            if(!date.contains("GMT")) {
                retDate = readISTFormat.parse(date);
            }else{
                retDate = readFormat.parse(date);
            }


        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }


        String arraydate[] = retDate.toString().split(" ");

        return arraydate[5]+"-"+retDate.getMonth()+"-"+arraydate[2];
    }

    public static String getMonthAndDayAndYear(String date){
        //Wed Jun 27 20:50:00 GMT+05:30 2007
        DateFormat readISTFormat = new SimpleDateFormat(lStrISTDateFormater);
        DateFormat readFormat = new SimpleDateFormat(lStrGMTDateFormater);

        date  = containsIndianTimeZone(date);

        Date retDate = null;

        try {
            if(!date.contains("GMT")) {
                retDate = readISTFormat.parse(date);
            }else{
                retDate = readFormat.parse(date);
            }


        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }


        String arraydate[] = retDate.toString().split(" ");

        return arraydate[2]+"-"+arraydate[1]+"-"+arraydate[5];
    }





   public static Date getDate(String date){

            //Wed Jun 27 20:50:00 GMT+05:30 2007
            DateFormat readISTFormat = new SimpleDateFormat(lStrISTDateFormater);
            DateFormat readFormat = new SimpleDateFormat(lStrGMTDateFormater);

            date  = containsIndianTimeZone(date);

            Date retDate = null;

            try {
                if(!date.contains("GMT")) {
                    retDate = readISTFormat.parse(date);
                }else{
                    retDate = readFormat.parse(date);
                }


            } catch (java.text.ParseException e) {
                e.printStackTrace();
            }


            String arraydate[] = retDate.toString().split(" ");

            return retDate;

   }

   public static boolean compareDateTime(Date date2, Date dateX){

        try {

            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(date2);

            if(date2.before(dateX)){
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
   }

   public static int monthInInteger(String monthName){

        if(monthName.equalsIgnoreCase("Jan")){
            return 0;
        }else if(monthName.equalsIgnoreCase("Feb")){
            return 1;
        }else if(monthName.equalsIgnoreCase("Mar")){
            return 2;
        }else if(monthName.equalsIgnoreCase("Apr")){
            return 3;
        }else if(monthName.equalsIgnoreCase("May")){
            return 4;
        }else if(monthName.equalsIgnoreCase("Jun")){
            return 5;
        }else if(monthName.equalsIgnoreCase("Jul")){
            return 6;
        }else if(monthName.equalsIgnoreCase("Aug")){
            return 7;
        }else if(monthName.equalsIgnoreCase("Sep")){
            return 8;
        }else if(monthName.equalsIgnoreCase("Oct")){
            return 9;
        }else if(monthName.equalsIgnoreCase("Nov")){
            return 10;
        }else if(monthName.equalsIgnoreCase("Dec")){
            return 11;
        }

        return 0;
   }
   public static String containsIndianTimeZone(String timeDate){

        if(timeDate.contains("GMT")){

            if(!timeDate.contains("GMT+05:30")){

                String str[] = timeDate.split(" ");
                timeDate = str[0]+" "+str[1]+" "+str[2]+" "+str[3]+" GMT+05:30 "+str[5];
            }
        }
        return timeDate;
   }

    public static Date getAfterTime(String serverDate, int addValue, int CheckAdd) {

        serverDate = containsIndianTimeZone(serverDate);

        if (CheckAdd == 1) {

            String dateArr[] = serverDate.split(" ");
            dateArr[3] = "00:00:00";
            serverDate = dateArr[0] + " " + dateArr[1] + " " + dateArr[2] + " " + dateArr[3] + " " + dateArr[4] + " " + dateArr[5];
        }

        DateFormat readFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
        Date date = null;
        Date lObjAdd30Min = null;
        try {
            date = readFormat.parse(serverDate);

            Calendar lObjCal = new GregorianCalendar();
            lObjCal.setTime(date);
            lObjCal.add(Calendar.YEAR, addValue);
            lObjAdd30Min = lObjCal.getTime();

        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        return lObjAdd30Min;
    }

    public static Calendar getCalender(String time) {
        Calendar calendar = Calendar.getInstance();
        if (!TextUtils.isEmpty(time)) {
            SimpleDateFormat format = new SimpleDateFormat("hh:mm aa");
            try {
                Date d = format.parse(time);
                Calendar c = Calendar.getInstance();
                c.setTime(d);
                calendar.set(Calendar.HOUR_OF_DAY, c.get(Calendar.HOUR_OF_DAY));
                calendar.set(Calendar.MINUTE, c.get(Calendar.MINUTE));
                calendar.set(Calendar.SECOND, 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return calendar;
    }
}
