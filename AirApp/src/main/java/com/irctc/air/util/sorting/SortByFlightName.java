package com.irctc.air.util.sorting;

import com.irctc.air.model.FlightOnWardDetailBean;
import com.irctc.air.model.search_result_one_way.Flights;

import java.util.Comparator;

/**
 * Created by tourism on 4/13/2016.
 */
public class SortByFlightName implements Comparator<Flights>{

    @Override
    public int compare(Flights obj1, Flights obj2) {

        String f1 = obj1.getCarrierName();
        String f2 = obj2.getCarrierName();

        return f1.compareToIgnoreCase(f2);
    }
}
