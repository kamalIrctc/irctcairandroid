package com.irctc.air.util.sorting;

import com.irctc.air.model.FlightOnWardDetailBean;
import com.irctc.air.model.search_result_one_way.Flights;
import com.irctc.air.util.DateUtility;

import java.util.Comparator;

/**
 * Created by tourism on 4/13/2016.
 */
public class SortByArrivalTime implements Comparator<Flights>{

    @Override
    public int compare(Flights obj1, Flights obj2) {

        double arrivalTimeOne = Double.parseDouble(obj1.getArrivalTime().replace(":", "."));
        double arrivalTimeTwo = Double.parseDouble(obj2.getArrivalTime().replace(":", "."));

//        double arrivalTimeOne = Double.parseDouble(obj1.getFlight().get(0).getFlightArrivalTime().replace(":", "."));
//        double arrivalTimeTwo = Double.parseDouble(obj2.getFlight().get(0).getFlightArrivalTime().replace(":", "."));

        if (arrivalTimeOne > arrivalTimeTwo) {
            return 1;
        } else if (arrivalTimeOne < arrivalTimeTwo) {
            return -1;
        } else {
            return 0;
        }
    }
}
