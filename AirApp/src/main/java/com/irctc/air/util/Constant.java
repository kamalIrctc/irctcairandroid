package com.irctc.air.util;


public class Constant {

    public static final int TIMEOUT = 90000;
    public static final int MAX_RETRY = 2;

    //Alert Action
    public static final int ALERT_ACTION_ONE = 0;
    public static final int ALERT_ACTION_TWO = 1;


    // Fragment value
    public static final int PLANNER_FRAGMENT = 1;
    public static final int STATION_SEARCH_FRAGMENT = 2;
    public static final int CALENDER_FRAGMENT = 3;
    public static final int RECENT_SEARCH_FRAGMENT = 4;
    public static final int ONEWAY_FLIGHT_FRAGMENT = 5;
    public static final int FARE_QUOTE_FRAGMENT = 6;
    public static final int FRAGMENT_ROUND_TRIP = 62;
    public static final int FRAGMENT_INTERNATION_ROUND_TRIP = 82;

    public static final int LOGIN_FRAGMENT = 7;
    public static final int ONEWAY_FILTER_FRAGMENT = 8;

    public static final int ADD_PASS_FRAGMENT = 11;
    public static final int PASS_SIDE_LIST_FRAGMENT = 12;


    //  SCREEN TYPE FOR ROUND TRIP
    public static final int IS_ROUNDTRIP_ONWARD_RETURN = 1;
    public static final int IS_ROUNDTRIP_ROUND_ONWARD = 2;
    public static final int IS_ROUND_RETURN_SCREEN = 3;


    // Booking history
    public static final int BOOKING_HISTORY_FRAGMENT = 21;

    // Payment webview
    public static final int PAYMENT_WEBVIEW_FRAGMENT = 22;



    public static final int RETURN_FLIGHT_FRAGMENT = 31;



    // Confirm ticket
    public static final int CONFIRM_TICKET_FRAGMENT = 40;
    public static final int PAYMENT_FAILED_FRAGMENT = 45;


    public static final int MY_PROFILE = 13;

    public static final int WEBVIEW_FRAGMENT = 81;
    // Booking history
    public static final int BOOKING_HISTORY_INFO_FRAGMENT = 51;

    public static final int TERM_AND_CONDITIONS = 71;

    public static final int ADD_MODIFY_PASS_FRAGMENT = 91;

    public static final int CANCEL_TICKET_FRAGMENT = 90;



}
