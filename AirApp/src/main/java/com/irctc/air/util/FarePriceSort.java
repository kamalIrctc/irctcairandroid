package com.irctc.air.util;

import com.irctc.air.model.FlightOnWardDetailBean;

import java.util.Comparator;

/**
 * Created by tourism on 4/13/2016.
 */
public class FarePriceSort implements Comparator<FlightOnWardDetailBean>{

    @Override
    public int compare(FlightOnWardDetailBean obj1, FlightOnWardDetailBean obj2) {

        int farePriceOne = obj1.getFlightFare();
        int farePriceTwo = obj2.getFlightFare();

        if (farePriceOne > farePriceTwo) {
            return 1;
        } else if (farePriceOne < farePriceTwo) {
            return -1;
        } else {
            return 0;
        }
    }
}
