package com.irctc.air.util;

import com.irctc.air.model.FlightOnWardDetailBean;

import java.util.Comparator;

/**
 * Created by tourism on 4/13/2016.
 */
public class AirFlightSort implements Comparator<FlightOnWardDetailBean>{

    @Override
    public int compare(FlightOnWardDetailBean obj1, FlightOnWardDetailBean obj2) {

        String farePriceOne = obj1.getFlightAirline();
        String farePriceTwo = obj2.getFlightAirline();

        return farePriceOne.compareToIgnoreCase(farePriceTwo);
    }
}
