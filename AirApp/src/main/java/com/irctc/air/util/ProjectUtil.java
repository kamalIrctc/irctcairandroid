package com.irctc.air.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.navigationdrawer.SliderMenu;
import com.irctc.air.Database.SharedPrefrenceAir;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by vivek on 04/13/2016 for utilities
 */
public class ProjectUtil {

    public static final String keyText = "dummykey12345689";


    public static void showToast(final String message, final Context ctx) {

        ((AppCompatActivity) ctx).runOnUiThread(new Runnable() {
            public void run() {
                Toast toast = Toast.makeText(ctx,message, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 250);
                toast.show();

            }
        });


    }

    public static boolean checkInternetConnection(Context context) {

        boolean flag = false;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected()) {
            flag = true;
        }
        return flag;
    }



    public static String getAlpha(String lstrFrom){

        String strArr[] = lstrFrom.split(" ");

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 16; i++) {
            builder.append(strArr[i].charAt(0));
        }
        String str = builder.toString();
        return str;
    }

    // JSON Parser
    public static JSONObject getJSONObject(final String JSON_DATA) {

        JSONObject lObjJSON = null;
        try {
            lObjJSON = new JSONObject(JSON_DATA);

        } catch (JSONException e) {
            lObjJSON = null;
        }
        return lObjJSON;
    }


    public synchronized static void replaceFragment(Context context, Fragment fragment, int frameLayoutId, EnumAnimation enumAnimation) {


        // In case this activity was started with special instructions from an
        // Intent, pass the Intent's extras to the fragment as arguments

        // Add the fragment to the 'fragment_container' FrameLayout
        try {

            FragmentTransaction transaction = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();

            // Replace whatever is in the fragment_container view with this fragment,
            // and add the transaction to the back stack so the user can navigate back


            switch (enumAnimation) {
                case LEFT_TO_RIGHT:
                    transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
                    break;
                case RIGHT_TO_LEFT:

                    transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
                    break;
                case NEITHER_LEFT_NOR_RIGHT:
                    break;
                case DOWN_TO_TOP:
                    transaction.setCustomAnimations(R.anim.slide_up, R.anim.slide_down_from_top);
                    break;
                /*case TOP_TO_DOWN:
                    transaction.setCustomAnimations(R.anim.top_to_bottom, R.anim.slide_up);
                    break;*/
                default:
                    break;
            }
            //  transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);


            transaction.replace(frameLayoutId, fragment);
            transaction.addToBackStack(null);

            // Commit the transaction
            transaction.commitAllowingStateLoss();

//            ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction()
//                    .add(frameLayoutId, fragment).commitAllowingStateLoss();

        } catch (Exception e) {
            e.printStackTrace();
        }



    }


    /**
     * validate your email address format. Ex-akhi@mani.com
     */
    public static boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }


    //Decimal Checker
    public static boolean isValidNumber(String param) {
        //  String MOBILE_REGX="^([0-9]|[1-9][0-9]|[1-9][0-9][0-9])$";
        String MOBILE_REGX = "^[0-9]{1,3}$";

        return param.matches(MOBILE_REGX);
    }





    /**
     * Sets ListView height dynamically based on the height of the items.
     *
     * @return true if the listView is successfully resized, false otherwise
     */
    public static boolean setListViewHeightBasedOnItems(ExpandableListView listView) {

        ExpandableListAdapter listAdapter = listView.getExpandableListAdapter();
        if (listAdapter != null) {

            int totalDividersHeight = 0;

            int numberOfItems = listAdapter.getGroupCount();

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {

                int numberOfChildItems = listAdapter.getChildrenCount(itemPos);
                for(int itemChildPos=0;itemChildPos<numberOfChildItems;itemChildPos++)
                {

                    View item =  listAdapter.getChildView(itemPos, itemChildPos, false, null,null);
                    item.measure(0, 0);
                    totalItemsHeight += item.getMinimumHeight();

                }

                View groupItem =  listAdapter.getGroupView(itemPos, true, null, listView);
                groupItem.measure(0, 0);
                totalItemsHeight += groupItem.getMeasuredHeight();

            }



            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();


            return true;

        } else {
            return false;
        }

    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }


    public static void dialogColor(Dialog dialog) {

        try {

           //dialog.getWindow().setBackgroundDrawable(dialog.getContext().getResources().getDrawable(R.drawable.test));

            int dividerId = dialog.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
            View divider = dialog.findViewById(dividerId);

            if (divider == null) {

            } else {
                divider.setBackgroundColor(dialog.getContext().getResources().getColor(R.color.colorGrayBlack));

            }

            int textViewId = dialog.getContext().getResources().getIdentifier("android:id/alertTitle", null, null);
            TextView tv = (TextView) dialog.findViewById(textViewId);

            if (tv == null) {

            } else {
                tv.setTextColor(dialog.getContext().getResources().getColor(R.color.colorDarktBlue));
            }
        } catch (Resources.NotFoundException e) {


        } catch (Exception e) {

        }

    }

    public static void dialogColorAlert(AlertDialog.Builder alertDialogBuilder) {

        try {
            Dialog d = alertDialogBuilder.show();

            d.setCanceledOnTouchOutside(false);

            int dividerId = d.getContext().getResources().getIdentifier("android:id/titleDivider", null, null);
            View divider = d.findViewById(dividerId);
            if (divider == null) {

            } else {
                divider.setBackgroundColor(d.getContext().getResources().getColor(R.color.colorGrayBlack));

            }

            int textViewId = d.getContext().getResources().getIdentifier("android:id/alertTitle", null, null);
            TextView tv = (TextView) d.findViewById(textViewId);

            if (tv == null) {

            } else {
                tv.setTextColor(d.getContext().getResources().getColor(R.color.colorDarktBlue));

            }
        } catch (Exception e) {
            // alertDialogBuilder.show();
        }


    }


    //Decimal Creation
    public static double decimalConverter(double userSelectedPrice) {

        try {
            java.text.DecimalFormat df = new java.text.DecimalFormat("#.00");
            userSelectedPrice = Double.valueOf(df.format(Math.round(userSelectedPrice)));
        } catch (NumberFormatException e) {


        }


        return userSelectedPrice;
    }

    public static Object deepCopy(Object input) {

        ObjectOutputStream oos = null;
        ObjectInputStream ois = null;
        Object output = null;

        try {

            // deep copy
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(bos);
            // serialize and pass the object
            oos.writeObject(input);
            oos.flush();
            ByteArrayInputStream bin =
                    new ByteArrayInputStream(bos.toByteArray());
            ois = new ObjectInputStream(bin);
            // return the new object
            output = ois.readObject();

            // verify it is the same

        } catch (Exception e) {
            //System.out.println("Exception in main = " + e);
        }


        return output;
    }


    public static void showAlertDialogAndStartFragment(final Context context, final Fragment fragment, String message) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ActivityMain.context);

        // Setting Dialog Title
        alertDialog.setTitle(R.string.app_name);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        alertDialog.setCancelable(false);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                try {

                    dialog.cancel();

                    ProjectUtil.replaceFragment(context, fragment, R.id.frame_layout, EnumAnimation.RIGHT_TO_LEFT);

                    // Stop back button thru backStackManagement class flag
                    ActivityMain.boolBackClose = false;

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });


        // Showing Alert Message
        ProjectUtil.dialogColorAlert(alertDialog);

    }

//    public static void showPNRDialogBox(final Context context) {
//        final Dialog dialog = new Dialog(context);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.pnr_dialog);
//
//
//        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        final AutoCompleteTextView actvPnrNumber = (AutoCompleteTextView) dialog.findViewById(R.id.et_pnr_number);
//        final TextView lTxtErrorMsg = (TextView) dialog.findViewById(R.id.tv_error);
//        lTxtErrorMsg.setVisibility(View.GONE);
//
//        /**
//         * check if sbs is false then show arraylist in autocompletetextview otherwise no
//         */
//
//
//            AirDatabase database = new AirDatabase(context);
//            ArrayList<String> alPnr = database.getPnrList();
//            ArrayAdapter<String> adapter = new ArrayAdapter<String>
//                    (context, R.layout.list_row_pnr, alPnr);
//            actvPnrNumber.setThreshold(1);//will start working from first character
//            actvPnrNumber.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
//
//
//        dialog.show();
//
//        Button lBtnSubmit = (Button) dialog.findViewById(R.id.btn_submit);
//        // if submit button is clicked, perform operation
//
//        lBtnSubmit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (!ProjectUtil.checkInternetConnection(context)) {
//                    ProjectUtil.showToast(context.getResources().getString(R.string.network_not_available), context);
//
//                } else {
//                    if (actvPnrNumber.getText().toString().trim().equalsIgnoreCase("")) {
//                        lTxtErrorMsg.setVisibility(View.VISIBLE);
//                        lTxtErrorMsg.setText("Please enter the PNR.");
//                    } else if (actvPnrNumber.getText().toString().trim().length() < 10) {
//                        lTxtErrorMsg.setVisibility(View.VISIBLE);
//                        lTxtErrorMsg.setText("Please enter the 10 digit PNR.");
//                    } else {
//                        lTxtErrorMsg.setVisibility(View.GONE);
//                        lTxtErrorMsg.setText("");
//
//
//                        // cancel the dialog
//                        dialog.dismiss();
//
//
//                        new PnrNumberVolleyRequest(context, actvPnrNumber.getText().toString().trim(), actvPnrNumber).execute();
//
//                    }
//
//                }
//
//
//            }
//        });
//
//        actvPnrNumber.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                lTxtErrorMsg.setVisibility(View.GONE);
//                lTxtErrorMsg.setText("");
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
//    }



    // Method to return xml from hashmap
    public String convertMapToXml(HashMap<String,String> map, String rootTag){

        StringBuilder mXml = new StringBuilder("<");
        mXml.append(rootTag);
        mXml.append(">");

        for (Map.Entry<String,String> e : map.entrySet()) {

            mXml.append("<");
            mXml.append(e.getKey());
            mXml.append(">");

            mXml.append(e.getValue());
            mXml.append("</");
            mXml.append(e.getKey());
            mXml.append(">");
        }

        mXml.append("</");
        mXml.append(rootTag);
        mXml.append(">");

//        return "<?xml version=\"1.0\" encoding=\"utf-8\"?>" + mXml.toString();

        return mXml.toString();
    }

    public String convertMapToXml1(LinkedHashMap<String,String> map, String rootTag){

        StringBuilder mXml = new StringBuilder("<");
        mXml.append(rootTag);
        mXml.append(" ");

        for (Map.Entry<String,String> e : map.entrySet()) {

            mXml.append("");
            mXml.append(e.getKey());
            mXml.append("=\"");

            mXml.append(e.getValue());
            mXml.append("\" ");
//            mXml.append(e.getKey());
//            mXml.append("");
        }

       // mXml.append(rootTag);
        mXml.append("/>");

        return /*"<?xml version=\"1.0\" encoding=\"utf-16\"?>" + */mXml.toString();
    }


    public static void updateListViewHeight(ListView myListView) {
        ListAdapter myListAdapter = myListView.getAdapter();
        if (myListAdapter == null) {
            return;
        }
        //get listview height
        int totalHeight = 0;
        int adapterCount = myListAdapter.getCount();
        for (int size = 0; size < adapterCount ; size++) {
            View listItem = myListAdapter.getView(size, null, myListView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        //Change Height of ListView
        ViewGroup.LayoutParams params = myListView.getLayoutParams();
        params.height = totalHeight + (myListView.getDividerHeight() * (adapterCount - 1));
        myListView.setLayoutParams(params);
    }


    public static void updatePassExpListViewHeight(ExpandableListView myListView) {
        ExpandableListAdapter myExpListAdapter = myListView.getExpandableListAdapter();
        if (myExpListAdapter == null) {
            return;
        }
        //Get listview height
        int totalHeight = 0;
        int adapterGroupCount = myExpListAdapter.getGroupCount();

        for (int size = 0; size < adapterGroupCount ; size++) {

            int adapterChildCount = myExpListAdapter.getChildrenCount(size);

            for (int size1 = 0; size1 < adapterChildCount ; size1++) {

                View listItem = myExpListAdapter.getChildView(size, size1, false, null, myListView);
                listItem.measure(0, 0);
                totalHeight += listItem.getMeasuredHeight();

                View listeItem = myExpListAdapter.getGroupView(size,true,null, myListView);
                listeItem.measure(0, 0);
                totalHeight += listeItem.getMeasuredHeight();
            }
        }

        //Change Height of ListView
        ViewGroup.LayoutParams params = myListView.getLayoutParams();
        params.height = totalHeight;
        myListView.setLayoutParams(params);
    }


    /**********************************************************************************************
     * CUSTOM
     *********************************************************************************************/
    public static void showCustomDFialog(Dialog lObjDialogShowFlightDetails, ActivityMain mainActivity) {


        final Dialog finalLObjDialogShowFlightDetails = lObjDialogShowFlightDetails;
        if (!mainActivity.isFinishing()) {

            lObjDialogShowFlightDetails = new Dialog(mainActivity, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
            mainActivity.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            //mainActivity.getWindow().setGravity();
            lObjDialogShowFlightDetails.requestWindowFeature(Window.FEATURE_NO_TITLE);
            lObjDialogShowFlightDetails.getWindow().setBackgroundDrawable(new ColorDrawable(Color.LTGRAY));

            // Set layout
            lObjDialogShowFlightDetails.setContentView(R.layout.custom_dialog);

            PlayGifView pGif = (PlayGifView) lObjDialogShowFlightDetails.findViewById(R.id.viewGif);
            pGif.setImageResource(R.drawable.test);



            TextView lObjBtnCancel = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.CANCEL);


            lObjBtnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    finalLObjDialogShowFlightDetails.cancel();
                }
            });

            TextView lObjBtnDone = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.OK);

            lObjBtnDone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    finalLObjDialogShowFlightDetails.cancel();
                }
            });


            // Show dialog
            lObjDialogShowFlightDetails.show();

        }
    }




//    public static String getImeiNumber(Context context){
//
//        String value = null;
//        // Check if the READ_PHONE_STATE permission is already available.
//                // READ_PHONE_STATE permissions is already available, show the camera preview.
//                TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
//
//        try {
//            value = telephonyManager.getDeviceId();
//        }catch (Exception e){
//            value = "0000000000";
//        }
//         return value;
//    }


    public static boolean isInternationalOrDomestic(String value, String value2){
        boolean valid = false;

        String valueArr[] = value.split(" ");

        boolean validOne = false;
        for (int i = 0; i < valueArr.length ; i++) {

            if(valueArr[i].equalsIgnoreCase("IN")){
                validOne = true;
            }
        }

        String valueArr2[] = value2.split(" ");
        boolean validTwo = false;
        for (int i = 0; i < valueArr2.length ; i++) {

            if(valueArr2[i].equalsIgnoreCase("IN")){
                validTwo = true;
            }
        }

        if(validTwo && validOne)
        {
            valid = true;
        }

        return valid;
    }



    public static void setUserLogin(Context mObjContext){
/*

        if(new SharedPrefrenceAir(mObjContext).getIsLoggedIn()){

            if(new SharedPrefrenceAir(mObjContext).getIsGuestUser()){

                SliderMenu.txtUserId.setText("Guest");
                //SliderMenu.txtUserId.setText(AES.decrypt(new SharedPrefrenceAir(mObjContext).getGuestUserEmail()));

            }else{

                SliderMenu.txtUserId.setText(AES.decrypt(new SharedPrefrenceAir(mObjContext).getUserName()));
            }

        }else{
            SliderMenu.txtUserId.setText("guest");
        }
*/
    }

    public static String capitalFirstLetter(String string) {

        String lstr = string.trim();

        String str[] = lstr.split(" ");
        String output = "";

        for (int i = 0; i < str.length; i++) {
            output = output + str[i].substring(0, 1).toUpperCase() + str[i].substring(1, str[i].length()).toLowerCase() +" ";
        }
       return output.trim();
    }

}

