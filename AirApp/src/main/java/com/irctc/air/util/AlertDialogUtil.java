package com.irctc.air.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;

import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;


public class AlertDialogUtil {

    private Context context;
    private String message = "";
    private String title;
    private int action;
    Fragment frag;
    private AlertDialog.Builder alertDialog;
    private ActivityMain mainActivity;

    public AlertDialogUtil(Context context, String message, String title, int action) {

        this.context = context;
        this.message = message;
        this.title = title;
        this.action = action;
        this.mainActivity = (ActivityMain)context;

        alertDialog = new AlertDialog.Builder(context);
    }


    public AlertDialogUtil(Context context, String message, String title, int action, Fragment frag) {

        this.context = context;
        this.message = message;
        this.title = title;
        this.action = action;
        this.frag = frag;
        alertDialog = new AlertDialog.Builder(context);
    }

    public void generateAlert() {
        switch (action) {

            case 0:
                alertDialogWithOneAction();
                break;
            case 1:
                alertDialogWithOneAction(frag);
                break;
            default:
                ProjectUtil.showToast(context.getString(R.string.ERROR_MESSAGE), context);
                break;
        }
    }



    public void alertDialogWithOneAction(final Fragment frag) {

        try {
            alertDialog.setTitle(title);

            if (message == null)
            {
                alertDialog.setMessage(R.string.ERROR_MESSAGE);
            } else if (message.isEmpty())
            {
                alertDialog.setMessage(R.string.ERROR_MESSAGE);
            }
            else{
                alertDialog.setMessage(message);
            }

            // Setting OK Button
            alertDialog.setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    dialog.cancel();

                    mainActivity.lastActiveFragment = Constant.PLANNER_FRAGMENT;
                    ProjectUtil.replaceFragment(context, frag, R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);


                }
            });
            // Showing Alert Message
            ProjectUtil.dialogColorAlert(alertDialog);
        }

        catch (Exception e)
        {

            ProjectUtil.showToast(context.getString(R.string.ERROR_MESSAGE), context);

        }

    }

    public void alertDialogWithOneAction() {

        try {
        alertDialog.setTitle(title);

        if (message == null)
        {
            alertDialog.setMessage(R.string.ERROR_MESSAGE);
        } else if (message.isEmpty())
        {
            alertDialog.setMessage(R.string.ERROR_MESSAGE);
        }
        else{
            alertDialog.setMessage(message);
        }

        // Setting OK Button
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();

                // Write your code here to execute after dialog closed
                //Toast.makeText(getApplicationContext(), "You clicked on OK", Toast.LENGTH_SHORT).show();
            }
        });
        // Showing Alert Message
        ProjectUtil.dialogColorAlert(alertDialog);
        }

        catch (Exception e)
        {

            ProjectUtil.showToast(context.getString(R.string.ERROR_MESSAGE), context);

        }

    }


}
