package com.irctc.air.util.sorting_round_trip_international;

import android.util.Log;

import com.irctc.air.model.search_result_one_way.Flights;
import com.irctc.air.model.search_result_round_trip.model_combo.ModelFlightsCombo;

import java.util.Comparator;

/**
 * Created by tourism on 4/13/2016.
 */
public class SortByFarePrice implements Comparator<ModelFlightsCombo>{

    @Override
    public int compare(ModelFlightsCombo obj1, ModelFlightsCombo obj2) {
try {
    int farePriceOne = Integer.parseInt(obj1.getOnwardFlightsList().get(0).getPrice());
    int farePriceTwo = Integer.parseInt(obj2.getOnwardFlightsList().get(0).getPrice());
    if (farePriceOne > farePriceTwo) {
        return 1;
    } else if (farePriceOne < farePriceTwo) {
        return -1;
    } else {
        return 0;
    }
}catch(Exception e){
    return  0;
}

    }

}
