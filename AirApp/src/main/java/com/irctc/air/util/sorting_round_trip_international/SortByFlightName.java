package com.irctc.air.util.sorting_round_trip_international;

import com.irctc.air.model.search_result_one_way.Flights;
import com.irctc.air.model.search_result_round_trip.model_combo.ModelFlightsCombo;

import java.util.Comparator;

/**
 * Created by tourism on 4/13/2016.
 */
public class SortByFlightName implements Comparator<ModelFlightsCombo>{

    @Override
    public int compare(ModelFlightsCombo obj1, ModelFlightsCombo obj2) {
        String f1 = obj1.getOnwardFlightsList().get(0).getCarrierName();
        String f2 = obj2.getOnwardFlightsList().get(0).getCarrierName();
        return f1.compareToIgnoreCase(f2);
    }
}
