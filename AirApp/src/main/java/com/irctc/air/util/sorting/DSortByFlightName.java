package com.irctc.air.util.sorting;


import com.irctc.air.round.trip.domastic.model.Flights;

import java.util.Comparator;

/**
 * Created by tourism on 4/13/2016.
 */
public class DSortByFlightName implements Comparator<Flights>{

    @Override
    public int compare(Flights obj1, Flights obj2) {

        String f1 = obj1.getCarrierName();
        String f2 = obj2.getCarrierName();

        return f1.compareToIgnoreCase(f2);
    }
}
