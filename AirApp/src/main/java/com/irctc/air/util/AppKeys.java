package com.irctc.air.util;

/**
 * Created by Rajnikant Kumar on 8/30/2018.
 */

public class AppKeys {
    public static final String LOGIN_STATUS = "loginStatus";
    //region : User Details
    public static final String USER_DETAIL_ADDRESS_1 = "address1";
    public static final String USER_DETAIL_ADDRESS_2 = "address2";
    public static final String USER_DETAIL_FIRST_NAME = "firstName";
    public static final String USER_DETAIL_LAST_NAME = "lastName";
    public static final String USER_DETAIL_CITY = "city";
    public static final String USER_DETAIL_STATE = "state";
    public static final String USER_DETAIL_COUNTRY = "country";
    public static final String USER_DETAIL_PIN_CODE = "pinCode";
    public static final String USER_DETAIL_EMAIL = "email";
    public static final String USER_DETAIL_MOBILE_NO = "mobileNo";
    public static final String USER_DETAIL_USER_TYPE = "UserType";
    //endregion

    //region : Remember user choice
   public static final String USER_CHOICE_CHECK_1 = "userChoiceCheck1";
    public static final String USER_CHOICE_CHECK_2 = "userChoiceCheck2";
    public static final String USER_CHOICE_CHECK_3 = "userChoiceCheck3";
    public static final String USER_CHOICE_CHECK_4 = "userChoiceCheck4";
    public static final String USER_CHOICE_STOP_1 = "userChoiceStop1";
    public static final String USER_CHOICE_STOP_2 = "userChoiceStop2";
    public static final String USER_CHOICE_STOP_3 = "userChoiceStop3";
    //endregion
    //region : Flight icon path
    public static final String FLIGHT_ICON_PATH = "flightIconPath";
    //endregion
  /*    public static final String USER_CHOICE_DEP_MONTH = "userChoiceDepMonth";
    public static final String USER_CHOICE_DEP_YEAR = "userChoiceDepYear";
    public static final String USER_CHOICE_DEP_MONTH_NAME = "userChoiceDepMonthName";
    public static final String USER_CHOICE_DEP_DAY_NAME = "userChoiceDepDayName";
    public static final String USER_CHOICE_ARR_DATE = "userChoiceArrDate";
    public static final String USER_CHOICE_ARR_MONTH = "userChoiceArrMonth";
    public static final String USER_CHOICE_ARR_YEAR = "userChoiceArrYear";
    public static final String USER_CHOICE_ARR_MONTH_NAME = "userChoiceArrMonthName";
    public static final String USER_CHOICE_ARR_DAY_NAME = "userChoiceArrDayName";*/
    //endregion
}


