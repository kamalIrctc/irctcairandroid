package com.irctc.air.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.PopupWindow;

import com.irctc.air.Database.AirDatabase;
import com.irctc.air.R;
import com.irctc.air.adapter.PassengerListOperationAdapter;
import com.irctc.air.fragment.AddModifyPassengerFragment;
import com.irctc.air.fragment.FragmentPassengerList;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.model.PassDetailbean;
import com.irctc.air.model.PassengerOperationBean;

import java.util.ArrayList;

/**
 * Created by tourism on 6/14/2016.
 */
public class PassengerListClickHandler implements View.OnClickListener {

    Context context;
    int position;
    private PopupWindow popupWindow;

    public PassengerListClickHandler(Context context, int position) {

        this.context = context;
        this.position = position;

    }

    @Override
    public void onClick(View v) {

        PopupWindow popUp = popupWindowsort();
        popUp.showAsDropDown(v, 0, 0); // show popup like dropdown list
    }

    /**
     * show popup window method reuturn PopupWindow
     */
    private PopupWindow popupWindowsort() {

        // initialize a pop up window type
        popupWindow = new PopupWindow(context);

//        ArrayList<String> sortList = new ArrayList<String>();
//        sortList.add("A to Z");
//        sortList.add("Z to A");
//        sortList.add("Low to high price");

       // ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line, sortList);

        PassengerListOperationAdapter adapter = new PassengerListOperationAdapter(context, getData() );

        // the drop down list is a list view
        ListView listViewSort = new ListView(context);
        listViewSort.setPadding(0, 10, 20, 0);

        // set our adapter and pass our pop up window contents
        listViewSort.setAdapter(adapter);

        // set on item selected
        listViewSort.setOnItemClickListener(onItemClickListener());

        // some other visual settings for popup window
        popupWindow.setFocusable(true);
        popupWindow.setWidth(250);
        popupWindow.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        // set the list view as pop up window content
        popupWindow.setContentView(listViewSort);

        return popupWindow;
    }

    private ArrayList<PassengerOperationBean> getData() {

        // here definwe the operation to be perform on passenger list item
        int []opeartionImage = {R.drawable.delete, R.drawable.edit};
        String []opeartions = {"Delete","Modify"};

        ArrayList<PassengerOperationBean> operationList = new ArrayList<>();

        for (int i = 0; i < opeartionImage.length; i++) {
            PassengerOperationBean operationBean = new PassengerOperationBean();
            operationBean.setImageId(opeartionImage[i]);
            operationBean.setOperationName(opeartions[i]);
            operationList.add(operationBean);
        }
        return operationList;
    }

    private AdapterView.OnItemClickListener onItemClickListener() {
        return new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView parent, View view, int position1, long id) {

                if (position1 == 0) {
                    // to perform the delete operation
                    // here open the dialog for deletion confirmation

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                    alertDialog.setTitle("Warning");
                    alertDialog.setMessage(R.string.DELETE_MESSAGE);
                    // Setting OK Button

                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {

                            AirDatabase database = new AirDatabase(context);
                            database.open();
                            ArrayList<PassDetailbean> lAlPasseger = database.getAllPassengerList();
                            database.clearPassengerList();
                            lAlPasseger.remove(position);

                            database.insertPassengerInDB(lAlPasseger);
                            database.close();

                            ProjectUtil.replaceFragment(context, new FragmentPassengerList(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                            dialog.cancel();
                        }
                    });
                    alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    // Showing Alert Message
                    ProjectUtil.dialogColorAlert(alertDialog);

                } else if (position1 == 1) {
                    // to perform the modify operation

                    Fragment addModifyPassengerFragment = new AddModifyPassengerFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt("modifyAt", position);
                    bundle.putString("commingFrom", "modify");
                    addModifyPassengerFragment.setArguments(bundle);

                    ActivityMain.lastActiveFragment = Constant.ADD_PASS_FRAGMENT;
                    ProjectUtil.replaceFragment(context, addModifyPassengerFragment, R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

                }
                dismissPopup();
            }
        };
    }

    private void dismissPopup() {
        if (popupWindow != null) {
            popupWindow.dismiss();
        }
    }
}
