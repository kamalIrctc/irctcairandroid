package com.irctc.air.util;

import com.irctc.air.model.FlightOnWardDetailBean;

import java.util.Comparator;

/**
 * Created by tourism on 4/13/2016.
 */
public class ArrivalTimeSort implements Comparator<FlightOnWardDetailBean>{

    @Override
    public int compare(FlightOnWardDetailBean obj1, FlightOnWardDetailBean obj2) {

        double arrivalTimeOne = Double.parseDouble(DateUtility.getTimeFromCal(obj1.getFlightOnwArrivalTime()).replace(":", "."));
        double arrivalTimeTwo = Double.parseDouble(DateUtility.getTimeFromCal(obj2.getFlightOnwArrivalTime()).replace(":", "."));

//        double arrivalTimeOne = Double.parseDouble(obj1.getFlight().get(0).getFlightArrivalTime().replace(":", "."));
//        double arrivalTimeTwo = Double.parseDouble(obj2.getFlight().get(0).getFlightArrivalTime().replace(":", "."));

        if (arrivalTimeOne > arrivalTimeTwo) {
            return 1;
        } else if (arrivalTimeOne < arrivalTimeTwo) {
            return -1;
        } else {
            return 0;
        }
    }
}
