package com.irctc.air.util;

/**
 * Created by vivek on 04/13/2016
 */
public enum EnumAnimation {

    LEFT_TO_RIGHT, RIGHT_TO_LEFT, NEITHER_LEFT_NOR_RIGHT, TOP_TO_DOWN, DOWN_TO_TOP

}
