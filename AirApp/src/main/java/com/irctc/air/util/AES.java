package com.irctc.air.util;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.irctc.air.R;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


public class AES
{
	private static final byte[] IV_Parameter = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

	/* key used for encryption */
	private static SecretKeySpec secretKey;

	private final static String AES_CBC = "AES/CBC/PKCS7Padding";

	private static Context context = null;
	private static String value = null;

	public AES(Context context){
		this.context = context;
		//value = TProjectUtil.getAlpha(TSplashActivity.ctx.getResources().getString(R.string.Exception));
	}



	/* generate() is used to generate a secret key for AES algorithm */
	private static void generate()
	{
		try
		{

			value = generateException();

			if(value!= null && !value.equalsIgnoreCase("")) {
				secretKey = new SecretKeySpec(value.getBytes("UTF-8"), "AES");
			}else{
				secretKey = new SecretKeySpec(value.getBytes("UTF-8"), "AES");

				//secretKey = new SecretKeySpec(TProjectUtil.getAlpha(context.getResources().getString(R.string.Exception)).getBytes("UTF-8"), "AES");
			}
		}
		catch (UnsupportedEncodingException e)
		{
			Log.w("AES not supported", e.toString());
		}
	}



	/**
	 * Encrypt the data
	 * 
	 * @param tobeEncrypted
	 * @return encrypted
	 */
	public static String encrypt ( String tobeEncrypted)
	{
		String encrypted = "";

		generate();
		try
		{
			Cipher cipher = Cipher.getInstance(AES_CBC);
			cipher.init(Cipher.ENCRYPT_MODE, secretKey, new IvParameterSpec(IV_Parameter));
			byte[] result = cipher.doFinal(tobeEncrypted.getBytes("UTF-8"));
			// encrypted = new String(Base64.encodeToString(result, Base64.DEFAULT));
			encrypted = new String(Base64.encodeToString(result, Base64.NO_WRAP));
		}
		catch (NoSuchAlgorithmException e)
		{
			Log.w("NSAE", e.getCause());
			// e.printStackTrace();
		}
		catch (NoSuchPaddingException e)
		{
			Log.w("encrypt_NSPE", e.getCause());
			// e.printStackTrace();
		}
		catch (InvalidKeyException e)
		{
			Log.w("encrypt_InvalidKeyExp", e.getCause());
			// e.printStackTrace();
		}
		catch (InvalidAlgorithmParameterException e)
		{
			Log.w("encrypt_IAPE", e.getCause());
			// e.printStackTrace();
		}
		catch (IllegalBlockSizeException e)
		{
			Log.w("encrypt_IBSE", e.getCause());
			// e.printStackTrace();
		}
		catch (BadPaddingException e)
		{
			Log.w("encrypt_BPE", e.getCause());
			// e.printStackTrace();
		}
		catch (UnsupportedEncodingException e)
		{
			Log.w("encrypt_UEE", e.getCause());
			// e.printStackTrace();
		}
		return encrypted.trim();
	}

	private static String generateException(){

		return validateExceotion("The 500 Exception was not common among 7100 cases out of 680 possible issue and 10th of its subclasses are form of Throwable");
	}

	public static String validateExceotion(String lstrFrom){

		String strArr[] = lstrFrom.split(" ");

		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < 16; i++) {
			builder.append(strArr[i].charAt(0));
		}
		String str = builder.toString();
		return str;
	}

	/**
	 * Decrypt the data
	 * 
	 * @param encrypted
	 * @return decoded
	 */
	public static String decrypt ( String encrypted )
	{
		String decoded = "";
		generate();
		try
		{
			Cipher cipher = Cipher.getInstance(AES_CBC);
			cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(IV_Parameter));
			final byte[] DECODES = Base64.decode(encrypted, Base64.DEFAULT);
			decoded = new String(cipher.doFinal(DECODES), "UTF-8");
		}
		catch (NoSuchAlgorithmException e)
		{
			Log.w("encrypt_NSAE", e.getCause());
			// e.printStackTrace();
		}
		catch (NoSuchPaddingException e)
		{
			Log.w("encrypt_NSPE", e.getCause());
			// e.printStackTrace();
		}
		catch (InvalidKeyException e)
		{
			Log.w("encrypt_IKE", e.getCause());
			// e.printStackTrace();
		}
		catch (InvalidAlgorithmParameterException e)
		{
			Log.w("encrypt_IAPE", e.getCause());
			// e.printStackTrace();
		}
		catch (IllegalBlockSizeException e)
		{
			Log.w("encrypt_IBSE", e.getCause());
			// e.printStackTrace();
		}
		catch (BadPaddingException e)
		{
			Log.w("encrypt_BPE", e.getCause());
			// e.printStackTrace();	
		}
		catch (UnsupportedEncodingException e)
		{
			Log.w("encrypt_UEE", e.getCause());
			// e.printStackTrace();
		}
		return decoded;
	}

	/////////////////////////////////////////////////////////////////////////////////////////////


/*
	*//**
	 * Encrypt the data
	 *
	 * @param tobeEncrypted
	 * @return encrypted
	 *//*
	public static String encryptMain ( String tobeEncrypted, Context context)
	{
		String encrypted = "";

		generate(ProjectUtil.getAlpha(context.getResources().getString(R.string.Exception)));
		try
		{
			Cipher cipher = Cipher.getInstance(AES_CBC);
			cipher.init(Cipher.ENCRYPT_MODE, secretKey, new IvParameterSpec(IV_Parameter));
			byte[] result = cipher.doFinal(tobeEncrypted.getBytes("UTF-8"));
			// encrypted = new String(Base64.encodeToString(result, Base64.DEFAULT));
			encrypted = new String(Base64.encodeToString(result, Base64.NO_WRAP));
		}
		catch (NoSuchAlgorithmException e)
		{
			Log.w("NSAE", e.getCause());
			// e.printStackTrace();
		}
		catch (NoSuchPaddingException e)
		{
			Log.w("encrypt_NSPE", e.getCause());
			// e.printStackTrace();
		}
		catch (InvalidKeyException e)
		{
			Log.w("encrypt_InvalidKeyExp", e.getCause());
			// e.printStackTrace();
		}
		catch (InvalidAlgorithmParameterException e)
		{
			Log.w("encrypt_IAPE", e.getCause());
			// e.printStackTrace();
		}
		catch (IllegalBlockSizeException e)
		{
			Log.w("encrypt_IBSE", e.getCause());
			// e.printStackTrace();
		}
		catch (BadPaddingException e)
		{
			Log.w("encrypt_BPE", e.getCause());
			// e.printStackTrace();
		}
		catch (UnsupportedEncodingException e)
		{
			Log.w("encrypt_UEE", e.getCause());
			// e.printStackTrace();
		}
		return encrypted.trim();
	}





	*//**
	 * Decrypt the data
	 *
	 * @param encrypted
	 * @return decoded
	 *//*
	public static String decryptMain ( String encrypted, Context context)
	{
		String decoded = "";
		generate(ProjectUtil.getAlpha(context.getResources().getString(R.string.Exception)));
		try
		{
			Cipher cipher = Cipher.getInstance(AES_CBC);
			cipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(IV_Parameter));
			final byte[] DECODES = Base64.decode(encrypted, Base64.DEFAULT);
			decoded = new String(cipher.doFinal(DECODES), "UTF-8");
		}
		catch (NoSuchAlgorithmException e)
		{
			Log.w("encrypt_NSAE", e.getCause());
			// e.printStackTrace();
		}
		catch (NoSuchPaddingException e)
		{
			Log.w("encrypt_NSPE", e.getCause());
			// e.printStackTrace();
		}
		catch (InvalidKeyException e)
		{
			Log.w("encrypt_IKE", e.getCause());
			// e.printStackTrace();
		}
		catch (InvalidAlgorithmParameterException e)
		{
			Log.w("encrypt_IAPE", e.getCause());
			// e.printStackTrace();
		}
		catch (IllegalBlockSizeException e)
		{
			Log.w("encrypt_IBSE", e.getCause());
			// e.printStackTrace();
		}
		catch (BadPaddingException e)
		{
			Log.w("encrypt_BPE", e.getCause());
			// e.printStackTrace();
		}
		catch (UnsupportedEncodingException e)
		{
			Log.w("encrypt_UEE", e.getCause());
			// e.printStackTrace();
		}
		return decoded;
	}*/
}