package com.irctc.air.util;

import com.irctc.air.model.FlightOnWardDetailBean;

import java.util.Comparator;

/**
 * Created by tourism on 4/13/2016.
 */
public class DurationTimeSort implements Comparator<FlightOnWardDetailBean>{

    @Override
    public int compare(FlightOnWardDetailBean obj1, FlightOnWardDetailBean obj2) {

//        String str1 = obj1.getFlightOnwardTotalDuration().replace("h ",":").replace("m","");
//        String str2 = obj2.getFlightOnwardTotalDuration().replace("h ",":").replace("m","");

        double durationTimeOne = 0;
        double durationTimeTwo = 0;

        if(obj1.getFlightOnwardTotalDuration().contains("m")){

            durationTimeOne = Double.parseDouble(obj1.getFlightOnwardTotalDuration().replace("h ", ".").replace("m", ""));
        }
        else{
            durationTimeOne = Double.parseDouble(obj1.getFlightOnwardTotalDuration().replace("h", ".00"));

        }

        if(obj2.getFlightOnwardTotalDuration().contains("m")){

            durationTimeTwo = Double.parseDouble(obj2.getFlightOnwardTotalDuration().replace("h ", ".").replace("m", ""));
        }
        else{
            durationTimeTwo = Double.parseDouble(obj2.getFlightOnwardTotalDuration().replace("h", ".00"));

        }

       // double durationTimeTwo = Double.parseDouble(obj2.getFlightOnwardTotalDuration().replace("h ",".").replace("m",""));

        if (durationTimeOne > durationTimeTwo) {
            return 1;
        } else if (durationTimeOne < durationTimeTwo) {
            return -1;
        } else {
            return 0;
        }

    }
}
