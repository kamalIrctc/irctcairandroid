package com.irctc.air.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Set;

/**
 * Created by Rajnikant Kumar on 8/30/2018.
 */

public class Pref {
    public static final String IS_RECENT_DATA_CLEAR= "isResentDataClear";
    public static SharedPreferences sharedPreferences;
    public static SharedPreferences.Editor editor;

    public static void setBoolean(Context context,boolean status){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPreferences.edit();
        editor.putBoolean(AppKeys.LOGIN_STATUS,status);
        editor.commit();
    }

    public static boolean getBoolean(Context context){
    sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    return sharedPreferences.getBoolean(AppKeys.LOGIN_STATUS,false);
    }

    public static void setBooleanParam(Context context,String key,boolean value){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPreferences.edit();
        editor.putBoolean(key,value);
        editor.commit();
    }
    public static boolean getBooleanParam(Context context,String key){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getBoolean(key,false);
    }

    public static void setString(Context context,String key,String value){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPreferences.edit();
        editor.putString(key,value);
        editor.commit();
    }

    public static String getString(Context context,String key){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(key,"");
    }

    public static void setFloat(Context context,String key,float value){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPreferences.edit();
        editor.putFloat(key,value);
        editor.commit();
    }

    public static float getFloat(Context context,String key){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getFloat(key,0);
    }

    public static void clearPref(Context context){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }

    public static synchronized boolean saveData(String key, boolean value) {
        editor.putBoolean(key, value);
        return editor.commit();
    }

    public static boolean isRecentDataClear() {
        return getData(IS_RECENT_DATA_CLEAR, false);
    }

    public static synchronized Boolean getData(String key, boolean defaultValue) {
        return sharedPreferences.getBoolean(key, defaultValue);
    }

}
