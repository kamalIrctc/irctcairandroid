package com.irctc.air.util;

import android.content.Context;

import com.irctc.air.parser.StationListDomParser;

import java.util.ArrayList;

/**
 * Created by Aasif on 4/18/2016.
 *
 *
 */
public class StationtList {

    public ArrayList<String> mList;
    private Context mObjCtx;

    public StationtList(Context ctx)
    {

        mObjCtx = ctx;

        if((mList!= null) && (mList.size() > 0))
        {
            mList  = new ArrayList<String>();
            initAirStationList();
        }
        else{
            mList  = new ArrayList<String>();
            initAirStationList();
        }
    }

    private void initAirStationList()
    {


        mList = new StationListDomParser(mObjCtx).getListFromXML();

//        mList.add("Agartala - IXA");
//        mList.add("Agatti Island - AGX");
//        mList.add("Agra - AGR");
//        mList.add("Ahmedabad - AMD");
//        mList.add("Aizawl - AJL");
//        mList.add("Akola - AKD");
//        mList.add("Allahabad - IXD");
//        mList.add("Along - IXV");
//        mList.add("Amritsar - ATQ");
//        mList.add("Aurangabad - IXU");
//        mList.add("Bagdogra - IXB");
//        mList.add("Balurghat - RGH");
//        mList.add("Bangalore - BLR");
//        mList.add("Bareli - BEK");
//        mList.add("Belgaum - IXG");
//        mList.add("Bellary - BEP");
//        mList.add("Bhatinda - BUP");
//        mList.add("Bhavnagar - BHU");
//        mList.add("Bhopal - BHO");
//        mList.add("Bhubaneswar - BBI");
//        mList.add("Bhuj - BHJ");
//        mList.add("Bikaner - BKB");
//        mList.add("Bilaspur - PAB");
//        mList.add("Bombay (Mumbai) - BOM");
//        mList.add("Calcutta (Kolkata) - CCU");
//        mList.add("Car Nicobar - CBD");
//        mList.add("Chandigarh - IXC");
//        mList.add("Coimbatore - CJB");
//        mList.add("Cooch Behar - COH");
//        mList.add("Cuddapah - CDP");
//        mList.add("Daman - NMB");
//        mList.add("Daparizo - DAE");
//        mList.add("Darjeeling - DAI");
//        mList.add("Dehra Dun - DED");
//        mList.add("Delhi (New Delhi) - DEL");
//        mList.add("Deparizo - DEP");
//        mList.add("Dhanbad - DBD");
//        mList.add("Dharamsala - DHM");
//        mList.add("Dibrugarh - DIB");
//        mList.add("Dimapur - DMU");
//        mList.add("Diu - DIU");
//        mList.add("Durgapur - RDP");
//        mList.add("Gaya - GAY");
//        mList.add("Goa - GOI");
//        mList.add("Gorakhpur - GOP");
//        mList.add("Guna - GUX");
//        mList.add("Guwahati - GAU");
//        mList.add("Gwalior - GWL");
//        mList.add("Hissar - HSS");
//        mList.add("Hubli - HBX");
//        mList.add("Hyderabad - HYD");
//        mList.add("Imphal - IMF");
//        mList.add("Indore - IDR");
//        mList.add("Jabalpur - JLR");
//        mList.add("Jagdalpur - JGB");
//        mList.add("Jaipur - JAI");
//        mList.add("Jaisalmer - JSA");
//        mList.add("Jammu - IXJ");
//        mList.add("Jamnagar - JGA");
//        mList.add("Jamshedpur - IXW");
//        mList.add("Jeypore - PYB");
//        mList.add("Jodhpur - JDH");
//        mList.add("Jorhat - JRH");
//        mList.add("Kailashahar - IXH");
//        mList.add("Kamalpur - IXQ");
//        mList.add("Kandla - IXY");
//        mList.add("Kanpur - KNU");
//        mList.add("Keshod - IXK");
//        mList.add("Khajuraho - HJR");
//        mList.add("Khowai - IXN");
//        mList.add("Kochi - COK");
//        mList.add("Kolhapur - KLH");
//        mList.add("Kota - KTU");
//        mList.add("Kozhikode - CCJ");
//        mList.add("Kulu - KUU");
//        mList.add("Leh - IXL");
//        mList.add("Lilabari - IXI");
//        mList.add("Lucknow - LKO");
//        mList.add("Ludhiana - LUH");
//        mList.add("Madras (Chennai) - MAA");
//        mList.add("Madurai - IXM");
//        mList.add("Malda - LDA");
//        mList.add("Mangalore - IXE");
//        mList.add("Mohanbari - MOH");
//        mList.add("Muzaffarnagar - MZA");
//        mList.add("Muzaffarpur - MZU");
//        mList.add("Mysore - MYQ");
//        mList.add("Nagpur - NAG");
//        mList.add("Nanded - NDC");
//        mList.add("Nasik - ISK");
//        mList.add("Neyveli - NVY");
//        mList.add("Osmanabad - OMN");
//        mList.add("Pantnagar - PGH");
//        mList.add("Pasighat - IXT");
//        mList.add("Pathankot - IXP");
//        mList.add("Patna - PAT");
//        mList.add("Pondicherry - PNY");
//        mList.add("Porbandar - PBD");
//        mList.add("Port Blair - IXZ");
//        mList.add("Pune - PNQ");
//        mList.add("Puttaparthi - PUT");
//        mList.add("Raipur - RPR");
//        mList.add("Rajahmundry - RJA");
//        mList.add("Rajkot - RAJ");
//        mList.add("Rajouri - RJI");
//        mList.add("Ramagundam - RMD");
//        mList.add("Ranchi - IXR");
//        mList.add("Ratnagiri - RTC");
//        mList.add("Rewa - REW");
//        mList.add("Rourkela - RRK");
//        mList.add("Rupsi - RUP");
//        mList.add("Salem - SXV");
//        mList.add("Satna - TNI");
//        mList.add("Shillong - SHL");
//        mList.add("Sholapur - SSE");
//        mList.add("Silchar - IXS");
//        mList.add("Simla - SLV");
//        mList.add("Srinagar - SXR");
//        mList.add("Surat - STV");
//        mList.add("Tezpur - TEZ");
//        mList.add("Tezu - TEI");
//        mList.add("Thanjavur - TJV");
//        mList.add("Thiruvananthapuram - TRV");
//        mList.add("Tiruchirapally - TRZ");
//        mList.add("Tirupati - TIR");
//        mList.add("Tuticorin - TCR");
//        mList.add("Udaipur - UDR");
//        mList.add("Vadodara - BDQ");
//        mList.add("Varanasi - VNS");
//        mList.add("Vijayawada - VGA");
//        mList.add("Visakhapatnam - VTZ");
//        mList.add("Warangal - WGC");
//        mList.add("Zero - ZER");
    }


//    private void initRestStationList()
//    {
//
//    }


}
