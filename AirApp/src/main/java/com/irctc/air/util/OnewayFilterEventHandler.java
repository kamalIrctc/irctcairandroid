package com.irctc.air.util;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.appyvet.rangebar.RangeBar;
import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.adapter.AdapterOneWaySearchResults;
import com.irctc.air.adapter.FilterAirlineAdapter;
import com.irctc.air.fragment.FragmentOneWayFlight;
import com.irctc.air.fragment.FragmentPlanner;
import com.irctc.air.model.FlightFilterBean;
import com.irctc.air.model.FlightOnWardDetailBean;
import com.irctc.air.model.InnerFlightDetailsBeans;
import com.irctc.air.model.search_result_one_way.Flights;
import com.irctc.air.util.sorting.SortByFarePrice;
import com.irctc.air.util.sorting.SortByFlightName;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;


/**
 * Created by tourism on 5/31/2016.
 */
public class OnewayFilterEventHandler implements View.OnClickListener, RangeBar.OnRangeBarChangeListener, DialogInterface.OnKeyListener {

    ActivityMain mainActivity;
    FlightFilterBean filterBean;
    Dialog view;

    CheckBox zeroStopsCheckBox;
    CheckBox oneStopsCheckBox;
    CheckBox twoStopsMoreCheckBox;

    CheckBox chBoxfilterOnward_1;
    CheckBox chBoxfilterOnward_2;
    CheckBox chBoxfilterOnward_3;
    CheckBox chBoxfilterOnward_4;

    private RangeBar roundRangeBar;

    private TextView minSelectedFlightFare;
    private TextView maxSelectedFlightFare;
    private TextView minFlightFare;
    private TextView maxFlightFare;

    ArrayList<FlightOnWardDetailBean> specificFlight;
    ListView listViewRouOnwGdsFlight;

    FragmentOneWayFlight roundTrip;

    /*private int minRound;
    private int maxReturn;*/

    int minFilterPrice = 0, maxFilterPrice = 0;
    public static ArrayList<Flights> filteredFlightsArrayList;
    ArrayList<FlightOnWardDetailBean> mSpecificCloneOfClone = new ArrayList<>();

    ListView filterPreferFlightsListView;
    private FilterAirlineAdapter mAdapter;

    ArrayList<Flights> flightsArrayList;
    public static ArrayList<String> filteredAirlines = new ArrayList<>();
    Dialog dialog;
    TextView onwFlightSearcFlightPrice;
    public static Flights selectedFlight;

    public interface AfterApply {
        void doPerform(ArrayList<FlightOnWardDetailBean> onwrdClone);
    }

    public OnewayFilterEventHandler(Dialog dialog, ArrayList<Flights> flightsArrayList, FragmentOneWayFlight fragmentRoundTrip, ActivityMain mainActivity, Dialog lObjDialogShowFilterOption, ArrayList<FlightOnWardDetailBean> specificFlight, ListView listRouOnwFlight, FlightFilterBean filterBean, TextView totalPrice) {

        // USE FOR THE ONE LIST VIEW
        this.dialog = dialog;
        this.flightsArrayList = flightsArrayList;
        this.specificFlight = specificFlight;
        this.mainActivity = mainActivity;
        this.filterBean = filterBean;
        this.onwFlightSearcFlightPrice = totalPrice;
        view = lObjDialogShowFilterOption;
        initializeDialogVariable(view);
        roundTrip = fragmentRoundTrip;
        this.listViewRouOnwGdsFlight = listRouOnwFlight;
        mSpecificCloneOfClone.addAll(specificFlight);

        //FragmentOneWayFlight.checkStatus=null;
        makeAirlinesList();
        setPreselected();
    }

    private void setPreselected() {
        if (Pref.getBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_CHECK_1)) {
            chBoxfilterOnward_1.setChecked(true);
        } else {
            chBoxfilterOnward_1.setChecked(false);
        }
        if (Pref.getBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_CHECK_2)) {
            chBoxfilterOnward_2.setChecked(true);
        } else {
            chBoxfilterOnward_2.setChecked(false);
        }
        if (Pref.getBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_CHECK_3)) {
            chBoxfilterOnward_3.setChecked(true);
        } else {
            chBoxfilterOnward_3.setChecked(false);
        }
        if (Pref.getBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_CHECK_4)) {
            chBoxfilterOnward_4.setChecked(true);
        } else {
            chBoxfilterOnward_4.setChecked(false);
        }

        if (Pref.getBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_STOP_1)) {
            zeroStopsCheckBox.setChecked(true);
        } else {
            zeroStopsCheckBox.setChecked(false);
        }

        if (Pref.getBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_STOP_2)) {
            oneStopsCheckBox.setChecked(true);
        } else {
            oneStopsCheckBox.setChecked(false);
        }

        if (Pref.getBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_STOP_3)) {
            twoStopsMoreCheckBox.setChecked(true);
        } else {
            twoStopsMoreCheckBox.setChecked(false);
        }
        initRangebar();
    }

    @Override
    public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {

            mainActivity.isComingFromRoundFilterDialog = true;
            view.cancel();

            specificFlight.clear();
            specificFlight.addAll(mSpecificCloneOfClone);
            listViewRouOnwGdsFlight.invalidateViews();
            roundTrip.doPerform(specificFlight);

        }
        return true;
    }

    public static void updateListViewHeight(ListView myListView) {
        ListAdapter myListAdapter = myListView.getAdapter();
        if (myListAdapter == null) {
            return;
        }
        //get listview height
        int totalHeight = 0;
        int adapterCount = myListAdapter.getCount();
        for (int size = 0; size < adapterCount; size++) {
            View listItem = myListAdapter.getView(size, null, myListView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        //Change Height of ListView
        ViewGroup.LayoutParams params = myListView.getLayoutParams();
        params.height = totalHeight + (myListView.getDividerHeight() * (adapterCount - 1));
        myListView.setLayoutParams(params);
    }

    private void initializeDialogVariable(Dialog view) {

        LinearLayout lLReset = (LinearLayout) view.findViewById(R.id.FILTER_RESET_LAY);

        zeroStopsCheckBox = (CheckBox) view.findViewById(R.id.filterCkBoxZeroStops);
        oneStopsCheckBox = (CheckBox) view.findViewById(R.id.filterCkBoxOneStops);
        twoStopsMoreCheckBox = (CheckBox) view.findViewById(R.id.filterCkBoxMoreStops);

        chBoxfilterOnward_1 = (CheckBox) view.findViewById(R.id.chBoxfilterOnward_1);
        chBoxfilterOnward_2 = (CheckBox) view.findViewById(R.id.chBoxfilterOnward_2);
        chBoxfilterOnward_3 = (CheckBox) view.findViewById(R.id.chBoxfilterOnward_3);
        chBoxfilterOnward_4 = (CheckBox) view.findViewById(R.id.chBoxfilterOnward_4);

        minSelectedFlightFare = (TextView) view.findViewById(R.id.txtSelectedFlightFareMin);
        maxSelectedFlightFare = (TextView) view.findViewById(R.id.txtSelectedFlightFareMax);
        minFlightFare = (TextView) view.findViewById(R.id.txtFlightFareMin);
        maxFlightFare = (TextView) view.findViewById(R.id.txtFlightFareMax);

        roundRangeBar = (RangeBar) view.findViewById(R.id.rangebarPrice);
        roundRangeBar.setBarColor(Color.parseColor("#9DA2A3"));
        roundRangeBar.setPinColor(Color.parseColor("#21A1CE")); // change the background color of pin like drop
        roundRangeBar.setPinRadius(30);// change the radious of pin like drop
        roundRangeBar.setTickColor(Color.TRANSPARENT); // background color line
        roundRangeBar.setConnectingLineColor(Color.parseColor("#21A1CE"));
        roundRangeBar.setSelectorColor(Color.parseColor("#4F565E"));//
        roundRangeBar.setConnectingLineWeight(1);

        initRangebar();
        final ScrollView scrollView = (ScrollView) view.findViewById(R.id.scrollView);
        /*scrollView.postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0,0);
            }
        }, 200);*/
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0, 0);
            }
        });

        filterPreferFlightsListView = (ListView) view.findViewById(R.id.filterPreferFlightsListView);


        Button btnFilterApply = (Button) view.findViewById(R.id.filterBtnApply);

        zeroStopsCheckBox.setOnClickListener(this);
        oneStopsCheckBox.setOnClickListener(this);
        twoStopsMoreCheckBox.setOnClickListener(this);

        chBoxfilterOnward_1.setOnClickListener(this);
        chBoxfilterOnward_2.setOnClickListener(this);
        chBoxfilterOnward_3.setOnClickListener(this);
        chBoxfilterOnward_4.setOnClickListener(this);

        btnFilterApply.setOnClickListener(this);
        lLReset.setOnClickListener(this);
        view.setOnKeyListener(this);

      /*  mAdapter = new DFilterAirlineAdapter(mainActivity,AirDataHolder.getListHolder().getList().get(0).getCheapestFlightFare(),filterBean.getPreferredAirline()); //getAirlineData(airlinesImage,airlines,flightAirCode)
        filterPreferFlightsListView.setAdapter(mAdapter);
        //updateListViewHeight(filterPreferFlightsListView);
        // it is used to provide previous selection
        setListViewHeightBasedOnChildren(filterPreferFlightsListView);
*/      //  providePreviousSelection();
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    private void providePreviousSelection() {

        if (filterBean.getStopZero() != 0) zeroStopsCheckBox.setChecked(true);
        if (filterBean.getStopOne() != 0) oneStopsCheckBox.setChecked(true);
        if (filterBean.getStopTwoMore() != 0) twoStopsMoreCheckBox.setChecked(true);

        if (filterBean.getOnwardOption1() != 0) chBoxfilterOnward_1.setChecked(true);
        if (filterBean.getOnwardOption2() != 0) chBoxfilterOnward_2.setChecked(true);
        if (filterBean.getOnwardOption3() != 0) chBoxfilterOnward_3.setChecked(true);
        if (filterBean.getOnwardOption4() != 0) chBoxfilterOnward_4.setChecked(true);

        if (filterBean.getPreferredAirline() != null) {


        }
    }

    // USE TO FILTER GDS FLIGHTS
    private ArrayList<FlightOnWardDetailBean> filterBasedOnStopsOnewayFlight(ArrayList<FlightOnWardDetailBean> onewayFlightClone) {

        ArrayList<FlightOnWardDetailBean> al = new ArrayList<>();
        al.addAll(onewayFlightClone);

        onewayFlightClone.clear();

        if (filterBean.getStopZero() != 0 || filterBean.getStopOne() != 0 || filterBean.getStopTwoMore() != 0) {

            for (int i = 0; i < al.size(); i++) {

                FlightOnWardDetailBean flightDetailBean = al.get(i);

                ArrayList<InnerFlightDetailsBeans> flight = flightDetailBean.getFlight();

                if (flight.size() > 0) { //3

                    if (filterBean.getStopZero() != 0) { // S

                        if (flight.size() == 1) {  // Add flight to clone
                            onewayFlightClone.add(flightDetailBean);
                            continue;
                        }
                    }
                    if (filterBean.getStopOne() != 0) { // S
                        if (flight.size() == 2) {  // Add flight to clone
                            onewayFlightClone.add(flightDetailBean);
                            continue;
                        }
                    }
                    if (filterBean.getStopTwoMore() != 0) { // S
                        if (flight.size() > 2) {  // Add flight to clone
                            onewayFlightClone.add(flightDetailBean);
                            continue;
                        }
                    }
                }
            }
        }


        if ((onewayFlightClone.size() > 0) && (filterBean.getStopZero() != 0 || filterBean.getStopOne() != 0 || filterBean.getStopTwoMore() != 0)) {

        } else {
            if ((filterBean.getStopZero() != 0 || filterBean.getStopOne() != 0 || filterBean.getStopTwoMore() != 0)) {

            } else {
                specificFlight = (ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().clone();
            }
        }

        if (filterBean.getOnwardOption1() != 0 || filterBean.getOnwardOption2() != 0 || filterBean.getOnwardOption3() != 0 || filterBean.getOnwardOption4() != 0) {

            if (specificFlight.size() > 0) {
                // filter the clone data on behalf of Onwards time
                filterBasedOnOnwardsTime();
            }
        }

        if (specificFlight.size() > 0) {
            if (filterBean.getIsChange() != 0) {
                // filter the clone data on behalf of fare Price
                AppLogger.enable();
                AppLogger.e("MIN Avl=> ", "" + filterBean.getMinPrice());
                AppLogger.e("MAAX Avl=> ", "" + filterBean.getMaxPrice());
                onewayFlightClone = filterBasedOnPrice(specificFlight);
            }
        }

        /*if (specificFlight.size() > 0) {
            if (filterBean.getPreferredAirline().size() > 0) {
                // filter the clone data on behalf of Airline Preferred
                onewayFlightClone = filterBasedOnAirlines(specificFlight);
            }
        }*/

        AppLogger.enable();
        AppLogger.e("Flight SIZE =>", "" + onewayFlightClone.size());

        // Show alert if filtered list is zoro and provide new clone with all data
        if (onewayFlightClone.size() == 0) {
//            mAirFlightMainHolderClone = (ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().clone();
//            new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.FLIGHT_FILTER_SEARCH_ERROR_TEXT), mainActivity.getResources().getString(R.string.FLIGHT_SEARCH_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE, null).generateAlert();
        }

        return onewayFlightClone;
    }

    private ArrayList<FlightOnWardDetailBean> filterBasedOnPrice(ArrayList<FlightOnWardDetailBean> flightClone) {

        ArrayList<FlightOnWardDetailBean> al = new ArrayList<>();
        al.addAll(flightClone);
        flightClone.clear();

        for (int i = 0; i < al.size(); i++) {

            FlightOnWardDetailBean flightDetailBean = al.get(i);

            if (flightDetailBean.getFlightFare() >= filterBean.getMinPrice() && flightDetailBean.getFlightFare() <= filterBean.getMaxPrice()) {

                flightClone.add(flightDetailBean);
            }
        }
        return flightClone;
    }

   /* private ArrayList<FlightOnWardDetailBean> filterBasedOnAirlines(ArrayList<FlightOnWardDetailBean> flightClone) {

        ArrayList<FlightOnWardDetailBean> al = new ArrayList<>();
        al.addAll(flightClone);
        flightClone.clear();

        for (int i = 0; i < al.size() ; i++) {

            FlightOnWardDetailBean flightDetailBean =  al.get(i);
            String ailline = flightDetailBean.getFlightAirline();

            if(getPreferredAirline().size() > 0){

                if (filterBean.getPreferredAirline().contains(ailline)) {
                    flightClone.add(flightDetailBean);
                }
            }
        }
        return flightClone;
    }*/

    private void filterBasedOnOnwardsTime() {

        ArrayList<FlightOnWardDetailBean> filteredbasedQnw = new ArrayList<FlightOnWardDetailBean>();

        int length = specificFlight.size();
        for (int i = 0; i < length; i++) {

            FlightOnWardDetailBean flighBeans = specificFlight.get(i);
            Date dateX = DateUtility.getBeforeAfterTime(flighBeans.getFlight().get(0).getFlightDepartureTime(), 0, 0);

            if (filterBean.getOnwardOption1() != 0) {     // S

                Date date1 = DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(), 0, 1);
                Date date2 = DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(), 6, 1);

                if (compareDateTime(date1, date2, dateX)) {

                    filteredbasedQnw.add(flighBeans);
                }
            }
            if (filterBean.getOnwardOption2() != 0) { // S

                Date date1 = DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(), 6, 1);
                Date date2 = DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(), 12, 1);

                if (compareDateTime(date1, date2, dateX)) {

                    filteredbasedQnw.add(flighBeans);

                }
            }
            if (filterBean.getOnwardOption3() != 0) {  // S
                Date date1 = DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(), 12, 1);
                Date date2 = DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(), 18, 1);

                if (compareDateTime(date1, date2, dateX)) {

                    filteredbasedQnw.add(flighBeans);
                }

            }
            if (filterBean.getOnwardOption4() != 0) {  // S

                Date date1 = DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(), 18, 1);
                Date date2 = DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(), 24, 1);

                if (compareDateTime(date1, date2, dateX)) {

                    filteredbasedQnw.add(flighBeans);

                }
            }
        }
        specificFlight.clear();
        specificFlight.addAll(filteredbasedQnw);
    }

    private boolean compareDateTime(Date date1, Date date2, Date dateX) {

        try {

            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(date1);

            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(date2);

            Calendar calendar3 = Calendar.getInstance();
            calendar3.setTime(dateX);

            Date x = calendar3.getTime();

            if (((x.getTime() == calendar1.getTime().getTime()) || (x.getTime() == calendar1.getTime().getTime())) || (x.after(calendar1.getTime()) && x.before(calendar2.getTime()))) {
                //checkes whether the current time is between 14:49:00 and 20:11:13.
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private void performFilterOPt() {

        specificFlight = filterBasedOnStopsOnewayFlight(specificFlight);
        listViewRouOnwGdsFlight.invalidateViews();
    }

    @Override
    public void onClick(View v) {

        /************************************
         * * CLICK HANDLING FOR FILTER DIALOG
         ************************************/

        switch (v.getId()) {

            // FOR STOPS HANDLING

            case R.id.filterCkBoxZeroStops:

                if (zeroStopsCheckBox.isChecked()) {
                    Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_STOP_1, true);
                    filterBean.setStopZero(1);
                } else {
                    Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_STOP_1, false);
                    filterBean.setStopZero(0);
                }

                break;

            case R.id.filterCkBoxOneStops:
                if (oneStopsCheckBox.isChecked()) {
                    Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_STOP_2, true);
                    filterBean.setStopOne(1);
                } else {
                    Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_STOP_2, false);
                    filterBean.setStopOne(0);
                }

                break;

            case R.id.filterCkBoxMoreStops:

                if (twoStopsMoreCheckBox.isChecked()) {
                    Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_STOP_3, true);
                    filterBean.setStopTwoMore(1);
                } else {
                    Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_STOP_3, false);
                    filterBean.setStopTwoMore(0);
                }

                break;

            // for onwardTime handeling
            case R.id.chBoxfilterOnward_1:

                if (chBoxfilterOnward_1.isChecked()) {
                    Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_CHECK_1, true);
                    filterBean.setOnwardOption1(1);
                } else {
                    Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_CHECK_1, false);
                    filterBean.setOnwardOption1(0);
                }
                break;

            case R.id.chBoxfilterOnward_2:

                if (chBoxfilterOnward_2.isChecked()) {
                    Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_CHECK_2, true);
                    filterBean.setOnwardOption2(1);
                } else {
                    Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_CHECK_2, false);
                    filterBean.setOnwardOption2(0);
                }

                break;
            case R.id.chBoxfilterOnward_3:

                if (chBoxfilterOnward_3.isChecked()) {
                    Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_CHECK_3, true);
                    filterBean.setOnwardOption3(1);
                } else {
                    Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_CHECK_3, false);
                    filterBean.setOnwardOption3(0);
                }

                break;
            case R.id.chBoxfilterOnward_4:

                if (chBoxfilterOnward_4.isChecked()) {
                    Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_CHECK_4, true);
                    filterBean.setOnwardOption4(1);
                } else {
                    Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_CHECK_2, false);
                    filterBean.setOnwardOption4(0);
                }

                break;


            case R.id.filterBtnApply:
                filteredFlightsArrayList = new ArrayList<>();
                for (Flights flight : flightsArrayList) {
                    if (Integer.parseInt(flight.getPrice()) >= minFilterPrice && Integer.parseInt(flight.getPrice()) <= maxFilterPrice) {
                        if (filteredAirlines.size() > 0) {
                            for (String airline : filteredAirlines) {
                                if (airline.equals(flight.getCarrierName())) {
                                    if (chBoxfilterOnward_1.isChecked() || chBoxfilterOnward_2.isChecked() || chBoxfilterOnward_3.isChecked() || chBoxfilterOnward_4.isChecked()) {

                                        if (chBoxfilterOnward_1.isChecked() && Float.parseFloat(flight.getDepartureTime().replace(":", ".")) <= 6) {
                                            checkStops(flight);
                                        }
                                        if (chBoxfilterOnward_2.isChecked() && Float.parseFloat(flight.getDepartureTime().replace(":", ".")) >= 6 && chBoxfilterOnward_2.isChecked() && Float.parseFloat(flight.getDepartureTime().replace(":", ".")) <= 12) {
                                            checkStops(flight);
                                        }
                                        if (chBoxfilterOnward_3.isChecked() && Float.parseFloat(flight.getDepartureTime().replace(":", ".")) >= 12 && chBoxfilterOnward_3.isChecked() && Float.parseFloat(flight.getDepartureTime().replace(":", ".")) <= 18) {
                                            checkStops(flight);
                                        }
                                        if (chBoxfilterOnward_4.isChecked() && Float.parseFloat(flight.getDepartureTime().replace(":", ".")) >= 18) {
                                            checkStops(flight);
                                        }

                                    } else {
                                        checkStops(flight);
                                    }
                                }
                            }
                        } else {
                            if (chBoxfilterOnward_1.isChecked() || chBoxfilterOnward_2.isChecked() || chBoxfilterOnward_3.isChecked() || chBoxfilterOnward_4.isChecked()) {
                                if (chBoxfilterOnward_1.isChecked() && Float.parseFloat(flight.getDepartureTime().replace(":", ".")) <= 6) {
                                    checkStops(flight);
                                }
                                if (chBoxfilterOnward_2.isChecked() && Float.parseFloat(flight.getDepartureTime().replace(":", ".")) >= 6 && chBoxfilterOnward_2.isChecked() && Float.parseFloat(flight.getDepartureTime().replace(":", ".")) <= 12) {
                                    checkStops(flight);
                                }
                                if (chBoxfilterOnward_3.isChecked() && Float.parseFloat(flight.getDepartureTime().replace(":", ".")) >= 12 && chBoxfilterOnward_3.isChecked() && Float.parseFloat(flight.getDepartureTime().replace(":", ".")) <= 18) {
                                    checkStops(flight);
                                }
                                if (chBoxfilterOnward_4.isChecked() && Float.parseFloat(flight.getDepartureTime().replace(":", ".")) >= 18) {
                                    checkStops(flight);
                                }
                            } else {
                                checkStops(flight);
                            }
                        }
                    }
                }
                if (filteredFlightsArrayList.size() > 0) {
                    mainActivity.isComingFromFilterFrag = true;
                    FragmentOneWayFlight.flightListView.setAdapter(new AdapterOneWaySearchResults(mainActivity, filteredFlightsArrayList));
                    FragmentOneWayFlight.flightListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            onwFlightSearcFlightPrice.setText(mainActivity.getString(R.string.symbol_rs) + " " + filteredFlightsArrayList.get(position).getPrice());
                            selectedFlight = filteredFlightsArrayList.get(position);
                        }
                    });
                    selectedFlight = filteredFlightsArrayList.get(0);
                    onwFlightSearcFlightPrice.setText(mainActivity.getString(R.string.symbol_rs) + " " + filteredFlightsArrayList.get(0).getPrice());
                    FragmentOneWayFlight.flightListView.setItemChecked(0, true);
                    dialog.dismiss();
                } else {
                    Toast.makeText(mainActivity, "No flights Available in this selection", Toast.LENGTH_SHORT).show();
                }
/*
                filterBean.setPreferredAirline(getPreferredAirline());
                mainActivity.isComingFromFilterFrag = true;
                performFilterOPt();

                if(specificFlight.size() > 0){
                    view.cancel();
                    roundTrip.doPerform(specificFlight);

                }else{

                    specificFlight.clear();
                    specificFlight.addAll(mSpecificCloneOfClone);
                    new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.FLIGHT_FILTER_SEARCH_ERROR_TEXT), mainActivity.getResources().getString(R.string.FLIGHT_SEARCH_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE, null).generateAlert();
                }*/
                break;
            case R.id.FILTER_RESET_LAY:

                resetAllFields();
                break;

            default:
                break;

        }
    }

    private void checkStops(Flights flight) {
        if (zeroStopsCheckBox.isChecked() || oneStopsCheckBox.isChecked() || twoStopsMoreCheckBox.isChecked()) {
            int stops = Integer.parseInt(flight.getStops());
            if (zeroStopsCheckBox.isChecked() && stops == 0) {
                filteredFlightsArrayList.add(flight);
            }
            if (oneStopsCheckBox.isChecked() && stops == 1) {
                filteredFlightsArrayList.add(flight);
            }
            if (twoStopsMoreCheckBox.isChecked() && stops >= 2) {
                filteredFlightsArrayList.add(flight);
            }
        } else {
            filteredFlightsArrayList.add(flight);
        }

    }

    /*private ArrayList<String> getPreferredAirline() {

        ArrayList<String> lAlAirline = new ArrayList<>();

        for (int i = 0; i< mAdapter.checkedItemList.size(); i++) {

            lAlAirline.add(mAdapter.checkedItemList.get(i).getmFlightCode());

        }
        return lAlAirline;
    }*/

    private void resetAllFields() {

        if (zeroStopsCheckBox.isChecked()) {
            filterBean.setStopOne(0);
            zeroStopsCheckBox.setChecked(false);
        }
        if (oneStopsCheckBox.isChecked()) {
            filterBean.setStopOne(0);
            oneStopsCheckBox.setChecked(false);
        }
        if (twoStopsMoreCheckBox.isChecked()) {
            filterBean.setStopTwoMore(0);
            twoStopsMoreCheckBox.setChecked(false);
        }
        if (chBoxfilterOnward_1.isChecked()) {
            filterBean.setReturnOption1(0);
            chBoxfilterOnward_1.setChecked(false);
        }
        if (chBoxfilterOnward_2.isChecked()) {
            filterBean.setReturnOption2(0);
            chBoxfilterOnward_2.setChecked(false);
        }
        if (chBoxfilterOnward_3.isChecked()) {
            filterBean.setReturnOption3(0);
            chBoxfilterOnward_3.setChecked(false);
        }
        if (chBoxfilterOnward_4.isChecked()) {
            filterBean.setReturnOption4(0);
            chBoxfilterOnward_4.setChecked(false);
        }
        filterBean.setIsChange(0);

        initRangebar();

        /*if(filterBean.getIsChange() == 1){

            filterBean.setIsChange(0);
            filterBean.setMaxPrice(maxReturn);
            filterBean.setMinPrice(minRound);

            initRangebar();
            roundRangeBar.setTickEnd(maxReturn);
            roundRangeBar.setTickStart(minRound);
            roundRangeBar.setTickInterval(1);

            roundRangeBar.setRangePinsByValue(minFilterPrice, maxFilterPrice);
            minSelectedFlightFare.setText("\u20B9  " + minFilterPrice);
            maxSelectedFlightFare.setText("\u20B9  " + maxFilterPrice);
        }*/

        filterBean.getPreferredAirline().clear();
        filterPreferFlightsListView.invalidateViews();
        FragmentOneWayFlight.checkStatus = null;
        filteredAirlines.clear();

        Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_CHECK_1, false);
        Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_CHECK_2, false);
        Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_CHECK_3, false);
        Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_CHECK_4, false);

        Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_STOP_1, false);
        Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_STOP_2, false);
        Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_STOP_3, false);
        makeAirlinesList();
        Toast.makeText(mainActivity.getApplicationContext(), "All values have been reset", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
        minFilterPrice = Integer.parseInt(rangeBar.getLeftPinValue());
        maxFilterPrice = Integer.parseInt(rangeBar.getRightPinValue());
        filterBean.setIsChange(1);
        filterBean.setMaxPrice(Integer.parseInt(rangeBar.getRightPinValue()));
        filterBean.setMinPrice(Integer.parseInt(rangeBar.getLeftPinValue()));
        minSelectedFlightFare.setText("\u20B9  " + Integer.parseInt(rangeBar.getLeftPinValue()));
        maxSelectedFlightFare.setText("\u20B9  " + Integer.parseInt(rangeBar.getRightPinValue()));
    }

    private void initRangebar() {

        /*roundRangeBar = (RangeBar) view.findViewById(R.id.rangebarPrice);
        roundRangeBar.setBarColor(Color.parseColor("#9DA2A3"));
        roundRangeBar.setPinColor(Color.parseColor("#21A1CE")); // change the background color of pin like drop
        roundRangeBar.setPinRadius(30);// change the radious of pin like drop
        roundRangeBar.setTickColor(Color.TRANSPARENT); // background color line
        roundRangeBar.setConnectingLineColor(Color.parseColor("#21A1CE"));
        roundRangeBar.setSelectorColor(Color.parseColor("#4F565E"));//
        roundRangeBar.setConnectingLineWeight(1);*/

        //assignMinMaxPriceGDS();

        Collections.sort(flightsArrayList, new SortByFarePrice());
        int minRound = Integer.parseInt(flightsArrayList.get(0).getPrice());
        int maxReturn = Integer.parseInt(flightsArrayList.get(flightsArrayList.size() - 1).getPrice());



        /*if (minFilterPrice == 0) minFilterPrice = minRound;
        else minFilterPrice = filterBean.getMinPrice();
        if (maxFilterPrice == 0) maxFilterPrice = maxReturn;
        else maxFilterPrice = filterBean.getMaxPrice();*/

        if (filterBean.getIsChange() == 1) {
            minFilterPrice = filterBean.getMinPrice();
            maxFilterPrice = filterBean.getMaxPrice();
        } else {
            minFilterPrice = minRound;
            maxFilterPrice = maxReturn;
        }

        roundRangeBar.setTickEnd(maxReturn);
        roundRangeBar.setTickStart(minRound);
        roundRangeBar.setTickInterval(1);
        roundRangeBar.setRangePinsByValue(minFilterPrice, maxFilterPrice);

        minFlightFare.setText("\u20B9  " + minFilterPrice);
        maxFlightFare.setText("\u20B9  " + maxFilterPrice);

        roundRangeBar.setPinTextFormatter(new RangeBar.PinTextFormatter() {
            @Override
            public String getText(String value) {
                return value;
            }
        });

        roundRangeBar.setOnRangeBarChangeListener(this);
        /*if (maxReturn - minRound > 2) {
            roundRangeBar.setTickEnd(maxReturn);
            roundRangeBar.setTickStart(minRound);
            roundRangeBar.setTickInterval(1);
        } else {
            roundRangeBar.setEnabled(false);
            minSelectedFlightFare.setText("\u20B9  " + minRound);
            maxSelectedFlightFare.setText("\u20B9  " + maxReturn);
        }
        minFlightFare.setText("\u20B9  " + minRound);
        maxFlightFare.setText("\u20B9  " + maxReturn);*/
    }

    private void assignMinMaxPriceGDS() {
        /*Collections.sort(flightsArrayList, new SortByFarePrice());
        minRound = Integer.parseInt(flightsArrayList.get(0).getPrice());
        maxReturn = Integer.parseInt(flightsArrayList.get(flightsArrayList.size() - 1).getPrice());
        minFilterPrice = minRound;
        maxFilterPrice = maxReturn;*/
    }

    private void makeAirlinesList() {
        LinkedHashMap<String, String> airlinesHashMap = new LinkedHashMap<>();
        LinkedHashMap<String, String> airlinesIconHashMap = new LinkedHashMap<>();
        ArrayList<Flights> tempFlightsArrayList = new ArrayList<>();
        tempFlightsArrayList.addAll(flightsArrayList);

        Collections.sort(tempFlightsArrayList, new SortByFlightName());
        //Collections.sort(flightsArrayList, new SortByFlightName());
        for (Flights flight : tempFlightsArrayList) {
            if (airlinesHashMap.containsKey(flight.getCarrierName())) {
                if (Integer.parseInt(airlinesHashMap.get(flight.getCarrierName())) >= Integer.parseInt(flight.getPrice())) {
                    airlinesHashMap.put(flight.getCarrierName(), flight.getPrice());
                    airlinesIconHashMap.put(flight.getCarrierName(), flight.getCarrier());
                }
            } else {
                airlinesHashMap.put(flight.getCarrierName(), flight.getPrice());
                airlinesIconHashMap.put(flight.getCarrierName(), flight.getCarrier());
            }
        }
        ArrayList<String> airlines = new ArrayList<>(airlinesHashMap.keySet());
        ArrayList<String> airFare = new ArrayList<>(airlinesHashMap.values());
        ArrayList<String> airlineIcons = new ArrayList<>(airlinesIconHashMap.values());

        //FragmentOneWayFlight.checkStatus=null;
        mAdapter = new FilterAirlineAdapter(mainActivity, airlines, airFare, airlineIcons);
        filterPreferFlightsListView.setAdapter(mAdapter);
        //updateListViewHeight(filterPreferFlightsListView);
        // it is used to provide previous selection
        setListViewHeightBasedOnChildren(filterPreferFlightsListView);

    }
}
