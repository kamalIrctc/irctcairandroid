package com.irctc.air.util;

import com.irctc.air.model.PassDetailbean;
import com.irctc.air.model.PassengerDetailBean;
import com.irctc.air.model.book_ticket.PassengerDetails;

import java.util.Comparator;

/**
 * Created by tourism on 4/13/2016.
 */
public class PassengerFirstNameSort implements Comparator<PassengerDetails>{

    @Override
    public int compare(PassengerDetails obj1, PassengerDetails obj2) {
        String farePriceOne = obj1.getFirstName();
        String farePriceTwo = obj2.getFirstName();
        return farePriceOne.compareToIgnoreCase(farePriceTwo);
    }
}
