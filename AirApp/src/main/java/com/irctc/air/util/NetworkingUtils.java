package com.irctc.air.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

/**
 * Created by Rajnikant Kumar on 8/23/2018.
 */

public class NetworkingUtils {

    //region : Msg for Internet not working
    public static void noInternetAccess(Context context){
        Toast.makeText(context,"No Internet Access!",Toast.LENGTH_SHORT).show();
    }
    //endregion

    //region : Msg for Flights not Available
    public static void noFlightsAvailable(Context context){
        Toast.makeText(context,"No Flights Available!",Toast.LENGTH_SHORT).show();
    }
    //endregion

    //region : Msg for Internet not working
    public static void internalServerIssue(Context context){
        Toast.makeText(context,"Internal Server Issue!",Toast.LENGTH_SHORT).show();
    }
    //endregion

    public static ProgressDialog progressDialog;
    //region : Show Progress
    public static void showProgress(Activity activity){
     if(progressDialog==null){
     progressDialog = new ProgressDialog(activity);
     progressDialog.setMessage("Loading, Please Wait...");
     progressDialog.setCancelable(false);
     }
     progressDialog.show();
    }
    //endregion

    //region : Hide Progress
    public static void dismissProgress(){
       if(progressDialog!=null){
            if(progressDialog.isShowing()){
            progressDialog.dismiss();
            }
        }
    }
    //endregion
}
