package com.irctc.air.session;

import android.content.Context;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.irctc.air.Database.AirDatabase;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.model.login.PojoLoginUser;
import com.irctc.air.model.refresh_token.RefreshTokenModel;
import com.irctc.air.networking.Networking;
import com.irctc.air.util.NetworkingUtils;

import org.json.JSONObject;

/**
 * Created by Rajnikant Kumar on 12/22/2018.
 */

public class Waiter extends Thread {
    private static final String TAG = Waiter.class.getName();
    public static long lastUsed;
    private long period;
    public boolean stop;

    public Waiter(long period) {
        this.period = period;
        stop = false;
       // ActivityMain.isSessionExpired = false;
    }

    public void run() {
        long idle = 0;
        this.touch();
        do {
            idle = System.currentTimeMillis() - lastUsed;
            Log.d(TAG, "Application is idle for " + idle + " ms " + System.currentTimeMillis() + ", " + lastUsed);
            try {
                //refreshToken();
                Thread.sleep(2 * 60 * 1000); //check every 5 seconds
            } catch (InterruptedException e) {
                Log.d(TAG, "Waiter interrupted!");
            }
            if (idle > period) {
                stop = true;
               // ActivityMain.isSessionExpired = true;
                idle = 0;
                Log.e("Ideal time expired", "Expired");

                //do something here - e.g. call popup or so
            }

        } while (!stop);
        Log.d(TAG, "Finishing Waiter thread");
    }

    public synchronized void touch() {
        lastUsed = System.currentTimeMillis();
    }

    public synchronized void forceInterrupt() {
        this.interrupt();
    }

    //soft stopping of thread
    /*public synchronized void stop()
    {
        stop=true;
    }*/


    public synchronized void setPeriod(long period) {
        this.period = period;
    }

    private void refreshToken() {
        final AirDatabase airDatabase = new AirDatabase(ActivityMain.context);
        String authToken = airDatabase.getAuthToken();

        Networking.refreshToken(authToken, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("Res", response.toString());
                Gson gson = new Gson();
                RefreshTokenModel tokenModel = gson.fromJson(response.toString(), RefreshTokenModel.class);
                //NetworkingUtils.dismissProgress();
                if (tokenModel.getStatus().equals("SUCCESS")) {
                    //AirDatabase airDatabase = new AirDatabase(ActivityMain.context);
                    airDatabase.removeAuthToken();
                    airDatabase.insertAuthToken(tokenModel.getData());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error Res", "Error: " + error.getMessage());
            }
        });
    }

}