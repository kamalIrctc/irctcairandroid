package com.irctc.air.round.trip.international.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appyvet.rangebar.RangeBar;
import com.google.gson.Gson;
import com.irctc.air.AppController;
import com.irctc.air.Database.SharedPrefrenceAir;
import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.adapter.AdapterOneWayFlightInfo;
import com.irctc.air.adapter.AdapterRoundTripInternationalListView;
import com.irctc.air.adapter.AdapterRoundTripOnwardFlight;
import com.irctc.air.adapter.GdsFlightInfoAdapter;
import com.irctc.air.fragment.FragmentPlanner;
import com.irctc.air.header.AirHeader;
import com.irctc.air.model.gst.ModelGstDetails;
import com.irctc.air.model.reprice_one_way.Data;
import com.irctc.air.model.reprice_one_way.PojoOneWayReprice;
import com.irctc.air.model.route_list.ModelRoutListItem;
import com.irctc.air.model.search_result_round_trip.IntlFlights;
import com.irctc.air.model.search_result_round_trip.LstFlightDetails;
import com.irctc.air.networking.Networking;
import com.irctc.air.round.trip.domastic.adapter.AdapterSpecialFareGds;
import com.irctc.air.round.trip.domastic.adapter.DFilterAirlineAdapter;
import com.irctc.air.round.trip.domastic.adapter.OnwardFlight;
import com.irctc.air.round.trip.domastic.model.Flights;
import com.irctc.air.round.trip.domastic.model.LstBaggageDetails;
import com.irctc.air.round.trip.domastic.sorting.DSortByFarePrice;
import com.irctc.air.round.trip.international.adapter.IFilterAirlineAdapter;
import com.irctc.air.round.trip.international.sorting.ISortByDepart;
import com.irctc.air.round.trip.international.sorting.ISortByDuration;
import com.irctc.air.round.trip.international.sorting.ISortByFarePrice;
import com.irctc.air.util.Constant;
import com.irctc.air.util.DateUtility;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.ListViewInsideScrollView;
import com.irctc.air.util.NetworkingUtils;
import com.irctc.air.util.ProjectUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;

/**
 * Created by tourism on 5/6/2016.
 */
public class SearchResultsComboDetails extends Fragment implements View.OnClickListener{
    LinearLayout  layFlightInfo,layBookFlight,resetFilter;
    Context context;
    Activity activity;
    ArrayList<IntlFlights> flightsDepartureList;
    ArrayList<IntlFlights> flightsReturnList;
    LinearLayout FLOATING_BTN_ROUND;
    ListView LIST_MAIN_ONWARD_FLIGHT,LIST_MAIN_RETURN_FLIGHT;
    Button BTN_MAIN_ONWARD_DEPART,BTN_MAIN_ONWARD_DURATION,BTN_MAIN_ONWARD_DEPART1,BTN_MAIN_ONWARD_DURATION1;
    public static IntlFlights selectedDepertureFlight,selectedReturnFlight;
    public static Data data;
    RangeBar roundRangeBar;
    Button applyFilter;
    TextView minFlightFare, maxFlightFare,maxSelectedFlightFare,minSelectedFlightFare,totalPrice;
    TextView fromOnward, toOnward, fromReturn, toReturn;
    Dialog filterDialog;
    CheckBox nonStop,oneStop,twoOrMoreStop,time1,time2,time3,time4;
    ListView filterPreferFlightsListView;
    Float highestPrice,lowestPrice;
    boolean sortDepart,sortDuration,sortReturn,sortDuration1;
    boolean nonStopState=false,oneStopState=false,twoOrMoreStopState=false,timeState1=false,timeState2=false,timeState3=false,timeState4=false;
    public static ArrayList<Boolean> checkStatus;
    public static ArrayList<String> filteredAirlines = new ArrayList<>();
    public static String gstInNumber,gstCompanyName,gstEmailId;
    public static boolean gstFlag=false;
    ImageView imageViewDepart1,imageViewPrice1,imageViewDepart2,imageViewPrice2;
    AlertDialog.Builder alertDialogBuilder;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMain.backstack = true;
        context=getActivity();
        activity=getActivity();
        selectedDepertureFlight=null;
        selectedReturnFlight=null;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        ActivityMain.backStack2 = true;
        ActivityMain.lastActiveFragment = Constant.PLANNER_FRAGMENT;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        AirHeader.showHeaderText((ActivityMain)activity, false, "");
        AirHeader.showRecentSearchIcon(true);
        AirHeader.showDrawerToggleAndToolbar(true, true);
        View view = inflater.inflate(R.layout.fragment_round_trip_international, null);
        alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage("Flight sold out, Please search another flight");
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            ActivityMain.soldOutDialog = true;
            getActivity().onBackPressed();
            }
        });
        alertDialogBuilder.create();
        flightsDepartureList = AdapterRoundTripInternationalListView.clickedModel.getOnwardFlightsList();
        flightsReturnList = AdapterRoundTripInternationalListView.clickedModel.getReturnFlightsList();
        FLOATING_BTN_ROUND = (LinearLayout)view.findViewById(R.id.FLOATING_BTN_ROUND);
        BTN_MAIN_ONWARD_DEPART = (Button)view.findViewById(R.id.BTN_MAIN_ONWARD_DEPART);
        BTN_MAIN_ONWARD_DURATION = (Button)view.findViewById(R.id.BTN_MAIN_ONWARD_DURATION);
        BTN_MAIN_ONWARD_DEPART1 = (Button)view.findViewById(R.id.BTN_MAIN_ONWARD_DEPART1);
        BTN_MAIN_ONWARD_DURATION1 = (Button)view.findViewById(R.id.BTN_MAIN_ONWARD_DURATION1);
        totalPrice=(TextView)view.findViewById(R.id.TXT_ROUND_TOTAL_PRICE);
        fromOnward = (TextView)view.findViewById(R.id.fromOnward);
        toOnward = (TextView)view.findViewById(R.id.toOnward);
        fromReturn = (TextView)view.findViewById(R.id.fromReturn);
        toReturn = (TextView)view.findViewById(R.id.toReturn);
        imageViewDepart1 = (ImageView)view.findViewById(R.id.imageViewDepart1);
        imageViewPrice1 = (ImageView)view.findViewById(R.id.imageViewPrice1);
        imageViewDepart2 = (ImageView)view.findViewById(R.id.imageViewDepart2);
        imageViewPrice2 = (ImageView)view.findViewById(R.id.imageViewPrice2);
        fromOnward.setText(FragmentPlanner.searchResultIntData.getIntlFlights()[0][0].getDepartureCityWithCode());
        toOnward.setText(FragmentPlanner.searchResultIntData.getIntlFlights()[0][0].getArrivalCityWithCode());
        fromReturn.setText(FragmentPlanner.searchResultIntData.getIntlFlights()[0][0].getArrivalCityWithCode());
        toReturn.setText(FragmentPlanner.searchResultIntData.getIntlFlights()[0][0].getDepartureCityWithCode());
        BTN_MAIN_ONWARD_DEPART.setOnClickListener(this);
        BTN_MAIN_ONWARD_DURATION.setOnClickListener(this);
        BTN_MAIN_ONWARD_DEPART1.setOnClickListener(this);
        BTN_MAIN_ONWARD_DURATION1.setOnClickListener(this);
        FLOATING_BTN_ROUND.setOnClickListener(this);
        LIST_MAIN_ONWARD_FLIGHT = (ListView)view.findViewById(R.id.LIST_MAIN_ONWARD_FLIGHT);
        LIST_MAIN_ONWARD_FLIGHT.setAdapter(new AdapterRoundTripOnwardFlight(getActivity(),flightsDepartureList));
        totalPrice.setText(getString(R.string.symbol_rs)+" "+flightsDepartureList.get(0).getPrice());
        LIST_MAIN_ONWARD_FLIGHT.setItemChecked(0,true);
        LIST_MAIN_ONWARD_FLIGHT.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedDepertureFlight=flightsDepartureList.get(position);
            }
        });
        LIST_MAIN_RETURN_FLIGHT = (ListView)view.findViewById(R.id.LIST_MAIN_RETURN_FLIGHT);
        LIST_MAIN_RETURN_FLIGHT.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedReturnFlight=flightsReturnList.get(position);
            }
        });
        LIST_MAIN_RETURN_FLIGHT.setAdapter(new AdapterRoundTripOnwardFlight(getActivity(),flightsReturnList));
        LIST_MAIN_RETURN_FLIGHT.setItemChecked(0,true);
        layFlightInfo = (LinearLayout) view.findViewById(R.id.FLIGHT_INFO);
        layFlightInfo.setOnClickListener(this);
        layBookFlight = (LinearLayout) view.findViewById(R.id.LAY_ROUND_BOOKING);
        layBookFlight.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.FLIGHT_INFO:
        showSelectedFlightDetailsDialog(selectedDepertureFlight,selectedReturnFlight);
        break;
        case R.id.LAY_ROUND_BOOKING:
            if(DateUtility.getDateAndMonthFromCal(AirDataHolder.getListHolder().getList().get(0).getDepDate()).equals(DateUtility.getDateAndMonthFromCal(AirDataHolder.getListHolder().getList().get(0).getReturnDate()))){
                if(selectedDepertureFlight==null){
                    selectedDepertureFlight=flightsDepartureList.get(0);
                }
                if(selectedReturnFlight==null){
                    selectedReturnFlight=flightsReturnList.get(0);
                }
                int dep= Integer.parseInt(selectedDepertureFlight.getArrivalTime().replace(":",""));
                int ret= Integer.parseInt(selectedReturnFlight.getDepartureTime().replace(":",""));
                if(ret-dep<200){
                    Toast.makeText(context,getString(R.string.two_hour_diff),Toast.LENGTH_SHORT).show();
                }else{
                    showGSTPopup();
                }
            }else{
                showGSTPopup();
            }
        break;
        case R.id.FLOATING_BTN_ROUND:
        showFilterDialog();
        break;
        case R.id.BTN_MAIN_ONWARD_DEPART:
        if(!sortDepart){
            Collections.sort(flightsDepartureList,new ISortByDepart());
            sortDepart=true;
            imageViewDepart1.setImageResource(R.drawable.down_arrow);
        }else{
            Collections.sort(flightsDepartureList,new ISortByDepart());
            Collections.reverse(flightsDepartureList);
            sortDepart=false;
            imageViewDepart1.setImageResource(R.drawable.up_arrow);
        }
        LIST_MAIN_ONWARD_FLIGHT.setAdapter(new AdapterRoundTripOnwardFlight(getActivity(),flightsDepartureList));
        BTN_MAIN_ONWARD_DEPART.setBackgroundColor(ContextCompat.getColor(context,R.color.colorLightBlue));
        BTN_MAIN_ONWARD_DEPART.setTextColor(ContextCompat.getColor(context,android.R.color.white));
        BTN_MAIN_ONWARD_DURATION.setBackgroundColor(ContextCompat.getColor(context,android.R.color.white));
        BTN_MAIN_ONWARD_DURATION.setTextColor(ContextCompat.getColor(context,R.color.colorLightBlue));
        break;
        case R.id.BTN_MAIN_ONWARD_DURATION:
            if(!sortDuration){
                Collections.sort(flightsDepartureList,new ISortByDuration());
                sortDuration=true;
                imageViewPrice1.setImageResource(R.drawable.down_arrow);
            }else{
                Collections.sort(flightsDepartureList,new ISortByDuration());
                Collections.reverse(flightsDepartureList);
                sortDuration=false;
                imageViewPrice1.setImageResource(R.drawable.up_arrow);
            }
        LIST_MAIN_ONWARD_FLIGHT.setAdapter(new AdapterRoundTripOnwardFlight(getActivity(),flightsDepartureList));
        BTN_MAIN_ONWARD_DURATION.setBackgroundColor(ContextCompat.getColor(context,R.color.colorLightBlue));
        BTN_MAIN_ONWARD_DURATION.setTextColor(ContextCompat.getColor(context,android.R.color.white));
        BTN_MAIN_ONWARD_DEPART.setBackgroundColor(ContextCompat.getColor(context,android.R.color.white));
        BTN_MAIN_ONWARD_DEPART.setTextColor(ContextCompat.getColor(context,R.color.colorLightBlue));
        break;
        case R.id.BTN_MAIN_ONWARD_DEPART1:
            if(!sortReturn){
                Collections.sort(flightsReturnList,new ISortByDepart());
                sortReturn=true;
                imageViewDepart2.setImageResource(R.drawable.down_arrow);
            }else{
                Collections.sort(flightsReturnList,new ISortByDepart());
                Collections.reverse(flightsReturnList);
                sortReturn=false;
                imageViewDepart2.setImageResource(R.drawable.up_arrow);
            }
        LIST_MAIN_RETURN_FLIGHT.setAdapter(new AdapterRoundTripOnwardFlight(getActivity(),flightsReturnList));
        BTN_MAIN_ONWARD_DEPART1.setBackgroundColor(ContextCompat.getColor(context,R.color.colorLightBlue));
        BTN_MAIN_ONWARD_DEPART1.setTextColor(ContextCompat.getColor(context,android.R.color.white));
        BTN_MAIN_ONWARD_DURATION1.setBackgroundColor(ContextCompat.getColor(context,android.R.color.white));
        BTN_MAIN_ONWARD_DURATION1.setTextColor(ContextCompat.getColor(context,R.color.colorLightBlue));
        break;
        case R.id.BTN_MAIN_ONWARD_DURATION1:
            if(!sortDuration1){
                Collections.sort(flightsReturnList,new ISortByDuration());
                sortDuration1=true;
                imageViewPrice2.setImageResource(R.drawable.down_arrow);
            }else{
                Collections.sort(flightsReturnList,new ISortByDuration());
                Collections.reverse(flightsReturnList);
                sortDuration1=false;
                imageViewPrice2.setImageResource(R.drawable.up_arrow);
            }

        LIST_MAIN_RETURN_FLIGHT.setAdapter(new AdapterRoundTripOnwardFlight(getActivity(),flightsReturnList));
        BTN_MAIN_ONWARD_DURATION1.setBackgroundColor(ContextCompat.getColor(context,R.color.colorLightBlue));
        BTN_MAIN_ONWARD_DURATION1.setTextColor(ContextCompat.getColor(context,android.R.color.white));
        BTN_MAIN_ONWARD_DEPART1.setBackgroundColor(ContextCompat.getColor(context,android.R.color.white));
        BTN_MAIN_ONWARD_DEPART1.setTextColor(ContextCompat.getColor(context,R.color.colorLightBlue));
        break;
        }
    }

    private void showGSTPopup(){
        gstFlag = false;
        gstInNumber ="";
        gstCompanyName ="";
        gstEmailId = "";
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        dialog.dismiss();
                        showFillGSTDetailsPopup();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        reprice();
                        dialog.dismiss();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Do you want to use GST number for this booking?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    private void showFillGSTDetailsPopup(){
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.view_fill_gst_details);
        Button dialogButton = (Button) dialog.findViewById(R.id.btnOk);

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkValidations(dialog);
            }
        });
        dialog.show();
    }

    private void checkValidations(Dialog dialog){
        AutoCompleteTextView gstinNumber = (AutoCompleteTextView) dialog.findViewById(R.id.gstinNumber);
        AutoCompleteTextView companyName = (AutoCompleteTextView) dialog.findViewById(R.id.companyName);
        AutoCompleteTextView emailId = (AutoCompleteTextView) dialog.findViewById(R.id.emailId);
        TextView textViewWarning = (TextView) dialog.findViewById(R.id.textViewWarning);
        if(gstinNumber.getText().toString().trim().length()==0){
            textViewWarning.setVisibility(View.VISIBLE);
            textViewWarning.setText("GSTIN Number Can't be blank");

        }else if(!gstinNumber.getText().toString().trim().matches("^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$")){
            textViewWarning.setVisibility(View.VISIBLE);
            textViewWarning.setText("GSTIN Number is Invalid");
        }/*else if(!gstinNumber.getText().toString().trim().matches("\\d{2}[A-Z]{5}\\d{4}[A-Z]{1}\\d{1}[A-Z]{1}\\d{1}")){
            textViewWarning.setVisibility(View.VISIBLE);
            textViewWarning.setText("GSTIN Number is Invalid");
        }*/else if(companyName.getText().toString().trim().length()==0){
            textViewWarning.setVisibility(View.VISIBLE);
            textViewWarning.setText("Company Name Can't be blank");
        }else if(emailId.getText().toString().trim().length()==0){
            textViewWarning.setVisibility(View.VISIBLE);
            textViewWarning.setText("Email-Id Can't be blank");
        }else if(!Patterns.EMAIL_ADDRESS.matcher(emailId.getText().toString().trim()).matches()){
            textViewWarning.setVisibility(View.VISIBLE);
            textViewWarning.setText("Email-Id is Invalid");
        }else{
            gstFlag=true;
            gstInNumber=gstinNumber.getText().toString().trim();
            gstCompanyName=companyName.getText().toString().trim();
            gstEmailId=emailId.getText().toString().trim();
            dialog.dismiss();
            reprice();
        }

    }

    private void showSelectedFlightDetailsDialog(IntlFlights selectedDepertureFlight,IntlFlights selectedReturnFlight){
    if(selectedDepertureFlight==null){
        selectedDepertureFlight=flightsDepartureList.get(0);
    }
    if(selectedReturnFlight==null){
        selectedReturnFlight=flightsReturnList.get(0);
    }
    final Dialog flightDetailDialog = new Dialog(context, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
    flightDetailDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
    flightDetailDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    flightDetailDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.LTGRAY));
    flightDetailDialog.setContentView(R.layout.round_trip_gds_flight_info);
    ListView lvGdsFlightDetail;
    GdsFlightInfoAdapter mGdsFlightAdapter;
    TextView flightBaseFare,flightTaxes,flightTotal;
    ListView flightOnward = (ListView)flightDetailDialog.findViewById(R.id.flightOnward);
        LstBaggageDetails lstBaggageDetails = new LstBaggageDetails();
        for (int i = 0; i < selectedDepertureFlight.getLstBaggageDetails().length; i++) {
            if (selectedDepertureFlight.getLstBaggageDetails()[i].getPaxType().equalsIgnoreCase("ADT")){
                lstBaggageDetails = selectedDepertureFlight.getLstBaggageDetails()[i];
                break;
            }
        }

        LstBaggageDetails lstBaggageDetailsReturn = new LstBaggageDetails();
        for (int i = 0; i < selectedReturnFlight.getLstBaggageDetails().length; i++) {
            if (selectedReturnFlight.getLstBaggageDetails()[i].getPaxType().equalsIgnoreCase("ADT")){
                lstBaggageDetailsReturn = selectedReturnFlight.getLstBaggageDetails()[i];
                break;
            }
        }

    ArrayList<ModelRoutListItem> arrayListDeparture = new ArrayList<>();
    for(int i=0;i<selectedDepertureFlight.getLstFlightDetails().length;i++){
        ModelRoutListItem modelRoutListItem =new ModelRoutListItem();
        String flightCode=selectedDepertureFlight.getLstFlightDetails()[i].getFlightNumber();
        String fromAirportCode=selectedDepertureFlight.getLstFlightDetails()[i].getOrigin();
        String fromAirportName="";
        if(selectedDepertureFlight.getLstFlightDetails()[i].getOriginCity()!=null){
        fromAirportName=selectedDepertureFlight.getLstFlightDetails()[i].getOriginCity();
        }
        String fromAirportTerminal=selectedDepertureFlight.getLstFlightDetails()[i].getOriginTerminal();
        String toAirportCode=selectedDepertureFlight.getLstFlightDetails()[i].getDestination();
        String toAirportName="";
        if(selectedDepertureFlight.getLstFlightDetails()[i].getDestinationCity()!=null){
        toAirportName=selectedDepertureFlight.getLstFlightDetails()[i].getDestinationCity();
        }
        String toAirportTerminal=selectedDepertureFlight.getLstFlightDetails()[i].getDestinationTerminal();
        String travelDuration=selectedDepertureFlight.getLstFlightDetails()[i].getFlightTime();
        String departureDate=selectedDepertureFlight.getLstFlightDetails()[i].getDepartureDate();
        String departureTime=selectedDepertureFlight.getLstFlightDetails()[i].getDepartureTime();
        String arrivalDate=selectedDepertureFlight.getLstFlightDetails()[i].getArrivalDate();
        String arrivalTime=selectedDepertureFlight.getLstFlightDetails()[i].getArrivalTime();
        String halt=selectedDepertureFlight.getLstFlightDetails()[i].getHalt();
        modelRoutListItem.setServiceProvider(selectedDepertureFlight.getServiceProvider());
        modelRoutListItem.setFreeMeal(selectedDepertureFlight.getLstFlightDetails()[i].isFreeMeal());
        modelRoutListItem.setLstBaggageDetails(lstBaggageDetails);
        modelRoutListItem.setFlightCode(flightCode);
        modelRoutListItem.setCarrear(selectedDepertureFlight.getLstFlightDetails()[i].getAirlineCode());
        modelRoutListItem.setFromAirportCode(fromAirportCode);
        modelRoutListItem.setFromAirportName(fromAirportName);
        modelRoutListItem.setFromAirportTerminal(fromAirportTerminal);
        modelRoutListItem.setToAirportCode(toAirportCode);
        modelRoutListItem.setToAirportName(toAirportName);
        modelRoutListItem.setToAirportTerminal(toAirportTerminal);
        modelRoutListItem.setTravelDuration(travelDuration);
        SimpleDateFormat formatter5=new SimpleDateFormat("yyyy-MM-dd");
        Date depDate=null,arrDate=null;
        try {
            depDate = formatter5.parse(departureDate.substring(0,10));
            arrDate = formatter5.parse(arrivalDate.substring(0,10));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        modelRoutListItem.setDepartureDate(new SimpleDateFormat("dd MMM yyyy").format(depDate));
        modelRoutListItem.setArrivalDate(new SimpleDateFormat("dd MMM yyyy").format(arrDate));
        modelRoutListItem.setDepartureTime(departureTime);
        modelRoutListItem.setArrivalTime(arrivalTime);
        modelRoutListItem.setHalt(halt);
        arrayListDeparture.add(modelRoutListItem);
    }
    flightOnward.setAdapter(new AdapterOneWayFlightInfo(context,arrayListDeparture));
    ListViewInsideScrollView.open(flightOnward);
    ListView flightReturn = (ListView)flightDetailDialog.findViewById(R.id.flightReturn);
        TextView txtOnewayRefundable1 = (TextView)flightDetailDialog.findViewById(R.id.INTER_ONWARD_REFUNDABLE_TXT);
        TextView txtOnewayRefundable2 = (TextView)flightDetailDialog.findViewById(R.id.INTER_RETURN_REFUNDABLE_TXT);
        ImageView imgOnewayRefundable1 = (ImageView)flightDetailDialog.findViewById(R.id.INTER_ONWARD_REFUNDABLE_IMG);
        ImageView imgOnewayRefundable2 = (ImageView)flightDetailDialog.findViewById(R.id.INTER_RETURN_REFUNDABLE_IMG);

        ((TextView)flightDetailDialog.findViewById(R.id.flightClass1)).setText("("+FragmentPlanner.flightTravellClass+")");
        ((TextView)flightDetailDialog.findViewById(R.id.flightClass2)).setText("("+FragmentPlanner.flightTravellClass+")");

        if(selectedDepertureFlight.getLstFareDetails()[0].getBaseType().equals("false")){
            imgOnewayRefundable1.setImageResource(R.drawable.refundable_red);
            txtOnewayRefundable1.setText("Non Refundable");
        }else if(selectedDepertureFlight.getLstFareDetails()[0].getBaseType().equals("true")){
            imgOnewayRefundable1.setImageResource(R.drawable.refundable_green);
            txtOnewayRefundable1.setText("Refundable");
        }
        if(selectedReturnFlight.getLstFareDetails()[0].getBaseType().equals("false")){
            imgOnewayRefundable2.setImageResource(R.drawable.refundable_red);
            txtOnewayRefundable2.setText("Non Refundable");
        }else if(selectedReturnFlight.getLstFareDetails()[0].getBaseType().equals("true")){
            imgOnewayRefundable2.setImageResource(R.drawable.refundable_green);
            txtOnewayRefundable2.setText("Refundable");
        }

    ArrayList<ModelRoutListItem> arrayListReturn = new ArrayList<>();
    for(int j=0;j<selectedReturnFlight.getLstFlightDetails().length;j++){
        ModelRoutListItem modelRoutListItem =new ModelRoutListItem();
        String flightCode=selectedReturnFlight.getLstFlightDetails()[j].getFlightNumber();
        String fromAirportCode=selectedReturnFlight.getLstFlightDetails()[j].getOrigin();
        String fromAirportName="";
        if(selectedReturnFlight.getLstFlightDetails()[j].getOriginCity()!=null){
        fromAirportName=selectedReturnFlight.getLstFlightDetails()[j].getOriginCity();
        }
        String fromAirportTerminal=selectedReturnFlight.getLstFlightDetails()[j].getOriginTerminal();
        String toAirportCode=selectedReturnFlight.getLstFlightDetails()[j].getDestination();
        String toAirportName="";
        if(selectedReturnFlight.getLstFlightDetails()[j].getDestinationCity()!=null){
        toAirportName=selectedReturnFlight.getLstFlightDetails()[j].getDestinationCity();
        }
        String toAirportTerminal=selectedReturnFlight.getLstFlightDetails()[j].getDestinationTerminal();
        String travelDuration=selectedReturnFlight.getLstFlightDetails()[j].getFlightTime();
        String departureDate=selectedReturnFlight.getLstFlightDetails()[j].getDepartureDate();
        String departureTime=selectedReturnFlight.getLstFlightDetails()[j].getDepartureTime();
        String arrivalDate=selectedReturnFlight.getLstFlightDetails()[j].getArrivalDate();
        String arrivalTime=selectedReturnFlight.getLstFlightDetails()[j].getArrivalTime();
        String halt=selectedReturnFlight.getLstFlightDetails()[j].getHalt();
        modelRoutListItem.setFlightCode(flightCode);
        modelRoutListItem.setServiceProvider(selectedReturnFlight.getServiceProvider());
        modelRoutListItem.setFreeMeal(selectedReturnFlight.getLstFlightDetails()[j].isFreeMeal());
        modelRoutListItem.setLstBaggageDetails(lstBaggageDetailsReturn);
        modelRoutListItem.setCarrear(selectedReturnFlight.getLstFlightDetails()[j].getAirlineCode());
        modelRoutListItem.setFromAirportCode(fromAirportCode);
        modelRoutListItem.setFromAirportName(fromAirportName);
        modelRoutListItem.setFromAirportTerminal(fromAirportTerminal);
        modelRoutListItem.setToAirportCode(toAirportCode);
        modelRoutListItem.setToAirportName(toAirportName);
        modelRoutListItem.setToAirportTerminal(toAirportTerminal);
        modelRoutListItem.setTravelDuration(travelDuration);
        SimpleDateFormat formatter5=new SimpleDateFormat("yyyy-MM-dd");
        Date depDate=null,arrDate=null;
        try {
            depDate = formatter5.parse(departureDate.substring(0,10));
            arrDate = formatter5.parse(arrivalDate.substring(0,10));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        modelRoutListItem.setDepartureDate(new SimpleDateFormat("dd MMM yyyy").format(depDate));
        modelRoutListItem.setArrivalDate(new SimpleDateFormat("dd MMM yyyy").format(arrDate));
        modelRoutListItem.setDepartureTime(departureTime);
        modelRoutListItem.setArrivalTime(arrivalTime);
        modelRoutListItem.setHalt(halt);
        arrayListReturn.add(modelRoutListItem);
    }
    flightReturn.setAdapter(new AdapterOneWayFlightInfo(context,arrayListReturn));
    ListViewInsideScrollView.open(flightReturn);
    flightBaseFare = (TextView) flightDetailDialog.findViewById(R.id.TXT_FARE_BASE);
    flightTaxes = (TextView) flightDetailDialog.findViewById(R.id.TXT_FARE_TAXES);
    flightTotal = (TextView) flightDetailDialog.findViewById(R.id.TXT_FARE_TOTAL);
    Button lObjBtnDone = (Button) flightDetailDialog.findViewById(R.id.BTN_FLIGHT_DETAIL_DONE);
    lObjBtnDone.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            flightDetailDialog.cancel();
        }
    });

     float baseFare=0.0f,tax=0.0f,total=0.0f;
     for(int xc=0;xc<selectedDepertureFlight.getLstFareDetails().length;xc++){
         baseFare=baseFare+Float.parseFloat(selectedDepertureFlight.getLstFareDetails()[xc].getBaseFare());
         tax=tax+Float.parseFloat(selectedDepertureFlight.getLstFareDetails()[xc].getTax());
         total=total+Float.parseFloat(selectedDepertureFlight.getLstFareDetails()[xc].getTotal());
     }

    flightBaseFare.setText(getString(R.string.symbol_rs)+String.valueOf(baseFare));
    flightTaxes.setText(getString(R.string.symbol_rs)+String.valueOf(tax));
    flightTotal.setText(getString(R.string.symbol_rs)+String.valueOf(total));
    flightDetailDialog.show();
}

    private void reprice(){
        ModelGstDetails modelGstDetails=new ModelGstDetails();
        modelGstDetails.setGstflag(gstFlag);
        modelGstDetails.setGstNumber(gstInNumber);
        modelGstDetails.setCompanyName(gstCompanyName);
        modelGstDetails.setEmailid(gstEmailId);
        NetworkingUtils.showProgress(getActivity());
        if(selectedDepertureFlight==null){
            selectedDepertureFlight=flightsDepartureList.get(0);
        }
        if(selectedReturnFlight==null){
            selectedReturnFlight=flightsReturnList.get(0);
        }
        String[] flightKeys=new String[]{selectedDepertureFlight.getKey(),selectedReturnFlight.getKey()};
        ArrayList<String> arrayListGdsKeys = new ArrayList<>();
        for(LstFlightDetails lstFlightDetails : selectedDepertureFlight.getLstFlightDetails()){
            arrayListGdsKeys.add(lstFlightDetails.getKey());
        }
        for(LstFlightDetails lstFlightDetails : selectedReturnFlight.getLstFlightDetails()){
            arrayListGdsKeys.add(lstFlightDetails.getKey());
        }
        String[] gdsKeys=new String[arrayListGdsKeys.size()];
        for(int i=0;i<arrayListGdsKeys.size();i++){
            gdsKeys[i]=arrayListGdsKeys.get(i);
        }
        NetworkingUtils.showProgress(getActivity());
        Networking.repriceRoundTripInternational(
                String.valueOf(Float.parseFloat(selectedDepertureFlight.getLstFareDetails()[0].getTotal())+Float.parseFloat(selectedReturnFlight.getLstFareDetails()[0].getTotal())),
                FragmentPlanner.searchResultIntData.getSearchKey(),
                flightKeys,
                FragmentPlanner.noOfAdults,
                FragmentPlanner.noOfChildren,
                FragmentPlanner.noOfInfants,
                modelGstDetails,
                gdsKeys,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Response",response.toString());
                        NetworkingUtils.dismissProgress();
                        Gson gson=new Gson();
                        PojoOneWayReprice pojoOneWayReprice =gson.fromJson(response.toString(),PojoOneWayReprice.class);
                        if(pojoOneWayReprice.getStatus().equalsIgnoreCase("SUCCESS")){
                            try {
                                JSONObject jsonObject = new JSONObject(response.toString());
                                JSONObject data = jsonObject.getJSONObject("data");
                                if (data.has("signMap")) {
                                    String signMap = data.getJSONObject("signMap").toString();
                                    SharedPrefrenceAir sharedPrefrenceAir = new SharedPrefrenceAir(getContext());
                                    sharedPrefrenceAir.setSignMap(signMap);
                                    Log.e("signMap ", sharedPrefrenceAir.getSignMap());
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            data = pojoOneWayReprice.getData();
                            if(data.getIsPriceChange().equals("1")){
                                showPriceChangeDialog();
                            }else {
                                ProjectUtil.replaceFragment(activity, new IReprice(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                            }
                        }else if(pojoOneWayReprice.getStatus().equalsIgnoreCase("FAILURE")){
                            if(pojoOneWayReprice.getData().getPriceChangeFlag().equals("1")){
                            Toast.makeText(context,pojoOneWayReprice.getMessage(),Toast.LENGTH_SHORT).show();
                            alertDialogBuilder.show();
                            }
//                            Toast.makeText(context,pojoOneWayReprice.getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(error.networkResponse!=null){
                            if(error.networkResponse.statusCode==500){
                                NetworkingUtils.dismissProgress();
                                Toast.makeText(context,"Internal Server Error.",Toast.LENGTH_SHORT).show();
                            }else{
                                NetworkingUtils.dismissProgress();
                                NetworkingUtils.noInternetAccess(context);
                            }
                        }else{
                            NetworkingUtils.dismissProgress();
                            NetworkingUtils.noInternetAccess(context);
                        }
                    }
                }
        );
    }
    private void showPriceChangeDialog(){
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        dialog.dismiss();
                        ProjectUtil.replaceFragment(activity, new IReprice(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Price changed, Are you sure to continue?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

   private void showFilterDialog(){
       filterDialog = new Dialog(activity, android.R.style.Theme_DeviceDefault_Light_NoActionBar);
       activity.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
       filterDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
       filterDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.LTGRAY));
       filterDialog.setContentView(R.layout.fragment_filter);
       applyFilter = (Button)filterDialog.findViewById(R.id.filterBtnApply);
       resetFilter = (LinearLayout)filterDialog.findViewById(R.id.FILTER_RESET_LAY);
       nonStop = (CheckBox)filterDialog.findViewById(R.id.filterCkBoxZeroStops);
       oneStop = (CheckBox)filterDialog.findViewById(R.id.filterCkBoxOneStops);
       twoOrMoreStop = (CheckBox)filterDialog.findViewById(R.id.filterCkBoxMoreStops);
       time1 = (CheckBox)filterDialog.findViewById(R.id.chBoxfilterOnward_1);
       time2 = (CheckBox)filterDialog.findViewById(R.id.chBoxfilterOnward_2);
       time3 = (CheckBox)filterDialog.findViewById(R.id.chBoxfilterOnward_3);
       time4 = (CheckBox)filterDialog.findViewById(R.id.chBoxfilterOnward_4);
       filterPreferFlightsListView = (ListView)filterDialog.findViewById(R.id.filterPreferFlightsListView);
       if(nonStopState){
           nonStop.setChecked(true);
       }else{
           nonStop.setChecked(false);
       }
       if(oneStopState){
           oneStop.setChecked(true);
       }else{
           oneStop.setChecked(false);
       }
       if(twoOrMoreStopState){
           twoOrMoreStop.setChecked(true);
       }else{
           twoOrMoreStop.setChecked(false);
       }
       if(timeState1){
           time1.setChecked(true);
       }else{
           time1.setChecked(false);
       }
       if(timeState2){
           time2.setChecked(true);
       }else{
           time2.setChecked(false);
       }
       if(timeState3){
           time3.setChecked(true);
       }else{
           time3.setChecked(false);
       }
       if(timeState4){
           time4.setChecked(true);
       }else{
           time4.setChecked(false);
       }
       applyFilter.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               applyFilter();
           }
       });
       resetFilter.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               filterDialog.dismiss();
               resetFilter();
           }
       });
       makeAirlinesList();
       setPriceOnRangeBar();
       filterDialog.show();
   }

    private void setPriceOnRangeBar(){
        roundRangeBar = (RangeBar)filterDialog.findViewById(R.id.rangebarPrice);
        minFlightFare = (TextView)filterDialog.findViewById(R.id.txtFlightFareMin);
        maxFlightFare = (TextView)filterDialog.findViewById(R.id.txtFlightFareMax);
        maxSelectedFlightFare = (TextView)filterDialog.findViewById(R.id.txtSelectedFlightFareMax);
        minSelectedFlightFare = (TextView)filterDialog.findViewById(R.id.txtSelectedFlightFareMin);
        roundRangeBar.setBarColor(Color.parseColor("#9DA2A3"));
        roundRangeBar.setPinColor(Color.parseColor("#21A1CE")); // change the background color of pin like drop
        roundRangeBar.setPinRadius(30);// change the radious of pin like drop
        roundRangeBar.setTickColor(Color.TRANSPARENT); // background color line
        roundRangeBar.setConnectingLineColor(Color.parseColor("#21A1CE"));
        roundRangeBar.setSelectorColor(Color.parseColor("#4F565E"));//
        roundRangeBar.setConnectingLineWeight(1);
        //assignMinMaxPriceGDS();
        roundRangeBar.setPinTextFormatter(new RangeBar.PinTextFormatter() {
            @Override
            public String getText(String value) {
                return value;
            }
        });
        highestPrice=getHighestPrice();
        lowestPrice=getLowestPrice();
        if(!((highestPrice-lowestPrice)<=1)){
            roundRangeBar.setTickEnd(highestPrice);
            roundRangeBar.setTickStart(lowestPrice);
            roundRangeBar.setTickInterval(1);
            roundRangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
                @Override
                public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                   minSelectedFlightFare.setText("\u20B9  " + Integer.parseInt(rangeBar.getLeftPinValue()));
                    maxSelectedFlightFare.setText("\u20B9  " + Integer.parseInt(rangeBar.getRightPinValue()));
                    highestPrice=Float.parseFloat(rangeBar.getRightPinValue());
                    lowestPrice=Float.parseFloat(rangeBar.getLeftPinValue());
                }
            });
        }else{
            roundRangeBar.setEnabled(false);
        }


    }

    private float getLowestPrice(){
        IntlFlights onwardMin = Collections.min(flightsDepartureList, new ISortByFarePrice());
        IntlFlights returnMin = Collections.min(flightsReturnList, new ISortByFarePrice());
        return Float.parseFloat((Float.parseFloat(onwardMin.getPrice())<Float.parseFloat(returnMin.getPrice())) ? onwardMin.getPrice() : returnMin.getPrice());
    }
    private float getHighestPrice(){
        IntlFlights onwardMax = Collections.max(flightsDepartureList, new ISortByFarePrice());
        IntlFlights returnMax = Collections.max(flightsReturnList, new ISortByFarePrice());
        return Float.parseFloat(((Float.parseFloat(onwardMax.getPrice())>Float.parseFloat(returnMax.getPrice())) ? onwardMax.getPrice() : returnMax.getPrice()));
    }

    private void resetFilter(){
        checkStatus=null;
        nonStopState=false;
        oneStopState=false;
        twoOrMoreStopState=false;
        timeState1=false;
        timeState2=false;
        timeState3=false;
        timeState4=false;
        LIST_MAIN_ONWARD_FLIGHT.setAdapter(new AdapterRoundTripOnwardFlight(getActivity(),flightsDepartureList));
        LIST_MAIN_RETURN_FLIGHT.setAdapter(new AdapterRoundTripOnwardFlight(getActivity(),flightsReturnList));
        Toast.makeText(context,"Filter Reset",Toast.LENGTH_SHORT).show();
    }

    private void makeAirlinesList(){
        LinkedHashMap<String,String> airlinesHashMap =new LinkedHashMap<>();
        LinkedHashMap<String,String> airlinesIconHashMap =new LinkedHashMap<>();
            for(IntlFlights flight : flightsDepartureList){
                if(airlinesHashMap.containsKey(flight.getCarrierName())){
                    if(Integer.parseInt(airlinesHashMap.get(flight.getCarrierName()))>=Integer.parseInt(flight.getPrice())){
                        airlinesHashMap.put(flight.getCarrierName(),flight.getPrice());
                        airlinesIconHashMap.put(flight.getCarrierName(),flight.getCarrier());
                    }
                }else{
                    airlinesHashMap.put(flight.getCarrierName(),flight.getPrice());
                    airlinesIconHashMap.put(flight.getCarrierName(),flight.getCarrier());
                }
            }
            for(IntlFlights flight : flightsReturnList){
                if(airlinesHashMap.containsKey(flight.getCarrierName())){
                    if(Integer.parseInt(airlinesHashMap.get(flight.getCarrierName()))>=Integer.parseInt(flight.getPrice())){
                        airlinesHashMap.put(flight.getCarrierName(),flight.getPrice());
                        airlinesIconHashMap.put(flight.getCarrierName(),flight.getCarrier());
                    }
                }else{
                    airlinesHashMap.put(flight.getCarrierName(),flight.getPrice());
                    airlinesIconHashMap.put(flight.getCarrierName(),flight.getCarrier());
                }
            }
        ArrayList<String> airlines = new ArrayList<>(airlinesHashMap.keySet());
        ArrayList<String> airFare = new ArrayList<>(airlinesHashMap.values());
        ArrayList<String> airlineIcons = new ArrayList<>(airlinesIconHashMap.values());
        filterPreferFlightsListView.setAdapter(new IFilterAirlineAdapter(context,airlines,airFare,airlineIcons));
        setListViewHeightBasedOnChildren(filterPreferFlightsListView);
    }

    private void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    private void applyFilter(){
            ArrayList<IntlFlights> onwardFlightsListClone=new ArrayList<>();
            ArrayList<IntlFlights> returnFlightsListClone=new ArrayList<>();
            for (int i = 0; i < flightsDepartureList.size(); i++) {
                if(filteredAirlines.size()>0){
                    for(String carrearName : filteredAirlines) {
                        if (carrearName.equals(flightsDepartureList.get(i).getCarrierName()) && carrearName.equals(flightsDepartureList.get(i).getCarrierName())){
                            if(checkForTime()){
                                if (Float.parseFloat(flightsDepartureList.get(i).getPrice()) >= lowestPrice && Float.parseFloat(flightsDepartureList.get(i).getPrice()) <= highestPrice) {
                                    if(checkForStops()){
                                        if(checkForNonStop(flightsDepartureList.get(i).getStops())){
                                            if(checkForTime1(flightsDepartureList.get(i).getDepartureTime())){
                                                onwardFlightsListClone.add(flightsDepartureList.get(i));
                                            }
                                            if(checkForTime2(flightsDepartureList.get(i).getDepartureTime())){
                                                onwardFlightsListClone.add(flightsDepartureList.get(i));
                                            }
                                            if(checkForTime3(flightsDepartureList.get(i).getDepartureTime())){
                                                onwardFlightsListClone.add(flightsDepartureList.get(i));
                                            }
                                            if(checkForTime4(flightsDepartureList.get(i).getDepartureTime())){
                                                onwardFlightsListClone.add(flightsDepartureList.get(i));
                                            }
                                        }
                                        if(checkForOneStop(flightsDepartureList.get(i).getStops())){
                                            if(checkForTime1(flightsDepartureList.get(i).getDepartureTime())){
                                                onwardFlightsListClone.add(flightsDepartureList.get(i));
                                            }
                                            if(checkForTime2(flightsDepartureList.get(i).getDepartureTime())){
                                                onwardFlightsListClone.add(flightsDepartureList.get(i));
                                            }
                                            if(checkForTime3(flightsDepartureList.get(i).getDepartureTime())){
                                                onwardFlightsListClone.add(flightsDepartureList.get(i));
                                            }
                                            if(checkForTime4(flightsDepartureList.get(i).getDepartureTime())){
                                                onwardFlightsListClone.add(flightsDepartureList.get(i));
                                            }
                                        }
                                        if(checkForTwoOrMoreStop(flightsDepartureList.get(i).getStops())){
                                            if(checkForTime1(flightsDepartureList.get(i).getDepartureTime())){
                                                onwardFlightsListClone.add(flightsDepartureList.get(i));
                                            }
                                            if(checkForTime2(flightsDepartureList.get(i).getDepartureTime())){
                                                onwardFlightsListClone.add(flightsDepartureList.get(i));
                                            }
                                            if(checkForTime3(flightsDepartureList.get(i).getDepartureTime())){
                                                onwardFlightsListClone.add(flightsDepartureList.get(i));
                                            }
                                            if(checkForTime4(flightsDepartureList.get(i).getDepartureTime())){
                                                onwardFlightsListClone.add(flightsDepartureList.get(i));
                                            }
                                        }
                                    }else{
                                        if(checkForTime1(flightsDepartureList.get(i).getDepartureTime())){
                                            onwardFlightsListClone.add(flightsDepartureList.get(i));
                                        }
                                        if(checkForTime2(flightsDepartureList.get(i).getDepartureTime())){
                                            onwardFlightsListClone.add(flightsDepartureList.get(i));
                                        }
                                        if(checkForTime3(flightsDepartureList.get(i).getDepartureTime())){
                                            onwardFlightsListClone.add(flightsDepartureList.get(i));
                                        }
                                        if(checkForTime4(flightsDepartureList.get(i).getDepartureTime())){
                                            onwardFlightsListClone.add(flightsDepartureList.get(i));
                                        }                            }
                                }
                            }else{
                                if (Float.parseFloat(flightsDepartureList.get(i).getPrice()) >= lowestPrice && Float.parseFloat(flightsDepartureList.get(i).getPrice()) <= highestPrice) {
                                    if(checkForStops()){
                                        if(checkForNonStop(flightsDepartureList.get(i).getStops())){
                                            onwardFlightsListClone.add(flightsDepartureList.get(i));
                                        }
                                        if(checkForOneStop(flightsDepartureList.get(i).getStops())){
                                            onwardFlightsListClone.add(flightsDepartureList.get(i));
                                        }
                                        if(checkForTwoOrMoreStop(flightsDepartureList.get(i).getStops())){
                                            onwardFlightsListClone.add(flightsDepartureList.get(i));
                                        }
                                    }else{
                                        onwardFlightsListClone.add(flightsDepartureList.get(i));
                                    }
                                }
                            }
                        }
                    }
                }else{
                    if(checkForTime()){
                        if (Float.parseFloat(flightsDepartureList.get(i).getPrice()) >= lowestPrice && Float.parseFloat(flightsDepartureList.get(i).getPrice()) <= highestPrice) {
                            if(checkForStops()){
                                if(checkForNonStop(flightsDepartureList.get(i).getStops())){
                                    if(checkForTime1(flightsDepartureList.get(i).getDepartureTime())){
                                        onwardFlightsListClone.add(flightsDepartureList.get(i));
                                    }
                                    if(checkForTime2(flightsDepartureList.get(i).getDepartureTime())){
                                        onwardFlightsListClone.add(flightsDepartureList.get(i));
                                    }
                                    if(checkForTime3(flightsDepartureList.get(i).getDepartureTime())){
                                        onwardFlightsListClone.add(flightsDepartureList.get(i));
                                    }
                                    if(checkForTime4(flightsDepartureList.get(i).getDepartureTime())){
                                        onwardFlightsListClone.add(flightsDepartureList.get(i));
                                    }
                                }
                                if(checkForOneStop(flightsDepartureList.get(i).getStops())){
                                    if(checkForTime1(flightsDepartureList.get(i).getDepartureTime())){
                                        onwardFlightsListClone.add(flightsDepartureList.get(i));
                                    }
                                    if(checkForTime2(flightsDepartureList.get(i).getDepartureTime())){
                                        onwardFlightsListClone.add(flightsDepartureList.get(i));
                                    }
                                    if(checkForTime3(flightsDepartureList.get(i).getDepartureTime())){
                                        onwardFlightsListClone.add(flightsDepartureList.get(i));
                                    }
                                    if(checkForTime4(flightsDepartureList.get(i).getDepartureTime())){
                                        onwardFlightsListClone.add(flightsDepartureList.get(i));
                                    }
                                }
                                if(checkForTwoOrMoreStop(flightsDepartureList.get(i).getStops())){
                                    if(checkForTime1(flightsDepartureList.get(i).getDepartureTime())){
                                        onwardFlightsListClone.add(flightsDepartureList.get(i));
                                    }
                                    if(checkForTime2(flightsDepartureList.get(i).getDepartureTime())){
                                        onwardFlightsListClone.add(flightsDepartureList.get(i));
                                    }
                                    if(checkForTime3(flightsDepartureList.get(i).getDepartureTime())){
                                        onwardFlightsListClone.add(flightsDepartureList.get(i));
                                    }
                                    if(checkForTime4(flightsDepartureList.get(i).getDepartureTime())){
                                        onwardFlightsListClone.add(flightsDepartureList.get(i));
                                    }
                                }
                            }else{
                                if(checkForTime1(flightsDepartureList.get(i).getDepartureTime())){
                                    onwardFlightsListClone.add(flightsDepartureList.get(i));
                                }
                                if(checkForTime2(flightsDepartureList.get(i).getDepartureTime())){
                                    onwardFlightsListClone.add(flightsDepartureList.get(i));
                                }
                                if(checkForTime3(flightsDepartureList.get(i).getDepartureTime())){
                                    onwardFlightsListClone.add(flightsDepartureList.get(i));
                                }
                                if(checkForTime4(flightsDepartureList.get(i).getDepartureTime())){
                                    onwardFlightsListClone.add(flightsDepartureList.get(i));
                                }
                            }
                        }
                    }else{
                        if (Float.parseFloat(flightsDepartureList.get(i).getPrice()) >= lowestPrice && Float.parseFloat(flightsDepartureList.get(i).getPrice()) <= highestPrice) {
                            if(checkForStops()){
                                if(checkForNonStop(flightsDepartureList.get(i).getStops())){
                                    onwardFlightsListClone.add(flightsDepartureList.get(i));
                                }
                                if(checkForOneStop(flightsDepartureList.get(i).getStops())){
                                    onwardFlightsListClone.add(flightsDepartureList.get(i));
                                }
                                if(checkForTwoOrMoreStop(flightsDepartureList.get(i).getStops())){
                                    onwardFlightsListClone.add(flightsDepartureList.get(i));
                                }
                            }else{
                                onwardFlightsListClone.add(flightsDepartureList.get(i));
                            }
                        }
                    }}
            }
            for (int j = 0; j < flightsReturnList.size(); j++) {
                if(filteredAirlines.size()>0){
                    for(String carrearName : filteredAirlines) {
                        if (carrearName.equals(flightsReturnList.get(j).getCarrierName()) && carrearName.equals(flightsReturnList.get(j).getCarrierName())){
                            if(checkForTime()){
                                if (Float.parseFloat(flightsReturnList.get(j).getPrice()) >= lowestPrice && Float.parseFloat(flightsReturnList.get(j).getPrice()) <= highestPrice){
                                    if(checkForStops()){
                                        if(checkForNonStop(flightsReturnList.get(j).getStops())){
                                            if(checkForTime1(flightsReturnList.get(j).getDepartureTime())){
                                                returnFlightsListClone.add(flightsReturnList.get(j));
                                            }
                                            if(checkForTime2(flightsReturnList.get(j).getDepartureTime())){
                                                returnFlightsListClone.add(flightsReturnList.get(j));
                                            }
                                            if(checkForTime3(flightsReturnList.get(j).getDepartureTime())){
                                                returnFlightsListClone.add(flightsReturnList.get(j));
                                            }
                                            if(checkForTime4(flightsReturnList.get(j).getDepartureTime())){
                                                returnFlightsListClone.add(flightsReturnList.get(j));
                                            }
                                        }
                                        if(checkForOneStop(flightsReturnList.get(j).getStops())){
                                            if(checkForTime1(flightsReturnList.get(j).getDepartureTime())){
                                                returnFlightsListClone.add(flightsReturnList.get(j));
                                            }
                                            if(checkForTime2(flightsReturnList.get(j).getDepartureTime())){
                                                returnFlightsListClone.add(flightsReturnList.get(j));
                                            }
                                            if(checkForTime3(flightsReturnList.get(j).getDepartureTime())){
                                                returnFlightsListClone.add(flightsReturnList.get(j));
                                            }
                                            if(checkForTime4(flightsReturnList.get(j).getDepartureTime())){
                                                returnFlightsListClone.add(flightsReturnList.get(j));
                                            }
                                        }
                                        if(checkForTwoOrMoreStop(flightsReturnList.get(j).getStops())){
                                            if(checkForTime1(flightsReturnList.get(j).getDepartureTime())){
                                                returnFlightsListClone.add(flightsReturnList.get(j));
                                            }
                                            if(checkForTime2(flightsReturnList.get(j).getDepartureTime())){
                                                returnFlightsListClone.add(flightsReturnList.get(j));
                                            }
                                            if(checkForTime3(flightsReturnList.get(j).getDepartureTime())){
                                                returnFlightsListClone.add(flightsReturnList.get(j));
                                            }
                                            if(checkForTime4(flightsReturnList.get(j).getDepartureTime())){
                                                returnFlightsListClone.add(flightsReturnList.get(j));
                                            }
                                        }
                                    }else{
                                        if(checkForTime1(flightsReturnList.get(j).getDepartureTime())){
                                            returnFlightsListClone.add(flightsReturnList.get(j));
                                        }
                                        if(checkForTime2(flightsReturnList.get(j).getDepartureTime())){
                                            returnFlightsListClone.add(flightsReturnList.get(j));
                                        }
                                        if(checkForTime3(flightsReturnList.get(j).getDepartureTime())){
                                            returnFlightsListClone.add(flightsReturnList.get(j));
                                        }
                                        if(checkForTime4(flightsReturnList.get(j).getDepartureTime())){
                                            returnFlightsListClone.add(flightsReturnList.get(j));
                                        }
                                    }
                                }
                            }else{
                                if (Float.parseFloat(flightsReturnList.get(j).getPrice()) >= lowestPrice && Float.parseFloat(flightsReturnList.get(j).getPrice()) <= highestPrice){
                                    if(checkForStops()){
                                        if(checkForNonStop(flightsReturnList.get(j).getStops())){
                                            returnFlightsListClone.add(flightsReturnList.get(j));
                                        }
                                        if(checkForOneStop(flightsReturnList.get(j).getStops())){
                                            returnFlightsListClone.add(flightsReturnList.get(j));
                                        }
                                        if(checkForTwoOrMoreStop(flightsReturnList.get(j).getStops())){
                                            returnFlightsListClone.add(flightsReturnList.get(j));
                                        }
                                    }else{
                                        returnFlightsListClone.add(flightsReturnList.get(j));
                                    }
                                }
                            }
                        }
                    }
                }else{
                    if(checkForTime()){
                        if (Float.parseFloat(flightsReturnList.get(j).getPrice()) >= lowestPrice && Float.parseFloat(flightsReturnList.get(j).getPrice()) <= highestPrice){
                            if(checkForStops()){
                                if(checkForNonStop(flightsReturnList.get(j).getStops())){
                                    if(checkForTime1(flightsReturnList.get(j).getDepartureTime())){
                                        returnFlightsListClone.add(flightsReturnList.get(j));
                                    }
                                    if(checkForTime2(flightsReturnList.get(j).getDepartureTime())){
                                        returnFlightsListClone.add(flightsReturnList.get(j));
                                    }
                                    if(checkForTime3(flightsReturnList.get(j).getDepartureTime())){
                                        returnFlightsListClone.add(flightsReturnList.get(j));
                                    }
                                    if(checkForTime4(flightsReturnList.get(j).getDepartureTime())){
                                        returnFlightsListClone.add(flightsReturnList.get(j));
                                    }
                                }
                                if(checkForOneStop(flightsReturnList.get(j).getStops())){
                                    if(checkForTime1(flightsReturnList.get(j).getDepartureTime())){
                                        returnFlightsListClone.add(flightsReturnList.get(j));
                                    }
                                    if(checkForTime2(flightsReturnList.get(j).getDepartureTime())){
                                        returnFlightsListClone.add(flightsReturnList.get(j));
                                    }
                                    if(checkForTime3(flightsReturnList.get(j).getDepartureTime())){
                                        returnFlightsListClone.add(flightsReturnList.get(j));
                                    }
                                    if(checkForTime4(flightsReturnList.get(j).getDepartureTime())){
                                        returnFlightsListClone.add(flightsReturnList.get(j));
                                    }
                                }
                                if(checkForTwoOrMoreStop(flightsReturnList.get(j).getStops())){
                                    if(checkForTime1(flightsReturnList.get(j).getDepartureTime())){
                                        returnFlightsListClone.add(flightsReturnList.get(j));
                                    }
                                    if(checkForTime2(flightsReturnList.get(j).getDepartureTime())){
                                        returnFlightsListClone.add(flightsReturnList.get(j));
                                    }
                                    if(checkForTime3(flightsReturnList.get(j).getDepartureTime())){
                                        returnFlightsListClone.add(flightsReturnList.get(j));
                                    }
                                    if(checkForTime4(flightsReturnList.get(j).getDepartureTime())){
                                        returnFlightsListClone.add(flightsReturnList.get(j));
                                    }
                                }
                            }else{
                                if(checkForTime1(flightsReturnList.get(j).getDepartureTime())){
                                    returnFlightsListClone.add(flightsReturnList.get(j));
                                }
                                if(checkForTime2(flightsReturnList.get(j).getDepartureTime())){
                                    returnFlightsListClone.add(flightsReturnList.get(j));
                                }
                                if(checkForTime3(flightsReturnList.get(j).getDepartureTime())){
                                    returnFlightsListClone.add(flightsReturnList.get(j));
                                }
                                if(checkForTime4(flightsReturnList.get(j).getDepartureTime())){
                                    returnFlightsListClone.add(flightsReturnList.get(j));
                                }
                            }
                        }
                    }else{
                        if (Float.parseFloat(flightsReturnList.get(j).getPrice()) >= lowestPrice && Float.parseFloat(flightsReturnList.get(j).getPrice()) <= highestPrice){
                            if(checkForStops()){
                                if(checkForNonStop(flightsReturnList.get(j).getStops())){
                                    returnFlightsListClone.add(flightsReturnList.get(j));
                                }
                                if(checkForOneStop(flightsReturnList.get(j).getStops())){
                                    returnFlightsListClone.add(flightsReturnList.get(j));
                                }
                                if(checkForTwoOrMoreStop(flightsReturnList.get(j).getStops())){
                                    returnFlightsListClone.add(flightsReturnList.get(j));
                                }
                            }else{
                                returnFlightsListClone.add(flightsReturnList.get(j));
                            }
                        }
                    }}
            }
            if(onwardFlightsListClone.size()>0 && returnFlightsListClone.size()>0){
                LIST_MAIN_ONWARD_FLIGHT.setAdapter(new AdapterRoundTripOnwardFlight(getActivity(),flightsDepartureList));
                LIST_MAIN_RETURN_FLIGHT.setAdapter(new AdapterRoundTripOnwardFlight(getActivity(),flightsReturnList));
                saveFilterState();
                filterDialog.dismiss();
            }else{
                Toast.makeText(context,"No flights available in this selection.",Toast.LENGTH_SHORT).show();
            }
    }

    private void saveFilterState(){
        if(nonStop.isChecked()){
            nonStopState=true;
        }else{
            nonStopState=false;
        }
        if(oneStop.isChecked()){
            oneStopState=true;
        }else{
            oneStopState=false;
        }
        if(twoOrMoreStop.isChecked()){
            twoOrMoreStopState=true;
        }else{
            twoOrMoreStopState=false;
        }
        if(time1.isChecked()){
            timeState1=true;
        }else{
            timeState1=false;
        }
        if(time2.isChecked()){
            timeState2=true;
        }else{
            timeState2=false;
        }
        if(time3.isChecked()){
            timeState3=true;
        }else{
            timeState3=false;
        }
        if(time4.isChecked()){
            timeState4=true;
        }else{
            timeState4=false;
        }

    }

    private boolean checkForStops(){
        if(nonStop.isChecked() || oneStop.isChecked() || twoOrMoreStop.isChecked()){
            return true;
        }else{
            return false;
        }
    }
    private boolean checkForNonStop(String stops){
        if(nonStop.isChecked()){
            if(stops.equals("0")){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    private boolean checkForOneStop(String stops){
        if(oneStop.isChecked()){
            if(stops.equals("1")){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    private boolean checkForTwoOrMoreStop(String stops){
        if(twoOrMoreStop.isChecked()){
            if(Integer.parseInt(stops)>=2){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    private boolean checkForTime(){
        if(time1.isChecked() || time2.isChecked() || time3.isChecked() || time4.isChecked()){
            return true;
        }else{
            return false;
        }
    }
    private boolean checkForTime1(String time){
        if(time1.isChecked()){
            if(Integer.parseInt(time.replace(":",""))<0600){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    private boolean checkForTime2(String time){
        if(time2.isChecked()){
            if(Integer.parseInt(time.replace(":",""))>=0600 && Integer.parseInt(time.replace(":",""))<1200){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    private boolean checkForTime3(String time){
        if(time3.isChecked()){
            if(Integer.parseInt(time.replace(":",""))>=1200 && Integer.parseInt(time.replace(":",""))<1800){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    private boolean checkForTime4(String time){
        if(time4.isChecked()){
            if(Integer.parseInt(time.replace(":",""))>=1800){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        AppController.getInstance().trackScreenView("Flight List Screen");
    }
}
