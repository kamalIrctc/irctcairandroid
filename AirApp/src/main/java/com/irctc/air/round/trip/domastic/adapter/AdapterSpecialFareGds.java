package com.irctc.air.round.trip.domastic.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.irctc.air.R;
import com.irctc.air.round.trip.domastic.model.Flights;
import com.irctc.air.util.AppKeys;
import com.irctc.air.util.Pref;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.irctc.air.activity.ActivityMain.context;

/**
 * Created by tourism on 4/23/2016.
 */
public class AdapterSpecialFareGds extends BaseAdapter {
    ArrayList<ArrayList<Flights>> flightsPairList;
    Context mContext;

    public AdapterSpecialFareGds(Context context, ArrayList<ArrayList<Flights>> flightsPairList) {
        mContext = context;
        this.flightsPairList = flightsPairList;
    }

    @Override
    public int getCount() {
        return flightsPairList.size();

    }

    @Override
    public Object getItem(int arg0) {
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        MyHolder holder = null;

        if (row == null) {

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row =  inflater.inflate(R.layout.view_special_fare_gds,  null);
            holder = new MyHolder(row);
            row.setTag(holder);
        } else {
            holder = (MyHolder) row.getTag();
        }

        ArrayList<Flights> flightPair = flightsPairList.get(position);
        if(flightPair.size()>1) {
            holder.TXT_ROUND_FLIGHT_NO.setText(flightPair.get(0).getCarrier() + "-" + flightPair.get(0).getFlightNumber());
            holder.TXT_ROUND_DEPART_TIME.setText(flightPair.get(0).getDepartureTime());
            holder.TXT_ROUND_ARRIVE_TIME.setText(flightPair.get(0).getArrivalTime());
            holder.TXT_ROUND_DURATION.setText(flightPair.get(0).getDuration());
            if (flightPair.get(0).getStops().equals("0")) {
                holder.TXT_ROUND_STOPS.setText("Non Stop");
            } else {
                holder.TXT_ROUND_STOPS.setText(flightPair.get(0).getStops() + " Stops");
            }
            Picasso.with(context).load(Pref.getString(context, AppKeys.FLIGHT_ICON_PATH) + flightPair.get(0).getCarrier() + ".png").into(holder.IMG_ROUND_FLIGHT_ICON);

            holder.TXT_ROUND_FLIGHT_NO1.setText(flightPair.get(1).getCarrier() + "-" + flightPair.get(1).getFlightNumber());
            holder.TXT_ROUND_DEPART_TIME1.setText(flightPair.get(1).getDepartureTime());
            holder.TXT_ROUND_ARRIVE_TIME1.setText(flightPair.get(1).getArrivalTime());
            holder.TXT_ROUND_DURATION1.setText(flightPair.get(1).getDuration());
            holder.TXT_ROUND_PRICE1.setText(context.getString(R.string.symbol_rs) + flightPair.get(1).getPrice());
            if (flightPair.get(1).getStops().equals("0")) {
                holder.TXT_ROUND_STOPS1.setText("Non Stop");
            } else {
                holder.TXT_ROUND_STOPS1.setText(flightPair.get(1).getStops() + " Stops");
            }
            Picasso.with(context).load(Pref.getString(context, AppKeys.FLIGHT_ICON_PATH) + flightPair.get(1).getCarrier() + ".png").into(holder.IMG_ROUND_FLIGHT_ICON1);
        }
        return row;
    }

    class MyHolder {
         TextView TXT_ROUND_DEPART_TIME,TXT_ROUND_ARRIVE_TIME,TXT_ROUND_DURATION,TXT_ROUND_STOPS,TXT_ROUND_FLIGHT_NO;
        ImageView  IMG_ROUND_FLIGHT_ICON;
        TextView TXT_ROUND_DEPART_TIME1,TXT_ROUND_ARRIVE_TIME1,TXT_ROUND_DURATION1,TXT_ROUND_STOPS1,TXT_ROUND_FLIGHT_NO1,TXT_ROUND_PRICE1;
        ImageView  IMG_ROUND_FLIGHT_ICON1;
        public MyHolder(View itemView) {
            IMG_ROUND_FLIGHT_ICON = (ImageView) itemView.findViewById(R.id.IMG_ROUND_FLIGHT_ICON);
            TXT_ROUND_DEPART_TIME = (TextView) itemView.findViewById(R.id.TXT_ROUND_DEPART_TIME);
            TXT_ROUND_ARRIVE_TIME = (TextView) itemView.findViewById(R.id.TXT_ROUND_ARRIVE_TIME);
            TXT_ROUND_DURATION = (TextView) itemView.findViewById(R.id.TXT_ROUND_DURATION);
            TXT_ROUND_STOPS = (TextView) itemView.findViewById(R.id.TXT_ROUND_STOPS);
            TXT_ROUND_FLIGHT_NO = (TextView) itemView.findViewById(R.id.TXT_ROUND_FLIGHT_NO);

            IMG_ROUND_FLIGHT_ICON1 = (ImageView) itemView.findViewById(R.id.IMG_ROUND_FLIGHT_ICON1);
            TXT_ROUND_DEPART_TIME1 = (TextView) itemView.findViewById(R.id.TXT_ROUND_DEPART_TIME1);
            TXT_ROUND_ARRIVE_TIME1 = (TextView) itemView.findViewById(R.id.TXT_ROUND_ARRIVE_TIME1);
            TXT_ROUND_DURATION1 = (TextView) itemView.findViewById(R.id.TXT_ROUND_DURATION1);
            TXT_ROUND_STOPS1 = (TextView) itemView.findViewById(R.id.TXT_ROUND_STOPS1);
            TXT_ROUND_FLIGHT_NO1 = (TextView) itemView.findViewById(R.id.TXT_ROUND_FLIGHT_NO1);
            TXT_ROUND_PRICE1 = (TextView) itemView.findViewById(R.id.TXT_ROUND_PRICE1);
        }
    }
}