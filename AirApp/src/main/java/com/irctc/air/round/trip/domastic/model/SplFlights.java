package com.irctc.air.round.trip.domastic.model;

public class SplFlights
{
    private Flights[] flights;

    private Airlines[] airlines;

    public Flights[] getFlights ()
    {
        return flights;
    }

    public void setFlights (Flights[] flights)
    {
        this.flights = flights;
    }

    public Airlines[] getAirlines ()
    {
        return airlines;
    }

    public void setAirlines (Airlines[] airlines)
    {
        this.airlines = airlines;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [flights = "+flights+", airlines = "+airlines+"]";
    }
}
