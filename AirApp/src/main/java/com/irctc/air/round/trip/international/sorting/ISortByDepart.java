package com.irctc.air.round.trip.international.sorting;



import com.irctc.air.model.search_result_round_trip.IntlFlights;
import com.irctc.air.round.trip.domastic.model.Flights;

import java.util.Comparator;

/**
 * Created by tourism on 4/13/2016.
 */
public class ISortByDepart implements Comparator<IntlFlights>{

    @Override
    public int compare(IntlFlights obj1,IntlFlights obj2) {

        double departTimeOne = Double.parseDouble(obj1.getDepartureTime().replace(":", "."));
        double departTimeTwo = Double.parseDouble(obj2.getDepartureTime().replace(":", "."));

//        double departTimeOne = Double.parseDouble(obj1.getFlight().get(0).getFlightDepartureTime().substring(0, 5).replace(":", "."));
//        double departTimeTwo = Double.parseDouble(obj2.getFlight().get(0).getFlightDepartureTime().substring(0, 5).replace(":", "."));

        if (departTimeOne > departTimeTwo) {
            return 1;
        } else if (departTimeOne < departTimeTwo) {
            return -1;
        } else {
            return 0;
        }
    }
}
