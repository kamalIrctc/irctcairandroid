package com.irctc.air.round.trip.domastic.sorting;



import com.irctc.air.round.trip.domastic.model.Flights;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by tourism on 4/13/2016.
 */
public class DSortByDepartGDS implements Comparator<ArrayList<Flights>>{

    @Override
    public int compare(ArrayList<Flights> obj1, ArrayList<Flights> obj2) {
        int farePriceOne = Integer.parseInt(obj1.get(0).getDepartureTime().replace(":",""));
        int farePriceTwo = Integer.parseInt(obj2.get(0).getDepartureTime().replace(":",""));
        if (farePriceOne > farePriceTwo) {
            return 1;
        } else if (farePriceOne < farePriceTwo) {
            return -1;
        } else {
            return 0;
        }
    }

}
