package com.irctc.air.round.trip.domastic.model;

public class LstFareDetails
{
    private String total;

    //private null irctcCharge;

    private String baseFare;

    //private null fareInfoRef;

    private String carrierCode;

    ///private null sgst;

    private String flightCancelPenalty;

/*
    private null paxType;

    private null addCharge;

    private null difference;

    private null originalTotalFare;

    private null gstTotal;
*/

    private String tax;

//    private null flightChangePenalty;

    private String baseType;

    private String fareRuleKey;

//    private null cgst;

    private TaxDetails taxDetails;

    private String cancellationFee;

    public String getTotal ()
    {
        return total;
    }

    public void setTotal (String total)
    {
        this.total = total;
    }

/*
    public null getIrctcCharge ()
    {
        return irctcCharge;
    }

    public void setIrctcCharge (null irctcCharge)
    {
        this.irctcCharge = irctcCharge;
    }
*/

    public String getBaseFare ()
    {
        return baseFare;
    }

    public void setBaseFare (String baseFare)
    {
        this.baseFare = baseFare;
    }

/*
    public null getFareInfoRef ()
    {
        return fareInfoRef;
    }

    public void setFareInfoRef (null fareInfoRef)
    {
        this.fareInfoRef = fareInfoRef;
    }
*/

    public String getCarrierCode ()
    {
        return carrierCode;
    }

    public void setCarrierCode (String carrierCode)
    {
        this.carrierCode = carrierCode;
    }

/*
    public null getSgst ()
    {
        return sgst;
    }

    public void setSgst (null sgst)
    {
        this.sgst = sgst;
    }
*/

    public String getFlightCancelPenalty ()
    {
        return flightCancelPenalty;
    }

    public void setFlightCancelPenalty (String flightCancelPenalty)
    {
        this.flightCancelPenalty = flightCancelPenalty;
    }

/*
    public null getPaxType ()
    {
        return paxType;
    }

    public void setPaxType (null paxType)
    {
        this.paxType = paxType;
    }

    public null getAddCharge ()
    {
        return addCharge;
    }

    public void setAddCharge (null addCharge)
    {
        this.addCharge = addCharge;
    }

    public null getDifference ()
    {
        return difference;
    }

    public void setDifference (null difference)
    {
        this.difference = difference;
    }

    public null getOriginalTotalFare ()
    {
        return originalTotalFare;
    }

    public void setOriginalTotalFare (null originalTotalFare)
    {
        this.originalTotalFare = originalTotalFare;
    }

    public null getGstTotal ()
    {
        return gstTotal;
    }

    public void setGstTotal (null gstTotal)
    {
        this.gstTotal = gstTotal;
    }
*/

    public String getTax ()
    {
        return tax;
    }

    public void setTax (String tax)
    {
        this.tax = tax;
    }

/*
    public null getFlightChangePenalty ()
    {
        return flightChangePenalty;
    }

    public void setFlightChangePenalty (null flightChangePenalty)
    {
        this.flightChangePenalty = flightChangePenalty;
    }

*/
    public String getBaseType ()
    {
        return baseType;
    }

    public void setBaseType (String baseType)
    {
        this.baseType = baseType;
    }

    public String getFareRuleKey ()
    {
        return fareRuleKey;
    }

    public void setFareRuleKey (String fareRuleKey)
    {
        this.fareRuleKey = fareRuleKey;
    }

/*
    public null getCgst ()
    {
        return cgst;
    }

    public void setCgst (null cgst)
    {
        this.cgst = cgst;
    }
*/

    public TaxDetails getTaxDetails ()
    {
        return taxDetails;
    }

    public void setTaxDetails (TaxDetails taxDetails)
    {
        this.taxDetails = taxDetails;
    }

    public String getCancellationFee ()
    {
        return cancellationFee;
    }

    public void setCancellationFee (String cancellationFee)
    {
        this.cancellationFee = cancellationFee;
    }

/*    @Override
    public String toString()
    {
        return "ClassPojo [total = "+total+", irctcCharge = "+irctcCharge+", baseFare = "+baseFare+", fareInfoRef = "+fareInfoRef+", carrierCode = "+carrierCode+", sgst = "+sgst+", flightCancelPenalty = "+flightCancelPenalty+", paxType = "+paxType+", addCharge = "+addCharge+", difference = "+difference+", originalTotalFare = "+originalTotalFare+", gstTotal = "+gstTotal+", tax = "+tax+", flightChangePenalty = "+flightChangePenalty+", baseType = "+baseType+", fareRuleKey = "+fareRuleKey+", cgst = "+cgst+", taxDetails = "+taxDetails+", cancellationFee = "+cancellationFee+"]";
    }*/
}

		