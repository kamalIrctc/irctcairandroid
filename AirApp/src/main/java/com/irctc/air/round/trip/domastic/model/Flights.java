package com.irctc.air.round.trip.domastic.model;

import android.text.TextUtils;

public class Flights
{
    private String destinationCity;

    //private null segments;

    //private null isBaggageAllowed;

    private LstBaggageDetails[] lstBaggageDetails;

    //private null originAirportName;

    private String gdsKey;

    private String departureDate;

    private String departureCityWithCode;

    private String cacheKeySPl;

    private String flightNumber;

    private String stops;

    private String carrier;

    private String arrivalDate;

    private String key;

    public String getGdsKey() {
        return gdsKey;
    }

    public void setGdsKey(String gdsKey) {
        this.gdsKey = gdsKey;
    }

    public String getServiceProvider() {
        if (!TextUtils.isEmpty(serviceProvider)){
            return serviceProvider;
        }
            return "";

    }

    public void setServiceProvider(String serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    private String serviceProvider;

    private LstFareDetails[] lstFareDetails;

    private String departureTime;

    private String cacheKeyIntl;

    private String arrivalCityWithCode;

    private String cacheKey;

    private String duration;

    //private null additionalTypeBaseAirSegment;

    private String price;

    private String arrivalTime;

    //private null destinationAirportName;

    private String carrierName;

    private String originCity;

    private String segmentType;

    private LstFlightDetails[] lstFlightDetails;

    private boolean isBaggageAllowed;
    private boolean isFreeMeal;


    public String getDestinationCity ()
    {
        return destinationCity;
    }

    public void setDestinationCity (String destinationCity)
    {
        this.destinationCity = destinationCity;
    }

/*
    public null getSegments ()
    {
        return segments;
    }

    public void setSegments (null segments)
    {
        this.segments = segments;
    }

    public null getIsBaggageAllowed ()
    {
        return isBaggageAllowed;
    }

    public void setIsBaggageAllowed (null isBaggageAllowed)
    {
        this.isBaggageAllowed = isBaggageAllowed;
    }
*/

    public LstBaggageDetails[] getLstBaggageDetails ()
    {
        return lstBaggageDetails;
    }

    public void setLstBaggageDetails (LstBaggageDetails[] lstBaggageDetails)
    {
        this.lstBaggageDetails = lstBaggageDetails;
    }

/*
    public null getOriginAirportName ()
    {
        return originAirportName;
    }

    public void setOriginAirportName (null originAirportName)
    {
        this.originAirportName = originAirportName;
    }

    public null getGdsKey ()
    {
        return gdsKey;
    }

    public void setGdsKey (null gdsKey)
    {
        this.gdsKey = gdsKey;
    }
*/

    public String getDepartureDate ()
    {
        return departureDate;
    }

    public void setDepartureDate (String departureDate)
    {
        this.departureDate = departureDate;
    }

    public String getDepartureCityWithCode ()
    {
        return departureCityWithCode;
    }

    public void setDepartureCityWithCode (String departureCityWithCode)
    {
        this.departureCityWithCode = departureCityWithCode;
    }

    public String getCacheKeySPl ()
    {
        return cacheKeySPl;
    }

    public void setCacheKeySPl (String cacheKeySPl)
    {
        this.cacheKeySPl = cacheKeySPl;
    }

    public String getFlightNumber ()
    {
        return flightNumber;
    }

    public void setFlightNumber (String flightNumber)
    {
        this.flightNumber = flightNumber;
    }

    public String getStops ()
    {
        return stops;
    }

    public void setStops (String stops)
    {
        this.stops = stops;
    }

    public String getCarrier ()
    {
        return carrier;
    }

    public void setCarrier (String carrier)
    {
        this.carrier = carrier;
    }

    public String getArrivalDate ()
    {
        return arrivalDate;
    }

    public void setArrivalDate (String arrivalDate)
    {
        this.arrivalDate = arrivalDate;
    }

    public String getKey ()
    {
        return key;
    }

    public void setKey (String key)
    {
        this.key = key;
    }


    public LstFareDetails[] getLstFareDetails ()
    {
        return lstFareDetails;
    }

    public void setLstFareDetails (LstFareDetails[] lstFareDetails)
    {
        this.lstFareDetails = lstFareDetails;
    }

    public String getDepartureTime ()
    {
        return departureTime;
    }

    public void setDepartureTime (String departureTime)
    {
        this.departureTime = departureTime;
    }

    public String getCacheKeyIntl ()
    {
        return cacheKeyIntl;
    }

    public void setCacheKeyIntl (String cacheKeyIntl)
    {
        this.cacheKeyIntl = cacheKeyIntl;
    }

    public String getArrivalCityWithCode ()
    {
        return arrivalCityWithCode;
    }

    public void setArrivalCityWithCode (String arrivalCityWithCode)
    {
        this.arrivalCityWithCode = arrivalCityWithCode;
    }

    public String getCacheKey ()
    {
        return cacheKey;
    }

    public void setCacheKey (String cacheKey)
    {
        this.cacheKey = cacheKey;
    }

    public String getDuration ()
    {
        return duration;
    }

    public void setDuration (String duration)
    {
        this.duration = duration;
    }

/*
    public null getAdditionalTypeBaseAirSegment ()
    {
        return additionalTypeBaseAirSegment;
    }

    public void setAdditionalTypeBaseAirSegment (null additionalTypeBaseAirSegment)
    {
        this.additionalTypeBaseAirSegment = additionalTypeBaseAirSegment;
    }
*/

    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }

    public String getArrivalTime ()
    {
        return arrivalTime;
    }

    public void setArrivalTime (String arrivalTime)
    {
        this.arrivalTime = arrivalTime;
    }

/*
    public null getDestinationAirportName ()
    {
        return destinationAirportName;
    }

    public void setDestinationAirportName (null destinationAirportName)
    {
        this.destinationAirportName = destinationAirportName;
    }
*/

    public String getCarrierName ()
    {
        return carrierName;
    }

    public void setCarrierName (String carrierName)
    {
        this.carrierName = carrierName;
    }

    public String getOriginCity ()
    {
        return originCity;
    }

    public void setOriginCity (String originCity)
    {
        this.originCity = originCity;
    }

    public String getSegmentType ()
    {
        return segmentType;
    }

    public void setSegmentType (String segmentType)
    {
        this.segmentType = segmentType;
    }

    public LstFlightDetails[] getLstFlightDetails ()
    {
        return lstFlightDetails;
    }

    public void setLstFlightDetails (LstFlightDetails[] lstFlightDetails)
    {
        this.lstFlightDetails = lstFlightDetails;
    }

    public boolean isBaggageAllowed() {
        return isBaggageAllowed;
    }

    public void setBaggageAllowed(boolean baggageAllowed) {
        isBaggageAllowed = baggageAllowed;
    }

    public boolean isFreeMeal() {
        return isFreeMeal;
    }

    public void setFreeMeal(boolean freeMeal) {
        isFreeMeal = freeMeal;
    }

    /*    @Override
    public String toString()
    {
        return "ClassPojo [destinationCity = "+destinationCity+", segments = "+segments+", isBaggageAllowed = "+isBaggageAllowed+", lstBaggageDetails = "+lstBaggageDetails+", originAirportName = "+originAirportName+", gdsKey = "+gdsKey+", departureDate = "+departureDate+", departureCityWithCode = "+departureCityWithCode+", cacheKeySPl = "+cacheKeySPl+", flightNumber = "+flightNumber+", stops = "+stops+", carrier = "+carrier+", arrivalDate = "+arrivalDate+", key = "+key+", serviceProvider = "+serviceProvider+", lstFareDetails = "+lstFareDetails+", departureTime = "+departureTime+", cacheKeyIntl = "+cacheKeyIntl+", arrivalCityWithCode = "+arrivalCityWithCode+", cacheKey = "+cacheKey+", duration = "+duration+", additionalTypeBaseAirSegment = "+additionalTypeBaseAirSegment+", price = "+price+", arrivalTime = "+arrivalTime+", destinationAirportName = "+destinationAirportName+", carrierName = "+carrierName+", originCity = "+originCity+", segmentType = "+segmentType+", lstFlightDetails = "+lstFlightDetails+"]";
    }*/
}

			