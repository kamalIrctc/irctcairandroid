package com.irctc.air.round.trip.domastic.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.irctc.air.R;
import com.irctc.air.fragment.FragmentOneWayFlight;
import com.irctc.air.round.trip.domastic.fragment.DSearchResults;
import com.irctc.air.util.AppKeys;
import com.irctc.air.util.Pref;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by tourism on 4/23/2016.
 */
public class DFilterAirlineAdapter extends BaseAdapter {
    static ArrayList<String> airlines;
    ArrayList<String> airFare;
    ArrayList<String> airlineIcons;
    Context mContext;

    public DFilterAirlineAdapter(Context context, ArrayList<String> airlines, ArrayList<String> airFare, ArrayList<String> airlineIcons) {

        mContext = context;
        this.airlines = airlines;
        this.airFare = airFare;
        this.airlineIcons = airlineIcons;
        if (DSearchResults.checkStatus == null) {
            DSearchResults.checkStatus = new ArrayList<>();
            for (String s : airlines) {
                DSearchResults.checkStatus.add(false);
            }
        }
    }

    @Override
    public int getCount() {

        return airlines.size();
    }

    @Override
    public Object getItem(int arg0) {

        return null;
    }

    @Override
    public long getItemId(int arg0) {

        return arg0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        MyHolder holder = null;

        if (row == null) {

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.filter_airlini_list_item, null);
            holder = new MyHolder(row);
            row.setTag(holder);

            holder.checked.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;
                    if (cb.isChecked()) {
                        DSearchResults.checkStatus.remove(position);
                        DSearchResults.checkStatus.add(position, true);
                        DSearchResults.filteredAirlines.add(airlines.get(position));
                    } else {
                        DSearchResults.checkStatus.remove(position);
                        DSearchResults.checkStatus.add(position, false);
                        DSearchResults.filteredAirlines.remove(airlines.get(position));
                    }
                }

            });
        } else {
            holder = (MyHolder) row.getTag();
        }

/*
        FilterCheapFareAirlineBean info = mDataset.get(position);

        if(lastPreferredAirline.contains(mDataset.get(position).getmFlightCode())){

            if(!checkedItemList.contains(mDataset.get(position))) {
                checkedItemList.add(mDataset.get(position));
            }
            holder.checked.setChecked(true);

        }else{
        holder.checked.setChecked(false);
            checkedItemList.remove(mDataset.get(position));
        }
*/

        //  holder.checked.setTag(info);
        //  holder.flightIcon.setImageResource(mDataset.get(position).getmFlightIcon());
        Picasso.with(mContext).load(Pref.getString(mContext, AppKeys.FLIGHT_ICON_PATH) + airlineIcons.get(position) + ".png").into(holder.flightIcon);
        if (DSearchResults.checkStatus.get(position) == true) {
            holder.checked.setChecked(true);
        } else {
            holder.checked.setChecked(false);
        }

        holder.flightName.setText(airlines.get(position));
        holder.flightMinFare.setText("\u20B9 " + airFare.get(position));// add live data
        return row;
    }

    class MyHolder {

        ImageView flightIcon;
        TextView flightName;
        CheckBox checked;
        TextView flightMinFare;

        public MyHolder(View itemView) {

            flightIcon = (ImageView) itemView.findViewById(R.id.filter_list_item_img);
            flightName = (TextView) itemView.findViewById(R.id.filter_txt_air_name);
            flightMinFare = (TextView) itemView.findViewById(R.id.filter_txt_air_min_fare);
            checked = (CheckBox) itemView.findViewById(R.id.filter_list_item_check_bx);
        }
    }
}
