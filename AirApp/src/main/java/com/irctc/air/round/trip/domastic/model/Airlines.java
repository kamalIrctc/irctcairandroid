package com.irctc.air.round.trip.domastic.model;

public class Airlines
{
    private String price;

    private String name;

    private String code;

    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [price = "+price+", name = "+name+", code = "+code+"]";
    }
}

			