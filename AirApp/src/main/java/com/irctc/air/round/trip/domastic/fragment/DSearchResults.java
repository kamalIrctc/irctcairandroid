package com.irctc.air.round.trip.domastic.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appyvet.rangebar.RangeBar;
import com.google.gson.Gson;
import com.irctc.air.AppController;
import com.irctc.air.Database.SharedPrefrenceAir;
import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.adapter.AdapterOneWayFlightInfo;
import com.irctc.air.adapter.FilterAirlineAdapter;
import com.irctc.air.fragment.FragmentOneWayFlight;
import com.irctc.air.fragment.FragmentPlanner;
import com.irctc.air.header.AirHeader;
import com.irctc.air.model.FlightFilterBean;
import com.irctc.air.model.FlightOnWardDetailBean;
import com.irctc.air.model.gst.ModelGstDetails;
import com.irctc.air.model.reprice_one_way.Data;
import com.irctc.air.model.reprice_one_way.PojoOneWayReprice;
import com.irctc.air.model.route_list.ModelRoutListItem;
import com.irctc.air.networking.Networking;
import com.irctc.air.round.trip.domastic.adapter.AdapterSpecialFare;
import com.irctc.air.round.trip.domastic.adapter.AdapterSpecialFareGds;
import com.irctc.air.round.trip.domastic.adapter.DFilterAirlineAdapter;
import com.irctc.air.round.trip.domastic.adapter.OnwardFlight;
import com.irctc.air.round.trip.domastic.model.Airlines;
import com.irctc.air.round.trip.domastic.model.Flights;
import com.irctc.air.round.trip.domastic.model.LstBaggageDetails;
import com.irctc.air.round.trip.domastic.model.LstFlightDetails;
import com.irctc.air.round.trip.domastic.sorting.DSortByDepart;
import com.irctc.air.round.trip.domastic.sorting.DSortByDepartGDS;
import com.irctc.air.round.trip.domastic.sorting.DSortByFarePrice;
import com.irctc.air.round.trip.domastic.sorting.DSortByFarePriceGDS;
import com.irctc.air.util.Constant;
import com.irctc.air.util.DateUtility;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.ListViewInsideScrollView;
import com.irctc.air.util.NetworkingUtils;
import com.irctc.air.util.OnewayFilterEventHandler;
import com.irctc.air.util.ProjectUtil;
import com.irctc.air.util.sorting.DSortByFlightName;
import com.irctc.air.util.sorting.SortByFlightName;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import static com.irctc.air.activity.ActivityMain.context;

/**
 * Created by tourism on 5/6/2016.
 */
public class DSearchResults extends Fragment implements View.OnClickListener {
    LinearLayout layFlightInfo, layBookFlight, resetFilter;
    Context context;
    Activity activity;
    public static Flights selectedDepertureFlight, selectedReturnFlight;
    public static ArrayList<Flights> onwardFlightsList, returnFlightsList;
    public static Data data;
    ListView listViewOnwardFlight, listViewReturnFlight, listViewSpecialFareGDS;
    public static boolean specialFare;
    ArrayList<ArrayList<Flights>> listCombo;
    ArrayList<Flights> selectedGdsCombo;
    Button BTN_MAIN_ONWARD_DEPART, BTN_MAIN_ONWARD_PRICE, BTN_MAIN_ONWARD_DEPART1, BTN_MAIN_ONWARD_PRICE1, BTN_MAIN_ONWARD_DEPART2, BTN_MAIN_ONWARD_PRICE2;
    LinearLayout FLOATING_BTN_ROUND;
    FrameLayout sortingNav, sortingGds;
    TextView flightBaseFare, flightTaxes, flightTotal;
    boolean sorting11, sorting12, sorting21, sorting22, sorting31, sorting32;
    //Float highestPrice, lowestPrice, selectedHighestPrice=0.0f, selectedLowestPrice=0.0f;
    Float selectedHighestPrice = 0.0f, selectedLowestPrice = 0.0f;
    RangeBar roundRangeBar;
    Button applyFilter;
    View line_ver;
    TextView minFlightFare, maxFlightFare, maxSelectedFlightFare, minSelectedFlightFare, totalPrice;
    TextView fromOnward, toOnward, fromReturn, toReturn;
    Dialog filterDialog;
    CheckBox nonStop, oneStop, twoOrMoreStop, time1, time2, time3, time4;
    ListView filterPreferFlightsListView;
    ImageView imageViewDepart1, imageViewPrice1, imageViewDepart2, imageViewPrice2, imageViewDepart3, imageViewPrice3;
    boolean nonStopState = false, oneStopState = false, twoOrMoreStopState = false, timeState1 = false, timeState2 = false, timeState3 = false, timeState4 = false;
    public static ArrayList<Boolean> checkStatus;
    public static ArrayList<String> filteredAirlines = new ArrayList<>();
    public static String gstInNumber, gstCompanyName, gstEmailId;
    public static boolean gstFlag = false;
    OnwardFlight adapterOnwardFlight, adapterReturnFlight;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().registerReceiver(broadcastReceiverSpecialFare, new IntentFilter("SPECIAL_FARE"));
        ActivityMain.backstack = true;
        specialFare = false;
        context = getActivity();
        activity = getActivity();
        Log.e("Fragment", "DSearchResults");
        makeList();
    }

    private void makeList() {
        onwardFlightsList = new ArrayList<>();
        returnFlightsList = new ArrayList<>();
        returnFlightsList.clear();
        selectedDepertureFlight = null;
        selectedReturnFlight = null;
        selectedGdsCombo = null;
        if (FragmentPlanner.searchResultData.getFlights().length > 0) {
            for (int i = 0; i < FragmentPlanner.searchResultData.getFlights().length; i++) {
                if (FragmentPlanner.searchResultData.getFlights()[i].getSegmentType().equalsIgnoreCase("O")) {
                    onwardFlightsList.add(FragmentPlanner.searchResultData.getFlights()[i]);
                } else {
                    returnFlightsList.add(FragmentPlanner.searchResultData.getFlights()[i]);
                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ActivityMain.backStack2 = true;
        ActivityMain.lastActiveFragment = Constant.PLANNER_FRAGMENT;
        getActivity().unregisterReceiver(broadcastReceiverSpecialFare);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        AirHeader.showHeaderText((ActivityMain) activity, false, "");
        AirHeader.showRecentSearchIcon(true);
        AirHeader.showDrawerToggleAndToolbar(true, true);
        View view = inflater.inflate(R.layout.fragment_round_trip_domestic_1, null);
        FLOATING_BTN_ROUND = (LinearLayout) view.findViewById(R.id.FLOATING_BTN_ROUND);
        BTN_MAIN_ONWARD_DEPART = (Button) view.findViewById(R.id.BTN_MAIN_ONWARD_DEPART);
        BTN_MAIN_ONWARD_PRICE = (Button) view.findViewById(R.id.BTN_MAIN_ONWARD_PRICE);
        BTN_MAIN_ONWARD_DEPART1 = (Button) view.findViewById(R.id.BTN_MAIN_ONWARD_DEPART1);
        BTN_MAIN_ONWARD_PRICE1 = (Button) view.findViewById(R.id.BTN_MAIN_ONWARD_PRICE1);
        BTN_MAIN_ONWARD_PRICE2 = (Button) view.findViewById(R.id.BTN_MAIN_ONWARD_PRICE2);
        BTN_MAIN_ONWARD_DEPART2 = (Button) view.findViewById(R.id.BTN_MAIN_ONWARD_DEPART2);
        imageViewDepart1 = (ImageView) view.findViewById(R.id.imageViewDepart1);
        imageViewPrice1 = (ImageView) view.findViewById(R.id.imageViewPrice1);
        imageViewDepart2 = (ImageView) view.findViewById(R.id.imageViewDepart2);
        imageViewPrice2 = (ImageView) view.findViewById(R.id.imageViewPrice2);
        imageViewDepart3 = (ImageView) view.findViewById(R.id.imageViewDepart3);
        imageViewPrice3 = (ImageView) view.findViewById(R.id.imageViewPrice3);

        totalPrice = (TextView) view.findViewById(R.id.TXT_ROUND_TOTAL_PRICE);
        line_ver = (View) view.findViewById(R.id.line_ver);
        BTN_MAIN_ONWARD_DEPART.setOnClickListener(this);
        BTN_MAIN_ONWARD_PRICE.setOnClickListener(this);
        BTN_MAIN_ONWARD_DEPART1.setOnClickListener(this);
        BTN_MAIN_ONWARD_PRICE1.setOnClickListener(this);
        BTN_MAIN_ONWARD_PRICE2.setOnClickListener(this);
        BTN_MAIN_ONWARD_DEPART2.setOnClickListener(this);
        FLOATING_BTN_ROUND.setOnClickListener(this);
        sortingGds = (FrameLayout) view.findViewById(R.id.sortingGds);
        sortingNav = (FrameLayout) view.findViewById(R.id.sortingNav);
        RecyclerView specialFareList = (RecyclerView) view.findViewById(R.id.specialFareList);
        TextView textViewAllFlights = (TextView) view.findViewById(R.id.textViewAllFlights);
        textViewAllFlights.setOnClickListener(this);
        specialFareList.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        ArrayList<Airlines> list = new ArrayList<Airlines>(Arrays.asList(FragmentPlanner.searchResultData.getSplFlights().getAirlines()));
        for (int a = 0; a < list.size(); a++) {
            ArrayList<Flights> onwardFlightsList1 = new ArrayList<>();
            for (int i = 0; i < FragmentPlanner.searchResultData.getSplFlights().getFlights().length; i++) {
                if (FragmentPlanner.searchResultData.getSplFlights().getFlights()[i].getSegmentType().equalsIgnoreCase("O") && FragmentPlanner.searchResultData.getSplFlights().getFlights()[i].getCarrier().equals(list.get(a).getCode())) {
                    onwardFlightsList1.add(FragmentPlanner.searchResultData.getSplFlights().getFlights()[i]);
                } else if (FragmentPlanner.searchResultData.getSplFlights().getFlights()[i].getCarrier().equals(list.get(a).getCode())) {
                    //returnFlightsList.add(FragmentPlanner.searchResultData.getSplFlights().getFlights()[i]);
                }
            }
            if (onwardFlightsList1.size() == 0) {
                list.remove(list.get(a));
            }
        }

        specialFareList.setAdapter(new AdapterSpecialFare(getActivity(), list));
        listViewOnwardFlight = (ListView) view.findViewById(R.id.LIST_MAIN_ONWARD_FLIGHT);
        listViewSpecialFareGDS = (ListView) view.findViewById(R.id.listViewSpecialFareGDS);
        listViewReturnFlight = (ListView) view.findViewById(R.id.LIST_MAIN_RETURN_FLIGHT);
        fromOnward = (TextView) view.findViewById(R.id.fromOnward);
        toOnward = (TextView) view.findViewById(R.id.toOnward);
        fromReturn = (TextView) view.findViewById(R.id.fromReturn);
        toReturn = (TextView) view.findViewById(R.id.toReturn);

        fromOnward.setText(FragmentPlanner.orig);
        toOnward.setText(FragmentPlanner.dest);
        fromReturn.setText(FragmentPlanner.dest);
        toReturn.setText(FragmentPlanner.orig);

        /*fromOnward.setText(FragmentPlanner.searchResultData.getFlights()[0].getDepartureCityWithCode());
        toOnward.setText(FragmentPlanner.searchResultData.getFlights()[0].getArrivalCityWithCode());
        fromReturn.setText(FragmentPlanner.searchResultData.getFlights()[0].getArrivalCityWithCode());
        toReturn.setText(FragmentPlanner.searchResultData.getFlights()[0].getDepartureCityWithCode());*/

        if (selectedDepertureFlight != null && selectedReturnFlight != null) {
            totalPrice.setText(getString(R.string.symbol_rs) + " " + String.valueOf(Integer.parseInt(selectedDepertureFlight.getPrice()) + Integer.parseInt(selectedReturnFlight.getPrice())));
        } else if (selectedDepertureFlight == null && selectedReturnFlight != null) {
            totalPrice.setText(getString(R.string.symbol_rs) + " " + String.valueOf(Integer.parseInt(onwardFlightsList.get(0).getPrice()) + Integer.parseInt(selectedReturnFlight.getPrice())));
        } else if (selectedDepertureFlight != null && selectedReturnFlight == null) {
            totalPrice.setText(getString(R.string.symbol_rs) + " " + String.valueOf(Integer.parseInt(selectedDepertureFlight.getPrice()) + Integer.parseInt(returnFlightsList.get(0).getPrice())));
        } else if (onwardFlightsList.size() > 0 && returnFlightsList.size() > 0) {
            totalPrice.setText(getString(R.string.symbol_rs) + " " + String.valueOf(Integer.parseInt(onwardFlightsList.get(0).getPrice()) + Integer.parseInt(returnFlightsList.get(0).getPrice())));
        }
        setAdapterOnBothList();
        layFlightInfo = (LinearLayout) view.findViewById(R.id.FLIGHT_INFO);
        layFlightInfo.setOnClickListener(this);
        layBookFlight = (LinearLayout) view.findViewById(R.id.LAY_ROUND_BOOKING);
        layBookFlight.setOnClickListener(this);
        return view;
    }

    private void setAdapterOnBothList() {
        sortingNav.setVisibility(View.VISIBLE);
        sortingGds.setVisibility(View.GONE);
        line_ver.setVisibility(View.VISIBLE);
        listViewSpecialFareGDS.setVisibility(View.GONE);
        listViewOnwardFlight.setVisibility(View.VISIBLE);
        listViewReturnFlight.setVisibility(View.VISIBLE);
        Collections.sort(onwardFlightsList, new DSortByFarePrice());
        adapterOnwardFlight = new OnwardFlight(getActivity(), onwardFlightsList);
        listViewOnwardFlight.setAdapter(adapterOnwardFlight);
        Collections.sort(returnFlightsList, new DSortByFarePrice());
        adapterReturnFlight = new OnwardFlight(getActivity(), returnFlightsList);
        listViewReturnFlight.setAdapter(adapterReturnFlight);
        listViewOnwardFlight.setItemChecked(0, true);
        listViewReturnFlight.setItemChecked(0, true);
        if (selectedDepertureFlight != null && selectedReturnFlight != null) {
            totalPrice.setText(getString(R.string.symbol_rs) + " " + String.valueOf(Integer.parseInt(selectedDepertureFlight.getPrice()) + Integer.parseInt(selectedReturnFlight.getPrice())));
        } else if (selectedDepertureFlight == null && selectedReturnFlight != null) {
            totalPrice.setText(getString(R.string.symbol_rs) + " " + String.valueOf(Integer.parseInt(onwardFlightsList.get(0).getPrice()) + Integer.parseInt(selectedReturnFlight.getPrice())));
        } else if (selectedDepertureFlight != null && selectedReturnFlight == null) {
            totalPrice.setText(getString(R.string.symbol_rs) + " " + String.valueOf(Integer.parseInt(selectedDepertureFlight.getPrice()) + Integer.parseInt(returnFlightsList.get(0).getPrice())));
        } else if (onwardFlightsList.size() > 0 && returnFlightsList.size() > 0) {
            totalPrice.setText(getString(R.string.symbol_rs) + " " + String.valueOf(Integer.parseInt(onwardFlightsList.get(0).getPrice()) + Integer.parseInt(returnFlightsList.get(0).getPrice())));
        }
        setOnItemClickOnListCombo(listCombo);
        setOnItemClickListenerOnBothList(onwardFlightsList, returnFlightsList);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.FLOATING_BTN_ROUND:
                if (listCombo != null) {
                    showFilterDialogGDS();
                } else {
                    showFilterDialog();
                }
                break;
            case R.id.FLIGHT_INFO:
                showSelectedFlightDetailsDialog(selectedDepertureFlight, selectedReturnFlight);
                break;
            case R.id.LAY_ROUND_BOOKING:
                if (DateUtility.getDateAndMonthFromCal(AirDataHolder.getListHolder().getList().get(0).getDepDate()).equals(DateUtility.getDateAndMonthFromCal(AirDataHolder.getListHolder().getList().get(0).getReturnDate()))) {
                    if (onwardFlightsList.size() == 0) {
                        if (selectedGdsCombo == null) {
                            selectedGdsCombo = listCombo.get(0);
                        }
                        int dep = Integer.parseInt(selectedGdsCombo.get(0).getArrivalTime().replace(":", ""));
                        int ret = Integer.parseInt(selectedGdsCombo.get(1).getDepartureTime().replace(":", ""));
                        //   Log.e("Date:",selectedGdsCombo.get(0).getArrivalDate());
                        //   Log.e("Time:",selectedGdsCombo.get(0).getArrivalTime());

                        //if (ret - dep < 200) {
                        //  Toast.makeText(context, getString(R.string.two_hour_diff), Toast.LENGTH_SHORT).show();
                        //} else {
                        showGSTPopup();
                        //}
                    } else {
                        if (selectedDepertureFlight == null) {
                            selectedDepertureFlight = onwardFlightsList.get(0);
                        }
                        if (selectedReturnFlight == null) {
                            selectedReturnFlight = returnFlightsList.get(0);
                        }
                        int dep = Integer.parseInt(selectedDepertureFlight.getArrivalTime().replace(":", ""));
                        int ret = Integer.parseInt(selectedReturnFlight.getDepartureTime().replace(":", ""));
                        SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
                        try {
                            Date getArrivalDate = formatter.parse(selectedDepertureFlight.getArrivalDate());
                            Date getDepartureDate = formatter.parse(selectedReturnFlight.getDepartureDate());
                            if (getArrivalDate.equals(getDepartureDate)) {
                                Log.e("Dep:", dep + "");
                                Log.e("Arr:", ret + "");
                                if (ret - dep < 200) {
                                    Toast.makeText(context, getString(R.string.two_hour_diff), Toast.LENGTH_SHORT).show();
                                } else {
                                    showGSTPopup();
                                }
                            } else if (getArrivalDate.after(getDepartureDate)) {
                                Toast.makeText(activity.getApplicationContext(), getString(R.string.two_hour_diff), Toast.LENGTH_SHORT).show();
                            } else {
                                showGSTPopup();
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    showGSTPopup();
                }
                break;
            case R.id.textViewAllFlights:
                checkStatus = null;
                specialFare = false;
                listCombo = null;
                nonStopState = false;
                oneStopState = false;
                twoOrMoreStopState = false;
                timeState1 = false;
                timeState2 = false;
                timeState3 = false;
                timeState4 = false;
                filteredAirlines = new ArrayList<>();
                showAllFlight();
                break;
            case R.id.BTN_MAIN_ONWARD_DEPART:
                if (!sorting11) {
                    sorting11 = true;
                    Collections.sort(onwardFlightsList, new DSortByDepart());
                    adapterOnwardFlight = new OnwardFlight(getActivity(), onwardFlightsList);
                    listViewOnwardFlight.setAdapter(adapterOnwardFlight);
                    setOnItemClickListenerOnBothList(onwardFlightsList, returnFlightsList);
                    imageViewDepart1.setImageResource(R.drawable.down_arrow);
                } else {
                    sorting11 = false;
                    Collections.sort(onwardFlightsList, new DSortByDepart());
                    Collections.reverse(onwardFlightsList);
                    adapterOnwardFlight = new OnwardFlight(getActivity(), onwardFlightsList);
                    listViewOnwardFlight.setAdapter(adapterOnwardFlight);
                    setOnItemClickListenerOnBothList(onwardFlightsList, returnFlightsList);
                    imageViewDepart1.setImageResource(R.drawable.up_arrow);
                }
                BTN_MAIN_ONWARD_DEPART.setBackgroundColor(ContextCompat.getColor(context, R.color.colorLightBlue));
                BTN_MAIN_ONWARD_DEPART.setTextColor(ContextCompat.getColor(context, android.R.color.white));
                BTN_MAIN_ONWARD_PRICE.setBackgroundColor(ContextCompat.getColor(context, android.R.color.white));
                BTN_MAIN_ONWARD_PRICE.setTextColor(ContextCompat.getColor(context, R.color.colorLightBlue));
                break;
            case R.id.BTN_MAIN_ONWARD_PRICE:
                if (!sorting12) {
                    sorting12 = true;
                    Collections.sort(onwardFlightsList, new DSortByFarePrice());
                    adapterOnwardFlight = new OnwardFlight(getActivity(), onwardFlightsList);
                    listViewOnwardFlight.setAdapter(adapterOnwardFlight);
                    setOnItemClickListenerOnBothList(onwardFlightsList, returnFlightsList);
                    imageViewPrice1.setImageResource(R.drawable.down_arrow);
                } else {
                    sorting12 = false;
                    Collections.sort(onwardFlightsList, new DSortByFarePrice());
                    Collections.reverse(onwardFlightsList);
                    adapterOnwardFlight = new OnwardFlight(getActivity(), onwardFlightsList);
                    listViewOnwardFlight.setAdapter(adapterOnwardFlight);
                    setOnItemClickListenerOnBothList(onwardFlightsList, returnFlightsList);
                    imageViewPrice1.setImageResource(R.drawable.up_arrow);
                }
                BTN_MAIN_ONWARD_PRICE.setBackgroundColor(ContextCompat.getColor(context, R.color.colorLightBlue));
                BTN_MAIN_ONWARD_PRICE.setTextColor(ContextCompat.getColor(context, android.R.color.white));
                BTN_MAIN_ONWARD_DEPART.setBackgroundColor(ContextCompat.getColor(context, android.R.color.white));
                BTN_MAIN_ONWARD_DEPART.setTextColor(ContextCompat.getColor(context, R.color.colorLightBlue));
                break;
            case R.id.BTN_MAIN_ONWARD_DEPART1:
                if (!sorting21) {
                    sorting21 = true;
                    Collections.sort(returnFlightsList, new DSortByDepart());
                    adapterReturnFlight = new OnwardFlight(getActivity(), returnFlightsList);
                    listViewReturnFlight.setAdapter(adapterReturnFlight);
                    setOnItemClickListenerOnBothList(onwardFlightsList, returnFlightsList);
                    imageViewDepart2.setImageResource(R.drawable.down_arrow);
                } else {
                    sorting21 = false;
                    Collections.sort(returnFlightsList, new DSortByDepart());
                    Collections.reverse(returnFlightsList);
                    adapterReturnFlight = new OnwardFlight(getActivity(), returnFlightsList);
                    listViewReturnFlight.setAdapter(adapterReturnFlight);
                    setOnItemClickListenerOnBothList(onwardFlightsList, returnFlightsList);
                    imageViewDepart2.setImageResource(R.drawable.up_arrow);
                }
                BTN_MAIN_ONWARD_DEPART1.setBackgroundColor(ContextCompat.getColor(context, R.color.colorLightBlue));
                BTN_MAIN_ONWARD_DEPART1.setTextColor(ContextCompat.getColor(context, android.R.color.white));
                BTN_MAIN_ONWARD_PRICE1.setBackgroundColor(ContextCompat.getColor(context, android.R.color.white));
                BTN_MAIN_ONWARD_PRICE1.setTextColor(ContextCompat.getColor(context, R.color.colorLightBlue));
                break;
            case R.id.BTN_MAIN_ONWARD_PRICE1:
                if (!sorting22) {
                    sorting22 = true;
                    Collections.sort(returnFlightsList, new DSortByFarePrice());
                    adapterReturnFlight = new OnwardFlight(getActivity(), returnFlightsList);
                    listViewReturnFlight.setAdapter(adapterReturnFlight);
                    setOnItemClickListenerOnBothList(onwardFlightsList, returnFlightsList);
                    imageViewPrice2.setImageResource(R.drawable.down_arrow);
                } else {
                    sorting22 = false;
                    Collections.sort(returnFlightsList, new DSortByFarePrice());
                    Collections.reverse(returnFlightsList);
                    adapterReturnFlight = new OnwardFlight(getActivity(), returnFlightsList);
                    listViewReturnFlight.setAdapter(adapterReturnFlight);
                    setOnItemClickListenerOnBothList(onwardFlightsList, returnFlightsList);
                    imageViewPrice2.setImageResource(R.drawable.up_arrow);
                }
                BTN_MAIN_ONWARD_PRICE1.setBackgroundColor(ContextCompat.getColor(context, R.color.colorLightBlue));
                BTN_MAIN_ONWARD_PRICE1.setTextColor(ContextCompat.getColor(context, android.R.color.white));
                BTN_MAIN_ONWARD_DEPART1.setBackgroundColor(ContextCompat.getColor(context, android.R.color.white));
                BTN_MAIN_ONWARD_DEPART1.setTextColor(ContextCompat.getColor(context, R.color.colorLightBlue));
                break;
            case R.id.BTN_MAIN_ONWARD_DEPART2:
                if (!sorting31) {
                    Collections.sort(listCombo, new DSortByDepartGDS());
                    sorting31 = true;
                    imageViewDepart3.setImageResource(R.drawable.down_arrow);
                } else {
                    Collections.sort(listCombo, new DSortByDepartGDS());
                    Collections.reverse(listCombo);
                    sorting31 = false;
                    imageViewDepart3.setImageResource(R.drawable.up_arrow);
                }
                listViewSpecialFareGDS.setAdapter(new AdapterSpecialFareGds(getActivity(), listCombo));
                setOnItemClickOnListCombo(listCombo);
                BTN_MAIN_ONWARD_DEPART2.setBackgroundColor(ContextCompat.getColor(context, R.color.colorLightBlue));
                BTN_MAIN_ONWARD_DEPART2.setTextColor(ContextCompat.getColor(context, android.R.color.white));
                BTN_MAIN_ONWARD_PRICE2.setBackgroundColor(ContextCompat.getColor(context, android.R.color.white));
                BTN_MAIN_ONWARD_PRICE2.setTextColor(ContextCompat.getColor(context, R.color.colorLightBlue));
                break;
            case R.id.BTN_MAIN_ONWARD_PRICE2:
                if (!sorting32) {
                    Collections.sort(listCombo, new DSortByFarePriceGDS());
                    sorting32 = true;
                    imageViewPrice3.setImageResource(R.drawable.down_arrow);
                } else {
                    Collections.sort(listCombo, new DSortByFarePriceGDS());
                    Collections.reverse(listCombo);
                    sorting32 = false;
                    imageViewPrice3.setImageResource(R.drawable.up_arrow);
                }
                listViewSpecialFareGDS.setAdapter(new AdapterSpecialFareGds(getActivity(), listCombo));
                setOnItemClickOnListCombo(listCombo);
                BTN_MAIN_ONWARD_PRICE2.setBackgroundColor(ContextCompat.getColor(context, R.color.colorLightBlue));
                BTN_MAIN_ONWARD_PRICE2.setTextColor(ContextCompat.getColor(context, android.R.color.white));
                BTN_MAIN_ONWARD_DEPART2.setBackgroundColor(ContextCompat.getColor(context, android.R.color.white));
                BTN_MAIN_ONWARD_DEPART2.setTextColor(ContextCompat.getColor(context, R.color.colorLightBlue));
                break;
        }
    }

    private void showGSTPopup() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        dialog.dismiss();
                        showFillGSTDetailsPopup();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        reprice();
                        dialog.dismiss();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Do you want to use GST number for this booking?").setPositiveButton("Yes", dialogClickListener).setNegativeButton("No", dialogClickListener).show();
    }

    private void showFillGSTDetailsPopup() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.view_fill_gst_details);
        Button dialogButton = (Button) dialog.findViewById(R.id.btnOk);

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkValidations(dialog);
            }
        });
        dialog.show();
    }

    private void checkValidations(Dialog dialog) {
        AutoCompleteTextView gstinNumber = (AutoCompleteTextView) dialog.findViewById(R.id.gstinNumber);
        AutoCompleteTextView companyName = (AutoCompleteTextView) dialog.findViewById(R.id.companyName);
        AutoCompleteTextView emailId = (AutoCompleteTextView) dialog.findViewById(R.id.emailId);
        TextView textViewWarning = (TextView) dialog.findViewById(R.id.textViewWarning);
        if (gstinNumber.getText().toString().trim().length() == 0) {
            textViewWarning.setVisibility(View.VISIBLE);
            textViewWarning.setText("GSTIN Number Can't be blank");
        } else if (!gstinNumber.getText().toString().trim().matches("^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$")) {
            textViewWarning.setVisibility(View.VISIBLE);
            textViewWarning.setText("GSTIN Number is Invalid");
        } else if (companyName.getText().toString().trim().length() == 0) {
            textViewWarning.setVisibility(View.VISIBLE);
            textViewWarning.setText("Company Name Can't be blank");
        } else if (emailId.getText().toString().trim().length() == 0) {
            textViewWarning.setVisibility(View.VISIBLE);
            textViewWarning.setText("Email-Id Can't be blank");
        } else if (!Patterns.EMAIL_ADDRESS.matcher(emailId.getText().toString().trim()).matches()) {
            textViewWarning.setVisibility(View.VISIBLE);
            textViewWarning.setText("Email-Id is Invalid");
        } else {
            gstFlag = true;
            gstInNumber = gstinNumber.getText().toString().trim();
            gstCompanyName = companyName.getText().toString().trim();
            gstEmailId = emailId.getText().toString().trim();
            dialog.dismiss();
            reprice();
        }

    }

    private void showSelectedFlightDetailsDialog(Flights selectedDepertureFlight, Flights selectedReturnFlight) {
        if (specialFare) {
            if (listCombo != null) {
                if (selectedGdsCombo == null) {
                    selectedDepertureFlight = listCombo.get(0).get(0);
                    selectedReturnFlight = listCombo.get(0).get(1);
                } else {
                    selectedDepertureFlight = selectedGdsCombo.get(0);
                    selectedReturnFlight = selectedGdsCombo.get(1);
                }
            } else {
                if (selectedDepertureFlight == null) {
                    selectedDepertureFlight = onwardFlightsList.get(0);
                }
                if (selectedReturnFlight == null) {
                    selectedReturnFlight = returnFlightsList.get(0);
                }
            }
        } else {
            if (selectedDepertureFlight == null) {
                selectedDepertureFlight = onwardFlightsList.get(0);
            }
            if (selectedReturnFlight == null) {
                selectedReturnFlight = returnFlightsList.get(0);
            }
        }
        final Dialog flightDetailDialog = new Dialog(context, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
        flightDetailDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        flightDetailDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        flightDetailDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.LTGRAY));
        flightDetailDialog.setContentView(R.layout.round_trip_gds_flight_info);
        TextView txtOnewayRefundable1 = (TextView) flightDetailDialog.findViewById(R.id.INTER_ONWARD_REFUNDABLE_TXT);
        TextView txtOnewayRefundable2 = (TextView) flightDetailDialog.findViewById(R.id.INTER_RETURN_REFUNDABLE_TXT);
        TextView tv_no_free_meel = (TextView) flightDetailDialog.findViewById(R.id.tv_no_free_meel);
        TextView tv_no_free_meel_return = (TextView) flightDetailDialog.findViewById(R.id.tv_no_free_meel_return);
        ImageView imgOnewayRefundable1 = (ImageView) flightDetailDialog.findViewById(R.id.INTER_ONWARD_REFUNDABLE_IMG);
        ImageView imgOnewayRefundable2 = (ImageView) flightDetailDialog.findViewById(R.id.INTER_RETURN_REFUNDABLE_IMG);


        TextView tvOnlyHandBaggage = (TextView) flightDetailDialog.findViewById(R.id.tvOnlyHandBaggage);
        TextView tvOnlyHandBaggage_return = (TextView) flightDetailDialog.findViewById(R.id.tvOnlyHandBaggage_return);
        ImageView ivBag = (ImageView) flightDetailDialog.findViewById(R.id.ivBag);
        ImageView ivBag_return = (ImageView) flightDetailDialog.findViewById(R.id.ivBag_return);

        ((TextView) flightDetailDialog.findViewById(R.id.flightClass1)).setText("(" + FragmentPlanner.flightTravellClass + ")");
        ((TextView) flightDetailDialog.findViewById(R.id.flightClass2)).setText("(" + FragmentPlanner.flightTravellClass + ")");
        if (selectedDepertureFlight.getLstFareDetails()[0].getBaseType().equals("false")) {
            imgOnewayRefundable1.setImageResource(R.drawable.refundable_red);
            txtOnewayRefundable1.setText("Non Refundable");
        } else if (selectedDepertureFlight.getLstFareDetails()[0].getBaseType().equals("true")) {
            imgOnewayRefundable1.setImageResource(R.drawable.refundable_green);
            txtOnewayRefundable1.setText("Refundable");
        }
        if (selectedReturnFlight.getLstFareDetails()[0].getBaseType().equals("false")) {
            imgOnewayRefundable2.setImageResource(R.drawable.refundable_red);
            txtOnewayRefundable2.setText("Non Refundable");
        } else if (selectedReturnFlight.getLstFareDetails()[0].getBaseType().equals("true")) {
            imgOnewayRefundable2.setImageResource(R.drawable.refundable_green);
            txtOnewayRefundable2.setText("Refundable");
        }
        if (selectedDepertureFlight.isBaggageAllowed()) {
            ivBag.setVisibility(View.VISIBLE);
            tvOnlyHandBaggage.setVisibility(View.VISIBLE);
        } else {
            ivBag.setVisibility(View.GONE);
            tvOnlyHandBaggage.setVisibility(View.GONE);
        }
        if (selectedReturnFlight.isBaggageAllowed()) {
            ivBag_return.setVisibility(View.VISIBLE);
            tvOnlyHandBaggage_return.setVisibility(View.VISIBLE);
        } else {
            ivBag_return.setVisibility(View.GONE);
            tvOnlyHandBaggage_return.setVisibility(View.GONE);
        }
        LstBaggageDetails lstBaggageDetails = new LstBaggageDetails();
        for (int i = 0; i < selectedDepertureFlight.getLstBaggageDetails().length; i++) {
            if (selectedDepertureFlight.getLstBaggageDetails()[i].getPaxType().equalsIgnoreCase("ADT")){
                lstBaggageDetails = selectedDepertureFlight.getLstBaggageDetails()[i];
                break;
            }
        }
        LstBaggageDetails lstBaggageDetailsReturn = new LstBaggageDetails();
        for (int i = 0; i < selectedReturnFlight.getLstBaggageDetails().length; i++) {
            if (selectedReturnFlight.getLstBaggageDetails()[i].getPaxType().equalsIgnoreCase("ADT")){
                lstBaggageDetailsReturn = selectedReturnFlight.getLstBaggageDetails()[i];
                break;
            }
        }

        ListView flightOnward = (ListView) flightDetailDialog.findViewById(R.id.flightOnward);
        ArrayList<ModelRoutListItem> arrayListDeparture = new ArrayList<>();
        for (int i = 0; i < selectedDepertureFlight.getLstFlightDetails().length; i++) {
            ModelRoutListItem modelRoutListItem = new ModelRoutListItem();
            String flightCode = selectedDepertureFlight.getLstFlightDetails()[i].getFlightNumber();
            String fromAirportCode = selectedDepertureFlight.getLstFlightDetails()[i].getOrigin();
            String fromAirportName = selectedDepertureFlight.getLstFlightDetails()[i].getOriginCity();
            String fromAirportTerminal = selectedDepertureFlight.getLstFlightDetails()[i].getOriginTerminal();
            String toAirportCode = selectedDepertureFlight.getLstFlightDetails()[i].getDestination();
            String toAirportName = selectedDepertureFlight.getLstFlightDetails()[i].getDestinationCity();
            String toAirportTerminal = selectedDepertureFlight.getLstFlightDetails()[i].getDestinationTerminal();
            String travelDuration = selectedDepertureFlight.getLstFlightDetails()[i].getFlightTime();
            String departureDate = selectedDepertureFlight.getLstFlightDetails()[i].getDepartureDate();
            String departureTime = selectedDepertureFlight.getLstFlightDetails()[i].getDepartureTime();
            String arrivalDate = selectedDepertureFlight.getLstFlightDetails()[i].getArrivalDate();
            String arrivalTime = selectedDepertureFlight.getLstFlightDetails()[i].getArrivalTime();
            String halt = selectedDepertureFlight.getLstFlightDetails()[i].getHalt();
            String serviceProvider = selectedDepertureFlight.getServiceProvider();
            modelRoutListItem.setFlightCode(flightCode);
            modelRoutListItem.setLstBaggageDetails(lstBaggageDetails);
            modelRoutListItem.setServiceProvider(serviceProvider);
            modelRoutListItem.setFreeMeal(selectedDepertureFlight.getLstFlightDetails()[i].isFreeMeal());
            modelRoutListItem.setCarrear(selectedDepertureFlight.getLstFlightDetails()[i].getAirlineCode());
            modelRoutListItem.setFromAirportCode(fromAirportCode);
            modelRoutListItem.setFromAirportName(fromAirportName);
            modelRoutListItem.setFromAirportTerminal(fromAirportTerminal);
            modelRoutListItem.setToAirportCode(toAirportCode);
            modelRoutListItem.setToAirportName(toAirportName);
            modelRoutListItem.setToAirportTerminal(toAirportTerminal);
            modelRoutListItem.setTravelDuration(travelDuration);
            SimpleDateFormat formatter5 = new SimpleDateFormat("yyyy-MM-dd");
            Date depDate = null, arrDate = null;
            try {
                depDate = formatter5.parse(departureDate.substring(0, 10));
                arrDate = formatter5.parse(arrivalDate.substring(0, 10));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            modelRoutListItem.setDepartureDate(new SimpleDateFormat("dd MMM yyyy").format(depDate));
            modelRoutListItem.setArrivalDate(new SimpleDateFormat("dd MMM yyyy").format(arrDate));
            modelRoutListItem.setDepartureTime(departureTime);
            modelRoutListItem.setArrivalTime(arrivalTime);
            modelRoutListItem.setHalt(halt);
            modelRoutListItem.setServiceProvider(serviceProvider);
            arrayListDeparture.add(modelRoutListItem);
        }


        flightOnward.setAdapter(new AdapterOneWayFlightInfo(context, arrayListDeparture));
        ListViewInsideScrollView.open(flightOnward);
        ListView flightReturn = (ListView) flightDetailDialog.findViewById(R.id.flightReturn);
        ArrayList<ModelRoutListItem> arrayListReturn = new ArrayList<>();
        for (int j = 0; j < selectedReturnFlight.getLstFlightDetails().length; j++) {
            ModelRoutListItem modelRoutListItem = new ModelRoutListItem();
            String flightCode = selectedReturnFlight.getLstFlightDetails()[j].getFlightNumber();
            String fromAirportCode = selectedReturnFlight.getLstFlightDetails()[j].getOrigin();
            String fromAirportName = selectedReturnFlight.getLstFlightDetails()[j].getOriginCity();
            String fromAirportTerminal = selectedReturnFlight.getLstFlightDetails()[j].getOriginTerminal();
            String toAirportCode = selectedReturnFlight.getLstFlightDetails()[j].getDestination();
            String toAirportName = selectedReturnFlight.getLstFlightDetails()[j].getDestinationCity();
            String toAirportTerminal = selectedReturnFlight.getLstFlightDetails()[j].getDestinationTerminal();
            String travelDuration = selectedReturnFlight.getLstFlightDetails()[j].getFlightTime();
            String departureDate = selectedReturnFlight.getLstFlightDetails()[j].getDepartureDate();
            String departureTime = selectedReturnFlight.getLstFlightDetails()[j].getDepartureTime();
            String arrivalDate = selectedReturnFlight.getLstFlightDetails()[j].getArrivalDate();
            String arrivalTime = selectedReturnFlight.getLstFlightDetails()[j].getArrivalTime();
            String halt = selectedReturnFlight.getLstFlightDetails()[j].getHalt();
            String serviceProvider = selectedReturnFlight.getServiceProvider();
            modelRoutListItem.setFlightCode(flightCode);
            modelRoutListItem.setLstBaggageDetails(lstBaggageDetailsReturn);
            modelRoutListItem.setServiceProvider(serviceProvider);
            modelRoutListItem.setFreeMeal(selectedReturnFlight.getLstFlightDetails()[j].isFreeMeal());
            modelRoutListItem.setCarrear(selectedReturnFlight.getLstFlightDetails()[j].getAirlineCode());
            modelRoutListItem.setFromAirportCode(fromAirportCode);
            modelRoutListItem.setFromAirportName(fromAirportName);
            modelRoutListItem.setFromAirportTerminal(fromAirportTerminal);
            modelRoutListItem.setToAirportCode(toAirportCode);
            modelRoutListItem.setToAirportName(toAirportName);
            modelRoutListItem.setToAirportTerminal(toAirportTerminal);
            modelRoutListItem.setTravelDuration(travelDuration);
            SimpleDateFormat formatter5 = new SimpleDateFormat("yyyy-MM-dd");
            Date depDate = null, arrDate = null;
            try {
                depDate = formatter5.parse(departureDate.substring(0, 10));
                arrDate = formatter5.parse(arrivalDate.substring(0, 10));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            modelRoutListItem.setDepartureDate(new SimpleDateFormat("dd MMM yyyy").format(depDate));
            modelRoutListItem.setArrivalDate(new SimpleDateFormat("dd MMM yyyy").format(arrDate));
            modelRoutListItem.setDepartureTime(departureTime);
            modelRoutListItem.setArrivalTime(arrivalTime);
            modelRoutListItem.setHalt(halt);
            modelRoutListItem.setServiceProvider(serviceProvider);
            arrayListReturn.add(modelRoutListItem);
        }
        flightReturn.setAdapter(new AdapterOneWayFlightInfo(context, arrayListReturn));
        ListViewInsideScrollView.open(flightReturn);
        flightBaseFare = (TextView) flightDetailDialog.findViewById(R.id.TXT_FARE_BASE);
        flightTaxes = (TextView) flightDetailDialog.findViewById(R.id.TXT_FARE_TAXES);
        flightTotal = (TextView) flightDetailDialog.findViewById(R.id.TXT_FARE_TOTAL);
        Button lObjBtnDone = (Button) flightDetailDialog.findViewById(R.id.BTN_FLIGHT_DETAIL_DONE);
        lObjBtnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flightDetailDialog.cancel();
            }
        });
        if (specialFare) {
            if (listCombo != null) {
                setSpecialFarePrice(selectedDepertureFlight);
            } else {
                setPrice(selectedDepertureFlight, selectedReturnFlight);
            }
        } else {
            setPrice(selectedDepertureFlight, selectedReturnFlight);
        }
        flightDetailDialog.show();
    }

    private void setPrice(Flights selectedDepertureFlight, Flights selectedReturnFlight) {
        float baseFare = 0.0f, tax = 0.0f, total = 0.0f;
        if (selectedDepertureFlight.getServiceProvider() == null) {
            for (int i = 0; i < selectedDepertureFlight.getLstFareDetails().length; i++) {
                try {
                    baseFare = baseFare + Float.parseFloat(selectedDepertureFlight.getLstFareDetails()[i].getBaseFare());
                } catch (Exception e) {
                }
                try {
                    tax = tax + Float.parseFloat(selectedDepertureFlight.getLstFareDetails()[i].getTax());
                } catch (Exception e) {
                }
                try {
                    total = total + Float.parseFloat(selectedDepertureFlight.getLstFareDetails()[i].getTotal());
                } catch (Exception e) {
                }
            }
        } else if (selectedDepertureFlight.getServiceProvider().equalsIgnoreCase("GDS")) {
            for (int i = 0; i < selectedDepertureFlight.getLstFareDetails().length; i++) {
                try {
                    baseFare = baseFare + Float.parseFloat(selectedDepertureFlight.getLstFareDetails()[i].getBaseFare());
                } catch (Exception e) {
                }
                try {
                    tax = tax + Float.parseFloat(selectedDepertureFlight.getLstFareDetails()[i].getTax());
                } catch (Exception e) {
                }
                try {
                    total = total + Float.parseFloat(selectedDepertureFlight.getLstFareDetails()[i].getTotal());
                } catch (Exception e) {
                }
            }
        } else {
            baseFare = baseFare + Float.parseFloat(selectedDepertureFlight.getLstFareDetails()[0].getBaseFare());
            tax = tax + Float.parseFloat(selectedDepertureFlight.getLstFareDetails()[0].getTax());
            total = total + Float.parseFloat(selectedDepertureFlight.getLstFareDetails()[0].getTotal());
        }
        if (selectedReturnFlight.getServiceProvider() == null) {
            for (int i = 0; i < selectedReturnFlight.getLstFareDetails().length; i++) {
                try {
                    baseFare = baseFare + Float.parseFloat(selectedReturnFlight.getLstFareDetails()[i].getBaseFare());
                } catch (Exception e) {
                }
                try {
                    tax = tax + Float.parseFloat(selectedReturnFlight.getLstFareDetails()[i].getTax());
                } catch (Exception e) {
                }
                try {
                    total = total + Float.parseFloat(selectedReturnFlight.getLstFareDetails()[i].getTotal());
                } catch (Exception e) {
                }
            }
        } else if (selectedReturnFlight.getServiceProvider().equalsIgnoreCase("GDS")) {
            for (int i = 0; i < selectedReturnFlight.getLstFareDetails().length; i++) {
                try {
                    baseFare = baseFare + Float.parseFloat(selectedReturnFlight.getLstFareDetails()[i].getBaseFare());
                } catch (Exception e) {
                }
                try {
                    tax = tax + Float.parseFloat(selectedReturnFlight.getLstFareDetails()[i].getTax());
                } catch (Exception e) {
                }
                try {
                    total = total + Float.parseFloat(selectedReturnFlight.getLstFareDetails()[i].getTotal());
                } catch (Exception e) {
                }
            }
        } else {
            baseFare = baseFare + Float.parseFloat(selectedReturnFlight.getLstFareDetails()[0].getBaseFare());
            tax = tax + Float.parseFloat(selectedReturnFlight.getLstFareDetails()[0].getTax());
            total = total + Float.parseFloat(selectedReturnFlight.getLstFareDetails()[0].getTotal());
        }
        flightBaseFare.setText(getString(R.string.Rs) + baseFare);
        flightTaxes.setText(getString(R.string.Rs) + tax);
        flightTotal.setText(getString(R.string.Rs) + total);

    }

    private void setSpecialFarePrice(Flights selectedDepertureFlight) {
        float baseFare = 0.0f, tax = 0.0f, total = 0.0f;
        if (selectedDepertureFlight.getServiceProvider() == null) {
            for (int i = 0; i < selectedDepertureFlight.getLstFareDetails().length; i++) {
                try {
                    baseFare = baseFare + Float.parseFloat(selectedDepertureFlight.getLstFareDetails()[i].getBaseFare());
                } catch (Exception e) {
                }
                try {
                    tax = tax + Float.parseFloat(selectedDepertureFlight.getLstFareDetails()[i].getTax());
                } catch (Exception e) {
                }
                try {
                    total = total + Float.parseFloat(selectedDepertureFlight.getLstFareDetails()[i].getTotal());
                } catch (Exception e) {
                }
            }
        } else if (selectedDepertureFlight.getServiceProvider().equalsIgnoreCase("GDS")) {
            for (int i = 0; i < selectedDepertureFlight.getLstFareDetails().length; i++) {
                try {
                    baseFare = baseFare + Float.parseFloat(selectedDepertureFlight.getLstFareDetails()[i].getBaseFare());
                } catch (Exception e) {
                }
                try {
                    tax = tax + Float.parseFloat(selectedDepertureFlight.getLstFareDetails()[i].getTax());
                } catch (Exception e) {
                }
                try {
                    total = total + Float.parseFloat(selectedDepertureFlight.getLstFareDetails()[i].getTotal());
                } catch (Exception e) {
                }
            }
        } else {
            baseFare = baseFare + Float.parseFloat(selectedDepertureFlight.getLstFareDetails()[0].getBaseFare());
            tax = tax + Float.parseFloat(selectedDepertureFlight.getLstFareDetails()[0].getTax());
            total = total + Float.parseFloat(selectedDepertureFlight.getLstFareDetails()[0].getTotal());
        }
        flightBaseFare.setText(getString(R.string.Rs) + baseFare);
        flightTaxes.setText(getString(R.string.Rs) + tax);
        flightTotal.setText(getString(R.string.Rs) + total);

    }

    private void reprice() {
        ModelGstDetails modelGstDetails = new ModelGstDetails();
        modelGstDetails.setGstflag(gstFlag);
        modelGstDetails.setGstNumber(gstInNumber);
        modelGstDetails.setCompanyName(gstCompanyName);
        modelGstDetails.setEmailid(gstEmailId);
        NetworkingUtils.showProgress(getActivity());
        if (specialFare) {
            String total = "";
            if (listCombo != null) {
                if (selectedGdsCombo == null) {
                    selectedDepertureFlight = listCombo.get(0).get(0);
                    selectedReturnFlight = listCombo.get(0).get(1);
                } else {
                    selectedDepertureFlight = selectedGdsCombo.get(0);
                    selectedReturnFlight = selectedGdsCombo.get(1);
                }
                total = String.valueOf(Float.parseFloat(selectedDepertureFlight.getLstFareDetails()[0].getTotal()));
            } else {
                if (selectedDepertureFlight == null) {
                    selectedDepertureFlight = onwardFlightsList.get(0);
                }
                if (selectedReturnFlight == null) {
                    selectedReturnFlight = returnFlightsList.get(0);
                }
                total = String.valueOf(Float.parseFloat(selectedDepertureFlight.getLstFareDetails()[0].getTotal()) + Float.parseFloat(selectedReturnFlight.getLstFareDetails()[0].getTotal()));
            }
            String[] flightKeys = new String[]{selectedDepertureFlight.getKey(), selectedReturnFlight.getKey()};
            String[] gdsKeys = new String[]{selectedDepertureFlight.getGdsKey(), selectedReturnFlight.getGdsKey()};
            LstFlightDetails[] lstFlightDetailses1 = selectedDepertureFlight.getLstFlightDetails();
            StringBuilder segKeysString1 = new StringBuilder();
            ArrayList<String> segKeyList = new ArrayList<>();
            if (lstFlightDetailses1.length > 0) {
                for (int i = 0; i < lstFlightDetailses1.length; i++) {
                    segKeyList.add(lstFlightDetailses1[i].getKey());
                }
            }
            LstFlightDetails[] lstFlightDetailses2 = selectedReturnFlight.getLstFlightDetails();
            StringBuilder segKeysString2 = new StringBuilder();
            if (lstFlightDetailses2.length > 0) {
                for (int i = 0; i < lstFlightDetailses2.length; i++) {
                    segKeyList.add(lstFlightDetailses2[i].getKey());
                }
            }

            segKeysString1.append(",");
            segKeysString1.append(segKeysString2);

            String[] segKeys = new String[segKeyList.size()];
            for (int i = 0; i < segKeyList.size(); i++) {
                segKeys[i] = segKeyList.get(i);
            }
            String serviceProvider = selectedDepertureFlight.getServiceProvider();
            if (serviceProvider == null) {
                serviceProvider = "GDS";
            }
            Networking.repriceSpecialFare(total, FragmentPlanner.searchResultData.getSearchKey(), flightKeys, FragmentPlanner.searchResultData.getIsInternational(), gdsKeys, segKeys, serviceProvider, FragmentPlanner.noOfAdults, FragmentPlanner.noOfChildren, FragmentPlanner.noOfInfants, modelGstDetails, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("Response", response.toString());
                    NetworkingUtils.dismissProgress();
                    Gson gson = new Gson();
                    PojoOneWayReprice pojoOneWayReprice = gson.fromJson(response.toString(), PojoOneWayReprice.class);
                    if (pojoOneWayReprice.getStatus().equalsIgnoreCase("SUCCESS")) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.toString());
                            JSONObject data = jsonObject.getJSONObject("data");
                            if (data.has("signMap")) {
                                String signMap = data.getJSONObject("signMap").toString();
                                SharedPrefrenceAir sharedPrefrenceAir = new SharedPrefrenceAir(getContext());
                                sharedPrefrenceAir.setSignMap(signMap);
                                Log.e("signMap ", sharedPrefrenceAir.getSignMap());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        data = pojoOneWayReprice.getData();
                        if (data.getIsPriceChange().equals("1")) {
                            showPriceChangeDialog();
                        } else {
                            ProjectUtil.replaceFragment(activity, new DReprice(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                        }
                    } else {
                        Toast.makeText(context, pojoOneWayReprice.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse != null) {
                        if (error.networkResponse.statusCode == 500) {
                            NetworkingUtils.dismissProgress();
                            Toast.makeText(context, "Internal Server Error.", Toast.LENGTH_SHORT).show();
                        } else {
                            NetworkingUtils.dismissProgress();
                            NetworkingUtils.noInternetAccess(context);
                        }
                    } else {
                        NetworkingUtils.dismissProgress();
                        NetworkingUtils.noInternetAccess(context);
                    }
                }
            });
        } else {
            if (selectedDepertureFlight == null) {
                selectedDepertureFlight = onwardFlightsList.get(0);
            }
            if (selectedReturnFlight == null) {
                selectedReturnFlight = returnFlightsList.get(0);
            }
            String[] flightKeys = new String[]{selectedDepertureFlight.getKey(), selectedReturnFlight.getKey()};
            Networking.repriceOneWay(String.valueOf(Float.parseFloat(selectedDepertureFlight.getLstFareDetails()[0].getTotal()) + Float.parseFloat(selectedReturnFlight.getLstFareDetails()[0].getTotal())), FragmentPlanner.searchResultData.getSearchKey(), flightKeys, FragmentPlanner.searchResultData.getIsInternational(), modelGstDetails, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    NetworkingUtils.dismissProgress();
                    Gson gson = new Gson();
                    PojoOneWayReprice pojoOneWayReprice = gson.fromJson(response.toString(), PojoOneWayReprice.class);
                    if (pojoOneWayReprice.getStatus().equalsIgnoreCase("SUCCESS")) {
                        try {
                            JSONObject jsonObject = new JSONObject(response.toString());
                            JSONObject data = jsonObject.getJSONObject("data");
                            if (data.has("signMap")) {
                                String signMap = data.getJSONObject("signMap").toString();
                                SharedPrefrenceAir sharedPrefrenceAir = new SharedPrefrenceAir(getContext());
                                sharedPrefrenceAir.setSignMap(signMap);
                                Log.e("signMap ", sharedPrefrenceAir.getSignMap());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        data = pojoOneWayReprice.getData();
                        if (data.getIsPriceChange().equals("1")) {
                            showPriceChangeDialog();
                        } else {
                            ProjectUtil.replaceFragment(activity, new DReprice(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                        }
                    } else {
                        Toast.makeText(context, pojoOneWayReprice.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error.networkResponse != null) {
                        if (error.networkResponse.statusCode == 500) {
                            NetworkingUtils.dismissProgress();
                            Toast.makeText(context, "Internal Server Error.", Toast.LENGTH_SHORT).show();
                        } else {
                            NetworkingUtils.dismissProgress();
                            NetworkingUtils.noInternetAccess(context);
                        }
                    } else {
                        NetworkingUtils.dismissProgress();
                        NetworkingUtils.noInternetAccess(context);
                    }
                }
            });
        }
    }

    private void showPriceChangeDialog() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        dialog.dismiss();
                        ProjectUtil.replaceFragment(activity, new DReprice(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Price changed, Are you sure to continue?").setPositiveButton("Yes", dialogClickListener).setNegativeButton("No", dialogClickListener).show();
    }

    //region : Selected All Flights
    private void showAllFlight() {
        makeList();
        setAdapterOnBothList();
    }
    //endregion

    //region : Selected Special Flight
    BroadcastReceiver broadcastReceiverSpecialFare = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {
            specialFare = true;
            nonStopState = false;
            oneStopState = false;
            twoOrMoreStopState = false;
            timeState1 = false;
            timeState2 = false;
            timeState3 = false;
            timeState4 = false;
            listCombo = null;
            selectedDepertureFlight = null;
            selectedReturnFlight = null;
            filteredAirlines = new ArrayList<>();
            HashMap<String, ArrayList<Flights>> hashMapCombo = new HashMap<>();
            if (onwardFlightsList.size() > 0) {
                if (onwardFlightsList.get(0).getServiceProvider() == null) {
                    for (int i = 0; i < onwardFlightsList.size(); i++) {
                        ArrayList<Flights> arrayList = new ArrayList<>();
                        arrayList.add(onwardFlightsList.get(i));
                        hashMapCombo.put(onwardFlightsList.get(i).getGdsKey(), arrayList);
                    }
                    for (int i = 0; i < returnFlightsList.size(); i++) {
                        if (hashMapCombo.containsKey(returnFlightsList.get(i).getGdsKey())) {
                            ArrayList<Flights> arrayList = hashMapCombo.get(returnFlightsList.get(i).getGdsKey());
                            arrayList.add(returnFlightsList.get(i));
                            hashMapCombo.remove(returnFlightsList.get(i).getGdsKey());
                            hashMapCombo.put(returnFlightsList.get(i).getGdsKey(), arrayList);
                        }
                    }
                    listCombo = new ArrayList<>();
                    ArrayList<ArrayList<Flights>> templistCombo = new ArrayList<ArrayList<Flights>>(hashMapCombo.values());
                    for(ArrayList<Flights> tempFlights : templistCombo){
                        if(tempFlights.size()>1){
                            listCombo.add(tempFlights);
                        }
                    }
                    onwardFlightsList = new ArrayList<>();
                    returnFlightsList = new ArrayList<>();
                    sortingNav.setVisibility(View.GONE);
                    sortingGds.setVisibility(View.VISIBLE);
                    line_ver.setVisibility(View.GONE);
                    listViewOnwardFlight.setVisibility(View.GONE);
                    listViewReturnFlight.setVisibility(View.GONE);
                    listViewSpecialFareGDS.setVisibility(View.VISIBLE);
                    listViewSpecialFareGDS.setAdapter(new AdapterSpecialFareGds(getActivity(), listCombo));
                    setOnItemClickOnListCombo(listCombo);
                    selectedGdsCombo = null;
                    listViewSpecialFareGDS.setItemChecked(0, true);
                    totalPrice.setText(getString(R.string.symbol_rs) + " " + Integer.parseInt(listCombo.get(0).get(0).getPrice()));
                } else {
                    setAdapterOnBothList();
                }
            }
        }
    };
//endregion

    private void initializeFilter() {
        filterDialog = new Dialog(activity, android.R.style.Theme_DeviceDefault_Light_NoActionBar);
        activity.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        filterDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        filterDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.LTGRAY));
        filterDialog.setContentView(R.layout.fragment_filter);
        applyFilter = (Button) filterDialog.findViewById(R.id.filterBtnApply);
        resetFilter = (LinearLayout) filterDialog.findViewById(R.id.FILTER_RESET_LAY);
        nonStop = (CheckBox) filterDialog.findViewById(R.id.filterCkBoxZeroStops);
        oneStop = (CheckBox) filterDialog.findViewById(R.id.filterCkBoxOneStops);
        twoOrMoreStop = (CheckBox) filterDialog.findViewById(R.id.filterCkBoxMoreStops);
        time1 = (CheckBox) filterDialog.findViewById(R.id.chBoxfilterOnward_1);
        time2 = (CheckBox) filterDialog.findViewById(R.id.chBoxfilterOnward_2);
        time3 = (CheckBox) filterDialog.findViewById(R.id.chBoxfilterOnward_3);
        time4 = (CheckBox) filterDialog.findViewById(R.id.chBoxfilterOnward_4);
        filterPreferFlightsListView = (ListView) filterDialog.findViewById(R.id.filterPreferFlightsListView);

        roundRangeBar = (RangeBar) filterDialog.findViewById(R.id.rangebarPrice);
        minFlightFare = (TextView) filterDialog.findViewById(R.id.txtFlightFareMin);
        maxFlightFare = (TextView) filterDialog.findViewById(R.id.txtFlightFareMax);
        maxSelectedFlightFare = (TextView) filterDialog.findViewById(R.id.txtSelectedFlightFareMax);
        minSelectedFlightFare = (TextView) filterDialog.findViewById(R.id.txtSelectedFlightFareMin);
        roundRangeBar.setBarColor(Color.parseColor("#9DA2A3"));
        roundRangeBar.setPinColor(Color.parseColor("#21A1CE")); // change the background color of pin like drop
        roundRangeBar.setPinRadius(30);// change the radious of pin like drop
        roundRangeBar.setTickColor(Color.TRANSPARENT); // background color line
        roundRangeBar.setConnectingLineColor(Color.parseColor("#21A1CE"));
        roundRangeBar.setSelectorColor(Color.parseColor("#4F565E"));//
        roundRangeBar.setConnectingLineWeight(1);

        if (nonStopState) {
            nonStop.setChecked(true);
        } else {
            nonStop.setChecked(false);
        }
        if (oneStopState) {
            oneStop.setChecked(true);
        } else {
            oneStop.setChecked(false);
        }
        if (twoOrMoreStopState) {
            twoOrMoreStop.setChecked(true);
        } else {
            twoOrMoreStop.setChecked(false);
        }
        if (timeState1) {
            time1.setChecked(true);
        } else {
            time1.setChecked(false);
        }
        if (timeState2) {
            time2.setChecked(true);
        } else {
            time2.setChecked(false);
        }
        if (timeState3) {
            time3.setChecked(true);
        } else {
            time3.setChecked(false);
        }
        if (timeState4) {
            time4.setChecked(true);
        } else {
            time4.setChecked(false);
        }

        applyFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                applyFilter();
            }
        });
        resetFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //filterDialog.dismiss();
                resetFilter();
                //filterDialog.show();
            }
        });
        makeAirlinesList();
        filterDialog.show();
    }

    private void makeAirlinesList() {
        LinkedHashMap<String, String> airlinesHashMap = new LinkedHashMap<>();
        LinkedHashMap<String, String> airlinesIconHashMap = new LinkedHashMap<>();
        if (listCombo != null) {
            for (ArrayList<Flights> flightsArrayList : listCombo) {
                if (airlinesHashMap.containsKey(flightsArrayList.get(0).getCarrierName())) {
                    if (Integer.parseInt(airlinesHashMap.get(flightsArrayList.get(0).getCarrierName())) >= Integer.parseInt(flightsArrayList.get(0).getPrice())) {
                        airlinesHashMap.put(flightsArrayList.get(0).getCarrierName(), flightsArrayList.get(0).getPrice());
                        airlinesIconHashMap.put(flightsArrayList.get(0).getCarrierName(), flightsArrayList.get(0).getCarrier());
                    }
                } else {
                    airlinesHashMap.put(flightsArrayList.get(0).getCarrierName(), flightsArrayList.get(0).getPrice());
                    airlinesIconHashMap.put(flightsArrayList.get(0).getCarrierName(), flightsArrayList.get(0).getCarrier());
                }
            }
            for (ArrayList<Flights> flightsArrayList : listCombo) {
                if (airlinesHashMap.containsKey(flightsArrayList.get(1).getCarrierName())) {
                    if (Integer.parseInt(airlinesHashMap.get(flightsArrayList.get(1).getCarrierName())) >= Integer.parseInt(flightsArrayList.get(1).getPrice())) {
                        airlinesHashMap.put(flightsArrayList.get(1).getCarrierName(), flightsArrayList.get(1).getPrice());
                        airlinesIconHashMap.put(flightsArrayList.get(1).getCarrierName(), flightsArrayList.get(1).getCarrier());
                    }
                } else {
                    airlinesHashMap.put(flightsArrayList.get(1).getCarrierName(), flightsArrayList.get(1).getPrice());
                    airlinesIconHashMap.put(flightsArrayList.get(1).getCarrierName(), flightsArrayList.get(1).getCarrier());
                }
            }
        } else {
            ArrayList<Flights> tempOnwardFlightsList = new ArrayList<>();
            tempOnwardFlightsList.addAll(onwardFlightsList);
            ArrayList<Flights> tempReturnFlightsList = new ArrayList<>();
            tempReturnFlightsList.addAll(returnFlightsList);

            Collections.sort(tempOnwardFlightsList, new DSortByFlightName());
            Collections.sort(tempReturnFlightsList, new DSortByFlightName());

            for (Flights flight : tempOnwardFlightsList) {
                if (airlinesHashMap.containsKey(flight.getCarrierName())) {
                    if (Integer.parseInt(airlinesHashMap.get(flight.getCarrierName())) >= Integer.parseInt(flight.getPrice())) {
                        airlinesHashMap.put(flight.getCarrierName(), flight.getPrice());
                        airlinesIconHashMap.put(flight.getCarrierName(), flight.getCarrier());
                    }
                } else {
                    airlinesHashMap.put(flight.getCarrierName(), flight.getPrice());
                    airlinesIconHashMap.put(flight.getCarrierName(), flight.getCarrier());
                }
            }
            for (Flights flight : tempReturnFlightsList) {
                if (airlinesHashMap.containsKey(flight.getCarrierName())) {
                    if (Integer.parseInt(airlinesHashMap.get(flight.getCarrierName())) >= Integer.parseInt(flight.getPrice())) {
                        airlinesHashMap.put(flight.getCarrierName(), flight.getPrice());
                        airlinesIconHashMap.put(flight.getCarrierName(), flight.getCarrier());
                    }
                } else {
                    airlinesHashMap.put(flight.getCarrierName(), flight.getPrice());
                    airlinesIconHashMap.put(flight.getCarrierName(), flight.getCarrier());
                }
            }
        }
        ArrayList<String> airlines = new ArrayList<>(airlinesHashMap.keySet());
        ArrayList<String> airFare = new ArrayList<>(airlinesHashMap.values());
        ArrayList<String> airlineIcons = new ArrayList<>(airlinesIconHashMap.values());
        filterPreferFlightsListView.setAdapter(new DFilterAirlineAdapter(context, airlines, airFare, airlineIcons));
        setListViewHeightBasedOnChildren(filterPreferFlightsListView);

    }

    private void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    private float getLowestPrice() {
        Flights onwardMin = null, returnMin = null;
        if (onwardFlightsList != null) {
            if (onwardFlightsList.size() > 0) {
                onwardMin = Collections.min(onwardFlightsList, new DSortByFarePrice());
            }
        }
        if (returnFlightsList != null) {
            if (returnFlightsList.size() > 0) {
                returnMin = Collections.min(returnFlightsList, new DSortByFarePrice());
            }
        }

        if (onwardMin == null || returnMin == null) {
            return 0.0f;
        } else {
            return Float.parseFloat((Float.parseFloat(onwardMin.getPrice()) < Float.parseFloat(returnMin.getPrice())) ? onwardMin.getPrice() : returnMin.getPrice());
        }
    }

    private float getHighestPrice() {
        Flights onwardMax = null, returnMax = null;
        if (onwardFlightsList != null) {
            if (onwardFlightsList.size() > 0) {
                onwardMax = Collections.max(onwardFlightsList, new DSortByFarePrice());
            }
        }
        if (returnFlightsList != null) {
            if (returnFlightsList.size() > 0) {
                returnMax = Collections.max(returnFlightsList, new DSortByFarePrice());
            }
        }

        if (onwardMax == null || returnMax == null) {
            return 0.0f;
        } else {
            return Float.parseFloat(((Float.parseFloat(onwardMax.getPrice()) > Float.parseFloat(returnMax.getPrice())) ? onwardMax.getPrice() : returnMax.getPrice()));

        }
    }

    private void showFilterDialog() {
        initializeFilter();
        setPriceOnRangeBar();
    }

    private void showFilterDialogGDS() {
        initializeFilter();
        setPriceOnRangeBarGDS();
    }

    private void setPriceOnRangeBar() {
        //assignMinMaxPriceGDS();
        roundRangeBar.setPinTextFormatter(new RangeBar.PinTextFormatter() {
            @Override
            public String getText(String value) {
                return value;
            }
        });
        float highestPrice = getHighestPrice();
        float lowestPrice = getLowestPrice();
        selectedLowestPrice = lowestPrice;
        selectedHighestPrice = highestPrice;
        if (specialFare) {
            if (lowestPrice > 2 && highestPrice > 3 && (highestPrice - lowestPrice) > 2) {
                roundRangeBar.setTickEnd(highestPrice);
                roundRangeBar.setTickStart(lowestPrice);
                roundRangeBar.setTickInterval(1);

                minSelectedFlightFare.setText("\u20B9  " + lowestPrice);
                maxSelectedFlightFare.setText("\u20B9  " + highestPrice);
                roundRangeBar.setRangePinsByValue(lowestPrice, highestPrice);
            } else {
                roundRangeBar.setTickStart(0);
                roundRangeBar.setTickEnd(999);
                roundRangeBar.setEnabled(false);
                minSelectedFlightFare.setText("\u20B9  " + lowestPrice);
                maxSelectedFlightFare.setText("\u20B9  " + highestPrice);
            }
            // Log.e("Lowest Price : ",selectedLowestPrice+"");
            // Log.e("Highest Price : ",selectedHighestPrice+"");

        } else {
            if (lowestPrice > 2 && highestPrice > 3 && (highestPrice - lowestPrice) > 2) {
                Log.e("Lowest Price", lowestPrice + "");
                Log.e("Highest Price", highestPrice + "");
                roundRangeBar.setTickEnd(highestPrice);
                roundRangeBar.setTickStart(lowestPrice);
                roundRangeBar.setTickInterval(1);
                if (selectedLowestPrice == 0.0f) selectedLowestPrice = lowestPrice;
                if (selectedHighestPrice == 0.0f) selectedHighestPrice = highestPrice;
                if (selectedHighestPrice > highestPrice) selectedHighestPrice = highestPrice;
                minSelectedFlightFare.setText("\u20B9  " + selectedLowestPrice);
                maxSelectedFlightFare.setText("\u20B9  " + selectedHighestPrice);
                // Log.e("Lowest Price : ",selectedLowestPrice+"");
                // Log.e("Highest Price : ",selectedHighestPrice+"");
                roundRangeBar.setRangePinsByValue(selectedLowestPrice, selectedHighestPrice);
                //roundRangeBar.setVisibility(View.VISIBLE);
                //minSelectedFlightFare.setVisibility(View.VISIBLE);
                // maxSelectedFlightFare.setVisibility(View.VISIBLE);
            } else {
                minSelectedFlightFare.setText("\u20B9  " + selectedLowestPrice);
                maxSelectedFlightFare.setText("\u20B9  " + selectedHighestPrice);
                roundRangeBar.setEnabled(false);
            }
        }
        roundRangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
                //                filterBean.setIsChange(1);
//                filterBean.setMaxPrice(Integer.parseInt(rangeBar.getRightPinValue()));
//                filterBean.setMinPrice(Integer.parseInt(rangeBar.getLeftPinValue()));
                minSelectedFlightFare.setText("\u20B9  " + Integer.parseInt(rangeBar.getLeftPinValue()));
                maxSelectedFlightFare.setText("\u20B9  " + Integer.parseInt(rangeBar.getRightPinValue()));

                selectedHighestPrice = Float.parseFloat(rangeBar.getRightPinValue());
                selectedLowestPrice = Float.parseFloat(rangeBar.getLeftPinValue());
            }
        });

    }

    private void setPriceOnRangeBarGDS() {
        /*roundRangeBar = (RangeBar)dialog.findViewById(R.id.rangebarPrice);
        minFlightFare = (TextView)dialog.findViewById(R.id.txtFlightFareMin);
        maxFlightFare = (TextView)dialog.findViewById(R.id.txtFlightFareMax);
        maxSelectedFlightFare = (TextView)dialog.findViewById(R.id.txtSelectedFlightFareMax);
        minSelectedFlightFare = (TextView)dialog.findViewById(R.id.txtSelectedFlightFareMin);
        roundRangeBar.setBarColor(Color.parseColor("#9DA2A3"));
        roundRangeBar.setPinColor(Color.parseColor("#21A1CE")); // change the background color of pin like drop
        roundRangeBar.setPinRadius(30);// change the radious of pin like drop
        roundRangeBar.setTickColor(Color.TRANSPARENT); // background color line
        roundRangeBar.setConnectingLineColor(Color.parseColor("#21A1CE"));
        roundRangeBar.setSelectorColor(Color.parseColor("#4F565E"));//
        roundRangeBar.setConnectingLineWeight(1);*/
        //assignMinMaxPriceGDS();
        roundRangeBar.setPinTextFormatter(new RangeBar.PinTextFormatter() {
            @Override
            public String getText(String value) {
                return value;
            }
        });
        float highestPrice = getHighestPriceGDS();
        float lowestPrice = getLowestPriceGDS();
        selectedHighestPrice = highestPrice;
        selectedLowestPrice = lowestPrice;
        if (!((highestPrice - lowestPrice) <= 1)) {
            roundRangeBar.setTickEnd(highestPrice);
            roundRangeBar.setTickStart(lowestPrice);
            roundRangeBar.setTickInterval(1);
            if (selectedLowestPrice == 0.0f) selectedLowestPrice = lowestPrice;
            if (selectedHighestPrice == 0.0f) selectedHighestPrice = highestPrice;
            if (selectedHighestPrice > highestPrice) selectedHighestPrice = highestPrice;
            minSelectedFlightFare.setText("\u20B9  " + selectedLowestPrice);
            maxSelectedFlightFare.setText("\u20B9  " + selectedHighestPrice);
            try {
                roundRangeBar.setRangePinsByValue(selectedLowestPrice, selectedHighestPrice);
            } catch (Exception e) {

            }
            roundRangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
                @Override
                public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
//                filterBean.setIsChange(1);
//                filterBean.setMaxPrice(Integer.parseInt(rangeBar.getRightPinValue()));
//                filterBean.setMinPrice(Integer.parseInt(rangeBar.getLeftPinValue()));
                    minSelectedFlightFare.setText("\u20B9  " + Integer.parseInt(rangeBar.getLeftPinValue()));
                    maxSelectedFlightFare.setText("\u20B9  " + Integer.parseInt(rangeBar.getRightPinValue()));
                    selectedHighestPrice = Float.parseFloat(rangeBar.getRightPinValue());
                    selectedLowestPrice = Float.parseFloat(rangeBar.getLeftPinValue());
                }
            });
        } else {
            roundRangeBar.setEnabled(false);
        }
        if (selectedLowestPrice == 0.0f) selectedLowestPrice = lowestPrice;
        if (selectedHighestPrice == 0.0f) selectedHighestPrice = highestPrice;
        minSelectedFlightFare.setText("\u20B9  " + selectedLowestPrice);
        maxSelectedFlightFare.setText("\u20B9  " + selectedHighestPrice);
    }

    private float getLowestPriceGDS() {
        return Float.parseFloat(Collections.min(listCombo, new DSortByFarePriceGDS()).get(0).getPrice());
    }

    private float getHighestPriceGDS() {
        return Float.parseFloat(Collections.max(listCombo, new DSortByFarePriceGDS()).get(0).getPrice());
    }

    private void applyFilter() {
        if (listCombo != null) {
            ArrayList<ArrayList<Flights>> listComboClone = new ArrayList<>();
            for (int i = 0; i < listCombo.size(); i++) {
                if (filteredAirlines.size() > 0) {
                    for (String carrearName : filteredAirlines) {
                        if (carrearName.equals(listCombo.get(i).get(0).getCarrierName()) && carrearName.equals(listCombo.get(i).get(1).getCarrierName()))
                            if (checkForTime()) {
                                if (Float.parseFloat(listCombo.get(i).get(0).getPrice()) >= selectedLowestPrice && Float.parseFloat(listCombo.get(i).get(0).getPrice()) <= selectedHighestPrice) {
                                    if (checkForStops()) {
                                        if (checkForNonStop(listCombo.get(i).get(0).getStops()) && checkForNonStop(listCombo.get(i).get(1).getStops())) {
                                            if (checkForTime1(listCombo.get(i).get(0).getDepartureTime()) && checkForTime1(listCombo.get(i).get(1).getDepartureTime())) {
                                                listComboClone.add(listCombo.get(i));
                                            }
                                            if (checkForTime2(listCombo.get(i).get(0).getDepartureTime()) && checkForTime2(listCombo.get(i).get(1).getDepartureTime())) {
                                                listComboClone.add(listCombo.get(i));
                                            }
                                            if (checkForTime3(listCombo.get(i).get(0).getDepartureTime()) && checkForTime3(listCombo.get(i).get(1).getDepartureTime())) {
                                                listComboClone.add(listCombo.get(i));
                                            }
                                            if (checkForTime4(listCombo.get(i).get(0).getDepartureTime()) && checkForTime4(listCombo.get(i).get(0).getDepartureTime())) {
                                                listComboClone.add(listCombo.get(i));
                                            }
                                        }
                                        if (checkForOneStop(listCombo.get(i).get(0).getStops()) && checkForOneStop(listCombo.get(i).get(1).getStops())) {
                                            if (checkForTime1(listCombo.get(i).get(0).getDepartureTime()) && checkForTime1(listCombo.get(i).get(1).getDepartureTime())) {
                                                listComboClone.add(listCombo.get(i));
                                            }
                                            if (checkForTime2(listCombo.get(i).get(0).getDepartureTime()) && checkForTime2(listCombo.get(i).get(1).getDepartureTime())) {
                                                listComboClone.add(listCombo.get(i));
                                            }
                                            if (checkForTime3(listCombo.get(i).get(0).getDepartureTime()) && checkForTime3(listCombo.get(i).get(1).getDepartureTime())) {
                                                listComboClone.add(listCombo.get(i));
                                            }
                                            if (checkForTime4(listCombo.get(i).get(0).getDepartureTime()) && checkForTime4(listCombo.get(i).get(1).getDepartureTime())) {
                                                listComboClone.add(listCombo.get(i));
                                            }
                                        }
                                        if (checkForTwoOrMoreStop(listCombo.get(i).get(0).getStops()) && checkForTwoOrMoreStop(listCombo.get(i).get(1).getStops())) {
                                            if (checkForTime1(listCombo.get(i).get(0).getDepartureTime()) && checkForTime1(listCombo.get(i).get(1).getDepartureTime())) {
                                                listComboClone.add(listCombo.get(i));
                                            }
                                            if (checkForTime2(listCombo.get(i).get(0).getDepartureTime()) && checkForTime2(listCombo.get(i).get(1).getDepartureTime())) {
                                                listComboClone.add(listCombo.get(i));
                                            }
                                            if (checkForTime3(listCombo.get(i).get(0).getDepartureTime()) && checkForTime3(listCombo.get(i).get(1).getDepartureTime())) {
                                                listComboClone.add(listCombo.get(i));
                                            }
                                            if (checkForTime4(listCombo.get(i).get(0).getDepartureTime()) && checkForTime4(listCombo.get(i).get(1).getDepartureTime())) {
                                                listComboClone.add(listCombo.get(i));
                                            }
                                        }
                                    } else {
                                        if (checkForTime1(listCombo.get(i).get(0).getDepartureTime()) && checkForTime1(listCombo.get(i).get(1).getDepartureTime())) {
                                            listComboClone.add(listCombo.get(i));
                                        }
                                        if (checkForTime2(listCombo.get(i).get(0).getDepartureTime()) && checkForTime2(listCombo.get(i).get(1).getDepartureTime())) {
                                            listComboClone.add(listCombo.get(i));
                                        }
                                        if (checkForTime3(listCombo.get(i).get(0).getDepartureTime()) && checkForTime3(listCombo.get(i).get(1).getDepartureTime())) {
                                            listComboClone.add(listCombo.get(i));
                                        }
                                        if (checkForTime4(listCombo.get(i).get(0).getDepartureTime()) && checkForTime4(listCombo.get(i).get(1).getDepartureTime())) {
                                            listComboClone.add(listCombo.get(i));
                                        }
                                    }
                                }
                            } else {
                                if (Float.parseFloat(listCombo.get(i).get(0).getPrice()) >= selectedLowestPrice && Float.parseFloat(listCombo.get(i).get(0).getPrice()) <= selectedHighestPrice) {
                                    if (checkForStops()) {
                                        if (checkForNonStop(listCombo.get(i).get(0).getStops()) && checkForNonStop(listCombo.get(i).get(1).getStops())) {
                                            listComboClone.add(listCombo.get(i));
                                        }
                                        if (checkForOneStop(listCombo.get(i).get(0).getStops()) && checkForOneStop(listCombo.get(i).get(1).getStops())) {
                                            listComboClone.add(listCombo.get(i));
                                        }
                                        if (checkForTwoOrMoreStop(listCombo.get(i).get(0).getStops()) && checkForTwoOrMoreStop(listCombo.get(i).get(1).getStops())) {
                                            listComboClone.add(listCombo.get(i));
                                        }
                                    } else {
                                        listComboClone.add(listCombo.get(i));
                                    }
                                }
                            }
                    }
                } else {
                    if (checkForTime()) {
                        if (Float.parseFloat(listCombo.get(i).get(0).getPrice()) >= selectedLowestPrice && Float.parseFloat(listCombo.get(i).get(0).getPrice()) <= selectedHighestPrice) {
                            if (checkForStops()) {
                                if (checkForNonStop(listCombo.get(i).get(0).getStops()) && checkForNonStop(listCombo.get(i).get(1).getStops())) {
                                    if (checkForTime1(listCombo.get(i).get(0).getDepartureTime()) && checkForTime1(listCombo.get(i).get(1).getDepartureTime())) {
                                        listComboClone.add(listCombo.get(i));
                                    }
                                    if (checkForTime2(listCombo.get(i).get(0).getDepartureTime()) && checkForTime2(listCombo.get(i).get(1).getDepartureTime())) {
                                        listComboClone.add(listCombo.get(i));
                                    }
                                    if (checkForTime3(listCombo.get(i).get(0).getDepartureTime()) && checkForTime3(listCombo.get(i).get(1).getDepartureTime())) {
                                        listComboClone.add(listCombo.get(i));
                                    }
                                    if (checkForTime4(listCombo.get(i).get(0).getDepartureTime()) && checkForTime4(listCombo.get(i).get(0).getDepartureTime())) {
                                        listComboClone.add(listCombo.get(i));
                                    }
                                }
                                if (checkForOneStop(listCombo.get(i).get(0).getStops()) && checkForOneStop(listCombo.get(i).get(1).getStops())) {
                                    if (checkForTime1(listCombo.get(i).get(0).getDepartureTime()) && checkForTime1(listCombo.get(i).get(1).getDepartureTime())) {
                                        listComboClone.add(listCombo.get(i));
                                    }
                                    if (checkForTime2(listCombo.get(i).get(0).getDepartureTime()) && checkForTime2(listCombo.get(i).get(1).getDepartureTime())) {
                                        listComboClone.add(listCombo.get(i));
                                    }
                                    if (checkForTime3(listCombo.get(i).get(0).getDepartureTime()) && checkForTime3(listCombo.get(i).get(1).getDepartureTime())) {
                                        listComboClone.add(listCombo.get(i));
                                    }
                                    if (checkForTime4(listCombo.get(i).get(0).getDepartureTime()) && checkForTime4(listCombo.get(i).get(1).getDepartureTime())) {
                                        listComboClone.add(listCombo.get(i));
                                    }
                                }
                                if (checkForTwoOrMoreStop(listCombo.get(i).get(0).getStops()) && checkForTwoOrMoreStop(listCombo.get(i).get(1).getStops())) {
                                    if (checkForTime1(listCombo.get(i).get(0).getDepartureTime()) && checkForTime1(listCombo.get(i).get(1).getDepartureTime())) {
                                        listComboClone.add(listCombo.get(i));
                                    }
                                    if (checkForTime2(listCombo.get(i).get(0).getDepartureTime()) && checkForTime2(listCombo.get(i).get(1).getDepartureTime())) {
                                        listComboClone.add(listCombo.get(i));
                                    }
                                    if (checkForTime3(listCombo.get(i).get(0).getDepartureTime()) && checkForTime3(listCombo.get(i).get(1).getDepartureTime())) {
                                        listComboClone.add(listCombo.get(i));
                                    }
                                    if (checkForTime4(listCombo.get(i).get(0).getDepartureTime()) && checkForTime4(listCombo.get(i).get(1).getDepartureTime())) {
                                        listComboClone.add(listCombo.get(i));
                                    }
                                }
                            } else {
                                if (checkForTime1(listCombo.get(i).get(0).getDepartureTime()) && checkForTime1(listCombo.get(i).get(1).getDepartureTime())) {
                                    listComboClone.add(listCombo.get(i));
                                }
                                if (checkForTime2(listCombo.get(i).get(0).getDepartureTime()) && checkForTime2(listCombo.get(i).get(1).getDepartureTime())) {
                                    listComboClone.add(listCombo.get(i));
                                }
                                if (checkForTime3(listCombo.get(i).get(0).getDepartureTime()) && checkForTime3(listCombo.get(i).get(1).getDepartureTime())) {
                                    listComboClone.add(listCombo.get(i));
                                }
                                if (checkForTime4(listCombo.get(i).get(0).getDepartureTime()) && checkForTime4(listCombo.get(i).get(1).getDepartureTime())) {
                                    listComboClone.add(listCombo.get(i));
                                }
                            }
                        }
                    } else {
                        if (Float.parseFloat(listCombo.get(i).get(0).getPrice()) >= selectedLowestPrice && Float.parseFloat(listCombo.get(i).get(0).getPrice()) <= selectedHighestPrice) {
                            if (checkForStops()) {
                                if (checkForNonStop(listCombo.get(i).get(0).getStops()) && checkForNonStop(listCombo.get(i).get(1).getStops())) {
                                    listComboClone.add(listCombo.get(i));
                                }
                                if (checkForOneStop(listCombo.get(i).get(0).getStops()) && checkForOneStop(listCombo.get(i).get(1).getStops())) {
                                    listComboClone.add(listCombo.get(i));
                                }
                                if (checkForTwoOrMoreStop(listCombo.get(i).get(0).getStops()) && checkForTwoOrMoreStop(listCombo.get(i).get(1).getStops())) {
                                    listComboClone.add(listCombo.get(i));
                                }
                            } else {
                                listComboClone.add(listCombo.get(i));
                            }
                        }
                    }
                }
            }
            if (listComboClone.size() > 0) {
                listViewSpecialFareGDS.setAdapter(new AdapterSpecialFareGds(getActivity(), listComboClone));
                setOnItemClickOnListCombo(listComboClone);
                saveFilterState();
                filterDialog.dismiss();
            } else {
                Toast.makeText(context, "No flights available in this selection.", Toast.LENGTH_SHORT).show();
            }
        } else {
            ArrayList<Flights> onwardFlightsListClone = new ArrayList<>();
            final ArrayList<Flights> returnFlightsListClone = new ArrayList<>();
            for (int i = 0; i < onwardFlightsList.size(); i++) {
                if (filteredAirlines.size() > 0) {
                    for (String carrearName : filteredAirlines) {
                        if (carrearName.equals(onwardFlightsList.get(i).getCarrierName()) && carrearName.equals(onwardFlightsList.get(i).getCarrierName())) {
                            if (checkForTime()) {
                                if (Float.parseFloat(onwardFlightsList.get(i).getPrice()) >= selectedLowestPrice && Float.parseFloat(onwardFlightsList.get(i).getPrice()) <= selectedHighestPrice) {
                                    if (checkForStops()) {
                                        if (checkForNonStop(onwardFlightsList.get(i).getStops())) {
                                            if (checkForTime1(onwardFlightsList.get(i).getDepartureTime())) {
                                                onwardFlightsListClone.add(onwardFlightsList.get(i));
                                            }
                                            if (checkForTime2(onwardFlightsList.get(i).getDepartureTime())) {
                                                onwardFlightsListClone.add(onwardFlightsList.get(i));
                                            }
                                            if (checkForTime3(onwardFlightsList.get(i).getDepartureTime())) {
                                                onwardFlightsListClone.add(onwardFlightsList.get(i));
                                            }
                                            if (checkForTime4(onwardFlightsList.get(i).getDepartureTime())) {
                                                onwardFlightsListClone.add(onwardFlightsList.get(i));
                                            }
                                        }
                                        if (checkForOneStop(onwardFlightsList.get(i).getStops())) {
                                            if (checkForTime1(onwardFlightsList.get(i).getDepartureTime())) {
                                                onwardFlightsListClone.add(onwardFlightsList.get(i));
                                            }
                                            if (checkForTime2(onwardFlightsList.get(i).getDepartureTime())) {
                                                onwardFlightsListClone.add(onwardFlightsList.get(i));
                                            }
                                            if (checkForTime3(onwardFlightsList.get(i).getDepartureTime())) {
                                                onwardFlightsListClone.add(onwardFlightsList.get(i));
                                            }
                                            if (checkForTime4(onwardFlightsList.get(i).getDepartureTime())) {
                                                onwardFlightsListClone.add(onwardFlightsList.get(i));
                                            }
                                        }
                                        if (checkForTwoOrMoreStop(onwardFlightsList.get(i).getStops())) {
                                            if (checkForTime1(onwardFlightsList.get(i).getDepartureTime())) {
                                                onwardFlightsListClone.add(onwardFlightsList.get(i));
                                            }
                                            if (checkForTime2(onwardFlightsList.get(i).getDepartureTime())) {
                                                onwardFlightsListClone.add(onwardFlightsList.get(i));
                                            }
                                            if (checkForTime3(onwardFlightsList.get(i).getDepartureTime())) {
                                                onwardFlightsListClone.add(onwardFlightsList.get(i));
                                            }
                                            if (checkForTime4(onwardFlightsList.get(i).getDepartureTime())) {
                                                onwardFlightsListClone.add(onwardFlightsList.get(i));
                                            }
                                        }
                                    } else {
                                        if (checkForTime1(onwardFlightsList.get(i).getDepartureTime())) {
                                            onwardFlightsListClone.add(onwardFlightsList.get(i));
                                        }
                                        if (checkForTime2(onwardFlightsList.get(i).getDepartureTime())) {
                                            onwardFlightsListClone.add(onwardFlightsList.get(i));
                                        }
                                        if (checkForTime3(onwardFlightsList.get(i).getDepartureTime())) {
                                            onwardFlightsListClone.add(onwardFlightsList.get(i));
                                        }
                                        if (checkForTime4(onwardFlightsList.get(i).getDepartureTime())) {
                                            onwardFlightsListClone.add(onwardFlightsList.get(i));
                                        }
                                    }
                                }
                            } else {
                                if (Float.parseFloat(onwardFlightsList.get(i).getPrice()) >= selectedLowestPrice && Float.parseFloat(onwardFlightsList.get(i).getPrice()) <= selectedHighestPrice) {
                                    if (checkForStops()) {
                                        if (checkForNonStop(onwardFlightsList.get(i).getStops())) {
                                            onwardFlightsListClone.add(onwardFlightsList.get(i));
                                        }
                                        if (checkForOneStop(onwardFlightsList.get(i).getStops())) {
                                            onwardFlightsListClone.add(onwardFlightsList.get(i));
                                        }
                                        if (checkForTwoOrMoreStop(onwardFlightsList.get(i).getStops())) {
                                            onwardFlightsListClone.add(onwardFlightsList.get(i));
                                        }
                                    } else {
                                        onwardFlightsListClone.add(onwardFlightsList.get(i));
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (checkForTime()) {
                        if (Float.parseFloat(onwardFlightsList.get(i).getPrice()) >= selectedLowestPrice && Float.parseFloat(onwardFlightsList.get(i).getPrice()) <= selectedHighestPrice) {
                            if (checkForStops()) {
                                if (checkForNonStop(onwardFlightsList.get(i).getStops())) {
                                    if (checkForTime1(onwardFlightsList.get(i).getDepartureTime())) {
                                        onwardFlightsListClone.add(onwardFlightsList.get(i));
                                    }
                                    if (checkForTime2(onwardFlightsList.get(i).getDepartureTime())) {
                                        onwardFlightsListClone.add(onwardFlightsList.get(i));
                                    }
                                    if (checkForTime3(onwardFlightsList.get(i).getDepartureTime())) {
                                        onwardFlightsListClone.add(onwardFlightsList.get(i));
                                    }
                                    if (checkForTime4(onwardFlightsList.get(i).getDepartureTime())) {
                                        onwardFlightsListClone.add(onwardFlightsList.get(i));
                                    }
                                }
                                if (checkForOneStop(onwardFlightsList.get(i).getStops())) {
                                    if (checkForTime1(onwardFlightsList.get(i).getDepartureTime())) {
                                        onwardFlightsListClone.add(onwardFlightsList.get(i));
                                    }
                                    if (checkForTime2(onwardFlightsList.get(i).getDepartureTime())) {
                                        onwardFlightsListClone.add(onwardFlightsList.get(i));
                                    }
                                    if (checkForTime3(onwardFlightsList.get(i).getDepartureTime())) {
                                        onwardFlightsListClone.add(onwardFlightsList.get(i));
                                    }
                                    if (checkForTime4(onwardFlightsList.get(i).getDepartureTime())) {
                                        onwardFlightsListClone.add(onwardFlightsList.get(i));
                                    }
                                }
                                if (checkForTwoOrMoreStop(onwardFlightsList.get(i).getStops())) {
                                    if (checkForTime1(onwardFlightsList.get(i).getDepartureTime())) {
                                        onwardFlightsListClone.add(onwardFlightsList.get(i));
                                    }
                                    if (checkForTime2(onwardFlightsList.get(i).getDepartureTime())) {
                                        onwardFlightsListClone.add(onwardFlightsList.get(i));
                                    }
                                    if (checkForTime3(onwardFlightsList.get(i).getDepartureTime())) {
                                        onwardFlightsListClone.add(onwardFlightsList.get(i));
                                    }
                                    if (checkForTime4(onwardFlightsList.get(i).getDepartureTime())) {
                                        onwardFlightsListClone.add(onwardFlightsList.get(i));
                                    }
                                }
                            } else {
                                if (checkForTime1(onwardFlightsList.get(i).getDepartureTime())) {
                                    onwardFlightsListClone.add(onwardFlightsList.get(i));
                                }
                                if (checkForTime2(onwardFlightsList.get(i).getDepartureTime())) {
                                    onwardFlightsListClone.add(onwardFlightsList.get(i));
                                }
                                if (checkForTime3(onwardFlightsList.get(i).getDepartureTime())) {
                                    onwardFlightsListClone.add(onwardFlightsList.get(i));
                                }
                                if (checkForTime4(onwardFlightsList.get(i).getDepartureTime())) {
                                    onwardFlightsListClone.add(onwardFlightsList.get(i));
                                }
                            }
                        }
                    } else {
                        if (Float.parseFloat(onwardFlightsList.get(i).getPrice()) >= selectedLowestPrice && Float.parseFloat(onwardFlightsList.get(i).getPrice()) <= selectedHighestPrice) {
                            if (checkForStops()) {
                                if (checkForNonStop(onwardFlightsList.get(i).getStops())) {
                                    onwardFlightsListClone.add(onwardFlightsList.get(i));
                                }
                                if (checkForOneStop(onwardFlightsList.get(i).getStops())) {
                                    onwardFlightsListClone.add(onwardFlightsList.get(i));
                                }
                                if (checkForTwoOrMoreStop(onwardFlightsList.get(i).getStops())) {
                                    onwardFlightsListClone.add(onwardFlightsList.get(i));
                                }
                            } else {
                                onwardFlightsListClone.add(onwardFlightsList.get(i));
                            }
                        }
                    }
                }
            }
            for (int j = 0; j < returnFlightsList.size(); j++) {
                if (filteredAirlines.size() > 0) {
                    for (String carrearName : filteredAirlines) {
                        if (carrearName.equals(returnFlightsList.get(j).getCarrierName()) && carrearName.equals(returnFlightsList.get(j).getCarrierName())) {
                            if (checkForTime()) {
                                if (Float.parseFloat(returnFlightsList.get(j).getPrice()) >= selectedLowestPrice && Float.parseFloat(returnFlightsList.get(j).getPrice()) <= selectedHighestPrice) {
                                    if (checkForStops()) {
                                        if (checkForNonStop(returnFlightsList.get(j).getStops())) {
                                            if (checkForTime1(returnFlightsList.get(j).getDepartureTime())) {
                                                returnFlightsListClone.add(returnFlightsList.get(j));
                                            }
                                            if (checkForTime2(returnFlightsList.get(j).getDepartureTime())) {
                                                returnFlightsListClone.add(returnFlightsList.get(j));
                                            }
                                            if (checkForTime3(returnFlightsList.get(j).getDepartureTime())) {
                                                returnFlightsListClone.add(returnFlightsList.get(j));
                                            }
                                            if (checkForTime4(returnFlightsList.get(j).getDepartureTime())) {
                                                returnFlightsListClone.add(returnFlightsList.get(j));
                                            }
                                        }
                                        if (checkForOneStop(returnFlightsList.get(j).getStops())) {
                                            if (checkForTime1(returnFlightsList.get(j).getDepartureTime())) {
                                                returnFlightsListClone.add(returnFlightsList.get(j));
                                            }
                                            if (checkForTime2(returnFlightsList.get(j).getDepartureTime())) {
                                                returnFlightsListClone.add(returnFlightsList.get(j));
                                            }
                                            if (checkForTime3(returnFlightsList.get(j).getDepartureTime())) {
                                                returnFlightsListClone.add(returnFlightsList.get(j));
                                            }
                                            if (checkForTime4(returnFlightsList.get(j).getDepartureTime())) {
                                                returnFlightsListClone.add(returnFlightsList.get(j));
                                            }
                                        }
                                        if (checkForTwoOrMoreStop(returnFlightsList.get(j).getStops())) {
                                            if (checkForTime1(returnFlightsList.get(j).getDepartureTime())) {
                                                returnFlightsListClone.add(returnFlightsList.get(j));
                                            }
                                            if (checkForTime2(returnFlightsList.get(j).getDepartureTime())) {
                                                returnFlightsListClone.add(returnFlightsList.get(j));
                                            }
                                            if (checkForTime3(returnFlightsList.get(j).getDepartureTime())) {
                                                returnFlightsListClone.add(returnFlightsList.get(j));
                                            }
                                            if (checkForTime4(returnFlightsList.get(j).getDepartureTime())) {
                                                returnFlightsListClone.add(returnFlightsList.get(j));
                                            }
                                        }
                                    } else {
                                        if (checkForTime1(returnFlightsList.get(j).getDepartureTime())) {
                                            returnFlightsListClone.add(returnFlightsList.get(j));
                                        }
                                        if (checkForTime2(returnFlightsList.get(j).getDepartureTime())) {
                                            returnFlightsListClone.add(returnFlightsList.get(j));
                                        }
                                        if (checkForTime3(returnFlightsList.get(j).getDepartureTime())) {
                                            returnFlightsListClone.add(returnFlightsList.get(j));
                                        }
                                        if (checkForTime4(returnFlightsList.get(j).getDepartureTime())) {
                                            returnFlightsListClone.add(returnFlightsList.get(j));
                                        }
                                    }
                                }
                            } else {
                                if (Float.parseFloat(returnFlightsList.get(j).getPrice()) >= selectedLowestPrice && Float.parseFloat(returnFlightsList.get(j).getPrice()) <= selectedHighestPrice) {
                                    if (checkForStops()) {
                                        if (checkForNonStop(returnFlightsList.get(j).getStops())) {
                                            returnFlightsListClone.add(returnFlightsList.get(j));
                                        }
                                        if (checkForOneStop(returnFlightsList.get(j).getStops())) {
                                            returnFlightsListClone.add(returnFlightsList.get(j));
                                        }
                                        if (checkForTwoOrMoreStop(returnFlightsList.get(j).getStops())) {
                                            returnFlightsListClone.add(returnFlightsList.get(j));
                                        }
                                    } else {
                                        returnFlightsListClone.add(returnFlightsList.get(j));
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if (checkForTime()) {
                        if (Float.parseFloat(returnFlightsList.get(j).getPrice()) >= selectedLowestPrice && Float.parseFloat(returnFlightsList.get(j).getPrice()) <= selectedHighestPrice) {
                            if (checkForStops()) {
                                if (checkForNonStop(returnFlightsList.get(j).getStops())) {
                                    if (checkForTime1(returnFlightsList.get(j).getDepartureTime())) {
                                        returnFlightsListClone.add(returnFlightsList.get(j));
                                    }
                                    if (checkForTime2(returnFlightsList.get(j).getDepartureTime())) {
                                        returnFlightsListClone.add(returnFlightsList.get(j));
                                    }
                                    if (checkForTime3(returnFlightsList.get(j).getDepartureTime())) {
                                        returnFlightsListClone.add(returnFlightsList.get(j));
                                    }
                                    if (checkForTime4(returnFlightsList.get(j).getDepartureTime())) {
                                        returnFlightsListClone.add(returnFlightsList.get(j));
                                    }
                                }
                                if (checkForOneStop(returnFlightsList.get(j).getStops())) {
                                    if (checkForTime1(returnFlightsList.get(j).getDepartureTime())) {
                                        returnFlightsListClone.add(returnFlightsList.get(j));
                                    }
                                    if (checkForTime2(returnFlightsList.get(j).getDepartureTime())) {
                                        returnFlightsListClone.add(returnFlightsList.get(j));
                                    }
                                    if (checkForTime3(returnFlightsList.get(j).getDepartureTime())) {
                                        returnFlightsListClone.add(returnFlightsList.get(j));
                                    }
                                    if (checkForTime4(returnFlightsList.get(j).getDepartureTime())) {
                                        returnFlightsListClone.add(returnFlightsList.get(j));
                                    }
                                }
                                if (checkForTwoOrMoreStop(returnFlightsList.get(j).getStops())) {
                                    if (checkForTime1(returnFlightsList.get(j).getDepartureTime())) {
                                        returnFlightsListClone.add(returnFlightsList.get(j));
                                    }
                                    if (checkForTime2(returnFlightsList.get(j).getDepartureTime())) {
                                        returnFlightsListClone.add(returnFlightsList.get(j));
                                    }
                                    if (checkForTime3(returnFlightsList.get(j).getDepartureTime())) {
                                        returnFlightsListClone.add(returnFlightsList.get(j));
                                    }
                                    if (checkForTime4(returnFlightsList.get(j).getDepartureTime())) {
                                        returnFlightsListClone.add(returnFlightsList.get(j));
                                    }
                                }
                            } else {
                                if (checkForTime1(returnFlightsList.get(j).getDepartureTime())) {
                                    returnFlightsListClone.add(returnFlightsList.get(j));
                                }
                                if (checkForTime2(returnFlightsList.get(j).getDepartureTime())) {
                                    returnFlightsListClone.add(returnFlightsList.get(j));
                                }
                                if (checkForTime3(returnFlightsList.get(j).getDepartureTime())) {
                                    returnFlightsListClone.add(returnFlightsList.get(j));
                                }
                                if (checkForTime4(returnFlightsList.get(j).getDepartureTime())) {
                                    returnFlightsListClone.add(returnFlightsList.get(j));
                                }
                            }
                        }
                    } else {
                        if (Float.parseFloat(returnFlightsList.get(j).getPrice()) >= selectedLowestPrice && Float.parseFloat(returnFlightsList.get(j).getPrice()) <= selectedHighestPrice) {
                            if (checkForStops()) {
                                if (checkForNonStop(returnFlightsList.get(j).getStops())) {
                                    returnFlightsListClone.add(returnFlightsList.get(j));
                                }
                                if (checkForOneStop(returnFlightsList.get(j).getStops())) {
                                    returnFlightsListClone.add(returnFlightsList.get(j));
                                }
                                if (checkForTwoOrMoreStop(returnFlightsList.get(j).getStops())) {
                                    returnFlightsListClone.add(returnFlightsList.get(j));
                                }
                            } else {
                                returnFlightsListClone.add(returnFlightsList.get(j));
                            }
                        }
                    }
                }
            }
            if (onwardFlightsListClone.size() > 0 && returnFlightsListClone.size() > 0) {
                adapterOnwardFlight = new OnwardFlight(getActivity(), onwardFlightsListClone);
                listViewOnwardFlight.setAdapter(adapterOnwardFlight);
                adapterReturnFlight = new OnwardFlight(getActivity(), returnFlightsListClone);
                listViewReturnFlight.setAdapter(adapterReturnFlight);
                setOnItemClickListenerOnBothList(onwardFlightsListClone, returnFlightsListClone);
                saveFilterState();
                filterDialog.dismiss();
            } else {
                Toast.makeText(context, "No flights available in this selection.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void resetFilter() {
        checkStatus = null;
        filteredAirlines.clear();
        //DFilterAirlineAdapter.test();
        selectedLowestPrice = 0.0f;
        selectedHighestPrice = 0.0f;
        if (listCombo != null) {
            setPriceOnRangeBarGDS();
        } else {
            setPriceOnRangeBar();
        }

        nonStopState = false;
        oneStopState = false;
        twoOrMoreStopState = false;
        timeState1 = false;
        timeState2 = false;
        timeState3 = false;
        timeState4 = false;
        if (listCombo != null) {
            listViewSpecialFareGDS.setAdapter(new AdapterSpecialFareGds(getActivity(), listCombo));
            setOnItemClickOnListCombo(listCombo);
        } else {
            adapterOnwardFlight = new OnwardFlight(getActivity(), onwardFlightsList);
            listViewOnwardFlight.setAdapter(adapterOnwardFlight);
            adapterReturnFlight = new OnwardFlight(getActivity(), returnFlightsList);
            listViewReturnFlight.setAdapter(adapterReturnFlight);
            setOnItemClickListenerOnBothList(onwardFlightsList, returnFlightsList);
        }

        if (nonStopState) {
            nonStop.setChecked(true);
        } else {
            nonStop.setChecked(false);
        }
        if (oneStopState) {
            oneStop.setChecked(true);
        } else {
            oneStop.setChecked(false);
        }
        if (twoOrMoreStopState) {
            twoOrMoreStop.setChecked(true);
        } else {
            twoOrMoreStop.setChecked(false);
        }
        if (timeState1) {
            time1.setChecked(true);
        } else {
            time1.setChecked(false);
        }
        if (timeState2) {
            time2.setChecked(true);
        } else {
            time2.setChecked(false);
        }
        if (timeState3) {
            time3.setChecked(true);
        } else {
            time3.setChecked(false);
        }
        if (timeState4) {
            time4.setChecked(true);
        } else {
            time4.setChecked(false);
        }
        makeAirlinesList();
        Toast.makeText(context, "Filter Reset", Toast.LENGTH_SHORT).show();
    }

    private void saveFilterState() {
        if (nonStop.isChecked()) {
            nonStopState = true;
        } else {
            nonStopState = false;
        }
        if (oneStop.isChecked()) {
            oneStopState = true;
        } else {
            oneStopState = false;
        }
        if (twoOrMoreStop.isChecked()) {
            twoOrMoreStopState = true;
        } else {
            twoOrMoreStopState = false;
        }
        if (time1.isChecked()) {
            timeState1 = true;
        } else {
            timeState1 = false;
        }
        if (time2.isChecked()) {
            timeState2 = true;
        } else {
            timeState2 = false;
        }
        if (time3.isChecked()) {
            timeState3 = true;
        } else {
            timeState3 = false;
        }
        if (time4.isChecked()) {
            timeState4 = true;
        } else {
            timeState4 = false;
        }

    }

    private boolean checkForStops() {
        if (nonStop.isChecked() || oneStop.isChecked() || twoOrMoreStop.isChecked()) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkForNonStop(String stops) {
        if (nonStop.isChecked()) {
            if (stops.equals("0")) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private boolean checkForOneStop(String stops) {
        if (oneStop.isChecked()) {
            if (stops.equals("1")) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private boolean checkForTwoOrMoreStop(String stops) {
        if (twoOrMoreStop.isChecked()) {
            if (Integer.parseInt(stops) >= 2) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private boolean checkForTime() {
        if (time1.isChecked() || time2.isChecked() || time3.isChecked() || time4.isChecked()) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkForTime1(String time) {
        if (time1.isChecked()) {
            if (Integer.parseInt(time.replace(":", "")) < 0600) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private boolean checkForTime2(String time) {
        if (time2.isChecked()) {
            if (Integer.parseInt(time.replace(":", "")) >= 0600 && Integer.parseInt(time.replace(":", "")) < 1200) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private boolean checkForTime3(String time) {
        if (time3.isChecked()) {
            if (Integer.parseInt(time.replace(":", "")) >= 1200 && Integer.parseInt(time.replace(":", "")) < 1800) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private boolean checkForTime4(String time) {
        if (time4.isChecked()) {
            if (Integer.parseInt(time.replace(":", "")) >= 1800) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }


    private void setOnItemClickListenerOnBothList(final ArrayList<Flights> onwardFlights, final ArrayList<Flights> returnFlights) {
        if (onwardFlights != null && returnFlights != null) {
            if (onwardFlights.size() > 0 && returnFlights.size() > 0) {
                listViewOnwardFlight.setItemChecked(0, true);
                listViewReturnFlight.setItemChecked(0, true);
                totalPrice.setText(getString(R.string.symbol_rs) + " " + String.valueOf(Integer.parseInt(onwardFlights.get(0).getPrice()) + Integer.parseInt(returnFlights.get(0).getPrice())));
                selectedDepertureFlight = onwardFlights.get(0);
                listViewOnwardFlight.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        selectedDepertureFlight = onwardFlights.get(position);
                        if (selectedReturnFlight != null) {
                            totalPrice.setText(getString(R.string.symbol_rs) + " " + String.valueOf(Integer.parseInt(selectedDepertureFlight.getPrice()) + Integer.parseInt(selectedReturnFlight.getPrice())));
                        } else {
                            totalPrice.setText(getString(R.string.symbol_rs) + " " + String.valueOf(Integer.parseInt(selectedDepertureFlight.getPrice()) + Integer.parseInt(returnFlights.get(0).getPrice())));
                        }

                    }
                });
                selectedReturnFlight = returnFlights.get(0);
                listViewReturnFlight.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        selectedReturnFlight = returnFlights.get(position);
                        if (selectedDepertureFlight != null) {
                            totalPrice.setText(getString(R.string.symbol_rs) + " " + String.valueOf(Integer.parseInt(selectedDepertureFlight.getPrice()) + Integer.parseInt(selectedReturnFlight.getPrice())));
                        } else {
                            totalPrice.setText(getString(R.string.symbol_rs) + " " + String.valueOf(Integer.parseInt(onwardFlights.get(0).getPrice()) + Integer.parseInt(selectedReturnFlight.getPrice())));
                        }
                    }
                });
            }
        }
    }

    private void setOnItemClickOnListCombo(final ArrayList<ArrayList<Flights>> flightsPairList) {
        if (flightsPairList != null) {
            if (flightsPairList.size() > 0) {
                selectedGdsCombo = flightsPairList.get(0);
                listViewSpecialFareGDS.setItemChecked(0, true);
                totalPrice.setText(getString(R.string.symbol_rs) + " " + Integer.parseInt(flightsPairList.get(0).get(0).getPrice()));
                listViewSpecialFareGDS.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        selectedGdsCombo = flightsPairList.get(position);
                        totalPrice.setText(getString(R.string.symbol_rs) + " " + Integer.parseInt(flightsPairList.get(position).get(0).getPrice()));
                    }
                });
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        AppController.getInstance().trackScreenView("Round Trip Flight List Screen");
    }
}
