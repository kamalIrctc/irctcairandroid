package com.irctc.air.round.trip.international.sorting;

import com.irctc.air.model.FlightOnWardDetailBean;
import com.irctc.air.model.search_result_round_trip.IntlFlights;

import java.util.Comparator;

/**
 * Created by tourism on 4/13/2016.
 */
public class ISortByDuration implements Comparator<IntlFlights>{

    @Override
    public int compare(IntlFlights obj1, IntlFlights obj2) {

//        String str1 = obj1.getFlightOnwardTotalDuration().replace("h ",":").replace("m","");
//        String str2 = obj2.getFlightOnwardTotalDuration().replace("h ",":").replace("m","");

        double durationTimeOne = 0;
        double durationTimeTwo = 0;

        if(obj1.getDuration().contains("m")){

            durationTimeOne = Double.parseDouble(obj1.getDuration().replace("h ", ".").replace("m", ""));
        }
        else{
            durationTimeOne = Double.parseDouble(obj1.getDuration().replace("h", ".00"));

        }

        if(obj2.getDuration().contains("m")){

            durationTimeTwo = Double.parseDouble(obj2.getDuration().replace("h ", ".").replace("m", ""));
        }
        else{
            durationTimeTwo = Double.parseDouble(obj2.getDuration().replace("h", ".00"));

        }

       // double durationTimeTwo = Double.parseDouble(obj2.getFlightOnwardTotalDuration().replace("h ",".").replace("m",""));

        if (durationTimeOne > durationTimeTwo) {
            return 1;
        } else if (durationTimeOne < durationTimeTwo) {
            return -1;
        } else {
            return 0;
        }

    }
}
