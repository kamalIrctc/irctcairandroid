package com.irctc.air.round.trip.domastic.model;

public class LstFlightDetails
{
    private boolean isFreeMeal;
    private boolean isBaggageAllowed;
    private String flightTime;

    private String destinationCity;

    private String originTerminal;

    private String originAirportName;

    private String departureDate;

    private String origin;

    private String departureTime;

    private String flightNumber;

    private String destinationTerminal;

    private String destination;

    private String arrivalTime;

    private String airlineCode;

    private String destinationAirportName;

    private String originCity;

    private String arrivalDate;

    private String halt;

    private String key;

    private String airline;

    private String tarvelClass;

    public String getTarvelClass() {
        return tarvelClass;
    }

    public void setTarvelClass(String tarvelClass) {
        this.tarvelClass = tarvelClass;
    }

    public String getFlightTime ()
    {
        return flightTime;
    }

    public void setFlightTime (String flightTime)
    {
        this.flightTime = flightTime;
    }

    public String getDestinationCity ()
    {
        return destinationCity;
    }

    public void setDestinationCity (String destinationCity)
    {
        this.destinationCity = destinationCity;
    }

    public String getOriginTerminal ()
    {
        return originTerminal;
    }

    public void setOriginTerminal (String originTerminal)
    {
        this.originTerminal = originTerminal;
    }

    public String getOriginAirportName ()
    {
        return originAirportName;
    }

    public void setOriginAirportName (String originAirportName)
    {
        this.originAirportName = originAirportName;
    }

    public String getDepartureDate ()
    {
        return departureDate;
    }

    public void setDepartureDate (String departureDate)
    {
        this.departureDate = departureDate;
    }

    public String getOrigin ()
    {
        return origin;
    }

    public void setOrigin (String origin)
    {
        this.origin = origin;
    }

    public String getDepartureTime ()
    {
        return departureTime;
    }

    public void setDepartureTime (String departureTime)
    {
        this.departureTime = departureTime;
    }

    public String getFlightNumber ()
    {
        return flightNumber;
    }

    public void setFlightNumber (String flightNumber)
    {
        this.flightNumber = flightNumber;
    }

    public String getDestinationTerminal ()
    {
        return destinationTerminal;
    }

    public void setDestinationTerminal (String destinationTerminal)
    {
        this.destinationTerminal = destinationTerminal;
    }

    public String getDestination ()
    {
        return destination;
    }

    public void setDestination (String destination)
    {
        this.destination = destination;
    }

    public String getArrivalTime ()
    {
        return arrivalTime;
    }

    public void setArrivalTime (String arrivalTime)
    {
        this.arrivalTime = arrivalTime;
    }

    public String getAirlineCode ()
    {
        return airlineCode;
    }

    public void setAirlineCode (String airlineCode)
    {
        this.airlineCode = airlineCode;
    }

    public String getDestinationAirportName ()
    {
        return destinationAirportName;
    }

    public void setDestinationAirportName (String destinationAirportName)
    {
        this.destinationAirportName = destinationAirportName;
    }

    public String getOriginCity ()
    {
        return originCity;
    }

    public void setOriginCity (String originCity)
    {
        this.originCity = originCity;
    }

    public String getArrivalDate ()
    {
        return arrivalDate;
    }

    public void setArrivalDate (String arrivalDate)
    {
        this.arrivalDate = arrivalDate;
    }

    public String getHalt ()
    {
        return halt;
    }

    public void setHalt (String halt)
    {
        this.halt = halt;
    }

    public String getKey ()
    {
        return key;
    }

    public void setKey (String key)
    {
        this.key = key;
    }

    public String getAirline ()
    {
        return airline;
    }

    public void setAirline (String airline)
    {
        this.airline = airline;
    }

    public boolean isFreeMeal() {
        return isFreeMeal;
    }

    public void setFreeMeal(boolean freeMeal) {
        isFreeMeal = freeMeal;
    }

    public boolean isBaggageAllowed() {
        return isBaggageAllowed;
    }

    public void setBaggageAllowed(boolean baggageAllowed) {
        isBaggageAllowed = baggageAllowed;
    }



    /*    @Override
    public String toString()
    {
        return "ClassPojo [flightTime = "+flightTime+", destinationCity = "+destinationCity+", originTerminal = "+originTerminal+", originAirportName = "+originAirportName+", departureDate = "+departureDate+", origin = "+origin+", departureTime = "+departureTime+", flightNumber = "+flightNumber+", destinationTerminal = "+destinationTerminal+", destination = "+destination+", arrivalTime = "+arrivalTime+", airlineCode = "+airlineCode+", destinationAirportName = "+destinationAirportName+", originCity = "+originCity+", arrivalDate = "+arrivalDate+", halt = "+halt+", key = "+key+", airline = "+airline+"]";
    }*/
}

		