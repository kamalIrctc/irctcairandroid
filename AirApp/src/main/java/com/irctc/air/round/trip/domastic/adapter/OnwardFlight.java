package com.irctc.air.round.trip.domastic.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.irctc.air.R;
import com.irctc.air.model.search_result_round_trip.IntlFlights;
import com.irctc.air.round.trip.domastic.model.Flights;
import com.irctc.air.util.AppKeys;
import com.irctc.air.util.Pref;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.irctc.air.activity.ActivityMain.context;

/**
 * Created by tourism on 4/23/2016.
 */
public class OnwardFlight extends BaseAdapter {
    ArrayList<Flights> mFlightsClone;
    Context mContext;

    public OnwardFlight(Context context, ArrayList<Flights> lFlightsClone) {
        mContext = context;
        this.mFlightsClone = lFlightsClone;
    }

    @Override
    public int getCount() {
        return mFlightsClone.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        MyHolder holder = null;

        if (row == null) {

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row =  inflater.inflate(R.layout.round_trip_list_item,  null);
            holder = new MyHolder(row);
            row.setTag(holder);
        } else {
            holder = (MyHolder) row.getTag();
        }

        Flights lflightDetails = mFlightsClone.get(position);
        holder.TXT_ROUND_FLIGHT_NO.setText(lflightDetails.getCarrier()+"-"+lflightDetails.getFlightNumber());
        holder.TXT_ROUND_DEPART_TIME.setText(lflightDetails.getDepartureTime());
        holder.TXT_ROUND_ARRIVE_TIME.setText(lflightDetails.getArrivalTime());
        holder.TXT_ROUND_DURATION.setText(lflightDetails.getDuration());
        holder.TXT_ROUND_PRICE.setText(context.getString(R.string.symbol_rs)+lflightDetails.getPrice());

        if (lflightDetails.isBaggageAllowed()) {
            holder.ivBag.setVisibility(View.VISIBLE);
        } else {
            holder.ivBag.setVisibility(View.GONE);
        }

        if(lflightDetails.getStops().equals("0")){
            holder.TXT_ROUND_STOPS.setText("Non Stop");
        }else{
            holder.TXT_ROUND_STOPS.setText(lflightDetails.getStops()+ " Stops");
        }
        Picasso.with(context).load(Pref.getString(context, AppKeys.FLIGHT_ICON_PATH)+lflightDetails.getCarrier()+".png").into(holder.IMG_ROUND_FLIGHT_ICON);
        return row;
    }

    class MyHolder {
 TextView TXT_ROUND_DEPART_TIME,TXT_ROUND_ARRIVE_TIME,TXT_ROUND_DURATION,TXT_ROUND_STOPS,TXT_ROUND_FLIGHT_NO,TXT_ROUND_PRICE;
        ImageView  IMG_ROUND_FLIGHT_ICON, ivBag;
        public MyHolder(View itemView) {
            IMG_ROUND_FLIGHT_ICON = (ImageView) itemView.findViewById(R.id.IMG_ROUND_FLIGHT_ICON);
            TXT_ROUND_DEPART_TIME = (TextView) itemView.findViewById(R.id.TXT_ROUND_DEPART_TIME);
            TXT_ROUND_ARRIVE_TIME = (TextView) itemView.findViewById(R.id.TXT_ROUND_ARRIVE_TIME);
            TXT_ROUND_DURATION = (TextView) itemView.findViewById(R.id.TXT_ROUND_DURATION);
            TXT_ROUND_STOPS = (TextView) itemView.findViewById(R.id.TXT_ROUND_STOPS);
            TXT_ROUND_FLIGHT_NO = (TextView) itemView.findViewById(R.id.TXT_ROUND_FLIGHT_NO);
            TXT_ROUND_PRICE = (TextView) itemView.findViewById(R.id.TXT_ROUND_PRICE);
            ivBag = (ImageView) itemView.findViewById(R.id.ivBag);
        }
    }
}