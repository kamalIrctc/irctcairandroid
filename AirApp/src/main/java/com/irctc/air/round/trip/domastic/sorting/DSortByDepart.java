package com.irctc.air.round.trip.domastic.sorting;



import com.irctc.air.round.trip.domastic.model.Flights;

import java.util.Comparator;

/**
 * Created by tourism on 4/13/2016.
 */
public class DSortByDepart implements Comparator<Flights>{

    @Override
    public int compare(Flights obj1,Flights obj2) {

        double departTimeOne = Double.parseDouble(obj1.getDepartureTime().replace(":", "."));
        double departTimeTwo = Double.parseDouble(obj2.getDepartureTime().replace(":", "."));

//        double departTimeOne = Double.parseDouble(obj1.getFlight().get(0).getFlightDepartureTime().substring(0, 5).replace(":", "."));
//        double departTimeTwo = Double.parseDouble(obj2.getFlight().get(0).getFlightDepartureTime().substring(0, 5).replace(":", "."));

        if (departTimeOne > departTimeTwo) {
            return 1;
        } else if (departTimeOne < departTimeTwo) {
            return -1;
        } else {
            return 0;
        }
    }
}
