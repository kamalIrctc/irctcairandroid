package com.irctc.air.round.trip.international.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.adapter.AdapterRoundTripInternationalListView;
import com.irctc.air.fragment.FragmentPlanner;
import com.irctc.air.header.AirHeader;
import com.irctc.air.model.search_result_round_trip.IntlFlights;
import com.irctc.air.model.search_result_round_trip.model_combo.ModelFlightsCombo;
import com.irctc.air.util.Constant;
import com.irctc.air.util.sorting_round_trip_international.SortByFarePrice;
import com.irctc.air.util.sorting_round_trip_international.SortByFlightName;

import java.util.ArrayList;
import java.util.Collections;

public class SearchResultsComboList extends Fragment implements View.OnClickListener {
    private RecyclerView roundTripInternationalListView;
    ArrayList<ModelFlightsCombo> diffFlightList;
    RelativeLayout layAirline,layPrice;
    ImageView sortIconAirline,sortIconPrice;
    boolean flagFlightName=true,flagFlightPrice=true;
    Activity activity;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMain.backstack = true;
        activity=getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_round_trip_search_results, null);
        AirHeader.showHeaderText((ActivityMain)activity, false, "");
        AirHeader.showRecentSearchIcon(true);
        AirHeader.showDrawerToggleAndToolbar(true, true);
        roundTripInternationalListView = (RecyclerView)view.findViewById(R.id.roundTripInternationalListView);
        layAirline=(RelativeLayout)view.findViewById(R.id.layAirline);
        layPrice=(RelativeLayout)view.findViewById(R.id.layPrice);
        sortIconAirline=(ImageView)view.findViewById(R.id.sortIconAirline);
        sortIconPrice=(ImageView)view.findViewById(R.id.sortIconPrice);
        layAirline.setOnClickListener(this);
        layPrice.setOnClickListener(this);
        roundTripInternationalListView.setLayoutManager(new GridLayoutManager(view.getContext(),1));
        ArrayList<IntlFlights[]> allFlightList= FragmentPlanner.intlFlightsArrayList;
        if(allFlightList.size()>0){
        diffFlightList=new ArrayList<>();
            for(IntlFlights[] allFlights : allFlightList){
                ModelFlightsCombo modelFlightsCombo=new ModelFlightsCombo();
                for(int i=0;i<allFlights.length;i++){
                    if(allFlights[i].getSegmentType().equalsIgnoreCase("O")){
                        modelFlightsCombo.setOnwardFlightsList(allFlights[i]);
                    }else{
                        modelFlightsCombo.setReturnFlightsList(allFlights[i]);
                    }
                }
                if(modelFlightsCombo.getOnwardFlightsList().size()>0 && modelFlightsCombo.getReturnFlightsList().size()>0)
                diffFlightList.add(modelFlightsCombo);
            }
        }

        roundTripInternationalListView.setAdapter(new AdapterRoundTripInternationalListView(diffFlightList));
        return view;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(ActivityMain.backStack2){
        ActivityMain.backstack = false;
            ActivityMain.backStack2=false;
        ActivityMain.lastActiveFragment = Constant.PLANNER_FRAGMENT;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.layAirline:
                layAirline.setBackgroundColor(ContextCompat.getColor(getActivity(),R.color.newGrayLite));
                layPrice.setBackgroundColor(ContextCompat.getColor(getActivity(),android.R.color.transparent));
                if(flagFlightName){
                Collections.sort(diffFlightList,new SortByFlightName());
                roundTripInternationalListView.setAdapter(new AdapterRoundTripInternationalListView(diffFlightList));
                sortIconAirline.setImageResource(R.drawable.arrow_down48);
                flagFlightName=false;
                }else{
                Collections.sort(diffFlightList,new SortByFlightName());
                Collections.reverse(diffFlightList);
                roundTripInternationalListView.setAdapter(new AdapterRoundTripInternationalListView(diffFlightList));
                sortIconAirline.setImageResource(R.drawable.arrow_up48);
                flagFlightName=true;
                }
                break;
            case R.id.layPrice:
                layPrice.setBackgroundColor(ContextCompat.getColor(getActivity(),R.color.newGrayLite));
                layAirline.setBackgroundColor(ContextCompat.getColor(getActivity(),android.R.color.transparent));
                if(flagFlightPrice){
                    Collections.sort(diffFlightList,new SortByFarePrice());
                    roundTripInternationalListView.setAdapter(new AdapterRoundTripInternationalListView(diffFlightList));
                    sortIconPrice.setImageResource(R.drawable.arrow_down48);
                    flagFlightPrice=false;
                }else{
                    Collections.sort(diffFlightList,new SortByFarePrice());
                    Collections.reverse(diffFlightList);
                    roundTripInternationalListView.setAdapter(new AdapterRoundTripInternationalListView(diffFlightList));
                    sortIconPrice.setImageResource(R.drawable.arrow_up48);
                    flagFlightPrice=true;
                }
                break;
        }
    }
}
