package com.irctc.air.round.trip.domastic.model;

public class LstBaggageDetails
{
    private String cabinValue;

    private String paxType;

    private String checkInValue;
    private boolean isKg;

    public String getCabinValue ()
    {
        return cabinValue;
    }

    public void setCabinValue (String cabinValue)
    {
        this.cabinValue = cabinValue;
    }

    public String getPaxType ()
    {
        return paxType;
    }

    public void setPaxType (String paxType)
    {
        this.paxType = paxType;
    }

    public String getCheckInValue ()
    {
        return checkInValue;
    }

    public boolean getIsKg() {
        return isKg;
    }

    public void setIsKg(boolean isKg) {
        this.isKg = isKg;
    }

    public void setCheckInValue (String checkInValue)
    {
        this.checkInValue = checkInValue;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [cabinValue = "+cabinValue+", paxType = "+paxType+", checkInValue = "+checkInValue+"]";
    }
}

	