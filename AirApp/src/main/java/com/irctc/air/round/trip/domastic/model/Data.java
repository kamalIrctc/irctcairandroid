package com.irctc.air.round.trip.domastic.model;

public class Data
{
    //private null intlFlights;

    private Flights[] flights;

    private String searchKey;

    private String isInternational;

    private SplFlights splFlights;

    //private null lstLowFareFlight;

    /*public null getIntlFlights ()
    {
        return intlFlights;
    }

    public void setIntlFlights (null intlFlights)
    {
        this.intlFlights = intlFlights;
    }
*/
    public Flights[] getFlights ()
    {
        return flights;
    }

    public void setFlights (Flights[] flights)
    {
        this.flights = flights;
    }

    public String getSearchKey ()
    {
        return searchKey;
    }

    public void setSearchKey (String searchKey)
    {
        this.searchKey = searchKey;
    }

    public String getIsInternational ()
    {
        return isInternational;
    }

    public void setIsInternational (String isInternational)
    {
        this.isInternational = isInternational;
    }

    public SplFlights getSplFlights ()
    {
        return splFlights;
    }

    public void setSplFlights (SplFlights splFlights)
    {
        this.splFlights = splFlights;
    }

  /*  public null getLstLowFareFlight ()
    {
        return lstLowFareFlight;
    }

    public void setLstLowFareFlight (null lstLowFareFlight)
    {
        this.lstLowFareFlight = lstLowFareFlight;
    }
*/
  /*  @Override
    public String toString()
    {
        return "ClassPojo [intlFlights = "+intlFlights+", flights = "+flights+", searchKey = "+searchKey+", isInternational = "+isInternational+", splFlights = "+splFlights+", lstLowFareFlight = "+lstLowFareFlight+"]";
    }*/
}

			