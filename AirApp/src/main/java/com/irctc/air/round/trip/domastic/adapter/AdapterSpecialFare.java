package com.irctc.air.round.trip.domastic.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.irctc.air.R;
import com.irctc.air.fragment.FragmentPlanner;
import com.irctc.air.round.trip.domastic.fragment.DSearchResults;
import com.irctc.air.round.trip.domastic.model.Airlines;

import java.util.ArrayList;

public class AdapterSpecialFare extends RecyclerView.Adapter<AdapterSpecialFare.MyViewHolder> {
 
    private ArrayList<Airlines> airlinesList;
    private Activity activity;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public View viewLine;
 
        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.textSpecialFare);
            viewLine = (View) view.findViewById(R.id.view);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(FragmentPlanner.searchResultData.getSplFlights().getFlights().length>0){
                        DSearchResults.onwardFlightsList=new ArrayList<>();
                        DSearchResults.returnFlightsList=new ArrayList<>();
                        DSearchResults.selectedDepertureFlight=null;
                        DSearchResults.selectedReturnFlight=null;
                        int position = getAdapterPosition();
                        for(int i=0;i<FragmentPlanner.searchResultData.getSplFlights().getFlights().length;i++){
                           // String s1=FragmentPlanner.searchResultData.getSplFlights().getFlights()[i].getSegmentType();
                          //  String s2=FragmentPlanner.searchResultData.getSplFlights().getFlights()[i].getCarrier();
                          //  String s3=airlinesList.get(position).getCode();
                            if(FragmentPlanner.searchResultData.getSplFlights().getFlights()[i].getSegmentType().equalsIgnoreCase("O")
                                    && FragmentPlanner.searchResultData.getSplFlights().getFlights()[i].getCarrier().equals(airlinesList.get(position).getCode()) ){
                                DSearchResults.onwardFlightsList.add(FragmentPlanner.searchResultData.getSplFlights().getFlights()[i]);
                            }else if(FragmentPlanner.searchResultData.getSplFlights().getFlights()[i].getCarrier().equals(airlinesList.get(position).getCode())){
                                DSearchResults.returnFlightsList.add(FragmentPlanner.searchResultData.getSplFlights().getFlights()[i]);
                            }
                        }
                        if(DSearchResults.onwardFlightsList.size()==0){
                            Toast.makeText(activity.getApplicationContext(),"No Flights Available",Toast.LENGTH_SHORT).show();
                        }else{
                            Intent intent=new Intent();
                            intent.setAction("SPECIAL_FARE");
                            activity.sendBroadcast(intent);
                        }
                    }else{
                        DSearchResults.onwardFlightsList=new ArrayList<>();
                        DSearchResults.returnFlightsList=new ArrayList<>();
                        DSearchResults.selectedDepertureFlight=null;
                        DSearchResults.selectedReturnFlight=null;
                        Intent intent=new Intent();
                        intent.setAction("SPECIAL_FARE");
                        activity.sendBroadcast(intent);

                    }

                }
            });
        }
    }
 
 
    public AdapterSpecialFare(Activity activity, ArrayList<Airlines> airlinesList) {
        this.airlinesList = airlinesList;
        this.activity = activity;
    }
 
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_special_fare_list, parent, false);
        return new MyViewHolder(itemView);
    }
 
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Airlines airline = airlinesList.get(position);
        holder.title.setText(airline.getName()+"\n"+activity.getString(R.string.symbol_rs)+airline.getPrice());
        if(position==(airlinesList.size()-1)){
        holder.viewLine.setVisibility(View.GONE);
        }
    }
 
    @Override
    public int getItemCount() {
        return airlinesList.size();
    }
}