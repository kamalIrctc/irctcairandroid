package com.irctc.air.round.trip.international.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.auth0.android.jwt.JWT;
import com.irctc.air.AppController;
import com.irctc.air.Database.AirDatabase;
import com.irctc.air.R;
import com.irctc.air.SendMailService;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.fragment.FragmentLogin;
import com.irctc.air.fragment.FragmentPlanner;
import com.irctc.air.header.AirHeader;
import com.irctc.air.model.book_ticket.PaymentPage;
import com.irctc.air.navigationdrawer.SliderMenu;
import com.irctc.air.networking.Networking;
import com.irctc.air.round.trip.domastic.fragment.DFragmentAddPassengers;
import com.irctc.air.round.trip.domastic.fragment.DFragmentTicket;
import com.irctc.air.util.AppKeys;
import com.irctc.air.util.Pref;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by asif on 4/30/2016.
 */
public class IFragmentPayment extends Fragment {
    WebView webViewPayment;
    ProgressDialog progressDialog;
    boolean showTicket = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment, null);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Processing, Please wait...");
        progressDialog.setCancelable(false);
        AirHeader.showRecentSearchIcon(false);
        AirHeader.showHeaderText((ActivityMain) getActivity(), true, "Payment Getaway");
        AirHeader.showDrawerToggleAndToolbar(false, false);
        webViewPayment = (WebView) view.findViewById(R.id.webViewPayment);
        if(FragmentLogin.handelBack){
            android.support.v4.app.FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction=  fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_layout,new FragmentPlanner());
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
        webViewPayment.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //view.loadUrl(url.replace("localhost:18081", Networking.BASE_URL_IP));
                view.loadUrl(url);
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                if (progressDialog != null)
                    progressDialog.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (progressDialog != null)
                    progressDialog.dismiss();
                if (url.contains(Networking.CLOSE_PAYMENT_PAGE) && showTicket) {
                    Intent serviceIntent = new Intent(getActivity(),SendMailService.class);
                    serviceIntent.putExtra("txnId",  IFragmentAddPassengers.orderId);
                    getActivity().startService(serviceIntent);
                    progressDialog = null;
                    showTicket = false;
                    ActivityMain.paymentPage = false;
                    AirDatabase airDatabase=new AirDatabase(getActivity());
                    JWT jwt=new JWT(airDatabase.getAuthToken());
                    Date dateExp=jwt.getExpiresAt();
                    Date dateCurr=new Date();
                    long duration  = dateExp.getTime()- dateCurr.getTime();
                    long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
                    if(diffInMinutes<=0){
                        Context ctx=getActivity().getApplicationContext();
                        Pref.setString(ctx, AppKeys.USER_DETAIL_ADDRESS_1,"");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_ADDRESS_2,"");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_FIRST_NAME,"");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_LAST_NAME,"");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_CITY,"");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_STATE,"");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_COUNTRY,"");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_PIN_CODE,"");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_EMAIL,"");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_MOBILE_NO,"");
                        Pref.setBoolean(ctx,false);
                        SliderMenu.txtUserId.setText("Guest");
                        new AirDatabase(getActivity()).removeAuthToken();
                        AppController.isComingFromFareQuote = true;
                        AppController.routeType="RoundTripInternationalTicket";
                        android.support.v4.app.FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction=  fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.frame_layout,new FragmentLogin());
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                    }else {
                        FragmentTransaction transaction;
                        transaction = getActivity().getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_layout, new IFragmentTicket());
                        transaction.commit();
                    }

        /*            FragmentTransaction transaction;
                    transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.frame_layout, new IFragmentTicket());
                    transaction.commit();*/
                }
            }
        });
        WebSettings webSettings = webViewPayment.getSettings();
        webSettings.setJavaScriptEnabled(true);
        String paymentPage = PaymentPage.get(
                IFragmentAddPassengers.orderId,
                IFragmentAddPassengers.txnAmount,
                IFragmentAddPassengers.paymentUrl,
                IFragmentAddPassengers.callbackUrl,
                IFragmentAddPassengers.custId,
                IFragmentAddPassengers.appCode,
                IFragmentAddPassengers.txnType);
        webViewPayment.loadDataWithBaseURL(null, paymentPage, "text/html", "UTF-8", null);
        webViewPayment.setVerticalScrollBarEnabled(false);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
        ActivityMain.paymentPage = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
        ActivityMain.paymentPage = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        AppController.getInstance().trackScreenView("Payment Screen");
    }
}