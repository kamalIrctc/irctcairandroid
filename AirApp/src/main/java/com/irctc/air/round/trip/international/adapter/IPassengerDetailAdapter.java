package com.irctc.air.round.trip.international.adapter;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.adapter.PassengerListAdapter;
import com.irctc.air.fragment.FragmentPlanner;
import com.irctc.air.model.passenger.ModelPassengerDetails;
import com.irctc.air.util.AssetsUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by vivek on 5/13/2016.
 */
public class IPassengerDetailAdapter extends BaseAdapter {

    private ActivityMain mainActivity;
    private Context context;
    ArrayList<ModelPassengerDetails> arrayListPassenger;
    private LayoutInflater mInflater;
    private FragmentActivity fragmantmanageCtx;
    Calendar myCalendar;

    private String minMaxDate;
    // added by asif
    int adultCount = 0, childCount = 0, infantCount = 0;

    public IPassengerDetailAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    public IPassengerDetailAdapter(ArrayList<ModelPassengerDetails> arrayListPassenger, Context contxt) {
        this.context = contxt;
        this.arrayListPassenger = arrayListPassenger;
        //this.IsAdultAgeAllowed = isAirAsia;
        this.fragmantmanageCtx = (FragmentActivity) contxt;
        mInflater = LayoutInflater.from(context);
        this.mainActivity = (ActivityMain) contxt;
        myCalendar = Calendar.getInstance();

        if (mainActivity.isOneWaySelected) minMaxDate = FragmentPlanner.travellingDate;
        else minMaxDate = changeDateFormat(FragmentPlanner.travellReturnDate);
    }


    public int getCount() {
        return arrayListPassenger.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int groupPosition, View convertView, ViewGroup parent) {

        final ViewHolder holder;
//            if (convertView == null) {
        convertView = mInflater.inflate(R.layout.layout_add_passenger_international, null);
        holder = new ViewHolder(context);
        holder.calendar = Calendar.getInstance();
        holder.itemHeader = (TextView) convertView.findViewById(R.id.itemHeader);
        holder.option1 = (RadioButton) convertView.findViewById(R.id.option1);
        holder.option2 = (RadioButton) convertView.findViewById(R.id.option2);
        holder.option3 = (RadioButton) convertView.findViewById(R.id.option3);
        holder.textViewDOB = (TextView) convertView.findViewById(R.id.textViewDOB);
        holder.pass_issue_date = (TextView) convertView.findViewById(R.id.pass_issue_date);
        holder.pass_exp_date = (TextView) convertView.findViewById(R.id.pass_exp_date);
        holder.layoutDOB = (LinearLayout) convertView.findViewById(R.id.layoutDOB);
        holder.layoutPassportIssueDate = (LinearLayout) convertView.findViewById(R.id.layoutPassportIssueDate);
        holder.layoutPassportExpireDate = (LinearLayout) convertView.findViewById(R.id.layoutPassportExpireDate);
        holder.passportLayout = (LinearLayout) convertView.findViewById(R.id.passportLayout);
        holder.firstName = (AutoCompleteTextView) convertView.findViewById(R.id.ET_FIRST_NAME);
        holder.lastName = (AutoCompleteTextView) convertView.findViewById(R.id.ET_LAST_NAME);
        holder.country = (Spinner) convertView.findViewById(R.id.country);
        List<String> arrayList = new ArrayList<>();
        try {
            final String countriesList = AssetsUtils.AssetJSONFile("countries.json", context);
            JSONArray jsonArray = new JSONArray(countriesList);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                arrayList.add(jsonObject.get("name").toString());
            }
            holder.country.getBackground().setColorFilter(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
            holder.country.setAdapter(new ArrayAdapter(context, R.layout.spinner_item_layout_default,R.id.textViewListItem, arrayList));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        holder.layoutPassportIssueDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                DatePickerDialog dialog = passportIssueDatePicker(holder.pass_issue_date);
                dialog.show();

                /*DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        holder.calendar.set(Calendar.YEAR, year);
                        holder.calendar.set(Calendar.MONTH, monthOfYear);
                        holder.calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        int month22 = monthOfYear + 1;
                        String month2;
                        if (month22 < 10) {
                            month2 = "0" + String.valueOf(month22);
                        } else {
                            month2 = String.valueOf(month22);
                        }
                        holder.pass_issue_date.setText(year + "-" + month2 + "-" + dayOfMonth);
                        holder.pass_issue_date.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.darker_gray));
                    }
                };
                new DatePickerDialog(mainActivity, date, holder.calendar.get(Calendar.YEAR), holder.calendar.get(Calendar.MONTH), holder.calendar.get(Calendar.DAY_OF_MONTH)).show();
*/            }
        });

        holder.layoutPassportExpireDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                DatePickerDialog dialog = passportExpiryDatePicker(holder.pass_exp_date);
                dialog.show();
                /*DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        holder.calendar.set(Calendar.YEAR, year);
                        holder.calendar.set(Calendar.MONTH, monthOfYear);
                        holder.calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        int month22 = monthOfYear + 1;
                        String month2;
                        if (month22 < 10) {
                            month2 = "0" + String.valueOf(month22);
                        } else {
                            month2 = String.valueOf(month22);
                        }
                        holder.pass_exp_date.setText(year + "-" + month2 + "-" + dayOfMonth);
                        holder.pass_exp_date.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.darker_gray));
                    }
                };

                //DatePickerDialog dialog = new DatePickerDialog(mainActivity, date, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

                //dialog.getDatePicker().setMinDate(getChildMinDate(minMaxDate));
                new DatePickerDialog(mainActivity, date, holder.calendar.get(Calendar.YEAR), holder.calendar.get(Calendar.MONTH), holder.calendar.get(Calendar.DAY_OF_MONTH)).show();
*/
            }
        });


        holder.option1.setOnCheckedChangeListener(holder);
        holder.option2.setOnCheckedChangeListener(holder);
        holder.option3.setOnCheckedChangeListener(holder);
        //holder.option1.setChecked(true);
        holder.itemHeader.setText(arrayListPassenger.get(groupPosition).getPassengerTypeString());

        if (PassengerListAdapter.selectedPassengers != null) {
            if (arrayListPassenger.get(groupPosition).getPassengerType() != null) {
                switch (arrayListPassenger.get(groupPosition).getPassengerType()) {
                    case "0":
                        switch (arrayListPassenger.get(groupPosition).getTitleType()) {
                            case "0":
                                holder.option1.setChecked(true);
                                break;
                            case "1":
                                holder.option2.setChecked(true);
                                break;
                            case "2":
                                holder.option3.setChecked(true);
                                break;
                        }
                        break;
                    case "1":
                        switch (arrayListPassenger.get(groupPosition).getTitleType()) {
                            case "0":
                                holder.option1.setChecked(true);
                                break;
                            case "1":
                                holder.option2.setChecked(true);
                                break;
                            case "2":
                                holder.option2.setChecked(true);
                                break;
                        }
                        break;
                    case "2":
                        switch (arrayListPassenger.get(groupPosition).getTitleType()) {
                            case "0":
                                holder.option1.setChecked(true);
                                break;
                            case "1":
                                holder.option2.setChecked(true);
                                break;
                            case "2":
                                holder.option2.setChecked(true);
                                break;
                        }
                        break;
                }
            }
        }

        if (arrayListPassenger.get(groupPosition).getFirstName() != null) {
            if (!arrayListPassenger.get(groupPosition).getFirstName().equals("")) {
                holder.firstName.setText(arrayListPassenger.get(groupPosition).getFirstName());
            }
        }
        if (arrayListPassenger.get(groupPosition).getLastName() != null) {
            if (!arrayListPassenger.get(groupPosition).getLastName().equals("")) {
                holder.lastName.setText(arrayListPassenger.get(groupPosition).getLastName());
            }
        }

        if (arrayListPassenger.get(groupPosition).getDob() != null) {
            if (!arrayListPassenger.get(groupPosition).getDob().equals("")) {
                holder.textViewDOB.setText(arrayListPassenger.get(groupPosition).getDob());
            }
        }


        if (arrayListPassenger.get(groupPosition).getPassengerTypeString().equals("Adult")) {
            holder.option1.setText("Mr.");
            holder.option2.setText("Miss");
            holder.option3.setText("Mrs");
            holder.layoutDOB.setVisibility(View.VISIBLE);
        } else if (arrayListPassenger.get(groupPosition).getPassengerTypeString().equals("Child")) {
            holder.option1.setText("Master");
            holder.option3.setText("Miss");
            holder.option2.setVisibility(View.GONE);
            holder.layoutDOB.setVisibility(View.VISIBLE);
        } else if (arrayListPassenger.get(groupPosition).getPassengerTypeString().equals("Infant")) {
            holder.option1.setText("Master");
            holder.option3.setText("Miss");
            holder.option2.setVisibility(View.GONE);
            holder.layoutDOB.setVisibility(View.VISIBLE);
        }
        holder.passportLayout.setVisibility(View.VISIBLE);
        final int month = holder.calendar.get(Calendar.MONTH) + 1;
        String month1;
        if (month < 10) {
            month1 = "0" + String.valueOf(month);
        } else {
            month1 = String.valueOf(month);
        }
        if (arrayListPassenger.get(groupPosition).getPassengerTypeString().equals("Adult")) {
            holder.calendar.set(Calendar.YEAR, holder.calendar.get(Calendar.YEAR) - 12);
            //if (arrayListPassenger.get(groupPosition).getDob() == null)
            //    holder.textViewDOB.setText(holder.calendar.get(Calendar.DAY_OF_MONTH) + "/" + month1 + "/" + String.valueOf(holder.calendar.get(Calendar.YEAR)));
            holder.layoutDOB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    DatePickerDialog dialog = getAdultDatePicker(holder.textViewDOB);
                    dialog.show();
                }
            });
        } else if (arrayListPassenger.get(groupPosition).getPassengerTypeString().equals("Child")) {
            holder.calendar.set(Calendar.YEAR, holder.calendar.get(Calendar.YEAR) - 2);
            //if (arrayListPassenger.get(groupPosition).getDob() == null)
            //    holder.textViewDOB.setText(holder.calendar.get(Calendar.DAY_OF_MONTH) + "/" + month1 + "/" + String.valueOf(holder.calendar.get(Calendar.YEAR)));
            holder.layoutDOB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {

                    DatePickerDialog dialog = getChildDatePicker(holder.textViewDOB);
                    dialog.show();
                }
            });
        } else if (arrayListPassenger.get(groupPosition).getPassengerTypeString().equals("Infant")) {
            //if (arrayListPassenger.get(groupPosition).getDob() == null)
            //    holder.textViewDOB.setText(holder.calendar.get(Calendar.DAY_OF_MONTH) + "/" + month1 + "/" + holder.calendar.get(Calendar.YEAR));
            holder.layoutDOB.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    DatePickerDialog dialog = getInfantDatePicker(holder.textViewDOB);
                    dialog.show();

                }
            });
        }
        return convertView;
    }

    private class ViewHolder implements CompoundButton.OnCheckedChangeListener {
        TextView itemHeader, textViewDOB, pass_issue_date, pass_exp_date;
        LinearLayout passportLayout;
        LinearLayout layoutDOB, layoutPassportIssueDate, layoutPassportExpireDate;
        RadioButton option1, option2, option3;
        Context context;
        Calendar calendar;
        AutoCompleteTextView firstName, lastName;
        Spinner country;

        private ViewHolder(Context context) {
            this.context = context;
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (option1.isChecked()) {
                option1.setBackgroundColor(ContextCompat.getColor(context, R.color.colorLightBlue));
                option2.setBackgroundColor(ContextCompat.getColor(context, R.color.colorDARKGrey));
                option3.setBackgroundColor(ContextCompat.getColor(context, R.color.colorDARKGrey));
            }
            if (option2.isChecked()) {
                option2.setBackgroundColor(ContextCompat.getColor(context, R.color.colorLightBlue));
                option1.setBackgroundColor(ContextCompat.getColor(context, R.color.colorDARKGrey));
                option3.setBackgroundColor(ContextCompat.getColor(context, R.color.colorDARKGrey));
            }
            if (option3.isChecked()) {
                option3.setBackgroundColor(ContextCompat.getColor(context, R.color.colorLightBlue));
                option2.setBackgroundColor(ContextCompat.getColor(context, R.color.colorDARKGrey));
                option1.setBackgroundColor(ContextCompat.getColor(context, R.color.colorDARKGrey));
            }
        }
    }

    private DatePickerDialog passportIssueDatePicker(final TextView textViewDOB) {
        final Calendar calendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                int month22 = monthOfYear + 1;
                String month2;
                if (month22 < 10) {
                    month2 = "0" + String.valueOf(month22);
                } else {
                    month2 = String.valueOf(month22);
                }
                textViewDOB.setText(dayOfMonth + "/" + month2 + "/" + year);
                textViewDOB.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.darker_gray));
            }
        };
        DatePickerDialog dialog = new DatePickerDialog(mainActivity, date, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        dialog.getDatePicker().setMinDate(stringToDate("01/01/2000"));
        dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        return dialog;
    }

    private DatePickerDialog passportExpiryDatePicker(final TextView textViewDOB) {
        final Calendar calendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                int month22 = monthOfYear + 1;
                String month2;
                if (month22 < 10) {
                    month2 = "0" + String.valueOf(month22);
                } else {
                    month2 = String.valueOf(month22);
                }
                textViewDOB.setText(dayOfMonth + "/" + month2 + "/" + year);
                textViewDOB.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.darker_gray));
            }
        };
        DatePickerDialog dialog = new DatePickerDialog(mainActivity, date, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        dialog.getDatePicker().setMinDate(System.currentTimeMillis());
        return dialog;
    }

    private DatePickerDialog getAdultDatePicker(final TextView textViewDOB) {
        final Calendar calendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                int month22 = monthOfYear + 1;
                String month2;
                if (month22 < 10) {
                    month2 = "0" + String.valueOf(month22);
                } else {
                    month2 = String.valueOf(month22);
                }
                textViewDOB.setText(dayOfMonth + "/" + month2 + "/" + year);
                textViewDOB.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.darker_gray));
            }
        };
        DatePickerDialog dialog = new DatePickerDialog(mainActivity, date, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        //dialog.getDatePicker().setMinDate(getChildMinDate(minMaxDate));
        dialog.getDatePicker().setMaxDate(getAdultMaxDate(minMaxDate));
        return dialog;
    }

    private long getAdultMinDate(String date) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date myDate = sdf.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(myDate);
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            calendar.add(Calendar.YEAR, -12);
            return calendar.getTimeInMillis() + 1000;
            //return ( sdf.format( calendar.getTime() ) );
        } catch (Exception e) {
            e.printStackTrace();
            return new Date().getTime();
        }
    }

    private long getAdultMaxDate(String date) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date myDate = sdf.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(myDate);
            calendar.add(Calendar.DAY_OF_MONTH, 2);
            calendar.add(Calendar.YEAR, -12);
            return calendar.getTimeInMillis() - 1000;
            //return ( sdf.format( calendar.getTime() ) );
        } catch (Exception e) {
            e.printStackTrace();
            return new Date().getTime();
        }
    }

    private DatePickerDialog getChildDatePicker(final TextView textViewDOB) {
        final Calendar calendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                int month22 = monthOfYear + 1;
                String month2;
                if (month22 < 10) {
                    month2 = "0" + String.valueOf(month22);
                } else {
                    month2 = String.valueOf(month22);
                }
                textViewDOB.setText(dayOfMonth + "/" + month2 + "/" + year);
                textViewDOB.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.darker_gray));
            }
        };
        DatePickerDialog dialog = new DatePickerDialog(mainActivity, date, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        dialog.getDatePicker().setMinDate(getChildMinDate(minMaxDate));
        dialog.getDatePicker().setMaxDate(getChildMaxDate(minMaxDate));
        return dialog;
    }

    private long getChildMinDate(String date) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date myDate = sdf.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(myDate);
            calendar.add(Calendar.DAY_OF_MONTH, 2);
            calendar.add(Calendar.YEAR, -12);
            return calendar.getTimeInMillis();
            //return ( sdf.format( calendar.getTime() ) );
        } catch (Exception e) {
            e.printStackTrace();
            return new Date().getTime();
        }
    }

    private long getChildMaxDate(String date) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date myDate = sdf.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(myDate);
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            calendar.add(Calendar.YEAR, -2);
            return calendar.getTimeInMillis();
            //return ( sdf.format( calendar.getTime() ) );
        } catch (Exception e) {
            e.printStackTrace();
            return new Date().getTime();
        }
    }

    private DatePickerDialog getInfantDatePicker(final TextView textViewDOB) {
        final Calendar calendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                int month22 = monthOfYear + 1;
                String month2;
                if (month22 < 10) {
                    month2 = "0" + String.valueOf(month22);
                } else {
                    month2 = String.valueOf(month22);
                }
                textViewDOB.setText(dayOfMonth + "/" + month2 + "/" + year);
                textViewDOB.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.darker_gray));
            }
        };
        DatePickerDialog dialog = new DatePickerDialog(mainActivity, date, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        dialog.getDatePicker().setMinDate(getInfantMinDate(minMaxDate));
        dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        return dialog;
    }

    private long getInfantMinDate(String date) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date myDate = sdf.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(myDate);
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            calendar.add(Calendar.YEAR, -2);
            return calendar.getTimeInMillis();
            //return ( sdf.format( calendar.getTime() ) );
        } catch (Exception e) {
            e.printStackTrace();
            return new Date().getTime();
        }
    }

    private String changeDateFormat(String date) {
        try {
            SimpleDateFormat in = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat out = new SimpleDateFormat("dd/MM/yyyy");
            Date myDate = in.parse(date);

            return out.format(myDate);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private long stringToDate(String date) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date myDate = sdf.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(myDate);
            return calendar.getTimeInMillis();
            //return ( sdf.format( calendar.getTime() ) );
        } catch (Exception e) {
            e.printStackTrace();
            return new Date().getTime();
        }
    }
}