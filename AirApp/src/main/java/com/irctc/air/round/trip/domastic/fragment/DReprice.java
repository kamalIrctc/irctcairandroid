package com.irctc.air.round.trip.domastic.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.auth0.android.jwt.JWT;
import com.irctc.air.AppController;
import com.irctc.air.Database.AirDatabase;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.adapter.FareQuoteOnwardFlightInfoAdapter;
import com.irctc.air.fragment.FragmentAddPassengers;
import com.irctc.air.fragment.FragmentLogin;
import com.irctc.air.fragment.FragmentOneWayFlight;
import com.irctc.air.fragment.FragmentReprice;
import com.irctc.air.header.AirHeader;
import com.irctc.air.model.route_list.ModelRoutListItem;
import com.irctc.air.navigationdrawer.SliderMenu;
import com.irctc.air.networking.Networking;
import com.irctc.air.round.trip.domastic.model.LstBaggageDetails;
import com.irctc.air.util.AlertDialogUtil;
import com.irctc.air.util.AppKeys;
import com.irctc.air.util.Constant;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.Pref;
import com.irctc.air.util.ProjectUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by asif on 4/30/2016.
 */
public class DReprice extends Fragment {

    private ActivityMain mainActivity;
    private Button btnSubmit;
    private ProgressBar progressBar;
    private Dialog lObjDialogShowFlightDetails;
    private TextView flightBaseFare, flightTaxes, flightTotal, irctcAgentServiceTax, irctcBookingCharges;
    private ListView listViewOnward, listViewReturn;
    FareQuoteOnwardFlightInfoAdapter mOnwardFlightAdapter;
    LinearLayout mOnwardFlightlayoutMain;
    TextView mOnwardFlightFromToHeader, mOnwardFlightToHeader, mOnwardHeaderDuration, mOnwardRefundable, returnCodeFrom, returnCodeTo;
    CardView mReturnFlightlayoutMain;
    TextView mReturnFlightFromToHeader, mReturnFlightToHeader, mReturnHeaderDuration, mReturnRefundable;
    TextView mGdsInternationalCodeFrom, mGdsInternationalCodeTo, textViewSGST, textViewCGST, onwardText, returnText;
    LinearLayout mGdsInterCode, mNormalCode;
    ImageView imgReturnRefundable;
    ImageView imgOnwardRefundable;
    ImageView ivBag, ivReturnBag;
    TextView tvOnlyHandBaggage, tvOnlyHandBaggageReturn,tv_no_free_meel, tv_no_free_meel_return;
    private CheckBox checkAccept;
    private com.irctc.air.round.trip.domastic.model.LstFlightDetails[] onwardFlightDetails, returnFlightDetails;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = (ActivityMain) getActivity();
        onwardFlightDetails = DSearchResults.selectedDepertureFlight.getLstFlightDetails();
        returnFlightDetails = DSearchResults.selectedReturnFlight.getLstFlightDetails();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fare_quote, null);
        initializeVariable(view);
        AirHeader.showRecentSearchIcon(false);
        AirHeader.showFareQuoteHeader(mainActivity, "Preconfirmation");
        AirHeader.showDrawerToggleAndToolbar(false, true);
        setData();
        return view;
    }

    private void setData() {
        setOnwardData();
        setReturnData();
        mOnwardFlightFromToHeader.setText(DSearchResults.selectedDepertureFlight.getDepartureCityWithCode());
        mOnwardFlightToHeader.setText(DSearchResults.selectedDepertureFlight.getArrivalCityWithCode());
        returnCodeFrom.setText(DSearchResults.selectedDepertureFlight.getArrivalCityWithCode());
        returnCodeTo.setText(DSearchResults.selectedDepertureFlight.getDepartureCityWithCode());
        flightBaseFare.setText(mainActivity.getResources().getString(R.string.Rs) + DSearchResults.data.getLstFareDetails()[0].getBaseFare());
        flightTaxes.setText(mainActivity.getResources().getString(R.string.Rs) + " " + DSearchResults.data.getLstFareDetails()[0].getTax());
        irctcBookingCharges.setText(mainActivity.getResources().getString(R.string.Rs) + DSearchResults.data.getLstFareDetails()[0].getIrctcCharge());
        flightTotal.setText(mainActivity.getResources().getString(R.string.Rs) + DSearchResults.data.getLstFareDetails()[0].getTotal());
        textViewSGST.setText(mainActivity.getResources().getString(R.string.Rs) + DSearchResults.data.getLstFareDetails()[0].getGst());
        //textViewCGST.setText(mainActivity.getResources().getString(R.string.Rs)+ DSearchResults.data.getLstFareDetails()[0].getCgst());
    }

    private void initializeVariable(View view) {
        mOnwardFlightlayoutMain = (LinearLayout) view.findViewById(R.id.ONWARD_LAYOUT_MAIN);
        listViewOnward = (ListView) view.findViewById(R.id.FARE_ONWARD_QUOTE_LISTVIEW);
        listViewReturn = (ListView) view.findViewById(R.id.FARE_RETURN_QUOTE_LISTVIEW);
        onwardText = (TextView) view.findViewById(R.id.onwardText);
        returnText = (TextView) view.findViewById(R.id.returnText);
        onwardText.setVisibility(View.VISIBLE);
        returnText.setVisibility(View.VISIBLE);
        mOnwardFlightFromToHeader = (TextView) view.findViewById(R.id.ONWARDS_HEADER_FROM_TO);
        mOnwardFlightToHeader = (TextView) view.findViewById(R.id.ONWARDS_HEADER_TO_CODE);
        returnCodeFrom = (TextView) view.findViewById(R.id.RETURN_HEADER_FROM_TO);
        returnCodeTo = (TextView) view.findViewById(R.id.RETURN_HEADER_TO_CODE);

        mOnwardHeaderDuration = (TextView) view.findViewById(R.id.ONWARDS_HEADER_DRATION);
        mOnwardRefundable = (TextView) view.findViewById(R.id.ONWARD_REFUNDABLE_TXT);
        imgOnwardRefundable = (ImageView) view.findViewById(R.id.ONWARD_REFUNDABLE_IMG);
        ivBag = (ImageView) view.findViewById(R.id.ivBag);
        ivReturnBag = (ImageView) view.findViewById(R.id.ivReturnBag);
        tvOnlyHandBaggage = (TextView) view.findViewById(R.id.tvOnlyHandBaggage);
        tvOnlyHandBaggageReturn = (TextView) view.findViewById(R.id.tvOnlyHandBaggageReturn);
        tv_no_free_meel = (TextView) view.findViewById(R.id.tv_no_free_meel);
        tv_no_free_meel_return = (TextView) view.findViewById(R.id.tv_no_free_meel_return);
        mReturnFlightlayoutMain = (CardView) view.findViewById(R.id.RETURN_LAYOUT_MAIN);
        mReturnFlightFromToHeader = (TextView) view.findViewById(R.id.RETURN_HEADER_FROM_TO);
        mReturnFlightToHeader = (TextView) view.findViewById(R.id.RETURN_HEADER_TO_CODE);

        mReturnHeaderDuration = (TextView) view.findViewById(R.id.RETURN_HEADER_DRATION);
        mReturnRefundable = (TextView) view.findViewById(R.id.RETURN_REFUNDABLE_TXT);
        imgReturnRefundable = (ImageView) view.findViewById(R.id.RETURN_REFUNDABLE_IMG);

        flightBaseFare = (TextView) view.findViewById(R.id.TXT_FARE_BASE);
        flightTaxes = (TextView) view.findViewById(R.id.TXT_FARE_TAXES);
        flightTotal = (TextView) view.findViewById(R.id.TXT_FARE_TOTAL);

        irctcAgentServiceTax = (TextView) view.findViewById(R.id.TXT_IRCTC_AGENT_TAX);
        irctcBookingCharges = (TextView) view.findViewById(R.id.TXT_IRCTC_BOOKING_CHARG);


        btnSubmit = (Button) view.findViewById(R.id.BTN_FARE_QUOTE_SUBMIT);


        // added by asif
        mGdsInterCode = (LinearLayout) view.findViewById(R.id.LAY_GDS_INTER_CODE_FROM_TO);
        mNormalCode = (LinearLayout) view.findViewById(R.id.LAY_NORMAL_CODE_FROM_TO);
        mGdsInternationalCodeFrom = (TextView) view.findViewById(R.id.TXT_STATION_CODE_FROM);
        mGdsInternationalCodeTo = (TextView) view.findViewById(R.id.TXT_STATION_CODE_TO);
        textViewSGST = (TextView) view.findViewById(R.id.textViewSGST);
        textViewCGST = (TextView) view.findViewById(R.id.textViewCGST);
        checkAccept = (CheckBox) view.findViewById(R.id.CHECKBOX_ACCEPT_TERMS);
        final boolean readyToProceed = true;

        SpannableString ss = new SpannableString(getString(R.string.accept_journey_details));
        ClickableSpan clickableSpanUser = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                showTermsCondition(Networking.USER_AGREMENT);
            }
        };

        ClickableSpan clickableSpanAirlineAgreement = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                showTermsCondition(Networking.AIRLINE_AGREMENT);
            }
        };

        String firstToken = "Airline";
        String secondToken = "User Agreement";
        int startPosition1 = ss.toString().indexOf(firstToken);
        int startPosition2 = ss.toString().indexOf(secondToken);

        ss.setSpan(clickableSpanUser, startPosition1, startPosition1 + firstToken.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(clickableSpanAirlineAgreement, startPosition2, startPosition2 + secondToken.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        checkAccept.setText(ss);

        checkAccept.setMovementMethod(LinkMovementMethod.getInstance());

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.lastActiveFragment = Constant.FARE_QUOTE_FRAGMENT;
                if (Pref.getBoolean(getActivity().getApplicationContext())) {
                    Context ctx = getActivity().getApplicationContext();
                    if (readyToProceed) {
                        if (!checkAccept.isChecked()) {
                            new AlertDialogUtil(mainActivity, "Kindly accept the terms & conditions", mainActivity.getResources().getString(R.string.ADD_PASS_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                            return;
                        }

                    }
                    //checkSession();
                    AirDatabase airDatabase = new AirDatabase(getActivity());
                    JWT jwt = new JWT(airDatabase.getAuthToken());
                    Date dateExp = jwt.getExpiresAt();
                    Date dateCurr = new Date();
                    long duration = dateExp.getTime() - dateCurr.getTime();
                    long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
                    if (diffInMinutes <= 0) {
                        Pref.setString(ctx, AppKeys.USER_DETAIL_ADDRESS_1, "");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_ADDRESS_2, "");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_FIRST_NAME, "");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_LAST_NAME, "");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_CITY, "");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_STATE, "");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_COUNTRY, "");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_PIN_CODE, "");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_EMAIL, "");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_MOBILE_NO, "");
                        Pref.setBoolean(ctx, false);
                        SliderMenu.txtUserId.setText("Guest");
                        new AirDatabase(getActivity()).removeAuthToken();
                        AppController.isComingFromFareQuote = true;
                        AppController.routeType = "RoundTripDomestic";
                        ProjectUtil.replaceFragment(mainActivity, new FragmentLogin(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                    } else {
                        ProjectUtil.replaceFragment(mainActivity, new DFragmentAddPassengers(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                    }
                } else {
                    if (!checkAccept.isChecked()) {
                        new AlertDialogUtil(mainActivity, "Kindly accept the terms & conditions", mainActivity.getResources().getString(R.string.ADD_PASS_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                        return;
                    }
                    AppController.isComingFromFareQuote = true;
                    AppController.routeType = "RoundTripDomestic";
                    ProjectUtil.replaceFragment(mainActivity, new FragmentLogin(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                }

            }
        });

    }


    @Override
    public void onResume() {
        super.onResume();
        AppController.getInstance().trackScreenView("Reprice Screen");
        ActivityMain.activeFragment = Constant.FARE_QUOTE_FRAGMENT;

    }


    private void setOnwardData() {
        LstBaggageDetails lstBaggageDetails = new LstBaggageDetails();
        for (int i = 0; i < DSearchResults.selectedDepertureFlight.getLstBaggageDetails().length; i++) {
            if (DSearchResults.selectedDepertureFlight.getLstBaggageDetails()[i].getPaxType().equalsIgnoreCase("ADT")){
                lstBaggageDetails = DSearchResults.selectedDepertureFlight.getLstBaggageDetails()[i];
                break;
            }
        }
        ArrayList<ModelRoutListItem> arrayListOnward = new ArrayList<>();
        for (int i = 0; i < onwardFlightDetails.length; i++) {
            ModelRoutListItem modelRoutListItem = new ModelRoutListItem();
            String flightCode = onwardFlightDetails[i].getAirlineCode() + "-" + onwardFlightDetails[i].getFlightNumber();
            String fromAirportCode = onwardFlightDetails[i].getOrigin();
            String fromAirportName = onwardFlightDetails[i].getOriginCity();
            String fromAirportTerminal = onwardFlightDetails[i].getOriginTerminal();
            String toAirportCode = onwardFlightDetails[i].getDestination();
            String toAirportName = onwardFlightDetails[i].getDestinationCity();
            String toAirportTerminal = onwardFlightDetails[i].getDestinationTerminal();
            String travelDuration = onwardFlightDetails[i].getFlightTime();
            String departureDate = onwardFlightDetails[i].getDepartureDate();
            String departureTime = onwardFlightDetails[i].getDepartureTime();
            String arrivalDate = onwardFlightDetails[i].getArrivalDate();
            String arrivalTime = onwardFlightDetails[i].getArrivalTime();
            String halt = onwardFlightDetails[i].getHalt();
            modelRoutListItem.setFlightCode(flightCode);
            modelRoutListItem.setCarrear(onwardFlightDetails[i].getAirlineCode());
            modelRoutListItem.setFromAirportCode(fromAirportCode);
            modelRoutListItem.setFromAirportName(fromAirportName);
            modelRoutListItem.setServiceProvider(DSearchResults.selectedDepertureFlight.getServiceProvider());
            if (fromAirportTerminal != null) {
                if (!fromAirportTerminal.equals("")) {
                    fromAirportTerminal = fromAirportTerminal;
                }
            }

            modelRoutListItem.setFromAirportTerminal(fromAirportTerminal);
            modelRoutListItem.setToAirportCode(toAirportCode);
            modelRoutListItem.setToAirportName(toAirportName);
            if (toAirportTerminal != null) {
                if (!toAirportTerminal.equals("")) {
                    toAirportTerminal = toAirportTerminal;
                }
            }
            modelRoutListItem.setLstBaggageDetails(lstBaggageDetails);
            modelRoutListItem.setFreeMeal(onwardFlightDetails[i].isFreeMeal());
            modelRoutListItem.setToAirportTerminal(toAirportTerminal);
            modelRoutListItem.setTravelDuration(travelDuration);
            modelRoutListItem.setDepartureDate(departureDate);
            modelRoutListItem.setDepartureTime(departureTime);
            modelRoutListItem.setArrivalDate(arrivalDate);
            modelRoutListItem.setArrivalTime(arrivalTime);
            modelRoutListItem.setHalt(halt);
            arrayListOnward.add(modelRoutListItem);
        }
        mOnwardFlightAdapter = new FareQuoteOnwardFlightInfoAdapter(mainActivity, arrayListOnward);
        listViewOnward.setAdapter(mOnwardFlightAdapter);
        ProjectUtil.updateListViewHeight(listViewOnward);
        if (DSearchResults.selectedDepertureFlight.getLstFareDetails()[0].getBaseType().equals("false")) {
            imgOnwardRefundable.setImageResource(R.drawable.refundable_red);
            mOnwardRefundable.setText("Non Refundable");
        } else if (DSearchResults.selectedDepertureFlight.getLstFareDetails()[0].getBaseType().equals("true")) {
            imgOnwardRefundable.setImageResource(R.drawable.refundable_green);
            mOnwardRefundable.setText("Refundable");
        }
        if (DSearchResults.selectedDepertureFlight.isBaggageAllowed()) {
            ivBag.setVisibility(View.VISIBLE);
            tvOnlyHandBaggage.setVisibility(View.VISIBLE);
        } else {
            ivBag.setVisibility(View.GONE);
            tvOnlyHandBaggage.setVisibility(View.GONE);
        }
    }

    private void setReturnData() {
        LstBaggageDetails lstBaggageDetails = new LstBaggageDetails();
        for (int i = 0; i < DSearchResults.selectedReturnFlight.getLstBaggageDetails().length; i++) {
            if (DSearchResults.selectedReturnFlight.getLstBaggageDetails()[i].getPaxType().equalsIgnoreCase("ADT")){
                lstBaggageDetails = DSearchResults.selectedReturnFlight.getLstBaggageDetails()[i];
                break;
            }
        }
        ArrayList<ModelRoutListItem> arrayListReturn = new ArrayList<>();
        for (int i = 0; i < returnFlightDetails.length; i++) {
            ModelRoutListItem modelRoutListItem = new ModelRoutListItem();
            String flightCode = returnFlightDetails[i].getAirlineCode() + "-" + returnFlightDetails[i].getFlightNumber();
            String fromAirportCode = returnFlightDetails[i].getOrigin();
            String fromAirportName = returnFlightDetails[i].getOriginCity();
            String fromAirportTerminal = returnFlightDetails[i].getOriginTerminal();
            String toAirportCode = returnFlightDetails[i].getDestination();
            String toAirportName = returnFlightDetails[i].getDestinationCity();
            String toAirportTerminal = returnFlightDetails[i].getDestinationTerminal();
            String travelDuration = returnFlightDetails[i].getFlightTime();
            String departureDate = returnFlightDetails[i].getDepartureDate();
            String departureTime = returnFlightDetails[i].getDepartureTime();
            String arrivalDate = returnFlightDetails[i].getArrivalDate();
            String arrivalTime = returnFlightDetails[i].getArrivalTime();
            String halt = returnFlightDetails[i].getHalt();
            modelRoutListItem.setFlightCode(flightCode);
            modelRoutListItem.setCarrear(returnFlightDetails[i].getAirlineCode());
            modelRoutListItem.setFromAirportCode(fromAirportCode);
            modelRoutListItem.setFromAirportName(fromAirportName);
            if (fromAirportTerminal != null) {
                if (!fromAirportTerminal.equals("")) {
                    fromAirportTerminal = fromAirportTerminal;
                }
            }
            modelRoutListItem.setFromAirportTerminal(fromAirportTerminal);
            modelRoutListItem.setToAirportCode(toAirportCode);
            modelRoutListItem.setToAirportName(toAirportName);
            if (toAirportTerminal != null) {
                if (!toAirportTerminal.equals("")) {
                    toAirportTerminal = toAirportTerminal;
                }
            }
            modelRoutListItem.setLstBaggageDetails(lstBaggageDetails);
            modelRoutListItem.setFreeMeal(returnFlightDetails[i].isFreeMeal());
            modelRoutListItem.setServiceProvider(DSearchResults.selectedReturnFlight.getServiceProvider());
            modelRoutListItem.setToAirportTerminal(toAirportTerminal);
            modelRoutListItem.setTravelDuration(travelDuration);
            modelRoutListItem.setDepartureDate(departureDate);
            modelRoutListItem.setDepartureTime(departureTime);
            modelRoutListItem.setArrivalDate(arrivalDate);
            modelRoutListItem.setArrivalTime(arrivalTime);
            modelRoutListItem.setHalt(halt);
            arrayListReturn.add(modelRoutListItem);
        }
        mOnwardFlightAdapter = new FareQuoteOnwardFlightInfoAdapter(mainActivity, arrayListReturn);
        listViewReturn.setAdapter(mOnwardFlightAdapter);
        ProjectUtil.updateListViewHeight(listViewReturn);
        if (DSearchResults.selectedReturnFlight.getLstFareDetails()[0].getBaseType().equals("false")) {
            imgReturnRefundable.setImageResource(R.drawable.refundable_red);
            mReturnRefundable.setText("Non Refundable");
        } else if (DSearchResults.selectedReturnFlight.getLstFareDetails()[0].getBaseType().equals("true")) {
            imgReturnRefundable.setImageResource(R.drawable.refundable_green);
            mReturnRefundable.setText("Refundable");
        }
        if (DSearchResults.selectedReturnFlight.isBaggageAllowed()) {
            ivReturnBag.setVisibility(View.VISIBLE);
            tvOnlyHandBaggageReturn.setVisibility(View.VISIBLE);
        } else {
            ivReturnBag.setVisibility(View.GONE);
            tvOnlyHandBaggageReturn.setVisibility(View.GONE);
        }
        /*if (!DSearchResults.selectedReturnFlight.isFreeMeal()){
            tv_no_free_meel_return.setVisibility(View.VISIBLE);
        }else {
            tv_no_free_meel_return.setVisibility(View.GONE);
        }*/
    }

    private void showTermsCondition(String lStrURL) {

        lObjDialogShowFlightDetails = new Dialog(mainActivity, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
        mainActivity.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        lObjDialogShowFlightDetails.requestWindowFeature(Window.FEATURE_NO_TITLE);
        lObjDialogShowFlightDetails.getWindow().setBackgroundDrawable(new ColorDrawable(Color.LTGRAY));

        // Set layout
        lObjDialogShowFlightDetails.setContentView(R.layout.web_view_terms_condition);

        // initialize the variables
        WebView lObjWebview = (WebView) lObjDialogShowFlightDetails.findViewById(R.id.terms_condition_webView);
        Button btnTerms = (Button) lObjDialogShowFlightDetails.findViewById(R.id.BTN_CANCEL_TERMS);
        progressBar = (ProgressBar) lObjDialogShowFlightDetails.findViewById(R.id.terms_progressBar);
        lObjWebview.getSettings().setJavaScriptEnabled(true);
        lObjWebview.setWebViewClient(new MyWebViewClient());
        lObjWebview.clearHistory();
        lObjWebview.clearCache(true);
        lObjWebview.loadUrl(lStrURL);
        lObjWebview.getSettings().setLoadWithOverviewMode(true);

        btnTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lObjDialogShowFlightDetails.cancel();
            }
        });

        lObjDialogShowFlightDetails.show();
    }

    private class MyWebViewClient extends WebViewClient {

        //      public MyWebViewClient(ProgressBar progressBar) {
        //        progressBar = progressBar;
        //     }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progressBar.setVisibility(View.GONE);
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            progressBar.setVisibility(View.VISIBLE);
            super.onPageStarted(view, url, favicon);
        }
    }
}
