package com.irctc.air.round.trip.domastic.sorting;



import com.irctc.air.round.trip.domastic.model.Flights;

import java.util.Comparator;

/**
 * Created by tourism on 4/13/2016.
 */
public class DSortByFarePrice implements Comparator<Flights>{

    @Override
    public int compare(Flights obj1, Flights obj2) {

        int farePriceOne = Integer.parseInt(obj1.getPrice());
        int farePriceTwo = Integer.parseInt(obj2.getPrice());

        if (farePriceOne > farePriceTwo) {
            return 1;
        } else if (farePriceOne < farePriceTwo) {
            return -1;
        } else {
            return 0;
        }
    }

}
