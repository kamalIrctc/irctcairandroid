package com.irctc.air;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.irctc.air.Database.AirDatabase;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.networking.Networking;

import org.json.JSONObject;

/**
 * Created by QCS2015 on 07-01-2018.
 */

public class SendMailService extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Bundle extras = intent.getExtras();
        if (extras == null) Log.d("Service", "null");
        else {
            Log.d("Service", "not null");
            final String from = (String) extras.get("txnId");
            if (!from.equals("")) {
                final AirDatabase airDatabase = new AirDatabase(ActivityMain.context);
                String authToken = airDatabase.getAuthToken();
                Networking.sendMail(authToken, from, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Mail Send to:",from);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Mail Failed for ",from);
                    }
                });
            }
        }
        return START_STICKY;
    }
}