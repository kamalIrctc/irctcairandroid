package com.irctc.air.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.fragment.AboutUsFragment;
import com.irctc.air.fragment.FragmentContactUs;
import com.irctc.air.fragment.GenricWebviewFragment;
import com.irctc.air.networking.Networking;
import com.irctc.air.util.AppLogger;
import com.irctc.air.util.Constant;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.Pref;
import com.irctc.air.util.ProjectUtil;
import com.irctc.air.Database.SharedPrefrenceAir;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by vivek on 04/13/2016
 */
public class NavDrawerExpandableAdapter extends BaseExpandableListAdapter {


    DrawerLayout lObjDrawerLayout;
    private Context _context;
    private ArrayList<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, ArrayList<String>> _listDataChild;
    private ActivityMain mainActivity;

    public NavDrawerExpandableAdapter(Context context, ArrayList<String> listDataHeader,
                                      HashMap<String, ArrayList<String>> listChildData, DrawerLayout lObjDrawerLayout) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        this.lObjDrawerLayout = lObjDrawerLayout;
        this.mainActivity = (ActivityMain)context;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.slider_list_row_child, null);


            // Get dynamic phone height
            final DisplayMetrics displayMetrics = _context.getResources().getDisplayMetrics();
            final float screenWidthInDp = displayMetrics.widthPixels/displayMetrics.density;
            final float screenHeightInDp = displayMetrics.heightPixels/displayMetrics.scaledDensity;

            int height = Math.round(screenHeightInDp);

            if(height > 700){
                convertView.setMinimumHeight(125);
            }else
            {
                convertView.setMinimumHeight(height / 7);
            }

            AppLogger.enable();
            AppLogger.e("screenWidthInDp : ", screenWidthInDp + "");
            AppLogger.e("screenHeightInDpi : ",screenHeightInDp+"");
            AppLogger.e("Group height : ", (height / 7) + "");

        }

        TextView txtListChild = (TextView) convertView.findViewById(R.id.TXT_SLIDER);
        ImageView ivIcon = (ImageView) convertView.findViewById(R.id.IMG_ICON);

        txtListChild.setText(childText);
        if (childText.equals(_context.getString(R.string.SUB_SIDE_MENU_ONE))) {
            ivIcon.setImageResource(R.drawable.aboutus);
        } else if (childText.equals(_context.getString(R.string.SUB_SIDE_MENU_TWO))) {
            ivIcon.setImageResource(R.drawable.faq);
        } else if (childText.equals(_context.getString(R.string.SUB_SIDE_MENU_THREE))) {
            ivIcon.setImageResource(R.drawable.tnc);
        } else if (childText.equals(_context.getString(R.string.SUB_SIDE_MENU_FOUR))) {
            ivIcon.setImageResource(R.drawable.airline_tnc);
        } else if (childText.equals(_context.getString(R.string.SUB_SIDE_MENU_FIVE))) {
            ivIcon.setImageResource(R.drawable.contactus);
        } else if (childText.equals(_context.getString(R.string.SUB_SIDE_MENU_SIX))) {
            ivIcon.setImageResource(R.drawable.home);
        }


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lObjDrawerLayout.closeDrawers();
                Fragment fragment = null;
                if (childText.equals(_context.getString(R.string.SUB_SIDE_MENU_ONE))) {
                    fragment = new AboutUsFragment();
                } else if (childText.equals(_context.getString(R.string.SUB_SIDE_MENU_TWO))) {

                    fragment = new GenricWebviewFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("TITLE", "FAQ");
                    bundle.putString("URL", Networking.FAQ);
                    fragment.setArguments(bundle);

                } else if (childText.equals(_context.getString(R.string.SUB_SIDE_MENU_THREE))) {

                    fragment = new GenricWebviewFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("TITLE", "Terms and Conditions");
                    bundle.putString("URL", Networking.USER_AGREMENT);
                    fragment.setArguments(bundle);

                } else if (childText.equals(_context.getString(R.string.SUB_SIDE_MENU_FOUR))) {

                    fragment = new GenricWebviewFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("TITLE", "Airlines T&C");
                    bundle.putString("URL",Networking.AIRLINE_AGREMENT);
                    fragment.setArguments(bundle);

                } else if (childText.equals(_context.getString(R.string.SUB_SIDE_MENU_FIVE))) {
                    fragment = new FragmentContactUs();
                } else if (childText.equals(_context.getString(R.string.SUB_SIDE_MENU_SIX))) {

//
//                    Uri marketUri = Uri.parse("market://details?id=com.irctc.air");
//
//                    Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
//
//                    _context.startActivity(marketIntent);
                }

                if (fragment != null) {
                    mainActivity.lastActiveFragment = Constant.PLANNER_FRAGMENT;
                    ProjectUtil.replaceFragment(_context, fragment, R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

                }
            }
        });

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(final int groupPosition, final boolean isExpanded,View convertView, ViewGroup parent) {
        final String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.slider_list_row_group, null);

            // Get dynamic phone height
            final DisplayMetrics displayMetrics = _context.getResources().getDisplayMetrics();
            final float screenWidthInDp = displayMetrics.widthPixels/displayMetrics.density;
            final float screenHeightInDp = displayMetrics.heightPixels/displayMetrics.scaledDensity;
            int height = Math.round(screenHeightInDp);

            if(height > 700){
                convertView.setMinimumHeight(160);
            }else
            {
                convertView.setMinimumHeight(height / 6);
            }


        }

        TextView lblListHeader = (TextView) convertView.findViewById(R.id.TXT_SLIDER);
        ImageView ivIcon = (ImageView) convertView.findViewById(R.id.IMG_ICON);
        ImageView iv_new = (ImageView) convertView.findViewById(R.id.iv_new);
        //final ImageView ivPlusIcon = (ImageView) convertView.findViewById(R.id.IMG_ICON_DETAILS);
        lblListHeader.setText(headerTitle);
        iv_new.setVisibility(View.GONE);


        if (headerTitle.equals(_context.getString(R.string.SIDE_MENU_ONE))) {
            //ivPlusIcon.setVisibility(View.INVISIBLE);
            ivIcon.setImageResource(R.drawable.home);
        } else if (headerTitle.equals(_context.getString(R.string.SIDE_MENU_TWO))) { //9 nov my_order

           // ivPlusIcon.setVisibility(View.INVISIBLE);
            ivIcon.setImageResource(R.drawable.ltc);
        } else if (headerTitle.equals(_context.getString(R.string.SIDE_MENU_THREE))) {

           // ivPlusIcon.setVisibility(View.INVISIBLE);
            ivIcon.setImageResource(R.drawable.adult);
        }else if (headerTitle.equals(_context.getString(R.string.SIDE_MENU_FIVE))) {

            //ivPlusIcon.setVisibility(View.INVISIBLE);
            ivIcon.setImageResource(R.drawable.bookinghistory);
        }else if (headerTitle.equals(_context.getString(R.string.SIDE_MENU_MORE))) {
            ivIcon.setImageResource(R.drawable.menu);
        }else if (headerTitle.equals("Cancel Old Tickets")) {
            ivIcon.setImageResource(R.drawable.bookinghistory);
        }else if (headerTitle.equals(_context.getString(R.string.SIDE_MENU_SIX))) {

            //ivPlusIcon.setVisibility(View.INVISIBLE);
            ivIcon.setImageResource(R.drawable.login_side_icon);
            if(Pref.getBoolean(_context)){
                lblListHeader.setText("Logout");
            }else {
                lblListHeader.setText("Login");
            }
        }else  if(headerTitle.equals(_context.getString(R.string.SIDE_MENU_EIGHT))){
            ivIcon.setImageResource(R.drawable.taxi_64);
            iv_new.setVisibility(View.VISIBLE);
        }

//        else if (headerTitle.equals(_context.getString(R.string.SIDE_MENU_SEVEN))) {
//
//            //ivPlusIcon.setVisibility(View.VISIBLE);
//            ivIcon.setImageResource(R.drawable.tour_menu);
//        }

//        if (isExpanded) {
//            ivPlusIcon.setImageResource(R.drawable.dummy);
//        } else {
//            ivPlusIcon.setImageResource(R.drawable.dummy);
//        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
