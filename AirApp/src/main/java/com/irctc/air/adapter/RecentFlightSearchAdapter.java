package com.irctc.air.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.model.RecentFlightSearchBean;
import com.irctc.air.model.recent_search.ModelRecentSearch;

import java.util.ArrayList;

/**
 * Created by vivek on 4/22/2016.
 */
public class RecentFlightSearchAdapter extends BaseAdapter{


    private Context context;
    private ArrayList<ModelRecentSearch> list;
    private LayoutInflater inflater;
    private ActivityMain mActivity;

    public RecentFlightSearchAdapter(Context context, ArrayList<ModelRecentSearch> list) {

        this.context = context;
        this.list = list;
        this.mActivity = (ActivityMain)context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        Log.e("Rec pass list size :", list.size() + "");
        return list.size();
    }

    @Override
    public Object getItem(int position) {

        return list.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(final int position, View v, ViewGroup parent) {

        ViewHolder holder;
        if (v == null) {
            v = inflater.inflate(R.layout.recent_search_lv_item_layout, null);
            holder = new ViewHolder();
            holder.tvFromStation = (TextView) v.findViewById(R.id.REC_FROM_STATION);
            holder.tvToStation = (TextView) v.findViewById(R.id.REC_TO_STATION);
            holder.tvDepDate = (TextView) v.findViewById(R.id.REC_DEPARTURE_DATE);
            holder.tvReturnDate = (TextView) v.findViewById(R.id.REC_RETUREN_DATE);
            holder.tvTravellerCount = (TextView) v.findViewById(R.id.REC_TRAVELLER_COUNT);
            holder.imgArrow = (ImageView) v.findViewById(R.id.IMG_STATION_CHAMGE_ARROW);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        if (list.get(position) != null) {
            holder.tvFromStation.setText(list.get(position).getStationCodeFrom());
            holder.tvToStation.setText(list.get(position).getStationCodeTo());
            holder.tvDepDate.setText(list.get(position).getDepertureDay()+" "+list.get(position).getDepertureMonthName());

            if(list.get(position).getTripType().equalsIgnoreCase("o")){
                holder.tvReturnDate.setVisibility(View.GONE);
                holder.imgArrow.setImageResource(R.drawable.onwards_arrow48);
            }else{
                holder.imgArrow.setImageResource(R.drawable.returnarrow48);
                holder.tvReturnDate.setVisibility(View.VISIBLE);
                holder.tvReturnDate.setText(list.get(position).getReturnDay()+" "+list.get(position).getReturnMonthName());
            }

            int lIntPassAdultCount = Integer.parseInt(list.get(position).getPassAdultCount());
            int lIntPassInfantCount = Integer.parseInt(list.get(position).getPassInfantCount());
            int lIntPassChildCount = Integer.parseInt(list.get(position).getPassChildCount());

//          int totalPass = lIntPassAdultCount + lIntPassInfantCount + lIntPassChildCount;
            String traveller = "";
            if(lIntPassAdultCount > 0) {
                if(lIntPassAdultCount > 1){

                    traveller += lIntPassAdultCount + " Adult(s)  ";
                }else{
                    traveller += lIntPassAdultCount + " Adult  ";
                }

               // traveller += lIntPassAdultCount + " Adult  ";
            }

            if(lIntPassChildCount > 0) {

                if(lIntPassChildCount > 1){

                    traveller += lIntPassChildCount + " Children  ";
                }else{
                    traveller += lIntPassChildCount + " Child  ";
                }

               // traveller += lIntPassChildCount + " Child   ";
            }

            if(lIntPassInfantCount > 0) {

                if(lIntPassInfantCount > 1){

                    traveller += lIntPassInfantCount + " Infant(s)  ";
                }else{
                    traveller += lIntPassInfantCount + " Infant  ";
                }

               // traveller += lIntPassInfantCount + " Infant";
            }

            holder.tvTravellerCount.setText(traveller);

        }
        return v;
    }

    class ViewHolder {
        private TextView tvFromStation;
        private TextView tvToStation;
        private TextView tvDepDate;
        private TextView tvReturnDate;
        private TextView tvTravellerCount;
        private ImageView imgArrow;

    }
}