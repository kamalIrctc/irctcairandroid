package com.irctc.air.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.irctc.air.R;
import com.irctc.air.util.AppLogger;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;


public class CalendarAdapterReturn extends BaseAdapter
{
	private Context mContext;

	private Calendar monthRet;
	private GregorianCalendar pmonthRet; // calendar instance for previous monthRet
	/**
	 * calendar instance for previous monthRet for getting complete view
	 */
	private GregorianCalendar pmonthmaxsetRet;
	private GregorianCalendar selectedDateRet;
	int firstDay;
	int maxWeeknumber;
	int maxP;
	int calMaxP;
	int lastWeekDay;
	int leftDays;
	int mnthlength;
	Date date1, date2, date3;
	String itemvalue, curentDateString;
	DateFormat df;

	private ArrayList<String> items;
	public static List<String> dayString;
	private View previousView;





	public CalendarAdapterReturn(Context c, GregorianCalendar monthCalendar)
	{
		CalendarAdapterReturn.dayString = new ArrayList<String>();
		Locale.setDefault(Locale.US);
		monthRet = monthCalendar;
		selectedDateRet = (GregorianCalendar) monthCalendar.clone();
		mContext = c;
		monthRet.set(GregorianCalendar.DAY_OF_MONTH, 1);
		this.items = new ArrayList<String>();
		df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
		curentDateString = df.format(selectedDateRet.getTime());
		refreshDays();
	}





	public void setItems ( ArrayList<String> items )
	{
		for (int i = 0; i != items.size(); i++)
		{
			if (items.get(i).length() == 1)
			{
				items.set(i, "0" + items.get(i));
			}
		}
		this.items = items;
	}





	public int getCount ()
	{
		return dayString.size();
	}





	public Object getItem ( int position )
	{
		return dayString.get(position);
	}





	public long getItemId ( int position )
	{
		return 0;
	}





	// create a new view for each item referenced by the Adapter
	public View getView ( int position, View convertView, ViewGroup parent )
	{
		View v = convertView;
		TextView dayView;
		if (convertView == null)
		{ // if it's not recycled, initialize some
			// attributes
			LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.calendar_item, null);

		}
		dayView = (TextView) v.findViewById(R.id.date);
		// separates daystring into parts.
		String[] separatedTime = dayString.get(position).split("-");
		// taking last part of date. ie; 2 from 2012-12-02
		String gridvalue = separatedTime[2].replaceFirst("^0*", "");

		try
		{
			date1 = df.parse(dayString.get(position));
			date2 = df.parse(curentDateString);

		}
		catch (ParseException e)
		{
			e.printStackTrace();
		}
		// checking whether the day is in current monthRet or not.
		if ((Integer.parseInt(gridvalue) > 1) && (position < firstDay))
		{
			// setting offdays to white color.
			dayView.setTextColor(Color.WHITE);
			dayView.setClickable(false);
			dayView.setFocusable(false);
			// Avishek Kumar: Cal issue fix reported by Varun. now only current months date will be display.
			dayView.setVisibility(TextView.INVISIBLE);
			// Avishek Kumar: Cal issue fix reported by Varun. now only current months date will be display.
		}
		else
			if ((Integer.parseInt(gridvalue) < 7) && (position > 28))
			{
				dayView.setTextColor(Color.WHITE);
				dayView.setClickable(false);
				dayView.setFocusable(false);
				// Avishek Kumar: Cal issue fix reported by Varun. now only current months date will be display.
				dayView.setVisibility(TextView.INVISIBLE);
				// Avishek Kumar: Cal issue fix reported by Varun. now only current months date will be display.
			}
			else
			{
				dayView.setVisibility(TextView.VISIBLE);
				dayView.setTextColor(Color.WHITE);
			}

		if (date1.compareTo(date2) < 0)
		{
			// setting curent monthRet's days in blue color.
			dayView.setTextColor(Color.DKGRAY);
			dayView.setClickable(false);
			dayView.setFocusable(false);
		}

		if (date1.compareTo((maxDate())) == 0 || date1.compareTo(maxDate()) > 0)
		{
			// setting curent monthRet's days in blue color.

			dayView.setTextColor(Color.DKGRAY);
			dayView.setClickable(false);
			dayView.setFocusable(false);
		}

		if (dayString.get(position).equals(curentDateString))
		{
			setSelected(v);
			previousView = v;
		}
		else
		{
			v.setBackgroundResource(R.drawable.list_item_background);
		}
		dayView.setText(gridvalue);

		// create date string for comparison
		String date = dayString.get(position);

		if (date.length() == 1)
		{
			date = "0" + date;
		}
		String monthStr = "" + (monthRet.get(GregorianCalendar.MONTH) + 1);
		if (monthStr.length() == 1)
		{
			monthStr = "0" + monthStr;
		}

		// show icon if date is not empty and it exists in the items array
		//		ImageView iw = (ImageView) v.findViewById(R.id.date_icon);
		//		if (date.length() > 0 && items != null && items.contains(date)) {
		//			iw.setVisibility(View.VISIBLE);
		//		} else {
		//			iw.setVisibility(View.INVISIBLE);
		//		}
		return v;
	}





	public View setSelected ( View view )
	{
		if (previousView != null)
		{
			previousView.setBackgroundResource(R.drawable.list_item_background);
		}
		previousView = view;
		view.setBackgroundResource(R.drawable.calendar_cel_select_pnk);
		return view;
	}





	public void refreshDays ()
	{
		try {
			// clear items
			items.clear();
			dayString.clear();
			Locale.setDefault(Locale.US);
			pmonthRet = (GregorianCalendar) monthRet.clone();
			Log.e("pmonthRet ",pmonthRet+"");
			// monthRet start day. ie; sun, mon, etc
			firstDay = monthRet.get(GregorianCalendar.DAY_OF_WEEK);
			Log.e("firstDay ",firstDay+"");
			// finding number of weeks in current monthRet.
			maxWeeknumber = monthRet.getActualMaximum(GregorianCalendar.WEEK_OF_MONTH);
			Log.e("maxWeeknumber ",maxWeeknumber+"");
			// allocating maximum row number for the gridview.
			mnthlength = maxWeeknumber * 7;
			Log.e("mnthlength ",mnthlength+"");
			maxP = getMaxP(); // previous monthRet maximum day 31,30....
			Log.e("maxP ",maxP+"");


			if(firstDay == 1) {
				calMaxP = maxP - (firstDay);// calendar offday starting 24,25 ...
			}else{
				calMaxP = maxP - (firstDay - 1);// calendar offday starting 24,25 ...
			}




			Log.e("calMaxP ",calMaxP+"");
			/**
			 * Calendar instance for getting a complete gridview including the three
			 * monthRet's (previous,current,next) dates.
			 */
			pmonthmaxsetRet = (GregorianCalendar) pmonthRet.clone();
			Log.e("pmonthmaxsetRet ",pmonthmaxsetRet+"");
			/**
			 * setting the start date as previous monthRet's required date.
			 */

			/**
			 * Date issue
			 * Oct 14 2016
			 */
//			if((calMaxP + 1) == 32 /*|| (calMaxP + 1) == 31*/) {
//				pmonthmaxsetRet.set(GregorianCalendar.DAY_OF_MONTH, calMaxP);
//			}else{
//			pmonthmaxsetRet.set(GregorianCalendar.DAY_OF_MONTH, calMaxP + 1);
//			}


			if(/*(calMaxP +1) == 32 || */(calMaxP + 1) == 31) {
				calMaxP += 1;

				pmonthmaxsetRet.set(GregorianCalendar.DAY_OF_MONTH, calMaxP + 1);

			}else
				pmonthmaxsetRet.set(GregorianCalendar.DAY_OF_MONTH, calMaxP + 1);













			Log.e("calMaxP ",calMaxP+"");
			/**
			 * filling calendar gridview.
			 */
			Log.e(".................... ",".................");


			for (int n = 0; n < mnthlength; n++) {
				itemvalue = df.format(pmonthmaxsetRet.getTime());
				pmonthmaxsetRet.add(GregorianCalendar.DATE, 1);
				dayString.add(itemvalue);
			}
		}catch (Exception e){
			AppLogger.enable();
			AppLogger.e("refreshDays ", e.getLocalizedMessage());
		}
	}

	private int getMaxP ()
	{
		int maxP;

		if (monthRet.get(GregorianCalendar.MONTH) == monthRet.getActualMinimum(GregorianCalendar.MONTH))
		{
			pmonthRet.set((monthRet.get(GregorianCalendar.YEAR) - 1), monthRet.getActualMaximum(GregorianCalendar.MONTH), 1);
		}
		else
		{
			pmonthRet.set(GregorianCalendar.MONTH, monthRet.get(GregorianCalendar.MONTH) - 1);
		}
		maxP = pmonthRet.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);

		return maxP;
	}



//
//
//	@SuppressLint("SimpleDateFormat")
//	public boolean maximumcalendarlimit ()
//	{
//		boolean result = false;
//
//		SimpleDateFormat sdf = new SimpleDateFormat("-*-MM-dd");
//		Calendar c = Calendar.getInstance();
//		c.setTime(date2);// Now use today date.
//		c.add(Calendar.DATE, 365); // Adding 365 days
//		String output = sdf.format(c.getTime());
//		try
//		{
//			date3 = sdf.parse(output);
//		}
//		catch (ParseException e)
//		{
//			e.printStackTrace();
//		}
//
//		System.out.println(output);
//
//		return result;
//
//	}





	public static Date maxDate ()
	{
		Date lObjToday = new Date();
		Calendar lObjCal = new GregorianCalendar();
		lObjCal.setTime(lObjToday);
		lObjCal.add(Calendar.DAY_OF_MONTH, 365);
		Date lObjDay60 = lObjCal.getTime();

		return lObjDay60;

	}

}