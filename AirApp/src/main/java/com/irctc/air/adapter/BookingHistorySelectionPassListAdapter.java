package com.irctc.air.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.irctc.air.R;
import com.irctc.air.model.PassengerDetailBean;
import com.irctc.air.util.AppLogger;
import com.irctc.air.util.ProjectUtil;

import java.util.ArrayList;

/**
 * Created by vivek on 4/26/2016.
 */
public class BookingHistorySelectionPassListAdapter extends BaseAdapter{

    private Context context;
    ArrayList<PassengerDetailBean> mAlPassData;
    private LayoutInflater inflater;

    public BookingHistorySelectionPassListAdapter(Context context, ArrayList<PassengerDetailBean> mAlPassData) {

        this.context = context;
        this.mAlPassData = mAlPassData;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mAlPassData.size();
    }

    @Override
    public Object getItem(int position) {

        return mAlPassData.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(final int position, View v, ViewGroup parent) {

        final ViewHolder holder;
        if (v == null) {
            v = inflater.inflate(R.layout.passenger_select_list_item, null);
            holder = new ViewHolder();
            holder.chkPassSelect = (CheckBox) v.findViewById(R.id.CHK_PASS);
            holder.txtCancel = (TextView) v.findViewById(R.id.TXT_CANCEL);
            holder.layDOB = (LinearLayout) v.findViewById(R.id.LAY_DOB);
            holder.txtPassDob = (TextView) v.findViewById(R.id.TV_DOB);
            holder.txtPassName = (TextView) v.findViewById(R.id.TV_PASS_NAME);
            holder.imgPassIcon = (ImageView) v.findViewById(R.id.IMG_PASS_ICON);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }


        if (mAlPassData.get(position) != null) {

            if(mAlPassData.get(position).getPasgtype().contains("Adult")){
                holder.imgPassIcon.setImageResource(R.drawable.adult);

                if(mAlPassData.get(position).getAge().equalsIgnoreCase("")){
                    holder.layDOB.setVisibility(View.GONE);

                }else{
                    holder.txtPassDob.setText(mAlPassData.get(position).getAge());
                }

            }else if(mAlPassData.get(position).getPasgtype().contains("Child")){
                holder.imgPassIcon.setImageResource(R.drawable.child);
                holder.txtPassDob.setText(mAlPassData.get(position).getAge());

            }else if(mAlPassData.get(position).getPasgtype().contains("Infant")) {
                holder.imgPassIcon.setImageResource(R.drawable.infant);
                holder.txtPassDob.setVisibility(View.VISIBLE);
                holder.txtPassDob.setText(mAlPassData.get(position).getAge());
            }
            holder.txtPassName.setText(ProjectUtil.capitalFirstLetter(mAlPassData.get(position).getFname()) + " " + ProjectUtil.capitalFirstLetter(mAlPassData.get(position).getLname()));

            holder.chkPassSelect.setChecked(false);

            mAlPassData.get(position).setIsSelected(false);

            AppLogger.enable();
            AppLogger.e("STATUS CODE => ", mAlPassData.get(position).getCanstatus());

            mAlPassData.get(position).getCanstatus();
            if( mAlPassData.get(position).getCanstatus().equalsIgnoreCase("9") || mAlPassData.get(position).getCanstatus().equalsIgnoreCase("14")){
                holder.chkPassSelect.setClickable(false);
                holder.chkPassSelect.setFocusable(false);
                holder.chkPassSelect.setEnabled(false);
             //   holder.chkPassSelect.setHighlightColor(Color.RED);//setBackgroundResource(R.drawable.air_china_ca);
             //   holder.chkPassSelect.setVisibility(View.GONE);
             //  holder.txtCancel.setVisibility(View.VISIBLE);
                Drawable draw = context.getResources().getDrawable(R.drawable.faded_customcheckbox);
                holder.chkPassSelect.setCompoundDrawablePadding(5);
                holder.chkPassSelect.setCompoundDrawablesWithIntrinsicBounds(null, null, draw, null);
            }

            holder.chkPassSelect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (holder.chkPassSelect.isChecked()) {
                        mAlPassData.get(position).setIsSelected(true);
                    } else {
                        mAlPassData.get(position).setIsSelected(false);
                    }
                }
            });

        }

        return v;
    }

    class ViewHolder {
        private CheckBox chkPassSelect;
        private TextView txtCancel;
        private LinearLayout layDOB;
        private TextView txtPassDob;
        private TextView txtPassName;
        private ImageView imgPassIcon;
    }
}