package com.irctc.air.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.fragment.FragmentPlanner;
import com.irctc.air.round.trip.international.fragment.SearchResultsComboDetails;
import com.irctc.air.model.search_result_round_trip.model_combo.ModelFlightsCombo;
import com.irctc.air.util.AppKeys;
import com.irctc.air.util.Pref;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.irctc.air.activity.ActivityMain.context;

/**
 * Created by Rajnikant Kumar on 8/21/2018.
 */

public class AdapterRoundTripInternationalListView extends RecyclerView.Adapter<AdapterRoundTripInternationalListView.MyViewHolder> {
    private ArrayList<ModelFlightsCombo> diffFlightList;
    public static ModelFlightsCombo clickedModel;
    public AdapterRoundTripInternationalListView(ArrayList<ModelFlightsCombo> diffFlightList) {
        this.diffFlightList = diffFlightList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textViewRoundTripInternational,airlineName,price,station_code_from,
                station_code_to,station_code_dep_from,station_code_dep_to,onwardStops,
                returnStops,onwardDuration,returnDuration,
                onwardDepartureTime,onwardArrivalTime,
                returnDepartureTime,returnArrivalTime,moreFlights;
        CircleImageView airlineIcon;
        public MyViewHolder(View view) {
            super(view);
            airlineIcon = (CircleImageView)view.findViewById(R.id.airlineIcon);
            textViewRoundTripInternational = (TextView) view.findViewById(R.id.textViewRoundTripInternational);
            airlineName = (TextView) view.findViewById(R.id.airlineName);
            station_code_from = (TextView) view.findViewById(R.id.station_code_from);
            station_code_to = (TextView) view.findViewById(R.id.station_code_to);
            station_code_dep_from = (TextView) view.findViewById(R.id.station_code_dep_from);
            station_code_dep_to = (TextView) view.findViewById(R.id.station_code_dep_to);
            onwardStops = (TextView) view.findViewById(R.id.onwardStops);
            returnStops = (TextView) view.findViewById(R.id.returnStops);
            onwardDuration = (TextView) view.findViewById(R.id.onwardDuration);
            returnDuration = (TextView) view.findViewById(R.id.returnDuration);
            onwardDepartureTime = (TextView) view.findViewById(R.id.onwardDepartureTime);
            onwardArrivalTime = (TextView) view.findViewById(R.id.onwardArrivalTime);
            returnDepartureTime = (TextView) view.findViewById(R.id.returnDepartureTime);
            returnArrivalTime = (TextView) view.findViewById(R.id.returnArrivalTime);
            moreFlights = (TextView) view.findViewById(R.id.moreFlights);
            price = (TextView) view.findViewById(R.id.price);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickedModel = diffFlightList.get(getAdapterPosition());
                    FragmentPlanner.transaction= ((ActivityMain)v.getContext()).getSupportFragmentManager().beginTransaction();
                    FragmentPlanner.transaction.replace(R.id.frame_layout,new SearchResultsComboDetails());
                    FragmentPlanner.transaction.addToBackStack(null);
                    FragmentPlanner.transaction.commit();
                }
            });
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_round_trip_international, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ModelFlightsCombo modelFlightsCombo = diffFlightList.get(position);
        if(modelFlightsCombo.getOnwardFlightsList().size()>0 && modelFlightsCombo.getReturnFlightsList().size()>0){
        Picasso.with(context).load(Pref.getString(context, AppKeys.FLIGHT_ICON_PATH)+modelFlightsCombo.getOnwardFlightsList().get(0).getCarrier()+".png").into(holder.airlineIcon);
        holder.airlineName.setText(modelFlightsCombo.getOnwardFlightsList().get(0).getCarrierName());
        holder.price.setText(context.getString(R.string.symbol_rs)+modelFlightsCombo.getOnwardFlightsList().get(0).getPrice());
        holder.textViewRoundTripInternational.setText(modelFlightsCombo.getOnwardFlightsList().get(0).getCarrierName());
        holder.station_code_from.setText(modelFlightsCombo.getOnwardFlightsList().get(0).getDepartureCityWithCode());
        holder.station_code_to.setText(modelFlightsCombo.getOnwardFlightsList().get(0).getArrivalCityWithCode());
        holder.station_code_dep_from.setText(modelFlightsCombo.getReturnFlightsList().get(0).getDepartureCityWithCode());
        holder.station_code_dep_to.setText(modelFlightsCombo.getReturnFlightsList().get(0).getArrivalCityWithCode());
        if(modelFlightsCombo.getOnwardFlightsList().get(0).getStops().equals("0")){
            holder.onwardStops.setText("NonStop");
        }else{
            holder.onwardStops.setText(modelFlightsCombo.getOnwardFlightsList().get(0).getStops()+" stops");
        }
        if(modelFlightsCombo.getReturnFlightsList().get(0).getStops().equals("0")){
            holder.returnStops.setText("NonStop");
        }else{
            holder.returnStops.setText(modelFlightsCombo.getReturnFlightsList().get(0).getStops()+" stops");
        }
        holder.onwardDuration.setText(modelFlightsCombo.getReturnFlightsList().get(0).getDuration());
        holder.returnDuration.setText(modelFlightsCombo.getReturnFlightsList().get(0).getDuration());
        holder.onwardDepartureTime.setText(modelFlightsCombo.getOnwardFlightsList().get(0).getDepartureTime());
        holder.onwardArrivalTime.setText(modelFlightsCombo.getOnwardFlightsList().get(0).getArrivalTime());
        holder.returnDepartureTime.setText(modelFlightsCombo.getReturnFlightsList().get(0).getDepartureTime());
        holder.returnArrivalTime.setText(modelFlightsCombo.getReturnFlightsList().get(0).getArrivalTime());
        int totalItems = modelFlightsCombo.getReturnFlightsList().size()+modelFlightsCombo.getOnwardFlightsList().size();
        if(totalItems>2){
        int more = totalItems-2;
        holder.moreFlights.setVisibility(View.VISIBLE);
        holder.moreFlights.setText(more+" more flights");
        }else{
        holder.moreFlights.setVisibility(View.GONE);
        }
        }
    }

    @Override
    public int getItemCount() {
        return diffFlightList.size();
    }
}