package com.irctc.air.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.irctc.air.R;
import com.irctc.air.model.FlightOnWardDetailBean;
import com.irctc.air.model.InnerFlightDetailsBeans;
import com.irctc.air.util.AppLogger;
import com.irctc.air.util.DateUtility;

import java.util.ArrayList;

/**
 * Created by tourism on 4/23/2016.
 */
public class RouRetReturnFlightAdapter extends BaseAdapter {

    ArrayList<FlightOnWardDetailBean> mFlightsClone;
    Context mContext;

    public RouRetReturnFlightAdapter(Context context, ArrayList<FlightOnWardDetailBean> lFlightsClone) {
        mContext = context;
        this.mFlightsClone = lFlightsClone;
        AppLogger.enable();
        AppLogger.e("SIZE ", "" + mFlightsClone.size());
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mFlightsClone.size();

    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        MyHolder holder = null;

        if (row == null) {

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row =  inflater.inflate(R.layout.round_trip_list_item,  null);
            holder = new MyHolder(row);
            row.setTag(holder);
        } else {
            holder = (MyHolder) row.getTag();
        }

        FlightOnWardDetailBean lflightDetails = mFlightsClone.get(position);
        ArrayList<InnerFlightDetailsBeans> flight = lflightDetails.getFlight();
//
//        holder.imgRouRetFlightIcon.setImageResource(lflightDetails.getFlightIcon());
//        holder.txtRouRetFlightNo.setText(lflightDetails.getFlightAirline()+"-"+flight.get(0).getFlightNo());
//        holder.txtRouRetDepartTime.setText(DateUtility.getTimeFromCal(flight.get(0).getFlightDepartureTime()).substring(0, 5));//getTimeFromCal
//        holder.txtRouRetArriveTime.setText(DateUtility.getTimeFromCal(flight.get(0).getFlightArrivalTime()).substring(0, 5));
//        holder.txtRouRetDuration.setText(flight.get(0).getFlightDurationTime());
//        holder.txtRouRetPrice.setText("\u20B9  " + lflightDetails.getFlightFare());
//
//        if(flight.get(0).getFlightStops() == 0){
//
//            holder.txtRouRetStops.setText("non stop");
//        }else{
//            holder.txtRouRetStops.setText(""+(flight.size()-1));
//        }

        holder.imgRouRetFlightIcon.setImageResource(lflightDetails.getFlightIcon());
        holder.txtRouRetFlightNo.setText(lflightDetails.getFlightAirline()+"-"+flight.get(0).getFlightNo());
        holder.txtRouRetDepartTime.setText(DateUtility.getTimeFromCal(lflightDetails.getFlightOnwDepartureTime()));//getTimeFromCal
        holder.txtRouRetArriveTime.setText(DateUtility.getTimeFromCal(lflightDetails.getFlightOnwArrivalTime()));
        holder.txtRouRetDuration.setText(lflightDetails.getFlightOnwardTotalDuration());
        holder.txtRouRetPrice.setText("\u20B9  " + lflightDetails.getFlightFare());


        if(lflightDetails.getFlightOnwardTotalStops() == 0){

            holder.txtRouRetStops.setText("non stop");
        }else{
            holder.txtRouRetStops.setText(""+lflightDetails.getFlightOnwardTotalStops());
        }

        return row;
    }

    class MyHolder {

        ImageView imgRouRetFlightIcon;
        TextView txtRouRetFlightNo;
        TextView txtRouRetDepartTime;
        TextView txtRouRetArriveTime;
        TextView txtRouRetDuration;
        TextView txtRouRetStops;
        TextView txtRouRetPrice;

        public MyHolder(View itemView) {

            imgRouRetFlightIcon = (ImageView) itemView.findViewById(R.id.IMG_ROUND_FLIGHT_ICON);
            txtRouRetFlightNo = (TextView) itemView.findViewById(R.id.TXT_ROUND_FLIGHT_NO);
            txtRouRetDepartTime = (TextView) itemView.findViewById(R.id.TXT_ROUND_DEPART_TIME);
            txtRouRetArriveTime = (TextView) itemView.findViewById(R.id.TXT_ROUND_ARRIVE_TIME);
            txtRouRetDuration = (TextView) itemView.findViewById(R.id.TXT_ROUND_DURATION);
            txtRouRetStops = (TextView) itemView.findViewById(R.id.TXT_ROUND_STOPS);
            txtRouRetPrice = (TextView) itemView.findViewById(R.id.TXT_ROUND_PRICE);
        }
    }
}