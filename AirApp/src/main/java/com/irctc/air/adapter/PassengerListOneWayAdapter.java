package com.irctc.air.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.irctc.air.R;
import com.irctc.air.model.passenger_list.ModelPassengerListItem;
import com.irctc.air.model.route_list.ModelRoutListItem;

import java.util.ArrayList;

/**
 * Created by Rajnikant Kumar on 9/21/2018.
 */

public class PassengerListOneWayAdapter  extends BaseAdapter {
    ArrayList<ModelPassengerListItem> passengerList;
    Context context;
    public PassengerListOneWayAdapter(Context context, ArrayList<ModelPassengerListItem> passengerList) {
        this.context = context;
        this.passengerList = passengerList;
    }

    @Override
    public int getCount() {
        return passengerList.size();
    }

    @Override
    public Object getItem(int arg0) {
        return passengerList.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        PassengerListOneWayAdapter.MyHolder holder = null;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row =  inflater.inflate(R.layout.passenger_list_item,  null);
            holder = new PassengerListOneWayAdapter.MyHolder(row);
            row.setTag(holder);
        } else {
            holder = (PassengerListOneWayAdapter.MyHolder) row.getTag();
        }
/*        if(passengerList.get(position).getGender().equals("0")){
            if(passengerList.get(position).getType().equals("0")){
                holder.fullName.setText("Mr. "+passengerList.get(position).getFirstName()+" "+passengerList.get(position).getLastName());
                holder.userIcon.setImageResource(R.drawable.adult);
            }else if(passengerList.get(position).getType().equals("1")){
                holder.fullName.setText("Master "+passengerList.get(position).getFirstName()+" "+passengerList.get(position).getLastName());
                holder.userIcon.setImageResource(R.drawable.child);
            }else if(passengerList.get(position).getType().equals("2")){
                holder.fullName.setText("Master "+passengerList.get(position).getFirstName()+" "+passengerList.get(position).getLastName());
                holder.userIcon.setImageResource(R.drawable.infant);
            }
        }else if(passengerList.get(position).getGender().equals("1")){
            if(passengerList.get(position).getType().equals("0")) {
                holder.fullName.setText("Mrs. "+passengerList.get(position).getFirstName()+" "+passengerList.get(position).getLastName());
                holder.userIcon.setImageResource(R.drawable.adult);
            }else if(passengerList.get(position).getType().equals("1")){
                holder.fullName.setText("Ms. "+passengerList.get(position).getFirstName()+" "+passengerList.get(position).getLastName());
                holder.userIcon.setImageResource(R.drawable.child);
            }else if(passengerList.get(position).getType().equals("2")){
                holder.userIcon.setImageResource(R.drawable.infant);
            }
        }else if(passengerList.get(position).getGender().equals("2")){
            holder.fullName.setText("Ms. "+passengerList.get(position).getFirstName()+" "+passengerList.get(position).getLastName());
            if(passengerList.get(position).getType().equals("0")) {
                holder.userIcon.setImageResource(R.drawable.adult);
            }else if(passengerList.get(position).getType().equals("1")){
                holder.userIcon.setImageResource(R.drawable.child);
            }else if(passengerList.get(position).getType().equals("2")){
                holder.userIcon.setImageResource(R.drawable.infant);
            }
        }*/

        switch (passengerList.get(position).getType()){
            case "0":
                holder.userIcon.setImageResource(R.drawable.adult);
                switch (passengerList.get(position).getGender()){
                    case "0":
                        holder.fullName.setText("Mr. "+passengerList.get(position).getFirstName()+" "+passengerList.get(position).getLastName());
                        break;
                    case "1":
                        holder.fullName.setText("Mrs. "+passengerList.get(position).getFirstName()+" "+passengerList.get(position).getLastName());
                        break;
                    case "2":
                        holder.fullName.setText("Miss. "+passengerList.get(position).getFirstName()+" "+passengerList.get(position).getLastName());
                        break;
                }
                break;
            case "1":
                holder.userIcon.setImageResource(R.drawable.child);
                switch (passengerList.get(position).getGender()){
                    case "0":
                        holder.fullName.setText("Master. "+passengerList.get(position).getFirstName()+" "+passengerList.get(position).getLastName());
                        break;
                    case "1":
                        holder.fullName.setText("Miss. "+passengerList.get(position).getFirstName()+" "+passengerList.get(position).getLastName());
                        break;
                    case "2":
                        holder.fullName.setText("Miss. "+passengerList.get(position).getFirstName()+" "+passengerList.get(position).getLastName());
                        break;
                }
                break;
            case "2":
                holder.userIcon.setImageResource(R.drawable.infant);
                switch (passengerList.get(position).getGender()){
                    case "0":
                        holder.fullName.setText("Master. "+passengerList.get(position).getFirstName()+" "+passengerList.get(position).getLastName());
                        break;
                    case "1":
                        holder.fullName.setText("Miss. "+passengerList.get(position).getFirstName()+" "+passengerList.get(position).getLastName());
                        break;
                    case "2":
                        holder.fullName.setText("Miss. "+passengerList.get(position).getFirstName()+" "+passengerList.get(position).getLastName());
                        break;
                }
                break;
        }

        holder.status.setText("Status : "+passengerList.get(position).getDeliveryStatus());
      return row;
    }

    class MyHolder {
        TextView fullName;

        TextView flightStartDate;
        TextView flightEndDate;
        TextView flightDuration,status;
        ImageView userIcon;
        public MyHolder(View itemView) {
            fullName = (TextView) itemView.findViewById(R.id.fullName);
            status = (TextView) itemView.findViewById(R.id.status);
            userIcon = (ImageView) itemView.findViewById(R.id.userIcon);
   /*         flightStartDate = (TextView) itemView.findViewById(R.id.TXT_FLIGHT_START_DATE);
            flightEndDate = (TextView) itemView.findViewById(R.id.TXT_END_FLIGHT_END_DATE);
            flightDuration = (TextView) itemView.findViewById(R.id.FLIGHT_DURATION);
    */    }
    }
}
