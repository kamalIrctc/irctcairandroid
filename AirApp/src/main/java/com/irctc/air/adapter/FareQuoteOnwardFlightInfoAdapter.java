package com.irctc.air.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.irctc.air.R;
import com.irctc.air.model.FareQuoteInnerFlightDetailsBeans;
import com.irctc.air.model.route_list.ModelRoutListItem;
import com.irctc.air.round.trip.domastic.model.LstBaggageDetails;
import com.irctc.air.util.AppKeys;
import com.irctc.air.util.AppLogger;
import com.irctc.air.util.DateUtility;
import com.irctc.air.util.MonthStringToMonthName;
import com.irctc.air.util.Pref;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by tourism on 4/30/2016.
 */
public class FareQuoteOnwardFlightInfoAdapter extends BaseAdapter {

    public ArrayList<ModelRoutListItem> routeList;
    Context mContext;

    public FareQuoteOnwardFlightInfoAdapter(Context context, ArrayList<ModelRoutListItem> routeList) {

        mContext = context;
        this.routeList = routeList;

    }

    @Override
    public int getCount() {

        return routeList.size();
    }

    @Override
    public Object getItem(int arg0) {

        return null;
    }

    @Override
    public long getItemId(int arg0) {

        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        MyHolder holder = null;

        if (row == null) {

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row =  inflater.inflate(R.layout.fare_quote_item_layout,  null);
            holder = new MyHolder(row);
            row.setTag(holder);


        } else {
            holder = (MyHolder) row.getTag();
        }


        String startDate =routeList.get(position).getDepartureDate().substring(0,10);
        String startDatePart2 = MonthStringToMonthName.convert(startDate.split("-")[1]);
        String startDatePart1 = startDate.split("-")[2];
        String startDatePart3 = startDate.split("-")[0];
        String endDate =routeList.get(position).getArrivalDate().substring(0,10);
        String endDatePart2 = MonthStringToMonthName.convert(endDate.split("-")[1]);
        String endDatePart1 = endDate.split("-")[2];
        String endDatePart3 = endDate.split("-")[0];
        //holder.flightIcon.setImageResource(mDataset.get(position).getFlightIcon());
        Picasso.with(mContext).load(Pref.getString(mContext, AppKeys.FLIGHT_ICON_PATH)+routeList.get(position).getCarrear()+".png").into(holder.flightIcon);
        holder.flightFromAirCode.setText(routeList.get(position).getFlightCode());
        holder.flightStartDate.setText(startDatePart1+" "+startDatePart2+" "+startDatePart3);
        holder.flightEndDate.setText(endDatePart1+" "+endDatePart2+" "+endDatePart3);
        holder.flightDuration.setText(routeList.get(position).getTravelDuration());
        holder.flightDepartFromCode.setText(routeList.get(position).getFromAirportCode());
        holder.flightDepartTime.setText(routeList.get(position).getDepartureTime());
        holder.flightArriveToCode.setText(routeList.get(position).getToAirportCode());
        holder.flightArriveTime.setText(routeList.get(position).getArrivalTime());
        if(routeList.get(position).getToAirportTerminal()==null) {
            holder.flightArriveCity.setText(routeList.get(position).getToAirportName() + "\n");
        }else{
            holder.flightArriveCity.setText(routeList.get(position).getToAirportName() + "\n" + routeList.get(position).getToAirportTerminal());
        }
        if(routeList.get(position).getFromAirportTerminal()==null) {
            holder.flightDepartCity.setText(routeList.get(position).getFromAirportName() + "\n");
        }else{
            holder.flightDepartCity.setText(routeList.get(position).getFromAirportName() + "\n" + routeList.get(position).getFromAirportTerminal());
        }

        if(position == 0) {
            holder.layoverTime.setVisibility(View.GONE);
            holder.layLayover.setVisibility(View.GONE);
        }else{
            if(routeList.get(position-1).getHalt()!=null){
            String halt = routeList.get(position-1).getHalt().replace("|","\n");
            holder.layoverTime.setVisibility(View.VISIBLE);
            holder.layLayover.setVisibility(View.VISIBLE);
            holder.layoverTime.setText(halt);
            }else{
            holder.layoverTime.setVisibility(View.GONE);
            holder.layLayover.setVisibility(View.GONE);
            }
        }

        if (routeList.get(position).getServiceProvider().equalsIgnoreCase("GDS")){
            if (!routeList.get(position).isFreeMeal()){
                if (position == 0){
                    LstBaggageDetails lstBaggageDetails = routeList.get(position).getLstBaggageDetails();
                    if (lstBaggageDetails !=null){

                        if (lstBaggageDetails.getCheckInValue() !=null && !TextUtils.isEmpty(lstBaggageDetails.getCabinValue())){
                            Log.d("CheckInValue", lstBaggageDetails.getCheckInValue());
                            if (lstBaggageDetails.getIsKg()){
                                holder.tv_no_free_meel.setText("No Free Meal, Baggage "+lstBaggageDetails.getCheckInValue()+" kg");
                            }else {
                                holder.tv_no_free_meel.setText("No Free Meal, Baggage "+lstBaggageDetails.getCheckInValue()+" pcs");
                            }

                        }

                    }
                }
                holder.tv_no_free_meel.setVisibility(View.VISIBLE);
            }else {
                holder.tv_no_free_meel.setVisibility(View.GONE);
            }

            if (routeList.get(position).getServiceProvider().equalsIgnoreCase("GDS") && routeList.get(position).isFreeMeal()){
                holder.tv_no_free_meel.setVisibility(View.VISIBLE);
                holder.tv_no_free_meel.setText("Free Meal");
            }
        }

        /*if (holder.tv_no_free_meel.getVisibility() == View.VISIBLE){
            holder.tv_test.setVisibility(View.VISIBLE);
        }else {
            holder.tv_test.setVisibility(View.GONE);
        }*/

        /*
        if(mDataset.get(position).getDuration().equalsIgnoreCase("")){

            holder.flightDuration.setText( DateUtility.getTimeDifferenceInTwoDate(mDataset.get(position).getArrivaltime(), mDataset.get(position).getDeparturetime()));

        }else{
            holder.flightDuration.setText(mDataset.get(position).getDuration());

        }

        holder.flightDepartFromCode.setText(mDataset.get(position).getDepartureairport());
        holder.flightDepartTime.setText(DateUtility.getTimeFromCal(mDataset.get(position).getDeparturetime()).substring(0, 5));
*/

        /**
         * 28 Oct
         * TERMINAL NAME ADDED
         * Show terminal name using via
         */
  /*      String via[] = mDataset.get(position).getVia().split(" to ");

        String depCity[] = via[0].split(" ");
        StringBuilder depCityName = new StringBuilder();
        depCityName.append(depCity[0]);
        if(depCity.length > 1){
            depCityName.append("\n");
        }

        for (int i = 1; i <depCity.length ; i++) {

            if(i != depCity.length-1){
                depCityName.append(depCity[i] + " ");
            }
            else{
                depCityName.append(depCity[i] );
            }

        }
        AppLogger.enable();
        AppLogger.e("depCityName ", depCityName.toString());

        holder.flightDepartCity.setText(depCityName);

        holder.flightArriveToCode.setText(mDataset.get(position).getArrivalairport());
        holder.flightArriveTime.setText(DateUtility.getTimeFromCal(mDataset.get(position).getArrivaltime()).substring(0, 5));
*/
        /**
         * 28 Oct
         * TERMINAL NAME ADDED
         * Show terminal name using via
         */

  /*      String arrCity[] = via[1].split(" ");
        StringBuilder arrCityName = new StringBuilder();
        arrCityName.append(arrCity[0]);
        if(arrCity.length > 1){
            arrCityName.append("\n");
        }

        for (int i = 1; i <arrCity.length ; i++) {

            if(i != arrCity.length-1){
                arrCityName.append(arrCity[i] + " ");
            }
            else{
                arrCityName.append(arrCity[i] );
            }

        }
        AppLogger.enable();
        AppLogger.e("arrCityName ", arrCityName.toString());

        holder.flightArriveCity.setText(arrCityName);


        if(position == 0) {
            holder.layoverTime.setVisibility(View.GONE);
            holder.layLayover.setVisibility(View.GONE);
        }else{


            if(mDataset.size() > (position) ){
                holder.layoverTime.setVisibility(View.VISIBLE);
                holder.layLayover.setVisibility(View.VISIBLE);

                holder.layoverTime.setText(DateUtility.getTimeDifferenceInTwoDate(mDataset.get(position).getDeparturetime(), mDataset.get(position - 1).getArrivaltime()));
            }

        }
*/
        return row;
    }

    class MyHolder {

        //strip one
        ImageView flightIcon;
        TextView flightFromAirCode;

        TextView flightStartDate;
        TextView flightEndDate;

        TextView flightDuration, tv_no_free_meel, tv_test;

        //strip 2
        TextView flightDepartFromCode;
        TextView flightDepartTime;
        TextView flightDepartCity;

        TextView flightArriveToCode;
        TextView flightArriveTime;
        TextView flightArriveCity;

        // LAYOVER LAYOUT AND TIME
        LinearLayout layLayover;
        TextView layoverTime;


        public MyHolder(View itemView) {

            flightIcon = (ImageView) itemView.findViewById(R.id.FLIGHT_IMAGE);
            flightFromAirCode = (TextView) itemView.findViewById(R.id.FLIGHT_NUMBER);

            flightStartDate = (TextView) itemView.findViewById(R.id.TXT_FLIGHT_START_DATE);
            flightEndDate = (TextView) itemView.findViewById(R.id.TXT_END_FLIGHT_END_DATE);
            tv_no_free_meel = (TextView) itemView.findViewById(R.id.tv_no_free_meel);
//            tv_test = (TextView) itemView.findViewById(R.id.tv_test);
            flightDuration = (TextView) itemView.findViewById(R.id.FLIGHT_DURATION);

            flightDepartFromCode = (TextView) itemView.findViewById(R.id.TXT_START_STN_CODE);
            flightDepartTime = (TextView) itemView.findViewById(R.id.TXT_FROM_STN_FLIGHT_TIME);
            flightDepartCity = (TextView) itemView.findViewById(R.id.TXT_START_STN_CITY_NAME);

            flightArriveToCode = (TextView) itemView.findViewById(R.id.TXT_END_STN_CODE);
            flightArriveTime = (TextView) itemView.findViewById(R.id.TXT_END_STN_FLIGHT_TIME);
            flightArriveCity = (TextView) itemView.findViewById(R.id.TXT_END_STN_CITY_NAME);

            layoverTime = (TextView) itemView.findViewById(R.id.LAYOVER_TIME);
            layLayover = (LinearLayout) itemView.findViewById(R.id.LAYOVER_LAYOUT);

        }
    }
}
