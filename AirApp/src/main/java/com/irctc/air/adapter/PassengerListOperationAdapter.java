package com.irctc.air.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.irctc.air.R;
import com.irctc.air.model.FlightOnWardDetailBean;
import com.irctc.air.model.InnerFlightDetailsBeans;
import com.irctc.air.model.PassengerOperationBean;
import com.irctc.air.util.AppLogger;

import java.util.ArrayList;

/**
 * Created by tourism on 6/14/2016.
 */
public class PassengerListOperationAdapter extends BaseAdapter{

    ArrayList<PassengerOperationBean> mOpeartionBean;
    Context mContext;

    public PassengerListOperationAdapter(Context context, ArrayList<PassengerOperationBean> mOpeartionBean) {
        mContext = context;
        this.mOpeartionBean = mOpeartionBean;
        AppLogger.enable();
        AppLogger.e("SIZE ", "" + mOpeartionBean.size());
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mOpeartionBean.size();

    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        MyHolder holder = null;

        if (row == null) {

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row =  inflater.inflate(R.layout.pass_operation_item_layout,  null);
            holder = new MyHolder(row);
            row.setTag(holder);
        } else {
            holder = (MyHolder) row.getTag();
        }

        PassengerOperationBean operationBean = mOpeartionBean.get(position);

        holder.IMG_OPERATION_NAME.setImageResource(operationBean.getImageId());
        holder.TXT_OPERATION_NAME.setText(operationBean.getOperationName());


        return row;
    }

    class MyHolder {

        ImageView IMG_OPERATION_NAME;
        TextView TXT_OPERATION_NAME;


        public MyHolder(View itemView) {

            IMG_OPERATION_NAME = (ImageView) itemView.findViewById(R.id.IMG_OPERATION_NAME);
            TXT_OPERATION_NAME = (TextView) itemView.findViewById(R.id.TXT_OPERATION_NAME);

        }
    }
}
