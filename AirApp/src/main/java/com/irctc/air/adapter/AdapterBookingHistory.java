package com.irctc.air.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.R;
import com.irctc.air.model.BookingHistoryBean;
import com.irctc.air.model.ModelBookingHistory;
import com.irctc.air.util.DateUtility;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by vivek on 4/22/2016.
 */
public class AdapterBookingHistory extends BaseAdapter{


    private Context context;
    private ArrayList<ModelBookingHistory> list;
    private LayoutInflater inflater;

    public AdapterBookingHistory(Context context, ArrayList<ModelBookingHistory> list) {

        this.context = context;
        this.list = list;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {

        return list.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View row = convertView;
        ViewHolder holder = null;

        if (row == null) {
            row = inflater.inflate(R.layout.booking_history_lv_item_layout, null);
            holder = new ViewHolder(row);
            row.setTag(holder);

        } else {
            holder = (ViewHolder) row.getTag();
        }

        if (list.get(position) != null) {
            ModelBookingHistory booking = list.get(position);
            if(booking.getTripType()!=null){
                if(booking.getTripType().equals("0")){
                    holder.imgArrow.setImageResource(R.drawable.onwards_arrow48);
                }else if(booking.getTripType().equals("3")){
                    holder.imgArrow.setImageResource(R.drawable.returnarrow48);
                }else{
                    holder.imgArrow.setImageResource(R.drawable.returnarrow48);
                }
            }
            holder.tvFrom.setText(booking.getOrigin());
            holder.tvTo.setText( booking.getDestination());
            holder.tvTransactionId.setText(booking.getTransactionId());
            holder.tvDateMonth.setText(booking.getMonth()+" "+booking.getYear());
            holder.tvDateDay.setText(booking.getDate());

            if(booking.getBookingStatus().equals("Delivered")){
                holder.textViewStatus.setBackgroundColor(ContextCompat.getColor(context,android.R.color.holo_green_dark));
/*            }else if(){
                holder.textViewStatus.setBackgroundColor(ContextCompat.getColor(context,android.R.color.holo_red_dark));*/
            }else if(booking.getBookingStatus().equals("Not Booked") || booking.getBookingStatus().equals("for cancellation") || booking.getBookingStatus().equals("Can/Mod")){
                holder.textViewStatus.setBackgroundColor(ContextCompat.getColor(context,android.R.color.holo_red_light));
            }else{
                holder.textViewStatus.setBackgroundColor(ContextCompat.getColor(context,android.R.color.holo_orange_light));
            }
            holder.textViewStatus.setText(booking.getBookingStatus().replace("Booking under process","Under Process"));

    }

        return row;
    }

    private boolean cancelButtonVisibility(int position) {
        long date = System.currentTimeMillis();
        String formatedDate = DateUtility.currentTime(date);
        Date dateToday =  DateUtility.getBeforeAfterTime(formatedDate, 0, 0);
        String departTime = AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlBookedOnwardDetail().get(0).getAlSegmentDetail().get(0).getDepartTime();
        Date dateDapart =  DateUtility.getBeforeAfterTime(departTime, 0, 0);

        if( DateUtility.compareDateTime(dateToday,dateDapart)){
            return  true;
        }
        return false;
    }

    class ViewHolder {

        private TextView tvFrom;
        private TextView tvTo;
        private TextView tvFromTo;
        private TextView tvTransactionId;
        private ImageView imgCancelIcon;
        private ImageView imgArrow;
        private TextView tvDateDay;
        private TextView tvDateMonth,textViewStatus;
        private TextView tvDateYear;

        ViewHolder(View v){

            tvFrom = (TextView) v.findViewById(R.id.TXT_FROM);
            tvTo = (TextView) v.findViewById(R.id.TXT_TO);
            tvFromTo = (TextView) v.findViewById(R.id.FROM_TO);
            tvTransactionId = (TextView) v.findViewById(R.id.TRANSACTION_ID);
            imgCancelIcon = (ImageView) v.findViewById(R.id.CANCEL_ICON);
            tvDateDay = (TextView) v.findViewById(R.id.TXT_DATE_DAY);
            tvDateMonth = (TextView) v.findViewById(R.id.TXT_DATE_MONTH);
            textViewStatus = (TextView) v.findViewById(R.id.textViewStatus);
            //tvDateYear = (TextView) v.findViewById(R.id.TXT_DATE_YEAR);
            imgArrow = (ImageView) v.findViewById(R.id.IMG_ARROW_ICON);

        }
    }
}