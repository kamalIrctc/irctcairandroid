package com.irctc.air.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.irctc.air.R;
import com.irctc.air.model.route_list.ModelRoutListItem;
import com.irctc.air.round.trip.domastic.model.LstBaggageDetails;
import com.irctc.air.util.AppKeys;
import com.irctc.air.util.ListViewInsideScrollView;
import com.irctc.air.util.MonthStringToMonthName;
import com.irctc.air.util.Pref;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.irctc.air.activity.ActivityMain.context;

/**
 * Created by tourism on 4/30/2016.
 */
public class TicketOneWayAdapter extends BaseAdapter {

    public ArrayList<ModelRoutListItem> routeList;
    Context mContext;

    public TicketOneWayAdapter(Context context, ArrayList<ModelRoutListItem> routeList) {

        mContext = context;
        this.routeList = routeList;

    }

    @Override
    public int getCount() {

        return routeList.size();
    }

    @Override
    public Object getItem(int arg0) {

        return null;
    }

    @Override
    public long getItemId(int arg0) {

        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        MyHolder holder = null;

        if (row == null) {

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row =  inflater.inflate(R.layout.item_ticket_segment,  null);
            holder = new MyHolder(row);
            row.setTag(holder);


        } else {
            holder = (MyHolder) row.getTag();
        }
        Picasso.with(context).load(Pref.getString(context, AppKeys.FLIGHT_ICON_PATH)+routeList.get(position).getCarrear()+".png").into(holder.flightIcon);
        holder.flightFromAirCode.setText(routeList.get(position).getFlightCode());
        routeList.get(position).setArrivalDateOnlyAnd(routeList.get(position).getArrivalTimeAnd());
        routeList.get(position).setArrivalTimeOnlyAnd(routeList.get(position).getArrivalTimeAnd());
        routeList.get(position).setDepartureDateOnlyAnd(routeList.get(position).getDepartureTimeAnd());
        routeList.get(position).setDepartureTimeOnlyAnd(routeList.get(position).getDepartureTimeAnd());
        holder.flightDepartTime.setText(routeList.get(position).getDepartureTimeOnlyAnd());
        holder.flightStartDate.setText(routeList.get(position).getDepartureDateOnlyAnd());
        holder.flightEndDate.setText(routeList.get(position).getArrivalDateOnlyAnd());
        holder.flightArriveTime.setText(routeList.get(position).getArrivalTimeOnlyAnd());
        holder.flightDuration.setText(routeList.get(position).getTravelDuration());
        holder.flightDepartFromCode.setText(routeList.get(position).getFromAirportCode());
        holder.flightArriveToCode.setText(routeList.get(position).getToAirportCode());
        if (routeList.get(position).getServiceProvider().equalsIgnoreCase("GDS")){
            if (!routeList.get(position).isFreeMeal()){
                holder.tv_no_free_meel.setVisibility(View.VISIBLE);
            }else {
                holder.tv_no_free_meel.setVisibility(View.GONE);
            }
        /*if (routeList.get(position).getServiceProvider().equalsIgnoreCase("GDS") && routeList.get(position).isFreeMeal()){
            holder.tv_no_free_meel.setVisibility(View.VISIBLE);
            holder.tv_no_free_meel.setText("Free Meal");
        }*/
        }

        if(routeList.get(position).getToAirportTerminal()!=null) {
            holder.flightArriveCity.setText(routeList.get(position).getToAirportName() + "\n"+routeList.get(position).getToAirportTerminal());
        }else{
            holder.flightArriveCity.setText(routeList.get(position).getToAirportName() + "\n");
        }
        if(routeList.get(position).getFromAirportTerminal()!=null) {
            holder.flightDepartCity.setText(routeList.get(position).getFromAirportName() + "\n"+routeList.get(position).getFromAirportTerminal());
        }else{
            holder.flightDepartCity.setText(routeList.get(position).getFromAirportName() + "\n");
        }

        if(position == 0) {
            holder.layoverTime.setVisibility(View.GONE);
            holder.layLayover.setVisibility(View.GONE);
        }else{
            holder.layoverTime.setVisibility(View.GONE);
            holder.layLayover.setVisibility(View.GONE);
        }
            if (!routeList.get(position).isFreeMeal()){
                holder.tv_no_free_meel.setVisibility(View.VISIBLE);

            }else {
                holder.tv_no_free_meel.setVisibility(View.GONE);
            }

        //holder.passengerList.setAdapter(new PassengerListOneWayAdapter(mContext,routeList.get(position).getPassengersList()));
        //ListViewInsideScrollView.open(holder.passengerList);
        return row;
    }

    class MyHolder {

        //strip one
        ImageView flightIcon;
        TextView flightFromAirCode;

        TextView flightStartDate;
        TextView flightEndDate, tv_no_free_meel;

        TextView flightDuration;

        //strip 2
        TextView flightDepartFromCode;
        TextView flightDepartTime;
        TextView flightDepartCity;

        TextView flightArriveToCode;
        TextView flightArriveTime;
        TextView flightArriveCity;

        // LAYOVER LAYOUT AND TIME
        LinearLayout layLayover;
        TextView layoverTime;
        ListView passengerList;


        public MyHolder(View itemView) {

            flightIcon = (ImageView) itemView.findViewById(R.id.FLIGHT_IMAGE);
            flightFromAirCode = (TextView) itemView.findViewById(R.id.FLIGHT_NUMBER);

            flightStartDate = (TextView) itemView.findViewById(R.id.TXT_FLIGHT_START_DATE);
            flightEndDate = (TextView) itemView.findViewById(R.id.TXT_END_FLIGHT_END_DATE);
            tv_no_free_meel = (TextView) itemView.findViewById(R.id.tv_no_free_meel);
            flightDuration = (TextView) itemView.findViewById(R.id.FLIGHT_DURATION);

            flightDepartFromCode = (TextView) itemView.findViewById(R.id.TXT_START_STN_CODE);
            flightDepartTime = (TextView) itemView.findViewById(R.id.TXT_FROM_STN_FLIGHT_TIME);
            flightDepartCity = (TextView) itemView.findViewById(R.id.TXT_START_STN_CITY_NAME);

            flightArriveToCode = (TextView) itemView.findViewById(R.id.TXT_END_STN_CODE);
            flightArriveTime = (TextView) itemView.findViewById(R.id.TXT_END_STN_FLIGHT_TIME);
            flightArriveCity = (TextView) itemView.findViewById(R.id.TXT_END_STN_CITY_NAME);

            layoverTime = (TextView) itemView.findViewById(R.id.LAYOVER_TIME);
            layLayover = (LinearLayout) itemView.findViewById(R.id.LAYOVER_LAYOUT);
            passengerList = (ListView) itemView.findViewById(R.id.passengerList);

        }
    }
}
