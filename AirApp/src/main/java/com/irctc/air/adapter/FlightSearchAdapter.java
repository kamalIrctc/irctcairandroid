package com.irctc.air.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.irctc.air.R;
import com.irctc.air.model.FlightOnWardDetailBean;
import com.irctc.air.model.InnerFlightDetailsBeans;
import com.irctc.air.util.AppLogger;
import com.irctc.air.util.DateUtility;

import java.util.ArrayList;

/**
 * Created by tourism on 4/23/2016.
 */
public class FlightSearchAdapter extends BaseAdapter {

    ArrayList<FlightOnWardDetailBean> mFlightsClone;
    Context mContext;

    public FlightSearchAdapter(Context context, ArrayList<FlightOnWardDetailBean> lFlightsClone) {
        mContext = context;
        this.mFlightsClone = lFlightsClone;
        AppLogger.enable();
        AppLogger.e("SIZE ", "" + mFlightsClone.size());
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mFlightsClone.size();

    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        MyHolder holder = null;

        if (row == null) {

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row =  inflater.inflate(R.layout.fragment_oneway_list_item,  null);
            holder = new MyHolder(row);
            row.setTag(holder);
        } else {
            holder = (MyHolder) row.getTag();
        }

        FlightOnWardDetailBean lflightDetails = mFlightsClone.get(position);
        ArrayList<InnerFlightDetailsBeans> flight = lflightDetails.getFlight();


//        holder.flightIcon.setImageResource(flight.get(0).getFlightIcon());//lflightDetails.getFlightIcon());
//        holder.txtFlightNo.setText(lflightDetails.getFlightAirline()+"-"+flight.get(0).getFlightNo());
//        holder.txtDepart.setText(DateUtility.getTimeFromCal(flight.get(0).getFlightDepartureTime()));//getTimeFromCal
//        holder.txtArrive.setText(DateUtility.getTimeFromCal(flight.get(0).getFlightArrivalTime()));
//        holder.txtDuration.setText(flight.get(0).getFlightDurationTime());
//        holder.txtFlightPrice.setText("\u20B9 " + lflightDetails.getFlightFare());


        holder.flightIcon.setImageResource(flight.get(0).getFlightIcon());//lflightDetails.getFlightIcon());
        holder.txtFlightNo.setText(lflightDetails.getFlightAirline()+"-"+flight.get(0).getFlightNo());
        holder.txtDepart.setText(DateUtility.getTimeFromCal(lflightDetails.getFlightOnwDepartureTime()));//getTimeFromCal
        holder.txtArrive.setText(DateUtility.getTimeFromCal(lflightDetails.getFlightOnwArrivalTime()));
        holder.txtDuration.setText(lflightDetails.getFlightOnwardTotalDuration());
        holder.txtFlightPrice.setText("\u20B9 " + lflightDetails.getFlightFare());


        if(flight.get(0).getFlightStops() == 0){

            holder.txtStops.setText("non stop");
        }else{
            holder.txtStops.setText(""+(flight.size()-1)+" stop");
        }


//        if(flight.size() == 1){
//
//            holder.flightIcon.setImageResource(lflightDetails.getFlightIcon());
//            holder.txtFlightNo.setText(lflightDetails.getFlightAirline()+"-"+flight.get(0).getFlightNo());
//            holder.txtDepart.setText(DateUtility.getTimeFromCal(flight.get(0).getFlightDepartureTime()).substring(0, 5));//getTimeFromCal
//            holder.txtArrive.setText(DateUtility.getTimeFromCal(flight.get(0).getFlightArrivalTime()).substring(0, 5));
//            holder.txtDuration.setText(flight.get(0).getFlightDurationTime());
//            holder.txtFlightPrice.setText("Rs."+ lflightDetails.getFlightFare());
//            holder.txtStops.setText("non stop");
//
//        }
//        else{
//
//            holder.flightIcon.setImageResource(lflightDetails.getFlightIcon());
//            holder.txtFlightNo.setText(lflightDetails.getFlightAirline()+"-"+flight.get(0).getFlightNo());
//            holder.txtDepart.setText(DateUtility.getTimeFromCal(flight.get(0).getFlightDepartureTime()).substring(0, 5));//getTimeFromCal
//            holder.txtArrive.setText(DateUtility.getTimeFromCal(flight.get(0).getFlightArrivalTime()).substring(0, 5));
//            holder.txtDuration.setText("n/a");
//            holder.txtFlightPrice.setText("Rs."+ lflightDetails.getFlightFare());
//            holder.txtStops.setText(""+flight.size());
//        }

        return row;
    }

    class MyHolder {

        LinearLayout mLLayout;
        ImageView flightIcon;
        TextView txtFlightNo;
        TextView txtDepart;
        TextView txtArrive;
        TextView txtDuration;
        TextView txtStops;
        TextView txtFlightPrice;

        public MyHolder(View itemView) {

            mLLayout =  (LinearLayout) itemView.findViewById(R.id.flightListItemLLayout);
            flightIcon = (ImageView) itemView.findViewById(R.id.imgFlight);
            txtFlightNo = (TextView) itemView.findViewById(R.id.txtFlightNo);
            txtDepart = (TextView) itemView.findViewById(R.id.txtDepart);
            txtArrive = (TextView) itemView.findViewById(R.id.txtArrive);
            txtDuration = (TextView) itemView.findViewById(R.id.txtDuration);
            txtStops = (TextView) itemView.findViewById(R.id.txtStops);
            txtFlightPrice = (TextView) itemView.findViewById(R.id.txtPrice);
        }
    }
}
//lccp_airLineCodeString="AI_0S_9W_6E_UK_G8_"