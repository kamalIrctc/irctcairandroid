package com.irctc.air.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.irctc.air.R;
import com.irctc.air.fragment.BookingHistoryFragment;
import com.irctc.air.model.ModelCancelTicket;
import com.irctc.air.model.ModelPnrPax;
import com.irctc.air.model.passenger_list.ModelPassengerListItem;
import com.irctc.air.model.route_list.ModelRoutListItem;
import com.irctc.air.model.search_result_round_trip.IntlFlights;
import com.irctc.air.model.search_result_round_trip.Segments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Rajnikant Kumar on 9/21/2018.
 */

public class AdapterReadyToCancelPassengerList extends BaseAdapter {
    ArrayList<ModelPassengerListItem> passengerList;
    public static ArrayList<ModelPnrPax> pnrPaxList;
    public ArrayList<ModelRoutListItem> routeList;
    public static List<ModelCancelTicket> cancelTicketList;
    Context context;
    public AdapterReadyToCancelPassengerList(Context context, ArrayList<ModelPassengerListItem> passengerList,ArrayList<ModelRoutListItem> routeList) {
        this.context = context;
        this.passengerList = passengerList;
        this.routeList=routeList;
        if(pnrPaxList==null){
        pnrPaxList = new ArrayList<>();
        }
        if(cancelTicketList==null){
            cancelTicketList=new ArrayList<>();
        }
    }

    @Override
    public int getCount() {
        return passengerList.size();
    }

    @Override
    public Object getItem(int arg0) {
        return passengerList.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }
    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View row = convertView;
        AdapterReadyToCancelPassengerList.MyHolder holder = null;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row =  inflater.inflate(R.layout.item_ready_to_cancel_passenger_list,  null);
            holder = new AdapterReadyToCancelPassengerList.MyHolder(row);
            row.setTag(holder);
        } else {
            holder = (AdapterReadyToCancelPassengerList.MyHolder) row.getTag();
        }
        switch (passengerList.get(position).getType()){
            case "0":
                holder.userIcon.setImageResource(R.drawable.adult);
                switch (passengerList.get(position).getGender()){
                    case "0":
                        holder.fullName.setText("Mr. "+passengerList.get(position).getFirstName()+" "+passengerList.get(position).getLastName());
                        break;
                    case "1":
                        holder.fullName.setText("Mrs. "+passengerList.get(position).getFirstName()+" "+passengerList.get(position).getLastName());
                        break;
                    case "2":
                        holder.fullName.setText("Miss. "+passengerList.get(position).getFirstName()+" "+passengerList.get(position).getLastName());
                        break;
                }
                break;
            case "1":
                holder.userIcon.setImageResource(R.drawable.child);
                switch (passengerList.get(position).getGender()){
                    case "0":
                        holder.fullName.setText("Master. "+passengerList.get(position).getFirstName()+" "+passengerList.get(position).getLastName());
                        break;
                    case "1":
                        holder.fullName.setText("Miss. "+passengerList.get(position).getFirstName()+" "+passengerList.get(position).getLastName());
                        break;
                    case "2":
                        holder.fullName.setText("Miss. "+passengerList.get(position).getFirstName()+" "+passengerList.get(position).getLastName());
                        break;
                }
                break;
            case "2":
                holder.userIcon.setImageResource(R.drawable.infant);
                switch (passengerList.get(position).getGender()){
                    case "0":
                        holder.fullName.setText("Master. "+passengerList.get(position).getFirstName()+" "+passengerList.get(position).getLastName());
                        break;
                    case "1":
                        holder.fullName.setText("Miss. "+passengerList.get(position).getFirstName()+" "+passengerList.get(position).getLastName());
                        break;
                    case "2":
                        holder.fullName.setText("Miss. "+passengerList.get(position).getFirstName()+" "+passengerList.get(position).getLastName());
                        break;
                }
                break;
        }

        holder.status.setText("Status : "+passengerList.get(position).getDeliveryStatus());
        if(!passengerList.get(position).getDeliveryStatus().equals("Delivered")){
            holder.userCancel.setEnabled(false);
        }else{
            holder.userCancel.setEnabled(true);
        }
        holder.userCancel.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                for(int i=0;i<routeList.size();i++) {
                    for (int j = 0; j < routeList.get(i).getPassengersList().size(); j++) {
                        String paxNo = routeList.get(i).getPassengersList().get(j).getPaxNo();
                        if(paxNo.equals(passengerList.get(position).getPaxNo())){
                            String pnr = routeList.get(i).getPnr();
                            ModelPnrPax modelPnrPax=new ModelPnrPax();
                            modelPnrPax.setPnr(pnr);
                            modelPnrPax.setPaxNo(paxNo);
                            if(isChecked){
                            pnrPaxList.add(modelPnrPax);
                            }else{
                                recRemovePax(modelPnrPax);
                            }
                        }
                    }
                }
               // Toast.makeText(context,pnrPaxList.size()+"",Toast.LENGTH_SHORT).show();
         /*       for(int i=0;i<routeList.size();i++) {
                    for (int j = 0; j < routeList.get(i).getPassengersList().size(); j++) {
                        String pnr= routeList.get(i).getPnr();
                        String paxNo=routeList.get(i).getPassengersList().get(j).getPaxNo();
                        for(ModelPnrPax modelPnrPax:pnrPaxList){
                            if(modelPnrPax.getPnr().equals(pnr) && modelPnrPax.getPaxNo().equals(paxNo)){
                                String transId= BookingHistoryFragment.transactionId;
                                String segNo= routeList.get(i).getPassengersList().get(j).getSegNo();
                                String oid= routeList.get(i).getPassengersList().get(j).getOid();
                                String airlineCode= routeList.get(i).getCarrear();
                                ModelCancelTicket cancelTicket=new ModelCancelTicket();
                                cancelTicket.setTransId(transId);
                                cancelTicket.setPnr(pnr);
                                cancelTicket.setPaxNo(paxNo);
                                cancelTicket.setSegNo(segNo);
                                cancelTicket.setOid(oid);
                                cancelTicket.setAirlineCode(airlineCode);
                                if(isChecked){
                                    boolean canAdd=true;
                                    for (ModelCancelTicket cancelTicket1 : cancelTicketList) {
                                        if (cancelTicket.getTransId().equals(cancelTicket1.getTransId()) &&
                                                cancelTicket.getPnr().equals(cancelTicket1.getPnr()) &&
                                                cancelTicket.getPaxNo().equals(cancelTicket1.getPaxNo()) &&
                                                cancelTicket.getSegNo().equals(cancelTicket1.getSegNo()) &&
                                                cancelTicket.getOid().equals(cancelTicket1.getOid()) &&
                                                cancelTicket.getAirlineCode().equals(cancelTicket1.getAirlineCode())) {
                                        canAdd=false;
                                        }
                                    }
                                    if(canAdd){
                                        cancelTicketList.add(cancelTicket);
                                    }
                                }else{
                                        recRemove(cancelTicketList,cancelTicket);
                                }
                            }
                        }
                    }
                }
        */
                 Intent intent=new Intent();
                    intent.setAction("BROADCAST_CANCEL");
                    buttonView.getContext().sendBroadcast(intent);
            }
        });
      return row;
    }

    /*public static List<ModelCancelTicket> makeQuery(){
        for(int i=0;i<routeList.size();i++) {
            for (int j = 0; j < routeList.get(i).getPassengersList().size(); j++) {
                String pnr= routeList.get(i).getPnr();
                String paxNo=routeList.get(i).getPassengersList().get(j).getPaxNo();
                for(ModelPnrPax modelPnrPax:pnrPaxList){
                    if(modelPnrPax.getPnr().equals(pnr) && modelPnrPax.getPaxNo().equals(paxNo)){
                        String transId= BookingHistoryFragment.transactionId;
                        String segNo= routeList.get(i).getPassengersList().get(j).getSegNo();
                        String oid= routeList.get(i).getPassengersList().get(j).getOid();
                        String airlineCode= routeList.get(i).getCarrear();
                        ModelCancelTicket cancelTicket=new ModelCancelTicket();
                        cancelTicket.setTransId(transId);
                        cancelTicket.setPnr(pnr);
                        cancelTicket.setPaxNo(paxNo);
                        cancelTicket.setSegNo(segNo);
                        cancelTicket.setOid(oid);
                        cancelTicket.setAirlineCode(airlineCode);
                            boolean canAdd=true;
                            for (ModelCancelTicket cancelTicket1 : cancelTicketList) {
                                if (cancelTicket.getTransId().equals(cancelTicket1.getTransId()) &&
                                        cancelTicket.getPnr().equals(cancelTicket1.getPnr()) &&
                                        cancelTicket.getPaxNo().equals(cancelTicket1.getPaxNo()) &&
                                        cancelTicket.getSegNo().equals(cancelTicket1.getSegNo()) &&
                                        cancelTicket.getOid().equals(cancelTicket1.getOid()) &&
                                        cancelTicket.getAirlineCode().equals(cancelTicket1.getAirlineCode())) {
                                    canAdd=false;
                                }
                            }
                            if(canAdd){
                                cancelTicketList.add(cancelTicket);
                            }
                    }
                }
            }
        }
        return cancelTicketList;
    }*/

   /* public static void recRemove(List<ModelCancelTicket> cancelTicketList,ModelCancelTicket cancelTicket){
        for (ModelCancelTicket cancelTicket1 : cancelTicketList) {
            if (cancelTicket.getTransId().equals(cancelTicket1.getTransId()) &&
                    cancelTicket.getPnr().equals(cancelTicket1.getPnr()) &&
                    cancelTicket.getPaxNo().equals(cancelTicket1.getPaxNo()) &&
                    cancelTicket.getSegNo().equals(cancelTicket1.getSegNo()) &&
                    cancelTicket.getOid().equals(cancelTicket1.getOid()) &&
                    cancelTicket.getAirlineCode().equals(cancelTicket1.getAirlineCode())) {
                cancelTicketList.remove(cancelTicket1);
                recRemove(cancelTicketList,cancelTicket);
                break;
            }
        }
    }
*/
    private void recRemovePax(ModelPnrPax modelPnrPax){
        for(ModelPnrPax modelPnrPax1:pnrPaxList){
            if(modelPnrPax.getPnr().equals(modelPnrPax1.getPnr()) && modelPnrPax.getPaxNo().equals(modelPnrPax1.getPaxNo())){
                pnrPaxList.remove(modelPnrPax1);
                recRemovePax(modelPnrPax);
                break;
            }
        }
    }

    class MyHolder {
        TextView fullName;

        TextView flightStartDate;
        TextView flightEndDate;
        TextView flightDuration,status;
        ImageView userIcon;
        CheckBox userCancel;
        public MyHolder(View itemView) {
            fullName = (TextView) itemView.findViewById(R.id.fullName);
            status = (TextView) itemView.findViewById(R.id.status);
            userIcon = (ImageView) itemView.findViewById(R.id.userIcon);
            userCancel=(CheckBox)itemView.findViewById(R.id.userCancel);

   /*         flightStartDate = (TextView) itemView.findViewById(R.id.TXT_FLIGHT_START_DATE);
            flightEndDate = (TextView) itemView.findViewById(R.id.TXT_END_FLIGHT_END_DATE);
            flightDuration = (TextView) itemView.findViewById(R.id.FLIGHT_DURATION);
    */    }
    }


}
