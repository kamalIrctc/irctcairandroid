package com.irctc.air.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.irctc.air.R;
import com.irctc.air.model.InnerFlightDetailsBeans;
import com.irctc.air.model.route_list.ModelRoutListItem;
import com.irctc.air.model.search_result_one_way.Flights;
import com.irctc.air.round.trip.domastic.model.LstBaggageDetails;
import com.irctc.air.util.AppKeys;
import com.irctc.air.util.DateUtility;
import com.irctc.air.util.Pref;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by tourism on 4/30/2016.
 */


public class AdapterOneWayFlightInfo extends BaseAdapter {

    ArrayList<ModelRoutListItem> routList;
    Context mContext;

    public AdapterOneWayFlightInfo(Context context, ArrayList<ModelRoutListItem> routList) {
        mContext = context;
        this.routList = routList;

    }

    @Override
    public int getCount() {

        return routList.size();
    }

    @Override
    public Object getItem(int arg0) {

        return null;
    }

    @Override
    public long getItemId(int arg0) {

        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        MyHolder holder = null;

        if (row == null) {

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row =  inflater.inflate(R.layout.flight_detail_info_item_layout,  null);
            holder = new MyHolder(row);
            row.setTag(holder);


        } else {
            holder = (MyHolder) row.getTag();
        }

        //Log.e("Image Path",Pref.getString(mContext,AppKeys.FLIGHT_ICON_PATH)+routList.get(position).getCarrear()+".png");
        Picasso.with(mContext).load(Pref.getString(mContext,AppKeys.FLIGHT_ICON_PATH)+routList.get(position).getCarrear()+".png").into(holder.flightIcon);
        holder.flightFromAirCode.setText(routList.get(position).getCarrear() + "-" + routList.get(position).getFlightCode());

        holder.flightStartDate.setText(routList.get(position).getDepartureDate());
        holder.flightEndDate.setText(routList.get(position).getArrivalDate());

   //     if(mDataset.get(position).getFlightDurationTime().equalsIgnoreCase("")){

   //         holder.flightDuration.setText( DateUtility.getTimeDifferenceInTwoDate(mDataset.get(position).getFlightArrivalTime(), mDataset.get(position).getFlightDepartureTime()));

    //    }else{
            holder.flightDuration.setText(routList.get(position).getTravelDuration());

//        }


        holder.flightDepartFromCode.setText(routList.get(position).getFromAirportCode());
        holder.flightDepartTime.setText(routList.get(position).getDepartureTime());

/*
        String depCity[] = mDataset.get(position).getDepart().split(" ");
        StringBuilder depCityName = new StringBuilder();
        depCityName.append(depCity[0]);

        if(depCity.length > 1){
            depCityName.append("\n");
        }
        for (int i = 1; i <depCity.length ; i++) {

            if(i != depCity.length-1){
                depCityName.append(depCity[i] + " ");
            }
            else{
                depCityName.append(depCity[i] );
            }
        }
*/
        if(routList.get(position).getFromAirportTerminal()!=null){
            String terminal = "\n"+routList.get(position).getFromAirportTerminal();
            holder.flightDepartCity.setText(routList.get(position).getFromAirportName()+terminal);
        }else{
            String terminal = "\n ";
            holder.flightDepartCity.setText(routList.get(position).getFromAirportName()+terminal);
        }
        holder.flightArriveToCode.setText(routList.get(position).getToAirportCode());
        holder.flightArriveTime.setText(routList.get(position).getArrivalTime());
      if(routList.get(position).getToAirportTerminal()!=null){
            String terminal = "\n"+routList.get(position).getToAirportTerminal();
            holder.flightArriveCity.setText(routList.get(position).getToAirportName()+terminal);
        }else{
            String terminal = "\n ";
            holder.flightArriveCity.setText(routList.get(position).getToAirportName()+terminal);
        }
        if(position == 0) {
            holder.layoverTime.setVisibility(View.GONE);
            holder.layLayover.setVisibility(View.GONE);
        }else{
            String halt = routList.get(position-1).getHalt().replace("|","\n");
            holder.layoverTime.setVisibility(View.VISIBLE);
            holder.layLayover.setVisibility(View.VISIBLE);
            holder.layoverTime.setText(halt);
        }
        if (routList.get(position).getServiceProvider().equalsIgnoreCase("GDS")){
            if (!routList.get(position).isFreeMeal()){
                if (position == 0){
                    LstBaggageDetails lstBaggageDetails = routList.get(position).getLstBaggageDetails();
                    if (lstBaggageDetails !=null){

                        if (lstBaggageDetails.getCheckInValue() !=null && !TextUtils.isEmpty(lstBaggageDetails.getCabinValue())){
                            if (lstBaggageDetails.getIsKg()){
                                holder.tv_no_free_meel.setText("No Free Meal, Baggage max "+lstBaggageDetails.getCheckInValue()+" kg, 1 pcs");
                            }else {
                                holder.tv_no_free_meel.setText("No Free Meal, Baggage "+lstBaggageDetails.getCheckInValue()+" pcs");
                            }
                        }

                    }
                }
                holder.tv_no_free_meel.setVisibility(View.VISIBLE);

            }else {
                holder.tv_no_free_meel.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(routList.get(position).getServiceProvider())){
                if (routList.get(position).getServiceProvider().equalsIgnoreCase("GDS") && routList.get(position).isFreeMeal()){
                    holder.tv_no_free_meel.setVisibility(View.VISIBLE);
                    holder.tv_no_free_meel.setText("Free Meal");
                }
            }
        }

        return row;
    }

    class MyHolder {
        ImageView flightIcon;
        TextView flightFromAirCode;
        TextView flightStartDate;
        TextView flightEndDate;
        TextView flightDuration;
        TextView flightDepartFromCode;
        TextView flightDepartTime;
        TextView flightDepartCity;
        TextView flightArriveToCode;
        TextView flightArriveTime;
        TextView flightArriveCity;
        LinearLayout layLayover;
        TextView layoverTime, tv_no_free_meel, tv_test;
        public MyHolder(View itemView) {
            flightIcon = (ImageView) itemView.findViewById(R.id.FLIGHT_IMAGE);
            flightFromAirCode = (TextView) itemView.findViewById(R.id.FLIGHT_NUMBER);
            flightStartDate = (TextView) itemView.findViewById(R.id.TXT_FLIGHT_START_DATE);
            flightEndDate = (TextView) itemView.findViewById(R.id.TXT_END_FLIGHT_END_DATE);
            tv_no_free_meel = (TextView) itemView.findViewById(R.id.tv_no_free_meel);
//            tv_test = (TextView) itemView.findViewById(R.id.tv_test);
            flightDuration = (TextView) itemView.findViewById(R.id.FLIGHT_DURATION);
            flightDepartFromCode = (TextView) itemView.findViewById(R.id.TXT_START_STN_CODE);
            flightDepartTime = (TextView) itemView.findViewById(R.id.TXT_FROM_STN_FLIGHT_TIME);
            flightDepartCity = (TextView) itemView.findViewById(R.id.TXT_START_STN_CITY_NAME);
            flightArriveToCode = (TextView) itemView.findViewById(R.id.TXT_END_STN_CODE);
            flightArriveTime = (TextView) itemView.findViewById(R.id.TXT_END_STN_FLIGHT_TIME);
            flightArriveCity = (TextView) itemView.findViewById(R.id.TXT_END_STN_CITY_NAME);
            layoverTime = (TextView) itemView.findViewById(R.id.LAYOVER_TIME);
            layLayover = (LinearLayout) itemView.findViewById(R.id.LAYOVER_LAYOUT);

        }
    }
}
