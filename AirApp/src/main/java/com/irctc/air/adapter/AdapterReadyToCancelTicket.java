package com.irctc.air.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.irctc.air.R;
import com.irctc.air.fragment.BookingHistoryFragment;
import com.irctc.air.model.passenger_list.ModelPassengerListItem;
import com.irctc.air.model.route_list.ModelRoutListItem;
import com.irctc.air.util.AppKeys;
import com.irctc.air.util.ListViewInsideScrollView;
import com.irctc.air.util.Pref;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.irctc.air.activity.ActivityMain.context;

/**
 * Created by tourism on 4/30/2016.
 */
public class AdapterReadyToCancelTicket extends BaseAdapter {

    public ArrayList<ModelRoutListItem> routeList;
    Context mContext;

    public AdapterReadyToCancelTicket(Context context, ArrayList<ModelRoutListItem> routeList) {

        mContext = context;
        this.routeList = routeList;

    }

    @Override
    public int getCount() {

        return routeList.size();
    }

    @Override
    public Object getItem(int arg0) {

        return null;
    }

    @Override
    public long getItemId(int arg0) {

        return arg0;
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {
        View row = convertView;
        MyHolder holder = null;

        if (row == null) {

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row =  inflater.inflate(R.layout.item_ticket_segment,  null);
            holder = new MyHolder(row);
            row.setTag(holder);


        } else {
            holder = (MyHolder) row.getTag();
        }
        Picasso.with(context).load(Pref.getString(context, AppKeys.FLIGHT_ICON_PATH)+routeList.get(position).getCarrear()+".png").into(holder.flightIcon);
        holder.flightFromAirCode.setText(routeList.get(position).getFlightCode());
        routeList.get(position).setArrivalDateOnlyAnd(routeList.get(position).getArrivalTimeAnd());
        routeList.get(position).setArrivalTimeOnlyAnd(routeList.get(position).getArrivalTimeAnd());
        routeList.get(position).setDepartureDateOnlyAnd(routeList.get(position).getDepartureTimeAnd());
        routeList.get(position).setDepartureTimeOnlyAnd(routeList.get(position).getDepartureTimeAnd());
        holder.flightDepartTime.setText(routeList.get(position).getDepartureTimeOnlyAnd());
        holder.flightStartDate.setText(routeList.get(position).getDepartureDateOnlyAnd());
        holder.flightEndDate.setText(routeList.get(position).getArrivalDateOnlyAnd());
        holder.flightArriveTime.setText(routeList.get(position).getArrivalTimeOnlyAnd());
        holder.flightDuration.setText(routeList.get(position).getTravelDuration());
        holder.flightDepartFromCode.setText(routeList.get(position).getFromAirportCode());
        holder.flightArriveToCode.setText(routeList.get(position).getToAirportCode());
        if(routeList.get(position).getToAirportTerminal()!=null) {
            holder.flightArriveCity.setText(routeList.get(position).getToAirportName() + "\n"+routeList.get(position).getToAirportTerminal());
        }else{
            holder.flightArriveCity.setText(routeList.get(position).getToAirportName() + "\n");
        }
        if(routeList.get(position).getFromAirportTerminal()!=null) {
            holder.flightDepartCity.setText(routeList.get(position).getFromAirportName() + "\n"+routeList.get(position).getFromAirportTerminal());
        }else{
            holder.flightDepartCity.setText(routeList.get(position).getFromAirportName() + "\n");
        }

        if(position == 0) {
            holder.layoverTime.setVisibility(View.GONE);
            holder.layLayover.setVisibility(View.GONE);
        }else{
                    holder.layoverTime.setVisibility(View.VISIBLE);
                    holder.layLayover.setVisibility(View.VISIBLE);
            if(routeList.get(position).getFromAirportName()!=null){
              holder.layoverTime.setText("Change Flight at "+routeList.get(position).getFromAirportName());
            }else{
                holder.layoverTime.setText("Change Flight");
            }
        }
        if(position==routeList.size()-1){
        holder.tvlList.setVisibility(View.VISIBLE);
        holder.v1.setVisibility(View.VISIBLE);
        holder.v2.setVisibility(View.VISIBLE);
        holder.passengerList.setVisibility(View.VISIBLE);
        holder.passengerList.setAdapter(new AdapterReadyToCancelPassengerList(mContext,routeList.get(position).getPassengersList(),routeList));
        //setListViewHeightBasedOnChildren(holder.passengerList);
        ListViewInsideScrollView.open(holder.passengerList);
        }else{
            holder.tvlList.setVisibility(View.GONE);
            holder.v1.setVisibility(View.GONE);
            holder.v2.setVisibility(View.GONE);
            holder.passengerList.setVisibility(View.GONE);
            holder.passengerList.setAdapter(null);

        }
        return row;
    }

    class MyHolder {

        //strip one
        ImageView flightIcon;
        TextView flightFromAirCode;

        TextView flightStartDate;
        TextView flightEndDate;

        TextView flightDuration;

        //strip 2
        TextView flightDepartFromCode;
        TextView flightDepartTime;
        TextView flightDepartCity;

        TextView flightArriveToCode;
        TextView flightArriveTime;
        TextView flightArriveCity;

        // LAYOVER LAYOUT AND TIME
        LinearLayout layLayover;
        TextView layoverTime,tvlList;
        ListView passengerList;
        View v1,v2;

        public MyHolder(View itemView) {

            flightIcon = (ImageView) itemView.findViewById(R.id.FLIGHT_IMAGE);
            flightFromAirCode = (TextView) itemView.findViewById(R.id.FLIGHT_NUMBER);

            flightStartDate = (TextView) itemView.findViewById(R.id.TXT_FLIGHT_START_DATE);
            flightEndDate = (TextView) itemView.findViewById(R.id.TXT_END_FLIGHT_END_DATE);

            flightDuration = (TextView) itemView.findViewById(R.id.FLIGHT_DURATION);

            flightDepartFromCode = (TextView) itemView.findViewById(R.id.TXT_START_STN_CODE);
            flightDepartTime = (TextView) itemView.findViewById(R.id.TXT_FROM_STN_FLIGHT_TIME);
            flightDepartCity = (TextView) itemView.findViewById(R.id.TXT_START_STN_CITY_NAME);

            flightArriveToCode = (TextView) itemView.findViewById(R.id.TXT_END_STN_CODE);
            flightArriveTime = (TextView) itemView.findViewById(R.id.TXT_END_STN_FLIGHT_TIME);
            flightArriveCity = (TextView) itemView.findViewById(R.id.TXT_END_STN_CITY_NAME);

            layoverTime = (TextView) itemView.findViewById(R.id.LAYOVER_TIME);
            layLayover = (LinearLayout) itemView.findViewById(R.id.LAYOVER_LAYOUT);
            passengerList = (ListView) itemView.findViewById(R.id.passengerList);
            tvlList = (TextView) itemView.findViewById(R.id.tvlList);
            v1 = (View) itemView.findViewById(R.id.v1);
            v2 = (View) itemView.findViewById(R.id.v2);
        }
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
}
