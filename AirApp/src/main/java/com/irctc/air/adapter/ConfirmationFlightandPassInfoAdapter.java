package com.irctc.air.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.irctc.air.R;
import com.irctc.air.model.FlightConfirmationBean;
import com.irctc.air.util.AppLogger;
import com.irctc.air.util.DateUtility;

import java.util.ArrayList;

/**
 * Created by tourism on 4/30/2016.
 */
public class ConfirmationFlightandPassInfoAdapter extends BaseAdapter {

    public  ArrayList<FlightConfirmationBean> mAlObjCnfFlightDetail;
    Context mContext;

    public ConfirmationFlightandPassInfoAdapter(Context context,  ArrayList<FlightConfirmationBean> cnfFlightandPassDetails) {

        mContext = context;
        mAlObjCnfFlightDetail = cnfFlightandPassDetails;

    }

    @Override
    public int getCount() {

        return mAlObjCnfFlightDetail.get(0).getAlSegmentDetail().size();
    }

    @Override
    public Object getItem(int arg0) {

        return null;
    }

    @Override
    public long getItemId(int arg0) {

        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        MyHolder holder = null;

        if (row == null) {

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row =  inflater.inflate(R.layout.confirmation_ticket_item_layout,  null);
            holder = new MyHolder(row);
            row.setTag(holder);


        } else {
            holder = (MyHolder) row.getTag();
        }

        holder.flightIcon.setImageResource(mAlObjCnfFlightDetail.get(0).getAlSegmentDetail().get(position).getFlighticon());
        holder.flightFromAirCode.setText(mAlObjCnfFlightDetail.get(0).getAlSegmentDetail().get(position).getOac() + "-" + mAlObjCnfFlightDetail.get(0).getAlSegmentDetail().get(position).getFno());


        holder.flightStartDate.setText(DateUtility.getDateAndMonthFromCal(mAlObjCnfFlightDetail.get(0).getAlSegmentDetail().get(position).getDepartTime()));
        holder.flightEndDate.setText(DateUtility.getDateAndMonthFromCal(mAlObjCnfFlightDetail.get(0).getAlSegmentDetail().get(position).getArriavlTime()));

        holder.flightDuration.setText(mAlObjCnfFlightDetail.get(0).getAlSegmentDetail().get(position).getDuration());


        holder.flightDepartFromCode.setText(mAlObjCnfFlightDetail.get(0).getAlSegmentDetail().get(position).getSegorigC());

       // holder.flightDepartFromCode.setText(mAlObjCnfFlightDetail.get(0).getAlSegmentDetail().get(position).getSegorig().substring(0, 3).toUpperCase());
        holder.flightDepartTime.setText(DateUtility.getTime(mAlObjCnfFlightDetail.get(0).getAlSegmentDetail().get(position).getDepartTime()));


        /**
         * 28 Oct
         * TERMINAL NAME ADDED
         * Show terminal name using via
         */
        String via[] = mAlObjCnfFlightDetail.get(0).getAlSegmentDetail().get(position).getVia().split(" to ");

        String depCity[] = via[0].split(" ");
        StringBuilder depCityName = new StringBuilder();
        depCityName.append(depCity[0]);
        if(depCity.length > 1){
            depCityName.append("\n");
        }

        for (int i = 1; i <depCity.length ; i++) {

            if(i != depCity.length-1){
                depCityName.append(depCity[i] + " ");
            }
            else{
                depCityName.append(depCity[i] );
            }

        }
        AppLogger.enable();
        AppLogger.e("depCityName ", depCityName.toString());

        holder.flightDepartCity.setText(depCityName);

        holder.flightArriveToCode.setText(mAlObjCnfFlightDetail.get(0).getAlSegmentDetail().get(position).getSegdestC());

        //holder.flightArriveToCode.setText(mAlObjCnfFlightDetail.get(0).getAlSegmentDetail().get(position).getSegdest().substring(0, 3).toUpperCase());
        holder.flightArriveTime.setText(DateUtility.getTime(mAlObjCnfFlightDetail.get(0).getAlSegmentDetail().get(position).getArriavlTime()));

        /**
         * 28 Oct
         * TERMINAL NAME ADDED
         * Show terminal name using via
         */

        String arrCity[] = via[1].split(" ");
        StringBuilder arrCityName = new StringBuilder();
        arrCityName.append(arrCity[0]);
        if(arrCity.length > 1){
            arrCityName.append("\n");
        }

        for (int i = 1; i <arrCity.length ; i++) {

            if(i != arrCity.length-1){
                arrCityName.append(arrCity[i] + " ");
            }
            else{
                arrCityName.append(arrCity[i] );
            }

        }
        AppLogger.enable();
        AppLogger.e("arrCityName ", arrCityName.toString());

        holder.flightArriveCity.setText(arrCityName);


        if(position == 0) {
            holder.layoverTime.setVisibility(View.GONE);
            holder.layLayover.setVisibility(View.GONE);
        }else{

           if(mAlObjCnfFlightDetail.get(0).getAlSegmentDetail().size() > (position) ){
                holder.layoverTime.setVisibility(View.VISIBLE);
                holder.layLayover.setVisibility(View.VISIBLE);

                holder.layoverTime.setText(DateUtility.getTimeDifferenceInTwoDate(mAlObjCnfFlightDetail.get(0).getAlSegmentDetail().get(position).getDepartTime(), mAlObjCnfFlightDetail.get(0).getAlSegmentDetail().get(position-1).getArriavlTime()));
            }

        }

        return row;
    }

    class MyHolder {

        //strip one
        ImageView flightIcon;
        TextView flightFromAirCode;

        TextView flightStartDate;
        TextView flightEndDate;

        TextView flightDuration;

        //strip 2
        TextView flightDepartFromCode;
        TextView flightDepartTime;
        TextView flightDepartCity;

        TextView flightArriveToCode;
        TextView flightArriveTime;
        TextView flightArriveCity;

        // LAYOVER LAYOUT AND TIME
        LinearLayout layLayover;
        TextView layoverTime;


        public MyHolder(View itemView) {

            flightIcon = (ImageView) itemView.findViewById(R.id.FLIGHT_IMAGE);
            flightFromAirCode = (TextView) itemView.findViewById(R.id.FLIGHT_NUMBER);

            flightStartDate = (TextView) itemView.findViewById(R.id.TXT_FLIGHT_START_DATE);
            flightEndDate = (TextView) itemView.findViewById(R.id.TXT_END_FLIGHT_END_DATE);

            flightDuration = (TextView) itemView.findViewById(R.id.FLIGHT_DURATION);

            flightDepartFromCode = (TextView) itemView.findViewById(R.id.TXT_START_STN_CODE);
            flightDepartTime = (TextView) itemView.findViewById(R.id.TXT_FROM_STN_FLIGHT_TIME);
            flightDepartCity = (TextView) itemView.findViewById(R.id.TXT_START_STN_CITY_NAME);

            flightArriveToCode = (TextView) itemView.findViewById(R.id.TXT_END_STN_CODE);
            flightArriveTime = (TextView) itemView.findViewById(R.id.TXT_END_STN_FLIGHT_TIME);
            flightArriveCity = (TextView) itemView.findViewById(R.id.TXT_END_STN_CITY_NAME);

            layoverTime = (TextView) itemView.findViewById(R.id.LAYOVER_TIME);
            layLayover = (LinearLayout) itemView.findViewById(R.id.LAYOVER_LAYOUT);

        }
    }
}
