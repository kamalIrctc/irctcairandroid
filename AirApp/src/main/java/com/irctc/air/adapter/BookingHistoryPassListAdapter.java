package com.irctc.air.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.irctc.air.R;
import com.irctc.air.model.PassengerDetailBean;
import com.irctc.air.util.ProjectUtil;

import java.util.ArrayList;

/**
 * Created by vivek on 4/26/2016.
 */
public class BookingHistoryPassListAdapter extends BaseAdapter{


    private Context context;
    ArrayList<PassengerDetailBean> mAlPassData;
    private LayoutInflater inflater;

    public BookingHistoryPassListAdapter(Context context, ArrayList<PassengerDetailBean> mAlPassData) {

        this.context = context;
        this.mAlPassData = mAlPassData;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mAlPassData.size();
    }

    @Override
    public Object getItem(int position) {

        return mAlPassData.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(final int position, View v, ViewGroup parent) {

        final ViewHolder holder;
        if (v == null) {
            v = inflater.inflate(R.layout.confirm_pass_list_item_layout, null);
            holder = new ViewHolder();

            holder.layDOB = (LinearLayout) v.findViewById(R.id.LAY_DOB);
            holder.txtPassDob = (TextView) v.findViewById(R.id.TV_DOB);
            holder.txtPassName = (TextView) v.findViewById(R.id.TV_PASS_NAME);
            holder.imgPassIcon = (ImageView) v.findViewById(R.id.IMG_PASS_ICON);

            /**
             * 21 Oct 2016
             * PNR - PASS STATUS
             */
            holder.txtPassStatus = (TextView) v.findViewById(R.id.TV_PASS_STATUS);
            holder.txtPassTicketNum = (TextView) v.findViewById(R.id.TV_PASS_TICKET_NUM);
            holder.txtPassTicketNumHeader = (TextView) v.findViewById(R.id.TV_PASS_TICKET_NUM_STATIC_HEADER);


            v.setTag(holder);

        } else {
            holder = (ViewHolder) v.getTag();
        }


        if (mAlPassData.get(position) != null) {
            if(mAlPassData.get(position).getPasgtype().contains("Adult")){
                holder.imgPassIcon.setImageResource(R.drawable.adult);

                //if(mAlPassData.get(position).getAge().equalsIgnoreCase("")){
                    holder.layDOB.setVisibility(View.GONE);

//                }else{
//                    holder.txtPassDob.setText(mAlPassData.get(position).getAge());
//                }
                holder.txtPassDob.setText(mAlPassData.get(position).getAge());

            }else if(mAlPassData.get(position).getPasgtype().contains("Child")){
                holder.imgPassIcon.setImageResource(R.drawable.child);
                holder.txtPassDob.setText(mAlPassData.get(position).getAge());

            }else if(mAlPassData.get(position).getPasgtype().contains("Infant")) {
                holder.imgPassIcon.setImageResource(R.drawable.infant);
                holder.txtPassDob.setVisibility(View.VISIBLE);
                holder.txtPassDob.setText(mAlPassData.get(position).getAge());
            }
            holder.txtPassName.setText(ProjectUtil.capitalFirstLetter(mAlPassData.get(position).getFname()) + " " + ProjectUtil.capitalFirstLetter(mAlPassData.get(position).getLname()));

            /**
             * 21 Oct 2016
             * PNR - PASS STATUS
             */
            holder.txtPassStatus.setText("Status  :  "+mAlPassData.get(position).getCanstatusText());

            if(!mAlPassData.get(position).getTicketNo().toString().trim().equalsIgnoreCase("")) {
                holder.txtPassTicketNum.setText(mAlPassData.get(position).getTicketNo());
                holder.txtPassTicketNumHeader.setVisibility(View.VISIBLE);
                holder.txtPassTicketNum.setVisibility(View.VISIBLE);
            }

        }

        return v;
    }

    class ViewHolder {
        private LinearLayout layDOB;
        private TextView txtPassDob;
        private TextView txtPassName;
        private ImageView imgPassIcon;
        /**
         * 21 Oct 2016
         * PNR - PASS STATUS
         */
        private TextView txtPassStatus;
        private TextView txtPassTicketNum;
        private TextView txtPassTicketNumHeader;
    }
}