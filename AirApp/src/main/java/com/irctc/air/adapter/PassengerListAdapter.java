package com.irctc.air.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.irctc.air.R;
import com.irctc.air.fragment.FragmentPlanner;
import com.irctc.air.model.AirDataModel;
import com.irctc.air.model.PassDetailbean;
import com.irctc.air.model.book_ticket.PassengerDetails;
import com.irctc.air.util.DateUtility;
import com.irctc.air.util.PassengerListClickHandler;

import java.util.ArrayList;

/**
 * Created by vivek on 4/26/2016.
 */
public class PassengerListAdapter extends BaseAdapter  {


    private Context context;
    private ArrayList<PassengerDetails> passListHolder;
    public static ArrayList<PassengerDetails> selectedPassengers;
    private LayoutInflater inflater;
    int noOfAdult = Integer.parseInt(FragmentPlanner.noOfAdults);
    int noOfChildren = Integer.parseInt(FragmentPlanner.noOfChildren);
    int noOfInfant= Integer.parseInt(FragmentPlanner.noOfInfants);
   // ArrayList<AirDataModel> mAlPlannerDataHolder;


//    private static int adultCounter = 0;
//    private static int childCounter = 0;
//    private static int infantCounter = 0;

    public PassengerListAdapter(Context context, ArrayList<PassengerDetails> list) {

        this.context = context;
        this.passListHolder = list;
        selectedPassengers=new ArrayList<>();
     //   this.mAlPlannerDataHolder = mAlPlannerData;

//        adultCounter = AirDataHolder.getListHolder().getList().get(0).getAdultPassNum();
//        childCounter = AirDataHolder.getListHolder().getList().get(0).getChildPassNum();
//        infantCounter = AirDataHolder.getListHolder().getList().get(0).getInfantPassNum();

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

         noOfAdult = Integer.parseInt(FragmentPlanner.noOfAdults);
         noOfChildren = Integer.parseInt(FragmentPlanner.noOfChildren);
         noOfInfant= Integer.parseInt(FragmentPlanner.noOfInfants);
    }

    @Override
    public int getCount() {
        return passListHolder.size();
    }

    @Override
    public Object getItem(int position) {

        return passListHolder.get(position);
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.pass_list_item_layout, null);
            holder = new ViewHolder();
            holder.cbPassSelect = (CheckBox) convertView.findViewById(R.id.PASS_CHECKBOX);
            holder.passName = (TextView) convertView.findViewById(R.id.PASS_NAME);
            holder.passDob = (TextView) convertView.findViewById(R.id.PASS_DOB);
            holder.passImage = (ImageView) convertView.findViewById(R.id.PASS_IMG_ICON);
            convertView.setTag(holder);
        }
        else {
            // The getTag returns the viewHolder object set as a tag to the view
            holder = (ViewHolder)convertView.getTag();
        }
        holder.cbPassSelect.setTag( position);

    //    final ViewHolder holder;
//        if (v == null) {
/*
        v = inflater.inflate(R.layout.pass_list_item_layout, null);
        holder = new ViewHolder();
        holder.cbPassSelect = (CheckBox) v.findViewById(R.id.PASS_CHECKBOX);
        holder.passName = (TextView) v.findViewById(R.id.PASS_NAME);
        holder.passDob = (TextView) v.findViewById(R.id.PASS_DOB);
        holder.passImage = (ImageView) v.findViewById(R.id.PASS_IMG_ICON);

*/

        // v.setTag(holder);
//        } else {
//            holder = (ViewHolder) v.getTag();
//        }
//        int incrementedPos = position;
//        incrementedPos+=1;


       /* if (passListHolder.get(position).isSelected()) {
            holder.cbPassSelect.setChecked(true);
        } else {
            holder.cbPassSelect.setChecked(false);

        }*/

//        if (passListHolder.get(position) != null) {
//            if (passListHolder.get(position).getPassType().contains("Adult")) {
//                holder.passImage.setImageResource(R.drawable.adult);
//            } else if (passListHolder.get(position).getPassType().contains("Child")) {
//                holder.passImage.setImageResource(R.drawable.child);
//            } else if (passListHolder.get(position).getPassType().contains("Infant")) {
//                holder.passImage.setImageResource(R.drawable.infant);
//            }
//        }

       // holder.passImage.setImageResource(R.drawable.menu_ver);
       // holder.passName.setText(passListHolder.get(position).getFirstName() + " " + passListHolder.get(position).getLastName());

        if(passListHolder.get(position).getDob()!=null){
          if (!passListHolder.get(position).getDob().equalsIgnoreCase("")) {
            holder.passDob.setText("DOB: " +passListHolder.get(position).getDob());
            } else {
            holder.passDob.setText("DOB not available");
            }
        }
        switch (passListHolder.get(position).getPassengerType()){
            case "0":
                holder.passImage.setImageResource(R.drawable.adult);
                switch (passListHolder.get(position).getTitleType()){
                    case "0":
                        holder.passName.setText("Mr. "+passListHolder.get(position).getFirstName()+" "+passListHolder.get(position).getLastName());
                        break;
                    case "1":
                        holder.passName.setText("Mrs. "+passListHolder.get(position).getFirstName()+" "+passListHolder.get(position).getLastName());
                        break;
                    case "2":
                        holder.passName.setText("Miss. "+passListHolder.get(position).getFirstName()+" "+passListHolder.get(position).getLastName());
                        break;
                }
                break;
            case "1":
                holder.passImage.setImageResource(R.drawable.child);
                switch (passListHolder.get(position).getTitleType()){
                    case "0":
                        holder.passName.setText("Master. "+passListHolder.get(position).getFirstName()+" "+passListHolder.get(position).getLastName());
                        break;
                    case "1":
                        holder.passName.setText("Miss. "+passListHolder.get(position).getFirstName()+" "+passListHolder.get(position).getLastName());
                        break;
                    case "2":
                        holder.passName.setText("Miss. "+passListHolder.get(position).getFirstName()+" "+passListHolder.get(position).getLastName());
                        break;
                }
                break;
            case "2":
                holder.passImage.setImageResource(R.drawable.infant);
                switch (passListHolder.get(position).getTitleType()){
                    case "0":
                        holder.passName.setText("Master. "+passListHolder.get(position).getFirstName()+" "+passListHolder.get(position).getLastName());
                        break;
                    case "1":
                        holder.passName.setText("Miss. "+passListHolder.get(position).getFirstName()+" "+passListHolder.get(position).getLastName());
                        break;
                    case "2":
                        holder.passName.setText("Miss. "+passListHolder.get(position).getFirstName()+" "+passListHolder.get(position).getLastName());
                        break;
                }
                break;
        }



        /* if ((passListHolder.get(position).getPassAge() != null) && (!passListHolder.get(position).getPassAge().equalsIgnoreCase(""))) {
            String[] passDob = passListHolder.get(position).getPassAge().split("/");
            holder.passDob.setText("DOB: " + passDob[0] + " " + DateUtility.getSingleMonthName(Integer.parseInt(passDob[1])) + " " + passDob[2]);
        } else {
            holder.passDob.setText("DOB not available");
        }
*/

        holder.cbPassSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.cbPassSelect.isChecked()) {
                    if(passListHolder.get(position).getPassengerType().equals("0")){
                        if(getAdultFromList(selectedPassengers)>=noOfAdult){
                        Toast.makeText(context,"You Can select "+noOfAdult+" Adults only",Toast.LENGTH_SHORT).show();
                        holder.cbPassSelect.setChecked(false);
                        }else{
                        selectedPassengers.add(passListHolder.get(position));
                        }
                    }else if(passListHolder.get(position).getPassengerType().equals("1")){
                        if(getChildFromList(selectedPassengers)>=noOfChildren){
                            if(noOfChildren==0){
                                Toast.makeText(context, "You Can't select Children", Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(context, "You Can select " + noOfChildren + " Children only", Toast.LENGTH_SHORT).show();
                            }
                            holder.cbPassSelect.setChecked(false);
                        }else{
                            selectedPassengers.add(passListHolder.get(position));
                        }
                    }else if(passListHolder.get(position).getPassengerType().equals("2")){
                        if(getInfantFromList(selectedPassengers)>=noOfInfant){
                            if(noOfInfant==0){
                                Toast.makeText(context, "You Can't select Infant", Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(context, "You Can select " + noOfInfant + " Infant only", Toast.LENGTH_SHORT).show();
                            }
                            holder.cbPassSelect.setChecked(false);
                        }else{
                            selectedPassengers.add(passListHolder.get(position));
                        }
                    }else{
                        holder.cbPassSelect.setChecked(false);
                        Toast.makeText(context,"Can't be selected",Toast.LENGTH_SHORT).show();
                    }
                }else {
                selectedPassengers.remove(passListHolder.get(position));
                }
            }
        });

  //      passListHolder.get(position).getPassIndex();
        // open after next build
//        holder.passImage.setOnClickListener(new PassengerListClickHandler(context, passListHolder.get(position).getPassIndex()));

        return convertView;
    }


    class ViewHolder {
        private CheckBox cbPassSelect;
        private TextView passName;
        private TextView passDob;
        private ImageView passImage;
    }

    /////////////////////////////////////

  /*  private class MyPassListClickListener implements OnClickListener
    {
        private int mIntGroupPosition;

    }

    holder.mChkSelectBox.setOnClickListener(new MyPassListClickListener(position));*/


  private int getAdultFromList(ArrayList<PassengerDetails> passengerDetailses){
      int r=0;
      for(int i=0;i<passengerDetailses.size();i++){
          if(passengerDetailses.get(i).getPassengerType().equals("0")){
              r++;
          }
      }
      return r;
  }
    private int getChildFromList(ArrayList<PassengerDetails> passengerDetailses){
        int r=0;
        for(int i=0;i<passengerDetailses.size();i++){
            if(passengerDetailses.get(i).getPassengerType().equals("1")){
                r++;
            }
        }
        return r;
    }
    private int getInfantFromList(ArrayList<PassengerDetails> passengerDetailses){
        int r=0;
        for(int i=0;i<passengerDetailses.size();i++){
            if(passengerDetailses.get(i).getPassengerType().equals("2")){
                r++;
            }
        }
        return r;
    }

}

