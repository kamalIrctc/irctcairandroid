package com.irctc.air.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.irctc.air.R;
import com.irctc.air.model.InnerFlightDetailsBeans;
import com.irctc.air.util.DateUtility;

import java.util.ArrayList;

/**
 * Created by tourism on 4/30/2016.
 */
public class GdsFlightInfoAdapter extends BaseAdapter {

    public ArrayList<InnerFlightDetailsBeans> mDataset;
    Context mContext;

    public GdsFlightInfoAdapter(Context context, ArrayList<InnerFlightDetailsBeans> fareInnerFlightDetails) {

        mContext = context;
        mDataset = fareInnerFlightDetails;

    }

    @Override
    public int getCount() {

        return mDataset.size();
    }

    @Override
    public Object getItem(int arg0) {

        return null;
    }

    @Override
    public long getItemId(int arg0) {

        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        MyHolder holder = null;

        if (row == null) {

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row =  inflater.inflate(R.layout.gds_flight_detail_info_item_layout,  null);
            holder = new MyHolder(row);
            row.setTag(holder);


        } else {
            holder = (MyHolder) row.getTag();
        }

        holder.flightIcon.setImageResource(mDataset.get(position).getFlightIcon());
        holder.flightFromAirCode.setText(mDataset.get(position).getOac() + "-" + mDataset.get(position).getFlightNo());

        holder.flightStartDate.setText(DateUtility.getTimeInDayMonthYear(mDataset.get(position).getFlightDepartureTime()));
        holder.flightEndDate.setText(DateUtility.getTimeInDayMonthYear(mDataset.get(position).getFlightArrivalTime()));

        holder.flightDuration.setText(mDataset.get(position).getFlightDurationTime());


        holder.flightDepartFromCode.setText(mDataset.get(position).getDepartureairport());
        holder.flightDepartTime.setText(DateUtility.getTimeFromCal(mDataset.get(position).getFlightDepartureTime()).substring(0, 5));
        holder.flightDepartCity.setText(mDataset.get(position).getDepart());

        holder.flightArriveToCode.setText(mDataset.get(position).getArrivalairport());
        holder.flightArriveTime.setText(DateUtility.getTimeFromCal(mDataset.get(position).getFlightArrivalTime()).substring(0, 5));
        holder.flightArriveCity.setText(mDataset.get(position).getArrive());

        if(position == 0) {
            holder.layoverTime.setVisibility(View.GONE);
            holder.layLayover.setVisibility(View.GONE);
        }else{
            holder.layoverTime.setVisibility(View.VISIBLE);
            holder.layLayover.setVisibility(View.VISIBLE);

            // Call timeDifferenceInTwoDate to get time difference
            holder.layoverTime.setText(DateUtility.getTimeDifferenceInTwoISTDate(mDataset.get(position).getFlightDepartureTime(), mDataset.get(position-1).getFlightArrivalTime()));
        }

        return row;
    }

    class MyHolder {

        //strip one
        ImageView flightIcon;
        TextView flightFromAirCode;

        TextView flightStartDate;
        TextView flightEndDate;

        TextView flightDuration;

        //strip 2
        TextView flightDepartFromCode;
        TextView flightDepartTime;
        TextView flightDepartCity;

        TextView flightArriveToCode;
        TextView flightArriveTime;
        TextView flightArriveCity;

        // LAYOVER LAYOUT AND TIME
        LinearLayout layLayover;
        TextView layoverTime;


        public MyHolder(View itemView) {

            flightIcon = (ImageView) itemView.findViewById(R.id.FLIGHT_IMAGE);
            flightFromAirCode = (TextView) itemView.findViewById(R.id.FLIGHT_NUMBER);

            flightStartDate = (TextView) itemView.findViewById(R.id.TXT_FLIGHT_START_DATE);
            flightEndDate = (TextView) itemView.findViewById(R.id.TXT_END_FLIGHT_END_DATE);

            flightDuration = (TextView) itemView.findViewById(R.id.FLIGHT_DURATION);

            flightDepartFromCode = (TextView) itemView.findViewById(R.id.TXT_START_STN_CODE);
            flightDepartTime = (TextView) itemView.findViewById(R.id.TXT_FROM_STN_FLIGHT_TIME);
            flightDepartCity = (TextView) itemView.findViewById(R.id.TXT_START_STN_CITY_NAME);

            flightArriveToCode = (TextView) itemView.findViewById(R.id.TXT_END_STN_CODE);
            flightArriveTime = (TextView) itemView.findViewById(R.id.TXT_END_STN_FLIGHT_TIME);
            flightArriveCity = (TextView) itemView.findViewById(R.id.TXT_END_STN_CITY_NAME);

            layoverTime = (TextView) itemView.findViewById(R.id.LAYOVER_TIME);
            layLayover = (LinearLayout) itemView.findViewById(R.id.LAYOVER_LAYOUT);

        }
    }
}
