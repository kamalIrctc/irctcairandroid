package com.irctc.air.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.irctc.air.R;
import com.irctc.air.model.BookingHistoryBean;
import com.irctc.air.util.DateUtility;

import java.util.ArrayList;

/**
 * Created by vivek on 4/22/2016.
 */
public class CancelTicketHistoryAdapter extends BaseAdapter{


    private Context context;
    private ArrayList<BookingHistoryBean> list;
    private LayoutInflater inflater;

    public CancelTicketHistoryAdapter(Context context, ArrayList<BookingHistoryBean> list) {

        this.context = context;
        this.list = list;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {

        return list.get(position);
    }

    @Override
    public long getItemId(int position) {

        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View row = convertView;
        ViewHolder holder = null;

        if (row == null) {
            row = inflater.inflate(R.layout.cancel_history_lv_item_layout, null);
            holder = new ViewHolder(row);
            row.setTag(holder);

        } else {
            holder = (ViewHolder) row.getTag();
        }

        if (list.get(position) != null) {
            // Wed Apr 20 15:58:48 GMT+05:30 2016

            BookingHistoryBean booking = list.get(position);

            if(booking.getAlBookedReturnDetail().get(0).getAlSegmentDetail().size() == 0){
                holder.imgArrow.setImageResource(R.drawable.onwards_arrow48);
            }else{
                holder.imgArrow.setImageResource(R.drawable.returnarrow48);
            }

            holder.tvFrom.setText(booking.getOrig());
            holder.tvTo.setText( booking.getDest());
            holder.tvTransactionId.setText(booking.getTrnId());
            holder.tvDateMonth.setText(DateUtility.getMonthFromCal(booking.getAlBookedOnwardDetail().get(0).getAlSegmentDetail().get(0).getDepartTime()).toUpperCase() + " " +DateUtility.getYearFromCal(booking.getAlBookedOnwardDetail().get(0).getAlSegmentDetail().get(0).getDepartTime()).substring(2,4));
            holder.tvDateDay.setText(DateUtility.getDateDayFromCal(booking.getAlBookedOnwardDetail().get(0).getAlSegmentDetail().get(0).getDepartTime()));
          //  holder.tvDateYear.setText(DateUtility.getYearFromCal(booking.getAlBookedOnwardDetail().get(0).getAlSegmentDetail().get(0).getDepartTime()));

        }

        return row;
    }

    class ViewHolder {

        private TextView tvFrom;
        private TextView tvTo;
        private TextView tvFromTo;
        private TextView tvTransactionId;
        private ImageView imgCancelIcon;
        private ImageView imgArrow;
        private TextView tvDateDay;
        private TextView tvDateMonth;
        private TextView tvDateYear;

        ViewHolder(View v){

            tvFrom = (TextView) v.findViewById(R.id.TXT_FROM);
            tvTo = (TextView) v.findViewById(R.id.TXT_TO);
            tvFromTo = (TextView) v.findViewById(R.id.FROM_TO);
            tvTransactionId = (TextView) v.findViewById(R.id.TRANSACTION_ID);
            imgCancelIcon = (ImageView) v.findViewById(R.id.CANCEL_ICON);
            tvDateDay = (TextView) v.findViewById(R.id.TXT_DATE_DAY);
            tvDateMonth = (TextView) v.findViewById(R.id.TXT_DATE_MONTH);
            //tvDateYear = (TextView) v.findViewById(R.id.TXT_DATE_YEAR);
            imgArrow = (ImageView) v.findViewById(R.id.IMG_ARROW_ICON);
        }
    }
}