package com.irctc.air.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.irctc.air.R;
import com.irctc.air.model.FlightOnWardDetailBean;
import com.irctc.air.model.InnerFlightDetailsBeans;
import com.irctc.air.util.AppLogger;
import com.irctc.air.util.DateUtility;

import java.util.ArrayList;

/**
 * Created by tourism on 4/23/2016.
 */
public class RoundOnwardFlightAdapter extends BaseAdapter {

    ArrayList<FlightOnWardDetailBean> mFlightsClone;
    Context mContext;

    public RoundOnwardFlightAdapter(Context context, ArrayList<FlightOnWardDetailBean> lFlightsClone) {
        mContext = context;
        this.mFlightsClone = lFlightsClone;
        AppLogger.enable();
        AppLogger.e("SIZE ", "" + mFlightsClone.size());
    }



    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mFlightsClone.size();

    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        MyHolder holder = null;

        if (row == null) {

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row =  inflater.inflate(R.layout.round_onward_list_item,  null);
            holder = new MyHolder(row);
            row.setTag(holder);
        } else {
            holder = (MyHolder) row.getTag();
        }

        FlightOnWardDetailBean lflightDetails = mFlightsClone.get(position);
        ArrayList<InnerFlightDetailsBeans> flight = lflightDetails.getFlight();

        holder.imgOnwflightIcon.setImageResource(lflightDetails.getFlightIcon());
        holder.txtOnwFlightNo.setText(lflightDetails.getFlightAirline()+"-"+lflightDetails.getFlightOnwardFlightNo());

        holder.txtOnwDepart.setText(DateUtility.getTimeFromCal(lflightDetails.getFlightOnwDepartureTime()));//getTimeFromCal
        holder.txtOnwArrive.setText(DateUtility.getTimeFromCal(lflightDetails.getFlightOnwArrivalTime()));
        holder.txtOnwDuration.setText(lflightDetails.getFlightOnwardTotalDuration());

        if(lflightDetails.getFlightOnwardTotalStops() == 0){
            holder.txtOnwStops.setText("non stop");
        }else{
            holder.txtOnwStops.setText(""+lflightDetails.getFlightOnwardTotalStops()+" stop");
        }

//
//        holder.txtOnwDepart.setText(DateUtility.getTimeFromCal(flight.get(0).getFlightDepartureTime()).substring(0, 5));//getTimeFromCal
//        holder.txtOnwArrive.setText(DateUtility.getTimeFromCal(flight.get(0).getFlightArrivalTime()).substring(0, 5));
//        holder.txtOnwDuration.setText(flight.get(0).getFlightDurationTime());


        holder.imgRetflightIcon.setImageResource(lflightDetails.getFlightIcon());
        holder.txtRetFlightNo.setText(lflightDetails.getFlightAirline()+"-"+lflightDetails.getFlightReturnFlightNo());

        holder.txtRetDepart.setText(DateUtility.getTimeFromCal(lflightDetails.getFlightRetDepartureTime()));//getTimeFromCal
        holder.txtRetArrive.setText(DateUtility.getTimeFromCal(lflightDetails.getFlightRetArrivalTime()));
        holder.txtRetDuration.setText(lflightDetails.getFlightReturnTotalDuration());

        if(lflightDetails.getFlightreturnTotalStops() == 0){
            holder.txtRetStops.setText("non stop");
        }else{
            holder.txtRetStops.setText(""+lflightDetails.getFlightreturnTotalStops()+" stop");
        }

//        holder.txtRetDepart.setText(DateUtility.getTimeFromCal(flight.get(1).getFlightDepartureTime()).substring(0, 5));//getTimeFromCal
//        holder.txtRetArrive.setText(DateUtility.getTimeFromCal(flight.get(1).getFlightArrivalTime()).substring(0, 5));
//        holder.txtRetDuration.setText(flight.get(1).getFlightDurationTime());

        holder.txtOnwRetFlightPrice.setText(""+lflightDetails.getFlightFare());

//        if(flight.get(0).getFlightStops() == 0){
//
//            holder.txtStops.setText("non stop");
//        }else{
//            holder.txtStops.setText(""+(flight.size()-1));
//        }

        return row;
    }

    class MyHolder {

        ImageView imgOnwflightIcon;
        TextView txtOnwFlightNo;
        TextView txtOnwDepart;
        TextView txtOnwArrive;
        TextView txtOnwDuration;
        TextView txtOnwStops;

        TextView txtOnwRetFlightPrice;

        ImageView imgRetflightIcon;
        TextView txtRetFlightNo;
        TextView txtRetDepart;
        TextView txtRetArrive;
        TextView txtRetDuration;
        TextView txtRetStops;


        public MyHolder(View itemView) {

            imgOnwflightIcon = (ImageView) itemView.findViewById(R.id.imgOnwFlight);
            txtOnwFlightNo = (TextView) itemView.findViewById(R.id.txtOnwFlightNo);
            txtOnwDepart = (TextView) itemView.findViewById(R.id.txtOnwDepart);
            txtOnwArrive = (TextView) itemView.findViewById(R.id.txtOnwArrive);
            txtOnwDuration = (TextView) itemView.findViewById(R.id.txtOnwDuration);
            txtOnwStops = (TextView) itemView.findViewById(R.id.txtOnwStops);

            txtOnwRetFlightPrice = (TextView) itemView.findViewById(R.id.txtOnwRetFare);

            imgRetflightIcon = (ImageView) itemView.findViewById(R.id.imgRetFlight);
            txtRetFlightNo = (TextView) itemView.findViewById(R.id.txtRetFlightNo);
            txtRetDepart = (TextView) itemView.findViewById(R.id.txtRetDepart);
            txtRetArrive = (TextView) itemView.findViewById(R.id.txtRetArrive);
            txtRetDuration = (TextView) itemView.findViewById(R.id.txtRetDuration);
            txtRetStops = (TextView) itemView.findViewById(R.id.txtRetStops);


        }
    }
}
