package com.irctc.air.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.irctc.air.R;
import com.irctc.air.model.FlightOnWardDetailBean;
import com.irctc.air.model.InnerFlightDetailsBeans;
import com.irctc.air.util.AppLogger;
import com.irctc.air.util.DateUtility;

import java.util.ArrayList;

/**
 * Created by tourism on 4/23/2016.
 */
public class ReturnFlightAdapter extends BaseAdapter {

    ArrayList<FlightOnWardDetailBean> mFlightsClone;
    Context mContext;

    public ReturnFlightAdapter(Context context, ArrayList<FlightOnWardDetailBean> lFlightsClone) {
        mContext = context;
        this.mFlightsClone = lFlightsClone;
        AppLogger.enable();
        AppLogger.e("SIZE ", "" + mFlightsClone.size());
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mFlightsClone.size();

    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        MyHolder holder = null;

        if (row == null) {

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row =  inflater.inflate(R.layout.round_trip_list_item,  null);
            holder = new MyHolder(row);
            row.setTag(holder);
        } else {
            holder = (MyHolder) row.getTag();
        }

//

        FlightOnWardDetailBean lflightDetails = mFlightsClone.get(position);
        ArrayList<InnerFlightDetailsBeans> flight = lflightDetails.getFlight();
//
//        holder.IMG_ROUND_FLIGHT_ICON.setImageResource(lflightDetails.getFlightIcon());
//        holder.TXT_ROUND_FLIGHT_NO.setText(lflightDetails.getFlightAirline()+"-"+flight.get(0).getFlightNo());
//        holder.TXT_ROUND_DEPART_TIME.setText(DateUtility.getTimeFromCal(flight.get(0).getFlightDepartureTime()).substring(0, 5));//getTimeFromCal
//        holder.TXT_ROUND_ARRIVE_TIME.setText(DateUtility.getTimeFromCal(flight.get(0).getFlightArrivalTime()).substring(0, 5));
//        holder.TXT_ROUND_DURATION.setText(flight.get(0).getFlightDurationTime());
//        holder.TXT_ROUND_PRICE.setText("\u20B9  " + lflightDetails.getFlightFare());
//
//        if(flight.get(0).getFlightStops() == 0){
//
//            holder.TXT_ROUND_STOPS.setText("non stop");
//        }else{
//            holder.TXT_ROUND_STOPS.setText(""+(flight.size()-1));
//        }

        holder.IMG_ROUND_FLIGHT_ICON.setImageResource(lflightDetails.getFlightIcon());
        holder.TXT_ROUND_FLIGHT_NO.setText(lflightDetails.getFlightAirline()+"-"+flight.get(0).getFlightNo());
        holder.TXT_ROUND_DEPART_TIME.setText(DateUtility.getTimeFromCal(lflightDetails.getFlightOnwDepartureTime()));//getTimeFromCal
        holder.TXT_ROUND_ARRIVE_TIME.setText(DateUtility.getTimeFromCal(lflightDetails.getFlightOnwArrivalTime()));
        holder.TXT_ROUND_DURATION.setText(lflightDetails.getFlightOnwardTotalDuration());
        holder.TXT_ROUND_PRICE.setText("\u20B9  " + lflightDetails.getFlightFare());


        if(lflightDetails.getFlightOnwardTotalStops() == 0){

            holder.TXT_ROUND_STOPS.setText("non stop");
        }else{
            holder.TXT_ROUND_STOPS.setText(""+lflightDetails.getFlightOnwardTotalStops() + " stops");
        }

        return row;
    }

    class MyHolder {

        ImageView  IMG_ROUND_FLIGHT_ICON;
        TextView TXT_ROUND_FLIGHT_NO;
        TextView TXT_ROUND_DEPART_TIME;
        TextView TXT_ROUND_ARRIVE_TIME;
        TextView TXT_ROUND_DURATION;
        TextView TXT_ROUND_STOPS;
        TextView TXT_ROUND_PRICE;

        public MyHolder(View itemView) {

            IMG_ROUND_FLIGHT_ICON = (ImageView) itemView.findViewById(R.id.IMG_ROUND_FLIGHT_ICON);
            TXT_ROUND_FLIGHT_NO = (TextView) itemView.findViewById(R.id.TXT_ROUND_FLIGHT_NO);
            TXT_ROUND_DEPART_TIME = (TextView) itemView.findViewById(R.id.TXT_ROUND_DEPART_TIME);
            TXT_ROUND_ARRIVE_TIME = (TextView) itemView.findViewById(R.id.TXT_ROUND_ARRIVE_TIME);
            TXT_ROUND_DURATION = (TextView) itemView.findViewById(R.id.TXT_ROUND_DURATION);
            TXT_ROUND_STOPS = (TextView) itemView.findViewById(R.id.TXT_ROUND_STOPS);
            TXT_ROUND_PRICE = (TextView) itemView.findViewById(R.id.TXT_ROUND_PRICE);
        }
    }
}