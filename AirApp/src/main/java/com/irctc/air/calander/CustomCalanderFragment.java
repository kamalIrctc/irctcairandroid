package com.irctc.air.calander;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.irctc.air.AppController;
import com.irctc.air.Database.AirDatabase;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.fragment.FragmentAddPassengers;
import com.irctc.air.fragment.FragmentPlanner;
import com.irctc.air.fragment.FragmentTicket;
import com.irctc.air.header.AirHeader;
import com.irctc.air.model.login.PojoLoginUser;
import com.irctc.air.model.low_fare_calendar.LowFareCalModel;
import com.irctc.air.navigationdrawer.SliderMenu;
import com.irctc.air.networking.Networking;
import com.irctc.air.round.trip.domastic.fragment.DFragmentAddPassengers;
import com.irctc.air.round.trip.domastic.fragment.DFragmentTicket;
import com.irctc.air.round.trip.international.fragment.IFragmentAddPassengers;
import com.irctc.air.round.trip.international.fragment.IFragmentTicket;
import com.irctc.air.util.AppKeys;
import com.irctc.air.util.Constant;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.NetworkingUtils;
import com.irctc.air.util.Pref;
import com.irctc.air.util.ProjectUtil;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by tourism on 4/6/2018.
 */

public class CustomCalanderFragment extends Fragment {


    private ActivityMain mainActivity;
    private TextView tvVersion;

    CustomCalendar calendar;
    public static ArrayList<LowFareCalModel.LowFareModel> lowFareList;

    @Deprecated
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mainActivity = (ActivityMain) activity;
        lowFareList= new ArrayList<>();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.custom_calander_layout, null);

        initView(view);

        // Header visibility
        AirHeader.showRecentSearchIcon(false);

        // Show header text
        AirHeader.showHeaderText(mainActivity, true, "Calendar");

        // Show&Hide toggle and drawer
        AirHeader.showDrawerToggleAndToolbar(true, true);

        initData();
        getLowFare();

        return view;
    }

    private void initView(View view) {

        // intializing calendar view
        calendar = (CustomCalendar) view.findViewById(R.id.view);

    }

    private void initData() {
        //calendar view fits in full width of the screen.
        calendar.setFullScreenWidth(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        ActivityMain.activeFragment = Constant.CALENDER_FRAGMENT;
    }


    private void getLowFare() {
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String org = bundle.getString("org");
            String dest = bundle.getString("dest");

            Networking.lowFareCalendar(
                    org,
                    dest,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Gson gson = new Gson();
                            LowFareCalModel lowFareCalModel = gson.fromJson(response.toString(), LowFareCalModel.class);
                            Log.e("Res",new Gson().toJson(lowFareCalModel));
                            NetworkingUtils.dismissProgress();
                            if(lowFareCalModel.getStatus().equals("SUCCESS")){
                                if(lowFareCalModel.getData() !=null || !lowFareCalModel.getData().isEmpty() || lowFareCalModel.getData().size()>0){
                                    lowFareList.clear();
                                    lowFareList.addAll(lowFareCalModel.getData());
                                }else {
                                    Toast.makeText(getActivity(), "Calender data not available", Toast.LENGTH_SHORT).show();
                                }
                                calendar.setMonth(calendar.calendar.get(Calendar.MONTH), calendar.calendar.get(Calendar.YEAR), calendar.calendar.get(Calendar.DAY_OF_MONTH));

                            }else{
                                //Toast.makeText(getContext(), "")
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            NetworkingUtils.internalServerIssue(getActivity().getApplicationContext());

                            NetworkingUtils.dismissProgress();
                            //NetworkingUtils.noInternetAccess(getActivity().getApplicationContext());
                        }
                    });

        }
    }
}
