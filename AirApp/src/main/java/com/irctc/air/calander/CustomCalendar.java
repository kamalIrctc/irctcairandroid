package com.irctc.air.calander;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Point;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/*import com.irctc.air.Database.ServerDateSharedPrefernce;*/
import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.util.DateUtility;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by vamsi on 23/12/15.
 */

public class CustomCalendar extends LinearLayout {

    ActivityMain mainActivity;
    Context context;

    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mRLayoutManager;
    RecyclerView.Adapter mAdapter;

    ImageView ivPreviceMonth, ivNextMonth;

    Calendar calendar;

    TextView tvMonth;

    int iCellSize = 50;

    LinearLayout llMonth, llRecyclear;

    int minMonths = -1;
    int maxMOnths = 13;

    //List<Badge> badges;

    String months[] = {
            "January", "February", "March", "April", "May",
            "June", "July", "August", "September", "October",
            "November", "December"
    };

    TextView tvMonthYear;


    int previousNextMonthCounter;
    int prevCount;


    public CustomCalendar(Context context) {
        super(context);

        this.context = context;
        this.mainActivity = (ActivityMain) context;

        if(mainActivity.isOneWaySelected){

            previousNextMonthCounter = 0;
            minMonths = -1;
        }else {

            if(mainActivity.isClickedDepartDate){
                previousNextMonthCounter = 0;
                minMonths = -1;
            }else {
                previousNextMonthCounter = previousNextMonthCounter + AirDataHolder.getListHolder().getList().get(0).getOnewayMonthCounter();
                minMonths = previousNextMonthCounter - 1;
            }
        }
        init();

    }


    public CustomCalendar(Context context, AttributeSet attrs) {
        super(context, attrs);


        this.context = context;
        this.mainActivity = (ActivityMain) context;

        if(mainActivity.isOneWaySelected){

            previousNextMonthCounter = 0;
            minMonths = -1;
        }else {

            if(mainActivity.isClickedDepartDate){
                previousNextMonthCounter = 0;
                minMonths = -1;
            }else {
                previousNextMonthCounter = previousNextMonthCounter + AirDataHolder.getListHolder().getList().get(0).getOnewayMonthCounter();
                minMonths = previousNextMonthCounter - 1;
            }
        }
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomCalendar, 0, 0);
        iCellSize = a.getInt(R.styleable.CustomCalendar_cellSize, 50);

        a.recycle();

        init();

    }

    private void init() {

        inflate(getContext(), R.layout.calenderview, this);

        llMonth = (LinearLayout) findViewById(R.id.llMonth);
        llRecyclear = (LinearLayout) findViewById(R.id.llMyRecyclearView);

        tvMonthYear = (TextView) findViewById(R.id.tvMonthYear);

        llMonth.setLayoutParams(new LayoutParams(iCellSize * 7, LayoutParams.WRAP_CONTENT));
        llRecyclear.setLayoutParams(new LayoutParams(iCellSize * 7, iCellSize * 7));

        calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);


        setFirstLastMonthInfo();


        mRecyclerView = (RecyclerView) findViewById(R.id.rView);
        this.mRLayoutManager = new GridLayoutManager(context, 7);
        mRecyclerView.setLayoutManager(mRLayoutManager);

        if(mainActivity.isOneWaySelected){

//            String curDate = new ServerDateSharedPrefernce(context).getServerDate();//new SimpleDateFormat(DateUtility.lStrGMTDateFormater).format(new Date());
            DateFormat dateFormat = new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy");
            Date date = new Date();
            String curDate = dateFormat.format(date);
            String[] dateArray =  curDate.split(" ");
            String formatedDateWithZeroHourTime = dateArray[0] +" "+dateArray[1]+" "+dateArray[2]+" 00:00:00 "+dateArray[4]+" "+dateArray[5];

            calendar = setDateInCalendar(formatedDateWithZeroHourTime);

            setMonth(calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR), calendar.get(Calendar.DAY_OF_MONTH));

        }else{


            if(mainActivity.isClickedDepartDate) {

                //String curDate = new ServerDateSharedPrefernce(context).getServerDate();//new SimpleDateFormat(DateUtility.lStrGMTDateFormater).format(new Date());
                DateFormat dateFormat = new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy");
                Date date = new Date();
                String curDate = dateFormat.format(date);
                String[] dateArray =  curDate.split(" ");
                String formatedDateWithZeroHourTime = dateArray[0] +" "+dateArray[1]+" "+dateArray[2]+" 00:00:00 "+dateArray[4]+" "+dateArray[5];

                calendar = setDateInCalendar(formatedDateWithZeroHourTime);

                setMonth(calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR), calendar.get(Calendar.DAY_OF_MONTH));

            }else {
                calendar = setDateInCalendar(AirDataHolder.getListHolder().getList().get(0).getDepDate());

                setMonth(calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR), calendar.get(Calendar.DAY_OF_MONTH));

            }
        }

//        setMonth(calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR), calendar.get(Calendar.DAY_OF_MONTH));


        ivPreviceMonth = (ImageView) findViewById(R.id.ivPrevesMonth);
        ivNextMonth = (ImageView) findViewById(R.id.ivNextMonth);

        tvMonth = (TextView) findViewById(R.id.tvMonthYear);

        tvMonth.setText("" + months[calendar.get(Calendar.MONTH)] + " " + calendar.get(Calendar.YEAR));


        ivPreviceMonth.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                previousNextMonthCounter--;

                // 0 for previous month
                if(isNextMonthAvailable(0)) {

                    if(mainActivity.isOneWaySelected){
                        AirDataHolder.getListHolder().getList().get(0).setOnewayMonthCounter(previousNextMonthCounter);
                    }
                    else{

                        if(mainActivity.isClickedDepartDate){

                            AirDataHolder.getListHolder().getList().get(0).setOnewayMonthCounter(previousNextMonthCounter);
                        }
                    }
                    calendar.set(Calendar.MONTH, (calendar.get(Calendar.MONTH) - 1));

                    int month = calendar.get(Calendar.MONTH);

                    tvMonth.setText("" + months[month] + " " + calendar.get(Calendar.YEAR));


                    setMonth(month, calendar.get(Calendar.YEAR), calendar.get(Calendar.DAY_OF_MONTH));

                }
                else{

                    // here to stop user to go previous month from the current month
                    //TODO : TO SHOW THE DIALOG FOR USER CAN NOT BOOK TICKET PAST MONTHS
                    Toast.makeText(context, "min Month", Toast.LENGTH_SHORT).show();

                }
            }
        });


        ivNextMonth.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                previousNextMonthCounter++;

                // 1 for next month

                if(isNextMonthAvailable(1)) {

                    if(mainActivity.isOneWaySelected){
                        AirDataHolder.getListHolder().getList().get(0).setOnewayMonthCounter(previousNextMonthCounter);
                    }
                    else{

                        if(mainActivity.isClickedDepartDate){

                            AirDataHolder.getListHolder().getList().get(0).setOnewayMonthCounter(previousNextMonthCounter);
                        }
                    }
                    calendar.set(Calendar.MONTH, (calendar.get(Calendar.MONTH) + 1));

                    int month = calendar.get(Calendar.MONTH);

                    tvMonth.setText("" + months[calendar.get(Calendar.MONTH)] + " " + calendar.get(Calendar.YEAR));

                    setMonth(month, calendar.get(Calendar.YEAR), calendar.get(Calendar.DAY_OF_MONTH));

                }else{

                    // here to stop user to go next month after one year
                    //TODO : TO SHOW THE DIALOG FOR USER CAN BOOK TICKET TILL ONE YEAR
                    Toast.makeText(context, "max Month", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private Calendar setDateInCalendar(String dateStr) { //Thu Apr 06 00:00:00 GMT+05:30 2017

        String date[] = dateStr.split(" ");
        DateUtility.getDate(dateStr)   ;

        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.MONTH, (DateUtility.monthInInteger(date[1])));
        calendar.set(Calendar.YEAR, Integer.parseInt(date[5]));

        calendar.set(Calendar.DAY_OF_MONTH, 1);

//        if(mainActivity.isOneWaySelected) {
//
//            calendar.set(Calendar.DAY_OF_MONTH, 1);
//        }
//        else{
//          calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date[2]));
//
//        }
        return calendar;
    }

    private boolean isNextMonthAvailable(int monthType) {

        switch (monthType){

            // FOR PREVIOUS MONTH
            case 0:

                if(previousNextMonthCounter == minMonths){

                    if(mainActivity.isOneWaySelected) {
                        previousNextMonthCounter = 0;
                    }else{
                        previousNextMonthCounter = minMonths+1;

                    }

                }else {

                    return true;
                }
                break;

            // FOR NEXT MONTH
            case 1:

//                if(mainActivity.isOneWaySelected){
//
//                }else{
//
//                    if(AirDataHolder.getListHolder().getList().get(0).getOnewayMonthCounter() != 0);
//                    previousNextMonthCounter = previousNextMonthCounter + AirDataHolder.getListHolder().getList().get(0).getOnewayMonthCounter();
//                    AirDataHolder.getListHolder().getList().get(0).setOnewayMonthCounter(0);
//                }

                if(previousNextMonthCounter == maxMOnths){

                    previousNextMonthCounter = maxMOnths-1;

                }else {

                    return true;
                }
                break;

            default:
                break;
        }

        return false;
    }


    public void setMonth(int month, int year, int toDay) {

        int dayMax = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        int firstDay = calendar.get(Calendar.DAY_OF_WEEK);

        this.mAdapter = new CustomCalendarAdapter(context, dayMax, firstDay, month, year, toDay, iCellSize);
        this.mRecyclerView.setAdapter(mAdapter);

    }

//    //this method is for handling date onClickListener
//    public static ClickInterface staticClickInterface() {
//
//        if (clickDate != null)
//            return clickDate;
//        else
//            return null;
//
//    }
//
//    //this method is for implementing Date onClickListener
//    public void setOnClickDate(ClickInterface clickDate) {
//
//        this.clickDate = clickDate;
//
//    }


    // month textview specifications
    public void setMonthTextSize(float sizeInSP) {

        tvMonthYear.setTextSize(pixelsToSp(sizeInSP));

    }

    public void setMonthTextColor(int color) {

        tvMonthYear.setTextColor(color);

    }



//    public void setBadgeDateList(List<Badge> badges) {
//
//        this.badges = badges;
//
//        setMonth(calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR), calendar.get(Calendar.DAY_OF_MONTH));
//
//    }



//    private List<Badge> badgsMonth(int month) {
//
//        List<Badge> monthBadge = new ArrayList<>();
//        if (badges != null) {
//            for (int i = 0; i < badges.size(); i++) {
//                if (badges.get(i).getMonth() == (month + 1)) {
//                    monthBadge.add(badges.get(i));
//                }
//            }
//        }
//
//        return monthBadge;
//    }


    public void setFullScreenWidth(boolean isFullScreen) {

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;

        if (isFullScreen) {
            iCellSize = (width) / 7 ;
            llMonth.setLayoutParams(new LayoutParams(iCellSize * 7, LayoutParams.WRAP_CONTENT));
            llRecyclear.setLayoutParams(new LayoutParams(iCellSize * 7, iCellSize * 7));
            setMonth(calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR), calendar.get(Calendar.DAY_OF_MONTH));


        }
    }


    private float pixelsToSp(float px) {
        float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
        return px/scaledDensity;
    }


    /*  ASIF
     * 4 Mar 2017
     * To Set the first
     * And last month info
     * */
    private void setFirstLastMonthInfo() {

        if(mainActivity.isOneWaySelected){
            // FOR ONEWAY TRIP

            // HERE SET THE FIRST MONTH FOR ONEWAY TRIP

            Calendar calendarOneWay = Calendar.getInstance();

            CalenderDate firstMonth = new CalenderDate();

            firstMonth.setDay(calendarOneWay.get(Calendar.DAY_OF_MONTH));
            firstMonth.setMonth(calendarOneWay.get(Calendar.MONTH));
            firstMonth.setYear(calendarOneWay.get(Calendar.YEAR));

            AirDataHolder.getListHolder().getList().get(0).setFirstMonthInfo(firstMonth);


            // HERE SET THE LAST MONTH FOR ONEWAY TRIP

            calendarOneWay.add(Calendar.YEAR, 1);
            CalenderDate lastMonth = new CalenderDate();

            lastMonth.setDay(calendarOneWay.get(Calendar.DAY_OF_MONTH));
            lastMonth.setMonth(calendarOneWay.get(Calendar.MONTH));
            lastMonth.setYear(calendarOneWay.get(Calendar.YEAR));

            AirDataHolder.getListHolder().getList().get(0).setLastMonthInfo(lastMonth);

        }else{
            // FOR ROUND TRIP
            // HERE SET THE FIRST MONTH FOR ROUND TRIP

            if(mainActivity.isClickedDepartDate){

                Calendar calendarOneWay = Calendar.getInstance();

                CalenderDate firstMonth = new CalenderDate();

                firstMonth.setDay(calendarOneWay.get(Calendar.DAY_OF_MONTH));
                firstMonth.setMonth(calendarOneWay.get(Calendar.MONTH));
                firstMonth.setYear(calendarOneWay.get(Calendar.YEAR));

                AirDataHolder.getListHolder().getList().get(0).setFirstMonthInfo(firstMonth);


                // HERE SET THE LAST MONTH FOR ONEWAY TRIP

                calendarOneWay.add(Calendar.YEAR, 1);
                CalenderDate lastMonth = new CalenderDate();

                lastMonth.setDay(calendarOneWay.get(Calendar.DAY_OF_MONTH));
                lastMonth.setMonth(calendarOneWay.get(Calendar.MONTH));
                lastMonth.setYear(calendarOneWay.get(Calendar.YEAR));

                AirDataHolder.getListHolder().getList().get(0).setLastMonthInfo(lastMonth);

            }else {
                String depDate = AirDataHolder.getListHolder().getList().get(0).getDepDate();

                Calendar calendarRet = setDateInCalendar(depDate);

                CalenderDate firstMonth = new CalenderDate();

                firstMonth.setDay(Integer.parseInt(depDate.split(" ")[2]));       //    calendarRet.get(Calendar.DAY_OF_MONTH)
                firstMonth.setMonth(calendarRet.get(Calendar.MONTH));
                firstMonth.setYear(calendarRet.get(Calendar.YEAR));

                AirDataHolder.getListHolder().getList().get(0).setFirstMonthInfo(firstMonth);


                // HERE SET THE LAST MONTH FOR ROUND TRIP
                Calendar calendar = Calendar.getInstance();

                //String curDate = new ServerDateSharedPrefernce(context).getServerDate();//new SimpleDateFormat(DateUtility.lStrGMTDateFormater).format(new Date());
                DateFormat dateFormat = new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy");
                Date date = new Date();
                String curDate = dateFormat.format(date);
                String[] dateArray = curDate.split(" ");
                String formatedDateWithZeroHourTime = dateArray[0] + " " + dateArray[1] + " " + dateArray[2] + " 00:00:00 " + dateArray[4] + " " + dateArray[5];
                Date currentDate = DateUtility.getDateInFormat(formatedDateWithZeroHourTime);

                calendar.setTime(currentDate);

                calendar.add(Calendar.YEAR, 1);
                CalenderDate lastMonth = new CalenderDate();

                lastMonth.setDay(calendar.get(Calendar.DAY_OF_MONTH));
                lastMonth.setMonth(calendar.get(Calendar.MONTH));
                lastMonth.setYear(calendar.get(Calendar.YEAR));

                AirDataHolder.getListHolder().getList().get(0).setLastMonthInfo(lastMonth);
            }
        }

    }

}
