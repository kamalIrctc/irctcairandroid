package com.irctc.air.calander;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/*import com.irctc.air.Database.ServerDateSharedPrefernce;*/
import com.google.gson.Gson;
import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.fragment.FragmentPlanner;
import com.irctc.air.model.AirDataModel;
import com.irctc.air.util.AppKeys;
import com.irctc.air.util.DateUtility;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.Pref;
import com.irctc.air.util.ProjectUtil;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


/**
 * Created by vamsi on 26/10/15.
 */

public class CustomCalendarAdapter extends RecyclerView.Adapter<CustomCalendarAdapter.ViewHolder> {

    ActivityMain mainActivity;
    Context context;
    int maxDay, firstDay, month, year, tDay;
    String[] day = {"Sun", "Mon", "Thu", "Wed", "Thr", "Fri", "Sat"};

    int cellSize = 50;

    int hideSquareCounter;

    AirDataModel calanderDataModel;
    //int lastSelectedPos = 0;

    ViewHolder preSelectHolder;
    int preSelectPosition;
    boolean isValidDate = true;

    boolean preSelect;

    public static CustomCalendarAdapter instance;
    private ViewHolder viewHolder;
    private int position;

    public CustomCalendarAdapter() {
        //instance = new CustomCalendarAdapter();
    }

    public CustomCalendarAdapter(Context context, int maxDay, int firstDay, int month, int year, int tDay, int cellSize) {

        this.mainActivity = (ActivityMain) context;
        this.context = context;
        this.maxDay = maxDay;
        this.firstDay = firstDay;
        this.month = month;
        this.year = year;
        this.tDay = tDay;
        this.cellSize = cellSize;
        instance = new CustomCalendarAdapter();


        calanderDataModel = AirDataHolder.getListHolder().getList().get(0);

        if (month == calanderDataModel.getFirstMonthInfo().getMonth()) {

            preSelect = true;

        } else {

            preSelect = false;

        }
    }

    public static CustomCalendarAdapter getInstance() {
        return instance;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_calender_cell, parent, false);

        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        this.viewHolder = holder;
        this.position = position;

        holder.tvBadge.setVisibility(View.GONE);

        // FOR SUNDAYS
        if (position % 7 == 0)
            holder.tvDate.setTextColor(Color.RED);


        // FOR DAYS NAME (Sun, Mon, Tue.....)
        if (position < 7) {
            // asif
            holder.tvDate.setText("" + day[position]);
            holder.tvFare.setVisibility(View.GONE);
            // holder.llCell.setVisibility(View.GONE);

        }
        // FOR DAY OF MONTH
        else if (position > (7 + firstDay - 2)) {


            if (month == calanderDataModel.getFirstMonthInfo().getMonth() && year == calanderDataModel.getFirstMonthInfo().getYear()) {
                //  FOR DAYS OF CURRENT MONTH OF YEAR

                if (position - 7 - hideSquareCounter == calanderDataModel.getFirstMonthInfo().getDay() - 1) {

                    // SET THE CURRENT DATE OF CURRENT MONTH
                    preSelectPosition = position;
                    preSelectHolder = holder;
                    int day = (position - (7 + firstDay - 2));
                    holder.tvDate.setText("" + day);

                    for (int i = 0; i < CustomCalanderFragment.lowFareList.size(); i++) {
                        if (CustomCalanderFragment.lowFareList.get(i).getDate().equals(DateUtility.convertDateForLowFareCalendar("" + day + "/" + (month + 1) + "/" + year).toString())) {
                            holder.tvFare.setText(context.getString(R.string.RUPEE_SYMBOL) + CustomCalanderFragment.lowFareList.get(i).getPrice());
                        }
                    }
                    //holder.tvDate.setTextColor(Color.WHITE);
                    // holder.tvDate.setText("" + calanderDataModel.getFirstMonthInfo().getDay());
                    holder.llCell.setBackgroundResource(R.drawable.button_round_shape_both_corner_blue_bg);

                } else {

                    // SET THE PAST DATE OF CURRENT MONTH
                    if (position - 7 - hideSquareCounter < calanderDataModel.getFirstMonthInfo().getDay() - 1) {

                        holder.llCell.setClickable(false);
                        holder.llCell.setEnabled(false);

                        int day = (position - (7 + firstDay - 2));
                        holder.tvDate.setText("" + day);
                        /*for (int i = 0; i < CustomCalanderFragment.lowFareList.size(); i++) {
                            if (CustomCalanderFragment.lowFareList.get(i).getDate().equals(DateUtility.convertDateForLowFareCalendar("" + day + "/" + (month + 1) + "/" + year).toString())) {
                                holder.tvFare.setText(context.getString(R.string.RUPEE_SYMBOL) + CustomCalanderFragment.lowFareList.get(i).getPrice());
                            }
                        }*/
                        holder.llCell.setBackgroundResource(R.drawable.button_round_shape_both_corner_gray_bg);

                    } else {
                        // SET THE FUTURE DATE OF CURRENT MONTH
                        int day = (position - (7 + firstDay - 2));
                        holder.tvDate.setText("" + day);
                        for (int i = 0; i < CustomCalanderFragment.lowFareList.size(); i++) {
                            if (CustomCalanderFragment.lowFareList.get(i).getDate().equals(DateUtility.convertDateForLowFareCalendar("" + day + "/" + (month + 1) + "/" + year).toString())) {
                                holder.tvFare.setText(context.getString(R.string.RUPEE_SYMBOL) + CustomCalanderFragment.lowFareList.get(i).getPrice());
                            }
                        }
                        holder.llCell.setBackgroundResource(R.drawable.button_round_shape_both_corner_white_bg);
                    }

                }

            } else if (month == calanderDataModel.getLastMonthInfo().getMonth() && year == calanderDataModel.getLastMonthInfo().getYear()) {
                //  FOR DAYS OF LAST MONTH OF YEAR

                // SET THE PAST DATE OF CURRENT MONTH
                if (position - 7 - hideSquareCounter > calanderDataModel.getLastMonthInfo().getDay() - 1) {

                    int day = (position - (7 + firstDay - 2));
                    holder.tvDate.setText("" + day);
                    for (int i = 0; i < CustomCalanderFragment.lowFareList.size(); i++) {
                        if (CustomCalanderFragment.lowFareList.get(i).getDate().equals(DateUtility.convertDateForLowFareCalendar("" + day + "/" + (month + 1) + "/" + year).toString())) {
                            holder.tvFare.setText(context.getString(R.string.RUPEE_SYMBOL) + CustomCalanderFragment.lowFareList.get(i).getPrice());
                        }
                    }
                    holder.llCell.setBackgroundResource(R.drawable.button_round_shape_both_corner_gray_bg);

                } else {
                    // SET THE FUTURE DATE OF CURRENT MONTH
                    int day = (position - (7 + firstDay - 2));
                    holder.tvDate.setText("" + day);
                    for (int i = 0; i < CustomCalanderFragment.lowFareList.size(); i++) {
                        if (CustomCalanderFragment.lowFareList.get(i).getDate().equals(DateUtility.convertDateForLowFareCalendar("" + day + "/" + (month + 1) + "/" + year).toString())) {
                            holder.tvFare.setText(context.getString(R.string.RUPEE_SYMBOL) + CustomCalanderFragment.lowFareList.get(i).getPrice());
                        }
                    }
                    holder.llCell.setBackgroundResource(R.drawable.button_round_shape_both_corner_white_bg);

                }

            } else {
                //  FOR DAYS OF MONTH BETWEEN FIRST AND LAST MONTH OF YEAR

                int toDay = (position - (7 + firstDay - 2));
                holder.tvDate.setText("" + toDay);
                for (int i = 0; i < CustomCalanderFragment.lowFareList.size(); i++) {
                    if (CustomCalanderFragment.lowFareList.get(i).getDate().equals(DateUtility.convertDateForLowFareCalendar("" + toDay + "/" + (month + 1) + "/" + year).toString())) {
                        holder.tvFare.setText(context.getString(R.string.RUPEE_SYMBOL) + CustomCalanderFragment.lowFareList.get(i).getPrice());
                    }
                }
                holder.llCell.setBackgroundResource(R.drawable.button_round_shape_both_corner_white_bg);

            }


//            if(position-7- hideSquareCounter == 15 ){
//
//                holder.llCell.setBackgroundResource(R.drawable.button_round_shape_both_corner_blue_bg);
//
//            }else {
//
//                holder.llCell.setBackgroundResource(R.drawable.button_round_shape_both_corner_white_bg);
//                int toDay = (position - (7 + firstDay - 2));
//                holder.tvDate.setText("" + toDay);
//            }
        }
        // FOR HIDING THE DAY OF MONTH
        else {

            // to hide the blank item
            // holder.llCell.setVisibility(View.GONE);
            // asif

            hideSquareCounter++;
            holder.tvDate.setText("");
            holder.tvFare.setText("");
            holder.tvFare.setVisibility(View.GONE);
        }


        holder.llCell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (position > (7 + firstDay - 2)) {


                    if (month == calanderDataModel.getFirstMonthInfo().getMonth() && year == calanderDataModel.getFirstMonthInfo().getYear()) {
                        //  FOR DAYS OF CURRENT MONTH OF YEAR

                        if (position - 7 - hideSquareCounter == calanderDataModel.getFirstMonthInfo().getDay() - 1) {

                            // SET THE CURRENT DATE OF CURRENT MONTH

                            if (preSelectPosition % 7 == 0)
                                preSelectHolder.tvDate.setTextColor(Color.RED);
                            else
                                preSelectHolder.tvDate.setTextColor(Color.BLACK);

                            holder.tvDate.setTextColor(Color.WHITE);
                            preSelectHolder.llCell.setBackgroundResource(R.drawable.button_round_shape_both_corner_white_bg);
                            holder.llCell.setBackgroundResource(R.drawable.button_round_shape_both_corner_blue_bg);
                            preSelectHolder = holder;
                            preSelectPosition = position;
                            isValidDate = true;

                        } else {

                            // SET THE PAST DATE OF CURRENT MONTH
                            if (position - 7 - hideSquareCounter < calanderDataModel.getFirstMonthInfo().getDay() - 1) {

                                // TODO: SHOW ERROR MESSAGE THIS DATE IS NOT AVAILABLE
                                holder.llCell.setClickable(false);
                                holder.llCell.setEnabled(false);
                                isValidDate = false;
                            } else {
                                // SET THE FUTURE DATE OF CURRENT MONTH
                                if (preSelectPosition % 7 == 0)
                                    preSelectHolder.tvDate.setTextColor(Color.RED);
                                else
                                    preSelectHolder.tvDate.setTextColor(Color.BLACK);

                                holder.tvDate.setTextColor(Color.WHITE);
                                preSelectHolder.llCell.setBackgroundResource(R.drawable.button_round_shape_both_corner_white_bg);
                                holder.llCell.setBackgroundResource(R.drawable.button_round_shape_both_corner_blue_bg);
                                preSelectHolder = holder;
                                preSelectPosition = position;
                                isValidDate = true;

                            }

                        }

                    } else if (month == calanderDataModel.getLastMonthInfo().getMonth() && year == calanderDataModel.getLastMonthInfo().getYear()) {
                        //  FOR DAYS OF LAST MONTH OF YEAR

                        // SET THE PAST DATE OF CURRENT MONTH
                        if (position - 7 - hideSquareCounter > calanderDataModel.getLastMonthInfo().getDay() - 1) {

                            // TODO: SHOW ERROR MESSAGE THIS DATE IS NOT AVAILABLE
                            holder.llCell.setClickable(false);
                            holder.llCell.setEnabled(false);
                            isValidDate = false;
                        } else {

                            // ENABLE THE FUTURE DATE OF LAST MONTH OF THE YEAR

                            if (preSelectHolder == null) {
                                preSelectHolder = holder;
                                preSelectHolder.llCell.setBackgroundResource(R.drawable.button_round_shape_both_corner_blue_bg);
                                preSelectHolder.tvDate.setTextColor(Color.WHITE);
                                isValidDate = true;

                            } else {

                                if (preSelectPosition % 7 == 0)
                                    preSelectHolder.tvDate.setTextColor(Color.RED);
                                else
                                    preSelectHolder.tvDate.setTextColor(Color.BLACK);

                                holder.tvDate.setTextColor(Color.WHITE);
                                preSelectHolder.llCell.setBackgroundResource(R.drawable.button_round_shape_both_corner_white_bg);
                                holder.llCell.setBackgroundResource(R.drawable.button_round_shape_both_corner_blue_bg);
                                preSelectHolder = holder;
                                preSelectPosition = position;
                                isValidDate = true;

                            }
                        }

                    } else {
                        //  FOR DAYS OF MONTH BETWEEN FIRST AND LAST MONTH OF YEAR

                        if (preSelectHolder == null) {
                            preSelectHolder = holder;
                            preSelectPosition = position;
                            preSelectHolder.llCell.setBackgroundResource(R.drawable.button_round_shape_both_corner_blue_bg);
                            preSelectHolder.tvDate.setTextColor(Color.WHITE);

                        } else {

                            if (preSelectPosition % 7 == 0)
                                preSelectHolder.tvDate.setTextColor(Color.RED);
                            else
                                preSelectHolder.tvDate.setTextColor(Color.BLACK);

                            preSelectHolder.llCell.setBackgroundResource(R.drawable.button_round_shape_both_corner_white_bg);
                            holder.llCell.setBackgroundResource(R.drawable.button_round_shape_both_corner_blue_bg);


                            holder.tvDate.setTextColor(Color.WHITE);

                            preSelectHolder = holder;
                            preSelectPosition = position;
                        }

                    }

                    if (isValidDate) {

                        setOnwRetDateInHolder(position);
                        mainActivity.isClickedDepartDate = false;

                        isDateSelectFromCurrentMonth();
                    } else {

                        Toast.makeText(context, "Please select the right date.", Toast.LENGTH_SHORT).show();
                    }

//                    CalanderDataHolder.getListHolder().getList().get(0).setOnwardDate(""+toDay+"/"+(month + 1)+"/"+year);
//                    CalanderDataHolder.getListHolder().getList().get(0).setReturnDate(""+toDay+"/"+(month + 1)+"/"+year);
//                    CalanderDataHolder.getListHolder().getList().get(0).setOnwardSelect(true);


//                    holder.llCell.setBackgroundResource(R.drawable.button_round_shape_both_corner_blue_bg);
//
//
//                    int toDay = (position - (7 + firstDay - 2));
//
//                    CalenderDate date = new CalenderDate();
//
//                    date.setDay(toDay);
//                    date.setMonth(month + 1);
//                    date.setYear(year);
//
//
//                    if (CustomCalendar.staticClickInterface() != null)
//                        CustomCalendar.staticClickInterface().setDateClicked(date);
                }
            }
        });

        /*int toDay = (position - (7 + firstDay - 2));
        for (int i = 0; i < CustomCalanderFragment.lowFareList.size(); i++) {
            if (CustomCalanderFragment.lowFareList.get(i).getDate().equals(DateUtility.convertDateForLowFareCalendar("" + toDay + "/" + (month + 1) + "/" + year).toString())) {
                holder.tvFare.setText(context.getString(R.string.RUPEE_SYMBOL) + CustomCalanderFragment.lowFareList.get(i).getPrice());
            }
        }*/
    }

    private void isDateSelectFromCurrentMonth() {

        // String curDate = new ServerDateSharedPrefernce(context).getServerDate();//new SimpleDateFormat(DateUtility.lStrGMTDateFormater).format(new Date());
        DateFormat dateFormat = new SimpleDateFormat("E MMM dd HH:mm:ss z yyyy");
        Date date = new Date();
        String curDate = dateFormat.format(date);
        String[] dateArray = curDate.split(" ");
        String formatedDateWithZeroHourTime = dateArray[0] + " " + dateArray[1] + " " + dateArray[2] + " 00:00:00 " + dateArray[4] + " " + dateArray[5];
        Date currentDate = DateUtility.getDateInFormat(formatedDateWithZeroHourTime);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currentDate);

        if (month == calendar.get(Calendar.MONTH)) {
            AirDataHolder.getListHolder().getList().get(0).setOnewayMonthCounter(0);
        }

    }

    private void setOnwRetDateInHolder(int position) {

        int toDay = (position - (7 + firstDay - 2));

        if (mainActivity.isOneWaySelected) {


            AirDataHolder.getListHolder().getList().get(0).setReturnDate(DateUtility.convertDate("" + toDay + "/" + (month + 1) + "/" + year).toString());
            AirDataHolder.getListHolder().getList().get(0).setDepDate(DateUtility.convertDate("" + toDay + "/" + (month + 1) + "/" + year).toString());

        } else {

            if (mainActivity.isClickedDepartDate) {
                AirDataHolder.getListHolder().getList().get(0).setDepDate(DateUtility.convertDate("" + toDay + "/" + (month + 1) + "/" + year).toString());

            } else {
                AirDataHolder.getListHolder().getList().get(0).setReturnDate(DateUtility.convertDate("" + toDay + "/" + (month + 1) + "/" + year).toString());
            }
        }
        ProjectUtil.replaceFragment(context, new FragmentPlanner(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);


    }


    @Override
    public int getItemCount() {
        return maxDay + 7 + firstDay - 1;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        protected TextView tvDate;
        protected TextView tvFare;
        protected TextView tvBadge;
        protected LinearLayout llCell;

        public ViewHolder(View view) {
            super(view);

            tvDate = (TextView) view.findViewById(R.id.tvDate);       //for date of month
            tvFare = (TextView) view.findViewById(R.id.tvFare);       //for fare
            tvBadge = (TextView) view.findViewById(R.id.tvBadge);     //for badge on date
            llCell = (LinearLayout) view.findViewById(R.id.llCell);   //single date layout

            llCell.setLayoutParams(new LinearLayout.LayoutParams(cellSize - 7, cellSize - 10)); //adjusting width and height for single date layout

        }
    }

}

