package com.irctc.air.header;

import android.content.Context;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.navigationdrawer.SliderMenu;
import com.irctc.air.util.AppKeys;
import com.irctc.air.util.DateUtility;
import com.irctc.air.util.Pref;

/**
 * Created by vivek on 04/13/2016
 */
public class AirHeader {


    static Toolbar toolBar;
    static ActionBarDrawerToggle actionBarDrawerToggle;
    public static DrawerLayout drawerLayout;
    static AppCompatActivity lObjAppCompatActivity;
    private static LinearLayout llNavSlider;
    public static final int LOCK_MODE_LOCKED = 1;
    public static final int LOCK_MODE_UNLOCKED = 0;

    public static void createHeader(final Context ctx, String headerTitle) {

        lObjAppCompatActivity = ((AppCompatActivity) ctx);
        toolBar = (Toolbar) lObjAppCompatActivity.findViewById(R.id.toolbar);




        //After instantiating your ActionBarDrawerToggle
        // actionBarDrawerToggle.setDrawerIndicatorEnabled(false);

        // lObjAppCompatActivity.getSupportActionBar().setIcon(R.drawable.images);

        drawerLayout = (DrawerLayout) lObjAppCompatActivity.findViewById(R.id.drawer_layout);
        llNavSlider = (LinearLayout) lObjAppCompatActivity.findViewById(R.id.ll_nav_slider);
        SliderMenu.initNavigationDrawer(lObjAppCompatActivity, drawerLayout);
        lObjAppCompatActivity.setSupportActionBar(toolBar);

        lObjAppCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        lObjAppCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        // lObjAppCompatActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.adult48);

        lObjAppCompatActivity.getSupportActionBar().setDisplayShowCustomEnabled(true); // enable overriding the default toolbar layout

        toolBar.setTitleTextColor(ctx.getResources().getColor(R.color.colorPrimaryDark));
        actionBarDrawerToggle = new ActionBarDrawerToggle(lObjAppCompatActivity, drawerLayout, toolBar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                ActivityMain.flagIsSliderOpen = true;

                if (Pref.getString(ctx, AppKeys.USER_DETAIL_FIRST_NAME).equals("")) {
                    SliderMenu.txtUserId.setText("Guest");
                } else {
                    SliderMenu.txtUserId.setText(Pref.getString(ctx, AppKeys.USER_DETAIL_FIRST_NAME));
                }

                // code here will execute once the drawer is opened( As I dont want anything happened whe drawer is
                // open I am not going to put anything here)
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                ActivityMain.flagIsSliderOpen = false;
                //Log.d(TAG, "onDrawerClosed: " + getTitle());
                // Code here will execute once drawer is closed
            }
        };
        // Drawer Toggle Object Made
        drawerLayout.setDrawerListener(actionBarDrawerToggle); // Drawer Listener set to the Drawer toggle
        actionBarDrawerToggle.syncState();               // Finally we set the drawer toggle sync State


        //actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
        //lObjAppCompatActivity.getSupportActionBar().hide();

    }

    public static void showDrawerToggleAndToolbar(boolean showDrawerAndToggle, boolean showToolbar) {

        if (showDrawerAndToggle) {
            actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
            // Hide drawer
            drawerLayout.setDrawerLockMode(LOCK_MODE_UNLOCKED);
        } else{
            // HIDE TOGGLE BTN
            actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
            // Hide drawer
            drawerLayout.setDrawerLockMode(LOCK_MODE_LOCKED);
        }

        if (showToolbar) {
            // show TOOLBAR
            toolBar.setVisibility(View.VISIBLE);
        } else{
            // HIDE TOOLBAR
            toolBar.setVisibility(View.GONE);
        }
    }


//    public static void showRecentSearchIcon(int fragmentNumber, boolean isVisible) {
//        ////// CUSTOM ICON /////
//        ImageView imageView = (ImageView) lObjAppCompatActivity.findViewById(R.id.RECENT_SEARCH);
//        if(isVisible){
//            imageView.setVisibility(View.VISIBLE);
//        }else {
//            imageView.setVisibility(View.GONE);
//        }
//
//        imageView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                // FragmentNumber will be used to make cases here
//                Toast.makeText(lObjAppCompatActivity, "Yet to work", Toast.LENGTH_SHORT).show();
//
//
//
//            }
//        });
//        /////////////////////////
//    }

    public static void showRecentSearchIcon(boolean isVisible) {
        ////// CUSTOM ICON /////
        if(isVisible){
            lObjAppCompatActivity.findViewById(R.id.RECENT_SEARCH).setVisibility(View.VISIBLE);
        }else {
            lObjAppCompatActivity.findViewById(R.id.RECENT_SEARCH).setVisibility(View.GONE);
        }

    }


    public static void showHeaderText(ActivityMain mainActivity, boolean isNormalHeader, String normalHeaderText){

        TextView textView = (TextView) lObjAppCompatActivity.findViewById(R.id.toolbar_title);
        LinearLayout toolBarLayoutMain = (LinearLayout) lObjAppCompatActivity.findViewById(R.id.TOOLBAR_DETAIL_LAYOUT);

        if(isNormalHeader) {
            ////// CUSTOM TITLE Default layout one /////
            textView.setVisibility(View.VISIBLE);
            toolBarLayoutMain.setVisibility(View.GONE);
            textView.setText(normalHeaderText);
        }else {
            ////// CUSTOM TITLE Default layout two /////
            textView.setVisibility(View.GONE);
            toolBarLayoutMain.setVisibility(View.VISIBLE);

            TextView txtFromStation = (TextView) lObjAppCompatActivity.findViewById(R.id.SUB_TOOLBAR_FROM);
            TextView txtToStation = (TextView) lObjAppCompatActivity.findViewById(R.id.SUB_TOOLBAR_TO);
            ImageView imgArrow = (ImageView) lObjAppCompatActivity.findViewById(R.id.IMAGE_ARROW);
            TextView txtTravellerDetail = (TextView) lObjAppCompatActivity.findViewById(R.id.SUB_TOOLBAR_DATE_TRAVELLER_DETAIL);

            txtFromStation.setText(AirDataHolder.getListHolder().getList().get(0).getFromStation());
            txtToStation.setText(AirDataHolder.getListHolder().getList().get(0).getToStation());
            txtToStation.setVisibility(View.VISIBLE);
            txtFromStation.setVisibility(View.VISIBLE);
            imgArrow.setVisibility(View.VISIBLE);

            String passengerTxt = "";
            if (mainActivity.isOneWaySelected) {

                int passCount = AirDataHolder.getListHolder().getList().get(0).getAdultPassNum() + AirDataHolder.getListHolder().getList().get(0).getChildPassNum() + AirDataHolder.getListHolder().getList().get(0).getInfantPassNum();
                if (passCount > 1) {
                    passengerTxt = passCount + " Travellers";
                } else {
                    passengerTxt = passCount + " Traveller";
                }

                txtTravellerDetail.setText(DateUtility.getDateAndMonthFromCal(AirDataHolder.getListHolder().getList().get(0).getDepDate()) + " | " + passengerTxt);
                imgArrow.setImageResource(R.drawable.right_arrow);

            } else {

                int passCount = AirDataHolder.getListHolder().getList().get(0).getAdultPassNum() + AirDataHolder.getListHolder().getList().get(0).getChildPassNum() + AirDataHolder.getListHolder().getList().get(0).getInfantPassNum();

                if (passCount > 1) {
                    passengerTxt = passCount + " Travellers";
                } else {
                    passengerTxt = passCount + " Traveller";
                }

                txtTravellerDetail.setText(DateUtility.getDateAndMonthFromCal(AirDataHolder.getListHolder().getList().get(0).getDepDate()) + " - " + DateUtility.getDateAndMonthFromCal(AirDataHolder.getListHolder().getList().get(0).getReturnDate()) + " | " + passengerTxt);
                imgArrow.setImageResource(R.drawable.plannerswap48);
            }
        }

    }

    public static void showFareQuoteHeader(ActivityMain mainActivity, String normalHeaderText){

        TextView textView = (TextView) lObjAppCompatActivity.findViewById(R.id.toolbar_title);
        LinearLayout toolBarLayoutMain = (LinearLayout) lObjAppCompatActivity.findViewById(R.id.TOOLBAR_DETAIL_LAYOUT);

        // textView.setVisibility(View.VISIBLE);
        toolBarLayoutMain.setVisibility(View.GONE);
        // textView.setText(normalHeaderText);

        textView.setVisibility(View.GONE);
        toolBarLayoutMain.setVisibility(View.VISIBLE);

        TextView txtFromStation = (TextView) lObjAppCompatActivity.findViewById(R.id.SUB_TOOLBAR_FROM);
        TextView txtToStation = (TextView) lObjAppCompatActivity.findViewById(R.id.SUB_TOOLBAR_TO);
        ImageView imgArrow = (ImageView) lObjAppCompatActivity.findViewById(R.id.IMAGE_ARROW);
        txtToStation.setVisibility(View.GONE);
        imgArrow.setVisibility(View.GONE);

        TextView txtTravellerDetail = (TextView) lObjAppCompatActivity.findViewById(R.id.SUB_TOOLBAR_DATE_TRAVELLER_DETAIL);

        txtFromStation.setText(normalHeaderText);


//            String passengerTxt = "";
//            if (mainActivity.isOneWaySelected) {
//
//                int passCount = AirDataHolder.getListHolder().getList().get(0).getAdultPassNum() + AirDataHolder.getListHolder().getList().get(0).getChildPassNum() + AirDataHolder.getListHolder().getList().get(0).getInfantPassNum();
//                if (passCount > 1) {
//                    passengerTxt = passCount + "Travellers";
//                } else {
//                    passengerTxt = passCount + "Traveller";
//                }
//
//                txtTravellerDetail.setText(DateUtility.getDateAndMonthFromCal(AirDataHolder.getListHolder().getList().get(0).getDepDate()) + " | " + passengerTxt);
//                imgArrow.setImageResource(R.drawable.right_arrow);
//
//            } else {
//
////                int passCount = AirDataHolder.getListHolder().getList().get(0).getAdultPassNum() + AirDataHolder.getListHolder().getList().get(0).getChildPassNum() + AirDataHolder.getListHolder().getList().get(0).getInfantPassNum();
////                if (passCount > 1) {
////                    passengerTxt = passCount + "Travellers";
////                } else {
////                    passengerTxt = passCount + "Traveller";
////                }
//
//
//                //imgArrow.setImageResource(R.drawable.plannerswap48);
//            }

        String travellerDetail="";
        if(AirDataHolder.getListHolder().getList().get(0).getAdultPassNum() > 0){
            if(AirDataHolder.getListHolder().getList().get(0).getAdultPassNum() > 1) {
                travellerDetail += AirDataHolder.getListHolder().getList().get(0).getAdultPassNum() + " Adult(s) ";
            }else{
                travellerDetail += AirDataHolder.getListHolder().getList().get(0).getAdultPassNum() + " Adult  ";
            }
        }
        if(AirDataHolder.getListHolder().getList().get(0).getChildPassNum() > 0){
            if(AirDataHolder.getListHolder().getList().get(0).getChildPassNum() > 1) {
                travellerDetail += "|  " + AirDataHolder.getListHolder().getList().get(0).getChildPassNum() + " Children  ";
            }else {
                travellerDetail += "|  "+AirDataHolder.getListHolder().getList().get(0).getChildPassNum()+" Child  ";
            }
        }
        if(AirDataHolder.getListHolder().getList().get(0).getInfantPassNum() > 0){
            if(AirDataHolder.getListHolder().getList().get(0).getInfantPassNum() > 1) {
                travellerDetail += "|  " + AirDataHolder.getListHolder().getList().get(0).getInfantPassNum() + " Infant(s)";
            }else{
                travellerDetail += "|  " + AirDataHolder.getListHolder().getList().get(0).getInfantPassNum() + " Infant";
            }
        }
        txtTravellerDetail.setText(travellerDetail);

        ViewGroup.LayoutParams lp = ((ViewGroup) toolBarLayoutMain).getLayoutParams();
        if( lp instanceof ViewGroup.MarginLayoutParams)
        {
            ((ViewGroup.MarginLayoutParams) lp).leftMargin = 45;
        }
    }


}
