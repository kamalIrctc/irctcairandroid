package com.irctc.air.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.irctc.air.R;
import com.irctc.air.header.AirHeader;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.util.Constant;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.ProjectUtil;


/**
 * Created by asif on 10/01/2018
 */
public class TestCalendarFragment extends Fragment {

    private ActivityMain mainActivity;
    private TextView tvVersion;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mainActivity = (ActivityMain) activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.test_cal, null);

        initView(view);

        // Header visibility
        AirHeader.showRecentSearchIcon(false);

        // Show header text
        AirHeader.showHeaderText(mainActivity, true, "About Us");

        // Show&Hide toggle and drawer
        AirHeader.showDrawerToggleAndToolbar(true, true);


        return view;
    }


    private void initView(View view) {

        AirHeader.createHeader(mainActivity, "About Us");

        Button onw = (Button) view.findViewById(R.id.btnONW);
        Button ret = (Button)view.findViewById(R.id.btnRET);

        onw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ProjectUtil.replaceFragment(mainActivity, new CalenderFragment(), R.id.frame_layout, EnumAnimation.DOWN_TO_TOP);

            }
        });

        ret.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ProjectUtil.replaceFragment(mainActivity, new CalenderFragment(), R.id.frame_layout, EnumAnimation.DOWN_TO_TOP);

            }
        });

    }



    @Override
    public void onResume() {
        super.onResume();
        ActivityMain.activeFragment = Constant.WEBVIEW_FRAGMENT;
    }
}
