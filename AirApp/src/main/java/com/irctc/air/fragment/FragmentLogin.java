package com.irctc.air.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.auth0.android.jwt.JWT;
import com.google.gson.Gson;
import com.irctc.air.AppController;
import com.irctc.air.Database.AirDatabase;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.header.AirHeader;
import com.irctc.air.model.login.PojoLoginUser;
import com.irctc.air.navigationdrawer.SliderMenu;
import com.irctc.air.round.trip.domastic.fragment.DFragmentAddPassengers;
import com.irctc.air.round.trip.domastic.fragment.DFragmentTicket;
import com.irctc.air.round.trip.international.fragment.IFragmentAddPassengers;
import com.irctc.air.round.trip.international.fragment.IFragmentTicket;
import com.irctc.air.util.AlertDialogUtil;
import com.irctc.air.util.AppKeys;
import com.irctc.air.util.Constant;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.NetworkingUtils;
import com.irctc.air.util.Pref;
import com.irctc.air.util.ProjectUtil;
import com.irctc.air.util.Validation;
import com.irctc.air.networking.Networking;

import org.json.JSONObject;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class FragmentLogin extends Fragment implements View.OnClickListener {
    private ActivityMain mainActivity;
    CheckBox cbGuestUser;
    private LinearLayout lLayoutUserLogin, lLayoutGuestUserLogin, lLayoutSignUp;
    private AutoCompleteTextView tvUserId, tvUserPassword, tvGuestUserEmail, tvGuestUserMobNum;
    private TextView tvForgetPassword;
    private Button btnLoginSubmit;
    private Boolean mStriValidPassUserName = false;
    private Boolean mStriValidPassUserPwd = false;
    private Boolean mStriValidGuestPassEmail = false;
    private Boolean mStriValidGuestPassMob = false;
    public static boolean handelBack = false;
    private int afterSessionOut = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle != null && bundle.containsKey("logout")) {
            Pref.clearPref(getContext());
            afterSessionOut = 1;
            //ActivityMain.backStackEnable =true;
            //Toast.makeText(getContext(), "Expired: "+ActivityMain.backStackEnable,Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_layout, null);
        initializeVariables(view);
        AirHeader.showRecentSearchIcon(false);
        AirHeader.showHeaderText(mainActivity, true, "Login");
        AirHeader.showDrawerToggleAndToolbar(false, false);
        //   mainActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        if (AppController.routeType != null) {
            if (AppController.routeType.equals("OneWayTicket")) {
                handelBack = true;
            } else if (AppController.routeType.equals("RoundTripDomesticTicket")) {
                handelBack = true;
            } else if (AppController.routeType.equals("RoundTripInternationalTicket")) {
                handelBack = true;
            }
        }
        return view;
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public void initializeVariables(View view) {
        mainActivity = (ActivityMain) getActivity();
        cbGuestUser = (CheckBox) view.findViewById(R.id.CB_GUEST_USER);

        lLayoutUserLogin = (LinearLayout) view.findViewById(R.id.LL_USER_LOGIN);
        tvUserId = (AutoCompleteTextView) view.findViewById(R.id.ET_USER_ID);
        tvUserPassword = (AutoCompleteTextView) view.findViewById(R.id.ET_PASSWORD);

        lLayoutGuestUserLogin = (LinearLayout) view.findViewById(R.id.LL_GUEST_USER_LOGIN);
        tvGuestUserEmail = (AutoCompleteTextView) view.findViewById(R.id.ET_GUEST_EMAIL);
        tvGuestUserMobNum = (AutoCompleteTextView) view.findViewById(R.id.ET_GUEST_MOBILE_NUM);

        btnLoginSubmit = (Button) view.findViewById(R.id.BTN_SUBMIT_LOGIN);

        tvForgetPassword = (TextView) view.findViewById(R.id.TV_FORGET_PASSWORD);
        lLayoutSignUp = (LinearLayout) view.findViewById(R.id.LL_SIGNUP);


        cbGuestUser.setOnClickListener(this);
        lLayoutUserLogin.setOnClickListener(this);
        tvUserId.setOnClickListener(this);
        tvUserPassword.setOnClickListener(this);
        lLayoutGuestUserLogin.setOnClickListener(this);
        tvGuestUserEmail.setOnClickListener(this);
        tvGuestUserMobNum.setOnClickListener(this);
        btnLoginSubmit.setOnClickListener(this);
        tvForgetPassword.setOnClickListener(this);
        lLayoutSignUp.setOnClickListener(this);

        /*tvUserId.setText("lakdeepak");
        tvUserPassword.setText("123");
        mStriValidPassUserName = true;
        mStriValidPassUserPwd = true;*/


        tvUserId.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                String lStrValidationMsg = Validation.getInstance().getUserNameValidation(tvUserId.getText().toString().trim());
                if (lStrValidationMsg.equalsIgnoreCase("SUCCESS")) {
                    tvUserId.setError(null);
                    mStriValidPassUserName = true;
                } else {
                    mStriValidPassUserName = false;
                    tvUserId.setError(lStrValidationMsg);
                }
            }
        });

        // USER PWD
        tvUserPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence userInput, int start, int before, int count) {

                String lStrValidationMsg = Validation.getInstance().getUserPwdValidation(tvUserPassword.getText().toString().trim());
                if (lStrValidationMsg.equalsIgnoreCase("SUCCESS")) {
                    tvUserPassword.setError(null);
                    mStriValidPassUserPwd = true;
                } else {
                    mStriValidPassUserPwd = false;
                    tvUserPassword.setError(lStrValidationMsg);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

        });

        // GUEST EMAIL
        tvGuestUserEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence userInput, int start, int before, int count) {

                isValidEmail(userInput);
                String lStrValidationMsg = Validation.getInstance().getEmailValidation(tvGuestUserEmail.getText().toString().trim());
                if (lStrValidationMsg.equalsIgnoreCase("SUCCESS")) {
                    tvGuestUserEmail.setError(null);
                    mStriValidGuestPassEmail = true;
                } else {
                    mStriValidGuestPassEmail = false;
                    tvGuestUserEmail.setError(lStrValidationMsg);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

        });

        tvGuestUserMobNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence userInput, int start, int before, int count) {

                String lStrValidationMsg = Validation.getInstance().getMobileNoValidation(tvGuestUserMobNum.getText().toString().trim());
                if (lStrValidationMsg.equalsIgnoreCase("SUCCESS")) {
                    tvGuestUserMobNum.setError(null);
                    mStriValidGuestPassMob = true;
                } else {
                    mStriValidGuestPassMob = false;
                    tvGuestUserMobNum.setError(lStrValidationMsg);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

        });
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.CB_GUEST_USER:
                if (cbGuestUser.isChecked()) {
                    lLayoutUserLogin.setVisibility(View.GONE);
                    lLayoutGuestUserLogin.setVisibility(View.VISIBLE);
                    tvForgetPassword.setVisibility(View.INVISIBLE);
                } else {
                    lLayoutGuestUserLogin.setVisibility(View.GONE);
                    lLayoutUserLogin.setVisibility(View.VISIBLE);
                    tvForgetPassword.setVisibility(View.VISIBLE);
                }
                break;

            case R.id.TV_FORGET_PASSWORD:

                ShowForgetPasswordDialog();


                break;

            case R.id.LL_SIGNUP:
                ShowSignUpDialog();
                break;

            case R.id.BTN_SUBMIT_LOGIN:
                if (cbGuestUser.isChecked()) {
                    if (!ProjectUtil.checkInternetConnection(mainActivity)) {
                        new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.INTERNET_DOWN), mainActivity.getResources().getString(R.string.NO_INTERNET_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                    } else {
                        loginGuest(tvGuestUserEmail.getText().toString().trim(), tvGuestUserMobNum.getText().toString().trim());
                    }

                } else {
                    if (mStriValidPassUserName && mStriValidPassUserPwd) {
                        if (!ProjectUtil.checkInternetConnection(mainActivity)) {
                            new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.INTERNET_DOWN), mainActivity.getResources().getString(R.string.NO_INTERNET_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                        } else {
                            loginUser(tvUserId.getText().toString().trim(), tvUserPassword.getText().toString().trim());
                        }
                    } else {
                        new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.LOGIN_VALIDATION_GENRIC_TEXT), mainActivity.getResources().getString(R.string.LOGIN_ERROR_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                    }
                }

                break;

            default:
                break;

        }


    }


    private void ShowSignUpDialog() {


        new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.SIGN_UP_MESSAGE_TEXT), mainActivity.getResources().getString(R.string.SIGN_UP_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();

    }


    private void ShowForgetPasswordDialog() {


        new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.FORGOT_PASS_MESSAGE_TEXT), mainActivity.getResources().getString(R.string.FORGOT_PASS_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();


//        if (!mainActivity.isFinishing()) {
//
//            final Dialog dialog = new Dialog(mainActivity);
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            dialog.setContentView(R.layout.forget_password_dialog);
//
//
//            dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//            final AutoCompleteTextView passUserID = (AutoCompleteTextView) dialog.findViewById(R.id.et_user_id);
//            final AutoCompleteTextView passEmailId = (AutoCompleteTextView) dialog.findViewById(R.id.et_email_id);
//            final TextView lTxtErrorMsg = (TextView) dialog.findViewById(R.id.tv_error);
//            final ProgressBar lProgressBar = (ProgressBar) dialog.findViewById(R.id.progress);
//            try {
//                lProgressBar.getIndeterminateDrawable().setColorFilter(Color.parseColor("#FF797C80"), PorterDuff.Mode.SRC_IN);
//            } catch (Exception e) {
//
//            }
//
//            lTxtErrorMsg.setVisibility(View.GONE);
//
//            dialog.show();
//
//            Button lBtnSubmit = (Button) dialog.findViewById(R.id.btn_submit);
//            // if submit button is clicked, perform operation
//
//            lBtnSubmit.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    if (!ProjectUtil.checkInternetConnection(mainActivity)) {
//                        new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.INTERNET_DOWN), mainActivity.getResources().getString(R.string.FORGOT_PASS_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
//
//                    } else {
//                        String passUserIdValidationRes = Validation.getInstance().getUserIdValidation(passUserID.getText().toString().trim());
//                        String passEmailIdValidationRes = Validation.getInstance().getEmailValidation(passEmailId.getText().toString().trim());
//
//                        if (!passUserIdValidationRes.equalsIgnoreCase("SUCCESS")) {
//                            lTxtErrorMsg.setVisibility(View.VISIBLE);
//                            lTxtErrorMsg.setText(passUserIdValidationRes);
//                        } else if(!passEmailIdValidationRes.equalsIgnoreCase("SUCCESS")) {
//                            lTxtErrorMsg.setVisibility(View.VISIBLE);
//                            lTxtErrorMsg.setText(passEmailIdValidationRes);
//                        } else {
//                            lTxtErrorMsg.setVisibility(View.GONE);
//                            lTxtErrorMsg.setText("");
//
//                            // cancel the dialog
//                            dialog.dismiss();
//
//                            String requestXml = "<forgetDetails><userID>"+passUserID.getText().toString().trim()+"</userID><email>"+passEmailId.getText().toString().trim()+"</email></forgetDetails>";
//
//                            ForgotPassWordRequest auth = new ForgotPassWordRequest(mainActivity, requestXml);
//                            auth.execute();
//
//                        }
//
//                    }
//
//
//                }
//            });
//
//            passUserID.addTextChangedListener(new TextWatcher() {
//                @Override
//                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                    lTxtErrorMsg.setVisibility(View.GONE);
//                    lTxtErrorMsg.setText("");
//                }
//
//                @Override
//                public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                }
//
//                @Override
//                public void afterTextChanged(Editable s) {
//
//                }
//            });
//
//            passEmailId.addTextChangedListener(new TextWatcher() {
//                @Override
//                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                    lTxtErrorMsg.setVisibility(View.GONE);
//                    lTxtErrorMsg.setText("");
//                }
//
//                @Override
//                public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                }
//
//                @Override
//                public void afterTextChanged(Editable s) {
//
//                }
//            });
//
//    }


    }


    public void removePhoneKeypad(View view) {
//        InputMethodManager inputManager = (InputMethodManager) view
//                .getContext()
//                .getSystemService(Context.INPUT_METHOD_SERVICE);
//
//        IBinder binder = view.getWindowToken();
//        inputManager.hideSoftInputFromWindow(binder,
//                InputMethodManager.HIDE_NOT_ALWAYS);

        // Check if no view has focus:
        view = mainActivity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) mainActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

//        InputMethodManager imm = (InputMethodManager) mainActivity.getSystemService(Activity.INPUT_METHOD_SERVICE);
//        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onResume() {
        super.onResume();
        AppController.getInstance().trackScreenView("Login Screen");
        // Show&Hide toggle and drawer and toolbar
        AirHeader.showDrawerToggleAndToolbar(false, false);

        ActivityMain.activeFragment = Constant.LOGIN_FRAGMENT;
    }


    //region : Login User
    private void loginUser(String userName, String password) {
        NetworkingUtils.showProgress(getActivity());
        Networking.loginUser(userName, password, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Gson gson = new Gson();
                PojoLoginUser pojoLoginUser = gson.fromJson(response.toString(), PojoLoginUser.class);
                NetworkingUtils.dismissProgress();
                if (pojoLoginUser.getStatus().equals("SUCCESS")) {
                    Log.e("Login Res", new Gson().toJson(pojoLoginUser));
                    //((ActivityMain) getContext()).startThread();
                    Context context = getActivity();
                    AirDatabase airDatabase = new AirDatabase(context);
                    airDatabase.insertAuthToken(pojoLoginUser.getData());
                    Pref.setString(context, AppKeys.USER_DETAIL_ADDRESS_1, pojoLoginUser.getUserDetails().getUserInfo().getAddress());
                    Pref.setString(context, AppKeys.USER_DETAIL_ADDRESS_2, "");
                    Pref.setString(context, AppKeys.USER_DETAIL_FIRST_NAME, pojoLoginUser.getUserDetails().getUserInfo().getFirstName());
                    Pref.setString(context, AppKeys.USER_DETAIL_LAST_NAME, pojoLoginUser.getUserDetails().getUserInfo().getLastName());
                    Pref.setString(context, AppKeys.USER_DETAIL_CITY, pojoLoginUser.getUserDetails().getUserInfo().getDistrict());
                    Pref.setString(context, AppKeys.USER_DETAIL_STATE, pojoLoginUser.getUserDetails().getUserInfo().getStateName());
                    String country="India";
                    if(pojoLoginUser.getUserDetails().getUserInfo().getCountry()!=null){
                        if(!pojoLoginUser.getUserDetails().getUserInfo().getCountry().equals("")){
                        country = pojoLoginUser.getUserDetails().getUserInfo().getCountry();
                        }
                    }
                    Pref.setString(context, AppKeys.USER_DETAIL_COUNTRY,country);
                    Pref.setString(context, AppKeys.USER_DETAIL_PIN_CODE, pojoLoginUser.getUserDetails().getPincode());
                    Pref.setString(context, AppKeys.USER_DETAIL_EMAIL, pojoLoginUser.getUserDetails().getEmail());
                    Pref.setString(context, AppKeys.USER_DETAIL_MOBILE_NO, pojoLoginUser.getUserDetails().getMobileNumber());
                    Pref.setBoolean(context, true);
                    Pref.setString(context, AppKeys.USER_DETAIL_USER_TYPE, "user");
                    SliderMenu.txtUserId.setText(pojoLoginUser.getUserDetails().getUserInfo().getFirstName());
                    Toast.makeText(getActivity().getApplicationContext(), "Login Success!", Toast.LENGTH_SHORT).show();
                    if (AppController.isComingFromFareQuote) {
                        AppController.isComingFromFareQuote = false;
                        if (AppController.routeType.equals("OneWay")) {
                            if(ActivityMain.isRunning)
                            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, new FragmentAddPassengers()).commit();
                        } else if (AppController.routeType.equals("RoundTripDomestic")) {
                            if(ActivityMain.isRunning)
                            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, new DFragmentAddPassengers()).commit();
                        } else if (AppController.routeType.equals("RoundTripInternational")) {
                            if(ActivityMain.isRunning)
                            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, new IFragmentAddPassengers()).commit();
                        } else if (AppController.routeType.equals("OneWayTicket")) {
                            handelBack = false;
                            if(ActivityMain.isRunning)
                            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, new FragmentTicket()).commit();
                        } else if (AppController.routeType.equals("RoundTripDomesticTicket")) {
                            handelBack = false;
                            if(ActivityMain.isRunning)
                            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, new DFragmentTicket()).commit();
                        } else if (AppController.routeType.equals("RoundTripInternationalTicket")) {
                            handelBack = false;
                            if(ActivityMain.isRunning)
                            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, new IFragmentTicket()).commit();
                        }
                    } else if (afterSessionOut == 1) {
                        if(ActivityMain.isRunning)
                        getActivity().getSupportFragmentManager().popBackStackImmediate();

                    } else {
                        ActivityMain.lastActiveFragment = Constant.PLANNER_FRAGMENT;
                        ProjectUtil.replaceFragment(mainActivity, new FragmentPlanner(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                    }
                } else {
                    Toast.makeText(getContext(), "Invalid user name or password", Toast.LENGTH_LONG).show();
                    //NetworkingUtils.internalServerIssue(getActivity().getApplicationContext());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkingUtils.dismissProgress();
                Toast.makeText(getContext(), "Internal Server Error", Toast.LENGTH_SHORT).show();
//                NetworkingUtils.noInternetAccess(getActivity().getApplicationContext());
            }
        });

    }
    //endregion

    //region : Login Guest
    private void loginGuest(final String userName, final String password) {
        NetworkingUtils.showProgress(getActivity());
        Networking.loginGuest(userName, password, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                NetworkingUtils.dismissProgress();
                Log.e("Response", response.toString());
                Gson gson = new Gson();
                PojoLoginUser pojoLoginUser = gson.fromJson(response.toString(), PojoLoginUser.class);
                NetworkingUtils.dismissProgress();
                if (pojoLoginUser.getStatus().equals("SUCCESS")) {
                    //((ActivityMain) getContext()).startThread();
                    Context context = getActivity();
                    AirDatabase airDatabase = new AirDatabase(context);
                    airDatabase.insertAuthToken(pojoLoginUser.getData());
                    Pref.setString(context, AppKeys.USER_DETAIL_EMAIL, userName);
                    Pref.setString(context, AppKeys.USER_DETAIL_MOBILE_NO, password);
                    Pref.setString(context, AppKeys.USER_DETAIL_USER_TYPE, "guest");
                    Pref.setBoolean(context, true);
                    Toast.makeText(getActivity().getApplicationContext(), "Login Success!", Toast.LENGTH_SHORT).show();
                    if (AppController.isComingFromFareQuote) {
                        AppController.isComingFromFareQuote = false;
                        if (AppController.routeType.equals("OneWay")) {
                            if(ActivityMain.isRunning)
                            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, new FragmentAddPassengers()).commit();
                        } else if (AppController.routeType.equals("RoundTripDomestic")) {
                            if(ActivityMain.isRunning)
                            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, new DFragmentAddPassengers()).commit();
                        } else if (AppController.routeType.equals("RoundTripInternational")) {
                            if(ActivityMain.isRunning)
                            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, new IFragmentAddPassengers()).commit();
                        }
                    } else {
                        ActivityMain.lastActiveFragment = Constant.PLANNER_FRAGMENT;
                        ProjectUtil.replaceFragment(mainActivity, new FragmentPlanner(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                    }
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "Invalid credentials", Toast.LENGTH_SHORT).show();
                    //NetworkingUtils.internalServerIssue(getActivity().getApplicationContext());
                }
            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkingUtils.dismissProgress();
                NetworkingUtils.noInternetAccess(getActivity().getApplicationContext());
            }
        });

    }
    //endregion

}
