package com.irctc.air.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.adapter.OnwardFlightAdapter;
import com.irctc.air.adapter.OnwardFlightInfoAdapter;
import com.irctc.air.adapter.ReturnFlightAdapter;
import com.irctc.air.adapter.RouRetOnwardFlightAdapter;
import com.irctc.air.adapter.RouRetReturnFlightAdapter;
import com.irctc.air.adapter.RoundOnwardFlightAdapter;
import com.irctc.air.header.AirHeader;
import com.irctc.air.model.AirDataModel;
import com.irctc.air.model.FlightFilterBean;
import com.irctc.air.model.FlightOnWardDetailBean;
import com.irctc.air.model.InnerFlightDetailsBeans;
import com.irctc.air.model.SpecialOfferAirlineBean;
import com.irctc.air.services.FareQuoteRoundTripService;
import com.irctc.air.util.AirFlightSort;
import com.irctc.air.util.AlertDialogUtil;
import com.irctc.air.util.AppLogger;
import com.irctc.air.util.ArrivalTimeSort;
import com.irctc.air.util.Constant;
import com.irctc.air.util.DateUtility;
import com.irctc.air.util.DepartTimeSort;
import com.irctc.air.util.DurationTimeSort;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.FarePriceSort;
import com.irctc.air.util.ProjectUtil;
import com.irctc.air.util.RoundFilterEventHandler;
import com.irctc.air.Database.SharedPrefrenceAir;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by tourism on 5/6/2016.
 */
public class FragmentRoundTripDomestic extends Fragment implements AdapterView.OnItemClickListener, View.OnClickListener,RoundFilterEventHandler.AfterApply {


    private ActivityMain mainActivity;

    LinearLayout mFloatinFilterBtn, layFlightInfo;
    private ImageView imgRecentSearch;
    //   RETURN MAIN SCREEN VARIABLES

    private ListView mainListViewOnwardFlights;
    private ListView mainListViewReturnFlights;
    private TextView mainTxtOnwardAirportFromCode;
    private TextView mainTxtOnwardAirportToCode;
    private Button mainBtnSortOnwardDepart;
    private Button mainBtnSortOnwardPrice;
    private TextView mainTxtReturnAirportToCode;
    private TextView mainTxtReturnAirportFromCode;
    private Button mainBtnSortReturnDepart;
    private Button mainBtnSortReturnPrice;
    private boolean btnOnwardDepart,btnOnwardPrice;
    boolean btnReturnDepart,btnReturnPrice;
    public ArrayList<FlightOnWardDetailBean> mainReturnFlightOnWardMainHolderClone;
    public ArrayList<FlightOnWardDetailBean> mainReturnFlightReturnMainHolderClone;
    private TextView txtTotalPrice;
    private int mainReturnOnwPosition;
    private int mainReturnRetPosition;
    private OnwardFlightAdapter mainFlightOnwardAdapter;
    private ReturnFlightAdapter mainFlightReturnAdapter;

    private LinearLayout mainLayReturnList;
    private LinearLayout mainLayReturnCode;
    private LinearLayout mainLaySortReturn;
    private LinearLayout mainLayReturnContent;
    private LinearLayout mainHorizontalList;
    private LinearLayout mainLayRoundBook;
    private Button mainBtnAllFlight;
    private int previousSpecialOfferPosition = -1;
    HashMap<Integer,View> map = new HashMap<>();
    private final int IS_ROUNDTRIP_ONWARD_RETURN = 1;
    private boolean specialOfferCreater = true;


    //  ROUND ONWARD  SCREEN VARIABLES

    private LinearLayout layRouOnwContent;
    private Button btnSortRouOnwAirline;
    private Button btnSortRouOnwDepart;
    private Button btnSortRouOnwArive;
    private Button btnSortRouOnwDuration;
    private Button btnSortRouOnwPrice;
    private ListView listViewRouOnwAllFlight;

    public ArrayList<FlightOnWardDetailBean> specificRoundOnwardFlight;
    private ArrayList<FlightOnWardDetailBean> alFliterGDSFlight;
    private RoundOnwardFlightAdapter roundOnwardAdapter;
    private int rouOnwPosition;
    private final int IS_ROUNDTRIP_ROUND_ONWARD = 2;

    private boolean btnRouOnwAirline, btnRouOnwPrice, btnRouOnwDepart, btnRouOnwArrive, btnRouOnwDuration;

    //  ROUND RETURN  SCREEN VARIABLES

    private LinearLayout layRouRetContent;
    private TextView txtOnwardRouRetAirportToCode;
    private TextView txtOnwardRouRetAirportFromCode;
    private Button btnSortOnwardRouRetDepart;
    private Button btnSortOnwardRouRetPrice;
    private ListView listViewOnwardRouRetAllFlight;

    private TextView txtReturnRouRetAirportToCode;
    private TextView txtReturnRouRetAirportFromCode;
    private Button btnSortReturnRouRetDepart;
    private Button btnSortReturnRouRetPrice;
    private ListView lisViewtReturnRouRetAllFlight;

    public ArrayList<FlightOnWardDetailBean> rouRetOnwardFlight;
    public ArrayList<FlightOnWardDetailBean> rouRetReturnFlight;

    private ArrayList<FlightOnWardDetailBean> alFliterRouRetOnwardFlight;
    private ArrayList<FlightOnWardDetailBean> alFliterRouRetReturnFlight;

    private RouRetOnwardFlightAdapter rouRetOnwardAdapter;
    private RouRetReturnFlightAdapter rouRetReturnAdapter;

    private boolean btnOnwardRouRetDepart,btnOnwardRouRetPrice;
    boolean btnReturnRouRetDepart,btnReturnRouRetPrice;

    private int rouRetOnwardPosition;
    private int rouRetReturnPosition;
    private final int IS_ROUND_RETURN_SCREEN = 3;


    /////////////////////// COMMON VARIABLES //////////////////////

    private Dialog lObjDialogShowFilterOption;
    String flightCode;


    // INFO DIALOG
    private Dialog lObjDialogShowFlightDetails;
    private FlightFilterBean filterBean;

    public FragmentRoundTripDomestic(){
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mainActivity = (ActivityMain) activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_round_trip_domestic, null);

        // Initialize
        initializeVariable(view);

        // Set data in variable from holder
        setdataInVarFromHolder();

        // Show header text
        AirHeader.showHeaderText(mainActivity, false, "");
        // Header visibility
        AirHeader.showRecentSearchIcon(true);
        // Show&Hide toggle and drawer and toolbar
        AirHeader.showDrawerToggleAndToolbar(true, true);

        return view;
    }

    private void setdataInVarFromHolder() {

        // HERE WE INITIALIZE THE MAIN RETURN SCREEN
        setDataInMainReturnVarFromHolder();

        /*// HERE WE INITIALIZE THE ROUND ONWARD SCREEN
        setDataInRoundOnwardVarFromHolder();
        // HERE WE INITIALIZE THE ROUND RETURN SCREEN
        setDataInRoundReturnVarFromHolder();*/

    }

    private void setDataInMainReturnVarFromHolder() {

        mainActivity.SCREEN_TYPE = IS_ROUNDTRIP_ONWARD_RETURN;

        if(specialOfferCreater){
            addSpecialOfferAirline();
        }

        if(!mainActivity.isComingFromRoundFilterDialog){

            mainReturnFlightOnWardMainHolderClone = (ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().clone();
            mainReturnFlightReturnMainHolderClone = (ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getReturnAlAllFlightDetails().clone();
        }else {
            mainActivity.isComingFromRoundFilterDialog = false;
        }
        mainFlightOnwardAdapter = new OnwardFlightAdapter(mainActivity, mainReturnFlightOnWardMainHolderClone);
        mainFlightReturnAdapter = new ReturnFlightAdapter(mainActivity, mainReturnFlightReturnMainHolderClone);

        mainListViewOnwardFlights.setAdapter(mainFlightOnwardAdapter);
        mainListViewReturnFlights.setAdapter(mainFlightReturnAdapter);

        mainTxtOnwardAirportFromCode.setText(AirDataHolder.getListHolder().getList().get(0).getFromStation());
        mainTxtOnwardAirportToCode.setText(AirDataHolder.getListHolder().getList().get(0).getToStation());
        mainTxtReturnAirportFromCode.setText(AirDataHolder.getListHolder().getList().get(0).getFromStation());//getFromStation
        mainTxtReturnAirportToCode.setText(AirDataHolder.getListHolder().getList().get(0).getToStation());


        //
        // Sort default by price
        defaultMainOnwardSortingOnFare();
        defaultMainReturnSortingOnFare();
        mainListViewOnwardFlights.setItemChecked(0, true);
        mainListViewReturnFlights.setItemChecked(0, true);
        setMainReturnFirstFlightsPrice(0, 0);
    }

    private void setDataInRoundOnwardVarFromHolder() {

        roundOnwardAdapter = new RoundOnwardFlightAdapter(mainActivity, specificRoundOnwardFlight);
        listViewRouOnwAllFlight.setAdapter(roundOnwardAdapter);

        defaultRoundOnwardSortingOnFare();
        listViewRouOnwAllFlight.setItemChecked(0, true);
        setRoundOnwardFirstFlightsPrice(0, 0);

    }

    private void setDataInRoundReturnVarFromHolder() {

        rouRetOnwardAdapter = new RouRetOnwardFlightAdapter(mainActivity, rouRetOnwardFlight);
        rouRetReturnAdapter = new RouRetReturnFlightAdapter(mainActivity, rouRetReturnFlight);
        lisViewtReturnRouRetAllFlight.setAdapter(rouRetReturnAdapter);
        listViewOnwardRouRetAllFlight.setAdapter(rouRetOnwardAdapter);

        txtOnwardRouRetAirportFromCode.setText(AirDataHolder.getListHolder().getList().get(0).getToStation());//getToStation
        txtOnwardRouRetAirportToCode.setText(AirDataHolder.getListHolder().getList().get(0).getFromStation());

        txtReturnRouRetAirportFromCode.setText(AirDataHolder.getListHolder().getList().get(0).getFromStation());//getFromStation
        txtReturnRouRetAirportToCode.setText(AirDataHolder.getListHolder().getList().get(0).getToStation());

        defaultRouRetOnwardSortingOnFare();
        defaultRouRetReturnSortingOnFare();
        lisViewtReturnRouRetAllFlight.setItemChecked(0, true);
        listViewOnwardRouRetAllFlight.setItemChecked(0, true);
        setRoundReturnFirstFlightsPrice(0, 0);
    }

    private void initializeVariable(View view) {

        mFloatinFilterBtn = (LinearLayout) view.findViewById(R.id.FLOATING_BTN_ROUND);
        mFloatinFilterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                specialOfferCreater = false;

                if(filterBean == null){
                    filterBean = new FlightFilterBean();
                }
                showFilterDialog(filterBean);

            }
        });

        imgRecentSearch = (ImageView) mainActivity.findViewById(R.id.RECENT_SEARCH);
        imgRecentSearch.setVisibility(View.VISIBLE);
        imgRecentSearch.setImageResource(R.drawable.recent_search);
        imgRecentSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mainActivity.lastActiveFragment = Constant.FRAGMENT_ROUND_TRIP;
                ProjectUtil.replaceFragment(mainActivity, new FragmentRecentSearch(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

            }
        });

         layFlightInfo = (LinearLayout) view.findViewById(R.id.FLIGHT_INFO);
         layFlightInfo.setOnClickListener(this);

        // HERE WE INITIALIZE THE MAIN RETURN SCREEN
        initializeMainReturnVariables(view);
        // HERE WE INITIALIZE THE ROUND ONWARD SCREEN
        initializeRoundOnwardVariables(view);
        // HERE WE INITIALIZE THE ROUND RETURN SCREEN
        initializeRoundReturnVariables(view);

    }

    private void initializeMainReturnVariables(View view) {


        mainBtnAllFlight = (Button) view.findViewById(R.id.BTN_MAIN_ALL_FLIGHT);
        mainHorizontalList = (LinearLayout) view.findViewById(R.id.HORZ_LIST_ITEM);

        mainLayReturnContent = (LinearLayout) view.findViewById(R.id.LAY_MAIN_RETURN_CONTENT);
        mainLayReturnList = (LinearLayout) view.findViewById(R.id.LAY_MAIN_ONWARD_AND_RETURN_LIST);
        mainLayReturnCode = (LinearLayout) view.findViewById(R.id.LAY_MAIN_AIR_CODE);
        mainLaySortReturn = (LinearLayout) view.findViewById(R.id.LAY_MAIN_SORTING);
        mainLayRoundBook = (LinearLayout) view.findViewById(R.id.LAY_ROUND_BOOKING);


        mainTxtOnwardAirportFromCode = (TextView) view.findViewById(R.id.TXT_MAIN_ONWARD_AIRPORT_FROM_CODE);
        mainTxtOnwardAirportToCode = (TextView) view.findViewById(R.id.TXT_MAIN_ONWARD_AIRPORT_TO_CODE);
        mainBtnSortOnwardDepart = (Button) view.findViewById(R.id.BTN_MAIN_ONWARD_DEPART);
        mainBtnSortOnwardPrice = (Button) view.findViewById(R.id.BTN_MAIN_ONWARD_PRICE);

        mainTxtReturnAirportFromCode = (TextView) view.findViewById(R.id.TXT_MAIN_RETURN_AIRPORT_FROM_CODE);
        mainTxtReturnAirportToCode = (TextView) view.findViewById(R.id.TXT_MAIN_RETURN_AIRPORT_TO_CODE);
        mainBtnSortReturnDepart = (Button) view.findViewById(R.id.BTN_MAIN_RETURN_DEPART);
        mainBtnSortReturnPrice = (Button) view.findViewById(R.id.BTN_MAIN_RETURN_PRICE);

        mainListViewOnwardFlights = (ListView) view.findViewById(R.id.LIST_MAIN_ONWARD_FLIGHT);
        mainListViewReturnFlights = (ListView) view.findViewById(R.id.LIST_MAIN_RETURN_FLIGHT);
        mainListViewOnwardFlights.setOnItemClickListener(this);
        mainListViewReturnFlights.setOnItemClickListener(this);

        txtTotalPrice = (TextView) view.findViewById(R.id.TXT_ROUND_TOTAL_PRICE);

        mainBtnAllFlight.setOnClickListener(this);
        mainBtnSortOnwardDepart.setOnClickListener(this);
        mainBtnSortOnwardPrice.setOnClickListener(this);
        mainBtnSortReturnDepart.setOnClickListener(this);
        mainBtnSortReturnPrice.setOnClickListener(this);
        mainLayRoundBook.setOnClickListener(this);

        mainLayReturnContent.setOnClickListener(this);
    }

    private void initializeRoundOnwardVariables(View view) {

        layRouOnwContent = (LinearLayout) view.findViewById(R.id.LAY_ROUND_ONWARD_CONTENT);
        btnSortRouOnwAirline = (Button) view.findViewById(R.id.BTN_ROUONW_AIRLINE);
        btnSortRouOnwDepart = (Button) view.findViewById(R.id.BTN_ROUONW_DEPART);
        btnSortRouOnwArive = (Button) view.findViewById(R.id.BTN_ROUONW_ARRIVE);
        btnSortRouOnwDuration = (Button) view.findViewById(R.id.BTN_ROUONW_DURATION);
        btnSortRouOnwPrice = (Button) view.findViewById(R.id.BTN_ROUONW_PRICE);
        listViewRouOnwAllFlight = (ListView) view.findViewById(R.id.LIST_ROUONW_ALL_FLIGHTS);

        listViewRouOnwAllFlight.setOnItemClickListener(this);

        btnSortRouOnwAirline.setOnClickListener(this);
        btnSortRouOnwDepart.setOnClickListener(this);
        btnSortRouOnwArive.setOnClickListener(this);
        btnSortRouOnwDuration.setOnClickListener(this);
        btnSortRouOnwPrice.setOnClickListener(this);

    }

    private void initializeRoundReturnVariables(View view) {


        layRouRetContent = (LinearLayout) view.findViewById(R.id.LAY_ROUND_RETURN_CONTENT);

        txtOnwardRouRetAirportToCode = (TextView) view.findViewById(R.id.TXT_ROURET_ONWARD_AIRPORT_FROM_CODE);
        txtOnwardRouRetAirportFromCode = (TextView) view.findViewById(R.id.TXT_ROURET_ONWARD_AIRPORT_TO_CODE);
        btnSortOnwardRouRetDepart = (Button) view.findViewById(R.id.BTN_ROURET_ONWARD_DEPART);
        btnSortOnwardRouRetPrice = (Button) view.findViewById(R.id.BTN_ROURET_ONWARD_PRICE);
        listViewOnwardRouRetAllFlight = (ListView) view.findViewById(R.id.LIST_ROURET_ONWARD_FLIGHT);

        txtReturnRouRetAirportToCode = (TextView) view.findViewById(R.id.TXT_ROURET_RETURN_AIRPORT_TO_CODE);
        txtReturnRouRetAirportFromCode = (TextView) view.findViewById(R.id.TXT_ROURET_RETURN_AIRPORT_FROM_CODE);
        btnSortReturnRouRetDepart = (Button) view.findViewById(R.id.BTN_ROURET_RETURN_DEPART);
        btnSortReturnRouRetPrice = (Button) view.findViewById(R.id.BTN_ROURET_RETURN_PRICE);
        lisViewtReturnRouRetAllFlight = (ListView) view.findViewById(R.id.LIST_ROURET_RETURN_FLIGHT);

        listViewOnwardRouRetAllFlight.setOnItemClickListener(this);
        lisViewtReturnRouRetAllFlight.setOnItemClickListener(this);

        btnSortOnwardRouRetDepart.setOnClickListener(this);
        btnSortOnwardRouRetPrice.setOnClickListener(this);
        btnSortReturnRouRetDepart.setOnClickListener(this);
        btnSortReturnRouRetPrice.setOnClickListener(this);


    }

    private void addSpecialOfferAirline() {

        ArrayList<SpecialOfferAirlineBean> alSpecialOffer = AirDataHolder.getListHolder().getList().get(0).getSpecialOfferFare();

        for (int i = 0; i <alSpecialOffer.size() ; i++) {

            mainHorizontalList.addView(getHorizontalListItemView(i));
        }
    }

    private View getHorizontalListItemView(final Integer index) {


        ArrayList<SpecialOfferAirlineBean> alSpecialOffer = AirDataHolder.getListHolder().getList().get(0).getSpecialOfferFare();

        LayoutInflater inflater = (LayoutInflater) mainActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View item =  inflater.inflate(R.layout.horizontal_list_item,  null);
        final TextView airlineName = (TextView)item.findViewById(R.id.TXT_AIRLINE_NAME);
        final TextView airlineFare = (TextView) item.findViewById(R.id.TXT_AIRLINE_FARE);

        item.setId(index);

        airlineName.setText(alSpecialOffer.get(index).getFlightName());
        airlineFare.setText("\u20B9  " + alSpecialOffer.get(index).getFlightSpecialFare());

        item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                map.put(index, v);
                //change the background color of selected offer
                chnageSelector(v,v.getId(),airlineName,airlineFare);
                previousSpecialOfferPosition = index;
                createViewAccordToSpecialOfferClicked(v.getId());

            }
        });

        return item;
    }

    private void chnageSelector(View v, int selectedPosetion, TextView airlineName, TextView airlineFare) {


        if(selectedPosetion == previousSpecialOfferPosition){

            v.setBackgroundColor(Color.parseColor("#21A1CE"));
            mainBtnAllFlight.setBackgroundColor(Color.TRANSPARENT);
            mainBtnAllFlight.setTextColor(Color.parseColor("#21A1CE"));
            airlineName.setTextColor(Color.WHITE);
            airlineFare.setTextColor(Color.WHITE);
        }
        else {

            if (previousSpecialOfferPosition != -1) {
                v.setBackgroundColor(Color.parseColor("#21A1CE"));
                View view = map.get(previousSpecialOfferPosition);//
                view.setBackgroundColor(Color.TRANSPARENT);
                TextView pAirlineName = (TextView) view.findViewById(R.id.TXT_AIRLINE_NAME);
                TextView pAirlineFare = (TextView) view.findViewById(R.id.TXT_AIRLINE_FARE);
                pAirlineName.setTextColor(Color.parseColor("#21A1CE"));
                pAirlineFare.setTextColor(Color.parseColor("#21A1CE"));

                mainBtnAllFlight.setBackgroundColor(Color.TRANSPARENT);
                mainBtnAllFlight.setTextColor(Color.parseColor("#21A1CE"));
                airlineName.setTextColor(Color.WHITE);
                airlineFare.setTextColor(Color.WHITE);
            } else {
                v.setBackgroundColor(Color.parseColor("#21A1CE"));
                mainBtnAllFlight.setBackgroundColor(Color.TRANSPARENT);
                mainBtnAllFlight.setTextColor(Color.parseColor("#21A1CE"));
                airlineName.setTextColor(Color.WHITE);
                airlineFare.setTextColor(Color.WHITE);
            }
        }
    }

    private void createViewAccordToSpecialOfferClicked(int position) {

        // WE DECIDE WHICH LAYOUT IS SHOW ON THE SCREEN

        ArrayList<SpecialOfferAirlineBean> alSpecialOffer = AirDataHolder.getListHolder().getList().get(0).getSpecialOfferFare();
        //SpecialOfferAirlineBean specialOfferAirline = alSpecialOffer.get(position);
        flightCode = alSpecialOffer.get(position).getFlightCode();

        if(!"6E_0S_G8_AK".contains(flightCode)){

            //HERE WE SHOW THE DATA FROM ROUND_ONWARD LAYOUT IN SINGLE LISTVIEW
            roundOnwardLayout();

        }else{

            //HERE WE SHOW THE DATA FROM ROUND_RETURN LAYOUT IN DOUBLE LISTVIEW
            roundReturnLayout();
        }

        /*if(!"6E_0S_G8_AK".contains(specialOfferAirline.getFlightCode())){

            //HERE WE SHOW THE DATA FROM ROUND_ONWARD LAYOUT IN SINGLE LISTVIEW
            roundOnwardLayout(specialOfferAirline.getFlightCode());

        }else{

            //HERE WE SHOW THE DATA FROM ROUND_RETURN LAYOUT IN DOUBLE LISTVIEW
            roundReturnLayout(specialOfferAirline.getFlightCode());
        }*/
    }

    private void mainReturnLayout(){

        mainActivity.SCREEN_TYPE = IS_ROUNDTRIP_ONWARD_RETURN;

        mainReturnOnwPosition = 0;
        mainReturnRetPosition = 0;
        mainListViewOnwardFlights.setItemChecked(0, true);
        mainListViewReturnFlights.setItemChecked(0, true);

        //  set the visibilty gone
        layRouRetContent.setVisibility(View.GONE);
        layRouOnwContent.setVisibility(View.GONE);
        mainLayReturnContent.setVisibility(View.VISIBLE);

        setDataInMainReturnVarFromHolder();
        // SET  THE TOTAL FARE AGAIN
        setMainReturnFirstFlightsPrice(mainReturnOnwPosition, mainReturnRetPosition);
    }

    private void roundOnwardLayout(){

        mainActivity.SCREEN_TYPE = IS_ROUNDTRIP_ROUND_ONWARD;
        rouOnwPosition = 0;
        specialOfferCreater = false;
        listViewRouOnwAllFlight.setItemChecked(0, true);

        //  set the visibilty gone
        mainLayReturnContent.setVisibility(View.GONE);
        layRouRetContent.setVisibility(View.GONE);
        layRouOnwContent.setVisibility(View.VISIBLE);

        specificRoundOnwardFlight = new ArrayList<>();

        // HERE WE GET THE GDS FLIGHTS
        ArrayList<FlightOnWardDetailBean> allRoundOnwardFlight = new ArrayList<>();
        if(!mainActivity.isComingFromRoundFilterDialog){

            allRoundOnwardFlight = (ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightGDS().clone();
        }else{

            allRoundOnwardFlight =  alFliterGDSFlight;
            mainActivity.isComingFromRoundFilterDialog = false;
        }
        //ArrayList<FlightOnWardDetailBean> allRoundOnwardFlight = (ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getRouOnwalAllFlightDetails().clone();

        for (int i = 0; i < allRoundOnwardFlight.size() ; i++) {

            FlightOnWardDetailBean flightDetailBean = allRoundOnwardFlight.get(i);

            if(flightDetailBean.getFlightAirline().equals(flightCode)){
                specificRoundOnwardFlight.add(flightDetailBean);
            }
        }
        // HERE WE SET THE DATA INTO  ROUND ONWARD VARIABLES FROM MAIN HOLDER CLONE
        setDataInRoundOnwardVarFromHolder();
    }

    // GET THE SELECTED GDS FLIGHT FROM ALL GDS FLIGHTS
    public ArrayList<FlightOnWardDetailBean> getSpecificGdsFlight(ArrayList<FlightOnWardDetailBean> allRoundOnwardFlight){

        specificRoundOnwardFlight.clear();
        for (int i = 0; i < allRoundOnwardFlight.size() ; i++) {

            FlightOnWardDetailBean flightDetailBean = allRoundOnwardFlight.get(i);

            if(flightDetailBean.getFlightAirline().equals(flightCode)){
                specificRoundOnwardFlight.add(flightDetailBean);
            }
        }
        return specificRoundOnwardFlight;
    }

    // GET THE SELECTED LCC FLIGHT FROM ALL ROUND ONWARD  & ROUND RETURN FLIGHTS
    private void getSelectedLccFlight(ArrayList<FlightOnWardDetailBean> allLccRoundOnwardFlight, ArrayList<FlightOnWardDetailBean> allLccRoundReturnFlight){


        rouRetOnwardFlight.clear();
        rouRetReturnFlight.clear();

        for (int i = 0; i < allLccRoundOnwardFlight.size() ; i++) {

            FlightOnWardDetailBean flightDetailBean = allLccRoundOnwardFlight.get(i);

            if(flightDetailBean.getFlightAirline().equals(flightCode)){
                rouRetOnwardFlight.add(flightDetailBean);
            }
        }

        for (int i = 0; i < allLccRoundReturnFlight.size() ; i++) {

            FlightOnWardDetailBean flightDetailBean = allLccRoundReturnFlight.get(i);

            if(flightDetailBean.getFlightAirline().equals(flightCode)){
                rouRetReturnFlight.add(flightDetailBean);
            }
        }
    }

    private void roundReturnLayout(){

        mainActivity.SCREEN_TYPE = IS_ROUND_RETURN_SCREEN;
        specialOfferCreater = false;
        rouRetOnwardPosition = 0;
        rouRetReturnPosition = 0;
        lisViewtReturnRouRetAllFlight.setItemChecked(0, true);
        listViewOnwardRouRetAllFlight.setItemChecked(0, true);

        //  set the visibilty gone
        mainLayReturnContent.setVisibility(View.GONE);
        layRouOnwContent.setVisibility(View.GONE);
        layRouRetContent.setVisibility(View.VISIBLE);

        rouRetOnwardFlight = new ArrayList<>();
        rouRetReturnFlight = new ArrayList<>();

        ArrayList<FlightOnWardDetailBean> allRouRetOnwardFlight = new ArrayList<>(); //AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightLCC().clone();
        ArrayList<FlightOnWardDetailBean> allRouRetReturnFlight = new ArrayList<>(); //(ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getRouRetalAllFlightDetails().clone();

        if(!mainActivity.isComingFromRoundFilterDialog){

            allRouRetOnwardFlight = (ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightLCC().clone();
            allRouRetReturnFlight = (ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getRouRetalAllFlightDetails().clone();

        }
        else{

            allRouRetOnwardFlight =  alFliterRouRetOnwardFlight;
            allRouRetReturnFlight =  alFliterRouRetReturnFlight;
            mainActivity.isComingFromRoundFilterDialog = false;
        }

        for (int i = 0; i < allRouRetOnwardFlight.size() ; i++) {

            FlightOnWardDetailBean flightDetailBean = allRouRetOnwardFlight.get(i);

            if(flightDetailBean.getFlightAirline().equals(flightCode)){
                rouRetOnwardFlight.add(flightDetailBean);
            }
        }

        for (int i = 0; i < allRouRetReturnFlight.size() ; i++) {

            FlightOnWardDetailBean flightDetailBean = allRouRetReturnFlight.get(i);

            if(flightDetailBean.getFlightAirline().equals(flightCode)){
                rouRetReturnFlight.add(flightDetailBean);
            }
        }
        // HERE WE SET THE DATA INTO  ROUND RETURN VARIABLES FROM MAIN HOLDER CLONE
        setDataInRoundReturnVarFromHolder();
    }

    private void defaultMainOnwardSortingOnFare() {

        Collections.sort(mainReturnFlightOnWardMainHolderClone, new FarePriceSort());
        Drawable img = getContext().getResources().getDrawable( R.drawable.up_arrow );
        img.setBounds(1, 1, 16, 23);
        mainBtnSortOnwardPrice.setCompoundDrawables(null, null, img, null);
        btnOnwardPrice = true;
        mainSortOnwardButtonHandler(2, mainBtnSortOnwardPrice);
    }

    private void defaultMainReturnSortingOnFare() {

        Collections.sort(mainReturnFlightReturnMainHolderClone, new FarePriceSort());
        Drawable img = getContext().getResources().getDrawable( R.drawable.up_arrow );
        img.setBounds(1, 1, 16, 23);
        mainBtnSortReturnPrice.setCompoundDrawables(null, null, img, null);
        btnReturnPrice = true;
        mainSortReturnButtonHandler(2, mainBtnSortReturnPrice);
    }

    private void defaultRoundOnwardSortingOnFare() {

        Collections.sort(specificRoundOnwardFlight, new FarePriceSort());
        btnRouOnwPrice = true;
        Drawable img = getContext().getResources().getDrawable( R.drawable.up_arrow );
        img.setBounds(1, 1, 16, 23);
        btnSortRouOnwPrice.setCompoundDrawables(null, null, img, null);
        rouOnwSortButtonHandler(5, btnSortRouOnwPrice);
    }

    private void defaultRouRetOnwardSortingOnFare() {

        Collections.sort(rouRetOnwardFlight, new FarePriceSort());
        Drawable img = getContext().getResources().getDrawable( R.drawable.up_arrow );
        img.setBounds(1, 1, 16, 23);
        btnSortOnwardRouRetPrice.setCompoundDrawables(null, null, img, null);
        btnOnwardRouRetPrice = true;
        rouRetSortOnwardButtonHandler(2, btnSortOnwardRouRetPrice);
    }

    private void defaultRouRetReturnSortingOnFare() {

        Collections.sort(rouRetReturnFlight, new FarePriceSort());
        Drawable img = getContext().getResources().getDrawable( R.drawable.up_arrow );
        img.setBounds(1, 1, 16, 23);
        btnSortReturnRouRetPrice.setCompoundDrawables(null, null, img, null);
        btnReturnRouRetPrice = true;
        rouRetSortReturnButtonHandler(2, btnSortReturnRouRetPrice);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        switch (parent.getId()){

         // LIST VIEW FOR THE MAIN RETURN SCREEN
            case R.id.LIST_MAIN_ONWARD_FLIGHT:

                mainListViewOnwardFlights.setSelector(R.drawable.list_selector);
                mainReturnOnwPosition = position;
                setMainReturnFirstFlightsPrice(mainReturnOnwPosition, mainReturnRetPosition);
                break;

            case R.id.LIST_MAIN_RETURN_FLIGHT:

                mainListViewReturnFlights.setSelector(R.drawable.list_selector);
                mainReturnRetPosition = position;
                setMainReturnFirstFlightsPrice(mainReturnOnwPosition, mainReturnRetPosition);
                break;

         // LIST VIEW FOR THE ROUND ONWARD  SCREEN

            case R.id.LIST_ROUONW_ALL_FLIGHTS:
                listViewRouOnwAllFlight.setSelector(R.drawable.list_selector);
                rouOnwPosition = position;
                setRoundOnwardFirstFlightsPrice(rouOnwPosition, 0);
                break;

         // LIST VIEW FOR THE ROUND  RETURN SCREEN

            case R.id.LIST_ROURET_ONWARD_FLIGHT:

                listViewOnwardRouRetAllFlight.setSelector(R.drawable.list_selector);
                rouRetOnwardPosition = position;
                setRoundReturnFirstFlightsPrice(rouRetOnwardPosition, rouRetReturnPosition);
                break;

            case R.id.LIST_ROURET_RETURN_FLIGHT:

                lisViewtReturnRouRetAllFlight.setSelector(R.drawable.list_selector);
                rouRetReturnPosition = position;
                setRoundReturnFirstFlightsPrice(rouRetOnwardPosition, rouRetReturnPosition);
                break;

            default:
                break;
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.FLIGHT_INFO:



                switch (mainActivity.SCREEN_TYPE){

                    case IS_ROUNDTRIP_ONWARD_RETURN:

                        if( mainReturnFlightOnWardMainHolderClone.size() > 0 && mainReturnFlightReturnMainHolderClone.size() > 0) {

                            showSelectedFlightDetailsDialog(mainReturnFlightOnWardMainHolderClone, mainReturnFlightReturnMainHolderClone);
                        } else{

                            new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.FLIGHT_BOOK_ERROR), mainActivity.getResources().getString(R.string.FLIGHT_INFO), Constant.ALERT_ACTION_ONE).generateAlert();
                        }

                        break;

                    case IS_ROUNDTRIP_ROUND_ONWARD:


                        if( specificRoundOnwardFlight.size() > 0 ) {

                            showSelectedFlightDetailsDialog(specificRoundOnwardFlight,null);
                        } else{

                            new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.FLIGHT_BOOK_ERROR), mainActivity.getResources().getString(R.string.FLIGHT_INFO), Constant.ALERT_ACTION_ONE).generateAlert();
                        }

                        break;

                    case IS_ROUND_RETURN_SCREEN:

                        if( rouRetOnwardFlight.size() > 0 && rouRetReturnFlight.size() > 0) {

                            showSelectedFlightDetailsDialog(rouRetOnwardFlight, rouRetReturnFlight);
                        } else{

                            new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.FLIGHT_BOOK_ERROR), mainActivity.getResources().getString(R.string.FLIGHT_INFO), Constant.ALERT_ACTION_ONE).generateAlert();
                        }

                        break;
                }
                break;


            // MAIN RETURN SCREEN CLICK LISTENER HANDLING
            case R.id.BTN_MAIN_ALL_FLIGHT:

                if (previousSpecialOfferPosition != -1) {
                    mainReturnLayout();
                    View view = map.get(previousSpecialOfferPosition);//
                    view.setBackgroundColor(Color.TRANSPARENT);
                    TextView pAirlineName = (TextView) view.findViewById(R.id.TXT_AIRLINE_NAME);
                    TextView pAirlineFare = (TextView) view.findViewById(R.id.TXT_AIRLINE_FARE);
                    pAirlineName.setTextColor(Color.parseColor("#21A1CE"));
                    pAirlineFare.setTextColor(Color.parseColor("#21A1CE"));
                    mainBtnAllFlight.setBackgroundColor(Color.parseColor("#21A1CE"));
                    mainBtnAllFlight.setTextColor(Color.WHITE);
                }
                break;

            case R.id.BTN_MAIN_ONWARD_DEPART:

                if(!btnOnwardDepart){

                    Collections.sort(mainReturnFlightOnWardMainHolderClone, new DepartTimeSort());
                    mainListViewOnwardFlights.invalidateViews();
                    btnOnwardDepart = true;

                    Drawable img = getContext().getResources().getDrawable( R.drawable.up_arrow );
                    img.setBounds(1, 1, 16, 23);
                    mainBtnSortOnwardDepart.setCompoundDrawables(null, null, img, null);
                }
                else{

                    Collections.reverse(mainReturnFlightOnWardMainHolderClone);
                    btnOnwardDepart = false;
                    mainListViewOnwardFlights.invalidateViews();

                    Drawable img = getContext().getResources().getDrawable( R.drawable.down_arrow);
                    img.setBounds(1, 1, 16, 23);
                    mainBtnSortOnwardDepart.setCompoundDrawables(null, null, img, null);
                }
                setMainReturnFirstFlightsPrice(mainReturnOnwPosition, mainReturnRetPosition);
                //setMainReturnFirstFlightsPrice(0, 0);
                mainSortOnwardButtonHandler(1, mainBtnSortOnwardDepart);
                break;

            case R.id.BTN_MAIN_ONWARD_PRICE:

                if(!btnOnwardPrice){

                    Collections.sort(mainReturnFlightOnWardMainHolderClone, new FarePriceSort());
                    mainListViewOnwardFlights.invalidateViews();
                    btnOnwardPrice = true;

                    Drawable img = getContext().getResources().getDrawable( R.drawable.up_arrow );
                    img.setBounds(1, 1, 16, 23);
                    mainBtnSortOnwardPrice.setCompoundDrawables(null, null, img, null);
                    btnOnwardPrice = true;
                }
                else{

                    Collections.reverse(mainReturnFlightOnWardMainHolderClone);
                    btnOnwardPrice = false;
                    mainListViewOnwardFlights.invalidateViews();

                    Drawable img = getContext().getResources().getDrawable( R.drawable.down_arrow);
                    img.setBounds(1, 1, 16, 23);
                    mainBtnSortOnwardPrice.setCompoundDrawables(null, null, img, null);
                }
                setMainReturnFirstFlightsPrice(mainReturnOnwPosition, mainReturnRetPosition);
                //setMainReturnFirstFlightsPrice(0, 0);
                mainSortOnwardButtonHandler(2, mainBtnSortOnwardPrice);

                break;


            case R.id.BTN_MAIN_RETURN_DEPART:

                if(!btnReturnDepart){

                    Collections.sort(mainReturnFlightReturnMainHolderClone, new DepartTimeSort());
                    mainListViewReturnFlights.invalidateViews();
                    btnReturnDepart = true;

                    Drawable img = getContext().getResources().getDrawable( R.drawable.up_arrow );
                    img.setBounds(1, 1, 16, 23);
                    mainBtnSortReturnDepart.setCompoundDrawables(null, null, img, null);
                }
                else{

                    Collections.reverse(mainReturnFlightReturnMainHolderClone);
                    btnReturnDepart = false;
                    mainListViewReturnFlights.invalidateViews();

                    Drawable img = getContext().getResources().getDrawable( R.drawable.down_arrow);
                    img.setBounds(1, 1, 16, 23);
                    mainBtnSortReturnDepart.setCompoundDrawables(null, null, img, null);
                }

//                private int mainReturnOnwPosition;
//                private int mainReturnRetPosition;
                setMainReturnFirstFlightsPrice(mainReturnOnwPosition, mainReturnRetPosition);
                mainSortReturnButtonHandler(1, mainBtnSortReturnDepart);
                break;

            case R.id.BTN_MAIN_RETURN_PRICE:

                if(!btnReturnPrice){

                    Collections.sort(mainReturnFlightReturnMainHolderClone, new FarePriceSort());
                    mainListViewReturnFlights.invalidateViews();
                    btnReturnPrice = true;

                    Drawable img = getContext().getResources().getDrawable( R.drawable.up_arrow );
                    img.setBounds(1, 1, 16, 23);
                    mainBtnSortReturnPrice.setCompoundDrawables(null, null, img, null);
                    btnReturnPrice = true;
                }
                else{

                    Collections.reverse(mainReturnFlightReturnMainHolderClone);
                    btnReturnPrice = false;
                    mainListViewReturnFlights.invalidateViews();

                    Drawable img = getContext().getResources().getDrawable( R.drawable.down_arrow);
                    img.setBounds(1, 1, 16, 23);
                    mainBtnSortReturnPrice.setCompoundDrawables(null, null, img, null);
                }
                setMainReturnFirstFlightsPrice(mainReturnOnwPosition, mainReturnRetPosition);
                mainSortReturnButtonHandler(2, mainBtnSortReturnPrice);
                break;

        // ROUND ONWARD SCREEN CLICK LISTENER HANDLING
            case R.id.LAY_MAIN_RETURN_CONTENT:
                break;

            case R.id.BTN_ROUONW_AIRLINE:

                if(!btnRouOnwAirline){

                    Collections.sort(specificRoundOnwardFlight, new AirFlightSort());
                    listViewRouOnwAllFlight.invalidateViews();
                    btnRouOnwAirline = true;

                    Drawable img = getContext().getResources().getDrawable(R.drawable.up_arrow);
                    img.setBounds(1, 1, 16, 23);
                    btnSortRouOnwAirline.setCompoundDrawables(null, null, img, null);

                }
                else{

                    Collections.reverse(specificRoundOnwardFlight);
                    listViewRouOnwAllFlight.invalidateViews();
                    btnRouOnwAirline = false;

                    Drawable img = getContext().getResources().getDrawable( R.drawable.down_arrow);
                    img.setBounds(1, 1, 16, 23);
                    btnSortRouOnwAirline.setCompoundDrawables(null, null, img, null);
                }
                setRoundOnwardFirstFlightsPrice(rouOnwPosition, 0);
                rouOnwSortButtonHandler(1, btnSortRouOnwAirline );

                break;

            case R.id.BTN_ROUONW_DEPART:

                if(!btnRouOnwDepart){

                    Collections.sort(specificRoundOnwardFlight, new DepartTimeSort());
                    listViewRouOnwAllFlight.invalidateViews();
                    btnRouOnwDepart = true;

                    Drawable img = getContext().getResources().getDrawable( R.drawable.up_arrow );
                    img.setBounds(1, 1, 16, 23);
                    btnSortRouOnwDepart.setCompoundDrawables(null, null, img, null);
                }
                else{

                    Collections.reverse(specificRoundOnwardFlight);
                    btnRouOnwDepart = false;
                    listViewRouOnwAllFlight.invalidateViews();

                    Drawable img = getContext().getResources().getDrawable( R.drawable.down_arrow);
                    img.setBounds(1, 1, 16, 23);
                    btnSortRouOnwDepart.setCompoundDrawables(null, null, img, null);
                }
                setRoundOnwardFirstFlightsPrice(rouOnwPosition,0);
                rouOnwSortButtonHandler(2, btnSortRouOnwDepart);

                break;
            case R.id.BTN_ROUONW_ARRIVE:

                if(!btnRouOnwArrive){

                    Collections.sort(specificRoundOnwardFlight, new ArrivalTimeSort());
                    listViewRouOnwAllFlight.invalidateViews();
                    btnRouOnwArrive = true;

                    Drawable img = getContext().getResources().getDrawable( R.drawable.up_arrow );
                    img.setBounds(1, 1, 16, 23);
                    btnSortRouOnwArive.setCompoundDrawables(null, null, img, null);
                }
                else{
                    Collections.reverse(specificRoundOnwardFlight);
                    btnRouOnwArrive = false;
                    listViewRouOnwAllFlight.invalidateViews();

                    Drawable img = getContext().getResources().getDrawable( R.drawable.down_arrow);
                    img.setBounds(1, 1, 16, 23);
                    btnSortRouOnwArive.setCompoundDrawables(null, null, img, null);
                }
                setRoundOnwardFirstFlightsPrice(rouOnwPosition,0);
                rouOnwSortButtonHandler(3, btnSortRouOnwArive);

                break;
            case R.id.BTN_ROUONW_DURATION:

                if(!btnRouOnwDuration){

                    Collections.sort(specificRoundOnwardFlight, new DurationTimeSort());
                    listViewRouOnwAllFlight.invalidateViews();
                    btnRouOnwDuration = true;

                    Drawable img = getContext().getResources().getDrawable( R.drawable.up_arrow );
                    img.setBounds(1, 1, 16, 23);
                    btnSortRouOnwDuration.setCompoundDrawables(null, null, img, null);
                }
                else{
                    Collections.reverse(specificRoundOnwardFlight);
                    btnRouOnwDuration = false;
                    listViewRouOnwAllFlight.invalidateViews();

                    Drawable img = getContext().getResources().getDrawable( R.drawable.down_arrow);
                    img.setBounds(1, 1, 16, 23);
                    btnSortRouOnwDuration.setCompoundDrawables(null, null, img, null);
                }
                setRoundOnwardFirstFlightsPrice(rouOnwPosition,0);
                rouOnwSortButtonHandler(4, btnSortRouOnwDuration);
                break;
            case R.id.BTN_ROUONW_PRICE:

                if(!btnRouOnwPrice){

                    Collections.sort(specificRoundOnwardFlight, new FarePriceSort());
                    listViewRouOnwAllFlight.invalidateViews();
                    btnRouOnwPrice = true;

                    Drawable img = getContext().getResources().getDrawable( R.drawable.up_arrow );
                    img.setBounds(1, 1, 16, 23);
                    btnSortRouOnwPrice.setCompoundDrawables(null, null, img, null);
                    btnRouOnwPrice = true;
                }
                else{

                    Collections.reverse(specificRoundOnwardFlight);
                    btnRouOnwPrice = false;
                    listViewRouOnwAllFlight.invalidateViews();

                    Drawable img = getContext().getResources().getDrawable( R.drawable.down_arrow);
                    img.setBounds(1, 1, 16, 23);
                    btnSortRouOnwPrice.setCompoundDrawables(null, null, img, null);
                }
                setRoundOnwardFirstFlightsPrice(rouOnwPosition,0);
                rouOnwSortButtonHandler(5, btnSortRouOnwPrice);
                break;



        // ROUND RETURN SCREEN CLICK LISTENER HANDLING

            case R.id.BTN_ROURET_ONWARD_DEPART:

                if(!btnOnwardRouRetDepart){

                    Collections.sort(rouRetOnwardFlight, new DepartTimeSort());
                    listViewOnwardRouRetAllFlight.invalidateViews();
                    btnOnwardRouRetDepart = true;

                    Drawable img = getContext().getResources().getDrawable( R.drawable.up_arrow );
                    img.setBounds(1, 1, 16, 23);
                    btnSortOnwardRouRetDepart.setCompoundDrawables(null, null, img, null);
                }
                else{

                    Collections.reverse(rouRetOnwardFlight);
                    btnOnwardRouRetDepart = false;
                    listViewOnwardRouRetAllFlight.invalidateViews();

                    Drawable img = getContext().getResources().getDrawable( R.drawable.down_arrow);
                    img.setBounds(1, 1, 16, 23);
                    btnSortOnwardRouRetDepart.setCompoundDrawables(null, null, img, null);
                }

                setRoundReturnFirstFlightsPrice(rouRetOnwardPosition, rouRetReturnPosition);
                //setRoundReturnFirstFlightsPrice(0, 0);
                rouRetSortOnwardButtonHandler(1, btnSortOnwardRouRetDepart);

                break;

            case R.id.BTN_ROURET_ONWARD_PRICE:

                if(!btnOnwardRouRetPrice){

                    Collections.sort(rouRetOnwardFlight, new FarePriceSort());
                    listViewOnwardRouRetAllFlight.invalidateViews();
                    btnOnwardRouRetPrice = true;

                    Drawable img = getContext().getResources().getDrawable( R.drawable.up_arrow );
                    img.setBounds(1, 1, 16, 23);
                    btnSortOnwardRouRetPrice.setCompoundDrawables(null, null, img, null);

                }
                else{

                    Collections.reverse(rouRetOnwardFlight);
                    btnOnwardRouRetPrice = false;
                    listViewOnwardRouRetAllFlight.invalidateViews();

                    Drawable img = getContext().getResources().getDrawable( R.drawable.down_arrow);
                    img.setBounds(1, 1, 16, 23);
                    btnSortOnwardRouRetPrice.setCompoundDrawables(null, null, img, null);
                }

                setRoundReturnFirstFlightsPrice(rouRetOnwardPosition, rouRetReturnPosition);
                //setRoundReturnFirstFlightsPrice(0, 0);
                rouRetSortOnwardButtonHandler(2, btnSortOnwardRouRetPrice);
                break;

            case R.id.BTN_ROURET_RETURN_DEPART:

                if(!btnReturnRouRetDepart){

                    Collections.sort(rouRetReturnFlight, new DepartTimeSort());
                    lisViewtReturnRouRetAllFlight.invalidateViews();
                    btnReturnRouRetDepart = true;

                    Drawable img = getContext().getResources().getDrawable( R.drawable.up_arrow );
                    img.setBounds(1, 1, 16, 23);
                    btnSortReturnRouRetDepart.setCompoundDrawables(null, null, img, null);
                }
                else{

                    Collections.reverse(rouRetReturnFlight);
                    btnReturnRouRetDepart = false;
                    lisViewtReturnRouRetAllFlight.invalidateViews();

                    Drawable img = getContext().getResources().getDrawable( R.drawable.down_arrow);
                    img.setBounds(1, 1, 16, 23);
                    btnSortReturnRouRetDepart.setCompoundDrawables(null, null, img, null);
                }
                setRoundReturnFirstFlightsPrice(rouRetOnwardPosition, rouRetReturnPosition);
                //setRoundReturnFirstFlightsPrice(0, 0);
                rouRetSortReturnButtonHandler(1, btnSortReturnRouRetDepart);
                break;

            case R.id.BTN_ROURET_RETURN_PRICE:

                if(!btnReturnRouRetPrice){

                    Collections.sort(rouRetReturnFlight, new FarePriceSort());
                    lisViewtReturnRouRetAllFlight.invalidateViews();
                    btnReturnRouRetPrice = true;

                    Drawable img = getContext().getResources().getDrawable( R.drawable.up_arrow );
                    img.setBounds(1, 1, 16, 23);
                    btnSortReturnRouRetPrice.setCompoundDrawables(null, null, img, null);
                    btnReturnRouRetPrice = true;
                }
                else{

                    Collections.reverse(rouRetReturnFlight);
                    btnReturnRouRetPrice = false;
                    lisViewtReturnRouRetAllFlight.invalidateViews();

                    Drawable img = getContext().getResources().getDrawable( R.drawable.down_arrow);
                    img.setBounds(1, 1, 16, 23);
                    btnSortReturnRouRetPrice.setCompoundDrawables(null, null, img, null);
                }
                setRoundReturnFirstFlightsPrice(rouRetOnwardPosition, rouRetReturnPosition);
                //setRoundReturnFirstFlightsPrice(0, 0);
                rouRetSortReturnButtonHandler(2, btnSortReturnRouRetPrice);
                break;

            // COMMON CLICK LISTENER HANDLING
            case R.id.LAY_ROUND_BOOKING:



                // CHECK HERE WHERE WE ARE ON A SCREEN LIKE MAIN, ROUONW OR ROURET
                // *** PUT THIS CODE IN BOOKING CLICK BLOCK
                switch ( mainActivity.SCREEN_TYPE ){

                    case IS_ROUNDTRIP_ONWARD_RETURN:

                        filterBean = null;
                        if(mainReturnFlightOnWardMainHolderClone.size() >0 && mainReturnFlightReturnMainHolderClone.size() > 0){

                            if (!ProjectUtil.checkInternetConnection(mainActivity)) {

                                new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.INTERNET_DOWN), mainActivity.getResources().getString(R.string.NO_INTERNET_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                                //ProjectUtil.showToast(getResources().getString(R.string.INTERNET_DOWN), mainActivity);

                            } else {

                                FlightOnWardDetailBean onWardDetailBeanONW = mainReturnFlightOnWardMainHolderClone.get(mainReturnOnwPosition);
                                FlightOnWardDetailBean onWardDetailBeanRET = mainReturnFlightReturnMainHolderClone.get(mainReturnRetPosition);

                                int lOnwUniqID = onWardDetailBeanONW.getFlightUniqueId();
                                int lRetUniqID = onWardDetailBeanRET.getFlightUniqueId();

                                FlightOnWardDetailBean bOnw = AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().get(lOnwUniqID);
                                FlightOnWardDetailBean bRet = AirDataHolder.getListHolder().getList().get(0).getReturnAlAllFlightDetails().get(lRetUniqID);

                                AppLogger.e("RoundONW : ", bOnw.getFlightJson());
                                AppLogger.e("RoundRET : ", bRet.getFlightJson());

                                /***********************
                                 * IF AIR ASIA SET VAR FOR DATE IN ADD PASS
                                 **********************/
                                if (AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().get(lOnwUniqID).getFlightAirline().contains("AK") ||
                                        AirDataHolder.getListHolder().getList().get(0).getReturnAlAllFlightDetails().get(lRetUniqID).getFlightAirline().contains("AK")) {
                                    mainActivity.isAirAsia = true;
                                }
                                // CREATE THE XML FOR THE ROUND TRIP FLIGHTS AVAILABILITY
                                createXMLReqAndCallService(bOnw.getFlightJson(), bRet.getFlightJson());
                            }
                        }
                        else{
                            new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.FLIGHT_BOOK_ERROR), mainActivity.getResources().getString(R.string.FARE_QUOTE_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();

                        }
                        break;

                    case IS_ROUNDTRIP_ROUND_ONWARD:

                        filterBean = null;
                        if(specificRoundOnwardFlight.size() >0 ) {

                            if (!ProjectUtil.checkInternetConnection(mainActivity)) {

                                new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.INTERNET_DOWN), mainActivity.getResources().getString(R.string.NO_INTERNET_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                                //ProjectUtil.showToast(getResources().getString(R.string.INTERNET_DOWN), mainActivity);

                            } else {
                                mainActivity.isGdsOrInternatioal = true;
                                FlightOnWardDetailBean onWardDetailBeanONW2 = specificRoundOnwardFlight.get(rouOnwPosition);

                                int lOnwUniqID2 = onWardDetailBeanONW2.getFlightUniqueId();//getFlightJson();

                                FlightOnWardDetailBean bOnw2 = AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightGDS().get(lOnwUniqID2);

                                /***********************
                                 * IF AIR ASIA SET VAR FOR DATE IN ADD PASS
                                 **********************/
                                if (AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightGDS().get(lOnwUniqID2).getFlightAirline().contains("AK")) {
                                    mainActivity.isAirAsia = true;
                                }


                                //FlightOnWardDetailBean bOnw2 = AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().get(lOnwUniqID2);

                                AppLogger.e("RoundONW : ", bOnw2.getFlightJson());
                                // CREATE THE XML FOR THE ROUND TRIP FLIGHTS AVAILABILITY
                                createXMLReqAndCallService(bOnw2.getFlightJson(), null);
                            }

                        }else{
                            new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.FLIGHT_BOOK_ERROR), mainActivity.getResources().getString(R.string.FARE_QUOTE_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();

                        }
                        break;

                    case IS_ROUND_RETURN_SCREEN:

                        filterBean = null;
                        if(rouRetOnwardFlight.size() >0 && rouRetReturnFlight.size() > 0) {
                            if (!ProjectUtil.checkInternetConnection(mainActivity)) {

                                new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.INTERNET_DOWN), mainActivity.getResources().getString(R.string.NO_INTERNET_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                                //ProjectUtil.showToast(getResources().getString(R.string.INTERNET_DOWN), mainActivity);

                            } else {
                                FlightOnWardDetailBean onWardDetailBeanONW3 = rouRetOnwardFlight.get(rouRetOnwardPosition);
                                FlightOnWardDetailBean onWardDetailBeanRET3 = rouRetReturnFlight.get(rouRetReturnPosition);

                                int lOnwUniqID3 = onWardDetailBeanONW3.getFlightUniqueId();//getFlightJson();
                                int lRetUniqID3 = onWardDetailBeanRET3.getFlightUniqueId();//getFlightJson();

                                FlightOnWardDetailBean bOnw3 = AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightLCC().get(lOnwUniqID3);
                                FlightOnWardDetailBean bRet3 = AirDataHolder.getListHolder().getList().get(0).getRouRetalAllFlightDetails().get(lRetUniqID3);

                                /***********************
                                 * IF AIR ASIA SET VAR FOR DATE IN ADD PASS
                                 **********************/
                                if (AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightLCC().get(lOnwUniqID3).getFlightAirline().contains("AK") ||
                                        AirDataHolder.getListHolder().getList().get(0).getRouRetalAllFlightDetails().get(lRetUniqID3).getFlightAirline().contains("AK")) {
                                    mainActivity.isAirAsia = true;
                                }


                                AppLogger.e("RoundONW : ", bOnw3.getFlightJson());
                                AppLogger.e("RoundRET : ", bRet3.getFlightJson());
                                // CREATE THE XML FOR THE ROUND TRIP FLIGHTS AVAILABILITY

                                createXMLReqAndCallService(bOnw3.getFlightJson(), bRet3.getFlightJson());
                            }
                        }else{
                            new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.FLIGHT_BOOK_ERROR), mainActivity.getResources().getString(R.string.FARE_QUOTE_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();

                        }
                        break;

                    default:
                        break;
                }

                break;
            default:
                break;
        }
    }

    private void showFilterDialog(FlightFilterBean filterBean) {

        switch (mainActivity.SCREEN_TYPE){

            case IS_ROUNDTRIP_ONWARD_RETURN:

                if (!mainActivity.isFinishing()) {

                    lObjDialogShowFilterOption = new Dialog(mainActivity, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
                    mainActivity.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                    lObjDialogShowFilterOption.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    lObjDialogShowFilterOption.getWindow().setBackgroundDrawable(new ColorDrawable(Color.LTGRAY));
                    // Set layout
                    lObjDialogShowFilterOption.setContentView(R.layout.round_trip_filter);

                    // PROVIDE ALL NEW REFRESH ONWARD FLIGHT DATA
                    mainReturnFlightOnWardMainHolderClone = (ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().clone();
                    // PROVIDE ALL NEW REFRESH RETURN FLIGHT DATA
                    mainReturnFlightReturnMainHolderClone = (ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getReturnAlAllFlightDetails().clone();

                    new RoundFilterEventHandler(this,mainActivity,
                            lObjDialogShowFilterOption,
                            mainReturnFlightOnWardMainHolderClone,
                            mainReturnFlightReturnMainHolderClone,
                            mainListViewOnwardFlights,
                            mainListViewReturnFlights,
                            IS_ROUNDTRIP_ONWARD_RETURN,
                            filterBean
                    );
                    // SHOW THE DAILOG
                    lObjDialogShowFilterOption.show();
                }

//                mainReturnFlightOnWardMainHolderClone;
//                mainReturnFlightReturnMainHolderClone;
                break;

            case IS_ROUNDTRIP_ROUND_ONWARD:

                if (!mainActivity.isFinishing()) {

                    lObjDialogShowFilterOption = new Dialog(mainActivity, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
                    mainActivity.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                    lObjDialogShowFilterOption.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    lObjDialogShowFilterOption.getWindow().setBackgroundDrawable(new ColorDrawable(Color.LTGRAY));
                    // Set layout
                    lObjDialogShowFilterOption.setContentView(R.layout.round_trip_filter);

                    // PROVIDE ALL NEW REFRESH GDS FLIGHT DATA
                    getSpecificGdsFlight((ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightGDS().clone());
                    //specificRoundOnwardFlight = (ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightGDS().clone();

                    new RoundFilterEventHandler(this,mainActivity,
                            lObjDialogShowFilterOption,
                            specificRoundOnwardFlight,
                            listViewRouOnwAllFlight,
                            IS_ROUNDTRIP_ROUND_ONWARD,
                            filterBean
                    );
                    // SHOW THE DAILOG
                    lObjDialogShowFilterOption.show();
                }

                //specificRoundOnwardFlight;

                break;

            case IS_ROUND_RETURN_SCREEN:

                if (!mainActivity.isFinishing()) {

                    lObjDialogShowFilterOption = new Dialog(mainActivity, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
                    mainActivity.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
                    lObjDialogShowFilterOption.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    lObjDialogShowFilterOption.getWindow().setBackgroundDrawable(new ColorDrawable(Color.LTGRAY));
                    // Set layout
                    lObjDialogShowFilterOption.setContentView(R.layout.round_trip_filter);

                    // PROVIDE ALL NEW REFRESH LCC FLIGHT DATA
//                    rouRetOnwardFlight = (ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightLCC().clone();
//                    // PROVIDE ALL NEW REFRESH ROUND RETURN FLIGHT DATA
//                    rouRetReturnFlight = (ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getRouRetalAllFlightDetails().clone();

                    //PROVIDE ALL NEW REFRESH LCC FLIGHT DATA

                    getSelectedLccFlight((ArrayList<FlightOnWardDetailBean>)AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightLCC().clone(),(ArrayList<FlightOnWardDetailBean>)AirDataHolder.getListHolder().getList().get(0).getRouRetalAllFlightDetails().clone());

                    new RoundFilterEventHandler(this,
                            mainActivity,
                            lObjDialogShowFilterOption,
                            rouRetOnwardFlight,
                            rouRetReturnFlight,
                            listViewOnwardRouRetAllFlight,
                            lisViewtReturnRouRetAllFlight,
                            IS_ROUND_RETURN_SCREEN,
                            filterBean
                    );
                    // SHOW THE DAILOG
                    lObjDialogShowFilterOption.show();
                }

//                rouRetOnwardFlight;
//                rouRetReturnFlight;
                break;
        }

    }

    // PERFORM ALL THE FILTER OPERATION
    private void performFilterOperation() {
    }

    private void createXMLReqAndCallService(String onwJson, String retJson) {

//        AppLogger.enable();
//        AppLogger.e("ONW => ", onwJson);
//        AppLogger.e("RET => ", retJson);

        ArrayList<AirDataModel> mAirMainHolder = AirDataHolder.getListHolder().getList();

        LinkedHashMap<String,String> mapParams = new LinkedHashMap<String,String>();

        mapParams.put("lccp_clientIP",  new SharedPrefrenceAir(mainActivity).getUserUniqueId());
        mapParams.put("lccp_userId", "0004888570");             // Default

        mapParams.put("lccp_domOrInt", mAirMainHolder.get(0).getTripDomOrInter());
        mapParams.put("lccp_tripType", mAirMainHolder.get(0).getTripType());
        mapParams.put("lccp_origin1", mAirMainHolder.get(0).getFromStationCity()+ ", " +mAirMainHolder.get(0).getFromStation());
        mapParams.put("lccp_origin2", "");
        mapParams.put("lccp_origin3", "");
        mapParams.put("lccp_origin4", "");
        mapParams.put("lccp_origin5", "");
        mapParams.put("lccp_origin6", "");

        mapParams.put("lccp_destination1", mAirMainHolder.get(0).getToStationCity()+ ", " +mAirMainHolder.get(0).getToStation());
        mapParams.put("lccp_destination2", "");
        mapParams.put("lccp_destination3", "");
        mapParams.put("lccp_destination4", "");
        mapParams.put("lccp_destination5", "");
        mapParams.put("lccp_destination6", "");

        //// Wed Apr 20 15:58:48 GMT+05:30 2016 ////
        mapParams.put("lccp_departDay1", mAirMainHolder.get(0).getDepDate().split(" ")[2]);
        mapParams.put("lccp_departDay2", "");
        mapParams.put("lccp_departDay3", "");
        mapParams.put("lccp_departDay4", "");
        mapParams.put("lccp_departDay5", "");
        mapParams.put("lccp_departDay6", "");


        mapParams.put("lccp_departMonth1", DateUtility.getMonthInInt(mAirMainHolder.get(0).getDepDate()));
        mapParams.put("lccp_departMonth2", "");
        mapParams.put("lccp_departMonth3", "");
        mapParams.put("lccp_departMonth4", "");
        mapParams.put("lccp_departMonth5", "");
        mapParams.put("lccp_departMonth6", "");

        mapParams.put("lccp_departYear1", mAirMainHolder.get(0).getDepDate().split(" ")[5]);
        mapParams.put("lccp_departYear2", "");
        mapParams.put("lccp_departYear3", "");
        mapParams.put("lccp_departYear4", "");
        mapParams.put("lccp_departYear5", "");
        mapParams.put("lccp_departYear6", "");

        if (mainActivity.isOneWaySelected) {
            mapParams.put("lccp_returnDay", "");
            mapParams.put("lccp_returnMonth", "");
            mapParams.put("lccp_returnYear", "");
        }else {
            mapParams.put("lccp_returnDay", mAirMainHolder.get(0).getReturnDate().split(" ")[2]);
            mapParams.put("lccp_returnMonth", DateUtility.getMonthInInt(mAirMainHolder.get(0).getReturnDate()));
            mapParams.put("lccp_returnYear", mAirMainHolder.get(0).getReturnDate().split(" ")[5]);
        }

        mapParams.put("lccp_noOfAdults", String.valueOf(mAirMainHolder.get(0).getAdultPassNum()));
        mapParams.put("lccp_noOfChildren", String.valueOf(mAirMainHolder.get(0).getChildPassNum()));
        mapParams.put("lccp_noOfInfants", String.valueOf(mAirMainHolder.get(0).getInfantPassNum()));

        mapParams.put("lccp_classType", mAirMainHolder.get(0).getTravelClass());

        mapParams.put("lccp_airlinePreference", "ALL");     // BCZ web _ selectipn pref.
        if(mainActivity.isComingFromSideLTC) {
            mapParams.put("lccp_isLTC", "true");
        }else{
            mapParams.put("lccp_isLTC", "false");
        }
 /*??*/         mapParams.put("lccp_oACCodeString", "");
        mapParams.put("lccp_oACAirlineXML", "");
 /*??*/         mapParams.put("lccp_airLineCodeString", ""); // ??
 /*??*/         mapParams.put("lccp_airPortCodeString", "");

        mapParams.put("lccp_railTransactionID", "");

 /*??*/       mapParams.put("lccp_flightId", "");
        mapParams.put("lccp_flightIdJson", "");
        mapParams.put("lccp_returnFlightId", "");
        mapParams.put("lccp_returnFlightIdJson", "");

        mapParams.put("lccp_jsonstring", "");
        mapParams.put("lccp_jsretstring", "");
        mapParams.put("lccp_roundOnwjson", "");
        mapParams.put("lccp_roundRetjson", "");

        mapParams.put("lccp_searchType", "normal");

        if(mainActivity.SCREEN_TYPE == IS_ROUNDTRIP_ROUND_ONWARD){
            mapParams.put("lccp_searchType", "oneway-return");
        }


        // If special round trip booking  : One way return

        mapParams.put("lccp_fromCache", String.valueOf(AirDataHolder.getListHolder().getList().get(0).isFlightOnWardFromCache()));       // Dynamic from F Search result

        mapParams.put("lccp_isDistress", "false");
        mapParams.put("lccp_isLCCRoundBook", "false");

        if(mainActivity.SCREEN_TYPE ==  IS_ROUND_RETURN_SCREEN){
            mapParams.put("lccp_isLCCRoundBook", "true");
        }

        String req = new ProjectUtil().convertMapToXml1(mapParams, "WSFareQuote");
        AppLogger.enable();
        AppLogger.e("REQ => ",req);


        // Round Trip FareQuoteService search service call
       /* FareQuoteRoundTripService auth = new FareQuoteRoundTripService(

                mainActivity,
                onwJson,
                retJson,
                new ProjectUtil().convertMapToXml1(mapParams, "WSFareQuote")
        );
        auth.execute();
*/
    }

    private void mainSortOnwardButtonHandler(int buttonNo, Button button){


        switch(buttonNo){


            case 1:
                // Set background Depart
                button.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLightBlue));
                button.setTextColor(mainActivity.getResources().getColor(R.color.colorWhite));

                mainBtnSortOnwardPrice.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorWhite));

                mainBtnSortOnwardPrice.setTextColor(mainActivity.getResources().getColor(R.color.colorLightBlue));

                mainBtnSortOnwardPrice.setCompoundDrawables(null, null, null, null);

                btnOnwardPrice = false;
                break;

            case 2:

                // Set background Price

                button.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLightBlue));
                button.setTextColor(mainActivity.getResources().getColor(R.color.colorWhite));

                mainBtnSortOnwardDepart.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorWhite));

                mainBtnSortOnwardDepart.setTextColor(mainActivity.getResources().getColor(R.color.colorLightBlue));

                mainBtnSortOnwardDepart.setCompoundDrawables(null, null, null, null);

                btnOnwardDepart = false;


                break;
            default:
                break;

        }
    }

    private void mainSortReturnButtonHandler(int buttonNo, Button button){


        switch(buttonNo){

            case 1:
                // Set background Depart
                button.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLightBlue));
                button.setTextColor(mainActivity.getResources().getColor(R.color.colorWhite));

                mainBtnSortReturnPrice.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorWhite));

                mainBtnSortReturnPrice.setTextColor(mainActivity.getResources().getColor(R.color.colorLightBlue));

                mainBtnSortReturnPrice.setCompoundDrawables(null, null, null, null);

                btnReturnPrice = false;
                break;

            case 2:

                // Set background Price
                button.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLightBlue));
                button.setTextColor(mainActivity.getResources().getColor(R.color.colorWhite));

                mainBtnSortReturnDepart.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorWhite));

                mainBtnSortReturnDepart.setTextColor(mainActivity.getResources().getColor(R.color.colorLightBlue));

                mainBtnSortReturnDepart.setCompoundDrawables(null, null, null, null);

                btnReturnDepart = false;


                break;
            default:
                break;

        }
    }

    private void rouOnwSortButtonHandler(int buttonNo, Button button){

        switch(buttonNo){

            case 1:
                // Set background Airline
                button.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLightBlue));
                button.setTextColor(mainActivity.getResources().getColor(R.color.colorWhite));

                btnSortRouOnwDepart.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwArive.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwDuration.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwPrice.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));

                btnSortRouOnwDepart.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwArive.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwDuration.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwPrice.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));

                btnSortRouOnwDepart.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwArive.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwDuration.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwPrice.setCompoundDrawables(null, null, null, null);

                btnRouOnwDepart = false;
                btnRouOnwArrive = false;
                btnRouOnwDuration = false;
                btnRouOnwPrice = false;

                break;
            case 2:
                // Set background Depart
                button.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLightBlue));
                button.setTextColor(mainActivity.getResources().getColor(R.color.colorWhite));

                btnSortRouOnwAirline.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwArive.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwDuration.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwPrice.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));

                btnSortRouOnwAirline.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwArive.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwDuration.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwPrice.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));

                btnSortRouOnwAirline.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwArive.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwDuration.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwPrice.setCompoundDrawables(null, null, null, null);

                btnRouOnwAirline = false;
                btnRouOnwArrive = false;
                btnRouOnwDuration = false;
                btnRouOnwPrice = false;


                break;
            case 3:
                // Set background Arrive
                button.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLightBlue));
                button.setTextColor(mainActivity.getResources().getColor(R.color.colorWhite));

                btnSortRouOnwAirline.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwDepart.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwDuration.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwPrice.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));

                btnSortRouOnwAirline.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwDepart.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwDuration.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwPrice.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));

                btnSortRouOnwAirline.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwDepart.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwDuration.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwPrice.setCompoundDrawables(null, null, null, null);

                btnRouOnwAirline = false;
                btnRouOnwDepart = false;
                btnRouOnwDuration = false;
                btnRouOnwPrice = false;

                break;
            case 4:
                // Set background Duration
                button.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLightBlue));
                button.setTextColor(mainActivity.getResources().getColor(R.color.colorWhite));

                btnSortRouOnwAirline.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwDepart.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwArive.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwPrice.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));

                btnSortRouOnwAirline.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwDepart.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwArive.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwPrice.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));

                btnSortRouOnwAirline.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwDepart.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwArive.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwPrice.setCompoundDrawables(null, null, null, null);

                btnRouOnwAirline = false;
                btnRouOnwDepart = false;
                btnRouOnwArrive = false;
                btnRouOnwPrice = false;

                break;
            case 5:

                // Set background Price
                button.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLightBlue));
                button.setTextColor(mainActivity.getResources().getColor(R.color.colorWhite));

                btnSortRouOnwAirline.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwDepart.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwArive.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwDuration.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));

                btnSortRouOnwAirline.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwDepart.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwArive.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwDuration.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));

                btnSortRouOnwAirline.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwDepart.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwArive.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwDuration.setCompoundDrawables(null, null, null, null);

                btnRouOnwAirline = false;
                btnRouOnwDepart = false;
                btnRouOnwArrive = false;
                btnRouOnwDuration = false;

                break;
            default:
                break;

        }
    }

    private void rouRetSortOnwardButtonHandler(int buttonNo, Button button){

        switch(buttonNo){

            case 1:
                // Set background Depart
                button.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLightBlue));
                button.setTextColor(mainActivity.getResources().getColor(R.color.colorWhite));

                btnSortOnwardRouRetPrice.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorWhite));

                btnSortOnwardRouRetPrice.setTextColor(mainActivity.getResources().getColor(R.color.colorLightBlue));

                btnSortOnwardRouRetPrice.setCompoundDrawables(null, null, null, null);

                btnOnwardRouRetPrice = false;
                break;

            case 2:

                // Set background Price

                button.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLightBlue));
                button.setTextColor(mainActivity.getResources().getColor(R.color.colorWhite));

                btnSortOnwardRouRetDepart.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorWhite));

                btnSortOnwardRouRetDepart.setTextColor(mainActivity.getResources().getColor(R.color.colorLightBlue));

                btnSortOnwardRouRetDepart.setCompoundDrawables(null, null, null, null);

                btnOnwardRouRetDepart = false;


                break;
            default:
                break;
        }
    }

    private void rouRetSortReturnButtonHandler(int buttonNo, Button button) {

        switch (buttonNo) {

            case 1:
                // Set background Depart
                button.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLightBlue));
                button.setTextColor(mainActivity.getResources().getColor(R.color.colorWhite));

                btnSortReturnRouRetPrice.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorWhite));

                btnSortReturnRouRetPrice.setTextColor(mainActivity.getResources().getColor(R.color.colorLightBlue));

                btnSortReturnRouRetPrice.setCompoundDrawables(null, null, null, null);

                btnReturnRouRetPrice = false;
                break;

            case 2:

                // Set background Price
                button.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLightBlue));
                button.setTextColor(mainActivity.getResources().getColor(R.color.colorWhite));

                btnSortReturnRouRetDepart.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorWhite));

                btnSortReturnRouRetDepart.setTextColor(mainActivity.getResources().getColor(R.color.colorLightBlue));

                btnSortReturnRouRetDepart.setCompoundDrawables(null, null, null, null);

                btnReturnRouRetDepart = false;

                break;
            default:
                break;
        }
    }

    private void setMainReturnFirstFlightsPrice(int onwPosition, int retPosition){

        if (mainReturnFlightOnWardMainHolderClone.size() != 0 && mainReturnFlightReturnMainHolderClone.size() != 0) {
            FlightOnWardDetailBean lOnwflightDetails = mainReturnFlightOnWardMainHolderClone.get(onwPosition);
            FlightOnWardDetailBean lRetflightDetails = mainReturnFlightReturnMainHolderClone.get(retPosition);
            txtTotalPrice.setText("\u20B9  " + (lOnwflightDetails.getFlightFare() + lRetflightDetails.getFlightFare()));
        }else{
            txtTotalPrice.setText("\u20B9  " + "00");
        }
    }

    private void setRoundOnwardFirstFlightsPrice(int onwPosition, int retPosition){
        if (specificRoundOnwardFlight.size() != 0 ) {

            FlightOnWardDetailBean lRouOnwflightDetails = specificRoundOnwardFlight.get(onwPosition);
            txtTotalPrice.setText("\u20B9  " + (lRouOnwflightDetails.getFlightFare()));
        }
        else{
            txtTotalPrice.setText("\u20B9  " + "00");
        }
    }

    private void setRoundReturnFirstFlightsPrice(int onwPosition, int retPosition) {

        if (rouRetOnwardFlight.size() != 0 && rouRetReturnFlight.size() != 0) {

            FlightOnWardDetailBean lOnwardRouRetFlightDetails = rouRetOnwardFlight.get(onwPosition);
            FlightOnWardDetailBean lReturnRouRetflightDetails = rouRetReturnFlight.get(retPosition);
            txtTotalPrice.setText("\u20B9  " + (lOnwardRouRetFlightDetails.getFlightFare() + lReturnRouRetflightDetails.getFlightFare()));
        }else{
            txtTotalPrice.setText("\u20B9  " + "00");

        }
    }
    @Override
    public void onResume() {
        super.onResume();



        ActivityMain.activeFragment = 31;
    }

    @Override
    public void doPerform(ArrayList<FlightOnWardDetailBean> onwrdClone, ArrayList<FlightOnWardDetailBean> returnClone) {

        switch(mainActivity.SCREEN_TYPE){

            case IS_ROUNDTRIP_ONWARD_RETURN:

                mainReturnFlightOnWardMainHolderClone = onwrdClone;
                mainReturnFlightReturnMainHolderClone = returnClone;
                mainReturnLayout();
                break;

            case IS_ROUNDTRIP_ROUND_ONWARD:
                alFliterGDSFlight = onwrdClone;
                roundOnwardLayout();
                break;

            case IS_ROUND_RETURN_SCREEN:

                alFliterRouRetOnwardFlight = onwrdClone;
                alFliterRouRetReturnFlight = returnClone;
                roundReturnLayout();
                break;
        }


    }

//    private void showSelectedFlightDetailsDialog(ArrayList<FlightOnWardDetailBean> lObjalOnwards){
//
//        int positionOnw = lObjalOnwards.get(rouOnwPosition).getFlightUniqueId();
//
//        lObjDialogShowFlightDetails = new Dialog(mainActivity, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
//        mainActivity.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
//        lObjDialogShowFlightDetails.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        lObjDialogShowFlightDetails.getWindow().setBackgroundDrawable(new ColorDrawable(Color.LTGRAY));
//
//        // Set layout
//        lObjDialogShowFlightDetails.setContentView(R.layout.round_trip_gds_flight_info);
//
//
//        ///// INITIALIZE VAR START //////
//        TextView mGdsFlightCodeToHeader,mGdsFlightCodeFromHeader ,mGdsHeaderDuration;
//        ListView lvGdsFlightDetail;
//        GdsFlightInfoAdapter mGdsFlightAdapter;
//        TextView flightBaseFare,flightTaxes,flightTotal;
//
//        //ONWARD
//        lvGdsFlightDetail = (ListView) lObjDialogShowFlightDetails.findViewById(R.id.GDS_LISTVIEW);
//        mGdsFlightCodeFromHeader = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.GDS_HEADER_FROM);
//        mGdsFlightCodeToHeader = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.GDS_HEADER_TO);
//        mGdsHeaderDuration = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.ONWARDS_HEADER_DRATION);
//
//
//        flightBaseFare = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.TXT_FARE_BASE);
//        flightTaxes = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.TXT_FARE_TAXES);
//        flightTotal = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.TXT_FARE_TOTAL);
//
//        Button lObjBtnDone = (Button) lObjDialogShowFlightDetails.findViewById(R.id.BTN_FLIGHT_DETAIL_DONE);
//
//        lObjBtnDone.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                lObjDialogShowFlightDetails.cancel();
//            }
//        });
//
//
//        // FOR ROUND ONWARD SCREEN GDS FLIGHTS
//
//        flightBaseFare.setText(mainActivity.getResources().getString(R.string.Rs) + AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightGDS().get(positionOnw).getFlightBaseFare());
//        flightTaxes.setText(mainActivity.getResources().getString(R.string.Rs) + (AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightGDS().get(positionOnw).getFlightTaxInFare() + AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightGDS().get(positionOnw).getFlightStaxInFare()));
//        flightTotal.setText(mainActivity.getResources().getString(R.string.Rs) + AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightGDS().get(positionOnw).getFlightFare());
//
//        mGdsFlightCodeFromHeader.setText(AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightGDS().get(positionOnw).getFlightJourneyStartFrom());
//        mGdsFlightCodeToHeader.setText(AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightGDS().get(positionOnw).getFlightJourneyEndTo());
//        /**
//         * If inner flight size is 1, show normal duration else find the first flight departure tine and last flight arrival time difference
//         */
//        int innerFlightSize = AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightGDS().get(positionOnw).getFlight().size();
//        if(innerFlightSize == 1){
//            mGdsHeaderDuration.setText(AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightGDS().get(positionOnw).getFlight().get(0).getFlightDurationTime());
//        }else{
//            mGdsHeaderDuration.setText(getOnWardFlightTotalTime());
//        }
//
//        mGdsFlightAdapter = new GdsFlightInfoAdapter(mainActivity, AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightGDS().get(positionOnw).getFlight());
//        lvGdsFlightDetail.setAdapter(mGdsFlightAdapter);
//        ProjectUtil.updateListViewHeight(lvGdsFlightDetail);
//
//        // Show dialog
//        lObjDialogShowFlightDetails.show();
//    }

    /************************************************************************************************************
     *  INFO DIALOG OF SELECTED FLIGHT
     ***********************************************************************************************************/
    private void showSelectedFlightDetailsDialog( ArrayList<FlightOnWardDetailBean> lObjalOnwards, ArrayList<FlightOnWardDetailBean> lObjalReturn) {

        int positionOnw = 0;
        int positionRet = 0;

        ArrayList<InnerFlightDetailsBeans> onwGDSFlightDetails = null;
        ArrayList<InnerFlightDetailsBeans> retGDSFlightDetails = null;

        if(mainActivity.SCREEN_TYPE == IS_ROUNDTRIP_ONWARD_RETURN){

            positionOnw = lObjalOnwards.get(mainReturnOnwPosition).getFlightUniqueId();
            positionRet = lObjalReturn.get(mainReturnRetPosition).getFlightUniqueId();

        }else if(mainActivity.SCREEN_TYPE == IS_ROUNDTRIP_ROUND_ONWARD){

             onwGDSFlightDetails = new ArrayList<>();
             retGDSFlightDetails = new ArrayList<>();


            for (int i = 0; i < lObjalOnwards.get(rouOnwPosition).getFlight().size(); i++) {

                InnerFlightDetailsBeans inner =  lObjalOnwards.get(rouOnwPosition).getFlight().get(i);

                if(inner.getFlightOnwRetType().equalsIgnoreCase("onward")){
                    onwGDSFlightDetails.add(inner);
                }else if(inner.getFlightOnwRetType().equalsIgnoreCase("return")){
                    retGDSFlightDetails.add(inner);
                }
            }



        }else if(mainActivity.SCREEN_TYPE == IS_ROUND_RETURN_SCREEN){

            positionOnw = lObjalOnwards.get(rouRetOnwardPosition).getFlightUniqueId();
            positionRet = lObjalReturn.get(rouRetReturnPosition).getFlightUniqueId();
        }

        if (!mainActivity.isFinishing()) {

            lObjDialogShowFlightDetails = new Dialog(mainActivity, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
            mainActivity.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            lObjDialogShowFlightDetails.requestWindowFeature(Window.FEATURE_NO_TITLE);
            lObjDialogShowFlightDetails.getWindow().setBackgroundDrawable(new ColorDrawable(Color.LTGRAY));

            // Set layout
            lObjDialogShowFlightDetails.setContentView(R.layout.round_trip_flight_info);


            ///// INITIALIZE VAR START //////
            TextView mOnwardFlightFromToHeader,mOnwardHeaderDuration,mReturnFlightFromToHeader,mReturnHeaderDuration;
            ListView lvOnwardFlightDetail, lvReturnFlightDetail;
            OnwardFlightInfoAdapter mOnwardFlightAdapter, mReturnFlightAdapter;
            TextView flightBaseFare,flightTaxes,flightTotal;
            LinearLayout layReturnHeader;

            TextView txtOnwardRefundable,txtReturnRefundable;
            ImageView imgOnwardRefundable,imgReturnRefundable;



            //ONWARD
            lvOnwardFlightDetail = (ListView) lObjDialogShowFlightDetails.findViewById(R.id.FARE_ONWARD_QUOTE_LISTVIEW);
            mOnwardFlightFromToHeader = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.ONWARDS_HEADER_FROM_TO);
            mOnwardHeaderDuration = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.ONWARDS_HEADER_DRATION);

            imgOnwardRefundable = (ImageView) lObjDialogShowFlightDetails.findViewById(R.id.ROUND_ONW_REFUNDABLE_IMG);
            txtOnwardRefundable = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.ROUND_ONW_REFUNDABLE_TXT);


            // RETURN
            lvReturnFlightDetail = (ListView) lObjDialogShowFlightDetails.findViewById(R.id.FARE_RETURN_QUOTE_LISTVIEW);
            mReturnFlightFromToHeader = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.RETURN_HEADER_FROM_TO);
            mReturnHeaderDuration = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.RETURN_HEADER_DRATION);
           // layReturnHeader = (LinearLayout) lObjDialogShowFlightDetails.findViewById(R.id.LAY_RETURN_HEADER);

            imgReturnRefundable = (ImageView) lObjDialogShowFlightDetails.findViewById(R.id.ROUND_RET_REFUNDABLE_IMG);
            txtReturnRefundable = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.ROUND_RET_REFUNDABLE_TXT);


            flightBaseFare = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.TXT_FARE_BASE);
            flightTaxes = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.TXT_FARE_TAXES);
            flightTotal = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.TXT_FARE_TOTAL);

            Button lObjBtnDone = (Button) lObjDialogShowFlightDetails.findViewById(R.id.BTN_FLIGHT_DETAIL_DONE);

            lObjBtnDone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    lObjDialogShowFlightDetails.cancel();
                }
            });

            ///// INITIALIZE VAR END //////

            ///// SET VALUES IN VAR START //////

            if(mainActivity.SCREEN_TYPE == IS_ROUNDTRIP_ONWARD_RETURN) {

                flightBaseFare.setText(mainActivity.getResources().getString(R.string.Rs) + ( AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().get(positionOnw).getFlightBaseFare() +  AirDataHolder.getListHolder().getList().get(0).getReturnAlAllFlightDetails().get(positionRet).getFlightBaseFare()));
                flightTaxes.setText(mainActivity.getResources().getString(R.string.Rs) + ( AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().get(positionOnw).getFlightTaxInFare() +  AirDataHolder.getListHolder().getList().get(0).getReturnAlAllFlightDetails().get(positionRet).getFlightTaxInFare()));
                flightTotal.setText(mainActivity.getResources().getString(R.string.Rs) + ( AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().get(positionOnw).getFlightFare() +  AirDataHolder.getListHolder().getList().get(0).getReturnAlAllFlightDetails().get(positionRet).getFlightFare()));


                if(AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().get(positionOnw).getFlight().get(0).getTkt().contains("Non")){
                    imgOnwardRefundable.setImageResource(R.drawable.refundable_red);

                    // mOnwardRefundable.setTextColor(mainActivity.getResources().getColor(R.color.TXT_RED));
                }else{
                    imgOnwardRefundable.setImageResource(R.drawable.refundable_green);

                    // mOnwardRefundable.setTextColor(mainActivity.getResources().getColor(R.color.TXT_GREEN));
                }
                txtOnwardRefundable.setText("" + AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().get(positionOnw).getFlight().get(0).getTkt());




                // FOR ONWARD FLIGHTS
                mOnwardFlightFromToHeader.setText(AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().get(positionOnw).getFlightJourneyStartFrom() +" - "+AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().get(positionOnw).getFlightJourneyEndTo());
                /**
                 * If inner flight size is 1, show normal duration else find the first flight departure tine and last flight arrival time difference
                 */
                int innerFlightSize = AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().get(positionOnw).getFlight().size();
                if(innerFlightSize == 1){
                    mOnwardHeaderDuration.setText(AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().get(positionOnw).getFlight().get(0).getFlightDurationTime());
                }else{
                    mOnwardHeaderDuration.setText(getOnWardFlightTotalTime());
                }

                mOnwardFlightAdapter = new OnwardFlightInfoAdapter(mainActivity, AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().get(positionOnw).getFlight());
                lvOnwardFlightDetail.setAdapter(mOnwardFlightAdapter);
                ProjectUtil.updateListViewHeight(lvOnwardFlightDetail);

                // FOR RETURN FLIGHTS
                mReturnFlightFromToHeader.setText(AirDataHolder.getListHolder().getList().get(0).getReturnAlAllFlightDetails().get(positionRet).getFlightJourneyEndTo() +" - "+ AirDataHolder.getListHolder().getList().get(0).getReturnAlAllFlightDetails().get(positionRet).getFlightJourneyStartFrom());
                /**
                 * If inner flight size is 1, show normal duration else find the first flight departure tine and last flight arrival time difference
                 */
                int innerFlightSize1 = AirDataHolder.getListHolder().getList().get(0).getReturnAlAllFlightDetails().get(positionRet).getFlight().size();
                if(innerFlightSize1 == 1){
                    mReturnHeaderDuration.setText(AirDataHolder.getListHolder().getList().get(0).getReturnAlAllFlightDetails().get(positionRet).getFlight().get(0).getFlightDurationTime());
                }else{
                    mReturnHeaderDuration.setText(getOnWardFlightTotalTime());
                }

                mReturnFlightAdapter = new OnwardFlightInfoAdapter(mainActivity, AirDataHolder.getListHolder().getList().get(0).getReturnAlAllFlightDetails().get(positionRet).getFlight());
                lvReturnFlightDetail.setAdapter(mReturnFlightAdapter);
                ProjectUtil.updateListViewHeight(lvReturnFlightDetail);

                if(AirDataHolder.getListHolder().getList().get(0).getReturnAlAllFlightDetails().get(positionRet).getFlight().get(0).getTkt().contains("Non")){
                    imgReturnRefundable.setImageResource(R.drawable.refundable_red);

                    // mOnwardRefundable.setTextColor(mainActivity.getResources().getColor(R.color.TXT_RED));
                }else{
                    imgReturnRefundable.setImageResource(R.drawable.refundable_green);

                    // mOnwardRefundable.setTextColor(mainActivity.getResources().getColor(R.color.TXT_GREEN));
                }
                txtReturnRefundable.setText("" + AirDataHolder.getListHolder().getList().get(0).getReturnAlAllFlightDetails().get(positionRet).getFlight().get(0).getTkt());


            }
            else if(mainActivity.SCREEN_TYPE == IS_ROUNDTRIP_ROUND_ONWARD) {

                // FOR ROUND ONWARD SCREEN GDS FLIGHTS

                flightBaseFare.setText(mainActivity.getResources().getString(R.string.Rs) + lObjalOnwards.get(rouOnwPosition).getFlightBaseFare());//getRouOnwGdsLccBean().getFlightLCC()
                flightTaxes.setText(mainActivity.getResources().getString(R.string.Rs) + (lObjalOnwards.get(rouOnwPosition).getFlightTaxInFare()+ lObjalOnwards.get(rouOnwPosition).getFlightStaxInFare()));
                flightTotal.setText(mainActivity.getResources().getString(R.string.Rs) + lObjalOnwards.get(rouOnwPosition).getFlightFare());




                mOnwardFlightFromToHeader.setText(lObjalOnwards.get(rouOnwPosition).getFlightJourneyStartFrom() +" - " + lObjalOnwards.get(rouOnwPosition).getFlightJourneyEndTo());

                mOnwardFlightAdapter = new OnwardFlightInfoAdapter(mainActivity, onwGDSFlightDetails);
                lvOnwardFlightDetail.setAdapter(mOnwardFlightAdapter);
                ProjectUtil.updateListViewHeight(lvOnwardFlightDetail);

                if(onwGDSFlightDetails.get(0).getTkt().contains("Non")){
                    imgOnwardRefundable.setImageResource(R.drawable.refundable_red);

                    // mOnwardRefundable.setTextColor(mainActivity.getResources().getColor(R.color.TXT_RED));
                }else{
                    imgOnwardRefundable.setImageResource(R.drawable.refundable_green);

                    // mOnwardRefundable.setTextColor(mainActivity.getResources().getColor(R.color.TXT_GREEN));
                }
                txtOnwardRefundable.setText("" + onwGDSFlightDetails.get(0).getTkt());


                // FOR ROUND RETURN FLIGHTS LCC
                mReturnFlightFromToHeader.setText(lObjalOnwards.get(rouOnwPosition).getFlightJourneyEndTo() +" - "+ lObjalOnwards.get(rouOnwPosition).getFlightJourneyStartFrom());


                mReturnFlightAdapter = new OnwardFlightInfoAdapter(mainActivity, retGDSFlightDetails);
                lvReturnFlightDetail.setAdapter(mReturnFlightAdapter);
                ProjectUtil.updateListViewHeight(lvReturnFlightDetail);


                if(retGDSFlightDetails.get(0).getTkt().contains("Non")){
                    imgReturnRefundable.setImageResource(R.drawable.refundable_red);

                    // mOnwardRefundable.setTextColor(mainActivity.getResources().getColor(R.color.TXT_RED));
                }else{
                    imgReturnRefundable.setImageResource(R.drawable.refundable_green);

                    // mOnwardRefundable.setTextColor(mainActivity.getResources().getColor(R.color.TXT_GREEN));
                }
                txtReturnRefundable.setText("" + onwGDSFlightDetails.get(0).getTkt());


            }
            else if(mainActivity.SCREEN_TYPE == IS_ROUND_RETURN_SCREEN) {

                // FOR ROUND ONWARD FLIGHTS LCC

                flightBaseFare.setText(mainActivity.getResources().getString(R.string.Rs) + (AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightLCC().get(positionOnw).getFlightBaseFare() +  AirDataHolder.getListHolder().getList().get(0).getRouRetalAllFlightDetails().get(positionRet).getFlightBaseFare()));//getRouOnwGdsLccBean().getFlightLCC()
                flightTaxes.setText(mainActivity.getResources().getString(R.string.Rs) + (AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightLCC().get(positionOnw).getFlightTaxInFare() +  AirDataHolder.getListHolder().getList().get(0).getRouRetalAllFlightDetails().get(positionRet).getFlightTaxInFare()));
                flightTotal.setText(mainActivity.getResources().getString(R.string.Rs) + (AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightLCC().get(positionOnw).getFlightFare() +  AirDataHolder.getListHolder().getList().get(0).getRouRetalAllFlightDetails().get(positionRet).getFlightFare()));

                mOnwardFlightFromToHeader.setText(AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightLCC().get(positionOnw).getFlightJourneyStartFrom() +" - "+AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightLCC().get(positionOnw).getFlightJourneyEndTo());
                /**
                 * If inner flight size is 1, show normal duration else find the first flight departure tine and last flight arrival time difference
                 */
                int innerFlightSize = AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightLCC().get(positionOnw).getFlight().size();
                if(innerFlightSize == 1){
                    mOnwardHeaderDuration.setText(AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().get(positionOnw).getFlight().get(0).getFlightDurationTime());
                }else{
                    mOnwardHeaderDuration.setText(getOnWardFlightTotalTime());
                }

                mOnwardFlightAdapter = new OnwardFlightInfoAdapter(mainActivity, AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightLCC().get(positionOnw).getFlight());
                lvOnwardFlightDetail.setAdapter(mOnwardFlightAdapter);
                ProjectUtil.updateListViewHeight(lvOnwardFlightDetail);

                if(AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightLCC().get(positionOnw).getFlight().get(0).getTkt().contains("Non")){
                    imgOnwardRefundable.setImageResource(R.drawable.refundable_red);

                    // mOnwardRefundable.setTextColor(mainActivity.getResources().getColor(R.color.TXT_RED));
                }else{
                    imgOnwardRefundable.setImageResource(R.drawable.refundable_green);

                    // mOnwardRefundable.setTextColor(mainActivity.getResources().getColor(R.color.TXT_GREEN));
                }
                txtOnwardRefundable.setText("" + AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightLCC().get(positionOnw).getFlight().get(0).getTkt());



                // FOR ROUND RETURN FLIGHTS LCC
                mReturnFlightFromToHeader.setText(AirDataHolder.getListHolder().getList().get(0).getRouRetalAllFlightDetails().get(positionRet).getFlightJourneyEndTo() +" - "+ AirDataHolder.getListHolder().getList().get(0).getRouRetalAllFlightDetails().get(positionRet).getFlightJourneyStartFrom());
                /**
                 * If inner flight size is 1, show normal duration else find the first flight departure tine and last flight arrival time difference
                 */
                int innerFlightSize1 = AirDataHolder.getListHolder().getList().get(0).getRouRetalAllFlightDetails().get(positionRet).getFlight().size();
                if(innerFlightSize1 == 1){
                    mReturnHeaderDuration.setText(AirDataHolder.getListHolder().getList().get(0).getRouRetalAllFlightDetails().get(positionRet).getFlight().get(0).getFlightDurationTime());
                }else{
                    mReturnHeaderDuration.setText(getOnWardFlightTotalTime());
                }

                mReturnFlightAdapter = new OnwardFlightInfoAdapter(mainActivity, AirDataHolder.getListHolder().getList().get(0).getRouRetalAllFlightDetails().get(positionRet).getFlight());
                lvReturnFlightDetail.setAdapter(mReturnFlightAdapter);
                ProjectUtil.updateListViewHeight(lvReturnFlightDetail);

                if(AirDataHolder.getListHolder().getList().get(0).getRouRetalAllFlightDetails().get(positionRet).getFlight().get(0).getTkt().contains("Non")){
                    imgReturnRefundable.setImageResource(R.drawable.refundable_red);

                    // mOnwardRefundable.setTextColor(mainActivity.getResources().getColor(R.color.TXT_RED));
                }else{
                    imgReturnRefundable.setImageResource(R.drawable.refundable_green);

                    // mOnwardRefundable.setTextColor(mainActivity.getResources().getColor(R.color.TXT_GREEN));
                }
                txtReturnRefundable.setText("" + AirDataHolder.getListHolder().getList().get(0).getRouRetalAllFlightDetails().get(positionRet).getFlight().get(0).getTkt());

            }

            // Show dialog
            lObjDialogShowFlightDetails.show();

        }
    }


    public String getOnWardFlightTotalTime(){

        //"" + AirDataHolder.getListHolder().getList().get(0).getmFareQuoteOneWayBean().getFlight().get(0).getDuration()

        return "Working";
    }



}
