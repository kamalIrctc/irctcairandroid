package com.irctc.air.fragment;

import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.auth0.android.jwt.JWT;
import com.irctc.air.AppController;
import com.irctc.air.Database.AirDatabase;
import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.adapter.FareQuoteOnwardFlightInfoAdapter;
import com.irctc.air.adapter.FareQuoteReturnFlightInfoAdapter;
import com.irctc.air.header.AirHeader;
import com.irctc.air.model.FareQuoteInnerFlightDetailsBeans;
import com.irctc.air.model.route_list.ModelRoutListItem;
import com.irctc.air.navigationdrawer.SliderMenu;
import com.irctc.air.networking.Networking;
import com.irctc.air.round.trip.domastic.model.Flights;
import com.irctc.air.round.trip.domastic.model.LstBaggageDetails;
import com.irctc.air.util.AlertDialogUtil;
import com.irctc.air.util.AppKeys;
import com.irctc.air.util.Constant;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.NetworkingUtils;
import com.irctc.air.util.OnewayFilterEventHandler;
import com.irctc.air.util.Pref;
import com.irctc.air.util.ProjectUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by asif on 4/30/2016.
 */
public class FragmentReprice extends Fragment {
    private ActivityMain mainActivity;
    private Button btnSubmit;
    private Dialog lObjDialogShowFlightDetails;
    private ProgressBar progressBar;
    private TextView flightBaseFare, flightTaxes, flightTotal, irctcAgentServiceTax, irctcBookingCharges;
    private ListView lvOnwardFlightDetail;
    FareQuoteOnwardFlightInfoAdapter mOnwardFlightAdapter;
    LinearLayout mOnwardFlightlayoutMain;
    TextView mOnwardFlightFromToHeader, mOnwardFlightToHeader, mOnwardHeaderDuration, mOnwardRefundable;
    CardView mReturnFlightlayoutMain;
    TextView mReturnFlightFromToHeader, mReturnFlightToHeader, mReturnHeaderDuration, mReturnRefundable;
    TextView mGdsInternationalCodeFrom, mGdsInternationalCodeTo, textViewSGST, textViewCGST;
    LinearLayout mGdsInterCode, mNormalCode;
    ImageView imgReturnRefundable;
    ImageView imgOnwardRefundable;
    ImageView ivBag;
    TextView tvOnlyHandBaggage;
    private CheckBox checkAccept;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = (ActivityMain) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fare_quote, null);
        initializeVariable(view);
        AirHeader.showRecentSearchIcon(false);
        AirHeader.showFareQuoteHeader(mainActivity, "Preconfirmation");
        AirHeader.showDrawerToggleAndToolbar(false, true);
        setData();
        return view;
    }

    private void setData() {
        if (FragmentOneWayFlight.flightForReprice.getSegmentType().equals("O")) {
            mReturnFlightlayoutMain.setVisibility(View.GONE);
        }

        ArrayList<ModelRoutListItem> arrayListRouteList = new ArrayList<>();

        /*com.irctc.air.model.search_result_one_way.Flights selectedFlights;
        if(mainActivity.isComingFromFilterFrag){
            selectedFlights = OnewayFilterEventHandler.selectedFlight;
        }else {
            selectedFlights = FragmentOneWayFlight.flightForReprice;
        }


        for(int i=0;i<selectedFlights.getLstFlightDetails().length;i++){
            ModelRoutListItem modelRoutListItem =new ModelRoutListItem();
            String flightCode=selectedFlights.getLstFlightDetails()[i].getAirlineCode()+"-"+selectedFlights.getLstFlightDetails()[i].getFlightNumber();
            String fromAirportCode=selectedFlights.getLstFlightDetails()[i].getOrigin();
            String fromAirportName=selectedFlights.getLstFlightDetails()[i].getOriginCity();
            String fromAirportTerminal=selectedFlights.getLstFlightDetails()[i].getOriginTerminal();
            String toAirportCode=selectedFlights.getLstFlightDetails()[i].getDestination();
            String toAirportName=selectedFlights.getLstFlightDetails()[i].getDestinationCity();
            String toAirportTerminal=selectedFlights.getLstFlightDetails()[i].getDestinationTerminal();
            String travelDuration=selectedFlights.getLstFlightDetails()[i].getFlightTime();
            String departureDate=selectedFlights.getLstFlightDetails()[i].getDepartureDate();
            String departureTime=selectedFlights.getLstFlightDetails()[i].getDepartureTime();
            String arrivalDate=selectedFlights.getLstFlightDetails()[i].getArrivalDate();
            String arrivalTime=selectedFlights.getLstFlightDetails()[i].getArrivalTime();
            String halt=selectedFlights.getLstFlightDetails()[i].getHalt();

            modelRoutListItem.setFlightCode(flightCode);
            modelRoutListItem.setCarrear(selectedFlights.getLstFlightDetails()[i].getAirlineCode());
            modelRoutListItem.setFromAirportCode(fromAirportCode);
            modelRoutListItem.setFromAirportName(fromAirportName);
            if(fromAirportTerminal!=null){
                if(!fromAirportTerminal.equals("")){
                    fromAirportTerminal=fromAirportTerminal;
                }
            }
            modelRoutListItem.setFromAirportTerminal(fromAirportTerminal);
            modelRoutListItem.setToAirportCode(toAirportCode);
            modelRoutListItem.setToAirportName(toAirportName);
            if(toAirportTerminal!=null){
                if(!toAirportTerminal.equals("")){
                    toAirportTerminal=toAirportTerminal;
                }
            }
            modelRoutListItem.setToAirportTerminal(toAirportTerminal);
            modelRoutListItem.setTravelDuration(travelDuration);
            modelRoutListItem.setDepartureDate(departureDate);
            modelRoutListItem.setDepartureTime(departureTime);
            modelRoutListItem.setArrivalDate(arrivalDate);
            modelRoutListItem.setArrivalTime(arrivalTime);
            modelRoutListItem.setHalt(halt);
            arrayListRouteList.add(modelRoutListItem);
        }
        mOnwardFlightAdapter = new FareQuoteOnwardFlightInfoAdapter(mainActivity, arrayListRouteList);
        lvOnwardFlightDetail.setAdapter(mOnwardFlightAdapter);
        ProjectUtil.updateListViewHeight(lvOnwardFlightDetail);

        if(selectedFlights.getLstFareDetails()[0].getBaseType().equals("false")){
            imgOnwardRefundable.setImageResource(R.drawable.refundable_red);
            mOnwardRefundable.setText("Non Refundable");
        }else if(selectedFlights.getLstFareDetails()[0].getBaseType().equals("true")){
            imgOnwardRefundable.setImageResource(R.drawable.refundable_green);
            mOnwardRefundable.setText("Refundable");
        }
        mOnwardFlightFromToHeader.setText(selectedFlights.getDepartureCityWithCode());
        mOnwardFlightToHeader.setText(selectedFlights.getArrivalCityWithCode());
        flightBaseFare.setText(mainActivity.getResources().getString(R.string.Rs)+FragmentOneWayFlight.data.getLstFareDetails()[0].getBaseFare());
        flightTaxes.setText(mainActivity.getResources().getString(R.string.Rs)+FragmentOneWayFlight.data.getLstFareDetails()[0].getTax());
        irctcBookingCharges.setText(mainActivity.getResources().getString(R.string.Rs)+FragmentOneWayFlight.data.getLstFareDetails()[0].getIrctcCharge());
        flightTotal.setText(mainActivity.getResources().getString(R.string.Rs)+FragmentOneWayFlight.data.getLstFareDetails()[0].getTotal());
        textViewSGST.setText(mainActivity.getResources().getString(R.string.Rs)+FragmentOneWayFlight.data.getLstFareDetails()[0].getSgst());
        textViewCGST.setText(mainActivity.getResources().getString(R.string.Rs)+FragmentOneWayFlight.data.getLstFareDetails()[0].getCgst());*/
        LstBaggageDetails lstBaggageDetails = new LstBaggageDetails();
        for (int i = 0; i < FragmentOneWayFlight.flightForReprice.getLstBaggageDetails().length; i++) {
            if (FragmentOneWayFlight.flightForReprice.getLstBaggageDetails()[i].getPaxType().equalsIgnoreCase("ADT")){
                lstBaggageDetails = FragmentOneWayFlight.flightForReprice.getLstBaggageDetails()[i];
                break;
            }
        }
        for (int i = 0; i < FragmentOneWayFlight.flightForReprice.getLstFlightDetails().length; i++) {
            ModelRoutListItem modelRoutListItem = new ModelRoutListItem();
            String flightCode = FragmentOneWayFlight.flightForReprice.getLstFlightDetails()[i].getAirlineCode() + "-" + FragmentOneWayFlight.flightForReprice.getLstFlightDetails()[i].getFlightNumber();
            String fromAirportCode = FragmentOneWayFlight.flightForReprice.getLstFlightDetails()[i].getOrigin();
            String fromAirportName = FragmentOneWayFlight.flightForReprice.getLstFlightDetails()[i].getOriginCity();
            String fromAirportTerminal = FragmentOneWayFlight.flightForReprice.getLstFlightDetails()[i].getOriginTerminal();
            String toAirportCode = FragmentOneWayFlight.flightForReprice.getLstFlightDetails()[i].getDestination();
            String toAirportName = FragmentOneWayFlight.flightForReprice.getLstFlightDetails()[i].getDestinationCity();
            String toAirportTerminal = FragmentOneWayFlight.flightForReprice.getLstFlightDetails()[i].getDestinationTerminal();
            String travelDuration = FragmentOneWayFlight.flightForReprice.getLstFlightDetails()[i].getFlightTime();
            String departureDate = FragmentOneWayFlight.flightForReprice.getLstFlightDetails()[i].getDepartureDate();
            String departureTime = FragmentOneWayFlight.flightForReprice.getLstFlightDetails()[i].getDepartureTime();
            String arrivalDate = FragmentOneWayFlight.flightForReprice.getLstFlightDetails()[i].getArrivalDate();
            String arrivalTime = FragmentOneWayFlight.flightForReprice.getLstFlightDetails()[i].getArrivalTime();
            String halt = FragmentOneWayFlight.flightForReprice.getLstFlightDetails()[i].getHalt();

            modelRoutListItem.setFlightCode(flightCode);
            modelRoutListItem.setCarrear(FragmentOneWayFlight.flightForReprice.getLstFlightDetails()[i].getAirlineCode());
            modelRoutListItem.setFromAirportCode(fromAirportCode);
            modelRoutListItem.setFromAirportName(fromAirportName);
            if (fromAirportTerminal != null) {
                if (!fromAirportTerminal.equals("")) {
                    fromAirportTerminal = fromAirportTerminal;
                }
            }
            modelRoutListItem.setFromAirportTerminal(fromAirportTerminal);
            modelRoutListItem.setToAirportCode(toAirportCode);
            modelRoutListItem.setToAirportName(toAirportName);
            if (toAirportTerminal != null) {
                if (!toAirportTerminal.equals("")) {
                    toAirportTerminal = toAirportTerminal;
                }
            }
            modelRoutListItem.setServiceProvider(FragmentOneWayFlight.flightForReprice.getServiceProvider());
            modelRoutListItem.setLstBaggageDetails(lstBaggageDetails);
            modelRoutListItem.setFreeMeal(FragmentOneWayFlight.flightForReprice.getLstFlightDetails()[i].isFreeMeal());
            modelRoutListItem.setToAirportTerminal(toAirportTerminal);
            modelRoutListItem.setTravelDuration(travelDuration);
            modelRoutListItem.setDepartureDate(departureDate);
            modelRoutListItem.setDepartureTime(departureTime);
            modelRoutListItem.setArrivalDate(arrivalDate);
            modelRoutListItem.setArrivalTime(arrivalTime);
            modelRoutListItem.setHalt(halt);
            arrayListRouteList.add(modelRoutListItem);
        }
        mOnwardFlightAdapter = new FareQuoteOnwardFlightInfoAdapter(mainActivity, arrayListRouteList);
        lvOnwardFlightDetail.setAdapter(mOnwardFlightAdapter);
        ProjectUtil.updateListViewHeight(lvOnwardFlightDetail);

        if (FragmentOneWayFlight.flightForReprice.getLstFareDetails()[0].getBaseType().equals("false")) {
            imgOnwardRefundable.setImageResource(R.drawable.refundable_red);
            mOnwardRefundable.setText("Non Refundable");
        } else if (FragmentOneWayFlight.flightForReprice.getLstFareDetails()[0].getBaseType().equals("true")) {
            imgOnwardRefundable.setImageResource(R.drawable.refundable_green);
            mOnwardRefundable.setText("Refundable");
        }
        if (FragmentOneWayFlight.flightForReprice.isBaggageAllowed()) {
            ivBag.setVisibility(View.VISIBLE);
            tvOnlyHandBaggage.setVisibility(View.VISIBLE);
        } else {
            ivBag.setVisibility(View.GONE);
            tvOnlyHandBaggage.setVisibility(View.GONE);
        }

        mOnwardFlightFromToHeader.setText(FragmentOneWayFlight.flightForReprice.getDepartureCityWithCode());
        mOnwardFlightToHeader.setText(FragmentOneWayFlight.flightForReprice.getArrivalCityWithCode());
        flightBaseFare.setText(mainActivity.getResources().getString(R.string.Rs) + FragmentOneWayFlight.data.getLstFareDetails()[0].getBaseFare());
        flightTaxes.setText(mainActivity.getResources().getString(R.string.Rs) + FragmentOneWayFlight.data.getLstFareDetails()[0].getTax());
        irctcBookingCharges.setText(mainActivity.getResources().getString(R.string.Rs) + FragmentOneWayFlight.data.getLstFareDetails()[0].getIrctcCharge());
        flightTotal.setText(mainActivity.getResources().getString(R.string.Rs) + FragmentOneWayFlight.data.getLstFareDetails()[0].getTotal());
        textViewSGST.setText(mainActivity.getResources().getString(R.string.Rs) + FragmentOneWayFlight.data.getLstFareDetails()[0].getGst());
        //textViewCGST.setText(mainActivity.getResources().getString(R.string.Rs)+FragmentOneWayFlight.data.getLstFareDetails()[0].getCgst());

    }

    private void initializeVariable(View view) {
        mOnwardFlightlayoutMain = (LinearLayout) view.findViewById(R.id.ONWARD_LAYOUT_MAIN);
        lvOnwardFlightDetail = (ListView) view.findViewById(R.id.FARE_ONWARD_QUOTE_LISTVIEW);

        mOnwardFlightFromToHeader = (TextView) view.findViewById(R.id.ONWARDS_HEADER_FROM_TO);
        mOnwardFlightToHeader = (TextView) view.findViewById(R.id.ONWARDS_HEADER_TO_CODE);

        mOnwardHeaderDuration = (TextView) view.findViewById(R.id.ONWARDS_HEADER_DRATION);
        mOnwardRefundable = (TextView) view.findViewById(R.id.ONWARD_REFUNDABLE_TXT);
        imgOnwardRefundable = (ImageView) view.findViewById(R.id.ONWARD_REFUNDABLE_IMG);
        ivBag = (ImageView) view.findViewById(R.id.ivBag);
        tvOnlyHandBaggage = (TextView) view.findViewById(R.id.tvOnlyHandBaggage);
        mReturnFlightlayoutMain = (CardView) view.findViewById(R.id.RETURN_LAYOUT_MAIN);
        mReturnFlightFromToHeader = (TextView) view.findViewById(R.id.RETURN_HEADER_FROM_TO);
        mReturnFlightToHeader = (TextView) view.findViewById(R.id.RETURN_HEADER_TO_CODE);

        mReturnHeaderDuration = (TextView) view.findViewById(R.id.RETURN_HEADER_DRATION);
        mReturnRefundable = (TextView) view.findViewById(R.id.RETURN_REFUNDABLE_TXT);
        imgReturnRefundable = (ImageView) view.findViewById(R.id.RETURN_REFUNDABLE_IMG);

        flightBaseFare = (TextView) view.findViewById(R.id.TXT_FARE_BASE);
        flightTaxes = (TextView) view.findViewById(R.id.TXT_FARE_TAXES);
        flightTotal = (TextView) view.findViewById(R.id.TXT_FARE_TOTAL);

        irctcAgentServiceTax = (TextView) view.findViewById(R.id.TXT_IRCTC_AGENT_TAX);
        irctcBookingCharges = (TextView) view.findViewById(R.id.TXT_IRCTC_BOOKING_CHARG);


        btnSubmit = (Button) view.findViewById(R.id.BTN_FARE_QUOTE_SUBMIT);
        //    boolean readyToProceed = true;


        // added by asif
        mGdsInterCode = (LinearLayout) view.findViewById(R.id.LAY_GDS_INTER_CODE_FROM_TO);
        mNormalCode = (LinearLayout) view.findViewById(R.id.LAY_NORMAL_CODE_FROM_TO);
        mGdsInternationalCodeFrom = (TextView) view.findViewById(R.id.TXT_STATION_CODE_FROM);
        mGdsInternationalCodeTo = (TextView) view.findViewById(R.id.TXT_STATION_CODE_TO);
        textViewSGST = (TextView) view.findViewById(R.id.textViewSGST);
        textViewCGST = (TextView) view.findViewById(R.id.textViewCGST);
        checkAccept = (CheckBox) view.findViewById(R.id.CHECKBOX_ACCEPT_TERMS);
        final boolean readyToProceed = true;

        SpannableString ss = new SpannableString(getString(R.string.accept_journey_details));
        ClickableSpan clickableSpanUser = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                showTermsCondition(Networking.USER_AGREMENT);
            }
        };

        ClickableSpan clickableSpanAirlineAgreement = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                showTermsCondition(Networking.AIRLINE_AGREMENT);
            }
        };
        String firstToken = "Airline";
        String secondToken = "User Agreement";
        int startPosition1 = ss.toString().indexOf(firstToken);
        int startPosition2 = ss.toString().indexOf(secondToken);

        ss.setSpan(clickableSpanUser, startPosition1, startPosition1 + firstToken.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(clickableSpanAirlineAgreement, startPosition2, startPosition2 + secondToken.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        checkAccept.setText(ss);

        checkAccept.setMovementMethod(LinkMovementMethod.getInstance());


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.lastActiveFragment = Constant.FARE_QUOTE_FRAGMENT;
                if (Pref.getBoolean(getActivity().getApplicationContext())) {
                    Context ctx = getActivity().getApplicationContext();
                    if (readyToProceed) {
                        if (!checkAccept.isChecked()) {
                            new AlertDialogUtil(mainActivity, "Kindly accept the terms & conditions", mainActivity.getResources().getString(R.string.ADD_PASS_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                            return;
                        }

                    }
                    //checkSession();
                    AirDatabase airDatabase = new AirDatabase(getActivity());
                    JWT jwt = new JWT(airDatabase.getAuthToken());
                    Date dateExp = jwt.getExpiresAt();
                    Date dateCurr = new Date();
                    long duration = dateExp.getTime() - dateCurr.getTime();
                    long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
                    if (diffInMinutes <= 0) {
                        Pref.setString(ctx, AppKeys.USER_DETAIL_ADDRESS_1, "");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_ADDRESS_2, "");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_FIRST_NAME, "");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_LAST_NAME, "");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_CITY, "");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_STATE, "");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_COUNTRY, "");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_PIN_CODE, "");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_EMAIL, "");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_MOBILE_NO, "");
                        Pref.setBoolean(ctx, false);
                        SliderMenu.txtUserId.setText("Guest");
                        new AirDatabase(getActivity()).removeAuthToken();
                        AppController.isComingFromFareQuote = true;
                        AppController.routeType = "OneWay";
                        android.support.v4.app.FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.frame_layout, new FragmentLogin());
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                        //ProjectUtil.replaceFragment(mainActivity, new FragmentLogin(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                    } else {
                        android.support.v4.app.FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.frame_layout, new FragmentAddPassengers());
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();

                        //  ProjectUtil.replaceFragment(mainActivity, new FragmentAddPassengers(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                    }
                } else {
                    if (!checkAccept.isChecked()) {
                        new AlertDialogUtil(mainActivity, "Kindly accept the terms & conditions", mainActivity.getResources().getString(R.string.ADD_PASS_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                        return;
                    }

                    AppController.isComingFromFareQuote = true;
                    AppController.routeType = "OneWay";
                    ProjectUtil.replaceFragment(mainActivity, new FragmentLogin(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                }
                /*if (readyToProceed) {
                    if (!checkAccept.isChecked()) {
                        new AlertDialogUtil(mainActivity, "Kindly accept the terms & conditions", mainActivity.getResources().getString(R.string.ADD_PASS_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                  return ;
                    }

                }*/

            }
        });


    }


    @Override
    public void onResume() {
        super.onResume();
        AppController.getInstance().trackScreenView("Reprice Screen");
        ActivityMain.activeFragment = Constant.FARE_QUOTE_FRAGMENT;

    }

    private void checkSession() {
        boolean readyToProceed = true;
        NetworkingUtils.showProgress(getActivity());
        Networking.checkSession(new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                NetworkingUtils.dismissProgress();
                try {
                    Log.e("Server Time", response.getString("data"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                AirDatabase airDatabase = new AirDatabase(getActivity());
                JWT jwt = new JWT(airDatabase.getAuthToken());
                Date date = jwt.getExpiresAt();
                Log.e("Expire Time", date.toString());

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkingUtils.dismissProgress();
            }
        });

    }

    private void showTermsCondition(String lStrURL) {

        lObjDialogShowFlightDetails = new Dialog(mainActivity, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
        mainActivity.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        lObjDialogShowFlightDetails.requestWindowFeature(Window.FEATURE_NO_TITLE);
        lObjDialogShowFlightDetails.getWindow().setBackgroundDrawable(new ColorDrawable(Color.LTGRAY));

        // Set layout
        lObjDialogShowFlightDetails.setContentView(R.layout.web_view_terms_condition);

        // initialize the variables
        WebView lObjWebview = (WebView) lObjDialogShowFlightDetails.findViewById(R.id.terms_condition_webView);
        Button btnTerms = (Button) lObjDialogShowFlightDetails.findViewById(R.id.BTN_CANCEL_TERMS);
        progressBar = (ProgressBar) lObjDialogShowFlightDetails.findViewById(R.id.terms_progressBar);
        lObjWebview.getSettings().setJavaScriptEnabled(true);
        lObjWebview.setWebViewClient(new MyWebViewClient());
        lObjWebview.clearHistory();
        lObjWebview.clearCache(true);
        lObjWebview.loadUrl(lStrURL);
        lObjWebview.getSettings().setLoadWithOverviewMode(true);

        btnTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lObjDialogShowFlightDetails.cancel();
            }
        });

        lObjDialogShowFlightDetails.show();
    }

    private class MyWebViewClient extends WebViewClient {

        //      public MyWebViewClient(ProgressBar progressBar) {
        //        progressBar = progressBar;
        //     }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progressBar.setVisibility(View.GONE);
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            progressBar.setVisibility(View.VISIBLE);
            super.onPageStarted(view, url, favicon);
        }
    }
}
