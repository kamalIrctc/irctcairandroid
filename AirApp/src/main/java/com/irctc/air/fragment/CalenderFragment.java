package com.irctc.air.fragment;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.irctc.air.Database.ServerDateSharedPrefernce;
import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.R;
import com.irctc.air.adapter.CalendarAdapter;
import com.irctc.air.header.AirHeader;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.util.AlertDialogUtil;
import com.irctc.air.util.AppLogger;
import com.irctc.air.util.Constant;
import com.irctc.air.util.DateUtility;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.ProjectUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Aasif on 4/18/2016.
 * It is used to select date
 */
public class CalenderFragment extends Fragment {

    private GregorianCalendar month, itemmonth;// calendar instances.

    private CalendarAdapter adapter;// adapter instance
    private Handler handler;// for grabbing some event values for showing the dot
    // marker.
    public ArrayList<String> items; // container to store calendar items which
    // needs showing the event marker
    ArrayList<String> event;
    LinearLayout rLayout;
    ArrayList<String> date;
    ArrayList<String> desc;
    SimpleDateFormat sdf;
    Date date1, date2;
    int IS_TRAIN_LIST_INTENT = 0;
    ImageView cancelbtn;
    String selectedGridDate;
    private ActivityMain mainActivity;

    public static ArrayList<String> nameOfEvent = new ArrayList<String>();
    public static ArrayList<String> startDates = new ArrayList<String>();

    String dateObj[];
    public CalenderFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.calendar, null);

        // Header visibility
        AirHeader.showRecentSearchIcon(false);

        // Show header text
        AirHeader.showHeaderText(mainActivity, true, "Select Date");

        // Show&Hide toggle and drawer
        AirHeader.showDrawerToggleAndToolbar(false, false);

        rLayout = (LinearLayout) view.findViewById(R.id.text);

        month = null;
        GregorianCalendar.getInstance().clear();
        month = (GregorianCalendar) GregorianCalendar.getInstance(TimeZone.getTimeZone("GMT+05:30"), Locale.US);

        // Wed Apr 20 15:58:48 GMT+05:30 2016
        dateObj = new ServerDateSharedPrefernce(mainActivity).getServerDate().split(" ");

       // month.set(Integer.parseInt(dateObj[5]), DateUtility.monthInInteger(dateObj[1]), Integer.parseInt(dateObj[2]));


        if(mainActivity.isOneWaySelected){
            month.set(Integer.parseInt(dateObj[5]), DateUtility.monthInInteger(dateObj[1]), Integer.parseInt(dateObj[2]));

        }else{
            month = setDepartureDate(AirDataHolder.getListHolder().getList().get(0).getDepDate());
        }




       // month = setServerToday(new SharedPrefrenceAir(mainActivity).getServerDate());

        itemmonth = (GregorianCalendar) month.clone();


        sdf = new SimpleDateFormat("yyyy-MM-dd");

        items = new ArrayList<String>();

        adapter = new CalendarAdapter(mainActivity, month);

        GridView gridview = (GridView) view.findViewById(R.id.gridview);
        gridview.setAdapter(adapter);

        handler = new Handler();
        handler.post(calendarUpdater);

        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));

        RelativeLayout previous = (RelativeLayout) view.findViewById(R.id.previous);

        previous.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setPreviousMonth();
                refreshCalendar();
            }
        });

        RelativeLayout next = (RelativeLayout) view.findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setNextMonth();
                refreshCalendar();
            }
        });


        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                try {
                    LinearLayout lObjLayout = (LinearLayout) parent.getChildAt(position);
                    TextView lObjTextView = (TextView) lObjLayout.getChildAt(0);
                    if (lObjTextView.getVisibility() == View.INVISIBLE) {
                        return;
                    }
                    // removing the previous view if added
                    if (((LinearLayout) rLayout).getChildCount() > 0) {
                        ((LinearLayout) rLayout).removeAllViews();
                    }
                    desc = new ArrayList<String>();
                    date = new ArrayList<String>();
                    ((CalendarAdapter) parent.getAdapter()).setSelected(v);
                    selectedGridDate = CalendarAdapter.dayString.get(position);//2016-06-28
                    String[] separatedTime = selectedGridDate.split("-");
                    String gridvalueString = separatedTime[2].replaceFirst("^0*", "");// taking last part of date. ie; 2 from 2012-12-02.
                    int gridvalue = Integer.parseInt(gridvalueString);
                    // navigate to next or previous month on clicking offdays.
                    if ((gridvalue > 10) && (position < 8)) {
                        setPreviousMonth();
                        refreshCalendar();
                    } else if ((gridvalue < 7) && (position > 28)) {
                        setNextMonth();
                        refreshCalendar();
                    }
                    ((CalendarAdapter) parent.getAdapter()).setSelected(v);

                    for (int i = 0; i < startDates.size(); i++)
                    {
                        if (startDates.get(i).equals(selectedGridDate))
                        {
                            desc.add(nameOfEvent.get(i));
                        }
                    }

                    if (desc.size() > 0) {
                        for (int i = 0; i < desc.size(); i++) {
                            TextView rowTextView = new TextView(mainActivity);

                            // set some properties of rowTextView or something
                            rowTextView.setText("Event:" + desc.get(i));
                            rowTextView.setTextColor(Color.BLACK);

                            // add the textview to the linearlayout
                            rLayout.addView(rowTextView);
                        }
                    }

                    desc = null;

//                    Calendar c = Calendar.getInstance(TimeZone.getTimeZone("GMT+05:30"), Locale.US);
//
//                    c.set(Integer.parseInt(dateObj[5]),DateUtility.monthInInteger(dateObj[1]),Integer.parseInt(dateObj[2]));
//
//                    int currentyear = c.get(Calendar.YEAR);
//                    int curentmonth = c.get(Calendar.MONTH) + 1;
//                    int currentdate = c.get(Calendar.DAY_OF_MONTH);

                    String curDate = new ServerDateSharedPrefernce(mainActivity).getServerDate();//new SimpleDateFormat(DateUtility.lStrGMTDateFormater).format(new Date());
                    String[] dateArray =  curDate.split(" ");


                    try {
                           date1 = sdf.parse(selectedGridDate);
                            date2 = sdf.parse(dateArray[5] + "-" + DateUtility.monthInInteger(dateArray[1]) + "-" + dateArray[2]);

                           // String curDate = new ServerDateSharedPrefernce(mainActivity).getServerDate();//new SimpleDateFormat(DateUtility.lStrGMTDateFormater).format(new Date());
                            //Wed May 25 00:00:00 GMT+05:30 2016
                           // String[] dateArray = curDate.split(" ");
                            String formatedDateWithZeroHourTime = dateArray[0] +" "+dateArray[1]+" "+dateArray[2]+" 00:00:00 "+dateArray[4]+" "+dateArray[5];
                            Date currentDate = DateUtility.getDateInFormat(formatedDateWithZeroHourTime);

                            if (!date1.before(currentDate)) {

                                if (!date1.after(getDateAfterOneYearFromToday(currentDate))) {
                                AppLogger.enable();
                                AppLogger.e("selectedGridDate : ", selectedGridDate + "");
                                AppLogger.e("Saved date in holder : ", date2 + "");
                                AppLogger.e("Saved date in holder : ", date1 + "");

                                /**
                                 * Handling of selected dep and return date
                                 * */
                                if (mainActivity.isDepartureDateSelected) {
                                    AirDataHolder.getListHolder().getList().get(0).setDepDate(date1.toString());
                                    /**
                                     * If return date is before return date set it same date
                                     */
                                    if(DateUtility.isReturnDateSmallerThenDepDate(date1.toString(), AirDataHolder.getListHolder().getList().get(0).getReturnDate())) {
                                        AirDataHolder.getListHolder().getList().get(0).setReturnDate(date1.toString());
                                    }
                                    // SEND USER TO PLANNER SCREEN
                                    mainActivity.lastActiveFragment = Constant.PLANNER_FRAGMENT;
                                    //mainActivity.isComingFromSideLTC = false;
                                    ProjectUtil.replaceFragment(mainActivity, new FragmentPlanner(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);


                                } else {
                                    /**
                                     * If return date is selected check if its greater than departure date
                                     */
                                    if (DateUtility.isReturnDateGreaterThenDepDate(date1.toString(), AirDataHolder.getListHolder().getList().get(0).getDepDate())) {
                                        AirDataHolder.getListHolder().getList().get(0).setReturnDate(date1.toString());
                                        // SEND USER TO PLANNER SCREEN
                                        mainActivity.lastActiveFragment = Constant.PLANNER_FRAGMENT;
                                        //mainActivity.isComingFromSideLTC = false;
                                        ProjectUtil.replaceFragment(mainActivity, new FragmentPlanner(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                                    } else {
                                        new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.CALENDER_INVALID_RETURN_DATE), mainActivity.getResources().getString(R.string.CALENDER_HEADER), Constant.ALERT_ACTION_ONE).generateAlert();
                                    }
                                }


                            } else {

                                new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.CALENDER_INVALID_DATE_AFTER_ONE_YEAR), mainActivity.getResources().getString(R.string.CALENDER_HEADER), Constant.ALERT_ACTION_ONE).generateAlert();

                            }

                        } else {
                            if (mainActivity.isOneWaySelected) {

                                new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.CALENDER_INVALID_ONWARD_DATE), mainActivity.getResources().getString(R.string.CALENDER_HEADER), Constant.ALERT_ACTION_ONE).generateAlert();

                            } else {
                                new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.CALENDER_INVALID_RETURN_DATE), mainActivity.getResources().getString(R.string.CALENDER_HEADER), Constant.ALERT_ACTION_ONE).generateAlert();

                            }
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        return view;
    }


    protected void setNextMonth() {
        try {
            if (month.get(GregorianCalendar.MONTH) == month
                    .getActualMaximum(GregorianCalendar.MONTH)) {
                month.set((month.get(GregorianCalendar.YEAR) + 1),
                        month.getActualMinimum(GregorianCalendar.MONTH), 1);
            } else {
                month.set(GregorianCalendar.MONTH,
                        month.get(GregorianCalendar.MONTH) + 1);
            }
        } catch (Exception e) {
            AppLogger.enable();
            AppLogger.e("setNextMonth ", e.getLocalizedMessage());
        }

    }

    protected void setPreviousMonth() {

        try {
            Calendar c = Calendar.getInstance();
            int currentyear = c.get(Calendar.YEAR);
            int curentmonth = c.get(Calendar.MONTH);
            // int currentdate = c.get(Calendar.DAY_OF_MONTH);

            if (currentyear == (month.get(GregorianCalendar.YEAR))
                    && (curentmonth == month.get(GregorianCalendar.MONTH))) {

            } else {
                if (month.get(GregorianCalendar.MONTH) == month.getActualMinimum(GregorianCalendar.MONTH)) {

                    month.set((month.get(GregorianCalendar.YEAR) - 1),
                            month.getActualMaximum(GregorianCalendar.MONTH), 1);

                } else {
                    month.set(GregorianCalendar.MONTH,
                            month.get(GregorianCalendar.MONTH) - 1);
                }

            }
        } catch (Exception e) {
            AppLogger.enable();
            AppLogger.e("setPreviousMonth ", e.getLocalizedMessage());
        }

    }


    public Runnable calendarUpdater = new Runnable() {

        @Override
        public void run() {
            items.clear();

            // Print dates of the current week
            // DateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            // String itemvalue;
            // event = Utility.readCalendarEvent(CalendarView.this);
            // Log.d("=====Event====", event.toString());
            // Log.d("=====Date ARRAY====", Utility.startDates.toString());
            //
            // for (int i = 0; i < Utility.startDates.size(); i++) {
            // itemvalue = df.format(itemmonth.getTime());
            // itemmonth.add(GregorianCalendar.DATE, 1);
            // items.add(Utility.startDates.get(i).toString());
            // }
            // adapter.setItems(items);
            // adapter.notifyDataSetChanged();
        }
    };


    public void refreshCalendar() {
        try {
            TextView title = (TextView) mainActivity.findViewById(R.id.title);

            adapter.refreshDays();
            adapter.notifyDataSetChanged();
            handler.post(calendarUpdater); // generate some calendar items
            if (title != null) {
                title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));
            }

        } catch (Exception e) {
            AppLogger.enable();
            AppLogger.e("refreshCalendar ", e.getLocalizedMessage());
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mainActivity = (ActivityMain) activity;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);



    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        ActivityMain.activeFragment = Constant.CALENDER_FRAGMENT;
    }

    public GregorianCalendar setServerToday(String lStrTodayDate) { // Wed Apr 20 15:58:48 GMT+05:30 2016

        String str[] = lStrTodayDate.split(" ");

        Date date = null;
        try {
            date = new SimpleDateFormat("MMM").parse(str[1]);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        GregorianCalendar gc = new GregorianCalendar();

        gc.setLenient(false);
        gc.set(GregorianCalendar.YEAR, Integer.parseInt(str[5]));
        gc.set(GregorianCalendar.MONTH, cal.get(Calendar.MONTH));
        gc.set(GregorianCalendar.DATE, Integer.parseInt(str[2]));

        return gc;
    }

    private Date getDateAfterOneYearFromToday(Date currentDate){

        Calendar lObjCal = new GregorianCalendar();
        lObjCal.setTime(currentDate);
        lObjCal.add(Calendar.YEAR, 1);
        Date afterOneYearDate = lObjCal.getTime();

        return afterOneYearDate;
    }

    public GregorianCalendar setDepartureDate(String lStrTodayDate) { // Wed Apr 20 15:58:48 GMT+05:30 2016

        String str[] = lStrTodayDate.split(" ");
        GregorianCalendar gc = new GregorianCalendar();

        gc.setLenient(false);
        gc.set(GregorianCalendar.YEAR, Integer.parseInt(str[5]));
        gc.set(GregorianCalendar.MONTH, DateUtility.monthInInteger(str[1]));
        gc.set(GregorianCalendar.DATE, Integer.parseInt(str[2]));

        return gc;
    }
}
