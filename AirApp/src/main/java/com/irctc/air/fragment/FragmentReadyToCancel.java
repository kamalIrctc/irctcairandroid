package com.irctc.air.fragment;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.irctc.air.AppController;
import com.irctc.air.Database.AirDatabase;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.adapter.AdapterBookingHistory;
import com.irctc.air.adapter.AdapterReadyToCancelPassengerList;
import com.irctc.air.adapter.AdapterReadyToCancelTicket;
import com.irctc.air.adapter.TicketOneWayAdapter;
import com.irctc.air.header.AirHeader;
import com.irctc.air.model.ModelBookingHistory;
import com.irctc.air.model.ModelCancelTicket;
import com.irctc.air.model.ModelPnrPax;
import com.irctc.air.model.booking_history.Data;
import com.irctc.air.model.booking_history.PojoBookingHistory;
import com.irctc.air.model.passenger_list.ModelPassengerListItem;
import com.irctc.air.model.route_list.ModelRoutListItem;
import com.irctc.air.model.ticket.LstIrFbFareDetail;
import com.irctc.air.model.ticket.Passengers;
import com.irctc.air.model.ticket.PojoTicket;
import com.irctc.air.model.ticket.TicketInfo;
import com.irctc.air.networking.Networking;
import com.irctc.air.round.trip.domastic.model.LstBaggageDetails;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.ListViewInsideScrollView;
import com.irctc.air.util.NetworkingUtils;
import com.irctc.air.util.ProjectUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.irctc.air.activity.ActivityMain.context;

/**
 * Created by asif on 4/30/2016.
 */
public class FragmentReadyToCancel extends Fragment {
    Button btnCancelTicket;
    TicketInfo[] ticketInfoFinal;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AdapterReadyToCancelPassengerList.pnrPaxList = null;
        AdapterReadyToCancelPassengerList.cancelTicketList = null;
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter("BROADCAST_CANCEL"));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_ticket, null);
        TextView toolbar = (TextView) view.findViewById(R.id.toolbar);
        btnCancelTicket = (Button) view.findViewById(R.id.BTN_CANCEL_TICKET);
        toolbar.setText("Ticket");
        AirDatabase airDatabase = new AirDatabase(getActivity().getApplicationContext());
        String authToken = airDatabase.getAuthToken();
        AirHeader.showRecentSearchIcon(false);
        AirHeader.showHeaderText((ActivityMain) getActivity(), true, "Ticket");
        AirHeader.showDrawerToggleAndToolbar(false, false);
        NetworkingUtils.showProgress(getActivity());
        Networking.checkTransactionStatus(authToken, BookingHistoryFragment.transactionId,
                //"5999107453",
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("ReadyRESPONSE", response.toString());
                        NetworkingUtils.dismissProgress();
                        Gson gson = new Gson();
                        PojoTicket pojoTicket = gson.fromJson(response.toString(), PojoTicket.class);
                        if (pojoTicket.getStatus().equals("SUCCESS")) {
                            ((TextView) view.findViewById(R.id.ticketBookingStatus)).setText("Success");
                            ((TextView) view.findViewById(R.id.transactionId)).setText(BookingHistoryFragment.transactionId);
                            ((TextView) view.findViewById(R.id.sourceStationCode)).setText(pojoTicket.getData().getTicketInfo()[0].getOrigin());
                            ((TextView) view.findViewById(R.id.destinationStationCode)).setText(pojoTicket.getData().getTicketInfo()[0].getDestination());
                            ((TextView) view.findViewById(R.id.sourceStationCodeReturn)).setText(pojoTicket.getData().getTicketInfo()[0].getDestination());
                            ((TextView) view.findViewById(R.id.destinationStationCodeReturn)).setText(pojoTicket.getData().getTicketInfo()[0].getOrigin());
                            TicketInfo[] ticketInfo = pojoTicket.getData().getTicketInfo();
                            ticketInfoFinal = ticketInfo;
                            ArrayList<ModelRoutListItem> arrayListReturnRouteList = new ArrayList<>();
                            ArrayList<ModelRoutListItem> arrayListOnwardRouteList = new ArrayList<>();
                            Map<String, String> pnrMap = new HashMap<String, String>();
                            LstBaggageDetails lstBaggageDetails = new LstBaggageDetails();
                            for (int i = 0; i < pojoTicket.getData().getLstBaggageDetails().length; i++) {
                                if (pojoTicket.getData().getLstBaggageDetails()[i].getPaxType().equalsIgnoreCase("ADT")){
                                    lstBaggageDetails = pojoTicket.getData().getLstBaggageDetails()[i];
                                    break;
                                }
                            }
                            for (int i = 0; i < ticketInfo.length; i++) {
                                ModelRoutListItem modelRoutListItem = new ModelRoutListItem();

                                String flightCode = ticketInfo[i].getAirline() + "-" + ticketInfo[i].getFlightNumber();
                                String fromAirportCode = ticketInfo[i].getOrigin();
                                String fromAirportName = ticketInfo[i].getOriginCity();
                                String fromAirportTerminal = "";
                                if (ticketInfo[i].getOriginAirport().contains("Terminal")) {
                                    fromAirportTerminal = "Terminal " + ticketInfo[i].getOriginAirport().split("Terminal")[1];
                                }
                                String toAirportCode = ticketInfo[i].getDestination();
                                String toAirportName = ticketInfo[i].getDestinationCity();
                                String toAirportTerminal = "";
                                if (ticketInfo[i].getDestinationAirport().contains("Terminal")) {
                                    toAirportTerminal = "Terminal " + ticketInfo[i].getDestinationAirport().split("Terminal")[1];
                                }
                                String carrear = ticketInfo[i].getAirline();
                                String travelDuration = ticketInfo[i].getDuration();
                                String departureDate = ticketInfo[i].getDepartureTime();
                                String arrivalDate = ticketInfo[i].getArrivalTime();
                                String pnr = "";
                                if (ticketInfo[i].getPnr() != null) {
                                    pnr = ticketInfo[i].getPnr();
                                }
                                ArrayList<ModelPassengerListItem> arrayListPassengers = new ArrayList<>();
                                Passengers[] passengersList = ticketInfo[i].getPassengers();
                                for (int j = 0; j < passengersList.length; j++) {
                                    ModelPassengerListItem modelPassengerListItem = new ModelPassengerListItem();
                                    modelPassengerListItem.setFirstName(passengersList[j].getFirstName());
                                    modelPassengerListItem.setLastName(passengersList[j].getLastName());
                                    modelPassengerListItem.setGender(passengersList[j].getTitle());
                                    modelPassengerListItem.setType(passengersList[j].getPaxType());
                                    modelPassengerListItem.setDeliveryStatus(passengersList[j].getStatus());
                                    modelPassengerListItem.setSegNo(passengersList[j].getSegmentNo());
                                    modelPassengerListItem.setOid(passengersList[j].getOid());
                                    modelPassengerListItem.setPaxNo(passengersList[j].getPaxNo());
                                    arrayListPassengers.add(modelPassengerListItem);
                                }
                                modelRoutListItem.setCarrear(carrear);
                                modelRoutListItem.setFreeMeal(ticketInfo[i].isFreeMeal());
                                modelRoutListItem.setServiceProvider(pojoTicket.getData().getServiceProvider());
                                modelRoutListItem.setLstBaggageDetails(lstBaggageDetails);
                                modelRoutListItem.setPassengersList(arrayListPassengers);
                                modelRoutListItem.setFlightCode(flightCode);
                                modelRoutListItem.setFromAirportCode(fromAirportCode);
                                modelRoutListItem.setFromAirportName(fromAirportName);
                                modelRoutListItem.setFromAirportTerminal(fromAirportTerminal);
                                modelRoutListItem.setToAirportCode(toAirportCode);
                                modelRoutListItem.setToAirportName(toAirportName);
                                modelRoutListItem.setToAirportTerminal(toAirportTerminal);
                                modelRoutListItem.setTravelDuration(travelDuration);
                                DateFormat formatter = new SimpleDateFormat("dd MMM");
                                DateFormat formatter2 = new SimpleDateFormat("HH:mm");

                                Calendar calendar = Calendar.getInstance();
                                calendar.setTimeInMillis(Long.parseLong(departureDate));
                                modelRoutListItem.setDepartureDate(formatter.format(calendar.getTime()));
                                modelRoutListItem.setDepartureTime(formatter2.format(calendar.getTime()));
                                calendar.setTimeInMillis(Long.parseLong(arrivalDate));
                                modelRoutListItem.setArrivalDate(formatter.format(calendar.getTime()));
                                modelRoutListItem.setArrivalTime(formatter2.format(calendar.getTime()));
                                modelRoutListItem.setArrivalTimeAnd(ticketInfo[i].getArrivalTimeAnd());
                                modelRoutListItem.setDepartureTimeAnd(ticketInfo[i].getDepartureTimeAnd());
                                //modelRoutListItem.setHalt(halt);
                                if (pojoTicket.getData().getIsSpl().equals("true")) {
                                    if (ticketInfo[i].getSegmentType().equals("O")) {
                                        modelRoutListItem.setPnr(pnr);
                                        arrayListOnwardRouteList.add(modelRoutListItem);
                                    } else {
                                        modelRoutListItem.setPnr(pnr);
                                        arrayListReturnRouteList.add(modelRoutListItem);
                                    }
                                } else {
                                    if (i > 0) {
                                        if (pnrMap.containsKey(pnr)) {
                                            modelRoutListItem.setPnr(pnr);
                                            arrayListOnwardRouteList.add(modelRoutListItem);
                                            pnrMap.put(pnr, "");
                                        } else {
                                            modelRoutListItem.setPnr(pnr);
                                            arrayListReturnRouteList.add(modelRoutListItem);
                                        }
                                    } else {
                                        modelRoutListItem.setPnr(pnr);
                                        arrayListOnwardRouteList.add(modelRoutListItem);
                                        pnrMap.put(pnr, "");
                                    }
                                }
                            }
                            ListView listView1 = ((ListView) view.findViewById(R.id.listView));
                            listView1.setAdapter(new AdapterReadyToCancelTicket(getActivity(), arrayListOnwardRouteList));
                            ListViewInsideScrollView.open(listView1);
                            ((TextView) view.findViewById(R.id.ticketPnr)).setText(pojoTicket.getData().getTicketInfo()[0].getPnr());
                            if (arrayListReturnRouteList.size() > 0) {
                                ((TextView) view.findViewById(R.id.ticketPnrReturn)).setText(arrayListReturnRouteList.get(0).getPnr());
                                ((CardView) view.findViewById(R.id.returnBlock)).setVisibility(View.VISIBLE);
                                ListView listView = ((ListView) view.findViewById(R.id.listViewReturn));
                                listView.setAdapter(new AdapterReadyToCancelTicket(getActivity(), arrayListReturnRouteList));
                                ListViewInsideScrollView.open(listView);
                            }
                            LstIrFbFareDetail[] fareDetails = pojoTicket.getData().getLstIrFbFareDetail();
                            float basefare = 0.0f, totalTax = 0.0f, subTotal = 0.0f, irctcBookingCharges = 0.0f;
                            for (int i = 0; i < fareDetails.length; i++) {
                                basefare = basefare + Float.parseFloat(fareDetails[i].getBaseFare());
                                totalTax = totalTax + Float.parseFloat(fareDetails[i].getTotalTax());
                                try {
                                    irctcBookingCharges = irctcBookingCharges + Float.parseFloat(fareDetails[i].getTax3());
                                } catch (Exception e) {
                                }
                            }

                            ((TextView) view.findViewById(R.id.baseFare)).setText(getString(R.string.RUPEE_SYMBOL) + String.valueOf(basefare));
                            ((TextView) view.findViewById(R.id.taxesAndFee)).setText(getString(R.string.RUPEE_SYMBOL) + String.valueOf(totalTax));
                            ((TextView) view.findViewById(R.id.TXT_FARE_TOTAL)).setText(getString(R.string.RUPEE_SYMBOL) + String.valueOf(basefare + totalTax + irctcBookingCharges));
                            ((TextView) view.findViewById(R.id.irctcBookingCharges)).setText(getString(R.string.RUPEE_SYMBOL) + String.valueOf(irctcBookingCharges));
                            ((ScrollView) view.findViewById(R.id.scrollView)).postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    ((ScrollView) view.findViewById(R.id.scrollView)).scrollTo(0, 0);
                                }
                            }, 200);
                        } else {
                            Toast.makeText(getActivity(), pojoTicket.getStatus(), Toast.LENGTH_LONG).show();
                            ((TextView) view.findViewById(R.id.ticketBookingStatus)).setText("Failed");
                            ((TextView) view.findViewById(R.id.transactionId)).setText(FragmentAddPassengers.transactionId);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("error", error.toString());
                        NetworkingUtils.dismissProgress();
                        NetworkingUtils.noInternetAccess(getActivity());
                    }
                });
        return view;
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {
            if (btnCancelTicket != null) {
                if (AdapterReadyToCancelPassengerList.pnrPaxList != null) {
                    if (AdapterReadyToCancelPassengerList.pnrPaxList.size() > 0) {
                        btnCancelTicket.setVisibility(View.VISIBLE);
                        btnCancelTicket.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String cancilationReq = new Gson().toJson(makeQuery(), ModelCancelTicket[].class);
                                cancelTicket(cancilationReq);
                            }
                        });
                    } else {
                        btnCancelTicket.setVisibility(View.GONE);
                    }
                }
            }
        }
    };


    private ModelCancelTicket[] makeQuery() {
        List<ModelCancelTicket> cancelTicketList = new ArrayList<>();
        for (int i = 0; i < ticketInfoFinal.length; i++) {
            for (int j = 0; j < ticketInfoFinal[i].getPassengers().length; j++) {
                String pnr = ticketInfoFinal[i].getPnr();
                String paxNo = ticketInfoFinal[i].getPassengers()[j].getPaxNo();
                String tarvelClass = ticketInfoFinal[i].getTarvelClass();
                for (ModelPnrPax modelPnrPax : AdapterReadyToCancelPassengerList.pnrPaxList) {
                    if (modelPnrPax.getPnr().equals(pnr) && modelPnrPax.getPaxNo().equals(paxNo)) {
                        String transId = BookingHistoryFragment.transactionId;
                        String segNo = ticketInfoFinal[i].getPassengers()[j].getSegmentNo();
                        String oid = ticketInfoFinal[i].getPassengers()[j].getOid();
                        String payType = ticketInfoFinal[i].getPassengers()[j].getPaxType();
                        String ticketNo = ticketInfoFinal[i].getPassengers()[j].getTicketNo();

                        String airlineCode = ticketInfoFinal[i].getAirline();
                        ModelCancelTicket cancelTicket = new ModelCancelTicket();
                        cancelTicket.setTransId(transId);
                        cancelTicket.setPnr(pnr);
                        cancelTicket.setPaxNo(paxNo);
                        cancelTicket.setSegNo(segNo);
                        cancelTicket.setOid(oid);
                        cancelTicket.setAirlineCode(airlineCode);
                        cancelTicket.setPaxType(payType);
                        cancelTicket.setTicketNo(ticketNo);
                        cancelTicket.setTarvelClass(tarvelClass);

                        boolean canAdd = true;
                        for (ModelCancelTicket cancelTicket1 : cancelTicketList) {
                            if (cancelTicket.getTransId().equals(cancelTicket1.getTransId()) && cancelTicket.getPnr().equals(cancelTicket1.getPnr()) && cancelTicket.getPaxNo().equals(cancelTicket1.getPaxNo()) && cancelTicket.getSegNo().equals(cancelTicket1.getSegNo()) && cancelTicket.getOid().equals(cancelTicket1.getOid()) && cancelTicket.getAirlineCode().equals(cancelTicket1.getAirlineCode())) {
                                canAdd = false;
                            }
                        }
                        if (canAdd) {
                            cancelTicketList.add(cancelTicket);
                        }
                    }
                }
            }
        }
        ModelCancelTicket[] stockArr = new ModelCancelTicket[cancelTicketList.size()];
        stockArr = cancelTicketList.toArray(stockArr);
        return stockArr;
    }


    private void cancelTicket(String cancelReq) {
        NetworkingUtils.showProgress(getActivity());
        AirDatabase airDatabase = new AirDatabase(getActivity().getApplicationContext());
        String authToken = airDatabase.getAuthToken();
        Networking.cancelTicket(authToken, cancelReq, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                NetworkingUtils.dismissProgress();
                Log.e("Response", response.toString());
                try {
                    if (response.get("status").equals("SUCCESS")) {
                        showDialog(response.get("message").toString());
                        btnCancelTicket.setVisibility(View.GONE);
                    } else {
                        showDialog(response.get("message").toString());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkingUtils.dismissProgress();
                Log.e("Error", error.toString());
                if (error.networkResponse != null)
                    Toast.makeText(getActivity().getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(getActivity().getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showDialog(String s) {
        String msg = "";
        if (s.equals("SUCCESS")) {
            msg = "Ticket Cancelled Successfully.";
        } else {
            msg = s;
        }
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        dialog.dismiss();
                        getActivity().onBackPressed();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(msg).setCancelable(false).setPositiveButton("OK", dialogClickListener).show();
    }

    @Override
    public void onResume() {
        super.onResume();
        AppController.getInstance().trackScreenView("Ready to cancel Screen");
    }
}