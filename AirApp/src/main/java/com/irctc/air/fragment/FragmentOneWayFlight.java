package com.irctc.air.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.irctc.air.AppController;
import com.irctc.air.Database.SharedPrefrenceAir;
import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.adapter.AdapterOneWayFlightInfo;
import com.irctc.air.adapter.AdapterOneWaySearchResults;
import com.irctc.air.adapter.FlightSearchAdapter;
import com.irctc.air.adapter.OnwardFlightInfoAdapter;
import com.irctc.air.header.AirHeader;
import com.irctc.air.model.FlightFilterBean;
import com.irctc.air.model.FlightOnWardDetailBean;
import com.irctc.air.model.InnerFlightDetailsBeans;
import com.irctc.air.model.gst.ModelGstDetails;
import com.irctc.air.model.reprice_one_way.PojoOneWayReprice;
import com.irctc.air.model.route_list.ModelRoutListItem;
import com.irctc.air.model.search_result_one_way.Flights;
import com.irctc.air.round.trip.domastic.model.LstBaggageDetails;
import com.irctc.air.util.AlertDialogUtil;
import com.irctc.air.util.AppKeys;
import com.irctc.air.util.AppLogger;
import com.irctc.air.util.Constant;
import com.irctc.air.util.DateUtility;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.FarePriceSort;
import com.irctc.air.util.NetworkingUtils;
import com.irctc.air.util.OnewayFilterEventHandler;
import com.irctc.air.util.Pref;
import com.irctc.air.util.ProjectUtil;
import com.irctc.air.util.sorting.SortByArrivalTime;
import com.irctc.air.util.sorting.SortByDepartTime;
import com.irctc.air.util.sorting.SortByDuration;
import com.irctc.air.util.sorting.SortByFarePrice;
import com.irctc.air.util.sorting.SortByFlightName;
import com.irctc.air.networking.Networking;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import static com.irctc.air.activity.ActivityMain.context;

public class FragmentOneWayFlight extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener, OnewayFilterEventHandler.AfterApply {

    private Button btnOneWayAirline;
    private Button btnOneWayDepart;
    private Button btnOneWayArrive;
    private Button btnOneWayDuration;
    private Button btnOneWayPrice;
    public static ListView flightListView;
    FlightSearchAdapter flightSearchAdapter;
    private ActivityMain mainActivity;
    LinearLayout mFloatinFilterBtn;
    private LinearLayout onwFlightSearchFlightlLBook, onwFlightInfoLayout;
    private boolean btnAirline, btnPrice, btnDepart, btnArrive, btnDuration;
    private ImageView imgRecentSearch;
    FlightFilterBean filterBean;
    public static Flights flightForReprice;
    private int selectedPosition = 0;
    public static com.irctc.air.model.reprice_one_way.Data data;
    private ArrayList<FlightOnWardDetailBean> mAirFlightMainHolderClone;
    private ArrayList<FlightOnWardDetailBean> alFliterOnwFlight;

    private Dialog lObjDialogShowFlightDetails;

    public static ArrayList<Boolean> checkStatus;
    // INFO FILTER DIALOG
    private Dialog lObjDialogShowFilterOption;
    TextView noFlights, onwFlightSearcFlightPrice;
    public static String gstInNumber, gstCompanyName, gstEmailId;
    public static boolean gstFlag = false;
    private View view;

    public FragmentOneWayFlight() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        OnewayFilterEventHandler.selectedFlight = null;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_oneway, null);
        initAllData();
        AirHeader.showRecentSearchIcon(true);
        AirHeader.showDrawerToggleAndToolbar(true, true);
        AirHeader.showHeaderText(mainActivity, false, "");

        /*

        try {
            mainActivity.isComingFromFlightListFrag = true;

            // Initialize

            // Header visibility

            // Show header text

            // Show&Hide toggle and drawer

            // Set data in variable from holder
            //setdataInVarFromHolder();
            onwayOnwardLayout();

        } catch (Exception e) {

            getActivity().finish();
        }
*/

        return view;

    }

    private void initAllData() {
        initializeVariable();
        resetFilter();

        Collections.sort(FragmentPlanner.searchOneWayFlightsList, new SortByFarePrice());
        btnPrice = true;
        Drawable img = getContext().getResources().getDrawable(R.drawable.up_arrow);
        img.setBounds(1, 1, 16, 23);
        btnOneWayPrice.setCompoundDrawables(null, null, img, null);
        btnPrice = true;
        sortButtonHandler(5, btnOneWayPrice);
        if (FragmentPlanner.searchOneWayFlightsList.size() > 0) noFlights.setVisibility(View.GONE);
        else noFlights.setVisibility(View.VISIBLE);
        noFlights.bringToFront();
        flightListView.setAdapter(new AdapterOneWaySearchResults(mainActivity, FragmentPlanner.searchOneWayFlightsList));
        flightListView.setItemChecked(selectedPosition, true);
        onwFlightSearcFlightPrice.setText(getString(R.string.symbol_rs) + " " + FragmentPlanner.searchOneWayFlightsList.get(selectedPosition).getPrice());
        flightListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                onwFlightSearcFlightPrice.setText(getString(R.string.symbol_rs) + " " + FragmentPlanner.searchOneWayFlightsList.get(i).getPrice());
                selectedPosition = i;
            }
        });
    }

    private void resetFilter() {
        //mainActivity.isComingFromFilterFrag = false;
        checkStatus = null;
        Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_CHECK_1, false);
        Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_CHECK_2, false);
        Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_CHECK_3, false);
        Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_CHECK_4, false);
        Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_STOP_1, false);
        Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_STOP_2, false);
        Pref.setBooleanParam(mainActivity.getApplicationContext(), AppKeys.USER_CHOICE_STOP_3, false);
    }

    private void onwayOnwardLayout() {

        selectedPosition = 0;
        flightListView.setItemChecked(0, true);

        // HERE WE GET THE GDS FLIGHTS
        ArrayList<FlightOnWardDetailBean> allRoundOnwardFlight = new ArrayList<>();

        if (!mainActivity.isComingFromFilterFrag) {

            // mAirFlightMainHolderClone = (ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().clone();

        } else {

            //  mAirFlightMainHolderClone =  alFliterOnwFlight;
            //mainActivity.isComingFromFilterFrag = false;
        }

//        for (int i = 0; i < allRoundOnwardFlight.size(); i++) {
//
//            FlightOnWardDetailBean flightDetailBean = allRoundOnwardFlight.get(i);
//            mAirFlightMainHolderClone.add(flightDetailBean);
//        }
        // HERE WE SET THE DATA INTO  ROUND ONWARD VARIABLES FROM MAIN HOLDER CLONE
        setDataInOnewayOnwFromHolder();
    }

    private void setDataInOnewayOnwFromHolder() {

        if (mAirFlightMainHolderClone.size() > 0) {

//            flightSearchAdapter = new FlightSearchAdapter(mainActivity, mAirFlightMainHolderClone);
//            flightListView.setAdapter(flightSearchAdapter);
            flightListView.setAdapter(new AdapterOneWaySearchResults(mainActivity, FragmentPlanner.searchOneWayFlightsList));
            flightListView.setItemChecked(selectedPosition, true);
            defaultSortingOnFare();
            setFirstFlightPrice();
        }
    }

    private void setdataInVarFromHolder() {

        mAirFlightMainHolderClone = (ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().clone();

        if (mainActivity.isComingFromFilterFrag) {

            mainActivity.isComingFromFilterFrag = false;
            // Call method to filter clone holder and update clone with filtered flight list
            performFilterOpration();
            // Toast.makeText(mainActivity, "Filter Result : "+mAirFlightMainHolderClone.size(), Toast.LENGTH_LONG).show();

        }

        // Sort default by price
        defaultSortingOnFare();


        if (mAirFlightMainHolderClone.size() > 0) {

            // Call adapter with clone
            flightSearchAdapter = new FlightSearchAdapter(mainActivity, mAirFlightMainHolderClone);
            flightListView.setAdapter(flightSearchAdapter);
            flightListView.setItemChecked(selectedPosition, true);

            FlightOnWardDetailBean lflightDetails = mAirFlightMainHolderClone.get(0);
            onwFlightSearcFlightPrice.setText("\u20B9 " + lflightDetails.getFlightFare());

        } else {
//            if (mainActivity.isComingFromFilterFrag) {
//                new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.FLIGHT_SEARCH_ERROR_TEXT), mainActivity.getResources().getString(R.string.FLIGHT_FILTER_SEARCH_ERROR_TEXT), Constant.ALERT_ACTION_TWO, new FragmentOneWayFlight()).generateAlert();
//            }else{
            new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.FLIGHT_SEARCH_ERROR_TEXT), mainActivity.getResources().getString(R.string.FLIGHT_SEARCH_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_TWO, new FragmentPlanner()).generateAlert();
//            }
        }
    }

    private void defaultSortingOnFare() {

        Collections.sort(mAirFlightMainHolderClone, new FarePriceSort());
        btnPrice = true;
        Drawable img = getContext().getResources().getDrawable(R.drawable.up_arrow);
        img.setBounds(1, 1, 16, 23);
        btnOneWayPrice.setCompoundDrawables(null, null, img, null);
        btnPrice = true;
        sortButtonHandler(5, btnOneWayPrice);
    }

    private void initializeVariable() {
        btnOneWayAirline = (Button) view.findViewById(R.id.btnOneWayAirline);
        btnOneWayDepart = (Button) view.findViewById(R.id.btnOneWayDepart);
        btnOneWayArrive = (Button) view.findViewById(R.id.btnOneWayArrive);
        btnOneWayDuration = (Button) view.findViewById(R.id.btnOneWayDuration);
        btnOneWayPrice = (Button) view.findViewById(R.id.btnOneWayPrice);
        flightListView = (ListView) view.findViewById(R.id.ALL_FLIGHTS_LISTVIEW);
        noFlights = (TextView) view.findViewById(R.id.noFlights);


        onwFlightInfoLayout = (LinearLayout) view.findViewById(R.id.onw_FLIGHT_INFO_LAYOUT);
        onwFlightSearchFlightlLBook = (LinearLayout) view.findViewById(R.id.onwFlightSearchFlightlLBook);
        onwFlightSearcFlightPrice = (TextView) view.findViewById(R.id.onwFlightSearcFlightPrice);
        //    onwFlightSearcFlightBook = (TextView) view.findViewById(R.id.onwFlightSearcFlightBook);

        // Recent searched
        imgRecentSearch = (ImageView) mainActivity.findViewById(R.id.RECENT_SEARCH);
        imgRecentSearch.setVisibility(View.VISIBLE);
        imgRecentSearch.setImageResource(R.drawable.recent_search);
        imgRecentSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mainActivity.lastActiveFragment = Constant.ONEWAY_FLIGHT_FRAGMENT;
                ProjectUtil.replaceFragment(mainActivity, new FragmentRecentSearch(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

            }
        });

        mFloatinFilterBtn = (LinearLayout) view.findViewById(R.id.FLOATING_BTN);
        mFloatinFilterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//
//                mainActivity.lastActiveFragment = Constant.ONEWAY_FLIGHT_FRAGMENT;
//                ProjectUtil.replaceFragment(mainActivity, new FilterFragment(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

                if (filterBean == null) {
                    filterBean = new FlightFilterBean();
                }

                showFilterDialog(filterBean);

            }
        });

        onwFlightInfoLayout.setOnClickListener(this);
        onwFlightSearchFlightlLBook.setOnClickListener(this);

        btnOneWayAirline.setOnClickListener(this);
        btnOneWayDepart.setOnClickListener(this);
        btnOneWayArrive.setOnClickListener(this);
        btnOneWayDuration.setOnClickListener(this);
        btnOneWayPrice.setOnClickListener(this);
        flightListView.setOnItemClickListener(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivity = (ActivityMain) context;
    }

    private void performFilterOpration() {

        // Get data from main holdr always
        ArrayList<FlightOnWardDetailBean> alMainFlightDetail = (ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().clone();
        //Toast.makeText(mainActivity, "Size of main FL : "+alMainFlightDetail.size(), Toast.LENGTH_SHORT).show();

        // Clear data from clone AL flight list
        mAirFlightMainHolderClone.clear();

        filterBean = AirDataHolder.getListHolder().getList().get(0).getmFlightFilterBean();

        if (filterBean.getStopZero() != 0 || filterBean.getStopOne() != 0 || filterBean.getStopTwoMore() != 0) {

            for (int i = 0; i < alMainFlightDetail.size(); i++) {

                FlightOnWardDetailBean flightDetailBean = alMainFlightDetail.get(i);

                ArrayList<InnerFlightDetailsBeans> flight = flightDetailBean.getFlight();

                if (flight.size() > 0) { //3

                    if (filterBean.getStopZero() != 0) { // S

                        if (flight.size() == 1) {  // Add flight to clone
                            mAirFlightMainHolderClone.add(flightDetailBean);
                            continue;
                        }
                    }
                    if (filterBean.getStopOne() != 0) { // S
                        if (flight.size() == 2) {  // Add flight to clone
                            mAirFlightMainHolderClone.add(flightDetailBean);
                            continue;
                        }
                    }
                    if (filterBean.getStopTwoMore() != 0) { // S
                        if (flight.size() > 2) {  // Add flight to clone
                            mAirFlightMainHolderClone.add(flightDetailBean);
                            continue;
                        }
                    }
                }
            }
        }/*else{
            mAirFlightMainHolderClone =  (ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().clone();
        }*/


        if ((mAirFlightMainHolderClone.size() > 0) && (filterBean.getStopZero() != 0 || filterBean.getStopOne() != 0 || filterBean.getStopTwoMore() != 0)) {

        } else {
            if ((filterBean.getStopZero() != 0 || filterBean.getStopOne() != 0 || filterBean.getStopTwoMore() != 0)) {

            } else {
                mAirFlightMainHolderClone = (ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().clone();
            }
        }


        if (filterBean.getOnwardOption1() != 0 || filterBean.getOnwardOption2() != 0 || filterBean.getOnwardOption3() != 0 || filterBean.getOnwardOption4() != 0) {

            if (mAirFlightMainHolderClone.size() > 0) {
                // filter the clone data on behalf of Onwards time
                filterBasedOnOnwardsTime();
            }
        }

        if (mAirFlightMainHolderClone.size() > 0) {
            if (filterBean.getIsChange() != 0) {
                // filter the clone data on behalf of fare Price
                AppLogger.enable();
                AppLogger.e("MIN Avl=> ", "" + filterBean.getMinPrice());
                AppLogger.e("MAAX Avl=> ", "" + filterBean.getMaxPrice());
                filterBasedOnPrice();
            }
        }

        if (mAirFlightMainHolderClone.size() > 0) {
            if (filterBean.getPreferredAirline().size() > 0) {
                // filter the clone data on behalf of Airline Preferred
                filterBasedOnAirlines();
            }
        }
        filterBean = null;

        AppLogger.enable();
        AppLogger.e("Flight SIZE =>", "" + mAirFlightMainHolderClone.size());

        // Show alert if filtered list is zoro and provide new clone with all data
        if (mAirFlightMainHolderClone.size() == 0) {
            mAirFlightMainHolderClone = (ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().clone();
            new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.FLIGHT_FILTER_SEARCH_ERROR_TEXT), mainActivity.getResources().getString(R.string.FLIGHT_SEARCH_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE, null).generateAlert();
        }

    }

    private void filterBasedOnPrice() {

        int length = mAirFlightMainHolderClone.size();

        for (int i = 0; i < length; i++) {

            FlightOnWardDetailBean flightDetailBean = mAirFlightMainHolderClone.get(i);

            if (flightDetailBean.getFlightFare() >= filterBean.getMinPrice() && flightDetailBean.getFlightFare() <= filterBean.getMaxPrice()) {

            } else {
                length = length - 1;
                mAirFlightMainHolderClone.remove(i);
                i--;
            }
        }
    }

    private void filterBasedOnAirlines() {

        int length = mAirFlightMainHolderClone.size();
        for (int i = 0; i < length; i++) {

            FlightOnWardDetailBean flightDetailBean = mAirFlightMainHolderClone.get(i);
            String ailline = flightDetailBean.getFlightAirline();

            if (filterBean.getPreferredAirline().size() > 0) {

                if (filterBean.getPreferredAirline().contains(ailline)) {

                } else {
                    length = length - 1;
                    mAirFlightMainHolderClone.remove(i);
                    i--;
                }
            }
        }
    }

    private void filterBasedOnOnwardsTime() {

        ArrayList<FlightOnWardDetailBean> filteredbasedQnw = new ArrayList<FlightOnWardDetailBean>();

        int length = mAirFlightMainHolderClone.size();
        for (int i = 0; i < length; i++) {

            FlightOnWardDetailBean flighBeans = mAirFlightMainHolderClone.get(i);
            Date dateX = DateUtility.getBeforeAfterTime(flighBeans.getFlight().get(0).getFlightDepartureTime(), 0, 0);

            if (filterBean.getOnwardOption1() != 0) {     // S

                Date date1 = DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(), 0, 1);
                Date date2 = DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(), 6, 1);

                if (compareDateTime(date1, date2, dateX)) {
                    // remains the flight in the clone
                    filteredbasedQnw.add(flighBeans);
                    //
                }
//                else {
//                    length = length-1;
//                    mAirFlightMainHolderClone.remove(i);
//                    i--;
//                    continue;
//                }
            }
            if (filterBean.getOnwardOption2() != 0) { // S

                Date date1 = DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(), 6, 1);
                Date date2 = DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(), 12, 1);

                if (compareDateTime(date1, date2, dateX)) {
                    // remains the flight in the clone
                    filteredbasedQnw.add(flighBeans);
                    //continue;
                }
//                else {
//                    length = length-1;
//                    mAirFlightMainHolderClone.remove(i);
//                    i--;
//                    continue;
//                }
            }
            if (filterBean.getOnwardOption3() != 0) {  // S
                Date date1 = DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(), 12, 1);
                Date date2 = DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(), 18, 1);

                if (compareDateTime(date1, date2, dateX)) {
                    // remains the flight in the clone
                    filteredbasedQnw.add(flighBeans);
                    //continue;
                }
//                else {
//                    length = length-1;
//                    mAirFlightMainHolderClone.remove(i);
//                    i--;
//                    continue;
//                }
            }
            if (filterBean.getOnwardOption4() != 0) {  // S

                Date date1 = DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(), 18, 1);
                Date date2 = DateUtility.getBeforeAfterTime(AirDataHolder.getListHolder().getList().get(0).getDepDate(), 24, 1);

                if (compareDateTime(date1, date2, dateX)) {
                    // remains the flight in the clone
                    filteredbasedQnw.add(flighBeans);
                    // continue;
                }
//                else {
//                    length = length-1;
//                    mAirFlightMainHolderClone.remove(i);
//                    i--;
//                    continue;
//                }
            }
        }
        mAirFlightMainHolderClone.clear();
        mAirFlightMainHolderClone.addAll(filteredbasedQnw);
    }

    private boolean compareDateTime(Date date1, Date date2, Date dateX) {

        try {

            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(date1);

            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(date2);

            Calendar calendar3 = Calendar.getInstance();
            calendar3.setTime(dateX);

            Date x = calendar3.getTime();

            if (x.after(calendar1.getTime()) && x.before(calendar2.getTime())) {
                //checkes whether the current time is between 14:49:00 and 20:11:13.
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        ArrayList<Flights> list;
        switch (v.getId()) {

            case R.id.onwFlightSearchFlightlLBook:

                if (FragmentPlanner.searchOneWayFlightsList.size() > 0) {

                    if (!ProjectUtil.checkInternetConnection(mainActivity)) {

                        new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.INTERNET_DOWN), mainActivity.getResources().getString(R.string.NO_INTERNET_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                        //ProjectUtil.showToast(getResources().getString(R.string.INTERNET_DOWN), mainActivity);

                    } else {
                        showGSTPopup();
                    }
                } else {
                    new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.FLIGHT_BOOK_ERROR), mainActivity.getResources().getString(R.string.FLIGHT_SEARCH_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                }


//
//                if(isFlightSelected) {
//
//                    //createXmlReqAndCallService(mainActivity.userSelectedUniqueIdOfFlight);
//
//                    FlightOnWardDetailBean lflightDetails = mAirFlightMainHolderClone.get(selectedPosition);
//                    createXmlReqAndCallService(lflightDetails.getFlightUniqueId());
//
//                }else{
//
////                    FlightOnWardDetailBean lflightDetails = mAirFlightMainHolderClone.get(0);
////                    onwFlightSearcFlightPrice.setText("\u20B9 "+lflightDetails.getFlightFare());
////                    // Set user selected position
////                    mainActivity.userSelectedUniqueIdOfFlight = lflightDetails.getFlightUniqueId();
////
////                    createXmlReqAndCallService(mainActivity.userSelectedUniqueIdOfFlight);
//
//                    FlightOnWardDetailBean lflightDetails = mAirFlightMainHolderClone.get(selectedPosition);
//                    onwFlightSearcFlightPrice.setText("\u20B9 "+lflightDetails.getFlightFare());
//                    // Set user selected position
//                    mainActivity.userSelectedUniqueIdOfFlight = lflightDetails.getFlightUniqueId();
//
//                    createXmlReqAndCallService(lflightDetails.getFlightUniqueId());
//                }


                break;

            case R.id.onw_FLIGHT_INFO_LAYOUT:
                //if (OnewayFilterEventHandler.selectedFlight != null) {
                if (mainActivity.isComingFromFilterFrag) {
                    showSelectedFlightDetail(OnewayFilterEventHandler.selectedFlight);
                } else {
                    if (FragmentPlanner.searchOneWayFlightsList.size() > 0) {
                        Flights lflightDetails2 = FragmentPlanner.searchOneWayFlightsList.get(selectedPosition);
                        showSelectedFlightDetail(lflightDetails2);
                    } else {
                        new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.FLIGHT_BOOK_ERROR), mainActivity.getResources().getString(R.string.FLIGHT_SEARCH_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                    }
                }
                break;

            case R.id.btnOneWayAirline:
                if (mainActivity.isComingFromFilterFrag)
                    list = OnewayFilterEventHandler.filteredFlightsArrayList;
                else list = FragmentPlanner.searchOneWayFlightsList;

                if (!btnAirline) {
                    Collections.sort(list, new SortByFlightName());
                    //flightListView.invalidateViews();
                    flightListView.setAdapter(new AdapterOneWaySearchResults(mainActivity, list));
                    Drawable img1 = getContext().getResources().getDrawable(R.drawable.up_arrow);
                    img1.setBounds(1, 1, 16, 23);
                    btnOneWayAirline.setCompoundDrawables(null, null, img1, null);
                    sortButtonHandler(1, btnOneWayAirline);
                    btnAirline = true;
                } else {
                    Collections.sort(list, new SortByFlightName());
                    Collections.reverse(list);
                    //flightListView.invalidateViews();
                    flightListView.setAdapter(new AdapterOneWaySearchResults(mainActivity, list));
                    Drawable img1 = getContext().getResources().getDrawable(R.drawable.down_arrow);
                    img1.setBounds(1, 1, 16, 23);
                    btnOneWayAirline.setCompoundDrawables(null, null, img1, null);
                    sortButtonHandler(1, btnOneWayAirline);
                    btnAirline = false;
                }
                onwFlightSearcFlightPrice.setText(mainActivity.getString(R.string.symbol_rs) + " " + list.get(0).getPrice());
                FragmentOneWayFlight.flightListView.setItemChecked(0, true);

                /*if (!btnAirline) {
                    Collections.sort(FragmentPlanner.searchOneWayFlightsList, new SortByFlightName());
                    //flightListView.invalidateViews();
                    flightListView.setAdapter(new AdapterOneWaySearchResults(mainActivity, FragmentPlanner.searchOneWayFlightsList));
                    Drawable img1 = getContext().getResources().getDrawable(R.drawable.up_arrow);
                    img1.setBounds(1, 1, 16, 23);
                    btnOneWayAirline.setCompoundDrawables(null, null, img1, null);
                    sortButtonHandler(1, btnOneWayAirline);
                    btnAirline = true;
                } else {
                    Collections.sort(FragmentPlanner.searchOneWayFlightsList, new SortByFlightName());
                    Collections.reverse(FragmentPlanner.searchOneWayFlightsList);
                    //flightListView.invalidateViews();
                    flightListView.setAdapter(new AdapterOneWaySearchResults(mainActivity, FragmentPlanner.searchOneWayFlightsList));
                    Drawable img1 = getContext().getResources().getDrawable(R.drawable.down_arrow);
                    img1.setBounds(1, 1, 16, 23);
                    btnOneWayAirline.setCompoundDrawables(null, null, img1, null);
                    sortButtonHandler(1, btnOneWayAirline);
                    btnAirline = false;
                }*/
                break;

            case R.id.btnOneWayDepart:
                if (mainActivity.isComingFromFilterFrag)
                    list = OnewayFilterEventHandler.filteredFlightsArrayList;
                else list = FragmentPlanner.searchOneWayFlightsList;
                if (!btnDepart) {
                    Collections.sort(list, new SortByDepartTime());
                    flightListView.setAdapter(new AdapterOneWaySearchResults(mainActivity, list));
                    //flightListView.invalidateViews();
                    Drawable img2 = getContext().getResources().getDrawable(R.drawable.up_arrow);
                    img2.setBounds(1, 1, 16, 23);
                    btnOneWayDepart.setCompoundDrawables(null, null, img2, null);
                    sortButtonHandler(2, btnOneWayDepart);
                    btnDepart = true;
                } else {
                    Collections.sort(list, new SortByDepartTime());
                    //flightListView.invalidateViews();
                    flightListView.setAdapter(new AdapterOneWaySearchResults(mainActivity, list));
                    Collections.reverse(list);
                    btnDepart = true;
                    Drawable img2 = getContext().getResources().getDrawable(R.drawable.down_arrow);
                    img2.setBounds(1, 1, 16, 23);
                    btnOneWayDepart.setCompoundDrawables(null, null, img2, null);
                    sortButtonHandler(2, btnOneWayDepart);
                    btnDepart = false;
                }
                onwFlightSearcFlightPrice.setText(mainActivity.getString(R.string.symbol_rs) + " " + list.get(0).getPrice());
                FragmentOneWayFlight.flightListView.setItemChecked(0, true);

                break;
            case R.id.btnOneWayArrive:
                if (mainActivity.isComingFromFilterFrag)
                    list = OnewayFilterEventHandler.filteredFlightsArrayList;
                else list = FragmentPlanner.searchOneWayFlightsList;

                if (!btnArrive) {
                    Collections.sort(list, new SortByArrivalTime());
                    flightListView.setAdapter(new AdapterOneWaySearchResults(mainActivity, list));
                    //flightListView.invalidateViews();
                    Drawable img3 = getContext().getResources().getDrawable(R.drawable.up_arrow);
                    img3.setBounds(1, 1, 16, 23);
                    btnOneWayArrive.setCompoundDrawables(null, null, img3, null);
                    btnArrive = true;
                } else {
                    Collections.sort(list, new SortByArrivalTime());
                    //flightListView.invalidateViews();
                    flightListView.setAdapter(new AdapterOneWaySearchResults(mainActivity, list));
                    Collections.reverse(list);
                    Drawable img3 = getContext().getResources().getDrawable(R.drawable.down_arrow);
                    img3.setBounds(1, 1, 16, 23);
                    btnOneWayArrive.setCompoundDrawables(null, null, img3, null);
                    btnArrive = false;
                }
                sortButtonHandler(3, btnOneWayArrive);
                onwFlightSearcFlightPrice.setText(mainActivity.getString(R.string.symbol_rs) + " " + list.get(0).getPrice());
                FragmentOneWayFlight.flightListView.setItemChecked(0, true);

                break;
            case R.id.btnOneWayDuration:
                if (mainActivity.isComingFromFilterFrag)
                    list = OnewayFilterEventHandler.filteredFlightsArrayList;
                else list = FragmentPlanner.searchOneWayFlightsList;

                if (!btnDuration) {
                    Collections.sort(list, new SortByDuration());
                    flightListView.setAdapter(new AdapterOneWaySearchResults(mainActivity, list));
                    //flightListView.invalidateViews();
                    Drawable img4 = getContext().getResources().getDrawable(R.drawable.up_arrow);
                    img4.setBounds(1, 1, 16, 23);
                    btnOneWayDuration.setCompoundDrawables(null, null, img4, null);
                    btnDuration = true;
                } else {
                    Collections.sort(list, new SortByDuration());
                    flightListView.setAdapter(new AdapterOneWaySearchResults(mainActivity, list));
                    //flightListView.invalidateViews();
                    Collections.reverse(list);
                    flightListView.invalidateViews();
                    Drawable img4 = getContext().getResources().getDrawable(R.drawable.down_arrow);
                    img4.setBounds(1, 1, 16, 23);
                    btnOneWayDuration.setCompoundDrawables(null, null, img4, null);
                    btnDuration = false;
                }
                sortButtonHandler(4, btnOneWayDuration);
                onwFlightSearcFlightPrice.setText(mainActivity.getString(R.string.symbol_rs) + " " + list.get(0).getPrice());
                FragmentOneWayFlight.flightListView.setItemChecked(0, true);

                break;
            case R.id.btnOneWayPrice:
                if (mainActivity.isComingFromFilterFrag)
                    list = OnewayFilterEventHandler.filteredFlightsArrayList;
                else list = FragmentPlanner.searchOneWayFlightsList;

                if (!btnPrice) {
                    Collections.sort(list, new SortByFarePrice());
                    flightListView.setAdapter(new AdapterOneWaySearchResults(mainActivity, list));
                    //flightListView.invalidateViews();
                    Drawable img5 = getContext().getResources().getDrawable(R.drawable.up_arrow);
                    img5.setBounds(1, 1, 16, 23);
                    btnOneWayPrice.setCompoundDrawables(null, null, img5, null);
                    btnPrice = true;
                } else {
                    Collections.sort(list, new SortByFarePrice());
                    Collections.reverse(list);
                    flightListView.setAdapter(new AdapterOneWaySearchResults(mainActivity, list));
                    //flightListView.invalidateViews();
                    Drawable img5 = getContext().getResources().getDrawable(R.drawable.down_arrow);
                    img5.setBounds(1, 1, 16, 23);
                    btnOneWayPrice.setCompoundDrawables(null, null, img5, null);
                    btnPrice = false;
                }
                sortButtonHandler(5, btnOneWayPrice);
                onwFlightSearcFlightPrice.setText(mainActivity.getString(R.string.symbol_rs) + " " + list.get(0).getPrice());
                FragmentOneWayFlight.flightListView.setItemChecked(0, true);

                break;

            default:
                break;
        }
    }

    private void showGSTPopup() {
        gstFlag = false;
        gstInNumber ="";
        gstCompanyName ="";
        gstEmailId = "";
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        dialog.dismiss();
                        showFillGSTDetailsPopup();
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        repricingFlight();
                        dialog.dismiss();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Do you want to use GST number for this booking?").setPositiveButton("Yes", dialogClickListener).setNegativeButton("No", dialogClickListener).show();
    }

    private void showFillGSTDetailsPopup() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.view_fill_gst_details);
        Button dialogButton = (Button) dialog.findViewById(R.id.btnOk);

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkValidations(dialog);
            }
        });
        dialog.show();
    }

    private void checkValidations(Dialog dialog) {
        AutoCompleteTextView gstinNumber = (AutoCompleteTextView) dialog.findViewById(R.id.gstinNumber);
        AutoCompleteTextView companyName = (AutoCompleteTextView) dialog.findViewById(R.id.companyName);
        AutoCompleteTextView emailId = (AutoCompleteTextView) dialog.findViewById(R.id.emailId);
        TextView textViewWarning = (TextView) dialog.findViewById(R.id.textViewWarning);
        if (gstinNumber.getText().toString().trim().length() == 0) {
            textViewWarning.setVisibility(View.VISIBLE);
            textViewWarning.setText("GSTIN Number Can't be blank");
        } else if (!gstinNumber.getText().toString().trim().matches("^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$")) {
            textViewWarning.setVisibility(View.VISIBLE);
            textViewWarning.setText("GSTIN Number is Invalid");
        } else if (companyName.getText().toString().trim().length() == 0) {
            textViewWarning.setVisibility(View.VISIBLE);
            textViewWarning.setText("Company Name Can't be blank");
        } else if (emailId.getText().toString().trim().length() == 0) {
            textViewWarning.setVisibility(View.VISIBLE);
            textViewWarning.setText("Email-Id Can't be blank");
        } else if (!Patterns.EMAIL_ADDRESS.matcher(emailId.getText().toString().trim()).matches()) {
            textViewWarning.setVisibility(View.VISIBLE);
            textViewWarning.setText("Email-Id is Invalid");
        } else {
            gstFlag = true;
            gstInNumber = gstinNumber.getText().toString().trim();
            gstCompanyName = companyName.getText().toString().trim();
            gstEmailId = emailId.getText().toString().trim();
            dialog.dismiss();
            repricingFlight();
        }

    }


    private void repricingFlight() {
        if (mainActivity.isComingFromFilterFrag)
            flightForReprice = OnewayFilterEventHandler.selectedFlight;
        else flightForReprice = FragmentPlanner.searchOneWayFlightsList.get(selectedPosition);
        ModelGstDetails modelGstDetails = new ModelGstDetails();
        modelGstDetails.setGstflag(gstFlag);
        modelGstDetails.setGstNumber(gstInNumber);
        modelGstDetails.setCompanyName(gstCompanyName);
        modelGstDetails.setEmailid(gstEmailId);
        String[] flightKeys = new String[]{flightForReprice.getKey()};
        NetworkingUtils.showProgress(getActivity());
        Networking.repriceOneWay(flightForReprice.getPrice(), FragmentPlanner.data.getSearchKey(), flightKeys, FragmentPlanner.data.getIsInternational(), modelGstDetails, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("Response", response.toString());
                NetworkingUtils.dismissProgress();
                Gson gson = new Gson();
                PojoOneWayReprice pojoOneWayReprice = gson.fromJson(response.toString(), PojoOneWayReprice.class);
                if (pojoOneWayReprice.getStatus().equalsIgnoreCase("SUCCESS")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.toString());
                        JSONObject data = jsonObject.getJSONObject("data");
                        if (data.has("signMap")) {
                            String signMap = data.getJSONObject("signMap").toString();
                            SharedPrefrenceAir sharedPrefrenceAir = new SharedPrefrenceAir(getContext());
                            sharedPrefrenceAir.setSignMap(signMap);
                            //Log.e("signMap", sharedPrefrenceAir.getSignMap());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    ///Log.e("signMap",pojoOneWayReprice.getData().getSignMap().toString());
                    data = pojoOneWayReprice.getData();
                    if (data.getIsPriceChange().equals("1")) {
                        showPriceChangeDialog();
                    } else {
                        ProjectUtil.replaceFragment(mainActivity, new FragmentReprice(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                    }
                } else if (pojoOneWayReprice.getData().getIssoldOut().equals("1")) {
                    Toast.makeText(context, getString(R.string.soldOut), Toast.LENGTH_SHORT).show();
//                    repricingFlight();
                    initAllData();
                } else {
                    Toast.makeText(context, pojoOneWayReprice.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse != null) {
                    if (error.networkResponse.statusCode == 500) {
                        NetworkingUtils.dismissProgress();
                        Toast.makeText(context, "Internal Server Error.", Toast.LENGTH_SHORT).show();
                    } else {
                        NetworkingUtils.dismissProgress();
                        NetworkingUtils.noInternetAccess(context);
                    }
                } else {
                    NetworkingUtils.dismissProgress();
                    NetworkingUtils.noInternetAccess(context);
                }
            }
        });
    }

   /* private void setSelectionAfterSortClick(){

        flightListView.setItemChecked(selectedPosition, true);
    }*/

    private void setFirstFlightPrice() {
        Flights lflightDetails = FragmentPlanner.searchOneWayFlightsList.get(selectedPosition);
        // onwFlightSearcFlightPrice.setText("\u20B9 " + lflightDetails.getFlightFare());

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        this.flightListView.setSelector(R.drawable.list_selector);
        Flights lflightDetails = FragmentPlanner.searchOneWayFlightsList.get(position);
        //onwFlightSearcFlightPrice.setText("\u20B9 "+lflightDetails.getFlightFare());
        // Set user selected position
        //mainActivity.userSelectedUniqueIdOfFlight = lflightDetails.getKey());

        selectedPosition = position;
    }

    private void sortButtonHandler(int buttonNo, Button button) {
        switch (buttonNo) {
            case 1:
                button.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLightBlue));
                button.setTextColor(mainActivity.getResources().getColor(R.color.colorWhite));
                btnOneWayDepart.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLessDarkGrey));
                btnOneWayArrive.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLessDarkGrey));
                btnOneWayDuration.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLessDarkGrey));
                btnOneWayPrice.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLessDarkGrey));
                btnOneWayDepart.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnOneWayArrive.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnOneWayDuration.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnOneWayPrice.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnOneWayDepart.setCompoundDrawables(null, null, null, null);
                btnOneWayArrive.setCompoundDrawables(null, null, null, null);
                btnOneWayDuration.setCompoundDrawables(null, null, null, null);
                btnOneWayPrice.setCompoundDrawables(null, null, null, null);
                btnDepart = false;
                btnArrive = false;
                btnDuration = false;
                btnPrice = false;
                break;
            case 2:
                button.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLightBlue));
                button.setTextColor(mainActivity.getResources().getColor(R.color.colorWhite));
                btnOneWayAirline.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLessDarkGrey));
                btnOneWayArrive.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLessDarkGrey));
                btnOneWayDuration.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLessDarkGrey));
                btnOneWayPrice.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLessDarkGrey));

                btnOneWayAirline.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnOneWayArrive.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnOneWayDuration.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnOneWayPrice.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));

                btnOneWayAirline.setCompoundDrawables(null, null, null, null);
                btnOneWayArrive.setCompoundDrawables(null, null, null, null);
                btnOneWayDuration.setCompoundDrawables(null, null, null, null);
                btnOneWayPrice.setCompoundDrawables(null, null, null, null);

                btnAirline = false;
                btnArrive = false;
                btnDuration = false;
                btnPrice = false;

                break;
            case 3:
                // Set background Arrive
                button.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLightBlue));
                button.setTextColor(mainActivity.getResources().getColor(R.color.colorWhite));

                btnOneWayAirline.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLessDarkGrey));
                btnOneWayDepart.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLessDarkGrey));
                btnOneWayDuration.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLessDarkGrey));
                btnOneWayPrice.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLessDarkGrey));

                btnOneWayAirline.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnOneWayDepart.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnOneWayDuration.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnOneWayPrice.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));

                btnOneWayAirline.setCompoundDrawables(null, null, null, null);
                btnOneWayDepart.setCompoundDrawables(null, null, null, null);
                btnOneWayDuration.setCompoundDrawables(null, null, null, null);
                btnOneWayPrice.setCompoundDrawables(null, null, null, null);

                btnAirline = false;
                btnDepart = false;
                btnDuration = false;
                btnPrice = false;

                break;
            case 4:
                // Set background Duration
                button.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLightBlue));
                button.setTextColor(mainActivity.getResources().getColor(R.color.colorWhite));

                btnOneWayAirline.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLessDarkGrey));
                btnOneWayDepart.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLessDarkGrey));
                btnOneWayArrive.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLessDarkGrey));
                btnOneWayPrice.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLessDarkGrey));

                btnOneWayAirline.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnOneWayDepart.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnOneWayArrive.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnOneWayPrice.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));

                btnOneWayAirline.setCompoundDrawables(null, null, null, null);
                btnOneWayDepart.setCompoundDrawables(null, null, null, null);
                btnOneWayArrive.setCompoundDrawables(null, null, null, null);
                btnOneWayPrice.setCompoundDrawables(null, null, null, null);

                btnAirline = false;
                btnDepart = false;
                btnArrive = false;
                btnPrice = false;

                break;
            case 5:

                // Set background Price
                button.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLightBlue));
                button.setTextColor(mainActivity.getResources().getColor(R.color.colorWhite));

                btnOneWayAirline.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLessDarkGrey));
                btnOneWayDepart.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLessDarkGrey));
                btnOneWayArrive.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLessDarkGrey));
                btnOneWayDuration.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLessDarkGrey));

                btnOneWayAirline.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnOneWayDepart.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnOneWayArrive.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnOneWayDuration.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));

                btnOneWayAirline.setCompoundDrawables(null, null, null, null);
                btnOneWayDepart.setCompoundDrawables(null, null, null, null);
                btnOneWayArrive.setCompoundDrawables(null, null, null, null);
                btnOneWayDuration.setCompoundDrawables(null, null, null, null);

                btnAirline = false;
                btnDepart = false;
                btnArrive = false;
                btnDuration = false;

                break;
            default:
                break;

        }
    }

    private void showSelectedFlightDetail(Flights selectedFlight) {

        if (!mainActivity.isFinishing()) {

            lObjDialogShowFlightDetails = new Dialog(mainActivity, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
            mainActivity.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            lObjDialogShowFlightDetails.requestWindowFeature(Window.FEATURE_NO_TITLE);
            lObjDialogShowFlightDetails.getWindow().setBackgroundDrawable(new ColorDrawable(Color.LTGRAY));

            // Set layout
            lObjDialogShowFlightDetails.setContentView(R.layout.onward_flight_info);


            ///// INITIALIZE VAR START //////
            TextView mOnwardFlightFromToHeader, mOnwardFlightToHeader, flightClass, mOnwardHeaderDuration, mOnwardRefundable, mOnwardFlightAmount;
            ListView lvOnwardFlightDetail;
            OnwardFlightInfoAdapter mOnwardFlightAdapter;
            LinearLayout mOnwardFlightlayoutMain;
            TextView flightBaseFare, flightTaxes, flightTotal,tv_no_free_meel;

            TextView txtOnewayRefundable;
            ImageView imgOnewayRefundable;

            TextView tvOnlyHandBaggage = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.tvOnlyHandBaggage);
            ImageView ivBag = (ImageView) lObjDialogShowFlightDetails.findViewById(R.id.ivBag);

            lvOnwardFlightDetail = (ListView) lObjDialogShowFlightDetails.findViewById(R.id.FARE_ONWARD_QUOTE_LISTVIEW);
            mOnwardFlightFromToHeader = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.ONWARDS_HEADER_FROM_TO);
            mOnwardFlightToHeader = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.ONWARDS_HEADER_TO_CODE);
            flightClass = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.flightClass);

            mOnwardHeaderDuration = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.ONWARDS_HEADER_DRATION);

            imgOnewayRefundable = (ImageView) lObjDialogShowFlightDetails.findViewById(R.id.ONEWAY_REFUNDABLE_IMG);
            txtOnewayRefundable = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.ONEWAY_REFUNDABLE_TXT);


            flightBaseFare = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.TXT_FARE_BASE);
            flightTaxes = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.TXT_FARE_TAXES);
            flightTotal = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.TXT_FARE_TOTAL);
            tv_no_free_meel = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.tv_no_free_meel);

            Button lObjBtnDone = (Button) lObjDialogShowFlightDetails.findViewById(R.id.BTN_FLIGHT_DETAIL_DONE);

            lObjBtnDone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    lObjDialogShowFlightDetails.cancel();
                }
            });
            ///// INITIALIZE VAR END //////

            ///// SET VALUES IN VAR START //////
            // AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().get(selectedFlightPos).
            if (selectedFlight.getServiceProvider().equalsIgnoreCase("GDS")) {
                float baseFare = 0.0f, tax = 0.0f, total = 0.0f;
                for (int i = 0; i < selectedFlight.getLstFareDetails().length; i++) {
                    try {
                        baseFare = baseFare + Float.parseFloat(selectedFlight.getLstFareDetails()[i].getBaseFare());
                    } catch (Exception e) {
                    }
                    try {
                        tax = tax + Float.parseFloat(selectedFlight.getLstFareDetails()[i].getTax());
                    } catch (Exception e) {
                    }
                    try {
                        total = total + Float.parseFloat(selectedFlight.getLstFareDetails()[i].getTotal());
                    } catch (Exception e) {
                    }
                }
                flightBaseFare.setText(mainActivity.getResources().getString(R.string.Rs) + baseFare);
                flightTaxes.setText(mainActivity.getResources().getString(R.string.Rs) + tax);
                flightTotal.setText(mainActivity.getResources().getString(R.string.Rs) + total);

            } else {
                flightBaseFare.setText(mainActivity.getResources().getString(R.string.Rs) + selectedFlight.getLstFareDetails()[0].getBaseFare());
                flightTaxes.setText(mainActivity.getResources().getString(R.string.Rs) + selectedFlight.getLstFareDetails()[0].getTax());
                flightTotal.setText(mainActivity.getResources().getString(R.string.Rs) + selectedFlight.getLstFareDetails()[0].getTotal());
            }
            if (selectedFlight.getLstFareDetails()[0].getBaseType().equals("false")) {
                imgOnewayRefundable.setImageResource(R.drawable.refundable_red);
                txtOnewayRefundable.setText("Non Refundable");
            } else if (selectedFlight.getLstFareDetails()[0].getBaseType().equals("true")) {
                imgOnewayRefundable.setImageResource(R.drawable.refundable_green);
                txtOnewayRefundable.setText("Refundable");
            }

            if (selectedFlight.isBaggageAllowed()){
                ivBag.setVisibility(View.VISIBLE);
                tvOnlyHandBaggage.setVisibility(View.VISIBLE);
            } else {
                ivBag.setVisibility(View.GONE);
                tvOnlyHandBaggage.setVisibility(View.GONE);
            }

            mOnwardFlightFromToHeader.setText(selectedFlight.getDepartureCityWithCode());
            mOnwardFlightToHeader.setText(selectedFlight.getArrivalCityWithCode());
            flightClass.setText("(" + FragmentPlanner.flightTravellClass + ")");
            LstBaggageDetails lstBaggageDetails = new LstBaggageDetails();
            for (int i = 0; i < selectedFlight.getLstBaggageDetails().length; i++) {
                if (selectedFlight.getLstBaggageDetails()[i].getPaxType().equalsIgnoreCase("ADT")){
                    lstBaggageDetails = selectedFlight.getLstBaggageDetails()[i];
                    break;
                }
            }

            ArrayList<ModelRoutListItem> arrayListRouteList = new ArrayList<>();
            for (int i = 0; i < selectedFlight.getLstFlightDetails().length; i++) {
                ModelRoutListItem modelRoutListItem = new ModelRoutListItem();
                String flightCode = selectedFlight.getLstFlightDetails()[i].getFlightNumber();
                String fromAirportCode = selectedFlight.getLstFlightDetails()[i].getOrigin();
                String fromAirportName = selectedFlight.getLstFlightDetails()[i].getOriginCity();
                String fromAirportTerminal = selectedFlight.getLstFlightDetails()[i].getOriginTerminal();
                String toAirportCode = selectedFlight.getLstFlightDetails()[i].getDestination();
                String toAirportName = selectedFlight.getLstFlightDetails()[i].getDestinationCity();
                String toAirportTerminal = selectedFlight.getLstFlightDetails()[i].getDestinationTerminal();
                String travelDuration = selectedFlight.getLstFlightDetails()[i].getFlightTime();
                String departureDate = selectedFlight.getLstFlightDetails()[i].getDepartureDate();
                String departureTime = selectedFlight.getLstFlightDetails()[i].getDepartureTime();
                String arrivalDate = selectedFlight.getLstFlightDetails()[i].getArrivalDate();
                String arrivalTime = selectedFlight.getLstFlightDetails()[i].getArrivalTime();
                String halt = selectedFlight.getLstFlightDetails()[i].getHalt();
                modelRoutListItem.setServiceProvider(selectedFlight.getServiceProvider());
                modelRoutListItem.setFreeMeal(selectedFlight.isFreeMeal());
                modelRoutListItem.setLstBaggageDetails(lstBaggageDetails);
                modelRoutListItem.setFlightCode(flightCode);
                modelRoutListItem.setCarrear(selectedFlight.getLstFlightDetails()[i].getAirlineCode());
                modelRoutListItem.setFromAirportCode(fromAirportCode);
                modelRoutListItem.setFromAirportName(fromAirportName);
                modelRoutListItem.setFromAirportTerminal(fromAirportTerminal);
                modelRoutListItem.setToAirportCode(toAirportCode);
                modelRoutListItem.setToAirportName(toAirportName);
                modelRoutListItem.setToAirportTerminal(toAirportTerminal);
                modelRoutListItem.setTravelDuration(travelDuration);
                modelRoutListItem.setHalt(halt);
                SimpleDateFormat formatter5 = new SimpleDateFormat("yyyy-MM-dd");
                Date depDate = null, arrDate = null;
                try {
                    depDate = (Date) formatter5.parse(departureDate.substring(0, 10));
                    arrDate = (Date) formatter5.parse(arrivalDate.substring(0, 10));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                modelRoutListItem.setDepartureDate(new SimpleDateFormat("dd MMM yyyy").format(depDate));
                modelRoutListItem.setArrivalDate(new SimpleDateFormat("dd MMM yyyy").format(arrDate));
                modelRoutListItem.setDepartureTime(departureTime);
                modelRoutListItem.setArrivalTime(arrivalTime);
                modelRoutListItem.setHalt(halt);
                arrayListRouteList.add(modelRoutListItem);
            }

            lvOnwardFlightDetail.setAdapter(new AdapterOneWayFlightInfo(mainActivity, arrayListRouteList));
            ProjectUtil.updateListViewHeight(lvOnwardFlightDetail);
            lObjDialogShowFlightDetails.show();

        }
    }

    private void showFilterDialog(FlightFilterBean filterBean) {

        //fragment_filter
        if (!mainActivity.isFinishing()) {

            lObjDialogShowFilterOption = new Dialog(mainActivity, android.R.style.Theme_DeviceDefault_Light_NoActionBar);
            mainActivity.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            lObjDialogShowFilterOption.requestWindowFeature(Window.FEATURE_NO_TITLE);
            lObjDialogShowFilterOption.getWindow().setBackgroundDrawable(new ColorDrawable(Color.LTGRAY));
            // Set layout
            lObjDialogShowFilterOption.setContentView(R.layout.fragment_filter);

            // PROVIDE ALL NEW REFRESH GDS FLIGHT DATA
            mAirFlightMainHolderClone = (ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().clone();

            new OnewayFilterEventHandler(lObjDialogShowFilterOption, FragmentPlanner.searchOneWayFlightsList, this, mainActivity, lObjDialogShowFilterOption, mAirFlightMainHolderClone, flightListView, filterBean, onwFlightSearcFlightPrice);
            // SHOW THE DAILOG
            lObjDialogShowFilterOption.show();
        }
    }

    @Override
    public void doPerform(ArrayList<FlightOnWardDetailBean> onwrdClone) {
        alFliterOnwFlight = onwrdClone;
        onwayOnwardLayout();
    }

    @Override
    public void onResume() {
        super.onResume();
        AppController.getInstance().trackScreenView("OneWay Flight List Screen");

        if (mainActivity.isComingFromFilterFrag) {
            mainActivity.isComingFromFilterFrag = false;
            selectedPosition = 0;
            flightListView.setAdapter(new AdapterOneWaySearchResults(mainActivity, FragmentPlanner.searchOneWayFlightsList));
            flightListView.setItemChecked(selectedPosition, true);
            onwFlightSearcFlightPrice.setText(getString(R.string.symbol_rs) + " " + FragmentPlanner.searchOneWayFlightsList.get(selectedPosition).getPrice());
        }
        ActivityMain.activeFragment = Constant.ONEWAY_FLIGHT_FRAGMENT;
    }


    private void showPriceChangeDialog() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        dialog.dismiss();
                        ProjectUtil.replaceFragment(mainActivity, new FragmentReprice(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Price changed, Are you sure to continue?").setPositiveButton("Yes", dialogClickListener).setNegativeButton("No", dialogClickListener).show();
    }

}
