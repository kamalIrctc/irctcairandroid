package com.irctc.air.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.irctc.air.AppController;
import com.irctc.air.Database.AirDatabase;
import com.irctc.air.Database.SharedPrefrenceAir;
import com.irctc.air.R;
import com.irctc.air.adapter.PassengerDetailAdapter;
import com.irctc.air.adapter.PassengerListAdapter;
import com.irctc.air.header.AirHeader;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.model.PassDetailbean;
import com.irctc.air.model.book_ticket.PassengerDetails;
import com.irctc.air.model.book_ticket.PassportInfo;
import com.irctc.air.model.book_ticket.PojoBookTicket;
import com.irctc.air.model.book_ticket.UserDetails;
import com.irctc.air.model.gst.ModelGstDetails;
import com.irctc.air.model.passenger.ModelPassengerDetails;
import com.irctc.air.model.reprice_one_way.Inf;
import com.irctc.air.model.reprice_one_way.LstFareDetails;
import com.irctc.air.model.reprice_one_way.PojoOneWayReprice;
import com.irctc.air.model.reprice_one_way.PricingInfo;
import com.irctc.air.util.AlertDialogUtil;
import com.irctc.air.util.AppKeys;
import com.irctc.air.util.Constant;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.NetworkingUtils;
import com.irctc.air.util.Pref;
import com.irctc.air.util.ProjectUtil;
import com.irctc.air.util.compare_dates.DateDifference;
import com.irctc.air.util.states.States;
import com.irctc.air.networking.Networking;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static com.irctc.air.activity.ActivityMain.context;

/**
 * Created by vivek on 4/25/2016.
 */
public class FragmentAddPassengers extends Fragment {

    private ArrayList<PassDetailbean> mAlPassListBean;
    private ActivityMain mainActivity;
    private ImageView imgRecentSearch;
    private Button btnSubmit;
    private ListView mLvPassengerDetail;
    private AirDatabase database;
    boolean isAdultAgeAllowed = false;
    Spinner stateList;
    FrameLayout stateListHolder;
    public static String orderId, txnAmount, paymentUrl, callbackUrl, custId, appCode, txnType;

    private LinearLayout layBookerDetail;
    private LinearLayout layBookerDetailHeader;

    private static boolean showError = false;
    public static boolean transactionInitiated = false;
    public static String transactionId;
    // added by asif
    private CheckBox checkAccept;
    private Dialog lObjDialogShowFlightDetails;
    private ProgressBar progressBar;

    private EditText userFirstName, userLastName, userPhoneNo, userEmailId, userState, empCode;
    private FrameLayout space;
    Rect visibleRect;
    boolean priceChange = false;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mainActivity = (ActivityMain) activity;
        visibleRect = new Rect();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        final View view = inflater.inflate(R.layout.passenger_detail_layout, null);
        initiatizaVar(view);
        autoFill();
        AirHeader.showRecentSearchIcon(true);
        AirHeader.showFareQuoteHeader(mainActivity, getString(R.string.traveller_details));
        AirHeader.showDrawerToggleAndToolbar(false, true);
        setData();
        //     getListAndListeners(view);
        mainActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        view.getWindowVisibleDisplayFrame(visibleRect);
        view.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {

                Rect r = new Rect();
                view.getWindowVisibleDisplayFrame(r);
                int diff = visibleRect.height() - r.height();

                if (diff > 200) {
                    space.setVisibility(View.VISIBLE);
                }
                if (diff < 200) {
                    space.setVisibility(View.GONE);
                }

            }
        });

        return view;
    }

    private void setData() {
        int adult = Integer.parseInt(FragmentPlanner.noOfAdults);
        int children = Integer.parseInt(FragmentPlanner.noOfChildren);
        int infant = Integer.parseInt(FragmentPlanner.noOfInfants);
        if (FragmentPlanner.isLTC) {
            empCode.setVisibility(View.VISIBLE);
        } else {
            empCode.setVisibility(View.GONE);
        }
        int totalPassanger = adult + children + infant;
        ArrayList<ModelPassengerDetails> arrayListPassengerList = new ArrayList<>();
        ModelPassengerDetails details;
        int adIndex = 0, chdIndex = 0, infIndex = 0;
        for (int i = 0; i < adult; i++) {
            details = new ModelPassengerDetails();
            details.setPassengerTypeString("Adult");
            if (PassengerListAdapter.selectedPassengers != null) {
                if (i < PassengerListAdapter.selectedPassengers.size()) {
                    boolean breakIndex = true;
                    for (int j = adIndex; j < PassengerListAdapter.selectedPassengers.size(); j++) {
                        if (PassengerListAdapter.selectedPassengers.get(j).getPassengerType().equals("0") && breakIndex) {
                            details.setFirstName(PassengerListAdapter.selectedPassengers.get(j).getFirstName());
                            details.setLastName(PassengerListAdapter.selectedPassengers.get(j).getLastName());
                            details.setDob(PassengerListAdapter.selectedPassengers.get(j).getDob());
                            details.setGender(PassengerListAdapter.selectedPassengers.get(j).getGender());
                            details.setPassengerType(PassengerListAdapter.selectedPassengers.get(j).getPassengerType());
                            details.setTitleType(PassengerListAdapter.selectedPassengers.get(j).getTitleType());
                            adIndex = j + 1;
                            breakIndex = false;
                        }
                    }
                }
            }
            arrayListPassengerList.add(details);
        }
        for (int i = 0; i < children; i++) {
            details = new ModelPassengerDetails();
            details.setPassengerTypeString("Child");
            if (PassengerListAdapter.selectedPassengers != null) {
                if (i < PassengerListAdapter.selectedPassengers.size()) {
                    boolean breakIndex = true;
                    for (int j = chdIndex; j < PassengerListAdapter.selectedPassengers.size(); j++) {
                        if (PassengerListAdapter.selectedPassengers.get(j).getPassengerType().equals("1") && breakIndex) {
                            details.setFirstName(PassengerListAdapter.selectedPassengers.get(j).getFirstName());
                            details.setLastName(PassengerListAdapter.selectedPassengers.get(j).getLastName());
                            details.setDob(PassengerListAdapter.selectedPassengers.get(j).getDob());
                            details.setGender(PassengerListAdapter.selectedPassengers.get(j).getGender());
                            details.setPassengerType(PassengerListAdapter.selectedPassengers.get(j).getPassengerType());
                            details.setTitleType(PassengerListAdapter.selectedPassengers.get(j).getTitleType());
                            chdIndex = j + 1;
                            breakIndex = false;
                        }
                    }
                }
            }
            arrayListPassengerList.add(details);
        }
        for (int i = 0; i < infant; i++) {
            details = new ModelPassengerDetails();
            details.setPassengerTypeString("Infant");
            if (PassengerListAdapter.selectedPassengers != null) {
                if (i < PassengerListAdapter.selectedPassengers.size()) {
                    boolean breakIndex = true;
                    for (int j = infIndex; j < PassengerListAdapter.selectedPassengers.size(); j++) {
                        if (PassengerListAdapter.selectedPassengers.get(j).getPassengerType().equals("2") && breakIndex) {
                            details.setFirstName(PassengerListAdapter.selectedPassengers.get(j).getFirstName());
                            details.setLastName(PassengerListAdapter.selectedPassengers.get(j).getLastName());
                            details.setDob(PassengerListAdapter.selectedPassengers.get(j).getDob());
                            details.setGender(PassengerListAdapter.selectedPassengers.get(j).getGender());
                            details.setPassengerType(PassengerListAdapter.selectedPassengers.get(j).getPassengerType());
                            details.setTitleType(PassengerListAdapter.selectedPassengers.get(j).getTitleType());
                            infIndex = j + 1;
                            breakIndex = false;
                        }
                    }
                }
            }
            arrayListPassengerList.add(details);
        }
        PassengerDetailAdapter passDetailAdapter = new PassengerDetailAdapter(arrayListPassengerList, mainActivity);
        mLvPassengerDetail.setAdapter(passDetailAdapter);
        ProjectUtil.updateListViewHeight(mLvPassengerDetail);
    }

    private void initiatizaVar(View view) {
        userFirstName = (EditText) view.findViewById(R.id.ET_FIRST_NAME);
        userLastName = (EditText) view.findViewById(R.id.ET_LAST_NAME);
        userPhoneNo = (EditText) view.findViewById(R.id.ET_MOB_NUM);
        userEmailId = (EditText) view.findViewById(R.id.ET_ADD_EMAIL);
        userState = (EditText) view.findViewById(R.id.state);
        empCode = (EditText) view.findViewById(R.id.ET_EMP_CODE);
        space = (FrameLayout) view.findViewById(R.id.space);
        stateList = (Spinner) view.findViewById(R.id.stateList);
        stateListHolder = (FrameLayout) view.findViewById(R.id.stateListHolder);
        if (!Pref.getString(context, AppKeys.USER_DETAIL_USER_TYPE).equals("user")) {
            userFirstName.setVisibility(View.GONE);
            userLastName.setVisibility(View.GONE);
        }
        stateList.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, States.getList(context)));
        stateList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                stateList.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.transparent));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mLvPassengerDetail = (ListView) view.findViewById(R.id.lvExp);
        checkAccept = (CheckBox) view.findViewById(R.id.CHECKBOX_ACCEPT_TERMS);

        SpannableString ss = new SpannableString(getString(R.string.accept_final_amount));
        imgRecentSearch = (ImageView) mainActivity.findViewById(R.id.RECENT_SEARCH);
        imgRecentSearch.setVisibility(View.VISIBLE);
        imgRecentSearch.setImageResource(R.drawable.addpassenger48);
        imgRecentSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ActivityMain.isRunning) {
                    mainActivity.lastActiveFragment = Constant.ADD_PASS_FRAGMENT;
                    ProjectUtil.replaceFragment(mainActivity, new FragmentPassengerList(), R.id.frame_layout, EnumAnimation.DOWN_TO_TOP);
                }
            }
        });
        checkAccept.setText(ss);
        checkAccept.setMovementMethod(LinkMovementMethod.getInstance());
        database = new AirDatabase(mainActivity);
        btnSubmit = (Button) view.findViewById(R.id.BTN_SUBMIT_PASS);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookFlight();
            }
        });

    }

    private void bookFlight() {
        ModelGstDetails modelGstDetails = new ModelGstDetails();
        modelGstDetails.setGstflag(FragmentOneWayFlight.gstFlag);
        modelGstDetails.setGstNumber(FragmentOneWayFlight.gstInNumber);
        modelGstDetails.setCompanyName(FragmentOneWayFlight.gstCompanyName);
        modelGstDetails.setEmailid(FragmentOneWayFlight.gstEmailId);
        boolean readyToProceed = true;
        final Context context = getActivity();
        AirDatabase airDatabase = new AirDatabase(context);
        String authToken = airDatabase.getAuthToken();
        SharedPrefrenceAir sharedPrefrenceAir = new SharedPrefrenceAir(getContext());
        String signMap = sharedPrefrenceAir.getSignMap();
        //region : Set User Details from Prefrences
        UserDetails userDetails = new UserDetails();
        userDetails.setAddress1(Pref.getString(context, AppKeys.USER_DETAIL_ADDRESS_1));
        userDetails.setAddress2(Pref.getString(context, AppKeys.USER_DETAIL_ADDRESS_2));
        userDetails.setFirstName(userFirstName.getText().toString().trim());
        userDetails.setLastName(userLastName.getText().toString().trim());
        userDetails.setCity(Pref.getString(context, AppKeys.USER_DETAIL_CITY));
        userDetails.setCountry(Pref.getString(context, AppKeys.USER_DETAIL_COUNTRY));
        userDetails.setPinCode(Pref.getString(context, AppKeys.USER_DETAIL_PIN_CODE));
        userDetails.setEmail(Pref.getString(context, AppKeys.USER_DETAIL_EMAIL));
        userDetails.setMobileNo(Pref.getString(context, AppKeys.USER_DETAIL_MOBILE_NO));
        userDetails.setEmpCode(empCode.getText().toString().trim());
        if (userState.getVisibility() == View.VISIBLE) {
            userDetails.setState(userState.getText().toString().trim());
        } else if (stateList.getVisibility() == View.VISIBLE) {
            userDetails.setState(stateList.getSelectedItem().toString());
        }
        if (Pref.getString(context, AppKeys.USER_DETAIL_USER_TYPE).equals("user")) {
            if (userFirstName.getText().toString().trim().length() > 0) {
                userFirstName.setHintTextColor(ContextCompat.getColor(getActivity(), android.R.color.darker_gray));
            } else {
                userFirstName.setHint("* First Name Required");
                userFirstName.setHintTextColor(ContextCompat.getColor(getActivity(), android.R.color.holo_red_dark));
                readyToProceed = false;
            }
            if (userLastName.getText().toString().trim().length() > 0) {
                userLastName.setHintTextColor(ContextCompat.getColor(getActivity(), android.R.color.darker_gray));
            } else {
                userLastName.setHint("* Last Name Required");
                userLastName.setHintTextColor(ContextCompat.getColor(getActivity(), android.R.color.holo_red_dark));
                readyToProceed = false;
            }
        }
        if (userState.getVisibility() == View.VISIBLE) {
            if (userState.getText().toString().trim().length() > 0) {
                userState.setHintTextColor(ContextCompat.getColor(getActivity(), android.R.color.darker_gray));
            } else {
                userState.setHint("* State Required");
                userState.setHintTextColor(ContextCompat.getColor(getActivity(), android.R.color.holo_red_dark));
                readyToProceed = false;
            }
        } else {
            if (!stateList.getSelectedItem().toString().equals("--Select State--")) {
                stateList.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.transparent));
            } else {
                userState.setHint("* State Required");
                stateList.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorLightRed));
                readyToProceed = false;
            }

        }
        if (userEmailId.getText().toString().trim().length() > 0) {
            userEmailId.setHintTextColor(ContextCompat.getColor(getActivity(), android.R.color.darker_gray));
        } else {
            userEmailId.setHint("* Email-Id Required");
            userEmailId.setHintTextColor(ContextCompat.getColor(getActivity(), android.R.color.holo_red_dark));
            readyToProceed = false;
        }

        if (empCode.getVisibility() == View.VISIBLE && empCode.getText().toString().trim().length() == 0) {
            empCode.setHint("* Employee Code Required");
            empCode.setHintTextColor(ContextCompat.getColor(getActivity(), android.R.color.holo_red_dark));
            readyToProceed = false;
        }

        if (userPhoneNo.getText().toString().trim().length() > 0) {
            userPhoneNo.setHintTextColor(ContextCompat.getColor(getActivity(), android.R.color.darker_gray));
        } else {
            userPhoneNo.setHint("* Phone-No Required");
            userPhoneNo.setHintTextColor(ContextCompat.getColor(getActivity(), android.R.color.holo_red_dark));
            readyToProceed = false;
        }
        //endregion
        //region : Passenger Details
        ArrayList<PassengerDetails> arrayListPassengerDetails = new ArrayList<>();
        ListView rootLayout = mLvPassengerDetail;

        List<String> adultNames = new ArrayList<>();
        List<String> childNames = new ArrayList<>();
        List<String> infantNames = new ArrayList<>();

        for (int i = 0; i < rootLayout.getChildCount(); i++) {
            LinearLayout c1 = (LinearLayout) rootLayout.getChildAt(i);
            CardView c2 = (CardView) c1.getChildAt(0);
            LinearLayout c3 = (LinearLayout) c2.getChildAt(0);
            LinearLayout c4 = (LinearLayout) c3.getChildAt(0);
            TextView c5 = (TextView) c4.getChildAt(0);
            LinearLayout c6 = (LinearLayout) c3.getChildAt(2);
            LinearLayout c7 = (LinearLayout) c6.getChildAt(0);
            RadioGroup c8 = (RadioGroup) c7.getChildAt(0);
            RadioButton c9 = (RadioButton) c8.getChildAt(0);
            RadioButton c10 = (RadioButton) c8.getChildAt(2);
            RadioButton c11 = (RadioButton) c8.getChildAt(4);
            FrameLayout c12 = (FrameLayout) c7.getChildAt(1);
            AutoCompleteTextView c13 = (AutoCompleteTextView) c12.getChildAt(0);
            FrameLayout c14 = (FrameLayout) c7.getChildAt(2);
            AutoCompleteTextView c15 = (AutoCompleteTextView) c14.getChildAt(0);
            LinearLayout c16 = (LinearLayout) c3.getChildAt(3);
            RelativeLayout c18a = (RelativeLayout) c16.getChildAt(1);
            Spinner c18 = (Spinner) c18a.getChildAt(1);
            AutoCompleteTextView c17 = (AutoCompleteTextView) c16.getChildAt(0);
            LinearLayout c19 = (LinearLayout) c3.getChildAt(4);
            LinearLayout c20 = (LinearLayout) c19.getChildAt(0);
            TextView c21 = (TextView) c20.getChildAt(1);
            LinearLayout c22 = (LinearLayout) c19.getChildAt(1);
            TextView c23 = (TextView) c22.getChildAt(1);
            //LinearLayout c18 = (LinearLayout) c3.getChildAt(4);
            //Spinner c19 = (Spinner) c18.getChildAt(0);
            //Spinner c20 = (Spinner) c18.getChildAt(1);
            //LinearLayout c21 = (LinearLayout) c3.getChildAt(5);
            //Spinner c22 = (Spinner) c21.getChildAt(0);

            PassengerDetails passengerDetails = new PassengerDetails();

            String fName = c13.getText().toString();
            fName = fName.replaceAll("^\\s+", "");

            if (fName.length() > 0) {
                if (fName.length() > 0) {
                    passengerDetails.setFirstName(c13.getText().toString().trim());
                    c13.setHintTextColor(ContextCompat.getColor(getActivity(), android.R.color.darker_gray));
                } else {
                    c13.setError("First Name should be minimum 1 character long");
                    readyToProceed = false;
                }
            } else {
                c13.setHint("* First Name Required");
                c13.setHintTextColor(ContextCompat.getColor(getActivity(), android.R.color.holo_red_dark));
                readyToProceed = false;
            }
            String lName = c15.getText().toString();
            lName = lName.replaceAll("^\\s+", "");
            if (lName.length() > 0) {
                if (lName.length() > 1) {
                    passengerDetails.setLastName(c15.getText().toString().trim());
                    c15.setHintTextColor(ContextCompat.getColor(getActivity(), android.R.color.darker_gray));
                } else {
                    c15.setError("Last Name should be minimum 2 character long");
                    readyToProceed = false;
                }
            } else {
                c15.setHint("* Last Name Required");
                c15.setHintTextColor(ContextCompat.getColor(getActivity(), android.R.color.holo_red_dark));
                readyToProceed = false;
            }
            if (FragmentPlanner.data.getIsInternational().equals("true")) {
                PassportInfo passportInfo = new PassportInfo();
                if (c17.getText().toString().trim().length() > 0) {
                    passportInfo.setNo(c17.getText().toString().trim());
                    c17.setHintTextColor(ContextCompat.getColor(getActivity(), android.R.color.darker_gray));
                } else {
                    c17.setHint("* Passport Required");
                    c17.setHintTextColor(ContextCompat.getColor(getActivity(), android.R.color.holo_red_dark));
                    readyToProceed = false;
                }
                //if (c18.getSelectedItemPosition() != 0) {
                passportInfo.setCountry(c18.getSelectedItem().toString());
                c18.setBackgroundColor(ContextCompat.getColor(getActivity(), android.R.color.white));
                /*} else {
                    c18.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorLightRed));
                    readyToProceed = false;
                }*/
                if (!c21.getText().toString().trim().equals("Issue Date")) {
                    try{
                        passportInfo.setIssueDate(new SimpleDateFormat("yyyy-mm-dd").format(new SimpleDateFormat("dd/mm/yyyy").parse(c21.getText().toString().trim())));
                    }catch(Exception e){}
                    c21.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.darker_gray));
                } else {
                    c21.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorLightRed));
                    readyToProceed = false;
                }

                if (!c23.getText().toString().trim().equals("Expire Date")) {
                    try{
                    passportInfo.setExpiryDate(new SimpleDateFormat("yyyy-mm-dd").format(new SimpleDateFormat("dd/mm/yyyy").parse(c23.getText().toString().trim())));
                    }catch(Exception e){}
                    c23.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.darker_gray));
                } else {
                    c23.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorLightRed));
                    readyToProceed = false;
                }
                passengerDetails.setPassportinfo(passportInfo);
            }


            if (c5.getText().toString().equalsIgnoreCase("Adult")) {
                passengerDetails.setPassengerType("0");
                if (adultNames.contains(fName + lName)) {
                    c13.setError("Duplicate entry found");
                    readyToProceed = false;
                }
                adultNames.add(fName + lName);
            } else if (c5.getText().toString().equalsIgnoreCase("Child")) {
                passengerDetails.setPassengerType("1");
                if (childNames.contains(fName + lName)) {
                    c13.setError("Duplicate entry found");
                    readyToProceed = false;
                }
                childNames.add(fName + lName);
            } else if (c5.getText().toString().equalsIgnoreCase("Infant")) {
                passengerDetails.setPassengerType("2");
                if (infantNames.contains(fName + lName)) {
                    c13.setError("Duplicate entry found");
                    readyToProceed = false;
                }
                infantNames.add(fName + lName);
            }
            if (c9.isChecked()) {
                passengerDetails.setGender("0");
                passengerDetails.setTitleType("0");
            } else if (c10.isChecked()) {
                passengerDetails.setGender("1");
                passengerDetails.setTitleType("1");
            } else if (c11.isChecked()) {
                passengerDetails.setGender("1");
                passengerDetails.setTitleType("2");
            } else {
                Toast.makeText(context, "Please select gender", Toast.LENGTH_SHORT).show();
                readyToProceed = false;
            }
            LinearLayout cc1 = (LinearLayout) c6.getChildAt(1);
            FrameLayout cc2 = (FrameLayout) cc1.getChildAt(0);
            TextView cc3 = (TextView) cc2.getChildAt(1);


            if(FragmentPlanner.data.getIsInternational().equals("true")){
                if (c5.getText().toString().equalsIgnoreCase("Adult")) {
                    if (cc3.getText().equals("Date of birth") || cc3.getText().equals("This field is required")) {
                        cc3.setTextColor(ContextCompat.getColor(context, android.R.color.holo_red_dark));
                        cc3.setText("This field is required");
                        readyToProceed = false;
                    } else {
                        cc3.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.darker_gray));
                        try {
                            passengerDetails.setDob(new SimpleDateFormat("yyyy-mm-dd").format(new SimpleDateFormat("dd/mm/yyyy").parse(cc3.getText().toString().trim())));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }


            if (c5.getText().toString().equalsIgnoreCase("Infant")) {
                /*if (DateDifference.get(FragmentPlanner.travellingDate, cc3.getText().toString().trim()).equals("infant")) {
                    cc3.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.darker_gray));
                    try {
                        passengerDetails.setDob(new SimpleDateFormat("yyyy-mm-dd").format(new SimpleDateFormat("dd/mm/yyyy").parse(cc3.getText().toString().trim())));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else {
                    cc3.setTextColor(ContextCompat.getColor(context, android.R.color.holo_red_dark));
                    cc3.setText("Date of Birth not valid.");
                    readyToProceed = false;
                }*/
                if (cc3.getText().equals("Date of birth") || cc3.getText().equals("This field is required")) {
                    cc3.setTextColor(ContextCompat.getColor(context, android.R.color.holo_red_dark));
                    cc3.setText("This field is required");
                    readyToProceed = false;
                } else {
                    cc3.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.darker_gray));
                    try {
                        passengerDetails.setDob(new SimpleDateFormat("yyyy-mm-dd").format(new SimpleDateFormat("dd/mm/yyyy").parse(cc3.getText().toString().trim())));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (c5.getText().toString().equalsIgnoreCase("child")) {

                if (cc3.getText().equals("Date of birth") || cc3.getText().equals("This field is required")) {
                    cc3.setTextColor(ContextCompat.getColor(context, android.R.color.holo_red_dark));
                    cc3.setText("This field is required");
                    readyToProceed = false;
                } else {
                    cc3.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.darker_gray));
                    try {
                        passengerDetails.setDob(new SimpleDateFormat("yyyy-mm-dd").format(new SimpleDateFormat("dd/mm/yyyy").parse(cc3.getText().toString().trim())));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }

            arrayListPassengerDetails.add(passengerDetails);
        }
        //endregion
        String segmentType = FragmentOneWayFlight.flightForReprice.getSegmentType();
        LstFareDetails[] lstFareDetails = FragmentOneWayFlight.data.getLstFareDetails();
        PricingInfo[] pricingInfo = FragmentOneWayFlight.data.getPricingInfo();
        String searchKey = FragmentPlanner.data.getSearchKey();
        final String[] flightKey = new String[]{FragmentOneWayFlight.flightForReprice.getKey()};
        final String isInternational = FragmentPlanner.data.getIsInternational();
        final String departureDate = FragmentOneWayFlight.flightForReprice.getDepartureDate();
        final boolean isBaggageAllowed = FragmentOneWayFlight.flightForReprice.isBaggageAllowed();
        Inf[] inf;
        if (FragmentOneWayFlight.data.getInf() != null) {
            inf = FragmentOneWayFlight.data.getInf();
        } else {
            inf = null;
        }
        String totalAmount = FragmentOneWayFlight.data.getLstFareDetails()[0].getTotal();
        String[] segKeys = new String[]{};
        boolean isSpecialFare = false;
        boolean isPriceCgange = priceChange;
        if (readyToProceed) {
            if (!checkAccept.isChecked()) {
                new AlertDialogUtil(mainActivity, "Kindly accept the terms & conditions", mainActivity.getResources().getString(R.string.ADD_PASS_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
            } else {
                final ArrayList<PassengerDetails> arrayListPassList = arrayListPassengerDetails;
                NetworkingUtils.showProgress(getActivity());
                Networking.bookFlight(isBaggageAllowed, signMap, authToken, userDetails, segmentType, lstFareDetails, pricingInfo, arrayListPassengerDetails, searchKey, flightKey, isInternational, inf, totalAmount, segKeys, isSpecialFare, isPriceCgange, modelGstDetails,null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Response", response.toString());
                        NetworkingUtils.dismissProgress();
                        try {
                            JSONObject jsonObject = new JSONObject(response.toString());
                            if (jsonObject.get("status").equals("SUCCESS")) {
                                if (jsonObject.getJSONObject("data").has("dup")) {
                                    if (jsonObject.getJSONObject("data").getString("dup").equals("true")) {
                                       // NetworkingUtils.dismissProgress();
                                        Toast.makeText(context, getString(R.string.dup_booking), Toast.LENGTH_SHORT).show();
                                    } else {
                                        if (jsonObject.getJSONObject("data").has("isPriceChange")) {
                                            if (jsonObject.getJSONObject("data").getString("isPriceChange").equals("1")) {
                                                //NetworkingUtils.dismissProgress();
                                                String total = jsonObject.getJSONObject("data").getJSONArray("lstFareDetails").getJSONObject(0).getString("total");
                                                showPriceChangeDialog(total);
                                            }
                                        } else {
                                           // NetworkingUtils.dismissProgress();
                                            Gson gson = new Gson();
                                            PojoBookTicket pojoBookTicket = gson.fromJson(response.toString(), PojoBookTicket.class);
                                            if (pojoBookTicket.getStatus().equals("SUCCESS")) {
                                                orderId = pojoBookTicket.getData().getOrder_ID();
                                                txnAmount = pojoBookTicket.getData().getTxn_AMOUNT();
                                                //paymentUrl=pojoBookTicket.getData().getPayment_URL().replace("localhost:18081",Networking.BASE_URL_IP);
                                                paymentUrl = pojoBookTicket.getData().getPayment_URL();
                                                //callbackUrl=pojoBookTicket.getData().getCallback_URL().replace("localhost:18081",Networking.BASE_URL_IP);
                                                callbackUrl = pojoBookTicket.getData().getCallback_URL();
                                                custId = pojoBookTicket.getData().getCust_ID();
                                                appCode = pojoBookTicket.getData().getApp_CODE();
                                                txnType = pojoBookTicket.getData().getTxnType();
                                                transactionInitiated = true;
                                                transactionId = orderId;
                                                if(ActivityMain.isRunning) {
                                                    mainActivity.lastActiveFragment = Constant.PLANNER_FRAGMENT;
                                                    database.insertPassengerList(arrayListPassList);
                                                    FragmentTransaction transaction;
                                                    transaction = getActivity().getSupportFragmentManager().beginTransaction();
                                                    transaction.replace(R.id.frame_layout, new FragmentPayment());
                                                    transaction.addToBackStack(null);
                                                    transaction.commit();
                                                }
                                             //   NetworkingUtils.dismissProgress();
                                            } else {
                                               // NetworkingUtils.dismissProgress();
                                                Toast.makeText(context, "Internal Server Error.", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }
                                } else {
                                    if (jsonObject.getJSONObject("data").has("isPriceChange")) {
                                        if (jsonObject.getJSONObject("data").getString("isPriceChange").equals("1")) {
                                           // NetworkingUtils.dismissProgress();
                                            String total = jsonObject.getJSONObject("data").getJSONArray("lstFareDetails").getJSONObject(0).getString("total");
                                            showPriceChangeDialog(total);
                                        }
                                    } else {
                                        //NetworkingUtils.dismissProgress();
                                        Gson gson = new Gson();
                                        PojoBookTicket pojoBookTicket = gson.fromJson(response.toString(), PojoBookTicket.class);
                                        if (pojoBookTicket.getStatus().equals("SUCCESS")) {
                                            orderId = pojoBookTicket.getData().getOrder_ID();
                                            txnAmount = pojoBookTicket.getData().getTxn_AMOUNT();
                                            //paymentUrl=pojoBookTicket.getData().getPayment_URL().replace("localhost:18081",Networking.BASE_URL_IP);
                                            paymentUrl = pojoBookTicket.getData().getPayment_URL();
                                            //callbackUrl=pojoBookTicket.getData().getCallback_URL().replace("localhost:18081",Networking.BASE_URL_IP);
                                            callbackUrl = pojoBookTicket.getData().getCallback_URL();
                                            custId = pojoBookTicket.getData().getCust_ID();
                                            appCode = pojoBookTicket.getData().getApp_CODE();
                                            txnType = pojoBookTicket.getData().getTxnType();
                                            transactionInitiated = true;
                                            transactionId = orderId;
                                            if(ActivityMain.isRunning){
                                            mainActivity.lastActiveFragment = Constant.PLANNER_FRAGMENT;
                                            database.insertPassengerList(arrayListPassList);
                                            FragmentTransaction transaction;
                                            transaction = getActivity().getSupportFragmentManager().beginTransaction();
                                            transaction.replace(R.id.frame_layout, new FragmentPayment());
                                            transaction.addToBackStack(null);
                                            transaction.commit();
                                            }
                                          //  NetworkingUtils.dismissProgress();
                                        } else {
                                            //NetworkingUtils.dismissProgress();
                                            Toast.makeText(context, "Internal Server Error.", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                            } else {
                               // NetworkingUtils.dismissProgress();
                                Toast.makeText(context, "Internal Server Error.", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                           // NetworkingUtils.dismissProgress();
                            Toast.makeText(context, "Internal Server Error.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error.networkResponse != null) {
                            if (error.networkResponse.statusCode == 500) {
                                NetworkingUtils.dismissProgress();
                                Toast.makeText(context, "Internal Server Error.", Toast.LENGTH_SHORT).show();
                            } else {
                                NetworkingUtils.dismissProgress();
                                NetworkingUtils.noInternetAccess(context);
                            }
                        } else {
                            NetworkingUtils.dismissProgress();
                            Toast.makeText(context, "Internal Server Error.", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
            }
        } else {
            Toast.makeText(getActivity(), "Fields Required", Toast.LENGTH_SHORT).show();
        }
    }

    private void showTermsCondition(String lStrURL) {

        lObjDialogShowFlightDetails = new Dialog(mainActivity, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
        mainActivity.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        lObjDialogShowFlightDetails.requestWindowFeature(Window.FEATURE_NO_TITLE);
        lObjDialogShowFlightDetails.getWindow().setBackgroundDrawable(new ColorDrawable(Color.LTGRAY));

        // Set layout
        lObjDialogShowFlightDetails.setContentView(R.layout.web_view_terms_condition);

        // initialize the variables
        WebView lObjWebview = (WebView) lObjDialogShowFlightDetails.findViewById(R.id.terms_condition_webView);
        Button btnTerms = (Button) lObjDialogShowFlightDetails.findViewById(R.id.BTN_CANCEL_TERMS);
        progressBar = (ProgressBar) lObjDialogShowFlightDetails.findViewById(R.id.terms_progressBar);
        lObjWebview.getSettings().setJavaScriptEnabled(true);
        lObjWebview.setWebViewClient(new MyWebViewClient());
        lObjWebview.clearHistory();
        lObjWebview.clearCache(true);
        lObjWebview.loadUrl(lStrURL);
        lObjWebview.getSettings().setLoadWithOverviewMode(true);

        btnTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lObjDialogShowFlightDetails.cancel();
            }
        });

        lObjDialogShowFlightDetails.show();
    }

    class MyWebViewClient extends WebViewClient {

//        public MyWebViewClient(ProgressBar progressBar) {
//           progressBar = progressBar;
//        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progressBar.setVisibility(View.GONE);
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            progressBar.setVisibility(View.VISIBLE);
            super.onPageStarted(view, url, favicon);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ActivityMain.activeFragment = Constant.ADD_PASS_FRAGMENT;
        AppController.getInstance().trackScreenView("Add Passenger Screen");
    }

    private void autoFill() {
        if(Pref.getString(getActivity(), AppKeys.USER_DETAIL_FIRST_NAME).length()>26 || Pref.getString(getActivity(), AppKeys.USER_DETAIL_LAST_NAME).length()>26){
            showNameExceedsPopup();
        }
        userFirstName.setText(Pref.getString(getActivity(), AppKeys.USER_DETAIL_FIRST_NAME));
        userLastName.setText(Pref.getString(getActivity(), AppKeys.USER_DETAIL_LAST_NAME));
        userPhoneNo.setText(Pref.getString(getActivity(), AppKeys.USER_DETAIL_MOBILE_NO));
        userEmailId.setText(Pref.getString(getActivity(), AppKeys.USER_DETAIL_EMAIL));
        userState.setText(Pref.getString(getActivity(), AppKeys.USER_DETAIL_STATE));
        if (FragmentPlanner.data.getIsInternational().equals("false")) {
            if (Pref.getString(getActivity(), AppKeys.USER_DETAIL_STATE).equals("")) {
                stateList.setVisibility(View.VISIBLE);
                stateListHolder.setVisibility(View.VISIBLE);
                userState.setVisibility(View.GONE);
            } else {
                stateList.setVisibility(View.GONE);
                stateListHolder.setVisibility(View.GONE);
                userState.setVisibility(View.VISIBLE);
            }
        } else {
            if (Pref.getString(getActivity(), AppKeys.USER_DETAIL_STATE).equals("")) {
                stateList.setVisibility(View.VISIBLE);
                stateListHolder.setVisibility(View.VISIBLE);
                userState.setVisibility(View.GONE);
            } else {
                stateList.setVisibility(View.GONE);
                stateListHolder.setVisibility(View.GONE);
                userState.setVisibility(View.VISIBLE);
            }
        }
    }

    private void showPriceChangeDialog(final String price) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        dialog.dismiss();
                        repriceAgain(price);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Price changed, Are you sure to continue?").setPositiveButton("Yes", dialogClickListener).setNegativeButton("No", dialogClickListener).show();
    }

    private void repriceAgain(final String price) {
        ModelGstDetails modelGstDetails = new ModelGstDetails();
        modelGstDetails.setGstflag(FragmentOneWayFlight.gstFlag);
        modelGstDetails.setGstNumber(FragmentOneWayFlight.gstInNumber);
        modelGstDetails.setCompanyName(FragmentOneWayFlight.gstCompanyName);
        modelGstDetails.setEmailid(FragmentOneWayFlight.gstEmailId);
        String[] flightKeys = new String[]{FragmentOneWayFlight.flightForReprice.getKey()};
        NetworkingUtils.showProgress(getActivity());
        Networking.repriceOneWay(price, FragmentPlanner.data.getSearchKey(), flightKeys, FragmentPlanner.data.getIsInternational(), modelGstDetails, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                NetworkingUtils.dismissProgress();
                Gson gson = new Gson();
                PojoOneWayReprice pojoOneWayReprice = gson.fromJson(response.toString(), PojoOneWayReprice.class);
                if (pojoOneWayReprice.getStatus().equalsIgnoreCase("SUCCESS")) {
                    try {
                        JSONObject jsonObject = new JSONObject(response.toString());
                        JSONObject data = jsonObject.getJSONObject("data");
                        if (data.has("signMap")) {
                            String signMap = data.getJSONObject("signMap").toString();
                            SharedPrefrenceAir sharedPrefrenceAir = new SharedPrefrenceAir(getContext());
                            sharedPrefrenceAir.setSignMap(signMap);
                            Log.e("signMap ", sharedPrefrenceAir.getSignMap());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    FragmentOneWayFlight.data = pojoOneWayReprice.getData();
                    priceChange = true;
                    bookFlight();
                } else {
                    Toast.makeText(context, pojoOneWayReprice.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse != null) {
                    if (error.networkResponse.statusCode == 500) {
                        NetworkingUtils.dismissProgress();
                        Toast.makeText(context, "Internal Server Error.", Toast.LENGTH_SHORT).show();
                    } else {
                        NetworkingUtils.dismissProgress();
                        NetworkingUtils.noInternetAccess(context);
                    }
                } else {
                    NetworkingUtils.dismissProgress();
                    NetworkingUtils.noInternetAccess(context);
                }
            }
        });
    }

    private void showNameExceedsPopup(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("First Name & Last Name in Contact Information can't be more than 26 characters Long, Please change accordingly.")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

}
