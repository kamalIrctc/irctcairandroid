package com.irctc.air.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.irctc.air.AppController;
import com.irctc.air.Database.AirDatabase;
import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.adapter.RecentFlightSearchAdapter;
import com.irctc.air.header.AirHeader;
import com.irctc.air.model.AirDataModel;
import com.irctc.air.model.RecentFlightSearchBean;
import com.irctc.air.model.recent_search.ModelRecentSearch;
import com.irctc.air.util.Constant;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.ListEventHandler;
import com.irctc.air.util.ProjectUtil;

import java.util.ArrayList;

/**
 * Created by vivek on 4/22/2016.
 */
public class FragmentRecentSearch extends Fragment {

    private ActivityMain mainActivity;
    private ListView mlvRecentSearchFlight;
    private TextView txtRecentSearch;
    ArrayList<ModelRecentSearch> arrayListRecentSearch;

    public FragmentRecentSearch() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mainActivity = (ActivityMain) activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.recent_search_flights, null);

        // Header visibility
        AirHeader.showRecentSearchIcon(false);

        // Show header text
        AirHeader.showHeaderText(mainActivity, true, "Recent Flight Search");

        // Show&Hide toggle and drawer and toolbar
        AirHeader.showDrawerToggleAndToolbar(false, false);

        // Pass AIR holder to set data in it from clicked item from recent searches
        //   mAirDataHolder = AirDataHolder.getListHolder().getList();

        // Get recent flight search from DB
        AirDatabase database = new AirDatabase(mainActivity);
        //database.open();
        ArrayList<RecentFlightSearchBean> alRecentFlightBean;
        arrayListRecentSearch = database.getRecentSearchList();
        //database.close();

        txtRecentSearch = (TextView) view.findViewById(R.id.TXT_NO_RECENT_SEARCH);
        mlvRecentSearchFlight = (ListView) view.findViewById(R.id.RECENT_FLIGHT_SEARCH_LISTVIEW);

        if (arrayListRecentSearch.size() > 0) {
            txtRecentSearch.setVisibility(View.GONE);
            mlvRecentSearchFlight.setVisibility(View.VISIBLE);
            mlvRecentSearchFlight.setAdapter(new RecentFlightSearchAdapter(mainActivity, arrayListRecentSearch));
            mlvRecentSearchFlight.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    AppController.autoFillByRecentSearch = true;
                    AppController.autoFillByRecentSearchValueIndex = position;
                    ProjectUtil.replaceFragment(mainActivity, new FragmentPlanner(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                }
            });
        } else {
            txtRecentSearch.setVisibility(View.VISIBLE);
            mlvRecentSearchFlight.setVisibility(View.GONE);
        }
        return view;

    }


    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onResume() {
        super.onResume();
        ActivityMain.activeFragment = Constant.RECENT_SEARCH_FRAGMENT;
        AppController.getInstance().trackScreenView("Recent Search Screen");
    }

}
