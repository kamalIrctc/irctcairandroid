package com.irctc.air.fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Proxy;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.auth0.android.jwt.JWT;
import com.irctc.air.AppController;
import com.irctc.air.Database.AirDatabase;
import com.irctc.air.R;
import com.irctc.air.SendMailService;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.header.AirHeader;
import com.irctc.air.model.book_ticket.PaymentPage;
import com.irctc.air.navigationdrawer.SliderMenu;
import com.irctc.air.networking.Networking;
import com.irctc.air.round.trip.domastic.fragment.DFragmentAddPassengers;
import com.irctc.air.util.AppKeys;
import com.irctc.air.util.Pref;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by asif on 4/30/2016.
 */
public class FragmentPayment extends Fragment {
    WebView webViewPayment;
    ProgressDialog progressDialog;
    boolean showTicket=true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment, null);
        FragmentPlanner.isLTC=false;
        ActivityMain.isComingFromSideLTC=false;
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Processing, Please wait...");
        progressDialog.setCancelable(false);
        AirHeader.showRecentSearchIcon(false);
        AirHeader.showHeaderText((ActivityMain) getActivity(), true, "Payment Getaway");
        AirHeader.showDrawerToggleAndToolbar(false, false);
      if(FragmentLogin.handelBack){
          android.support.v4.app.FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
          FragmentTransaction fragmentTransaction=  fragmentManager.beginTransaction();
          fragmentTransaction.replace(R.id.frame_layout,new FragmentPlanner());
          fragmentTransaction.addToBackStack(null);
          fragmentTransaction.commit();
      }

        webViewPayment = (WebView)view.findViewById(R.id.webViewPayment);
        webViewPayment.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //view.loadUrl(url.replace("localhost:18081", Networking.BASE_URL_IP));
                view.loadUrl(url);

                Log.e("Payment URL1",url);
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                if(progressDialog!=null)
                    progressDialog.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
               if(progressDialog!=null)
                progressDialog.dismiss();
                if(url.contains(Networking.CLOSE_PAYMENT_PAGE) && showTicket){
                    Intent serviceIntent = new Intent(getActivity(),SendMailService.class);
                    serviceIntent.putExtra("txnId", FragmentAddPassengers.orderId);
                    getActivity().startService(serviceIntent);
                    progressDialog=null;
                    showTicket=false;
                    ActivityMain.paymentPage=false;
                    AirDatabase airDatabase=new AirDatabase(getActivity());
                    JWT jwt=new JWT(airDatabase.getAuthToken());
                    Date dateExp=jwt.getExpiresAt();
                    Date dateCurr=new Date();
                    long duration  = dateExp.getTime()- dateCurr.getTime();
                    long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
                    if(diffInMinutes<=0){
                        Context ctx=getActivity().getApplicationContext();
                        Pref.setString(ctx, AppKeys.USER_DETAIL_ADDRESS_1,"");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_ADDRESS_2,"");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_FIRST_NAME,"");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_LAST_NAME,"");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_CITY,"");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_STATE,"");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_COUNTRY,"");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_PIN_CODE,"");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_EMAIL,"");
                        Pref.setString(ctx, AppKeys.USER_DETAIL_MOBILE_NO,"");
                        Pref.setBoolean(ctx,false);
                        SliderMenu.txtUserId.setText("Guest");
                        new AirDatabase(getActivity()).removeAuthToken();
                        AppController.isComingFromFareQuote = true;
                        AppController.routeType="OneWayTicket";
                        android.support.v4.app.FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction=  fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.frame_layout,new FragmentLogin());
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();
                    }else {
                        FragmentTransaction transaction;
                        transaction = getActivity().getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_layout, new FragmentTicket());
                        transaction.commit();
                    }
                }
            }
        });
        WebSettings webSettings = webViewPayment.getSettings();
        webSettings.setJavaScriptEnabled(true);
        String paymentPage=PaymentPage.get(
                FragmentAddPassengers.orderId,
                FragmentAddPassengers.txnAmount,
                FragmentAddPassengers.paymentUrl,
                FragmentAddPassengers.callbackUrl,
                FragmentAddPassengers.custId,
                FragmentAddPassengers.appCode,
                FragmentAddPassengers.txnType);
        Log.e("Payment Page",paymentPage);

        webViewPayment.loadDataWithBaseURL(null,paymentPage,"text/html", "UTF-8",null);
        //webViewPayment.loadUrl("https://www.google.co.in");
        //setProxy(webViewPayment, "10.34.33.3", 7071, "com.irctc.air");
        //setLPreViewWebViewProxy(getActivity().getApplicationContext(),"10.34.33.3", 7071);
        webViewPayment.setVerticalScrollBarEnabled(false);
        return view;
        }

    @Override
    public void onStart() {
        super.onStart();
        if(progressDialog!=null){
            if(progressDialog.isShowing()){
                progressDialog.dismiss();
            }
        }
        ActivityMain.paymentPage=true;
    }

    @Override
    public void onStop() {
        super.onStop();
        if(progressDialog!=null){
            if(progressDialog.isShowing()){
                progressDialog.dismiss();
            }
        }
        ActivityMain.paymentPage=false;
    }

    public static final String LOG_TAG = "otian";
    public static boolean setProxy(WebView webview, String host, int port, String applicationClassName) {

// 4.1-4.3 (JB)
        if (Build.VERSION.SDK_INT <= 18) {
            return setProxyJB(webview, host, port);
        }
// 4.4 (KK) & 5.0 (Lollipop)
        else {
            return setProxyKKPlus(webview, host, port, applicationClassName);
        }
    }
    /**
     * Set Proxy for Android 4.1 - 4.3.
     */
    @SuppressWarnings("all")
    private static boolean setProxyJB(WebView webview, String host, int port) {
        Log.d(LOG_TAG, "Setting proxy with 4.1 - 4.3 API.");

        try {
            Class wvcClass = Class.forName("android.webkit.WebViewClassic");
            Class wvParams[] = new Class[1];
            wvParams[0] = Class.forName("android.webkit.WebView");
            Method fromWebView = wvcClass.getDeclaredMethod("fromWebView", wvParams);
            Object webViewClassic = fromWebView.invoke(null, webview);

            Class wv = Class.forName("android.webkit.WebViewClassic");
            Field mWebViewCoreField = wv.getDeclaredField("mWebViewCore");
            Object mWebViewCoreFieldInstance = getFieldValueSafely(mWebViewCoreField, webViewClassic);

            Class wvc = Class.forName("android.webkit.WebViewCore");
            Field mBrowserFrameField = wvc.getDeclaredField("mBrowserFrame");
            Object mBrowserFrame = getFieldValueSafely(mBrowserFrameField, mWebViewCoreFieldInstance);

            Class bf = Class.forName("android.webkit.BrowserFrame");
            Field sJavaBridgeField = bf.getDeclaredField("sJavaBridge");
            Object sJavaBridge = getFieldValueSafely(sJavaBridgeField, mBrowserFrame);

            Class ppclass = Class.forName("android.net.ProxyProperties");
            Class pparams[] = new Class[3];
            pparams[0] = String.class;
            pparams[1] = int.class;
            pparams[2] = String.class;
            Constructor ppcont = ppclass.getConstructor(pparams);

            Class jwcjb = Class.forName("android.webkit.JWebCoreJavaBridge");
            Class params[] = new Class[1];
            params[0] = Class.forName("android.net.ProxyProperties");
            Method updateProxyInstance = jwcjb.getDeclaredMethod("updateProxy", params);

            updateProxyInstance.invoke(sJavaBridge, ppcont.newInstance(host, port, null));
        } catch (Exception ex) {
            Log.e(LOG_TAG,"Setting proxy with >= 4.1 API failed with error: " + ex.getMessage());
            return false;
        }

        Log.d(LOG_TAG, "Setting proxy with 4.1 - 4.3 API successful!");
        return true;
    }

    // from https://stackoverflow.com/questions/19979578/android-webview-set-proxy-programatically-kitkat
    @SuppressLint("NewApi")
    @SuppressWarnings("all")
    private static boolean setProxyKKPlus(WebView webView, String host, int port, String applicationClassName) {
        Log.d(LOG_TAG, "Setting proxy with >= 4.4 API.");
        Context appContext = webView.getContext().getApplicationContext();
        System.setProperty("http.proxyHost", host);
        System.setProperty("http.proxyPort", port + "");
        try {
            Class applictionCls = Class.forName(applicationClassName);
            Field loadedApkField = applictionCls.getField("mLoadedApk");
            loadedApkField.setAccessible(true);
            Object loadedApk = loadedApkField.get(appContext);
            Class loadedApkCls = Class.forName("android.app.LoadedApk");
            Field receiversField = loadedApkCls.getDeclaredField("mReceivers");
            receiversField.setAccessible(true);
            ArrayMap receivers = (ArrayMap) receiversField.get(loadedApk);
            for (Object receiverMap : receivers.values()) {
                for (Object rec : ((ArrayMap) receiverMap).keySet()) {
                    Class clazz = rec.getClass();
                    if (clazz.getName().contains("ProxyChangeListener")) {
                        Method onReceiveMethod = clazz.getDeclaredMethod("onReceive", Context.class, Intent.class);
                        Intent intent = new Intent(Proxy.PROXY_CHANGE_ACTION);

                        onReceiveMethod.invoke(rec, appContext, intent);
                    }
                }
            }

            Log.d(LOG_TAG, "Setting proxy with >= 4.4 API successful!");
            return true;
        } catch (ClassNotFoundException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exceptionAsString = sw.toString();
            Log.v(LOG_TAG, e.getMessage());
            Log.v(LOG_TAG, exceptionAsString);
        } catch (NoSuchFieldException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exceptionAsString = sw.toString();
            Log.v(LOG_TAG, e.getMessage());
            Log.v(LOG_TAG, exceptionAsString);
        } catch (IllegalAccessException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exceptionAsString = sw.toString();
            Log.v(LOG_TAG, e.getMessage());
            Log.v(LOG_TAG, exceptionAsString);
        } catch (IllegalArgumentException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exceptionAsString = sw.toString();
            Log.v(LOG_TAG, e.getMessage());
            Log.v(LOG_TAG, exceptionAsString);
        } catch (NoSuchMethodException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exceptionAsString = sw.toString();
            Log.v(LOG_TAG, e.getMessage());
            Log.v(LOG_TAG, exceptionAsString);
        } catch (InvocationTargetException e) {
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            String exceptionAsString = sw.toString();
            Log.v(LOG_TAG, e.getMessage());
            Log.v(LOG_TAG, exceptionAsString);
        }
        return false;
    }

    private static Object getFieldValueSafely(Field field, Object classInstance) throws IllegalArgumentException, IllegalAccessException {
        boolean oldAccessibleValue = field.isAccessible();
        field.setAccessible(true);
        Object result = field.get(classInstance);
        field.setAccessible(oldAccessibleValue);
        return result;
    }

    public static void setLPreViewWebViewProxy(Context context, String host, int port) {
        System.setProperty("http.proxyHost", host);
        System.setProperty("http.proxyPort", port + "");
        try {
            Context appContext = context.getApplicationContext();
            Class applictionClass = Class.forName("android.app.Application");
            Field mLoadedApkField = applictionClass.getDeclaredField("mLoadedApk");
            mLoadedApkField.setAccessible(true);
            Object mloadedApk = mLoadedApkField.get(appContext);
            Class loadedApkClass = Class.forName("android.app.LoadedApk");
            Field mReceiversField = loadedApkClass.getDeclaredField("mReceivers");
            mReceiversField.setAccessible(true);
            ArrayMap receivers = (ArrayMap) mReceiversField.get(mloadedApk);
            for (Object receiverMap : receivers.values()) {
                for (Object receiver : ((ArrayMap) receiverMap).keySet()) {
                    Class clazz = receiver.getClass();
                    if (clazz.getName().contains("ProxyChangeListener")) {
                        Method onReceiveMethod = clazz.getDeclaredMethod("onReceive", Context.class, Intent.class);
                        Intent intent = new Intent(Proxy.PROXY_CHANGE_ACTION);
                        onReceiveMethod.invoke(receiver, appContext, intent);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        AppController.getInstance().trackScreenView("Payment Screen");
    }
}