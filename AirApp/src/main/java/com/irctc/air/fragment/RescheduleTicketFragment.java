package com.irctc.air.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.irctc.air.Database.SharedPrefrenceAir;
import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.adapter.BookingHistorySegmentInfoAdapter;
import com.irctc.air.adapter.BookingHistorySelectionPassListAdapter;
import com.irctc.air.header.AirHeader;
import com.irctc.air.model.BookingHistoryBean;
import com.irctc.air.model.PassengerDetailBean;
import com.irctc.air.services.TicketCancelService;
import com.irctc.air.util.AES;
import com.irctc.air.util.AlertDialogUtil;
import com.irctc.air.util.AppLogger;
import com.irctc.air.util.Constant;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.ProjectUtil;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by tourism on 6/23/2016.
 */
public class RescheduleTicketFragment extends Fragment implements View.OnClickListener {

    ActivityMain mainActivity;
    private TextView txtBookingStatus, txtTransactionId;

    // Onward flights details
    private ListView lvBookHistOnwardFlightDetail, lvBookHistOnwardFlightPassDetail;
    BookingHistorySegmentInfoAdapter mBookHistiryOnwardFlightAdapter;
    BookingHistorySelectionPassListAdapter mBookHistOnwardPassAdapter;
    LinearLayout mCnfOnwardFlightlayoutMain ;
    TextView mCnfOnwardFlightFromToHeader,mCnfOnwardHeaderDuration,mCnfOnwardRefundable,mCnfOnwardFlightAmount;
    CheckBox chkOnwardSegment;

    // Return flights details
    private ListView lvBookHistReturnFlightDetail, lvBookHistReturnFlightPassDetail;
    BookingHistorySegmentInfoAdapter mBookHistiryReturnFlightAdapter;
    BookingHistorySelectionPassListAdapter mBookHistReturnPassAdapter;
    CardView mCnfReturnFlightlayoutMain;
    TextView mCnfReturnFlightFromToHeader, mCnfReturnHeaderDuration, mCnfReturnRefundable, mCnfReturnFlightAmount;
    CheckBox chkReturnSegment;

    LinearLayout onwardPassenger;
    LinearLayout returnPassenger;


    Button cancelTicket,BTN_CANCEL_TICKET1;

    public static int position = 0;



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivity = (ActivityMain) context;

        if (getArguments().containsKey("ClickedBookedFlightPos")) {
            position = getArguments().getInt("ClickedBookedFlightPos", 404);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.reschedule_ticket_fragment, null);
        // Initialize
        initializeVariable(view);

        // Header visibility
        AirHeader.showRecentSearchIcon(false);

        // Show header text
        AirHeader.showHeaderText(mainActivity, true, getString(R.string.reschedule_ticket));

        // Show&Hide toggle and drawer and toolbar
        AirHeader.showDrawerToggleAndToolbar(false, true);

        // Set data in variable from holder
        setdataInVarFromHolder();

        return view;
    }

    private void initializeVariable(View view) {

        cancelTicket  = (Button) view.findViewById(R.id.BTN_CANCEL_TICKET);
        chkOnwardSegment  = (CheckBox) view.findViewById(R.id.CHK_ONWARD_SEGMENT);
        chkReturnSegment  = (CheckBox) view.findViewById(R.id.CHK_RETURN_SEGMENT);

        onwardPassenger = (LinearLayout)view.findViewById(R.id.LAY_ONW_PASS_DETAILS);
        returnPassenger = (LinearLayout)view.findViewById(R.id.LAY_RET_PASS_DETAILS);
        BTN_CANCEL_TICKET1 = (Button)view.findViewById(R.id.BTN_CANCEL_TICKET1);
        BTN_CANCEL_TICKET1.setOnClickListener(this);

        // Onward flights details
        mCnfOnwardFlightlayoutMain = (LinearLayout)view.findViewById(R.id.CONFIRMATION_ONWARD_LAYOUT_MAIN);
        lvBookHistOnwardFlightDetail = (ListView) view.findViewById(R.id.CONFIRMATION_ONWARD_QUOTE_LISTVIEW);
        lvBookHistOnwardFlightPassDetail = (ListView) view.findViewById(R.id.CONFIRMATION_ONWARD_PASS_LISTVIEW);
        mCnfOnwardFlightFromToHeader = (TextView) view.findViewById(R.id.CONFIRMATION_ONWARDS_HEADER_FROM_TO);
        mCnfOnwardRefundable = (TextView) view.findViewById(R.id.CONFIRMATION_ONWARD_REFUNDABLE_TXT);

        // Return flights details
        mCnfReturnFlightlayoutMain = (CardView)view.findViewById(R.id.CONFIRMATION_RETURN_LAYOUT_MAIN);
        lvBookHistReturnFlightDetail = (ListView) view.findViewById(R.id.CONFIRMATION_RETURN_QUOTE_LISTVIEW);
        lvBookHistReturnFlightPassDetail = (ListView) view.findViewById(R.id.CONFIRMATION_RETURN_PASS_LISTVIEW);
        mCnfReturnFlightFromToHeader = (TextView) view.findViewById(R.id.CONFIRMATION_RETURN_FROM_TO_HEADER);
        mCnfReturnRefundable = (TextView) view.findViewById(R.id.CONFIRMATION_RETURN_REFUNDABLE_TXT);

        txtBookingStatus = (TextView) view.findViewById(R.id.TXT_BOOKING_STATUS_VALUE);
        txtTransactionId = (TextView) view.findViewById(R.id.TXT_TRANSACTION_ID_VALUE);

        // initialize click listener
        cancelTicket.setOnClickListener(this);
        chkOnwardSegment.setOnClickListener(this);
        chkReturnSegment.setOnClickListener(this);

    }

    private void setdataInVarFromHolder() {

        txtBookingStatus.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(0).getBookingstatusValue());
        txtTransactionId.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(0).getTrnId());

        /*************************************
         * If one way selected
         *************************************/

        mCnfOnwardFlightFromToHeader.setText(ProjectUtil.capitalFirstLetter(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getOrigin()) +" - "+ProjectUtil.capitalFirstLetter(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getDestination()));

        // ONWARD FLIGHT TICKET CONFIRMATION FLIGHT DETAIL
        mBookHistiryOnwardFlightAdapter = new BookingHistorySegmentInfoAdapter(mainActivity, AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlBookedOnwardDetail().get(0).getAlSegmentDetail());
        lvBookHistOnwardFlightDetail.setAdapter(mBookHistiryOnwardFlightAdapter);
        ProjectUtil.updateListViewHeight(lvBookHistOnwardFlightDetail);


        // ONWARD PASS PASS DETAIL
        mBookHistOnwardPassAdapter = new BookingHistorySelectionPassListAdapter(mainActivity, AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlBookedOnwardDetail().get(0).getAlPassengerDetail());
        lvBookHistOnwardFlightPassDetail.setAdapter(mBookHistOnwardPassAdapter);
        ProjectUtil.updateListViewHeight(lvBookHistOnwardFlightPassDetail);

        txtBookingStatus.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getBookingstatusValue());
        txtTransactionId.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getTrnId());

        /*************************************
         * If Round Trip selected
         *************************************/

        if(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlBookedReturnDetail().get(0).getAlSegmentDetail().size() >0) {

            mCnfReturnFlightFromToHeader.setText(ProjectUtil.capitalFirstLetter(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getDestination())+ " - "+ProjectUtil.capitalFirstLetter(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getOrigin()));

            // ONWARD FLIGHT TICKET CONFIRMATION FLIGHT DETAIL
            mBookHistiryReturnFlightAdapter = new BookingHistorySegmentInfoAdapter(mainActivity, AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlBookedReturnDetail().get(0).getAlSegmentDetail());
            lvBookHistReturnFlightDetail.setAdapter(mBookHistiryReturnFlightAdapter);
            ProjectUtil.updateListViewHeight(lvBookHistReturnFlightDetail);


            // ONWARD PASS PASS DETAIL
            mBookHistReturnPassAdapter = new BookingHistorySelectionPassListAdapter(mainActivity, AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlBookedReturnDetail().get(0).getAlPassengerDetail());
            lvBookHistReturnFlightPassDetail.setAdapter(mBookHistReturnPassAdapter);
            ProjectUtil.updateListViewHeight(lvBookHistReturnFlightPassDetail);

        }else{
            // HIDE RETURN LAYOUT
            mCnfReturnFlightlayoutMain.setVisibility(View.GONE);

        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.BTN_CANCEL_TICKET1:

                ActivityMain.lastActiveFragment = Constant.BOOKING_HISTORY_INFO_FRAGMENT;
                Fragment cancelTicketFragment = new CancelTicketFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("ClickedBookedFlightPos", position);
                //bundle.putString("commingFrom", "Booking");
                cancelTicketFragment.setArguments(bundle);
                ProjectUtil.replaceFragment(mainActivity, cancelTicketFragment, R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);


                break;

            case R.id.BTN_CANCEL_TICKET:

                String frdPass = "";
                String retPass = "";
                String segType = "";

                if(chkOnwardSegment.isChecked()){

                    ArrayList<PassengerDetailBean> alPassengerDetail = AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlBookedOnwardDetail().get(0).getAlPassengerDetail();

                    for (int i = 0; i < alPassengerDetail.size(); i++) {

                        PassengerDetailBean passengerDetailBean = alPassengerDetail.get(i);
                        passengerDetailBean.getAge();

                        if(passengerDetailBean.isSelected()){

                            frdPass = frdPass + (i+1)+",";
                        }
                    }
                }

                if(chkReturnSegment.isChecked()){

                    ArrayList<PassengerDetailBean> alPassengerDetail = AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlBookedReturnDetail().get(0).getAlPassengerDetail();

                    for (int i = 0; i < alPassengerDetail.size(); i++) {

                        PassengerDetailBean passengerDetailBean = alPassengerDetail.get(i);
                        passengerDetailBean.getAge();

                        if(passengerDetailBean.isSelected()){

                            retPass = retPass + (i+1)+",";
                        }
                    }
                }


                if(chkOnwardSegment.isChecked() && chkReturnSegment.isChecked()){
                    segType = "0,1";
                }else if(chkOnwardSegment.isChecked()){
                    segType = "0";
                }else if(chkReturnSegment.isChecked()){
                    segType = "1";
                }


                if(frdPass.length() > 0){

                    frdPass = frdPass.substring(0,frdPass.length()-1);
                }
                if(retPass.length() > 0){
                    retPass = retPass.substring(0,retPass.length()-1);
                }

                String errorMsg = isValidToContinue(frdPass,retPass);

                if(errorMsg.equalsIgnoreCase("SUCCESS")){

                    String strXML = new ProjectUtil().convertMapToXml(createXML(frdPass, retPass, segType), "cancelRequest");
                    AppLogger.enable();
                    AppLogger.e("XML =====> ",strXML);

                    if (!ProjectUtil.checkInternetConnection(mainActivity)) {

                        new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.INTERNET_DOWN), mainActivity.getResources().getString(R.string.NO_INTERNET_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();

                    } else {

                        ticketCancelRequest(new ProjectUtil().convertMapToXml(createXML(frdPass, retPass, segType), "cancelRequest"));
                    }

                }
                else{
                    new AlertDialogUtil(mainActivity, errorMsg, mainActivity.getResources().getString(R.string.CANCEL_TICKET_TITLE), Constant.ALERT_ACTION_ONE).generateAlert();

                }



                break;
            case R.id.CHK_ONWARD_SEGMENT:

                if(chkOnwardSegment.isChecked()){

                    onwardPassenger.setVisibility(View.VISIBLE);

                }else{
                    onwardPassenger.setVisibility(View.GONE);

                    ArrayList<PassengerDetailBean> alPassengerDetail = AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlBookedOnwardDetail().get(0).getAlPassengerDetail();

                    for (int i = 0; i < alPassengerDetail.size(); i++) {

                        PassengerDetailBean passengerDetailBean = alPassengerDetail.get(i);
                        passengerDetailBean.getAge();

                        if(passengerDetailBean.isSelected()){

                            passengerDetailBean.setIsSelected(false);
                        }
                    }
                    lvBookHistOnwardFlightPassDetail.invalidateViews();
                }

                break;
            case R.id.CHK_RETURN_SEGMENT:

                if (chkReturnSegment.isChecked()){

                    returnPassenger.setVisibility(View.VISIBLE);


                }else{
                    returnPassenger.setVisibility(View.GONE);


                    ArrayList<PassengerDetailBean> alPassengerDetail = AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlBookedReturnDetail().get(0).getAlPassengerDetail();

                    for (int i = 0; i < alPassengerDetail.size(); i++) {

                        PassengerDetailBean passengerDetailBean = alPassengerDetail.get(i);
                        passengerDetailBean.getAge();

                        if(passengerDetailBean.isSelected()){

                            passengerDetailBean.setIsSelected(false);
                        }
                    }
                    lvBookHistReturnFlightPassDetail.invalidateViews();
                }

                break;
        }
    }


    private String isValidToContinue(String frdPass, String retPass) {

        String resultMsg = "Please Select the Segment and choose atleast one the passenger";


        if(chkOnwardSegment.isChecked() && chkReturnSegment.isChecked()){

            if(frdPass.equalsIgnoreCase("") || retPass.equalsIgnoreCase("")){

                resultMsg = "Please select atleast one passenger";
                return resultMsg;
            }else if(!retPass.equalsIgnoreCase(frdPass)){

                resultMsg = "Only same passengers should be cancelled on both journey type (onway & return)." +
                            "\nIf you want this type of request please provide each request seperately";
                return resultMsg;
            }else{

                resultMsg = "SUCCESS";
            }
        }else if(chkOnwardSegment.isChecked()){

            if(frdPass.equalsIgnoreCase("")){

                resultMsg = "Please select atleast one passenger";
                return resultMsg;
            }else{

                resultMsg = "SUCCESS";
            }

        }else if(chkReturnSegment.isChecked()){

            if(retPass.equalsIgnoreCase("")){

                resultMsg = "Please select atleast one passenger";
                return resultMsg;
            }else{

                resultMsg = "SUCCESS";
            }
        }


        return resultMsg;
    }

    public LinkedHashMap<String,String> createXML(String frdPass, String rtnPass, String segTypeCan ){

        LinkedHashMap<String,String> mapParams = new LinkedHashMap<String,String>();

        BookingHistoryBean bookingHistoryBean = AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position);

        mapParams.put("oid", bookingHistoryBean.getTktoid());
        mapParams.put("trnoid", bookingHistoryBean.getTranoid());
        mapParams.put("segType", bookingHistoryBean.getSegtypecan());
        mapParams.put("fbNumber", bookingHistoryBean.getBrf());
        mapParams.put("visitorID", AES.decrypt(new SharedPrefrenceAir(mainActivity).getUserIdNumber()));

        if(new SharedPrefrenceAir(mainActivity).getIsGuestUser()){
            mapParams.put("visitorUserAlias", "bvairguest");
        }else{
            mapParams.put("visitorUserAlias", AES.decrypt(new SharedPrefrenceAir(mainActivity).getUserLogInId()));
        }

        mapParams.put("segTypeCan", segTypeCan);
        mapParams.put("tripType", bookingHistoryBean.getTripType());
        mapParams.put("frdPssNo", frdPass);
        mapParams.put("rtnPssNo", rtnPass);
        mapParams.put("clientIP", new SharedPrefrenceAir(mainActivity).getUserUniqueId());
        mapParams.put("trnId", bookingHistoryBean.getTrnId());

        return mapParams;
    }

    @Override
    public void onResume() {
        super.onResume();
        ActivityMain.activeFragment = Constant.CANCEL_TICKET_FRAGMENT;
    }


    private void ticketCancelRequest(final String requestXML){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mainActivity);
        alertDialog.setTitle(getString(R.string.reschedule_ticket));
        alertDialog.setMessage(R.string.RESCHEDULING_MESSAGE);
        // Setting OK Button
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
               /* TicketCancelService auth = new TicketCancelService(mainActivity, requestXML);
                auth.execute();*/

            }
        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        // Showing Alert Message
        ProjectUtil.dialogColorAlert(alertDialog);
    }
}
