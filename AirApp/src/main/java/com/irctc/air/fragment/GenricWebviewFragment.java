package com.irctc.air.fragment;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.header.AirHeader;
import com.irctc.air.networking.Networking;
import com.irctc.air.util.Constant;


/**
 * Created by vivek on 04/13/2016
 */
public class GenricWebviewFragment extends Fragment {


    private ActivityMain mainActivity;
    private TextView tvVersion;
    private String lStrTitle, lStrURL;
    private ProgressBar progressBar;
    private WebView lObjWebview;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mainActivity = (ActivityMain) activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.webview_genric, null);


        if (getArguments().containsKey("TITLE")) {
            lStrTitle = getArguments().getString("TITLE", "");
        }
        if (getArguments().containsKey("URL")) {
            lStrURL = getArguments().getString("URL", "");
        }

        initView(view);

        // Header visibility
        AirHeader.showRecentSearchIcon(false);

        // Show header text
        AirHeader.showHeaderText(mainActivity, true, lStrTitle);

        // Show&Hide toggle and drawer
        AirHeader.showDrawerToggleAndToolbar(true, true);


        return view;
    }


    private void initView(View v) {

        progressBar = (ProgressBar)v.findViewById(R.id.progressBar);



        lObjWebview = (WebView)v.findViewById(R.id.webView);
        lObjWebview.getSettings().setJavaScriptEnabled(true);
        if(lStrURL.equals(Networking.CANCEL_OLD_TICKETS)){
            lObjWebview.getSettings().setLoadWithOverviewMode(true);
            lObjWebview.getSettings().setUseWideViewPort(true);
            lObjWebview.getSettings().setBuiltInZoomControls(true);
            lObjWebview.getSettings().setDisplayZoomControls(false);
        }
        lObjWebview.setWebViewClient(new MyWebViewClient());
        lObjWebview.clearHistory();
        lObjWebview.clearCache(true);
        lObjWebview.loadUrl(lStrURL);
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            progressBar.setVisibility(View.GONE);
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            progressBar.setVisibility(View.VISIBLE);
            super.onPageStarted(view, url, favicon);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        ActivityMain.activeFragment = Constant.WEBVIEW_FRAGMENT;
    }

   /* @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (lObjWebview.canGoBack()) {
                        lObjWebview.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }*/
}
