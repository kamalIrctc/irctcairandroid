package com.irctc.air.fragment;


import android.app.DatePickerDialog;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.irctc.air.AppController;
import com.irctc.air.Database.AirDatabase;
import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.adapter.PassengerListAdapter;
import com.irctc.air.calander.CustomCalanderFragment;
import com.irctc.air.header.AirHeader;
import com.irctc.air.model.AirDataModel;
import com.irctc.air.model.RecentFlightSearchBean;
import com.irctc.air.model.recent_search.ModelRecentSearch;
import com.irctc.air.model.search_result_one_way.Flights;
import com.irctc.air.model.search_result_one_way.PojoOneWay;
import com.irctc.air.model.search_result_round_trip.Data;
import com.irctc.air.model.search_result_round_trip.IntlFlights;
import com.irctc.air.model.search_result_round_trip.PojoRoundTrip;
import com.irctc.air.networking.Networking;
import com.irctc.air.round.trip.domastic.fragment.DSearchResults;
import com.irctc.air.round.trip.domastic.model.PojoSearchResult;
import com.irctc.air.round.trip.international.fragment.SearchResultsComboList;
import com.irctc.air.util.AlertDialogUtil;
import com.irctc.air.util.Constant;
import com.irctc.air.util.DateUtility;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.NetworkingUtils;
import com.irctc.air.util.ProjectUtil;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

import static com.irctc.air.activity.ActivityMain.context;


public class FragmentPlanner extends Fragment implements View.OnClickListener {
    private Button btnRoundTrip, btnOneWay;
    private Button btnEconomy, btnBusiness;
    private Button btnSearchFlight;
    private Calendar myCalendar;
    private LinearLayout lLayoutDepartOnDate, lLayoutReturnOnDate, lLayoutReturnOnDateTriangle;
    private RelativeLayout lLayoutArrow;
    private TextView planTxtFromAirCode, planTxtToAirCode, planTxtFromCityName, planTxtToCityName;
    private TextView planTxtDepartOnDateMonth, planTxtDepartOnDay, planTxtDepartOnYear;
    public TextView planTxtReturnOnDateMonth, planTxtReturnOnDay, planTxtReturnOnYear;
    private TextView txtStaticHeader;
    public TextView planTxtDecreaseAdults, planTxtCountAdults, planTxtIncreaseAdults;
    private TextView planTxtDecreaseChild, planTxtCountChild, planTxtIncreaseChild;
    private TextView planTxtDecreaseInfant, planTxtCountInfant, planTxtIncreaseInfant;
    private ImageView imgStationCodeArrow, imgRecentSearch, imgDullCal;

    private int errorCode;
    private int adultInt, childInt, infantInt;
    int ERROR_CODE_MAX_LIMIT = 1;
    public static String travellingDate, departureDate, travellReturnDate;

    private ActivityMain mainActivity;

    ArrayList<AirDataModel> mAlPlannerData;
    AirDatabase database;
    public static FragmentTransaction transaction;

    // Used for : if coming from LTC or HOME side click
    public static boolean isLTC;
    SimpleDateFormat sdf;
    Spinner flightClass;
    CheckBox selectLtc;
    public static ArrayList<IntlFlights[]> intlFlightsArrayList;
    Gson gson;
    public static ArrayList<Flights> searchOneWayFlightsList;
    public static com.irctc.air.model.search_result_one_way.Data data;
    public static String noOfAdults = "0";
    public static String noOfChildren = "0";
    public static String noOfInfants = "0";
    public static com.irctc.air.round.trip.domastic.model.Data searchResultData;
    public static Data searchResultIntData;
    public static String flightTravellClass;
    public static FragmentPlanner instance;
    public static String orig, dest;
    private ModelRecentSearch recentSearch;

    public FragmentPlanner() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gson = new Gson();
        isLTC = false;
        flightClass = null;
        instance = this;
       /* AirDatabase airDatabase=new AirDatabase(getActivity());
        JWT jwt=new JWT(airDatabase.getAuthToken());
        Date aaa=new Date();
         jwt.isExpired(aaa.getTime());

        Date dateExp=jwt.getExpiresAt();
        Date dateCurr=new Date();
        long duration  = dateExp.getTime()- dateCurr.getTime();
        long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);

        Log.e("Time","diff "+diffInMinutes);*/
    }


    public void refreshFragment(FragmentTransaction transaction) {
        //Toast.makeText(getContext(), "refreshFragment", Toast.LENGTH_LONG).show();
        //  transaction = getActivity().getSupportFragmentManager().beginTransaction();

        // transaction.detach(this).attach(this).commit();
    }

    @Override
    public void onStart() {
        super.onStart();
        ActivityMain.backStackEnable = true;
        ltcEnable();
    }

    @Override
    public void onStop() {
        super.onStop();
        ActivityMain.backStackEnable = false;
    }

    public static FragmentPlanner newInstance() {
        //FragmentPlanner fragment = new FragmentPlanner();
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainActivity = (ActivityMain) getActivity();
        mAlPlannerData = AirDataHolder.getListHolder().getList();
        View view = inflater.inflate(R.layout.fragment_planner, null);

        //Initialize
        initializeVariable(view);
        refreshData();
        return view;
    }

    private void refreshData() {
        gson = new Gson();
        isLTC = false;

        flightClass.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.item_select_class, new String[]{getString(R.string.btn_class_economy), getString(R.string.btn_class_business), getString(R.string.btn_class_pre_economy)}));
        flightClass.getBackground().setColorFilter(getResources().getColor(android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        selectLtc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    ActivityMain.isComingFromSideLTC = true;
                    AirHeader.showHeaderText(mainActivity, true, "LTC Flight Search");
                } else {
                    ActivityMain.isComingFromSideLTC = false;
                    AirHeader.showHeaderText(mainActivity, true, "Flight Search");
                }
            }
        });
        //endregion
        try {

            sdf = new SimpleDateFormat("yyyy-MMM-dd");


            // Header visibility
            AirHeader.showRecentSearchIcon(true);

            // Show&Hide toggle and drawer
            AirHeader.showDrawerToggleAndToolbar(true, true);

            // Show header text
            if ((mainActivity.isComingFromSideLTC)) {
                AirHeader.showHeaderText(mainActivity, true, "LTC Flight Search");
            } else {
                AirHeader.showHeaderText(mainActivity, true, "Flight Search");
            }

            // Create db
            database = new AirDatabase(mainActivity);

            // Populate planner data in holder if coming from slider menu
//            if((mainActivity.isComingFromSideLTC && mainActivity.lastActiveFragment!= Constant.CALENDER_FRAGMENT)) {
//                mainActivity.setdataInAirDataHolder();
//            }

            // Set data in variable from holder
            setdataInVarFromHolder();


        } catch (Exception e) {

            getActivity().finish();
        }

        mainActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        autoFillByRecentSearches();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.PLN_IMG_ARROW_BUTTON_LAY:
                changeStnWithAnim();

                break;

            case R.id.btnRoundTrip:
                handelOneWayRoundTrip("r");
                break;

            case R.id.btnOneWay:
                handelOneWayRoundTrip("o");
                break;

            case R.id.btnEconomy:

                // Update holder too
                AirDataHolder.getListHolder().getList().get(0).setTravelClass("E");

                mainActivity.isEconomyClassSelected = true;

                // Set economy or bussiness class selection handling
                btnBusiness.setBackgroundResource(R.color.newGrayPlaner);
                btnEconomy.setBackgroundResource(R.color.bpblack);
                btnBusiness.setTextColor(getResources().getColor(android.R.color.black));
                btnEconomy.setTextColor(getResources().getColor(android.R.color.white));
                break;
            case R.id.btnBusiness:

                // Update holder too
                AirDataHolder.getListHolder().getList().get(0).setTravelClass("B");

                mainActivity.isEconomyClassSelected = false;

                // Set economy or bussiness class selection handling
                btnBusiness.setBackgroundResource(R.color.bpblack);
                btnEconomy.setBackgroundResource(R.color.newGrayPlaner);
                btnBusiness.setTextColor(getResources().getColor(android.R.color.white));
                btnEconomy.setTextColor(getResources().getColor(android.R.color.black));


                break;
            case R.id.btnSearchFlight:
                if (PassengerListAdapter.selectedPassengers != null) {
                    PassengerListAdapter.selectedPassengers.clear();
                    PassengerListAdapter.selectedPassengers = null;
                }

                if (!ProjectUtil.checkInternetConnection(mainActivity)) {

                    new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.INTERNET_DOWN), mainActivity.getResources().getString(R.string.BOOKING_PLANNER_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                    //ProjectUtil.showToast(getResources().getString(R.string.INTERNET_DOWN), mainActivity);

                } else {

                    if (planTxtFromAirCode.getText().toString().trim().equalsIgnoreCase(planTxtToAirCode.getText().toString().trim())) {

                        new AlertDialogUtil(mainActivity, "Departure and arrival airport must be different.", mainActivity.getResources().getString(R.string.BOOKING_PLANNER_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();

                    } else {
                        if (mainActivity.isOneWaySelected) {
                            //region : Save data in Local DB
                            saveSearch("o");
                            //endRegion
                            // Save data in DB for recent searches
                            if (!AppController.autoFillByRecentSearch) saveDataInAlInsertInDb();

                            // Update Holder with user selected data
                            updateDataInHolder();

                            // Save data in LinkedHashMap to create xml request
                            //createXmlReqAndCallService();
                            requestOneWayFlightSearch();
                        } else {
                            departureDate = planTxtDepartOnYear.getText().toString().trim() + "-" + DateUtility.getMonthInInt(mAlPlannerData.get(0).getDepDate()) + "-" + planTxtDepartOnDateMonth.getText().toString().trim().split(" ")[0];
                            travellingDate = planTxtDepartOnDateMonth.getText().toString().trim().split(" ")[0] + "/" + DateUtility.getMonthInInt(mAlPlannerData.get(0).getDepDate()) + "/" + planTxtDepartOnYear.getText().toString().trim();
                            String onwardDate = planTxtDepartOnYear.getText().toString().trim() + "-" + DateUtility.getMonthInInt(mAlPlannerData.get(0).getDepDate()) + "-" + planTxtDepartOnDateMonth.getText().toString().trim().split(" ")[0];
                            String monthString = planTxtReturnOnDateMonth.getText().toString().trim().split(" ")[1];
                            Calendar cal = Calendar.getInstance();
                            try {
                                cal.setTime(new SimpleDateFormat("MMM").parse(monthString));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            int monthInt = cal.get(Calendar.MONTH) + 1;
                            String month;
                            if (monthInt < 10) {
                                month = "0" + String.valueOf(monthInt);
                            } else {
                                month = String.valueOf(monthInt);
                            }

                            String returnDate = planTxtReturnOnYear.getText().toString().trim() + "-" + month + "-" + planTxtReturnOnDateMonth.getText().toString().trim().split(" ")[0];
                            travellReturnDate = returnDate;
                            if (DateUtility.getDateInFor(returnDate).before(DateUtility.getDateInFor(onwardDate))) {
                                new AlertDialogUtil(context, "Departure date cannot exceed return date. Please modify to continue.", context.getResources().getString(R.string.FLIGHT_SEARCH_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                            } else {
                                saveSearch("r");
                                if (!AppController.autoFillByRecentSearch) saveDataInAlInsertInDb();
                                updateDataInHolder();
                                requestRoundTripFlightSearch();
                            }
                        }

                    }
                }
                Log.e("Date", travellingDate + ": " + travellReturnDate);

                break;

            case R.id.lLayoutDepartOn:

                // Comment code
                mainActivity.isDepartureDateSelected = true;
                mainActivity.isClickedDepartDate = true;

                // Set pass count in main air holder
                setPassCountInHolderBeforeLeavingPage();

                // Save data in holder
                if(ActivityMain.isRunning) {
                    mainActivity.lastActiveFragment = Constant.PLANNER_FRAGMENT;
                    Bundle bundle = new Bundle();
                    bundle.putString("org", planTxtFromAirCode.getText().toString().trim());
                    bundle.putString("dest", planTxtToAirCode.getText().toString().trim());
                    CustomCalanderFragment calanderFragment = new CustomCalanderFragment();
                    calanderFragment.setArguments(bundle);
                    ProjectUtil.replaceFragment(mainActivity, calanderFragment, R.id.frame_layout, EnumAnimation.DOWN_TO_TOP);
                }
                // ProjectUtil.replaceFragment(mainActivity, new CalenderFragment(), R.id.frame_layout, EnumAnimation.DOWN_TO_TOP);

                break;

            case R.id.lLayoutReturnOn:

                mainActivity.isDepartureDateSelected = false;
                //mainActivity.isClickedReturnDate = true;

                // Set pass count in main air holder
                setPassCountInHolderBeforeLeavingPage();

                // If return trip is selected allow user to select date else show dialog
                if (mainActivity.isOneWaySelected) {

                    new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.BOOKING_PLANNER_RETURN_DATE_TEXT_MESSAGE), mainActivity.getResources().getString(R.string.BOOKING_PLANNER_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();

                } else {
                    // Save data in holder
                    // Date wil be saved in inside calender fragment
                    if(ActivityMain.isRunning) {
                        mainActivity.lastActiveFragment = Constant.PLANNER_FRAGMENT;
                        Bundle bundle1 = new Bundle();
                        bundle1.putString("dest", planTxtFromAirCode.getText().toString().trim());
                        bundle1.putString("org", planTxtToAirCode.getText().toString().trim());
                        CustomCalanderFragment calanderFragment1 = new CustomCalanderFragment();
                        calanderFragment1.setArguments(bundle1);
                        ProjectUtil.replaceFragment(mainActivity, calanderFragment1, R.id.frame_layout, EnumAnimation.DOWN_TO_TOP);
                    }
                }

                break;
            case R.id.planTxtFromAirCode:

                // Set pass count in main air holder
                setPassCountInHolderBeforeLeavingPage();
                mainActivity.isDepartureStationSelected = true;
                if(ActivityMain.isRunning) {
                    mainActivity.lastActiveFragment = Constant.PLANNER_FRAGMENT;
                    ProjectUtil.replaceFragment(mainActivity, new FragmentSearchAirports(), R.id.frame_layout, EnumAnimation.DOWN_TO_TOP);
                }
                break;
            case R.id.planTxtToAirCode:

                // Set pass count in main air holder
                setPassCountInHolderBeforeLeavingPage();
                if(ActivityMain.isRunning) {
                    mainActivity.isDepartureStationSelected = false;
                    mainActivity.lastActiveFragment = Constant.PLANNER_FRAGMENT;
                    ProjectUtil.replaceFragment(mainActivity, new FragmentSearchAirports(), R.id.frame_layout, EnumAnimation.DOWN_TO_TOP);
                    //Toast.makeText(getActivity(), "TO", Toast.LENGTH_SHORT).show();
                }
                break;
            // Traveller count increase and decrese
            case R.id.planTxtDecreaseAdults:
                passengerDecreaseValidtion(1);
                break;

            case R.id.planTxtIncreaseAdults:
                passengerIncreaseValidtion(1);
                if (errorCode == ERROR_CODE_MAX_LIMIT) {
                    Toast.makeText(getActivity(), R.string.MAX_PASSENGER_LIMIT, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.planTxtDecreaseChild:
                passengerDecreaseValidtion(2);
                break;

            case R.id.planTxtIncreaseChild:
                passengerIncreaseValidtion(2);
                if (errorCode == ERROR_CODE_MAX_LIMIT) {
                    Toast.makeText(getActivity(), R.string.MAX_PASSENGER_LIMIT, Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.planTxtDecreaseInfant:
                passengerDecreaseValidtion(3);
                break;

            case R.id.planTxtIncreaseInfant:
                passengerIncreaseValidtion(3);
                if (errorCode == ERROR_CODE_MAX_LIMIT) {
                    Toast.makeText(getActivity(), R.string.MAX_INFANTS_LIMIT, Toast.LENGTH_SHORT).show();
                }
                break;

            default:
                break;

        }

    }

    private void pickerDate(final TextView txt) {


        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "dd/MM/yyyy"; // In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                // txt.setText(sdf.format(myCalendar.getTime()));
            }
        };

        new DatePickerDialog(getActivity(), date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();

    }


    private void initializeVariable(View view) {

//region : Searching Dialog
        flightClass = (Spinner) view.findViewById(R.id.flightClass);
        selectLtc = (CheckBox) view.findViewById(R.id.selectLtc);

        imgStationCodeArrow = (ImageView) view.findViewById(R.id.PLN_IMG_STATION_CHAMGE_ARROW);
        lLayoutArrow = (RelativeLayout) view.findViewById(R.id.PLN_IMG_ARROW_BUTTON_LAY);


        // Recent searched
        imgRecentSearch = (ImageView) mainActivity.findViewById(R.id.RECENT_SEARCH);
        imgRecentSearch.setVisibility(View.VISIBLE);
        imgRecentSearch.setImageResource(R.drawable.recent_search);
        imgRecentSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Set pass count in main air holder
                setPassCountInHolderBeforeLeavingPage();
                if(ActivityMain.isRunning) {
                    mainActivity.lastActiveFragment = Constant.PLANNER_FRAGMENT;
                    ProjectUtil.replaceFragment(mainActivity, new FragmentRecentSearch(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                }
            }
        });


        //added by asif
        imgDullCal = (ImageView) view.findViewById(R.id.imgDullCal);

        btnRoundTrip = (Button) view.findViewById(R.id.btnRoundTrip);
        btnOneWay = (Button) view.findViewById(R.id.btnOneWay);
        btnEconomy = (Button) view.findViewById(R.id.btnEconomy);
        btnBusiness = (Button) view.findViewById(R.id.btnBusiness);
        btnSearchFlight = (Button) view.findViewById(R.id.btnSearchFlight);

        lLayoutDepartOnDate = (LinearLayout) view.findViewById(R.id.lLayoutDepartOn);
        lLayoutReturnOnDate = (LinearLayout) view.findViewById(R.id.lLayoutReturnOn);
        lLayoutReturnOnDateTriangle = (LinearLayout) view.findViewById(R.id.LAY_RETURN_TRINGLE);


        txtStaticHeader = (TextView) view.findViewById(R.id.TXT_RETURN_DATE_STATIC);


        lLayoutDepartOnDate.setOnClickListener(this);
        lLayoutReturnOnDate.setOnClickListener(this);

        planTxtFromAirCode = (TextView) view.findViewById(R.id.planTxtFromAirCode);

        planTxtToAirCode = (TextView) view.findViewById(R.id.planTxtToAirCode);

        planTxtFromCityName = (TextView) view.findViewById(R.id.planTxtFromCityName);
        planTxtToCityName = (TextView) view.findViewById(R.id.planTxtToCityName);

        planTxtDepartOnDateMonth = (TextView) view.findViewById(R.id.planTxtDepartOnDateMonth);
        planTxtDepartOnDay = (TextView) view.findViewById(R.id.planTxtDepartOnDay);
        planTxtDepartOnYear = (TextView) view.findViewById(R.id.planTxtDepartOnYear);

        planTxtReturnOnDateMonth = (TextView) view.findViewById(R.id.planTxtReturnOnDateMonth);
        ;
        planTxtReturnOnDateMonth.setText(DateUtility.getDateAndMonthFromCal(mAlPlannerData.get(0).getReturnDate()));
        planTxtReturnOnDay = (TextView) view.findViewById(R.id.planTxtReturnOnDay);


        planTxtReturnOnYear = (TextView) view.findViewById(R.id.planTxtReturnOnYear);

        planTxtDecreaseAdults = (TextView) view.findViewById(R.id.planTxtDecreaseAdults);
        planTxtCountAdults = (TextView) view.findViewById(R.id.planTxtCountAdults);
        planTxtIncreaseAdults = (TextView) view.findViewById(R.id.planTxtIncreaseAdults);

        planTxtDecreaseChild = (TextView) view.findViewById(R.id.planTxtDecreaseChild);
        planTxtCountChild = (TextView) view.findViewById(R.id.planTxtCountChild);
        planTxtIncreaseChild = (TextView) view.findViewById(R.id.planTxtIncreaseChild);

        planTxtDecreaseInfant = (TextView) view.findViewById(R.id.planTxtDecreaseInfant);
        planTxtCountInfant = (TextView) view.findViewById(R.id.planTxtCountInfant);
        planTxtIncreaseInfant = (TextView) view.findViewById(R.id.planTxtIncreaseInfant);


        lLayoutArrow.setOnClickListener(this);

        planTxtDecreaseAdults.setOnClickListener(this);
        planTxtIncreaseAdults.setOnClickListener(this);
        planTxtDecreaseChild.setOnClickListener(this);
        planTxtIncreaseChild.setOnClickListener(this);
        planTxtDecreaseInfant.setOnClickListener(this);
        planTxtIncreaseInfant.setOnClickListener(this);

        planTxtFromAirCode.setOnClickListener(this);
        planTxtToAirCode.setOnClickListener(this);
        btnRoundTrip.setOnClickListener(this);
        btnOneWay.setOnClickListener(this);
        btnEconomy.setOnClickListener(this);
        btnBusiness.setOnClickListener(this);
        btnSearchFlight.setOnClickListener(this);

        convertPassengerIntoInt();

        // If one Way is selected show only departure date selection else hide it
        // Set economy or business class selection handling
        setOneWayAndRoundTripHandlingChanges();

        // Set economy or bussiness class selection handling
        setEcomonyAndBusinessLayoutChanges();
    }

    private void setdataInVarFromHolder() {
        mAlPlannerData = AirDataHolder.getListHolder().getList();

        planTxtFromAirCode.setText(mAlPlannerData.get(0).getFromStation());
        planTxtToAirCode.setText(mAlPlannerData.get(0).getToStation());

        if (mAlPlannerData.get(0).getFromStationCity().contains("-")) {
            planTxtFromCityName.setText(mAlPlannerData.get(0).getFromStationCity().split("-")[0]);
        } else {
            planTxtFromCityName.setText(mAlPlannerData.get(0).getFromStationCity());
        }

        if (mAlPlannerData.get(0).getToStationCity().contains("-")) {
            planTxtToCityName.setText(mAlPlannerData.get(0).getToStationCity().split("-")[0]);
        } else {
            planTxtToCityName.setText(mAlPlannerData.get(0).getToStationCity());
        }

        adultInt = mAlPlannerData.get(0).getAdultPassNum();
        childInt = mAlPlannerData.get(0).getChildPassNum();
        infantInt = mAlPlannerData.get(0).getInfantPassNum();
        planTxtCountAdults.setText("" + adultInt);//mAlPlannerData.get(0).getAdultPassNum());
        planTxtCountChild.setText("" + childInt);//mAlPlannerData.get(0).getChildPassNum());
        planTxtCountInfant.setText("" + infantInt);//mAlPlannerData.get(0).getInfantPassNum());

        planTxtDepartOnDateMonth.setText(DateUtility.getDateAndMonthFromCal(mAlPlannerData.get(0).getDepDate()));
        planTxtDepartOnDay.setText(DateUtility.getDateNameFromCal(mAlPlannerData.get(0).getDepDate()));
        planTxtDepartOnYear.setText(DateUtility.getYearFromCal(mAlPlannerData.get(0).getDepDate()));


        if (mainActivity.isOneWaySelected) {
            planTxtReturnOnDateMonth.setText(DateUtility.getDateAndMonthFromCal(mAlPlannerData.get(0).getReturnDate()));
            planTxtReturnOnDay.setText(DateUtility.getDateNameFromCal(mAlPlannerData.get(0).getDepDate()));
            planTxtReturnOnYear.setText(DateUtility.getYearFromCal(mAlPlannerData.get(0).getDepDate()));
        } else {
            planTxtReturnOnDateMonth.setText(DateUtility.getDateAndMonthFromCal(mAlPlannerData.get(0).getReturnDate()));
            planTxtReturnOnDay.setText(DateUtility.getDateNameFromCal(mAlPlannerData.get(0).getReturnDate()));
            planTxtReturnOnYear.setText(DateUtility.getYearFromCal(mAlPlannerData.get(0).getReturnDate()));
        }


    }

    private void convertPassengerIntoInt() {

        adultInt = Integer.parseInt(planTxtCountAdults.getText().toString());
        childInt = Integer.parseInt(planTxtCountChild.getText().toString());
        infantInt = Integer.parseInt(planTxtCountInfant.getText().toString());
    }

    private int passengerIncreaseValidtion(int passengerType) {

        switch (passengerType) {
            case 1:
                if (adultInt < 9 && (adultInt < 9 - childInt)) {
                    adultInt++;
                    errorCode = 0;
                } else {
                    errorCode = ERROR_CODE_MAX_LIMIT;
                }
                planTxtCountAdults.setText("" + adultInt);
                break;
            case 2:

                if (childInt < 9 - adultInt) {
                    childInt++;
                    errorCode = 0;
                } else {
                    errorCode = ERROR_CODE_MAX_LIMIT;
                }
                planTxtCountChild.setText("" + childInt);
                break;
            case 3:
                if (infantInt < adultInt) {
                    infantInt++;
                    errorCode = 0;
                } else {
                    errorCode = ERROR_CODE_MAX_LIMIT;
                }
                planTxtCountInfant.setText("" + infantInt);
                break;

        }


        return errorCode;
    }

    private int passengerDecreaseValidtion(int passengerType) {

        switch (passengerType) {
            case 1:
                if (adultInt > 1) {
                    adultInt--;

                    if (infantInt > adultInt) {
                        infantInt = adultInt;
                        planTxtCountInfant.setText("" + infantInt);
                    }
                } else {

                }
                planTxtCountAdults.setText("" + adultInt);
                break;
            case 2:
                if (childInt > 0) {
                    childInt--;
                } else {

                }
                planTxtCountChild.setText("" + childInt);
                break;
            case 3:
                if (infantInt > 0) {
                    infantInt--;
                } else {

                }
                planTxtCountInfant.setText("" + infantInt);
                break;

        }

        return errorCode;
    }


    public void changeStnWithAnim() {

        final Animation animLeftOut = AnimationUtils.loadAnimation(mainActivity, R.anim.right_in);
        final Animation animLeftIn = AnimationUtils.loadAnimation(mainActivity, R.anim.left_in);
        final RotateAnimation rotateAnim = new RotateAnimation(0.0f, 360, RotateAnimation.RELATIVE_TO_SELF, 0.5f, RotateAnimation.RELATIVE_TO_SELF, 0.5f);

        rotateAnim.setDuration(500);
        rotateAnim.setFillAfter(true);

        String frmStnName = planTxtFromCityName.getText().toString();
        String frmStnCode = planTxtFromAirCode.getText().toString();

        String toStnName = planTxtToCityName.getText().toString();
        String toStnCode = planTxtToAirCode.getText().toString();

        planTxtFromCityName.setText(toStnName);
        planTxtFromAirCode.setText(toStnCode);
        planTxtToCityName.setText(frmStnName);
        planTxtToAirCode.setText(frmStnCode);

        imgStationCodeArrow.startAnimation(rotateAnim);

        planTxtFromCityName.startAnimation(animLeftIn);
        planTxtFromAirCode.startAnimation(animLeftIn);
        planTxtToCityName.startAnimation(animLeftOut);
        planTxtToAirCode.startAnimation(animLeftOut);

        // Set/change data in holder too
        ArrayList<AirDataModel> alAirsdData = AirDataHolder.getListHolder().getList();
        alAirsdData.get(0).setFromStation(toStnCode);
        alAirsdData.get(0).setFromStationCity(toStnName);

        alAirsdData.get(0).setToStation(frmStnCode);
        alAirsdData.get(0).setToStationCity(frmStnName);

    }


    private void saveDataInAlInsertInDb() {

        // Create bean to save data in db for recent searches
        RecentFlightSearchBean alObjRecFlightSearchBean = new RecentFlightSearchBean();
        alObjRecFlightSearchBean.setRecFromStationCode(planTxtFromAirCode.getText().toString().trim());
        alObjRecFlightSearchBean.setRecFromStationName(planTxtFromCityName.getText().toString().trim());
        alObjRecFlightSearchBean.setRecToStationCode(planTxtToAirCode.getText().toString().trim());
        alObjRecFlightSearchBean.setRecToStationName(planTxtToCityName.getText().toString().trim());

        if ((!planTxtFromCityName.getText().toString().trim().contains("IN")) || (!planTxtToCityName.getText().toString().trim().contains("IN"))) {
            alObjRecFlightSearchBean.setRecTripTypeDomOrInter("International");
        } else {
            alObjRecFlightSearchBean.setRecTripTypeDomOrInter("Domestic");
        }
        if (mainActivity.isEconomyClassSelected) {
            alObjRecFlightSearchBean.setRecTripClassEorB("E");
        } else {
            alObjRecFlightSearchBean.setRecTripClassEorB("B");
        }
        if (mainActivity.isOneWaySelected) {
            alObjRecFlightSearchBean.setRecOneWayOrRoundTrip("One way");
        } else {
            alObjRecFlightSearchBean.setRecOneWayOrRoundTrip("Round trip");
        }

        alObjRecFlightSearchBean.setRecPassAdoultCount(planTxtCountAdults.getText().toString().trim());
        alObjRecFlightSearchBean.setRecPassChildCount(planTxtCountChild.getText().toString().trim());
        alObjRecFlightSearchBean.setRecPassInfantCount(planTxtCountInfant.getText().toString().trim());
        Log.e("Depart date", mAlPlannerData.get(0).getDepDate());
        alObjRecFlightSearchBean.setRecDepDate(mAlPlannerData.get(0).getDepDate());
        alObjRecFlightSearchBean.setRecReturnDate(mAlPlannerData.get(0).getReturnDate());

        // Save in db
 /*       database.open();
        boolean lBoolInserted = database.insertRecentSearches(alObjRecFlightSearchBean);
        database.close();
*/
        // Toast.makeText(mainActivity, " RECENT search DATA inserted :" + lBoolInserted, Toast.LENGTH_SHORT).show();
    }


    // Set/update user selected data in holder for future use
    private void updateDataInHolder() {
        mAlPlannerData = AirDataHolder.getListHolder().getList();

        if (recentSearch !=null){
            String depDate = recentSearch.getDepertureDayName()+" "+recentSearch.getDepertureMonthName()+" "+recentSearch.getDepertureDay()+" 00:00:00 GMT+05:30 "+recentSearch.getDepertureYear();
            String retDate = recentSearch.getDepertureDayName()+" "+recentSearch.getDepertureMonthName()+" "+recentSearch.getDepertureDay()+" 00:00:00 GMT+05:30 "+recentSearch.getDepertureYear();

//            EEE MMM d HH:mm:ss 'IST' yyyy
            mAlPlannerData.get(0).setDepDate(depDate);
            mAlPlannerData.get(0).setRetDate(retDate);
        }



        mAlPlannerData.get(0).setFromStation(planTxtFromAirCode.getText().toString().trim());
        mAlPlannerData.get(0).setFromStationCity(planTxtFromCityName.getText().toString().trim());
        mAlPlannerData.get(0).setToStation(planTxtToAirCode.getText().toString().trim());
        mAlPlannerData.get(0).setToStationCity(planTxtToCityName.getText().toString().trim());
        //One way or round trip
        if (mainActivity.isOneWaySelected) {
            mAlPlannerData.get(0).setTripType("One way");
        } else {
            mAlPlannerData.get(0).setTripType("Round trip");
        }

        // DOM or INTER
        if (ProjectUtil.isInternationalOrDomestic(planTxtFromCityName.getText().toString().trim(), planTxtToCityName.getText().toString().trim())) {
            mAlPlannerData.get(0).setTripDomOrInter("Domestic");
        } else {
            mAlPlannerData.get(0).setTripDomOrInter("International");
        }

        // E or B
        if (mainActivity.isEconomyClassSelected) {
            mAlPlannerData.get(0).setTravelClass("E");
        } else {
            mAlPlannerData.get(0).setTravelClass("B");
        }

        mAlPlannerData.get(0).setAdultPassNum(Integer.parseInt(planTxtCountAdults.getText().toString().trim()));
        mAlPlannerData.get(0).setChildPassNum(Integer.parseInt(planTxtCountChild.getText().toString().trim()));
        mAlPlannerData.get(0).setInfantPassNum(Integer.parseInt(planTxtCountInfant.getText().toString().trim()));

        // DATE and RETURN DATE ARE ALREADY SET IN CALENDER VIEW


    }


    // Set text color light, seems unselectable in oneway trip else viseversa
    public void setReturnDateLAyoutTextColorLight() {
        if (mainActivity.isOneWaySelected) {
            txtStaticHeader.setTextColor(mainActivity.getResources().getColor(R.color.DISABLE_GREY_VERY_LIGHT_COLOR));
            planTxtReturnOnDateMonth.setTextColor(mainActivity.getResources().getColor(R.color.DISABLE_GREY_VERY_LIGHT_COLOR));
            planTxtReturnOnDay.setTextColor(mainActivity.getResources().getColor(R.color.DISABLE_GREY_VERY_LIGHT_COLOR));
            planTxtReturnOnYear.setTextColor(mainActivity.getResources().getColor(R.color.DISABLE_GREY_VERY_LIGHT_COLOR));
        } else {
            txtStaticHeader.setTextColor(mainActivity.getResources().getColor(R.color.colorGrayBlack));
            planTxtReturnOnDateMonth.setTextColor(mainActivity.getResources().getColor(R.color.colorGrayBlack));
            planTxtReturnOnDay.setTextColor(mainActivity.getResources().getColor(R.color.colorGrayBlack));
            planTxtReturnOnYear.setTextColor(mainActivity.getResources().getColor(R.color.colorGrayBlack));
        }
    }


    public void setPassCountInHolderBeforeLeavingPage() {
        mAlPlannerData.get(0).setAdultPassNum(Integer.parseInt(planTxtCountAdults.getText().toString().trim()));
        mAlPlannerData.get(0).setChildPassNum(Integer.parseInt(planTxtCountChild.getText().toString().trim()));
        mAlPlannerData.get(0).setInfantPassNum(Integer.parseInt(planTxtCountInfant.getText().toString().trim()));
    }


    public void setEcomonyAndBusinessLayoutChanges() {
        /// If economy is selected : Eco and business class handling
        if (mainActivity.isEconomyClassSelected) {
            btnBusiness.setBackgroundResource(R.color.newGrayPlaner);
            btnEconomy.setBackgroundResource(R.color.bpblack);

        } else {
            btnBusiness.setBackgroundResource(R.color.bpblack);
            btnEconomy.setBackgroundResource(R.color.newGrayPlaner);
        }
    }

    public void setOneWayAndRoundTripHandlingChanges() {

        if (mainActivity.isOneWaySelected) {
            lLayoutReturnOnDate.setBackgroundColor(mainActivity.getResources().getColor(R.color.RETURN_DATE_DISABLE));
            lLayoutReturnOnDateTriangle.setBackgroundColor(mainActivity.getResources().getColor(R.color.RETURN_DATE_DISABLE));

            setReturnDateLAyoutTextColorLight();

            // One way round trip selected
            btnRoundTrip.setBackgroundResource(R.color.colorDARKGrey);
            btnOneWay.setBackgroundResource(R.color.colorLightBlue);
        } else {
            lLayoutReturnOnDate.setBackgroundColor(mainActivity.getResources().getColor(R.color.newGrayPlaner));
            lLayoutReturnOnDateTriangle.setBackgroundColor(mainActivity.getResources().getColor(R.color.newGrayPlaner));
            setReturnDateLAyoutTextColorLight();
            // Round trip handling
            btnOneWay.setBackgroundResource(R.color.colorDARKGrey);
            btnRoundTrip.setBackgroundResource(R.color.colorLightBlue);
        }

    }


    @Override
    public void onResume() {
        super.onResume();
        // Tracking the screen view
        AppController.getInstance().trackScreenView("Planner Screen");

        ActivityMain.activeFragment = Constant.PLANNER_FRAGMENT;
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.containsKey("previousData")) {
                //ActivityMain.backStackEnable = true;
                //ltcEnable();
                mainActivity.setdataInAirDataHolder();
                //refreshData();
                setdataInVarFromHolder();
            }
        }

    }

    private void requestOneWayFlightSearch() {
        NetworkingUtils.showProgress(getActivity());
        final String tripType, returnDate, origin, destination, destinationCity, originCity, classOfTravel;
        if (mainActivity.isOneWaySelected) {
            tripType = "O";
        } else {
            tripType = "R";
        }
        Log.e("normaDepDate", mAlPlannerData.get(0).getDepDate());
        departureDate = planTxtDepartOnYear.getText().toString().trim() + "-" + DateUtility.getMonthInInt(mAlPlannerData.get(0).getDepDate()) + "-" + planTxtDepartOnDateMonth.getText().toString().trim().split(" ")[0];
        travellingDate = planTxtDepartOnDateMonth.getText().toString().trim().split(" ")[0] + "/" + DateUtility.getMonthInInt(mAlPlannerData.get(0).getDepDate()) + "/" + planTxtDepartOnYear.getText().toString().trim();
        Log.e("normaChangesDepDate", departureDate);
        returnDate = "";
        noOfAdults = planTxtCountAdults.getText().toString().trim();
        noOfChildren = planTxtCountChild.getText().toString().trim();
        noOfInfants = planTxtCountInfant.getText().toString().trim();
        origin = planTxtFromAirCode.getText().toString();
        destination = planTxtToAirCode.getText().toString();
        originCity = planTxtFromCityName.getText().toString();
        destinationCity = planTxtToCityName.getText().toString();

        orig = origin;
        dest = destination;
        if (flightClass.getSelectedItemPosition() == 0) {
            classOfTravel = "Economy";
        } else if (flightClass.getSelectedItemPosition() == 1) {
            classOfTravel = "Business";
        } else {
            classOfTravel = "PremiumEconomy";
        }
        flightTravellClass = classOfTravel;
        flightTravellClass = flightTravellClass.replace("PremiumEconomy", "Premium Economy");
        String preferedAirline = "";
        if (selectLtc.isChecked()) {
            preferedAirline = "AI";
            isLTC = true;
        } else if (classOfTravel.equals("PremiumEconomy")) {
            preferedAirline = "UK";
            isLTC = false;
        } else {
            preferedAirline = "";
            isLTC = false;
        }
        Networking.searchFlights(tripType, departureDate, returnDate, noOfAdults, noOfChildren, noOfInfants, origin, destination, destinationCity, originCity, classOfTravel, preferedAirline,isLTC, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                PojoOneWay pojoOneWay = gson.fromJson(response.toString(), PojoOneWay.class);
                Log.e("Response", response.toString());
                if (pojoOneWay.getStatus().equals("SUCCESS")) {
                    data = pojoOneWay.getData();
                    searchOneWayFlightsList = null;
                    if (data.getFlights() != null) {
                        searchOneWayFlightsList = new ArrayList<>(Arrays.asList(data.getFlights()));
                    } else {
                        searchOneWayFlightsList = new ArrayList<>();
                    }

                    if (searchOneWayFlightsList.size() > 0) {
                        if(ActivityMain.isRunning) {
                            mainActivity.lastActiveFragment = Constant.PLANNER_FRAGMENT;
                            ProjectUtil.replaceFragment(context, new FragmentOneWayFlight(), R.id.frame_layout, EnumAnimation.DOWN_TO_TOP);
                        }
                    } else {
                        NetworkingUtils.noFlightsAvailable(context);
                    }
                    NetworkingUtils.dismissProgress();
                } else {
                    Toast.makeText(context, pojoOneWay.getMessage(), Toast.LENGTH_SHORT).show();
                    requestOneWayFlightSearch();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                NetworkingUtils.dismissProgress();
                Toast.makeText(context, "Some Error Occurred", Toast.LENGTH_SHORT).show();

                /*Log.e("Volley", error.toString());
                if (error.networkResponse != null) {
                    if (error.networkResponse.statusCode == 500) {
                        NetworkingUtils.dismissProgress();
                        Toast.makeText(context, "Internal Server Error.", Toast.LENGTH_SHORT).show();
                    } else {
                        NetworkingUtils.dismissProgress();
                        NetworkingUtils.noInternetAccess(context);
                    }
                } else {
                    NetworkingUtils.dismissProgress();
                    Toast.makeText(context, "Internal Server Error.", Toast.LENGTH_SHORT).show();
                }*/
            }
        });
    }

    private void requestRoundTripFlightSearch() {
        NetworkingUtils.showProgress(getActivity());
        final String tripType, returnDate, origin, destination, destinationCity, originCity, classOfTravel;
        if (mainActivity.isOneWaySelected) {
            tripType = "O";
        } else {
            tripType = "R";
        }
        String monthString = planTxtReturnOnDateMonth.getText().toString().trim().split(" ")[1];
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(new SimpleDateFormat("MMM").parse(monthString));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int monthInt = cal.get(Calendar.MONTH) + 1;
        String month;
        if (monthInt < 10) {
            month = "0" + String.valueOf(monthInt);
        } else {
            month = String.valueOf(monthInt);
        }
        returnDate = planTxtReturnOnYear.getText().toString().trim() + "-" + month + "-" + planTxtReturnOnDateMonth.getText().toString().trim().split(" ")[0];
        noOfAdults = planTxtCountAdults.getText().toString().trim();
        noOfChildren = planTxtCountChild.getText().toString().trim();
        noOfInfants = planTxtCountInfant.getText().toString().trim();
        origin = planTxtFromAirCode.getText().toString();
        destination = planTxtToAirCode.getText().toString();
        originCity = planTxtFromCityName.getText().toString();
        destinationCity = planTxtToCityName.getText().toString();
        orig = origin;
        dest = destination;
        if (flightClass.getSelectedItemPosition() == 0) {
            classOfTravel = "Economy";
        } else if (flightClass.getSelectedItemPosition() == 1) {
            classOfTravel = "Business";
        } else {
            classOfTravel = "PremiumEconomy";
        }
        flightTravellClass = classOfTravel;
        flightTravellClass = flightTravellClass.replace("PremiumEconomy", "Premium Economy");
        String preferedAirline = "";
        if (selectLtc.isChecked()) {
            preferedAirline = "AI";
            isLTC = true;
        } else if (classOfTravel.equals("PremiumEconomy")) {
            preferedAirline = "UK";
            isLTC = false;
        } else {
            preferedAirline = "";
            isLTC = false;
        }

        Networking.searchFlights(tripType, departureDate, returnDate, noOfAdults, noOfChildren, noOfInfants, origin, destination, destinationCity, originCity, classOfTravel, preferedAirline,isLTC, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("Response", response.toString());
                Gson gson = new Gson();
                PojoRoundTrip pojoRoundTrip = gson.fromJson(response.toString(), PojoRoundTrip.class);

                if (pojoRoundTrip.getStatus().equals("SUCCESS")) {
                    Data data = pojoRoundTrip.getData();
                    searchResultIntData = data;
                    if (data.getIsInternational().equals("true")) {
                        IntlFlights[][] intlFlightsList = data.getIntlFlights();
                        intlFlightsArrayList = new ArrayList<>();
                        if (intlFlightsList != null) {
                            if (intlFlightsList.length > 0) {
                                for (int i = 0; i < intlFlightsList.length; i++) {
                                    intlFlightsArrayList.add(intlFlightsList[i]);
                                }
                                if(ActivityMain.isRunning){
                                    mainActivity.lastActiveFragment = Constant.PLANNER_FRAGMENT;
                                    transaction = getActivity().getSupportFragmentManager().beginTransaction();
                                    transaction.replace(R.id.frame_layout, new SearchResultsComboList());
                                    transaction.addToBackStack(null);
                                    transaction.commit();
                                }
                            } else {
                                Toast.makeText(context, "No Flights Available", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(context, "No Flights Available", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        PojoSearchResult pojoSearchResult = gson.fromJson(response.toString(), PojoSearchResult.class);
                        searchResultData = pojoSearchResult.getData();
                        if (searchResultData.getFlights() != null) {
                            if (searchResultData.getFlights().length > 0) {
                                ArrayList<com.irctc.air.round.trip.domastic.model.Flights> onwardFlightsList = new ArrayList<com.irctc.air.round.trip.domastic.model.Flights>();
                                ArrayList<com.irctc.air.round.trip.domastic.model.Flights> returnFlightsList = new ArrayList<com.irctc.air.round.trip.domastic.model.Flights>();
                                for (int i = 0; i < FragmentPlanner.searchResultData.getFlights().length; i++) {
                                    if (FragmentPlanner.searchResultData.getFlights()[i].getSegmentType().equalsIgnoreCase("O")) {
                                        onwardFlightsList.add(FragmentPlanner.searchResultData.getFlights()[i]);
                                    } else {
                                        returnFlightsList.add(FragmentPlanner.searchResultData.getFlights()[i]);
                                    }
                                }

                                if (onwardFlightsList.size() > 0 && onwardFlightsList.size() > 0) {
                                    if(ActivityMain.isRunning) {
                                        mainActivity.lastActiveFragment = Constant.PLANNER_FRAGMENT;
                                        ProjectUtil.replaceFragment(context, new DSearchResults(), R.id.frame_layout, EnumAnimation.DOWN_TO_TOP);
                                    }
                                } else {
                                    Toast.makeText(context, "No Flights Available", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(context, "No Flights Available", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(context, "No Flights Available", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(context, pojoRoundTrip.getMessage(), Toast.LENGTH_SHORT).show();
                    requestRoundTripFlightSearch();
                }
                NetworkingUtils.dismissProgress();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                NetworkingUtils.dismissProgress();
                Toast.makeText(context, "Some Error Occurred", Toast.LENGTH_SHORT).show();

                /*Log.e("Volley", error.toString());
                if (error.networkResponse != null) {
                    if (error.networkResponse.statusCode == 500) {
                        NetworkingUtils.dismissProgress();
                        Toast.makeText(context, "Internal Server Error.", Toast.LENGTH_SHORT).show();
                    } else {
                        NetworkingUtils.dismissProgress();
                        NetworkingUtils.noInternetAccess(context);
                    }
                } else {
                    NetworkingUtils.dismissProgress();
                    Toast.makeText(context, "Internal Server Error.", Toast.LENGTH_SHORT).show();
                }*/
            }
        });


    }


    private void saveSearch(String tripType) {
        ModelRecentSearch recentSearch = new ModelRecentSearch();
        recentSearch.setStationCodeFrom(planTxtFromAirCode.getText().toString().trim());
        recentSearch.setStationNameFrom(planTxtFromCityName.getText().toString().trim());
        recentSearch.setStationCodeTo(planTxtToAirCode.getText().toString().trim());
        recentSearch.setStationNameTo(planTxtToCityName.getText().toString().trim());
        recentSearch.setTripType(tripType);
        recentSearch.setTripClass(String.valueOf(flightClass.getSelectedItemPosition()));
        recentSearch.setPassAdultCount(planTxtCountAdults.getText().toString().trim());
        recentSearch.setPassChildCount(planTxtCountChild.getText().toString().trim());
        recentSearch.setPassInfantCount(planTxtCountInfant.getText().toString().trim());
        recentSearch.setDepertureDay(planTxtDepartOnDateMonth.getText().toString().trim().split(" ")[0]);
        recentSearch.setDepertureMonth(String.valueOf(DateUtility.monthInInteger(planTxtDepartOnDateMonth.getText().toString().trim().split(" ")[1])));
        recentSearch.setDepertureYear(planTxtDepartOnYear.getText().toString().trim());
        recentSearch.setDepertureDayName(planTxtDepartOnDay.getText().toString().trim());
        recentSearch.setDepertureMonthName(planTxtDepartOnDateMonth.getText().toString().trim().split(" ")[1]);
        recentSearch.setReturnDay(planTxtReturnOnDateMonth.getText().toString().trim().split(" ")[0]);
        recentSearch.setReturnMonth(String.valueOf(DateUtility.monthInInteger(planTxtReturnOnDateMonth.getText().toString().trim().split(" ")[1])));
        recentSearch.setReturnYear(planTxtReturnOnYear.getText().toString().trim());
        recentSearch.setReturnDayName(planTxtReturnOnDay.getText().toString().trim());
        recentSearch.setReturnMonthName(planTxtReturnOnDateMonth.getText().toString().trim().split(" ")[1]);
        if (selectLtc.isChecked()) {
            recentSearch.setIsLTC("1");
        } else {
            recentSearch.setIsLTC("0");
        }
        new AirDatabase(context).saveSearch(recentSearch);
    }

    private void autoFillByRecentSearches() {
        if (AppController.autoFillByRecentSearch) {
//            if (AppController.autoFillByRecentSearchValueIndex != -1){
                recentSearch = new AirDatabase(context).getRecentSearchList().get(AppController.autoFillByRecentSearchValueIndex);
                handelOneWayRoundTrip(recentSearch.getTripType());
                planTxtFromAirCode.setText(recentSearch.getStationCodeFrom());
                planTxtFromCityName.setText(recentSearch.getStationNameFrom());
                planTxtToAirCode.setText(recentSearch.getStationCodeTo());
                planTxtToCityName.setText(recentSearch.getStationNameTo());
                flightClass.setSelection(Integer.parseInt(recentSearch.getTripClass()));
                planTxtCountAdults.setText(recentSearch.getPassAdultCount());
                planTxtCountChild.setText(recentSearch.getPassChildCount());
                planTxtCountInfant.setText(recentSearch.getPassInfantCount());
            flightClass.setSelection(Integer.parseInt(recentSearch.getTripClass()));

                Log.e("date Recent", recentSearch.getDepertureDay() + " " + recentSearch.getDepertureMonthName() + " " + recentSearch.getDepertureYear() + " " + recentSearch.getDepertureDayName());
                planTxtDepartOnDateMonth.setText(recentSearch.getDepertureDay() + " " + recentSearch.getDepertureMonthName());
                planTxtDepartOnYear.setText(recentSearch.getDepertureYear());
                planTxtDepartOnDay.setText(recentSearch.getDepertureDayName());
                planTxtReturnOnDateMonth.setText(recentSearch.getReturnDay() + " " + recentSearch.getReturnMonthName());
                planTxtReturnOnYear.setText(recentSearch.getReturnYear());
                planTxtReturnOnDay.setText(recentSearch.getReturnDayName());
                if (recentSearch.getIsLTC().equals("1")) {
                    selectLtc.setChecked(true);
                    ActivityMain.isComingFromSideLTC = true;
                    AirHeader.showHeaderText(mainActivity, true, "LTC Flight Search");
                } else {
                    selectLtc.setChecked(false);
                    ActivityMain.isComingFromSideLTC = false;
                    AirHeader.showHeaderText(mainActivity, true, "Flight Search");
                }

            String depDate = recentSearch.getDepertureDayName()+" "+recentSearch.getDepertureMonthName()+" "+recentSearch.getDepertureDay()+" 00:00:00 GMT+05:30 "+recentSearch.getDepertureYear();
             String retDate = recentSearch.getDepertureDayName()+" "+recentSearch.getDepertureMonthName()+" "+recentSearch.getDepertureDay()+" 00:00:00 GMT+05:30 "+recentSearch.getDepertureYear();

//            EEE MMM d HH:mm:ss 'IST' yyyy
            mAlPlannerData.get(0).setDepDate(depDate);
            mAlPlannerData.get(0).setRetDate(retDate);
                AppController.autoFillByRecentSearch = false;
                AppController.autoFillByRecentSearchValueIndex = 0;
//            }
        }
    }

    private void handelOneWayRoundTrip(String tripType) {
        if (tripType.equalsIgnoreCase("o")) {
            imgDullCal.setImageResource(R.drawable.cal_dull_icon);
            AirDataHolder.getListHolder().getList().get(0).setTripType("One way");
            btnRoundTrip.setBackgroundResource(R.color.colorDARKGrey);
            btnOneWay.setBackgroundResource(R.color.colorLightBlue);
            mainActivity.isOneWaySelected = true;
            lLayoutReturnOnDate.setBackgroundColor(mainActivity.getResources().getColor(R.color.RETURN_DATE_DISABLE));
            lLayoutReturnOnDateTriangle.setBackgroundColor(mainActivity.getResources().getColor(R.color.RETURN_DATE_DISABLE));
            setReturnDateLAyoutTextColorLight();
        } else {
            imgDullCal.setImageResource(R.drawable.cal_icon);
            AirDataHolder.getListHolder().getList().get(0).setTripType("Round trip");
            btnOneWay.setBackgroundResource(R.color.colorDARKGrey);
            btnRoundTrip.setBackgroundResource(R.color.colorLightBlue);
            mainActivity.isOneWaySelected = false;
            lLayoutReturnOnDate.setBackgroundColor(mainActivity.getResources().getColor(R.color.newGrayPlaner));
            lLayoutReturnOnDateTriangle.setBackgroundColor(mainActivity.getResources().getColor(R.color.newGrayPlaner));
            setReturnDateLAyoutTextColorLight();
            //  AirDataHolder.getListHolder().getList().get(0).setReturnDate(DateUtility.getDateInFormat(AirDataHolder.getListHolder().getList().get(0).getDepDate()).toString());
            planTxtReturnOnDateMonth.setText(DateUtility.getDateAndMonthFromCal(mAlPlannerData.get(0).getReturnDate()));
            planTxtReturnOnDay.setText(DateUtility.getDateNameFromCal(mAlPlannerData.get(0).getReturnDate()));
            planTxtReturnOnYear.setText(DateUtility.getYearFromCal(mAlPlannerData.get(0).getReturnDate()));
        }
    }

    private void ltcEnable() {
        if (planTxtFromCityName.getText().toString().trim().contains("IN") && planTxtToCityName.getText().toString().trim().contains("IN")) {
            if(ActivityMain.isComingFromSideLTC){
                selectLtc.setChecked(true);
            }
            selectLtc.setEnabled(true);
            selectLtc.setAlpha(1.0f);
        } else {
            ActivityMain.isComingFromSideLTC=false;
            AirHeader.showHeaderText(mainActivity, true, "Flight Search");
            selectLtc.setChecked(false);
            selectLtc.setEnabled(false);
            selectLtc.setAlpha(0.5f);
        }
    }

}


