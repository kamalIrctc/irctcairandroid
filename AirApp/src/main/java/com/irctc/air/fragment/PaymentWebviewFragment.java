package com.irctc.air.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.header.AirHeader;

import com.irctc.air.util.AES;
import com.irctc.air.util.Constant;


/**
 * Created by vivek on 04/13/2016
 */
public class PaymentWebviewFragment extends Fragment {


    private ActivityMain mainActivity;
    private TextView tvVersion;
    private String lStrTransactionId;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mainActivity = (ActivityMain) activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.payment, null);



        if (getArguments().containsKey("TransactionID")) {
            lStrTransactionId = getArguments().getString("TransactionID", getActivity().getString(R.string.payment_error));
        }

        initView(view);

        // Header visibility
        AirHeader.showRecentSearchIcon(false);

        // Show header text
        AirHeader.showHeaderText(mainActivity, true, "Payment");

        // Show&Hide toggle and drawer
        AirHeader.showDrawerToggleAndToolbar(false, false);

        return view;
    }


    private void initView(View v) {
        AirHeader.createHeader(mainActivity, "Payment");

        WebView lObjWebview = (WebView)v.findViewById(R.id.webView);

        /*ADDED FOR ALERT*/
        lObjWebview.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                //Required functionality here
                return super.onJsAlert(view, url, message, result);
            }
        });



        //lObjWebview.setInitialScale(1);
        WebSettings webSettings = lObjWebview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAppCacheEnabled(false);
        //lObjWebview.getSettings().setDomStorageEnabled(true);

        //lObjWebview.setWebChromeClient(new ChromeClient(getActivity()));

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.payment_details));

        String url = AES.decrypt(getActivity().getResources().getString(R.string.payment_webview_url)) + lStrTransactionId;

        //String url = "http://www.tutorialspoint.com/android/android_webview_layout.htm";

        lObjWebview.clearHistory();
        lObjWebview.clearCache(true);
        lObjWebview.loadUrl(url);

    }

    private void initData() {

    }

    @Override
    public void onResume() {
        super.onResume();
        ActivityMain.activeFragment = Constant.PAYMENT_WEBVIEW_FRAGMENT;
    }
}
