package com.irctc.air.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.irctc.air.Database.ServerDateSharedPrefernce;
import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.adapter.BookingHistoryPassListAdapter;
import com.irctc.air.adapter.BookingHistorySegmentInfoAdapter;
import com.irctc.air.header.AirHeader;
import com.irctc.air.util.AlertDialogUtil;
import com.irctc.air.util.Constant;
import com.irctc.air.util.DateUtility;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.ProjectUtil;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by tourism on 5/15/2016.
 */
public class BookHistoryInfoFragment extends Fragment implements View.OnClickListener {

    private ActivityMain mainActivity;
    private Button btnSubmit;
    private TextView flightBaseFare,irctcTxnfare, flightTaxes,flightTotal,txtBookingStatus, txtTransactionId;
    private ImageView imgRecentSearch;

    private ListView lvBookHistOnwardFlightDetail, lvBookHistOnwardFlightPassDetail;
    BookingHistorySegmentInfoAdapter mBookHistiryOnwardFlightAdapter;
    BookingHistoryPassListAdapter mBookHistOnwardPassAdapter;
    LinearLayout mCnfOnwardFlightlayoutMain ;
    TextView mCnfOnwardFlightFromToHeader,mCnfOnwardFlightToHeader, mCnfOnwardHeaderDuration,mCnfOnwardRefundable,mCnfOnwardFlightAmount, mCnfOnwardPNR, mCnfOnwardPNRHeader;


    private ListView lvBookHistReturnFlightDetail, lvBookHistReturnFlightPassDetail;
    BookingHistorySegmentInfoAdapter mBookHistiryReturnFlightAdapter;
    BookingHistoryPassListAdapter mBookHistReturnPassAdapter;
    CardView mCnfReturnFlightlayoutMain;
    TextView mCnfReturnFlightFromToHeader,mCnfReturnFlightToHeader, mCnfReturnHeaderDuration, mCnfReturnRefundable, mCnfReturnFlightAmount, mCnfReturnPNR, mCnfReturnPNRHeader;

    Button cancelTicket,rescheduleTicket;
    private int position;
    private String comingFrom;

    private LinearLayout layoutAll;
    private LinearLayout layoutBtn;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mainActivity = (ActivityMain) activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.book_history_info_fragment, null);


        // Initialize
        initializeVariable(view);

        // Header visibility
        AirHeader.showRecentSearchIcon(false);

        // Show header text
        AirHeader.showHeaderText(mainActivity, true, "Ticket");

        // Show&Hide toggle and drawer and toolbar
        AirHeader.showDrawerToggleAndToolbar(true, true);

        // Set data in variable from holder
        setdataInVarFromHolder();
        return view;
    }

    private void initializeVariable(View view) {

        if (getArguments().containsKey("commingFrom") && getArguments().containsKey("ClickedBookedFlightPos")) {

            position = getArguments().getInt("ClickedBookedFlightPos", 404);
            comingFrom = getArguments().getString("commingFrom", "ERROR");
        }

        imgRecentSearch = (ImageView) mainActivity.findViewById(R.id.RECENT_SEARCH);
        imgRecentSearch.setVisibility(View.VISIBLE);
        imgRecentSearch.setImageResource(R.drawable.recent_search);
                imgRecentSearch.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        mainActivity.lastActiveFragment = Constant.CONFIRM_TICKET_FRAGMENT;
                        ProjectUtil.replaceFragment(mainActivity, new FragmentPlanner(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

                    }
                });



        layoutBtn= (LinearLayout)view.findViewById(R.id.LAY_CANCEL_BTN);
        layoutAll = (LinearLayout)view.findViewById(R.id.LAY_ALL);

        cancelTicket  = (Button) view.findViewById(R.id.BTN_CANCEL_TICKET);
        rescheduleTicket  = (Button) view.findViewById(R.id.BTN_RESCHEDULE_TICKET);

        // Onward flights details
        mCnfOnwardFlightlayoutMain = (LinearLayout)view.findViewById(R.id.CONFIRMATION_ONWARD_LAYOUT_MAIN);
        lvBookHistOnwardFlightDetail = (ListView) view.findViewById(R.id.CONFIRMATION_ONWARD_QUOTE_LISTVIEW);
        lvBookHistOnwardFlightPassDetail = (ListView) view.findViewById(R.id.CONFIRMATION_ONWARD_PASS_LISTVIEW);


        mCnfOnwardFlightFromToHeader = (TextView) view.findViewById(R.id.CONFIRMATION_ONWARDS_HEADER_FROM_TO);
        mCnfOnwardFlightToHeader = (TextView) view.findViewById(R.id.CONFIRMATION_ONWARDS_HEADER_TO_CODE);


        //mCnfOnwardHeaderDuration = (TextView) view.findViewById(R.id.CONFIRMATION_ONWARDS_HEADER_DRATION);
        mCnfOnwardRefundable = (TextView) view.findViewById(R.id.CONFIRMATION_ONWARD_REFUNDABLE_TXT);
        //mCnfOnwardFlightAmount = (TextView) view.findViewById(R.id.CONFIRMATION_ONWARD_TICKET_AMOUNT);
        /**
         * 21 Oct 2016
         * PNR - PASS STATUS
         */
        mCnfOnwardPNR = (TextView) view.findViewById(R.id.CONFIRMATION_ONWARDS_PNR);
        mCnfOnwardPNRHeader = (TextView) view.findViewById(R.id.CONFIRMATION_ONWARDS_PNR_HEADER);


        // Return flights details
        mCnfReturnFlightlayoutMain = (CardView)view.findViewById(R.id.CONFIRMATION_RETURN_LAYOUT_MAIN);
        lvBookHistReturnFlightDetail = (ListView) view.findViewById(R.id.CONFIRMATION_RETURN_QUOTE_LISTVIEW);
        lvBookHistReturnFlightPassDetail = (ListView) view.findViewById(R.id.CONFIRMATION_RETURN_PASS_LISTVIEW);

        mCnfReturnFlightFromToHeader = (TextView) view.findViewById(R.id.CONFIRMATION_RETURNS_HEADER_FROM_TO);
        mCnfReturnFlightToHeader = (TextView) view.findViewById(R.id.CONFIRMATION_RETURNS_HEADER_TO_CODE);


        //mCnfReturnHeaderDuration = (TextView) view.findViewById(R.id.CONFIRMATION_RETURN_HEADER_DRATION);
        mCnfReturnRefundable = (TextView) view.findViewById(R.id.CONFIRMATION_RETURN_REFUNDABLE_TXT);
        //mCnfReturnFlightAmount = (TextView) view.findViewById(R.id.CONFIRMATION_RETURN_TICKET_AMOUNT);
        /**
         * 21 Oct 2016
         * PNR - PASS STATUS
         */
        mCnfReturnPNR = (TextView) view.findViewById(R.id.CONFIRMATION_RETURN_PNR);
        mCnfReturnPNRHeader = (TextView) view.findViewById(R.id.CONFIRMATION_RETURN_PNR_HEADER);


        flightBaseFare = (TextView) view.findViewById(R.id.CONFIRMATION_TXT_FARE_BASE);
        flightTaxes = (TextView) view.findViewById(R.id.CONFIRMATION_TXT_FARE_TAXES);

        /**
         * 07 NOV 2016
         * IRCTC SERVICE CHARGE
         */
        irctcTxnfare = (TextView) view.findViewById(R.id.IRCTC_TXT_FARE_TAXES);

        flightTotal = (TextView) view.findViewById(R.id.CONFIRMATION_TXT_FARE_TOTAL);

        txtBookingStatus = (TextView) view.findViewById(R.id.TXT_BOOKING_STATUS_VALUE);
        txtTransactionId = (TextView) view.findViewById(R.id.TXT_TRANSACTION_ID_VALUE);

        cancelTicket.setOnClickListener(this);
        rescheduleTicket.setOnClickListener(this);
    }


    private void setdataInVarFromHolder() {

         /********************************************************************************************************************************
         * BOOKING HISTORY INFO OUTER LAYOUT DETAILS
         ********************************************************************************************************************************/

        if(comingFrom.equals("Booking")){

            String statusCode = AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getBookingstatus();
            //AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlFareDetail().get(0).getBfare()
            // HERE TO CHECK TICKET CANCEL BUTTON IS VISIBLE OR NOT
            if(cancelButtonVisibility() && (statusCode.equalsIgnoreCase("7") || statusCode.equalsIgnoreCase("21") || statusCode.equalsIgnoreCase("22"))){
            //    cancelTicket.setVisibility(View.VISIBLE);

            }
            else{
            //    cancelTicket.setVisibility(View.GONE);

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 98.0f);
                layoutAll.setLayoutParams(params);

                LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 2.0f);
               // layoutBtn.setLayoutParams(param);

            }

            flightBaseFare.setText(mainActivity.getResources().getString(R.string.Rs) + AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlFareDetail().get(0).getBfare());
            flightTaxes.setText(mainActivity.getResources().getString(R.string.Rs) + AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlFareDetail().get(0).getTax1());
            /**
             * 07 NOV 2016
             * IRCTC SERVICE CHARGE
             */
            irctcTxnfare.setText(mainActivity.getResources().getString(R.string.Rs) + AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlFareDetail().get(0).getIrctcTxnFees());

            flightTotal.setText(mainActivity.getResources().getString(R.string.Rs)
                    + (Integer.parseInt(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlFareDetail().get(0).getSubTotal())
                    + Math.ceil(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlFareDetail().get(0).getIrctcTxnFees())));

            txtBookingStatus.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(0).getBookingstatusValue());
            txtTransactionId.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(0).getTrnId());

            /*************************************
             * If one way selected
             *************************************/

            if(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlBookedOnwardDetail().get(0).getAlSegmentDetail().get(0).getFaretype().equalsIgnoreCase("Refundable")){
                mCnfOnwardRefundable.setTextColor(getResources().getColor(R.color.TXT_GREEN));
            }else{
                mCnfOnwardRefundable.setTextColor(getResources().getColor(R.color.TXT_RED));
            }

            mCnfOnwardRefundable.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlBookedOnwardDetail().get(0).getAlSegmentDetail().get(0).getFaretype());

            //mCnfOnwardHeaderDuration.setText(getOnWardFlightTotalTime());

            // CLOSE BY ASIF
           // mCnfOnwardFlightFromToHeader.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getOrig() +" - "+AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getDest());


            mCnfOnwardFlightFromToHeader.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getOrig());

            mCnfOnwardFlightToHeader.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getDest());


            /**
             * 21 Oct 2016
             * PNR - PASS STATUS
             */
            if(!AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlBookedOnwardDetail().get(0).getAlSegmentDetail().get(0).getAirpnr().equalsIgnoreCase("")) {
                mCnfOnwardPNR.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlBookedOnwardDetail().get(0).getAlSegmentDetail().get(0).getAirpnr());
                mCnfOnwardPNRHeader.setVisibility(View.VISIBLE);
            }
            // ONWARD FLIGHT TICKET CONFIRMATION FLIGHT DETAIL
            mBookHistiryOnwardFlightAdapter = new BookingHistorySegmentInfoAdapter(mainActivity, AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlBookedOnwardDetail().get(0).getAlSegmentDetail());
            lvBookHistOnwardFlightDetail.setAdapter(mBookHistiryOnwardFlightAdapter);
            ProjectUtil.updateListViewHeight(lvBookHistOnwardFlightDetail);


            // ONWARD PASS PASS DETAIL
            mBookHistOnwardPassAdapter = new BookingHistoryPassListAdapter(mainActivity, AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlBookedOnwardDetail().get(0).getAlPassengerDetail());
            lvBookHistOnwardFlightPassDetail.setAdapter(mBookHistOnwardPassAdapter);
            ProjectUtil.updateListViewHeight(lvBookHistOnwardFlightPassDetail);

            txtBookingStatus.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getBookingstatusValue());
            txtTransactionId.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getTrnId());

            /*************************************
             * If round trip data avilable
             *************************************/

            if(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlBookedReturnDetail().get(0).getAlSegmentDetail().size() >0) {

                // RETURN CONFIRM LAYOUT
                mCnfReturnFlightlayoutMain.setVisibility(View.VISIBLE);

                if (AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlBookedReturnDetail().get(0).getAlSegmentDetail().get(0).getFaretype().equalsIgnoreCase("Refundable")) {
                    mCnfReturnRefundable.setTextColor(getResources().getColor(R.color.TXT_GREEN));
                } else {
                    mCnfReturnRefundable.setTextColor(getResources().getColor(R.color.TXT_RED));
                }

                mCnfReturnRefundable.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlBookedReturnDetail().get(0).getAlSegmentDetail().get(0).getFaretype());

                //mCnfReturnHeaderDuration.setText(getOnWardFlightTotalTime());
                // CLOSE BY ASIF
                //mCnfReturnFlightFromToHeader.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getDest()+ " - "+AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getOrig());
                mCnfReturnFlightFromToHeader.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getDest());
                mCnfReturnFlightToHeader.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getOrig());

                /**
                 * 21 Oct 2016
                 * PNR - PASS STATUS
                 */
                if(!AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlBookedReturnDetail().get(0).getAlSegmentDetail().get(0).getAirpnr().equalsIgnoreCase("")) {
                    mCnfReturnPNR.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlBookedReturnDetail().get(0).getAlSegmentDetail().get(0).getAirpnr());
                    mCnfReturnPNRHeader.setVisibility(View.VISIBLE);
                }

                // ONWARD FLIGHT TICKET CONFIRMATION FLIGHT DETAIL
                mBookHistiryReturnFlightAdapter = new BookingHistorySegmentInfoAdapter(mainActivity, AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlBookedReturnDetail().get(0).getAlSegmentDetail());
                lvBookHistReturnFlightDetail.setAdapter(mBookHistiryReturnFlightAdapter);
                ProjectUtil.updateListViewHeight(lvBookHistReturnFlightDetail);


                // ONWARD PASS PASS DETAIL
                mBookHistReturnPassAdapter = new BookingHistoryPassListAdapter(mainActivity, AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlBookedReturnDetail().get(0).getAlPassengerDetail());
                lvBookHistReturnFlightPassDetail.setAdapter(mBookHistReturnPassAdapter);
                ProjectUtil.updateListViewHeight(lvBookHistReturnFlightPassDetail);

            }else{
                // HIDE RETURN LAYOUT
                mCnfReturnFlightlayoutMain.setVisibility(View.GONE);

            }
        }
        else{

            /********************************************************************************************************************************
             * CANCELED HISTORY INFO OUTER LAYOUT DETAILS
             ********************************************************************************************************************************/

            //cancelTicket.setVisibility(View.GONE);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 98.0f);
            layoutAll.setLayoutParams(params);

            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 2.0f);
           // layoutBtn.setLayoutParams(param);


            flightBaseFare.setText(mainActivity.getResources().getString(R.string.Rs) + AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(position).getAlFareDetail().get(0).getBfare());
            flightTaxes.setText(mainActivity.getResources().getString(R.string.Rs) + AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(position).getAlFareDetail().get(0).getTax1());
            /**
             * 07 NOV 2016
             * IRCTC SERVICE CHARGE
             */
            irctcTxnfare.setText(mainActivity.getResources().getString(R.string.Rs) + AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(position).getAlFareDetail().get(0).getIrctcTxnFees());

            flightTotal.setText(mainActivity.getResources().getString(R.string.Rs)
                    + (Integer.parseInt(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(position).getAlFareDetail().get(0).getSubTotal())
                    + Math.ceil(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(position).getAlFareDetail().get(0).getIrctcTxnFees())));

            txtBookingStatus.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(0).getBookingstatusValue());
            txtTransactionId.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(0).getTrnId());

            /*************************************
             * If one way selected
             *************************************/

            if(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(position).getAlBookedOnwardDetail().get(0).getAlSegmentDetail().get(0).getFaretype().equalsIgnoreCase("Refundable")){
                mCnfOnwardRefundable.setTextColor(getResources().getColor(R.color.TXT_GREEN));
            }else{
                mCnfOnwardRefundable.setTextColor(getResources().getColor(R.color.TXT_RED));
            }

            mCnfOnwardRefundable.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(position).getAlBookedOnwardDetail().get(0).getAlSegmentDetail().get(0).getFaretype());

            //mCnfOnwardHeaderDuration.setText(getOnWardFlightTotalTime());

            // CLOSE BY ASIF
            //mCnfOnwardFlightFromToHeader.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(position).getOrig() +" - "+AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(position).getDest());



            mCnfOnwardFlightFromToHeader.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(position).getOrig());

            mCnfOnwardFlightToHeader.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(position).getDest());




            /**
             * 21 Oct 2016
             * PNR - PASS STATUS
             */
            if(!AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(position).getAlBookedOnwardDetail().get(0).getAlSegmentDetail().get(0).getAirpnr().equalsIgnoreCase("")) {
                mCnfOnwardPNR.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(position).getAlBookedOnwardDetail().get(0).getAlSegmentDetail().get(0).getAirpnr());
                mCnfOnwardPNRHeader.setVisibility(View.VISIBLE);
            }

            // ONWARD FLIGHT TICKET CONFIRMATION FLIGHT DETAIL
            mBookHistiryOnwardFlightAdapter = new BookingHistorySegmentInfoAdapter(mainActivity, AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(position).getAlBookedOnwardDetail().get(0).getAlSegmentDetail());
            lvBookHistOnwardFlightDetail.setAdapter(mBookHistiryOnwardFlightAdapter);
            ProjectUtil.updateListViewHeight(lvBookHistOnwardFlightDetail);


            // ONWARD PASS PASS DETAIL
            mBookHistOnwardPassAdapter = new BookingHistoryPassListAdapter(mainActivity, AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(position).getAlBookedOnwardDetail().get(0).getAlPassengerDetail());
            lvBookHistOnwardFlightPassDetail.setAdapter(mBookHistOnwardPassAdapter);
            ProjectUtil.updateListViewHeight(lvBookHistOnwardFlightPassDetail);

            txtBookingStatus.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(position).getBookingstatusValue());
            txtTransactionId.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(position).getTrnId());

            /*************************************
             * If round trip selected
             *************************************/

            if(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(position).getAlBookedReturnDetail().get(0).getAlSegmentDetail().size() >0) {

                // RETURN CONFIRM LAYOUT
                mCnfReturnFlightlayoutMain.setVisibility(View.VISIBLE);

                if (AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(position).getAlBookedReturnDetail().get(0).getAlSegmentDetail().get(0).getFaretype().equalsIgnoreCase("Refundable")) {
                    mCnfReturnRefundable.setTextColor(getResources().getColor(R.color.TXT_GREEN));
                } else {
                    mCnfReturnRefundable.setTextColor(getResources().getColor(R.color.TXT_RED));
                }

                mCnfReturnRefundable.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(position).getAlBookedReturnDetail().get(0).getAlSegmentDetail().get(0).getFaretype());

                //mCnfReturnHeaderDuration.setText(getOnWardFlightTotalTime());
                mCnfReturnFlightFromToHeader.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(position).getDest());
                mCnfReturnFlightToHeader.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(position).getOrig());


                /**
                 * 21 Oct 2016
                 * PNR - PASS STATUS
                 */


                if(!AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(position).getAlBookedReturnDetail().get(0).getAlSegmentDetail().get(0).getAirpnr().equalsIgnoreCase("")) {
                    mCnfReturnPNR.setText(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(position).getAlBookedReturnDetail().get(0).getAlSegmentDetail().get(0).getAirpnr());
                    mCnfReturnPNRHeader.setVisibility(View.VISIBLE);
                }


                // ONWARD FLIGHT TICKET CONFIRMATION FLIGHT DETAIL
                mBookHistiryReturnFlightAdapter = new BookingHistorySegmentInfoAdapter(mainActivity, AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(position).getAlBookedReturnDetail().get(0).getAlSegmentDetail());
                lvBookHistReturnFlightDetail.setAdapter(mBookHistiryReturnFlightAdapter);
                ProjectUtil.updateListViewHeight(lvBookHistReturnFlightDetail);


                // ONWARD PASS PASS DETAIL
                mBookHistReturnPassAdapter = new BookingHistoryPassListAdapter(mainActivity, AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getCancelHistory().get(position).getAlBookedReturnDetail().get(0).getAlPassengerDetail());
                lvBookHistReturnFlightPassDetail.setAdapter(mBookHistReturnPassAdapter);
                ProjectUtil.updateListViewHeight(lvBookHistReturnFlightPassDetail);

            }else{
                // HIDE RETURN LAYOUT
                mCnfReturnFlightlayoutMain.setVisibility(View.GONE);

            }
        }

    }


    private boolean cancelButtonVisibility() {

        long date = System.currentTimeMillis();
       // String formatedDate = DateUtility.currentTime(date);
       //  Date dateToday =  DateUtility.getBeforeAfterTime(formatedDate, 0, 0);

        Date dateToday =  DateUtility.getBeforeAfterTime(new ServerDateSharedPrefernce(mainActivity).getServerDate(), 0, 0);
        String departTime = AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory().getBookedHistory().get(position).getAlBookedOnwardDetail().get(0).getAlSegmentDetail().get(0).getDepartTime();
        Date dateDapart =  DateUtility.getBeforeAfterTime(departTime, 0, 0);

        if( compareDateTime(dateToday,dateDapart)){
            return  true;
        }
        return false;
    }

    private boolean compareDateTime(Date date2, Date dateX){

        try {

            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(date2);

            if(date2.before(dateX)){
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public String getOnWardFlightTotalTime(){

        //"" + AirDataHolder.getListHolder().getList().get(0).getmFareQuoteOneWayBean().getFlight().get(0).getDuration()

        return "Working";
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        ActivityMain.activeFragment = Constant.BOOKING_HISTORY_INFO_FRAGMENT;
    }

    @Override
    public void onClick(View v) {

    switch (v.getId()){
        case R.id.BTN_RESCHEDULE_TICKET:


            if (!ProjectUtil.checkInternetConnection(mainActivity)) {

                new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.INTERNET_DOWN), mainActivity.getResources().getString(R.string.NO_INTERNET_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                //ProjectUtil.showToast(getResources().getString(R.string.INTERNET_DOWN), mainActivity);

            } else {

                int version = 0;
                try {
                    version = mainActivity.getPackageManager().getPackageInfo(mainActivity.getPackageName(), 0).versionCode;
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

                if(version < 6){
                    // update when click on cancel the ticket

                    final AlertDialog.Builder alertDialog = new AlertDialog.Builder(mainActivity);
                    alertDialog.setTitle("Cancel Ticket");
                    alertDialog.setMessage(R.string.CANCELLATION_MESSAGE);
                    // Setting OK Button

                    alertDialog.setTitle(mainActivity.getString(R.string.new_update_available));
                    alertDialog.setMessage(mainActivity.getString(R.string.update_your_app)).setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Uri marketUri = Uri.parse("market://details?id=com.irctc.air");
                            Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
                            mainActivity.startActivity(marketIntent);
                        }
                    })

                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    dialog.cancel();
                                    //((AppCompatActivity) mainActivity).finish();
                                }
                            });

                    alertDialog.show();

                }else {

                    ActivityMain.lastActiveFragment = Constant.BOOKING_HISTORY_INFO_FRAGMENT;
                    Fragment cancelTicketFragment = new RescheduleTicketFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt("ClickedBookedFlightPos", position);
                    //bundle.putString("commingFrom", "Booking");
                    cancelTicketFragment.setArguments(bundle);
                    ProjectUtil.replaceFragment(mainActivity, cancelTicketFragment, R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

                }
            }


            break;
        case R.id.BTN_CANCEL_TICKET:

            if (!ProjectUtil.checkInternetConnection(mainActivity)) {

                new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.INTERNET_DOWN), mainActivity.getResources().getString(R.string.NO_INTERNET_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                //ProjectUtil.showToast(getResources().getString(R.string.INTERNET_DOWN), mainActivity);

            } else {

                int version = 0;
                try {
                    version = mainActivity.getPackageManager().getPackageInfo(mainActivity.getPackageName(), 0).versionCode;
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

                if(version < 6){
                    // update when click on cancel the ticket

                    final AlertDialog.Builder alertDialog = new AlertDialog.Builder(mainActivity);
                    alertDialog.setTitle("Cancel Ticket");
                    alertDialog.setMessage(R.string.CANCELLATION_MESSAGE);
                    // Setting OK Button

                    alertDialog.setTitle(mainActivity.getString(R.string.new_update_available));
                    alertDialog.setMessage(mainActivity.getString(R.string.update_your_app)).setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Uri marketUri = Uri.parse("market://details?id=com.irctc.air");
                            Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);
                            mainActivity.startActivity(marketIntent);
                        }
                    })

                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    dialog.cancel();
                                    //((AppCompatActivity) mainActivity).finish();
                                }
                            });

                    alertDialog.show();

                }else {

                    ActivityMain.lastActiveFragment = Constant.BOOKING_HISTORY_INFO_FRAGMENT;
                    Fragment cancelTicketFragment = new CancelTicketFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt("ClickedBookedFlightPos", position);
                    //bundle.putString("commingFrom", "Booking");
                    cancelTicketFragment.setArguments(bundle);
                    ProjectUtil.replaceFragment(mainActivity, cancelTicketFragment, R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

                }
            }

            break;
    }
    }

}
