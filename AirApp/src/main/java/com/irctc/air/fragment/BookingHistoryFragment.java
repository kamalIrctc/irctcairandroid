package com.irctc.air.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.auth0.android.jwt.JWT;
import com.google.gson.Gson;
import com.irctc.air.AppController;
import com.irctc.air.Database.AirDatabase;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.adapter.AdapterBookingHistory;
import com.irctc.air.header.AirHeader;
import com.irctc.air.model.ModelBookingHistory;
import com.irctc.air.model.booking_history.Data;
import com.irctc.air.model.booking_history.PojoBookingHistory;
import com.irctc.air.navigationdrawer.SliderMenu;
import com.irctc.air.networking.Networking;
import com.irctc.air.util.AppKeys;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.NetworkingUtils;
import com.irctc.air.util.Pref;
import com.irctc.air.util.ProjectUtil;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by vivek on 4/27/2016.
 */
public class BookingHistoryFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener/*, AdapterView.OnItemClickListener */{
    private Activity activity;
    private ListView lvBookingHistory, lvCancellationHistory;
    private LinearLayout lLayCancelHisLV, lLayMainOuter, lLayCancelticket,lLayPrintTicket, lLayBookedBottom, lLayCancelBottom ;
    private FrameLayout lLayBookingHisLV;
    private TextView txtxErrorMsg;
    ArrayList<ModelBookingHistory> arrayList;
    public static String transactionId;
    AirDatabase airDatabase;
    JWT jwt;
    Date dateExp;
    Date dateCurr;
    long duration;
    long diffInMinutes;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity=getActivity();
        transactionId=null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.booking_history_layout, null);
        initializeVariable(view);
        AirHeader.showRecentSearchIcon(false);
        AirHeader.showHeaderText((ActivityMain)activity, true, "Booking History");
        AirHeader.showRecentSearchIcon(false);
        AirHeader.showDrawerToggleAndToolbar(true, true);
        airDatabase=new AirDatabase(getActivity());
        try{
        jwt=new JWT(airDatabase.getAuthToken());
        dateExp=jwt.getExpiresAt();
        dateCurr=new Date();
        duration  = dateExp.getTime()- dateCurr.getTime();
        diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
        if(diffInMinutes<=0) {
            Context ctx = getActivity().getApplicationContext();
            Pref.setString(ctx, AppKeys.USER_DETAIL_ADDRESS_1, "");
            Pref.setString(ctx, AppKeys.USER_DETAIL_ADDRESS_2, "");
            Pref.setString(ctx, AppKeys.USER_DETAIL_FIRST_NAME, "");
            Pref.setString(ctx, AppKeys.USER_DETAIL_LAST_NAME, "");
            Pref.setString(ctx, AppKeys.USER_DETAIL_CITY, "");
            Pref.setString(ctx, AppKeys.USER_DETAIL_STATE, "");
            Pref.setString(ctx, AppKeys.USER_DETAIL_COUNTRY, "");
            Pref.setString(ctx, AppKeys.USER_DETAIL_PIN_CODE, "");
            Pref.setString(ctx, AppKeys.USER_DETAIL_EMAIL, "");
            Pref.setString(ctx, AppKeys.USER_DETAIL_MOBILE_NO, "");
            Pref.setBoolean(ctx, false);
            SliderMenu.txtUserId.setText("Guest");
            new AirDatabase(getActivity()).removeAuthToken();
            ProjectUtil.replaceFragment(activity, new FragmentLogin(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
            }else{
            getBookingHistoryFromServer();
            }
        }catch(Exception e){
            getBookingHistoryFromServer();
        }

        return view;
    }


    private void initializeVariable(View view) {
        lLayPrintTicket = (LinearLayout) view.findViewById(R.id.BOOKING_HISTORY_TAB);
        lLayCancelticket = (LinearLayout) view.findViewById(R.id.CANCELLATION_HISTORY_TAB);
        lLayBookedBottom = (LinearLayout) view.findViewById(R.id.LAY_BOOKED_TAB_BTM);
        lLayCancelBottom = (LinearLayout) view.findViewById(R.id.LAY_CANCEL_TAB_BTM);
        lvBookingHistory = (ListView) view.findViewById(R.id.BOOKING_HISTORY_LISTVIEW);
        lvCancellationHistory = (ListView) view.findViewById(R.id.CANCEL_HISTORY_LISTVIEW);
        lLayBookingHisLV = (FrameLayout) view.findViewById(R.id.BOOKING_HISTORY_MAIN_LAYOUT);
        lLayCancelHisLV = (LinearLayout) view.findViewById(R.id.CANCEL_HISTORY_MAIN_LAYOUT);
        lLayMainOuter = (LinearLayout) view.findViewById(R.id.LAY_ERROR);
        txtxErrorMsg = (TextView) view.findViewById(R.id.textViewMsg);
        lLayPrintTicket.setOnClickListener(this);
        lLayCancelticket.setOnClickListener(this);
        lvBookingHistory.setOnItemClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.BOOKING_HISTORY_TAB:
                lLayBookedBottom.setBackgroundColor( getResources().getColor(R.color.colorLightBlue));
                lLayCancelBottom.setBackgroundColor(getResources().getColor(R.color.colorDARKGrey));
                lLayMainOuter.setVisibility(View.GONE);
                AirDatabase airDatabase=new AirDatabase(getActivity());
                jwt=new JWT(airDatabase.getAuthToken());
                dateExp=jwt.getExpiresAt();
                dateCurr=new Date();
                duration  = dateExp.getTime()- dateCurr.getTime();
                diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
                if(diffInMinutes<=0) {
                    Context ctx=getActivity().getApplicationContext();
                    Pref.setString(ctx, AppKeys.USER_DETAIL_ADDRESS_1,"");
                    Pref.setString(ctx, AppKeys.USER_DETAIL_ADDRESS_2,"");
                    Pref.setString(ctx, AppKeys.USER_DETAIL_FIRST_NAME,"");
                    Pref.setString(ctx, AppKeys.USER_DETAIL_LAST_NAME,"");
                    Pref.setString(ctx, AppKeys.USER_DETAIL_CITY,"");
                    Pref.setString(ctx, AppKeys.USER_DETAIL_STATE,"");
                    Pref.setString(ctx, AppKeys.USER_DETAIL_COUNTRY,"");
                    Pref.setString(ctx, AppKeys.USER_DETAIL_PIN_CODE,"");
                    Pref.setString(ctx, AppKeys.USER_DETAIL_EMAIL,"");
                    Pref.setString(ctx, AppKeys.USER_DETAIL_MOBILE_NO,"");
                    Pref.setBoolean(ctx,false);
                    SliderMenu.txtUserId.setText("Guest");
                    new AirDatabase(getActivity()).removeAuthToken();
                    ProjectUtil.replaceFragment(activity, new FragmentLogin(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                }else{
                    getBookingHistoryFromServer();
                }
                break;

            case R.id.CANCELLATION_HISTORY_TAB:
                lLayBookedBottom.setBackgroundColor( getResources().getColor(R.color.colorDARKGrey));
                lLayCancelBottom.setBackgroundColor(getResources().getColor(R.color.colorLightBlue));
                lLayMainOuter.setVisibility(View.GONE);
                airDatabase = new AirDatabase(getActivity());
                jwt = new JWT(airDatabase.getAuthToken());
                dateExp = jwt.getExpiresAt();
                dateCurr = new Date();
                duration  = dateExp.getTime()- dateCurr.getTime();
                diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
                if(diffInMinutes<=0) {
                    Context ctx=getActivity().getApplicationContext();
                    Pref.setString(ctx, AppKeys.USER_DETAIL_ADDRESS_1,"");
                    Pref.setString(ctx, AppKeys.USER_DETAIL_ADDRESS_2,"");
                    Pref.setString(ctx, AppKeys.USER_DETAIL_FIRST_NAME,"");
                    Pref.setString(ctx, AppKeys.USER_DETAIL_LAST_NAME,"");
                    Pref.setString(ctx, AppKeys.USER_DETAIL_CITY,"");
                    Pref.setString(ctx, AppKeys.USER_DETAIL_STATE,"");
                    Pref.setString(ctx, AppKeys.USER_DETAIL_COUNTRY,"");
                    Pref.setString(ctx, AppKeys.USER_DETAIL_PIN_CODE,"");
                    Pref.setString(ctx, AppKeys.USER_DETAIL_EMAIL,"");
                    Pref.setString(ctx, AppKeys.USER_DETAIL_MOBILE_NO,"");
                    Pref.setBoolean(ctx,false);
                    SliderMenu.txtUserId.setText("Guest");
                    new AirDatabase(getActivity()).removeAuthToken();
                    ProjectUtil.replaceFragment(activity, new FragmentLogin(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                }else{
                getCancellationHistoryFromServer();
                }
                break;
            default:
                break;
        }
    }

    private void getBookingHistoryFromServer(){
        NetworkingUtils.showProgress(activity);
        txtxErrorMsg.setVisibility(View.GONE);
        AirDatabase airDatabase=new AirDatabase(activity.getApplicationContext());
        String authToken=airDatabase.getAuthToken();
        Networking.getBookingHistory(authToken, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
            NetworkingUtils.dismissProgress();
            Log.e("Response",response.toString());
                PojoBookingHistory pojoBookingHistory=new Gson().fromJson(response.toString(),PojoBookingHistory.class);
                if(pojoBookingHistory.getStatus().equals("SUCCESS")){
                    Data[] bookingDataList = pojoBookingHistory.getData();
                    arrayList=new ArrayList<ModelBookingHistory>();
                    for(int i=0;i<bookingDataList.length;i++){
                        ModelBookingHistory modelBookingHistory=new ModelBookingHistory();
                        modelBookingHistory.setTransactionId(bookingDataList[i].getTransactionid());
                        modelBookingHistory.setOrigin(bookingDataList[i].getOrg());
                        modelBookingHistory.setDestination(bookingDataList[i].getDest());
                        modelBookingHistory.setTripType(bookingDataList[i].getOnretround());
                        modelBookingHistory.setBookingStatus(bookingDataList[i].getValue());
                        String departureDate=bookingDataList[i].getDeparturetime().split(" ")[0];
                        try {
                            Date date=new SimpleDateFormat("yyyy-MM-dd").parse(departureDate);
                            modelBookingHistory.setDate(new SimpleDateFormat("dd").format(date));
                            modelBookingHistory.setMonth(new SimpleDateFormat("MMM").format(date));
                            modelBookingHistory.setYear(new SimpleDateFormat("yyyy").format(date));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        arrayList.add(modelBookingHistory);
                    }
                        lvBookingHistory.setAdapter(new AdapterBookingHistory(activity.getApplicationContext(),arrayList));
                        if(arrayList.size()==0){
                        txtxErrorMsg.setVisibility(View.VISIBLE);
                        txtxErrorMsg.setText("Sorry, You don't have any booked ticket.");
                        }

                }else{
                    Toast.makeText(activity.getApplicationContext(),pojoBookingHistory.getMessage(),Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            NetworkingUtils.dismissProgress();
            Log.e("Error",error.toString());
                if(error.networkResponse!=null)
                Toast.makeText(activity.getApplicationContext(),"No Internet Access!",Toast.LENGTH_SHORT).show();
                else
                Toast.makeText(activity.getApplicationContext(),"No Internet Access!",Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getCancellationHistoryFromServer(){
        NetworkingUtils.showProgress(activity);
        txtxErrorMsg.setVisibility(View.GONE);
        AirDatabase airDatabase=new AirDatabase(activity.getApplicationContext());
        String authToken=airDatabase.getAuthToken();
        Networking.getCancellationHistory(authToken, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                NetworkingUtils.dismissProgress();
                Log.e("Response",response.toString());
                PojoBookingHistory pojoBookingHistory=new Gson().fromJson(response.toString(),PojoBookingHistory.class);
                if(pojoBookingHistory.getStatus().equals("SUCCESS")){
                    Data[] bookingDataList = pojoBookingHistory.getData();
                    arrayList=new ArrayList<ModelBookingHistory>();
                    for(int i=0;i<bookingDataList.length;i++){
                        ModelBookingHistory modelBookingHistory=new ModelBookingHistory();
                        modelBookingHistory.setTransactionId(bookingDataList[i].getTransactionid());
                        modelBookingHistory.setOrigin(bookingDataList[i].getOrg());
                        modelBookingHistory.setDestination(bookingDataList[i].getDest());
                        modelBookingHistory.setTripType(bookingDataList[i].getOnretround());
                        modelBookingHistory.setBookingStatus(bookingDataList[i].getValue());
                        String departureDate=bookingDataList[i].getDeparturetime().split(" ")[0];
                        try {
                            Date date=new SimpleDateFormat("yyyy-MM-dd").parse(departureDate);
                            modelBookingHistory.setDate(new SimpleDateFormat("dd").format(date));
                            modelBookingHistory.setMonth(new SimpleDateFormat("MMM").format(date));
                            modelBookingHistory.setYear(new SimpleDateFormat("yyyy").format(date));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        arrayList.add(modelBookingHistory);
                    }
                    lvBookingHistory.setAdapter(new AdapterBookingHistory(activity.getApplicationContext(),arrayList));
                    if(arrayList.size()==0){
                        txtxErrorMsg.setVisibility(View.VISIBLE);
                        txtxErrorMsg.setText("Sorry, You don't have any Cancelled ticket.");
                    }
                }else{
                    Toast.makeText(activity.getApplicationContext(),pojoBookingHistory.getMessage(),Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkingUtils.dismissProgress();
                Log.e("Error",error.toString());
                if(error.networkResponse!=null)
                    Toast.makeText(activity.getApplicationContext(),"No Internet Access!",Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(activity.getApplicationContext(),"No Internet Access!",Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        transactionId=arrayList.get(position).getTransactionId();
        ProjectUtil.replaceFragment(activity, new FragmentTicketDetails(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
    }

    @Override
    public void onResume() {
        super.onResume();
        AppController.getInstance().trackScreenView("Booking History Screen");
    }
}
