package com.irctc.air.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.header.AirHeader;
import com.irctc.air.util.Constant;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.ProjectUtil;


/**
 * Created by vivek on 04/13/2016
 */
public class PaymentFailedFragment extends Fragment {


    private ActivityMain mainActivity;
    private TextView tvTransactionId;
    private String lStrTransactionId;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mainActivity = (ActivityMain) activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.payment_failed, null);

        initView(view);

        // Header visibility
        AirHeader.showRecentSearchIcon(false);

        // Show header text
        AirHeader.showHeaderText(mainActivity, true, "Payment");

        // Show&Hide toggle and drawer
        AirHeader.showDrawerToggleAndToolbar(false, true);

        initData();

        return view;
    }


    private void initView(View view) {
        AirHeader.createHeader(mainActivity, "Payment");

        tvTransactionId = (TextView) view.findViewById(R.id.TRANSACTION_ID);

        if (getArguments().containsKey("TransactionID")) {
            lStrTransactionId = getArguments().getString("TransactionID", getActivity().getString(R.string.payment_error));
        }

        tvTransactionId.setText(lStrTransactionId);


        Button btnPlannerBack = (Button) view.findViewById(R.id.BTN_PLANNER);
        btnPlannerBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ProjectUtil.replaceFragment(mainActivity, new FragmentPlanner(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

            }
        });

    }

    private void initData() {



    }

    @Override
    public void onResume() {
        super.onResume();
        ActivityMain.activeFragment = Constant.PAYMENT_FAILED_FRAGMENT;
    }
}
