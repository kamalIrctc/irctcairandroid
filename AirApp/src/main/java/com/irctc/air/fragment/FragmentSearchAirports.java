package com.irctc.air.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.irctc.air.AppController;
import com.irctc.air.Database.AirDatabase;
import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.R;
import com.irctc.air.header.AirHeader;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.model.AirDataModel;
import com.irctc.air.model.search_airports.Data;
import com.irctc.air.model.search_airports.PojoSearchAirports;
import com.irctc.air.util.Constant;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.ProjectUtil;
import com.irctc.air.util.StationtList;
import com.irctc.air.networking.Networking;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

/**
 * Created by Aasif on 4/18/2016.
 * It is used to search the Airports
 */
public class FragmentSearchAirports extends Fragment implements AdapterView.OnItemClickListener {

    private StationtList mObjAirStationList;
    private ActivityMain mainActivity;
    public EditText mEditTextSearch;
    private TextView mTxtError;
    private ListView mSortedList;
    private static ArrayList<String> mAirStationListSearch;
    private LinearLayout mcancelBtn;
    ArrayList<Data> dataArrayList;
    Gson gson;
    private Context context;
    private AirDatabase airDatabase;
    public FragmentSearchAirports() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gson = new Gson();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mainActivity = (ActivityMain) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_station_search, null);

        // Header visibility
        AirHeader.showRecentSearchIcon(false);

        // Show header text
        AirHeader.showHeaderText(mainActivity, true, "Search Airport");

        // Show&Hide toggle and drawer and toolbar
        AirHeader.showDrawerToggleAndToolbar(false, false);
        context = getActivity();
        airDatabase = new AirDatabase(context);
        mObjAirStationList = new StationtList(mainActivity);
        mAirStationListSearch = new ArrayList<>();
        mAirStationListSearch.addAll(mObjAirStationList.mList);

        mEditTextSearch = (EditText) view.findViewById(R.id.EDIT_SEARCH_BOX);
        mTxtError = (TextView) view.findViewById(R.id.ALL_STATION_ERROR_MESSAGE);
        mSortedList = (ListView) view.findViewById(R.id.ALL_STATION_LISTVIEW);
        mcancelBtn = (LinearLayout) view.findViewById(R.id.CANCEL_BTN);
        if (mainActivity.isDepartureStationSelected) {
            mEditTextSearch.setHint("From");
        } else {
            mEditTextSearch.setHint("To");
        }
        mcancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditTextSearch.setText("");

                mObjAirStationList = new StationtList(mainActivity);
                mSortedList.setAdapter(null);
                mSortedList.invalidateViews();
                getFrequentAirport();
            }
        });

//        mSortedList.setAdapter(new StationListAdapter(,mainActivity));
        mSortedList.setOnItemClickListener(this);

        mEditTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence userInput, int start, int before, int count) {
                if (userInput.length() > 1) {
                    dataArrayList = airDatabase.getAirportsByKeyword(userInput.toString().trim());
                    mSortedList.setAdapter(new StationListAdapter(dataArrayList, mainActivity));
                   /* Networking.searchAirport(userInput.toString(), new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            PojoSearchAirports pojoSearchAirports = gson.fromJson(response.toString(), PojoSearchAirports.class);
                            if (pojoSearchAirports.getStatus().equals("SUCCESS")) {
                                Data[] dataList = pojoSearchAirports.getData();
                                dataArrayList = new ArrayList<Data>(Arrays.asList(dataList));
                                mSortedList.setAdapter(new StationListAdapter(dataArrayList, mainActivity));
                            } else {
                                Toast.makeText(getActivity(), pojoSearchAirports.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (getActivity() != null)
                                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });*/
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

        });

        getFrequentAirport();
        return view;
    }

    private void getFrequentAirport() {
        String json = "{\n" +
                "\"status\":\"SUCCESS\",\n" +
                "\"message\":\"Success\",\n" +
                "\"data\":[{\"code\":\"BOM\",\"cityCode\":\"BOM\",\"cityName\":\"Mumbai\",\"countryCode\":\"IN\",\"name\":\"Chhatrapati Shivaji\",\"oldCityName\":\"Bombay\",\"label\":\"Mumbai (BOM)\",\"value\":\"BOM\"},{\"code\":\"DEL\",\"cityCode\":\"DEL\",\"cityName\":\"New Delhi\",\"countryCode\":\"IN\",\"name\":\"Delhi Indira Gandhi Intl\",\"oldCityName\":\"Delhi\",\"label\":\"New Delhi (DEL)\",\"value\":\"DEL\"},{\"code\":\"BLR\",\"cityCode\":\"BLR\",\"cityName\":\"Bengaluru\",\"countryCode\":\"IN\",\"name\":\"Kempegowda Intl Arpt\",\"oldCityName\":\"Bangalore\",\"label\":\"Bengaluru (BLR)\",\"value\":\"BLR\"},{\"code\":\"MAA\",\"cityCode\":\"MAA\",\"cityName\":\"Chennai\",\"countryCode\":\"IN\",\"name\":\"Chennai Arpt\",\"oldCityName\":\"Madras\",\"label\":\"Chennai (MAA)\",\"value\":\"MAA\"},{\"code\":\"CCU\",\"cityCode\":\"CCU\",\"cityName\":\"Kolkata\",\"countryCode\":\"IN\",\"name\":\"Netaji Subhas Chandra Bose Intl\",\"oldCityName\":\"Calcutta\",\"label\":\"Kolkata (CCU)\",\"value\":\"CCU\"},{\"code\":\"LKO\",\"cityCode\":\"LKO\",\"cityName\":\"Lucknow\",\"countryCode\":\"IN\",\"name\":\"Amausi Arpt\",\"oldCityName\":null,\"label\":\"Lucknow (LKO)\",\"value\":\"LKO\"},{\"code\":\"PNQ\",\"cityCode\":\"PNQ\",\"cityName\":\"Pune\",\"countryCode\":\"IN\",\"name\":\"Lohegaon Arpt\",\"oldCityName\":null,\"label\":\"Pune (PNQ)\",\"value\":\"PNQ\"},{\"code\":\"HYD\",\"cityCode\":\"HYD\",\"cityName\":\"Hyderabad\",\"countryCode\":\"IN\",\"name\":\"Rajiv Gandhi Intl\",\"oldCityName\":null,\"label\":\"Hyderabad (HYD)\",\"value\":\"HYD\"},{\"code\":\"PAT\",\"cityCode\":\"PAT\",\"cityName\":\"Patna\",\"countryCode\":\"IN\",\"name\":\"Jai Prakash Narayan Arpt\",\"oldCityName\":null,\"label\":\"Patna (PAT)\",\"value\":\"PAT\"},{\"code\":\"JAI\",\"cityCode\":\"JAI\",\"cityName\":\"Jaipur\",\"countryCode\":\"IN\",\"name\":\"Jaipur International Airport\",\"oldCityName\":null,\"label\":\"Jaipur (JAI)\",\"value\":\"JAI\"}]}";
        PojoSearchAirports pojoSearchAirports = gson.fromJson(json, PojoSearchAirports.class);
        if (pojoSearchAirports.getStatus().equals("SUCCESS")) {
            Data[] dataList = pojoSearchAirports.getData();
            dataArrayList = new ArrayList<>(Arrays.asList(dataList));
            mSortedList.setAdapter(new StationListAdapter(dataArrayList, mainActivity));
        } else {
            Toast.makeText(getActivity(), pojoSearchAirports.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    private ArrayList<String> filterAirStationList(final CharSequence STAITON) {

        final String lStrStationSearchInput = STAITON.toString().toLowerCase(Locale.ENGLISH);
        try {

            mAirStationListSearch.clear();
            for (String lStationFromList : mObjAirStationList.mList) {

                String[] lStrStationListSpilt = lStationFromList.toLowerCase().split(",");

                if (lStrStationSearchInput.length() > 2) {
                    if (lStrStationListSpilt[0].trim().startsWith(lStrStationSearchInput) || lStrStationListSpilt[0].trim().contains(" " + lStrStationSearchInput) || lStrStationListSpilt[1].trim().startsWith(lStrStationSearchInput)) {
                        mAirStationListSearch.add(lStationFromList);
                    }
                } else {
                    if (lStrStationListSpilt[0].trim().contains(" " + lStrStationSearchInput) || lStrStationListSpilt[1].trim().startsWith(lStrStationSearchInput)) {
                        mAirStationListSearch.add(lStationFromList);
                    }
                }
            }
            mSortedList.invalidateViews();
            String[] lRetVal = new String[mAirStationListSearch.size()];
            mAirStationListSearch.toArray(lRetVal);

        } catch (Exception e) {
        }

        return mAirStationListSearch;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


        mainActivity.removePhoneKeypad(view);

        if (mainActivity.isDepartureStationSelected) {
            ArrayList<AirDataModel> alAirsdData = AirDataHolder.getListHolder().getList();
            if (mAirStationListSearch.get(position).split("-")[0].equalsIgnoreCase(alAirsdData.get(0).getToStation())) {
                Toast.makeText(mainActivity, "From and To station should be different.", Toast.LENGTH_SHORT).show();
            } else {

                alAirsdData.get(0).setFromStation(dataArrayList.get(position).getCode()/*mAirStationListSearch.get(position).split(",")[1]*/);
                alAirsdData.get(0).setFromStationCity(dataArrayList.get(position).getCityName() + " " + dataArrayList.get(position).getCountryCode()/*mAirStationListSearch.get(position).split(",")[0]*/);
            }
        } else {
            ArrayList<AirDataModel> alAirsdData = AirDataHolder.getListHolder().getList();
            if (mAirStationListSearch.get(position).split("-")[0].equalsIgnoreCase(alAirsdData.get(0).getFromStation())) {
                Toast.makeText(mainActivity, "From and To station should be different.", Toast.LENGTH_SHORT).show();
            } else {
                alAirsdData.get(0).setToStation(dataArrayList.get(position).getCode()/*mAirStationListSearch.get(position).split(",")[1]*/);
                alAirsdData.get(0).setToStationCity(dataArrayList.get(position).getCityName() + " " + dataArrayList.get(position).getCountryCode()/*mAirStationListSearch.get(position).split(",")[0]*/);
            }

        }


        // Send back to planner fragment
        mainActivity.lastActiveFragment = Constant.STATION_SEARCH_FRAGMENT;
        ProjectUtil.replaceFragment(mainActivity, new FragmentPlanner(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

    }

    private static class StationListAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        private ArrayList<Data> stationList;

        public StationListAdapter(ArrayList<Data> stationList, Context context) {
            mInflater = LayoutInflater.from(context);
            this.stationList = stationList;
        }

        public int getCount() {
            return stationList.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertView, ViewGroup parent) {

            ViewHolder holder;
            if (convertView == null) {

                convertView = mInflater.inflate(R.layout.station_search_list_item, null);

                holder = new ViewHolder();

                holder.mTxtStationName = (TextView) convertView.findViewById(R.id.SEARCH_PAGE_STATION_NAME_TEXT);
                // holder.mTxtStationCode = (TextView) convertView.findViewById(R.id.TEXT_STATION_DETAILS);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();

            }

            String Str = new String(mAirStationListSearch.get(position));

            String[] lStSplitStations;

            lStSplitStations = Str.split(",");


            holder.mTxtStationName.setText(stationList.get(position).getLabel());
            // holder.mTxtStationCode.setText(lStSplitStations[1]);

            return convertView;
        }

        static class ViewHolder {
            // TextView mTxtStationCode;
            TextView mTxtStationName;

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ActivityMain.activeFragment = Constant.STATION_SEARCH_FRAGMENT;
        AppController.getInstance().trackScreenView("Search Airport Screen");
    }
}
