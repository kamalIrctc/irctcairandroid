package com.irctc.air.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.irctc.air.AppController;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.header.AirHeader;
import com.irctc.air.util.AES;
import com.irctc.air.util.AppKeys;
import com.irctc.air.util.Constant;
import com.irctc.air.Database.SharedPrefrenceAir;
import com.irctc.air.util.Pref;

/**
 * Created by vivek on 4/27/2016.
 */
public class FragmentProfile extends Fragment {

    private ActivityMain mainActivity;
    TextView txtfullName,txtUserName,txtMobNum,txtUserAddress, txtEmail;

    public FragmentProfile(){
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mainActivity = (ActivityMain) activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.air_profile, null);

        // Initialize
        initializeVariable(view);

        // Header visibility
        AirHeader.showRecentSearchIcon(false);

        // Show header text
        AirHeader.showHeaderText(mainActivity, true, "Profile Details");

        return view;
    }




    private void initializeVariable(View view) {


        txtfullName = (TextView) view.findViewById(R.id.USER_FULL_NAME);
        txtUserName = (TextView) view.findViewById(R.id.USER_NAME);
        txtMobNum = (TextView) view.findViewById(R.id.USER_MOB_NUMBER);
        txtEmail = (TextView) view.findViewById(R.id.EMAIL);
        txtUserAddress = (TextView) view.findViewById(R.id.USER_ADDRESS);


        if(Pref.getString(getActivity(),AppKeys.USER_DETAIL_USER_TYPE).equals("user")) {
            txtfullName.setText(Pref.getString(getActivity(), AppKeys.USER_DETAIL_FIRST_NAME));
            txtfullName.setVisibility(View.VISIBLE);
            txtUserName.setText(Pref.getString(getActivity(), AppKeys.USER_DETAIL_FIRST_NAME)+" "+Pref.getString(getActivity(), AppKeys.USER_DETAIL_LAST_NAME));
            txtMobNum.setText(Pref.getString(getActivity(), AppKeys.USER_DETAIL_MOBILE_NO));
            txtUserAddress.setText(Pref.getString(getActivity(), AppKeys.USER_DETAIL_ADDRESS_1));
            txtEmail.setText(Pref.getString(getActivity(), AppKeys.USER_DETAIL_EMAIL));
        }else {
            txtfullName.setText("Not Available");
            txtfullName.setVisibility(View.INVISIBLE);
            txtUserName.setText("Not Available");
            txtMobNum.setText(Pref.getString(getActivity(), AppKeys.USER_DETAIL_MOBILE_NO));
            txtUserAddress.setText("Not Available");
            txtEmail.setText(Pref.getString(getActivity(), AppKeys.USER_DETAIL_EMAIL));
        }

    }







    @Override
    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onResume() {
        super.onResume();
        ActivityMain.activeFragment = Constant.MY_PROFILE;
            AppController.getInstance().trackScreenView("Profile Screen");
    }

}
