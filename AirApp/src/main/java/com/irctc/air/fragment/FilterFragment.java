package com.irctc.air.fragment;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.appyvet.rangebar.RangeBar;
import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.R;
import com.irctc.air.adapter.FilterAirlineAdapter;
import com.irctc.air.header.AirHeader;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.model.FlightFilterBean;
import com.irctc.air.util.Constant;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.ProjectUtil;


/**
 * //Asif//
 * for filter in one way flight search
 *
 */
public class FilterFragment extends Fragment implements View.OnClickListener, RangeBar.OnRangeBarChangeListener {

    //( adultInt< 9-childInt)
    private ActivityMain mainActivity;

    private Button btnFilterApply;

    FlightFilterBean filterBean ;
    private LinearLayout lLStopsOption1,lLStopsOption2,lLStopsOption3, lLReset;
    private CheckBox zeroStopsCheckBox,oneStopsCheckBox,twoStopsMoreCheckBox;
    ListView filterPreferFlightsListView;
    FilterAirlineAdapter mAdapter;
    private RadioGroup filterRadioGpOnwardOption;
    RadioButton filterRadioOnwardOption_1,filterRadioOnwardOption_2,filterRadioOnwardOption_3,filterRadioOnwardOption_4;

    private CheckBox chBoxfilterOnward_1,chBoxfilterOnward_2,chBoxfilterOnward_3,chBoxfilterOnward_4;

    private CheckBox chBoxfilterReturn_1,chBoxfilterReturn_2,chBoxfilterReturn_3,chBoxfilterReturn_4;

    private RadioGroup filterRadioGpReturnOption ;
    RadioButton filterRadioReturnOption_1,filterRadioReturnOption_2,filterRadioReturnOption_3,filterRadioReturnOption_4;

    private RangeBar rangeBar;
    private TextView minFlightFare;
    private TextView maxFlightFare;
    private TextView minSelectedFlightFare;
    private TextView maxSelectedFlightFare;

    public FilterFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_filter, null);

        try {
            // Initialize Variable
            initializeVariable(view);

            // Header visibility
            AirHeader.showRecentSearchIcon(false);

            // Show header text
            AirHeader.showHeaderText(mainActivity, true, "Filters");

            // Show&Hide toggle and drawer and toolbar
            AirHeader.showDrawerToggleAndToolbar(false, false);

//            // Set data in variable from holder
//            setdataInVarFromHolder();

//            // Set default value in variable
            setdataInVarFromHolder();


        } catch (Exception e) {

            getActivity().finish();
        }

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        filterBean = new FlightFilterBean();
        mainActivity = (ActivityMain) activity;

    }


    private void initializeVariable(View view){


        lLReset = (LinearLayout) view.findViewById(R.id.FILTER_RESET_LAY);

        lLStopsOption1 = (LinearLayout) view.findViewById(R.id.filterLlFlightNumbers_1);
        lLStopsOption2 = (LinearLayout) view.findViewById(R.id.filterLlFlightNumbers_2);
        lLStopsOption3 = (LinearLayout) view.findViewById(R.id.filterLlFlightNumbers_3);

        zeroStopsCheckBox = (CheckBox) view.findViewById(R.id.filterCkBoxZeroStops);
        oneStopsCheckBox = (CheckBox) view.findViewById(R.id.filterCkBoxOneStops);
        twoStopsMoreCheckBox = (CheckBox) view.findViewById(R.id.filterCkBoxMoreStops);

        //filterRadioGpOnwardOption = (CheckBox) view.findViewById(R.id.filterRadioGpOnwardOption);

        chBoxfilterOnward_1 = (CheckBox) view.findViewById(R.id.chBoxfilterOnward_1);
        chBoxfilterOnward_2 = (CheckBox) view.findViewById(R.id.chBoxfilterOnward_2);
        chBoxfilterOnward_3 = (CheckBox) view.findViewById(R.id.chBoxfilterOnward_3);
        chBoxfilterOnward_4 = (CheckBox) view.findViewById(R.id.chBoxfilterOnward_4);




        minSelectedFlightFare = (TextView) view.findViewById(R.id.txtSelectedFlightFareMin);
        maxSelectedFlightFare = (TextView) view.findViewById(R.id.txtSelectedFlightFareMax);
        minFlightFare = (TextView) view.findViewById(R.id.txtFlightFareMin);
        maxFlightFare = (TextView) view.findViewById(R.id.txtFlightFareMax);

        rangeBar = (RangeBar) view.findViewById(R.id.rangebarPrice);

        rangeBar.setBarColor(Color.parseColor("#9DA2A3"));
        rangeBar.setPinColor(Color.parseColor("#21A1CE")); // change the background color of pin like drop
        rangeBar.setPinRadius(30);// change the radious of pin like drop
        rangeBar.setTickColor(Color.TRANSPARENT); // background color line
        rangeBar.setConnectingLineColor(Color.parseColor("#21A1CE"));
        rangeBar.setSelectorColor(Color.parseColor("#4F565E"));//
        rangeBar.setConnectingLineWeight(2);
        rangeBar.setOnRangeBarChangeListener(this);

        rangeBar.setPinTextFormatter(new RangeBar.PinTextFormatter() {
            @Override
            public String getText(String value) {
                return value;
            }
        });


        rangeBar.setTickEnd(AirDataHolder.getListHolder().getList().get(0).getOnwFlightFareMax());
        rangeBar.setTickStart(AirDataHolder.getListHolder().getList().get(0).getOnwFlightFareMin());
        rangeBar.setTickInterval(100);

//        rangeBar.setTickEnd(10000);
//        rangeBar.setTickStart(3000);
//        rangeBar.setTickInterval(200);

       //filterRadioGpReturnOption = (RadioGroup) view.findViewById(R.id.filterRadioGpReturnOption);

        chBoxfilterReturn_1 = (CheckBox) view.findViewById(R.id.chBoxfilterReturn_1);
        chBoxfilterReturn_2 = (CheckBox) view.findViewById(R.id.chBoxfilterReturn_2);
        chBoxfilterReturn_3 = (CheckBox) view.findViewById(R.id.chBoxfilterReturn_3);
        chBoxfilterReturn_4 = (CheckBox) view.findViewById(R.id.chBoxfilterReturn_4);

        filterPreferFlightsListView = (ListView) view.findViewById(R.id.filterPreferFlightsListView);

        btnFilterApply = (Button)view.findViewById(R.id.filterBtnApply);

        zeroStopsCheckBox.setOnClickListener(this);
        oneStopsCheckBox.setOnClickListener(this);
        twoStopsMoreCheckBox.setOnClickListener(this);


        lLStopsOption1.setOnClickListener(this);
        lLStopsOption2.setOnClickListener(this);
        lLStopsOption3.setOnClickListener(this);


        chBoxfilterOnward_1.setOnClickListener(this);
        chBoxfilterOnward_2.setOnClickListener(this);
        chBoxfilterOnward_3.setOnClickListener(this);
        chBoxfilterOnward_4.setOnClickListener(this);

        chBoxfilterReturn_1.setOnClickListener(this);
        chBoxfilterReturn_2.setOnClickListener(this);
        chBoxfilterReturn_3.setOnClickListener(this);
        chBoxfilterReturn_4.setOnClickListener(this);

        btnFilterApply.setOnClickListener(this);

        lLReset.setOnClickListener(this);

       // mAdapter = new DFilterAirlineAdapter(mainActivity,AirDataHolder.getListHolder().getList().get(0).getCheapestFlightFare()); //getAirlineData(airlinesImage,airlines,flightAirCode)
        filterPreferFlightsListView.setAdapter(mAdapter);

    }

    private void setdataInVarFromHolder() {

      //  mAdapter = new DFilterAirlineAdapter(mainActivity,AirDataHolder.getListHolder().getList().get(0).getCheapestFlightFare()); //getAirlineData(airlinesImage,airlines,flightAirCode)
        filterPreferFlightsListView.setAdapter(mAdapter);
        updateListViewHeight(filterPreferFlightsListView);

        minFlightFare.setText("\u20B9  " + String.valueOf(AirDataHolder.getListHolder().getList().get(0).getOnwFlightFareMin()));
        maxFlightFare.setText("\u20B9  " + String.valueOf(AirDataHolder.getListHolder().getList().get(0).getOnwFlightFareMax()));

    }


    public static void updateListViewHeight(ListView myListView) {
        ListAdapter myListAdapter = myListView.getAdapter();
        if (myListAdapter == null) {
            return;
        }
        //get listview height
        int totalHeight = 0;
        int adapterCount = myListAdapter.getCount();
        for (int size = 0; size < adapterCount ; size++) {
            View listItem = myListAdapter.getView(size, null, myListView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        //Change Height of ListView
        ViewGroup.LayoutParams params = myListView.getLayoutParams();
        params.height = totalHeight + (myListView.getDividerHeight() * (adapterCount - 1));
        myListView.setLayoutParams(params);
    }
    @Override
    public void onClick(View v) {

        switch (v.getId()){

        // for stops handeling
            case R.id.filterCkBoxZeroStops:

                if(zeroStopsCheckBox.isChecked()){
                    // set the checked  value here
                    filterBean.setStopZero(1);
                }else{
                    // set the unchecked value here
                    filterBean.setStopZero(0);
                }

                break;

            case R.id.filterCkBoxOneStops:
                if(oneStopsCheckBox.isChecked()){
                    // set the checked value here
                    filterBean.setStopOne(1);
                }else{
                    // set the unchecked value here
                    filterBean.setStopOne(0);
                }

                break;

            case R.id.filterCkBoxMoreStops:

                if(twoStopsMoreCheckBox.isChecked()){
                    // set the checked value here
                    filterBean.setStopTwoMore(1);
                   }else{
                    // set the unchecked value here
                    filterBean.setStopTwoMore(0);
                }

                break;

        // for onwardTime handeling
            case R.id.chBoxfilterOnward_1:

                if(chBoxfilterOnward_1.isChecked()){
                    // set the checked value here
                    filterBean.setOnwardOption1(1);
                }else{
                    // set the unchecked value here
                    filterBean.setOnwardOption1(0);
                }
                break;

            case R.id.chBoxfilterOnward_2:

                if(chBoxfilterOnward_2.isChecked()){
                    // set the checked value here
                    filterBean.setOnwardOption2(1);
                }else{
                    // set the unchecked value here
                    filterBean.setOnwardOption2(0);
                }

                break;
            case R.id.chBoxfilterOnward_3:

                if(chBoxfilterOnward_3.isChecked()){
                    // set the checked value here
                    filterBean.setOnwardOption3(1);
                }else{
                    // set the unchecked value here
                    filterBean.setOnwardOption3(0);
                }

                break;
            case R.id.chBoxfilterOnward_4:

                if(chBoxfilterOnward_4.isChecked()){
                    // set the checked value here
                    filterBean.setOnwardOption4(1);
                }else{
                    // set the unchecked value here
                    filterBean.setOnwardOption4(0);
                }

                break;

        // for returnTime handeling
            case R.id.chBoxfilterReturn_1:

                if(chBoxfilterReturn_1.isChecked()){
                    // set the checked value here
                    filterBean.setReturnOption1(1);
                }else{
                    // set the unchecked value here
                    filterBean.setReturnOption1(0);
                }

                break;
            case R.id.chBoxfilterReturn_2:

                if(chBoxfilterReturn_2.isChecked()){
                    // set the checked value here
                    filterBean.setReturnOption2(1);
                }else{
                    // set the unchecked value here
                    filterBean.setReturnOption2(0);
                }

                break;
            case R.id.chBoxfilterReturn_3:

                if(chBoxfilterReturn_3.isChecked()){
                    // set the checked value here
                    filterBean.setReturnOption3(1);
                }else{
                    // set the unchecked value here
                    filterBean.setReturnOption3(0);
                }

                break;
            case R.id.chBoxfilterReturn_4:

                if(chBoxfilterReturn_4.isChecked()){
                    // set the checked value here
                    filterBean.setReturnOption4(1);
                }else{
                    // set the unchecked value here
                    filterBean.setReturnOption4(0);
                }

                break;

            case R.id.filterBtnApply:

               /* mainActivity.isComingFromFilterFrag = true;
                filterBean.setPreferredAirline(getTheCheckedData());
                AirDataHolder.getListHolder().getList().get(0).setmFlightFilterBean(filterBean);
                mainActivity.lastActiveFragment = Constant.ONEWAY_FILTER_FRAGMENT;
                ProjectUtil.replaceFragment(mainActivity, new FragmentOneWayFlight(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
*/
                break;

            case R.id.FILTER_RESET_LAY:
                mainActivity.lastActiveFragment = Constant.ONEWAY_FILTER_FRAGMENT;
                ProjectUtil.replaceFragment(mainActivity, new FilterFragment(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

                break;

            default:
                break;
        }
    }

    /*private ArrayList<String> getTheCheckedData() {

        ArrayList<String> lAlAirline = new ArrayList<>();

        for (int i = 0; i< mAdapter.checkedItemList.size(); i++) {

            lAlAirline.add(mAdapter.checkedItemList.get(i).getmFlightCode());

        }
        return lAlAirline;
    }*/


    @Override
    public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {

        filterBean.setIsChange(1);
        filterBean.setMaxPrice(Integer.parseInt(rangeBar.getRightPinValue()));
        filterBean.setMinPrice(Integer.parseInt(rangeBar.getLeftPinValue()));
        minSelectedFlightFare.setText("\u20B9  " + Integer.parseInt(rangeBar.getLeftPinValue()));
        maxSelectedFlightFare.setText("\u20B9  " + Integer.parseInt(rangeBar.getRightPinValue()));

    }

    @Override
    public void onResume() {
        super.onResume();
        ActivityMain.activeFragment = Constant.ONEWAY_FILTER_FRAGMENT;
    }

}
