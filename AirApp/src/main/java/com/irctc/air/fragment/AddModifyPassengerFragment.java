package com.irctc.air.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.calendardatepicker.MonthAdapter;
import com.irctc.air.AppController;
import com.irctc.air.Database.AirDatabase;
import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.header.AirHeader;
import com.irctc.air.model.PassDetailbean;
import com.irctc.air.util.AlertDialogUtil;
import com.irctc.air.util.Constant;
import com.irctc.air.util.DateUtility;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.ProjectUtil;
import com.irctc.air.util.Validation;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by tourism on 6/14/2016.
 */
public class AddModifyPassengerFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    ActivityMain mainActivity;
    private int position;
    String comingFrom;

    private LinearLayout layPassType;
    private LinearLayout layAdult,txtAdultTab;
    private LinearLayout layChild,txtChildTab;
    private LinearLayout layInfant,txtInfantTab;

    // Header var
    TextView tvHeaderTitle;
    ImageView imgPassIcon,imgErrorIcon;


    /*Close by asif 17 Mar 2017*/
    //Spinner spinnerUserTitle;

     /*new Change accrding to IOS
     * by asif
     * 17 Mar 2017
     * */

    LinearLayout mLayMr;
    TextView tvMr;

    LinearLayout mLayMiss;
    TextView tvMiss;

    LinearLayout mLayMrs;
    TextView tvMrs;

    LinearLayout lLayDate = null;
    LinearLayout lLayPassFnameLname = null;
    TextView txtDateSelector;
    AutoCompleteTextView aTvLastName  = null;
    AutoCompleteTextView aTvFirstName = null;


    TextView txtMonthOfDateValue;
    TextView txtYearOfDateValue;
    TextView txtDayOfDateValue;
    Spinner spinnerUserNationalty;
    LinearLayout lLayInternationalPassDetail;

    TextView txtPassportNumber;

    LinearLayout lLayPassportIssue = null;
    TextView txtPassportIssueMonth;
    TextView txtPassportIssueDay;
    TextView txtPassportIssueYear;

    LinearLayout lLayPassportValid = null;
    TextView txtPassportValidMonth;
    TextView txtPassportValidDay;
    TextView txtPassportValidYear;

    private PassDetailbean passDetailbean;
    private FragmentActivity fragmantmanageCtx;
    private Button btnSubmitUpdate;
    private AirDatabase database;
    private LinearLayout layBtn;
    private LinearLayout layScroll;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mainActivity = (ActivityMain) activity;
        fragmantmanageCtx = (FragmentActivity)activity;
        passDetailbean = new PassDetailbean();

        if (getArguments().containsKey("commingFrom") && getArguments().containsKey("modifyAt")) {
            position = getArguments().getInt("modifyAt", 404);
            comingFrom = getArguments().getString("commingFrom", "ERROR");
        }
        else{
            comingFrom = "newPassenger";
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.modify_add_pass_fragment, null);


        // Initialize
        initializeVariable(view);

        // Header visibility
        AirHeader.showRecentSearchIcon(false);

        // Show header text
        AirHeader.showHeaderText(mainActivity, true, "Passenger Details");

        // Show&Hide toggle and drawer and toolbar
        AirHeader.showDrawerToggleAndToolbar(true, true);

        // Set data in variable from holder
        setdataInVarFromHolder();
        return view;
    }


    private void setdataInVarFromHolder() {

        if(comingFrom.equalsIgnoreCase("modify")){

            setDataInVariableFromDatabase();
            btnSubmitUpdate.setText("Update");

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 90.0f);
            layScroll.setLayoutParams(params);

            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 10.0f);
            layBtn.setLayoutParams(param);



        }else{
            // set the new passenger details
            btnSubmitUpdate.setText("Submit");
            passDetailbean.setPassFirstName("");
            passDetailbean.setPassLastName("");

            layPassType.setVisibility(View.VISIBLE);
            tvHeaderTitle.setText("Adult");
            imgPassIcon.setImageResource(R.drawable.adult);

            passDetailbean.setPassType("Adult");

            // 1 FOR ADULT TYPE
            showPassengerTitle(1);
            passDetailbean.setPassTitle("Mr");

//            spinnerUserTitle.setAdapter(ArrayAdapter.createFromResource(mainActivity, R.array.NEW_TITLE_ADULT_ARRAY, R.layout.spinner_item));
//            passDetailbean.setPassTitle((String) spinnerUserTitle.getAdapter().getItem(position));



            spinnerUserNationalty.setAdapter(ArrayAdapter.createFromResource(mainActivity, R.array.NATIONALITY_ARRAY, R.layout.spinner_item));

        }
    }

    private void setDataInBean() {

        passDetailbean.setPassFirstName(aTvFirstName.getText().toString());
        passDetailbean.setPassLastName(aTvLastName.getText().toString());
        passDetailbean.setPassPassportNum(txtPassportNumber.getText().toString());

//        passDetailbean.setPassTitle((String) spinnerUserTitle.getSelectedItem());
//        passDetailbean.setPassTitle((String) spinnerUserNationalty.getSelectedItem());



    }

    private void setDataInVariableFromDatabase() {


        database = new AirDatabase(mainActivity);
        database.open();
        ArrayList<PassDetailbean> lAlPassenger = database.getAllPassengerList();
        database.close();

        passDetailbean = lAlPassenger.get(position);

        /*Close by asif 17 Mar 2017*/

//        if(passDetailbean.getPassType().equalsIgnoreCase("Adult")){
//            spinnerUserTitle.setAdapter(ArrayAdapter.createFromResource(mainActivity, R.array.NEW_TITLE_ADULT_ARRAY, R.layout.spinner_item));
//        }else{
//            spinnerUserTitle.setAdapter(ArrayAdapter.createFromResource(mainActivity, R.array.NEW_TITLE_BABY_ARRAY, R.layout.spinner_item));
//        }



        /*new Change accrding to IOS
         * by asif
         * 17 Mar 2017
         * */

        if(passDetailbean.getPassType().equalsIgnoreCase("Adult")){

            showPassengerTitle(1);
        }else{

            showPassengerTitle(2);
        }


        spinnerUserNationalty.setAdapter(ArrayAdapter.createFromResource(mainActivity, R.array.NATIONALITY_ARRAY, R.layout.spinner_item));

        // here set  the title and passenger type from database

        if(passDetailbean.getPassType().equalsIgnoreCase("Adult")){

            if(passDetailbean.getPassTitle()!= null  &&  !passDetailbean.getPassTitle().equalsIgnoreCase("")) {

//                ArrayAdapter<CharSequence> adapter;
//                adapter = ArrayAdapter.createFromResource(mainActivity, R.array.NEW_TITLE_ADULT_ARRAY, R.layout .spinner_item);
//                int spinnerTitlePosition = adapter.getPosition(passDetailbean.getPassTitle());
//                spinnerUserTitle.setSelection(spinnerTitlePosition);

                if(passDetailbean.getPassTitle().equalsIgnoreCase("Mr")){
                    selctedPassTitle(1);

                }else if(passDetailbean.getPassTitle().equalsIgnoreCase("Miss")){
                    selctedPassTitle(2);

                }else if(passDetailbean.getPassTitle().equalsIgnoreCase("Mrs")){
                    selctedPassTitle(3);
                }


            }
            tvHeaderTitle.setText("Adult");
            imgPassIcon.setImageResource(R.drawable.adult);

        }else if(passDetailbean.getPassType().equalsIgnoreCase("Child")){

            if(passDetailbean.getPassTitle()!= null  &&  !passDetailbean.getPassTitle().equalsIgnoreCase("")) {

//                ArrayAdapter<CharSequence> adapter;
//                adapter = ArrayAdapter.createFromResource(mainActivity, R.array.NEW_TITLE_BABY_ARRAY, R.layout .spinner_item);
//                int spinnerTitlePosition = adapter.getPosition(passDetailbean.getPassTitle());
//                spinnerUserTitle.setSelection(spinnerTitlePosition);

                if(passDetailbean.getPassTitle().equalsIgnoreCase("Mr")){
                    selctedPassTitle(4);

                }else if(passDetailbean.getPassTitle().equalsIgnoreCase("Miss")){
                    selctedPassTitle(5);

                }

            }
            tvHeaderTitle.setText("Child");
            imgPassIcon.setImageResource(R.drawable.child);

        }else{

            if(passDetailbean.getPassTitle()!= null  &&  !passDetailbean.getPassTitle().equalsIgnoreCase("")) {

//                ArrayAdapter<CharSequence> adapter;
//                adapter = ArrayAdapter.createFromResource(mainActivity, R.array.NEW_TITLE_BABY_ARRAY, R.layout .spinner_item);
//                int spinnerTitlePosition = adapter.getPosition(passDetailbean.getPassTitle());
//                spinnerUserTitle.setSelection(spinnerTitlePosition);

                if(passDetailbean.getPassTitle().equalsIgnoreCase("Mr")){
                    selctedPassTitle(4);

                }else if(passDetailbean.getPassTitle().equalsIgnoreCase("Miss")){
                    selctedPassTitle(5);

                }

            }
            tvHeaderTitle.setText("Infant");
            imgPassIcon.setImageResource(R.drawable.infant);

        }

        // here set  the first and last name from database
        aTvFirstName.setText(passDetailbean.getPassFirstName());
        aTvLastName.setText(passDetailbean.getPassLastName());

        // here set  the passport number from database
        if(passDetailbean.getPassPassportNum() != null && !passDetailbean.getPassPassportNum().equalsIgnoreCase("")){
            txtPassportNumber.setText(passDetailbean.getPassPassportNum());
        }

        // SET DATE OF BIRTH
        if((passDetailbean.getPassAge() != null) && !(passDetailbean.getPassAge().toString().trim().equalsIgnoreCase(""))) {
            String dobDateArray[] = passDetailbean.getPassAge().split("/");

            txtMonthOfDateValue.setText((DateUtility.getSingleMonthName(Integer.parseInt(dobDateArray[1]))).substring(0,3).toUpperCase());
            txtDayOfDateValue.setText(dobDateArray[0]);
            txtYearOfDateValue.setText(dobDateArray[2]);
        }else{
            txtMonthOfDateValue.setText("MM");
            txtDayOfDateValue.setText("DD");
            txtYearOfDateValue.setText("YYYY");
        }


        // SET PASS PASSPORT ISSUE DATE
        if((passDetailbean.getPassPassportIssueDate() != null) && !(passDetailbean.getPassPassportIssueDate().toString().trim().equalsIgnoreCase(""))) {
            String dobDateArray[] = passDetailbean.getPassPassportIssueDate().split("/");

            txtPassportIssueMonth.setText((DateUtility.getSingleMonthName(Integer.parseInt(dobDateArray[1]))).substring(0,3).toUpperCase());
            txtPassportIssueDay.setText(dobDateArray[0]);
            txtPassportIssueYear.setText(dobDateArray[2]);
        }else{
            txtPassportIssueMonth.setText("MM");
            txtPassportIssueDay.setText("DD");
            txtPassportIssueYear.setText("YYYY");
        }

        // SET PASS PASSPORT VALID UPTO
        if((passDetailbean.getPassPassportValidUpto() != null) && !(passDetailbean.getPassPassportValidUpto().toString().trim().equalsIgnoreCase(""))) {
            String dobDateArray[] = passDetailbean.getPassPassportValidUpto().split("/");

            txtPassportValidMonth.setText((DateUtility.getSingleMonthName(Integer.parseInt(dobDateArray[1]))).substring(0,3).toUpperCase());
            txtPassportValidDay.setText(dobDateArray[0]);
            txtPassportValidYear.setText(dobDateArray[2]);
        }else{
            txtPassportValidMonth.setText("MM");
            txtPassportValidDay.setText("DD");
            txtPassportValidYear.setText("YYYY");
        }


        // here set the nationality from database

        if(passDetailbean.getPassNationalty()!= null  &&  !passDetailbean.getPassNationalty().equalsIgnoreCase("")) {
            ArrayAdapter<CharSequence> adapter;
            adapter = ArrayAdapter.createFromResource(mainActivity, R.array.NATIONALITY_ARRAY, R.layout .spinner_item);
            int spinnerNationalityPosition = adapter.getPosition(passDetailbean.getPassNationalty());
            spinnerUserNationalty.setSelection(spinnerNationalityPosition);
        }
    }


    private void selctedPassTitle(int buttonNum ){

        switch (buttonNum){

            case 1:

                mLayMr.setBackgroundResource(R.color.colorLightBlue);
                mLayMiss.setBackgroundResource(R.color.colorDARKGrey);
                mLayMrs.setBackgroundResource(R.color.colorDARKGrey);

                break;

            case 2:

                mLayMr.setBackgroundResource(R.color.colorDARKGrey);
                mLayMiss.setBackgroundResource(R.color.colorLightBlue);
                mLayMrs.setBackgroundResource(R.color.colorDARKGrey);

                break;

            case 3:

                mLayMr.setBackgroundResource(R.color.colorDARKGrey);
                mLayMiss.setBackgroundResource(R.color.colorDARKGrey);
                mLayMrs.setBackgroundResource(R.color.colorLightBlue);

                break;

            // FOR CHILD AND INFANT
            case 4:
                // MASTER CHILD OR INFANT
                mLayMr.setBackgroundResource(R.color.colorLightBlue);
                mLayMiss.setBackgroundResource(R.color.colorDARKGrey);

                break;

            case 5:
                // MISS CHILD OR INFANT
                mLayMr.setBackgroundResource(R.color.colorDARKGrey);
                mLayMiss.setBackgroundResource(R.color.colorLightBlue);

                break;

            default:
                break;
        }

    }

    private void initializeVariable(View view) {

        layScroll = (LinearLayout)view.findViewById(R.id.SCROLL_LAYOUT);
        layBtn = (LinearLayout)view.findViewById(R.id.BTN_LAYOUT);


        layPassType = (LinearLayout) view.findViewById(R.id.LAY_PASSENGER_TYPE);

        layAdult = (LinearLayout)view.findViewById(R.id.LAY_ADULT);
        layChild = (LinearLayout)view.findViewById(R.id.LAY_CHILD);
        layInfant = (LinearLayout)view.findViewById(R.id.LAY_INFANT);

        // TABs
        txtAdultTab = (LinearLayout)view.findViewById(R.id.LAY_ADULT_TAB);
        txtChildTab = (LinearLayout)view.findViewById(R.id.LAY_CHILD_TAB);
        txtInfantTab = (LinearLayout)view.findViewById(R.id.LAY_INFANT_TAB);


        // HEADER VAR
        tvHeaderTitle = (TextView) view.findViewById(R.id.TV_GROUP_HEADER);
        imgPassIcon = (ImageView) view.findViewById(R.id.IMG_GROUP_HEADER_ICON);
        imgErrorIcon = (ImageView) view.findViewById(R.id.IMG_ERROR_ICON);
        imgErrorIcon.setVisibility(View.INVISIBLE);

        // INNER VAR
        // ADULT TITLE VAR

         /*Close by asif 17 Mar 2017*/
       // spinnerUserTitle = (Spinner) view.findViewById(R.id.SPINNER_USER_ADULT);

        /*new Change accrding to IOS
         * by asif
         * 17 Mar 2017
         * */

        mLayMr = (LinearLayout) view.findViewById(R.id.LAY_MR);
        tvMr = (TextView) view.findViewById(R.id.TXT_MR);
        mLayMiss = (LinearLayout) view.findViewById(R.id.LAY_MISS);
        tvMiss = (TextView) view.findViewById(R.id.TXT_MISS);
        mLayMrs = (LinearLayout) view.findViewById(R.id.LAY_MRS);
        tvMrs = (TextView) view.findViewById(R.id.TXT_MRS);

        aTvLastName = (AutoCompleteTextView) view.findViewById(R.id.ET_LAST_NAME);
        aTvFirstName = (AutoCompleteTextView) view.findViewById(R.id.ET_FIRST_NAME);

        // DOB DATE SELECTOR
        lLayDate = (LinearLayout) view.findViewById(R.id.DATE_LAYOUT);
        lLayPassFnameLname = (LinearLayout) view.findViewById(R.id.PASS_INFO_LAYOUT);
        txtMonthOfDateValue = (TextView) view.findViewById(R.id.DOB_MONTH_DATE);
        txtYearOfDateValue = (TextView) view.findViewById(R.id.DOB_YEAR_DATE);
        txtDayOfDateValue = (TextView) view.findViewById(R.id.DOB_DAY_DATE);



        /****************************************************
         * For international user
         ****************************************************/
        lLayInternationalPassDetail = (LinearLayout) view.findViewById(R.id.LAY_INTERNATIONAL_PASS);
        spinnerUserNationalty = (Spinner) view.findViewById(R.id.SPINNER_USER_STATE);

        txtPassportNumber = (TextView) view.findViewById(R.id.ET_PASSPORT_NUMBER);

        lLayPassportIssue = (LinearLayout) view.findViewById(R.id.LAY_PASSPORT_ISSUE_DATE);
        txtPassportIssueMonth = (TextView) view.findViewById(R.id.PASSPORT_ISSUE_MONTH_DATE);
        txtPassportIssueDay = (TextView) view.findViewById(R.id.PASSPORT_ISSUE_DAY_DATE);
        txtPassportIssueYear = (TextView) view.findViewById(R.id.PASSPORT_ISSUE_YEAR_DATE);

        lLayPassportValid = (LinearLayout) view.findViewById(R.id.LAY_PASSPORT_VALID_DATE);
        txtPassportValidMonth = (TextView) view.findViewById(R.id.PASSPORT_VALID_UPTO_MONTH_DATE);
        txtPassportValidDay = (TextView) view.findViewById(R.id.PASSPORT_VALID_UPTO_DAY_DATE);
        txtPassportValidYear = (TextView) view.findViewById(R.id.PASSPORT_VALID_UPTO_YEAR_DATE);

        btnSubmitUpdate = (Button) view.findViewById(R.id.BTN_ADD_NEW_MODIFY_PASS);


        // Initialize on text change listener
        aTvFirstName.addTextChangedListener(firstNameTextWatcher);
        aTvLastName.addTextChangedListener(lastNameTextWatcher);
        txtPassportNumber.addTextChangedListener(passporNoTextWatcher);


        // Initialize on item click listener
        layAdult.setOnClickListener(this);
        layChild.setOnClickListener(this);
        layInfant.setOnClickListener(this);

        lLayDate.setOnClickListener(this);
        lLayPassportIssue.setOnClickListener(this);
        lLayPassportValid.setOnClickListener(this);

        /*Close by asif 17 Mar 2017*/
        //spinnerUserTitle.setOnItemSelectedListener(this);

        /*new Change accrding to IOS
         * by asif
         * 17 Mar 2017
         * */

        mLayMr.setOnClickListener(this);
        mLayMiss.setOnClickListener(this);
        mLayMrs.setOnClickListener(this);

        spinnerUserNationalty.setOnItemSelectedListener(this);
        btnSubmitUpdate.setOnClickListener(this);
    }

    TextWatcher passporNoTextWatcher = new TextWatcher(){

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            if(txtPassportNumber.getText() != null && txtPassportNumber.getText().equals("") ){

                passDetailbean.setPassPassportNum("");

            }else{
                passDetailbean.setPassPassportNum(txtPassportNumber.getText().toString());
            }
        }
    };

    TextWatcher lastNameTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            // Save data(Last name) in passenger holder

                String lStrValidationMsg = Validation.getInstance().getLastNameValidation(aTvLastName.getText().toString().trim());
                if (lStrValidationMsg.equalsIgnoreCase("SUCCESS")) {
                    aTvLastName.setError(null);
                } else {
                    aTvLastName.setError(lStrValidationMsg);

                }


            passDetailbean.setPassLastName(aTvLastName.getText().toString());
//
        }

    };

    TextWatcher firstNameTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {


                String lStrValidationMsg = Validation.getInstance().getFirstNameValidation(aTvFirstName.getText().toString().trim());
                if (lStrValidationMsg.equalsIgnoreCase("SUCCESS")) {
                    aTvFirstName.setError(null);
                } else {
                    aTvFirstName.setError(lStrValidationMsg);

                }

            // Save data(first name) in passenger holder
            passDetailbean.setPassFirstName(aTvFirstName.getText().toString());
        }
    };

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.LAY_MR:

                if( tvMr.getText().toString().equalsIgnoreCase("Mr")){

                    passDetailbean.setPassTitle("Mr");
                    selctedPassTitle(1);
                }else{
                    passDetailbean.setPassTitle("Master");
                    selctedPassTitle(4);
                }

                break;

            case R.id.LAY_MISS:

                if( tvMiss.getTag().toString().equalsIgnoreCase("CHILD")){

                    passDetailbean.setPassTitle("Miss");
                    selctedPassTitle(5);

                }else{

                    passDetailbean.setPassTitle("Miss");
                    selctedPassTitle(2);
                }

                break;

            case R.id.LAY_MRS:

                passDetailbean.setPassTitle("Mrs");
                selctedPassTitle(3);
                break;

            case R.id.LAY_ADULT:

                txtAdultTab.setBackgroundColor( getResources().getColor(R.color.colorLightBlue));
                txtChildTab.setBackgroundColor(getResources().getColor(R.color.colorDARKGrey));
                txtInfantTab.setBackgroundColor(getResources().getColor(R.color.colorDARKGrey));
                tvHeaderTitle.setText("Adult");
                imgPassIcon.setImageResource(R.drawable.adult);

                // 1 FOR ADULT TYPE
                showPassengerTitle(1);

//                ArrayAdapter<CharSequence> titleAdapter =ArrayAdapter.createFromResource(mainActivity, R.array.NEW_TITLE_ADULT_ARRAY, R.layout .spinner_item);
//                spinnerUserTitle.setAdapter(titleAdapter);

                passDetailbean.setPassType("Adult");

                break;

            case R.id.LAY_CHILD:

                txtChildTab.setBackgroundColor( getResources().getColor(R.color.colorLightBlue));
                txtAdultTab.setBackgroundColor(getResources().getColor(R.color.colorDARKGrey));
                txtInfantTab.setBackgroundColor(getResources().getColor(R.color.colorDARKGrey));
                tvHeaderTitle.setText("Child");
                imgPassIcon.setImageResource(R.drawable.child);

                // 2 FOR CHILD TYPE
                showPassengerTitle(2);


//                ArrayAdapter<CharSequence> titleAdapterChild = ArrayAdapter.createFromResource(mainActivity, R.array.NEW_TITLE_BABY_ARRAY, R.layout.spinner_item);
//                spinnerUserTitle.setAdapter(titleAdapterChild);

                passDetailbean.setPassType("Child");
                break;

            case R.id.LAY_INFANT:

                txtInfantTab.setBackgroundColor( getResources().getColor(R.color.colorLightBlue));
                txtAdultTab.setBackgroundColor(getResources().getColor(R.color.colorDARKGrey));
                txtChildTab.setBackgroundColor(getResources().getColor(R.color.colorDARKGrey));
                tvHeaderTitle.setText("Infant");
                imgPassIcon.setImageResource(R.drawable.infant);


                // 3 FOR INFANT TYPE
                showPassengerTitle(3);

//                ArrayAdapter<CharSequence> titleAdapterInifant = ArrayAdapter.createFromResource(mainActivity, R.array.NEW_TITLE_BABY_ARRAY, R.layout.spinner_item);
//                spinnerUserTitle.setAdapter(titleAdapterInifant);

                passDetailbean.setPassType("Infant");
                break;

            case R.id.LAY_PASSPORT_ISSUE_DATE:

                ///////////////////////////////////////////////////
                String pasBookinDate = "";

//                long date = System.currentTimeMillis();
//                pasBookinDate = DateUtility.currentTime(date);
                pasBookinDate = DateUtility.currentTime( System.currentTimeMillis());

                Date datee = DateUtility.getDate(pasBookinDate);

                Calendar passJornyDate = Calendar.getInstance();
                passJornyDate.setTime(datee);


                MonthAdapter.CalendarDay startday = new MonthAdapter.CalendarDay();
                startday.setDay(passJornyDate.get(Calendar.YEAR), passJornyDate.get(Calendar.MONTH), passJornyDate.get(Calendar.DAY_OF_MONTH));
                ///////////////////////////////////////////////////

                final CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment();

                cdp.setFirstDayOfWeek(Calendar.SUNDAY);
                cdp.setPreselectedDate(passJornyDate.get(Calendar.YEAR), passJornyDate.get(Calendar.MONTH), passJornyDate.get(Calendar.DAY_OF_MONTH));
                cdp.setDateRange(null, startday);
                cdp.setDoneText("Yes");
                cdp.setCancelText("No");
                cdp.setThemeDark(false);

                cdp.show(fragmantmanageCtx.getSupportFragmentManager(), "");

                cdp.setOnDismissListener(new CalendarDatePickerDialogFragment.OnDialogDismissListener() {
                    @Override
                    public void onDialogDismiss(DialogInterface dialoginterface) {

                        cdp.dismiss();
                    }
                });

                cdp.setOnDateSetListener(new CalendarDatePickerDialogFragment.OnDateSetListener() {
                    @Override
                    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {

                        passDetailbean.setPassPassportIssueDate(DateUtility.getSingleDateWithZero(dayOfMonth) + "/" + DateUtility.getSingleMonthWithZero(monthOfYear + 1) + "/" + year);
                        txtPassportIssueMonth.setText(DateUtility.getSingleMonthName(monthOfYear + 1).substring(0,3).toUpperCase());
                        txtPassportIssueDay.setText(DateUtility.getSingleDateWithZero(dayOfMonth));
                        txtPassportIssueYear.setText(String.valueOf(year));

                    }
                });

                break;

            case R.id.LAY_PASSPORT_VALID_DATE:

                ///////////////////////////////////////////////////
                String pasJourneyDate = "";

                Date endDate;
                if (mainActivity.isOneWaySelected) {
                    pasJourneyDate = AirDataHolder.getListHolder().getList().get(0).getDepDate();
                } else {
                    pasJourneyDate = AirDataHolder.getListHolder().getList().get(0).getReturnDate();
                }

                pasJourneyDate = DateUtility.currentTime( System.currentTimeMillis());



                Date datee2 = DateUtility.getDate(pasJourneyDate);

                Calendar passJornyDate2 = Calendar.getInstance();
                passJornyDate2.setTime(datee2);


                MonthAdapter.CalendarDay startday2 = new MonthAdapter.CalendarDay();
                startday2.setDay(passJornyDate2.get(Calendar.YEAR), passJornyDate2.get(Calendar.MONTH), passJornyDate2.get(Calendar.DAY_OF_MONTH));
                ///////////////////////////////////////////////////


                final CalendarDatePickerDialogFragment cdp2 = new CalendarDatePickerDialogFragment();

                cdp2.setFirstDayOfWeek(Calendar.SUNDAY);
                cdp2.setPreselectedDate(passJornyDate2.get(Calendar.YEAR), passJornyDate2.get(Calendar.MONTH), passJornyDate2.get(Calendar.DAY_OF_MONTH));
                cdp2.setDateRange(startday2, null);
                cdp2.setDoneText("Yes");
                cdp2.setCancelText("No");
                cdp2.setThemeDark(false);

                cdp2.show(fragmantmanageCtx.getSupportFragmentManager(), "");

                cdp2.setOnDismissListener(new CalendarDatePickerDialogFragment.OnDialogDismissListener() {
                    @Override
                    public void onDialogDismiss(DialogInterface dialoginterface) {

                        cdp2.dismiss();
                    }
                });

                cdp2.setOnDateSetListener(new CalendarDatePickerDialogFragment.OnDateSetListener() {
                    @Override
                    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {

                   passDetailbean.setPassPassportValidUpto(DateUtility.getSingleDateWithZero(dayOfMonth) + "/" + DateUtility.getSingleMonthWithZero(monthOfYear + 1) + "/" + year);
                   txtPassportValidMonth.setText(DateUtility.getSingleMonthName(monthOfYear + 1).substring(0, 3).toUpperCase());
                   txtPassportValidDay.setText(DateUtility.getSingleDateWithZero(dayOfMonth));
                   txtPassportValidYear.setText(String.valueOf(year));


                    }
                });

                break;

            // set DOB of Passenger
            case R.id.DATE_LAYOUT:

                //////////////////////////////////////////////////////////////////////////////////////
                String pasJourneyDate1 = "";

              //   Date endDate;
              //  Mon Jun 20 00:00:00 GMT+05:30 2016
                if(mainActivity.isOneWaySelected) {
                    pasJourneyDate1 = AirDataHolder.getListHolder().getList().get(0).getDepDate();
                }else{
                    pasJourneyDate1 = AirDataHolder.getListHolder().getList().get(0).getReturnDate();
                }

                pasJourneyDate1 = DateUtility.currentTime( System.currentTimeMillis());
//                if(mAlPassList.get(groupPosition).getPassType().contains("Adult")){
//                    endDate = DateUtility.getEndDate(pasJourneyDate, "Adult");
//                }else if(mAlPassList.get(groupPosition).getPassType().contains("Child")){
//                    endDate = DateUtility.getEndDate(pasJourneyDate, "Child");
//                }else{
//                    endDate = DateUtility.getEndDate(pasJourneyDate, "Infant");
//                }

///////////////////////////////////////////////////////////////////////////////
                Calendar endCalender = Calendar.getInstance();
                endCalender.setTime(DateUtility.getDate(pasJourneyDate1));


                if(passDetailbean.getPassType().contains("Adult")){
                    endCalender.add(Calendar.YEAR, -12);
                }else if(passDetailbean.getPassType().contains("Child")){
                    endCalender.add(Calendar.YEAR, -2);
                }else{
                    endCalender.add(Calendar.YEAR, 0);
                }
////////////////////////////////////////////////////////////////////////////////
                Date dateee = endCalender.getTime();

                Calendar endCal = Calendar.getInstance();
                endCal.setTime(dateee);
                MonthAdapter.CalendarDay endDay = new MonthAdapter.CalendarDay();
                endDay.setDay(endCal.get(Calendar.YEAR), endCal.get(Calendar.MONTH), endCal.get(Calendar.DAY_OF_MONTH));
                //endDay.setDay(2016, 12, 01);

                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                        int year       = calendar.get(Calendar.YEAR);
//                        int month      = calendar.get(Calendar.MONTH); // Jan = 0, dec = 11
//                        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
//                        int dayOfWeek  = calendar.get(Calendar.DAY_OF_WEEK);
//                        int weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);
//                        int weekOfMonth= calendar.get(Calendar.WEEK_OF_MONTH);
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


                //startday.setDay(2000, 12, 01);
                Calendar startCalender = Calendar.getInstance();
                startCalender.setTime(DateUtility.getDate(pasJourneyDate1));


                if(passDetailbean.getPassType().contains("Adult")){
                    startCalender.add(Calendar.YEAR, -100);
                }else if(passDetailbean.getPassType().contains("Child")){
                    startCalender.add(Calendar.YEAR, -12);
                }else{
                    startCalender.add(Calendar.YEAR, -2);
                }


                Date datee1 = startCalender.getTime();


                Calendar startCal = Calendar.getInstance();
                startCal.setTime(datee1);


                MonthAdapter.CalendarDay startday1 = new MonthAdapter.CalendarDay();
                startday1.setDay(startCal.get(Calendar.YEAR), startCal.get(Calendar.MONTH), startCal.get(Calendar.DAY_OF_MONTH));
                //////////////////////////////////////////////////////////////////////////////////////


                final CalendarDatePickerDialogFragment cdpDob = new CalendarDatePickerDialogFragment();
                cdpDob.setFirstDayOfWeek(Calendar.SUNDAY);
                cdpDob.setPreselectedDate(startCal.get(Calendar.YEAR), startCal.get(Calendar.MONTH), startCal.get(Calendar.DAY_OF_MONTH));
                cdpDob.setDateRange(startday1, endDay);
                cdpDob.setDoneText("Yes");
                cdpDob.setCancelText("No");
                cdpDob.setThemeDark(false);

                cdpDob.show(fragmantmanageCtx.getSupportFragmentManager(), "");

                cdpDob.setOnDismissListener(new CalendarDatePickerDialogFragment.OnDialogDismissListener() {
                    @Override
                    public void onDialogDismiss(DialogInterface dialoginterface) {

                        cdpDob.dismiss();
                    }
                });

                cdpDob.setOnDateSetListener(new CalendarDatePickerDialogFragment.OnDateSetListener() {
                    @Override
                    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {

                       // mAlPassList.get(groupPosition).setPassAge(DateUtility.getSingleDateWithZero(dayOfMonth) + "/" + DateUtility.getSingleMonthWithZero(monthOfYear + 1) + "/" + year);
                        txtMonthOfDateValue.setText(DateUtility.getSingleMonthName(monthOfYear + 1).substring(0, 3).toUpperCase());
                        txtDayOfDateValue.setText(DateUtility.getSingleDateWithZero(dayOfMonth));
                        txtYearOfDateValue.setText(String.valueOf(year));
                        passDetailbean.setPassAge(DateUtility.getSingleDateWithZero(dayOfMonth) + "/" + DateUtility.getSingleMonthWithZero(monthOfYear + 1) + "/" + year);
                    }
                });

                break;

            case R.id.BTN_ADD_NEW_MODIFY_PASS:

                String validMsg = null;

                validMsg = isValidToContinue();

                if(validMsg.equalsIgnoreCase("SUCCESS")){
                    // here insert the record in database

                    if(comingFrom.equalsIgnoreCase("modify")) {
                        // Update data in db

                        database = new AirDatabase(mainActivity);
                        database.open();
                        ArrayList<PassDetailbean> lAlPasseger = database.getAllPassengerList();
                        database.clearPassengerList();

                        lAlPasseger.remove(position);

                        // setDataInBean();
                        lAlPasseger.add(position, passDetailbean);
                        database.insertPassengerInDB(lAlPasseger);
                        database.close();


                    }else{

                        // Save data in db
                        ArrayList<PassDetailbean> lAlPassListBean = new ArrayList<>(1);
                        lAlPassListBean.add(passDetailbean);
                        database = new AirDatabase(mainActivity);
                        database.open();
                        database.insertPassengerInDB(lAlPassListBean);
                        database.close();
                    }

                    ProjectUtil.replaceFragment(mainActivity, new FragmentPassengerList(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);


                }else{
                    // show the error message

                    new AlertDialogUtil(mainActivity, validMsg, mainActivity.getResources().getString(R.string.ADD_PASS_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();

                }

                break;

            default:
                break;
        }
    }

    private void showPassengerTitle(int i) {

        if(i == 1){

            // HERE TO SET THE VALUE FOR ADULT
            mLayMrs.setVisibility(View.VISIBLE);

            tvMr.setText("Mr");
            tvMiss.setText("Miss");
            tvMrs.setText("Mrs");
            tvMiss.setTag("ADULT");

        }else{

            // HERE TO SET THE VALUE FOR CHILD & INFANT
            mLayMrs.setVisibility(View.INVISIBLE);
            passDetailbean.setPassTitle("Master");

            tvMr.setText("Master");
            tvMiss.setText("Miss");
            tvMiss.setTag("CHILD");
        }


    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        switch (parent.getId()){

            case R.id.SPINNER_USER_ADULT:

//                passDetailbean.setPassTitle((String) spinnerUserTitle.getAdapter().getItem(position));
//
//                TextView selectedText = (TextView) parent.getChildAt(0);
//                if (selectedText != null) {
//                    selectedText.setTextColor(mainActivity.getResources().getColor(R.color.colorPrimary));
//                }
                break;

            case R.id.SPINNER_USER_STATE:

                passDetailbean.setPassNationalty((String) spinnerUserNationalty.getAdapter().getItem(position));

                break;


            default:
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

        switch (parent.getId()) {

            case R.id.SPINNER_USER_ADULT:
               // Toast.makeText(getActivity(),"Please choose Title",Toast.LENGTH_SHORT).show();
                break;

            case R.id.SPINNER_USER_STATE:
                break;
        }
    }



    public String isValidToContinue(){

        String resultMsg = "Invalid details.";

        String firstNameValidationres = Validation.getInstance().getFirstNameValidation(passDetailbean.getPassFirstName());
        String lastNameValidationres = Validation.getInstance().getLastNameValidation(passDetailbean.getPassLastName());

        if(passDetailbean.getPassTitle().equalsIgnoreCase("Select Title")) {
            resultMsg = "Please select Title.";

        } else if(!firstNameValidationres.equalsIgnoreCase("SUCCESS")){
            resultMsg = firstNameValidationres;

        }else if(!lastNameValidationres.equalsIgnoreCase("SUCCESS")){
            resultMsg = lastNameValidationres;

        }else if(passDetailbean.getPassFirstName().toString().trim().equalsIgnoreCase(passDetailbean.getPassLastName().toString().trim())){
            resultMsg = "First name and last name cannot be same.";

        }else{
            resultMsg = "SUCCESS";
        }


        if( !passDetailbean.getPassPassportNum().equalsIgnoreCase("") ||
            !passDetailbean.getPassPassportIssueDate().equalsIgnoreCase("") ||
            !passDetailbean.getPassPassportValidUpto().equalsIgnoreCase("") ||
            !passDetailbean.getPassNationalty().equalsIgnoreCase("Select nationality")

            )
        {
            if(passDetailbean.getPassTitle().equalsIgnoreCase("Select Title")) {
                resultMsg = "Please select Title.";

            } else if(!firstNameValidationres.equalsIgnoreCase("SUCCESS")){
                resultMsg = firstNameValidationres;

            }else if(!lastNameValidationres.equalsIgnoreCase("SUCCESS")){
                resultMsg = lastNameValidationres;

            }else if(passDetailbean.getPassFirstName().toString().trim().equalsIgnoreCase(passDetailbean.getPassLastName().toString().trim())){
                resultMsg = "First name and last name cannot be same.";

            }else if(passDetailbean.getPassAge().equalsIgnoreCase("")){
                resultMsg = "Please select Date of Birth";
            }else if (passDetailbean.getPassPassportNum().equalsIgnoreCase("")) {
                resultMsg = "Please provide passenger passport number";
            } else if (passDetailbean.getPassPassportIssueDate().equalsIgnoreCase("")) {
                resultMsg = "Please provide passenger passport issue date";
            } else if (passDetailbean.getPassPassportValidUpto().equalsIgnoreCase("")) {
                resultMsg = "Please provide passenger passport valid upto date";
            } else if (passDetailbean.getPassNationalty().equalsIgnoreCase("Select nationality")) {
                resultMsg = "Please select nationality.";

            }else if((!DateUtility.convertDate(passDetailbean.getPassAge()).before(DateUtility.convertDate(passDetailbean.getPassPassportIssueDate()))) ||
                    (!DateUtility.convertDate(passDetailbean.getPassAge()).before(DateUtility.convertDate(passDetailbean.getPassPassportValidUpto()))) ){
                resultMsg = "Date of birth should not be greater than Passport Issue Date and Passport valid Date";
            }else if(!DateUtility.convertDate(passDetailbean.getPassPassportIssueDate()).before(DateUtility.convertDate(passDetailbean.getPassPassportValidUpto()))) {
                resultMsg = "Passport Issue Date should not be greater than Passport valid Date";
            }else {
                resultMsg = "SUCCESS";
            }
        }

        return resultMsg;
    }

    @Override
    public void onResume() {
        super.onResume();
        ActivityMain.activeFragment = Constant.ADD_MODIFY_PASS_FRAGMENT;
        AppController.getInstance().trackScreenView("Modify Passenger Screen");
    }
}
