package com.irctc.air.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.irctc.air.AppController;
import com.irctc.air.Database.AirDatabase;
import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.Dataholder.PassDataHolder;
import com.irctc.air.R;
import com.irctc.air.adapter.PassengerListAdapter;
import com.irctc.air.header.AirHeader;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.model.PassDetailbean;
import com.irctc.air.model.book_ticket.PassengerDetails;
import com.irctc.air.util.AlertDialogUtil;
import com.irctc.air.util.Constant;
import com.irctc.air.util.DateUtility;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.PassengerFirstNameSort;
import com.irctc.air.util.ProjectUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

/**
 * Created by vivek on 4/26/2016.
 * This class is used for showing side passenger list to add multiple passenger by selecting checkbox.
 */
public class FragmentPassengerList extends Fragment {

    private ArrayList<PassengerDetails> mAlPassListHolder;
    private ActivityMain mainActivity;
    private ListView mlvPassList;
    private LinearLayout mbtnDone;
    private LinearLayout mbtnAddNewPass;
    AirDatabase database;
    private LinearLayout mlvPassListErrorMsg;
    private LinearLayout mLinearLayoutPassList;

    //private int totalPass = 0;
    private static int adultCounter = 0;
    private static int childCounter = 0;
    private static int infantCounter = 0;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mainActivity = (ActivityMain) activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_passenger_list, null);

        // Initialize variable
        initiatizaVar(view);

        // get value from DB
        getPassListFromDB();

        // Set values in var
        setValuesInVariables(view);

        // Header visibility
        AirHeader.showRecentSearchIcon(false);

        // Show header text
        AirHeader.showHeaderText(mainActivity, true, "");

        // Show&Hide toggle and drawer and toolbar
        AirHeader.showDrawerToggleAndToolbar(false, false);

        return view;
    }


    private void initiatizaVar(View view) {

        database = new AirDatabase(mainActivity);

        //totalPass = AirDataHolder.getListHolder().getList().get(0).getAdultPassNum()+AirDataHolder.getListHolder().getList().get(0).getChildPassNum()+AirDataHolder.getListHolder().getList().get(0).getInfantPassNum();
        adultCounter = AirDataHolder.getListHolder().getList().get(0).getAdultPassNum();
        childCounter = AirDataHolder.getListHolder().getList().get(0).getChildPassNum();
        infantCounter = AirDataHolder.getListHolder().getList().get(0).getInfantPassNum();


        // Set PASS data in LV thru DB
        mlvPassList = (ListView) view.findViewById(R.id.PASS_LIST_FROM_DB);
        mlvPassListErrorMsg = (LinearLayout) view.findViewById(R.id.LAY_ERROR_MESSAGE);
        mLinearLayoutPassList = (LinearLayout) view.findViewById(R.id.PASS_LAY);


    }


    private void setValuesInVariables(View view) {

        if (mAlPassListHolder.size() > 0) {
            mLinearLayoutPassList.setVisibility(View.VISIBLE);
            mlvPassListErrorMsg.setVisibility(View.GONE);

            isPassegerTypeChange();
            mlvPassList.setAdapter(new PassengerListAdapter(mainActivity, mAlPassListHolder));

            Collections.sort(mAlPassListHolder, new PassengerFirstNameSort());
            mlvPassList.invalidateViews();

        } else {
            mLinearLayoutPassList.setVisibility(View.GONE);
            mlvPassListErrorMsg.setVisibility(View.VISIBLE);
        }

        //mlvPassList.setOnItemClickListener(new ListItemClickListener(mainActivity, ListEventHandler.MULTI_PASS_SELECTION_LIST, AirDataHolder.getListHolder().getList(), mAlPassListBean, 0));

//        mlvPassList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//
//                if(mAlPassListBean.get(position).getPassType().equalsIgnoreCase("Adult"))
//                {
//                    if(adultCounter > 0){
//                        adultCounter --;
//                        if(mAlPassListBean.get(position).isSelected()){
//                            mAlPassListBean.get(position).setIsSelected(false);
//                        }else{
//                            mAlPassListBean.get(position).setIsSelected(true);
//                        }
//                    }else{
//                        Toast.makeText(mainActivity, "Adult max limit", Toast.LENGTH_LONG).show();
//                    }
//                }else if(mAlPassListBean.get(position).getPassType().equalsIgnoreCase("Child"))
//                {
//                    if(childCounter > 0){
//                        childCounter --;
//                        if(mAlPassListBean.get(position).isSelected()){
//                            mAlPassListBean.get(position).setIsSelected(false);
//                        }else{
//                            mAlPassListBean.get(position).setIsSelected(true);
//                        }
//                    }else{
//                        Toast.makeText(mainActivity, "Adult max limit", Toast.LENGTH_LONG).show();
//                    }
//                }else if(mAlPassListBean.get(position).getPassType().equalsIgnoreCase("Infant"))
//                {
//                    if(infantCounter > 0){
//                        infantCounter --;
//                        if(mAlPassListBean.get(position).isSelected()){
//                            mAlPassListBean.get(position).setIsSelected(false);
//                        }else{
//                            mAlPassListBean.get(position).setIsSelected(true);
//                        }
//                    }else{
//                        Toast.makeText(mainActivity, "Adult max limit", Toast.LENGTH_LONG).show();
//                    }
//                }
//
//            }
//        });

        mbtnAddNewPass = (LinearLayout) view.findViewById(R.id.BTN_ADD_NEW_PASS);
        // mbtnAddNewPass.setVisibility(View.INVISIBLE);

        mbtnAddNewPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
/*
                Fragment addModifyPassengerFragment = new AddModifyPassengerFragment();
                Bundle bundle = new Bundle();
                bundle.putString("commingFrom", "newPassenger");
                addModifyPassengerFragment.setArguments(bundle);
                ProjectUtil.replaceFragment(mainActivity, addModifyPassengerFragment, R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
        */
                PassengerListAdapter.selectedPassengers = null;
                getActivity().onBackPressed();
            }
        });


        mbtnDone = (LinearLayout) view.findViewById(R.id.BTN_ADD_PASS);
        mbtnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
/*

                int selectedAdultCount = 0;
                int selectedChildCount = 0;
                int selectedInfantCount = 0;

                // GET SELECTED PASS COUNT
*/
/*
                for (int m = 0; m < mAlPassListHolder.size(); m++) {
                    if (mAlPassListHolder.get(m).isSelected()) {
                        if(mAlPassListHolder.get(m).getType().contains("Adult")){
                            selectedAdultCount++;
                        }else if(mAlPassListHolder.get(m).getPassType().contains("Child")){
                            selectedChildCount++;
                        }else if(mAlPassListHolder.get(m).getPassType().contains("Infant")){
                            selectedInfantCount++;
                        }

                    }
                }
*//*



                String errorMessage = "";

                StringBuilder message = new StringBuilder();
                message.append("You have opted for "+(adultCounter+childCounter+infantCounter)+" travellers. \nTravellers can be ");

                if(adultCounter > 0 ){
                    if(adultCounter == 1 ){
                        message.append(adultCounter+" Adult ");
                    }else{
                        message.append(adultCounter+" Adults ");
                    }
                }
                if(childCounter > 0 ){
                    if(childCounter == 1 ){
                        message.append(childCounter+" Child ");
                    }else{
                        message.append(childCounter+" Children ");
                    }
                }
                if(infantCounter > 0 ){
                    if(infantCounter == 1 ){
                        message.append(infantCounter+" Infant ");
                    }else{
                        message.append(infantCounter+" Infants ");
                    }
                }


                    if((selectedAdultCount+ selectedChildCount+selectedInfantCount) > (childCounter+adultCounter+infantCounter)) {
                      new AlertDialogUtil(mainActivity, message.toString()+". Kindly select right combination.", mainActivity.getResources().getString(R.string.ADD_PASS_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();

                    }else if(selectedAdultCount > adultCounter){
                        if(adultCounter == 0){
                            errorMessage = "You cannot select adult.";
                        }else{
                            errorMessage = "You can only select "+adultCounter+" adults.";
                        }
                         new AlertDialogUtil(mainActivity, errorMessage, mainActivity.getResources().getString(R.string.ADD_PASS_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                    }else if(selectedChildCount > childCounter){
                        if(childCounter == 0){
                            errorMessage = "You cannot select child.";
                        }else{
                            errorMessage = "You can only select "+adultCounter+" child.";
                        }
                        new AlertDialogUtil(mainActivity, errorMessage, mainActivity.getResources().getString(R.string.ADD_PASS_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                    }else if(selectedInfantCount > infantCounter){
                        if(infantCounter == 0){
                            errorMessage = "You cannot select infant.";
                        }else{
                            errorMessage = "You can only select "+adultCounter+" child.";
                        }
                        new AlertDialogUtil(mainActivity, errorMessage, mainActivity.getResources().getString(R.string.ADD_PASS_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                    }else {

                        ArrayList<PassDetailbean> lObjPassHolderMain = PassDataHolder.getListHolder().getList();
                        // Get data from PassListDataHolder(Side pass list holder) and populate in PassDataHolder(Pass fragment main holder)mAlPassListHolder

                        // PASSENGER LIST LIST
*/
/*                        for (int i = 0; i < mAlPassListHolder.size(); i++) {

                            if (mAlPassListHolder.get(i).isSelected()) {

                                // PASS PLANNER LIST
                                for (int j = 0; j < lObjPassHolderMain.size(); j++) {

                                    if (mAlPassListHolder.get(i).getPassType().contains("Adult") && lObjPassHolderMain.get(j).getPassType().contains("Adult")) {
                                        if (lObjPassHolderMain.get(j).getPassFirstName().equalsIgnoreCase("") && lObjPassHolderMain.get(j).getPassLastName().equalsIgnoreCase("")) {
//                                    lObjPassHolderMain.remove(j);
//                                    lObjPassHolderMain.add(mAlPassListHolder.get(i));
                                            lObjPassHolderMain.set(j, mAlPassListHolder.get(i));
                                            break;
                                        }
                                    } else if (mAlPassListHolder.get(i).getPassType().contains("Child") && lObjPassHolderMain.get(j).getPassType().contains("Child")) {
                                        if (lObjPassHolderMain.get(j).getPassFirstName().equalsIgnoreCase("") && lObjPassHolderMain.get(j).getPassLastName().equalsIgnoreCase("")) {
//                                    lObjPassHolderMain.remove(j);
//                                    lObjPassHolderMain.add(mAlPassListHolder.get(i));
                                            lObjPassHolderMain.set(j, mAlPassListHolder.get(i));
                                            break;
                                        }
                                    } else if (mAlPassListHolder.get(i).getPassType().contains("Infant") && lObjPassHolderMain.get(j).getPassType().contains("Infant")) {
                                        if (lObjPassHolderMain.get(j).getPassFirstName().equalsIgnoreCase("") && lObjPassHolderMain.get(j).getPassLastName().equalsIgnoreCase("")) {
//                                    lObjPassHolderMain.remove(j);
//                                    lObjPassHolderMain.add(mAlPassListHolder.get(i));
                                            lObjPassHolderMain.set(j, mAlPassListHolder.get(i));
                                            break;
                                        }
                                    }
                                }
                            }
                        }*//*


                        // Send to pass page
                        mainActivity.isComingFromPassengerListPage = true;
                        mainActivity.lastActiveFragment = Constant.PASS_SIDE_LIST_FRAGMENT;
                        ProjectUtil.replaceFragment(mainActivity, new FragmentAddPassengers(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);


                    }
*/

            }
        });

    }

    private void isPassegerTypeChange() {

        String pasJourneyDate = null;

        if (mainActivity.isOneWaySelected) {
            pasJourneyDate = AirDataHolder.getListHolder().getList().get(0).getDepDate();
        } else {
            pasJourneyDate = AirDataHolder.getListHolder().getList().get(0).getReturnDate();
        }

        Date journyDate = DateUtility.getDateInFormat(pasJourneyDate);

/*        for (int i = 0; i < mAlPassListHolder.size(); i++) {

            PassengerDetails passBean = mAlPassListHolder.get(i);

            if(!passBean.getPassType().equalsIgnoreCase("Adult")){

                if(passBean.getPassType().equalsIgnoreCase("Child")){

                    Date childDob = DateUtility.convertDate(passBean.getPassAge());

                    Calendar childCalender = Calendar.getInstance();
                    childCalender.setTime(childDob);
                    childCalender.add(Calendar.YEAR, 12);
                    Date after12Years = childCalender.getTime();

                    //childCalender.setTime(DateUtility.getDate(pasJourneyDate));

                    //after12Years.before(journyDate);

                    if(journyDate.before(after12Years)){
                        // Do Nothing here
                    }else{
                        // Update the Passenger Type in List Holder
                        passBean.setPassType("Adult");

                        if(passBean.getPassTitle().equalsIgnoreCase("Master")){
                            passBean.setPassTitle("Mr");
                        }else{
                            passBean.setPassTitle("Miss");
                        }
                    }

                }else{

                    Date infantDob = DateUtility.convertDate(passBean.getPassAge());


                    Calendar infantCalender = Calendar.getInstance();
                    infantCalender.setTime(infantDob);
                    infantCalender.add(Calendar.YEAR, 2);
                    Date after2Years = infantCalender.getTime();

                    //childCalender.setTime(DateUtility.getDate(pasJourneyDate));

                    ///after2Years.before(journyDate);

                    if(journyDate.before(after2Years)){
                        // Do Nothing here
                    }else{
                        // Update the Passenger Type in List Holder
                        passBean.setPassType("Child");
                    }
                }
            }
        }*/
    }


    private void getPassListFromDB() {

        // Set value from db to secong passenger list holder (mAlPassListBean)
        //PassListDataHolder.getListHolder().getList().clear();

        // Get list from holder and set in db
        // database.open();
        //PassListDataHolder.getListHolder().setList(database.getAllPassengerList());
        // database.close();

        mAlPassListHolder = new AirDatabase(getActivity().getApplicationContext()).getPassengerList();
    }


    @Override
    public void onResume() {
        super.onResume();
        ActivityMain.activeFragment = Constant.PASS_SIDE_LIST_FRAGMENT;
        AppController.getInstance().trackScreenView("Passenger List Screen");
    }

}
