package com.irctc.air.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.internal.Excluder;
import com.irctc.air.AppController;
import com.irctc.air.Database.AirDatabase;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.activity.WebViewActivity;
import com.irctc.air.adapter.PassengerListOneWayAdapter;
import com.irctc.air.adapter.TicketOneWayAdapter;
import com.irctc.air.header.AirHeader;
import com.irctc.air.model.passenger_list.ModelPassengerListItem;
import com.irctc.air.model.route_list.ModelRoutListItem;
import com.irctc.air.model.ticket.LstIrFbFareDetail;
import com.irctc.air.model.ticket.Passengers;
import com.irctc.air.model.ticket.PojoTicket;
import com.irctc.air.model.ticket.TicketInfo;
import com.irctc.air.round.trip.domastic.fragment.DFragmentAddPassengers;
import com.irctc.air.round.trip.domastic.model.LstBaggageDetails;
import com.irctc.air.util.ListViewInsideScrollView;
import com.irctc.air.util.NetworkingUtils;
import com.irctc.air.networking.Networking;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by asif on 4/30/2016.
 */
public class FragmentTicket extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_ticket, null);
        TextView toolbar = (TextView) view.findViewById(R.id.toolbar);
        toolbar.setText("Ticket");
        AirHeader.showRecentSearchIcon(false);
        AirHeader.showHeaderText((ActivityMain) getActivity(), true, "Ticket");
        AirHeader.showDrawerToggleAndToolbar(false, false);
        NetworkingUtils.showProgress(getActivity());
        AirDatabase airDatabase = new AirDatabase(getActivity().getApplicationContext());
        String authToken = airDatabase.getAuthToken();
        Networking.checkTransactionStatus(authToken, FragmentAddPassengers.transactionId,
                //"5999107453",
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ActivityMain.refreshPlanerFragment = 1;
                        Log.e("RESPONSE", response.toString());
                        NetworkingUtils.dismissProgress();
                        Gson gson = new Gson();
                        PojoTicket pojoTicket = gson.fromJson(response.toString(), PojoTicket.class);
                        if (pojoTicket.getStatus().equals("SUCCESS")) {
                            ((TextView) view.findViewById(R.id.ticketBookingStatus)).setText(pojoTicket.getData().getStatus());
                            ((TextView) view.findViewById(R.id.transactionId)).setText(FragmentAddPassengers.transactionId);
                            /*((TextView) view.findViewById(R.id.sourceStationCode)).setText(pojoTicket.getData().getTicketInfo()[0].getOrigin());
                            ((TextView) view.findViewById(R.id.destinationStationCode)).setText(pojoTicket.getData().getTicketInfo()[0].getDestination());*/
                            ((TextView) view.findViewById(R.id.ticketPnr)).setText(pojoTicket.getData().getTicketInfo()[0].getPnr());
                            TicketInfo[] ticketInfo = pojoTicket.getData().getTicketInfo();
                            ArrayList<ModelRoutListItem> arrayListRouteList = new ArrayList<>();
                            LstBaggageDetails lstBaggageDetails = new LstBaggageDetails();
                            if (pojoTicket.getData().getLstBaggageDetails() !=null){
                                for (int i = 0; i < pojoTicket.getData().getLstBaggageDetails().length; i++) {
                                    if (pojoTicket.getData().getLstBaggageDetails()[i].getPaxType().equalsIgnoreCase("ADT")){
                                        lstBaggageDetails = pojoTicket.getData().getLstBaggageDetails()[i];
                                        break;
                                    }
                                }
                            }


                            for (int i = 0; i < ticketInfo.length; i++) {
                                ModelRoutListItem modelRoutListItem = new ModelRoutListItem();
                                String flightCode = ticketInfo[i].getAirline() + "-" + ticketInfo[i].getFlightNumber();
                                String fromAirportCode = ticketInfo[i].getOrigin();
                                String fromAirportName = ticketInfo[i].getOriginCity();
                                String fromAirportTerminal = "";
                                if (ticketInfo[i].getOriginAirport().contains("Terminal")) {
                                    fromAirportTerminal = "Terminal " + ticketInfo[i].getOriginAirport().split("Terminal")[1];
                                }
                                String toAirportCode = ticketInfo[i].getDestination();
                                String toAirportName = ticketInfo[i].getDestinationCity();
                                String toAirportTerminal = "";
                                if (ticketInfo[i].getDestinationAirport().contains("Terminal")) {
                                    toAirportTerminal = "Terminal " + ticketInfo[i].getDestinationAirport().split("Terminal")[1];
                                }
                                String carrear = ticketInfo[i].getAirline();
                                String travelDuration = ticketInfo[i].getDuration();
                                String departureDate = ticketInfo[i].getDepartureTime();
                                String arrivalDate = ticketInfo[i].getArrivalTime();
                                ArrayList<ModelPassengerListItem> arrayListPassengers = new ArrayList<>();
                                Passengers[] passengersList = ticketInfo[i].getPassengers();
                                for (int j = 0; j < passengersList.length; j++) {
                                    ModelPassengerListItem modelPassengerListItem = new ModelPassengerListItem();
                                    modelPassengerListItem.setFirstName(passengersList[j].getFirstName());
                                    modelPassengerListItem.setLastName(passengersList[j].getLastName());
                                    modelPassengerListItem.setGender(passengersList[j].getTitle());
                                    modelPassengerListItem.setType(passengersList[j].getPaxType());
                                    modelPassengerListItem.setDeliveryStatus(passengersList[j].getStatus());
                                    arrayListPassengers.add(modelPassengerListItem);
                                }
                                modelRoutListItem.setFreeMeal(ticketInfo[i].isFreeMeal());
                                modelRoutListItem.setServiceProvider(pojoTicket.getData().getServiceProvider());
                                modelRoutListItem.setLstBaggageDetails(lstBaggageDetails);
                                modelRoutListItem.setCarrear(carrear);
                                modelRoutListItem.setPassengersList(arrayListPassengers);
                                modelRoutListItem.setFlightCode(flightCode);
                                modelRoutListItem.setFromAirportCode(fromAirportCode);
                                modelRoutListItem.setFromAirportName(fromAirportName);
                                modelRoutListItem.setFromAirportTerminal(fromAirportTerminal);
                                modelRoutListItem.setToAirportCode(toAirportCode);
                                modelRoutListItem.setToAirportName(toAirportName);
                                modelRoutListItem.setToAirportTerminal(toAirportTerminal);
                                modelRoutListItem.setTravelDuration(travelDuration);
                                DateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
                                DateFormat formatter2 = new SimpleDateFormat("HH:mm");

                                Calendar calendar = Calendar.getInstance();
                                calendar.setTimeInMillis(Long.parseLong(departureDate));
                                modelRoutListItem.setDepartureDate(formatter.format(calendar.getTime()));
                                modelRoutListItem.setDepartureTime(formatter2.format(calendar.getTime()));
                                calendar.setTimeInMillis(Long.parseLong(arrivalDate));
                                modelRoutListItem.setArrivalDate(formatter.format(calendar.getTime()));
                                modelRoutListItem.setArrivalTime(formatter2.format(calendar.getTime()));
                                modelRoutListItem.setArrivalTimeAnd(ticketInfo[i].getArrivalTimeAnd());
                                modelRoutListItem.setDepartureTimeAnd(ticketInfo[i].getDepartureTimeAnd());
                                //modelRoutListItem.setHalt(halt);
                                arrayListRouteList.add(modelRoutListItem);
                            }

                            String originCode = arrayListRouteList.get(0).getFromAirportCode();
                            String destinationCode = arrayListRouteList.get(arrayListRouteList.size() - 1).getToAirportCode();


                            ((TextView) view.findViewById(R.id.sourceStationCode)).setText(originCode);
                            ((TextView) view.findViewById(R.id.destinationStationCode)).setText(destinationCode);
                            ((TextView) view.findViewById(R.id.flightClass)).setText("(" + FragmentPlanner.flightTravellClass + ")");
                            ((TextView) view.findViewById(R.id.flightClassReturn)).setText("(" + FragmentPlanner.flightTravellClass + ")");


                            ((ListView) view.findViewById(R.id.listView)).setAdapter(new TicketOneWayAdapter(getActivity(), arrayListRouteList));
                            ListViewInsideScrollView.open(((ListView) view.findViewById(R.id.listView)));

                            ((ListView) view.findViewById(R.id.lvPassenger)).setAdapter(new PassengerListOneWayAdapter(getContext(), arrayListRouteList.get(0).getPassengersList()));
                            ListViewInsideScrollView.open(((ListView) view.findViewById(R.id.lvPassenger)));

                            LstIrFbFareDetail[] fareDetails = pojoTicket.getData().getLstIrFbFareDetail();
                            float basefare = 0.0f, totalTax = 0.0f, subTotal = 0.0f, irctcBookingCharges = 0.0f;
                            for (int i = 0; i < fareDetails.length; i++) {
                                basefare = basefare + Float.parseFloat(fareDetails[i].getBaseFare());
                                totalTax = totalTax + Float.parseFloat(fareDetails[i].getTotalTax());
                                try {
                                    irctcBookingCharges = irctcBookingCharges + Float.parseFloat(fareDetails[i].getTax3());
                                } catch (Exception e) {
                                }
                            }

                            if (pojoTicket.getData().isCancelAllowed() &&
                                    !(pojoTicket.getData().getStatus().equalsIgnoreCase("Can/Mod") ||
                                            pojoTicket.getData().getStatus().equalsIgnoreCase("Not Booked") ||
                                            pojoTicket.getData().getStatus().equalsIgnoreCase("Under Process"))) {



//                                if (pojoTicket.getData().getIrFlightsBook().getTripType()==0){
                                (view.findViewById(R.id.btn_book_cab)).setVisibility(View.VISIBLE);

                                (view.findViewById(R.id.btn_book_cab)).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Bundle bundle = new Bundle();
                                        String url = getString(R.string.olaUrl);
                                        bundle.putString("OlaUrl",url);
                                        startNextActivityForResult(bundle, WebViewActivity.class,1);
                                    }
                                });
//                                }

                            }else {
                                (view.findViewById(R.id.BTN_CANCEL_TICKET)).setVisibility(View.GONE);
                            }

                            ((TextView) view.findViewById(R.id.baseFare)).setText(getString(R.string.RUPEE_SYMBOL) + String.valueOf(basefare));
                            ((TextView) view.findViewById(R.id.taxesAndFee)).setText(getString(R.string.RUPEE_SYMBOL) + String.valueOf(totalTax));
                            ((TextView) view.findViewById(R.id.TXT_FARE_TOTAL)).setText(getString(R.string.RUPEE_SYMBOL) + String.valueOf(basefare + totalTax + irctcBookingCharges));
                            ((TextView) view.findViewById(R.id.irctcBookingCharges)).setText(getString(R.string.RUPEE_SYMBOL) + String.valueOf(irctcBookingCharges));

                            ((ScrollView) view.findViewById(R.id.scrollView)).postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    ((ScrollView) view.findViewById(R.id.scrollView)).scrollTo(0, 0);
                                }
                            }, 200);
                        } else {
                            Toast.makeText(getActivity(), pojoTicket.getStatus(), Toast.LENGTH_LONG).show();
                            ((TextView) view.findViewById(R.id.ticketBookingStatus)).setText("Failed");
                            ((TextView) view.findViewById(R.id.transactionId)).setText(FragmentAddPassengers.transactionId);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("error", error.toString());
                        ((LinearLayout) view.findViewById(R.id.layout1)).setVisibility(View.GONE);
                        ((RelativeLayout) view.findViewById(R.id.layout2)).setVisibility(View.GONE);
                        ((TextView) view.findViewById(R.id.somethingWrong)).setVisibility(View.VISIBLE);

                        NetworkingUtils.dismissProgress();
                        NetworkingUtils.noInternetAccess(getActivity());
                    }
                });
        return view;
    }

    public void startNextActivityForResult(Bundle bundle,
                                           Class<? extends Activity> activityClass, int REQ_CODE) {

        Intent i = new Intent(getActivity(), activityClass);
        if (null != bundle) {
            i.putExtras(bundle);
        }
        getActivity().startActivityForResult(i, REQ_CODE);
    }

    @Override
    public void onStart() {
        super.onStart();
        ActivityMain.ticketPage = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        ActivityMain.ticketPage = false;
    }

    @Override
    public void onResume() {
        super.onResume();
        AppController.getInstance().trackScreenView("Ticket Screen");
    }
}