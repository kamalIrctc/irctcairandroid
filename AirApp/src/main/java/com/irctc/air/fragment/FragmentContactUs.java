package com.irctc.air.fragment;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.irctc.air.AppController;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.header.AirHeader;
import com.irctc.air.util.Constant;


/**
 * Created by vivek on 04/13/2016
 */
public class FragmentContactUs extends Fragment {


    private ActivityMain mainActivity;
    private TextView tvVersion;


    @Deprecated
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mainActivity = (ActivityMain) activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.contact_us, null);

//        initView(view);

        // Header visibility
        AirHeader.showRecentSearchIcon(false);

        // Show header text
        AirHeader.showHeaderText(mainActivity, true,"Contact Us");

        // Show&Hide toggle and drawer
        AirHeader.showDrawerToggleAndToolbar(true, true);

  //      initData();

        return view;
    }


    private void initView(View view) {
        AirHeader.createHeader(mainActivity, getString(R.string.about_us));

        tvVersion = (TextView) view.findViewById(R.id.tv_version);

//        PlayGifView pGif = (PlayGifView) view.findViewById(R.id.viewGif);
//        pGif.setImageResource(R.drawable.test);
    }

    private void initData() {
        try {
            tvVersion.setText(getString(R.string.version)+" : " + mainActivity.getPackageManager().getPackageInfo(mainActivity.getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ActivityMain.activeFragment = Constant.WEBVIEW_FRAGMENT;
            AppController.getInstance().trackScreenView("Contact us Screen");
    }
}
