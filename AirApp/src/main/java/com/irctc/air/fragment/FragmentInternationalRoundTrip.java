package com.irctc.air.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.adapter.OnwardFlightInfoAdapter;
import com.irctc.air.adapter.RoundOnwardFlightAdapter;
import com.irctc.air.header.AirHeader;
import com.irctc.air.model.AirDataModel;
import com.irctc.air.model.FlightFilterBean;
import com.irctc.air.model.FlightOnWardDetailBean;
import com.irctc.air.model.InnerFlightDetailsBeans;
import com.irctc.air.services.FareQuoteRoundTripService;
import com.irctc.air.util.AirFlightSort;
import com.irctc.air.util.AlertDialogUtil;
import com.irctc.air.util.AppLogger;
import com.irctc.air.util.ArrivalTimeSort;
import com.irctc.air.util.Constant;
import com.irctc.air.util.DateUtility;
import com.irctc.air.util.DepartTimeSort;
import com.irctc.air.util.DurationTimeSort;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.FarePriceSort;
import com.irctc.air.util.InternationalRoundFilterEventHandler;
import com.irctc.air.util.ProjectUtil;
import com.irctc.air.Database.SharedPrefrenceAir;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;

/**
 * Created by tourism on 5/18/2016.
 */
public class FragmentInternationalRoundTrip extends Fragment implements InternationalRoundFilterEventHandler.AfterApply,AdapterView.OnItemClickListener, View.OnClickListener {

    private ActivityMain mainActivity;

    private LinearLayout flightInfo, layRoundBook;
    private Button btnSortRouOnwAirline;
    private Button btnSortRouOnwDepart;
    private Button btnSortRouOnwArive;
    private Button btnSortRouOnwDuration;
    private Button btnSortRouOnwPrice;
    private ListView listViewRouOnwAllFlight;

    private ArrayList<FlightOnWardDetailBean> specificRoundOnwardFlight;
    private ArrayList<FlightOnWardDetailBean> alFliterGDSFlight;
    private RoundOnwardFlightAdapter roundOnwardAdapter;
    private int rouOnwPosition;
    private final int IS_ROUND_ONWARD_SCREEN = 2;

    private TextView txtTotalPrice;
    // INFO DIALOG
    private Dialog lObjDialogShowFlightDetails;

    private boolean btnRouOnwAirline, btnRouOnwPrice, btnRouOnwDepart, btnRouOnwArrive, btnRouOnwDuration;

    // INFO FILTER DIALOG
    private Dialog lObjDialogShowFilterOption;

    private ImageView imgRecentSearch;

    // FLOATING BUTTON
    LinearLayout mFloatinFilterBtn;
    private FlightFilterBean filterBean;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_international_round_trip, null);

        // Initialize
        initializeVariable(view);

        // Show header text
        AirHeader.showHeaderText(mainActivity, false, "");
        // Header visibility
        AirHeader.showRecentSearchIcon(true);
        // Show&Hide toggle and drawer and toolbar
        AirHeader.showDrawerToggleAndToolbar(true, true);

        roundOnwardLayout();
        // Set data in variable from holder
        //setDataInRoundOnwardVarFromHolder();

        return view;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mainActivity = (ActivityMain) activity;
    }

    private void initializeVariable(View view) {

        flightInfo = (LinearLayout) view.findViewById(R.id.INTER_FLIGHT_INFO);
        txtTotalPrice = (TextView) view.findViewById(R.id.TXT_INTER_TOTAL_PRICE);
        btnSortRouOnwAirline = (Button) view.findViewById(R.id.BTN_INTER_AIRLINE);
        btnSortRouOnwDepart = (Button) view.findViewById(R.id.BTN_INTER_DEPART);
        btnSortRouOnwArive = (Button) view.findViewById(R.id.BTN_INTER_ARRIVE);
        btnSortRouOnwDuration = (Button) view.findViewById(R.id.BTN_INTER_DURATION);
        btnSortRouOnwPrice = (Button) view.findViewById(R.id.BTN_INTER_PRICE);

        listViewRouOnwAllFlight = (ListView) view.findViewById(R.id.LIST_INTER_ALL_FLIGHTS);

        mFloatinFilterBtn = (LinearLayout) view.findViewById(R.id.FLOATING_BTN_ROUND);

        mFloatinFilterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(filterBean == null){
                    filterBean = new FlightFilterBean();
                }
                showFilterDialog(filterBean);

            }
        });

        imgRecentSearch = (ImageView) mainActivity.findViewById(R.id.RECENT_SEARCH);
        imgRecentSearch.setVisibility(View.VISIBLE);
        imgRecentSearch.setImageResource(R.drawable.recent_search);
        imgRecentSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mainActivity.lastActiveFragment = Constant.FRAGMENT_INTERNATION_ROUND_TRIP;
                ProjectUtil.replaceFragment(mainActivity, new FragmentRecentSearch(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

            }
        });

        listViewRouOnwAllFlight.setOnItemClickListener(this);
        layRoundBook = (LinearLayout) view.findViewById(R.id.LAY_ROUND_BOOKING);

        layRoundBook.setOnClickListener(this);
        flightInfo.setOnClickListener(this);
        btnSortRouOnwAirline.setOnClickListener(this);
        btnSortRouOnwDepart.setOnClickListener(this);
        btnSortRouOnwArive.setOnClickListener(this);
        btnSortRouOnwDuration.setOnClickListener(this);
        btnSortRouOnwPrice.setOnClickListener(this);

    }

    private void setDataInRoundOnwardVarFromHolder() {

        //specificRoundOnwardFlight = (ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getOnwInternationalFlightDetails().clone();

        roundOnwardAdapter = new RoundOnwardFlightAdapter(mainActivity, specificRoundOnwardFlight);
        listViewRouOnwAllFlight.setAdapter(roundOnwardAdapter);

        defaultRoundOnwardSortingOnFare();
        listViewRouOnwAllFlight.setItemChecked(0, true);
        setRoundOnwardFirstFlightsPrice(0, 0);
    }

    private void setRoundOnwardFirstFlightsPrice(int onwPosition, int i1) {

        if (specificRoundOnwardFlight.size() != 0 ) {

            FlightOnWardDetailBean lRouOnwflightDetails = specificRoundOnwardFlight.get(onwPosition);
            txtTotalPrice.setText("\u20B9  " + (lRouOnwflightDetails.getFlightFare()));
        }
        else{
            txtTotalPrice.setText("\u20B9  " + "00");
        }
    }

    private void defaultRoundOnwardSortingOnFare() {


        Collections.sort(specificRoundOnwardFlight, new FarePriceSort());
        btnRouOnwPrice = true;
        Drawable img = getContext().getResources().getDrawable( R.drawable.up_arrow );
        img.setBounds(1, 1, 16, 23);
        btnSortRouOnwPrice.setCompoundDrawables(null, null, img, null);
        rouOnwSortButtonHandler(5, btnSortRouOnwPrice);
    }


    private void rouOnwSortButtonHandler(int buttonNo, Button button){

        switch(buttonNo){

            case 1:
                // Set background Airline
                button.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLightBlue));
                button.setTextColor(mainActivity.getResources().getColor(R.color.colorWhite));

                btnSortRouOnwDepart.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwArive.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwDuration.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwPrice.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));

                btnSortRouOnwDepart.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwArive.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwDuration.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwPrice.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));

                btnSortRouOnwDepart.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwArive.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwDuration.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwPrice.setCompoundDrawables(null, null, null, null);

                btnRouOnwDepart = false;
                btnRouOnwArrive = false;
                btnRouOnwDuration = false;
                btnRouOnwPrice = false;

                break;
            case 2:
                // Set background Depart
                button.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLightBlue));
                button.setTextColor(mainActivity.getResources().getColor(R.color.colorWhite));

                btnSortRouOnwAirline.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwArive.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwDuration.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwPrice.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));

                btnSortRouOnwAirline.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwArive.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwDuration.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwPrice.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));

                btnSortRouOnwAirline.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwArive.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwDuration.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwPrice.setCompoundDrawables(null, null, null, null);

                btnRouOnwAirline = false;
                btnRouOnwArrive = false;
                btnRouOnwDuration = false;
                btnRouOnwPrice = false;


                break;
            case 3:
                // Set background Arrive
                button.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLightBlue));
                button.setTextColor(mainActivity.getResources().getColor(R.color.colorWhite));

                btnSortRouOnwAirline.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwDepart.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwDuration.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwPrice.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));

                btnSortRouOnwAirline.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwDepart.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwDuration.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwPrice.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));

                btnSortRouOnwAirline.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwDepart.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwDuration.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwPrice.setCompoundDrawables(null, null, null, null);

                btnRouOnwAirline = false;
                btnRouOnwDepart = false;
                btnRouOnwDuration = false;
                btnRouOnwPrice = false;

                break;
            case 4:
                // Set background Duration
                button.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLightBlue));
                button.setTextColor(mainActivity.getResources().getColor(R.color.colorWhite));

                btnSortRouOnwAirline.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwDepart.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwArive.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwPrice.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));

                btnSortRouOnwAirline.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwDepart.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwArive.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwPrice.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));

                btnSortRouOnwAirline.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwDepart.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwArive.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwPrice.setCompoundDrawables(null, null, null, null);

                btnRouOnwAirline = false;
                btnRouOnwDepart = false;
                btnRouOnwArrive = false;
                btnRouOnwPrice = false;

                break;
            case 5:

                // Set background Price
                button.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorLightBlue));
                button.setTextColor(mainActivity.getResources().getColor(R.color.colorWhite));

                btnSortRouOnwAirline.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwDepart.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwArive.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));
                btnSortRouOnwDuration.setBackgroundColor(mainActivity.getResources().getColor(R.color.colorHardGrey));

                btnSortRouOnwAirline.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwDepart.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwArive.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwDuration.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));
                btnSortRouOnwDuration.setTextColor(mainActivity.getResources().getColor(R.color.WHITE));

                btnSortRouOnwAirline.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwDepart.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwArive.setCompoundDrawables(null, null, null, null);
                btnSortRouOnwDuration.setCompoundDrawables(null, null, null, null);

                btnRouOnwAirline = false;
                btnRouOnwDepart = false;
                btnRouOnwArrive = false;
                btnRouOnwDuration = false;

                break;
            default:
                break;

        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        listViewRouOnwAllFlight.setSelector(R.drawable.list_selector);
        rouOnwPosition = position;
        setRoundOnwardFirstFlightsPrice(rouOnwPosition, 0);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.INTER_FLIGHT_INFO:
                if(specificRoundOnwardFlight.size() >0){

                    mainActivity.isGdsOrInternatioal = true;
                    showSelectedFlightDetailsDialog(specificRoundOnwardFlight);
                }else{
                    new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.FLIGHT_BOOK_ERROR), mainActivity.getResources().getString(R.string.FLIGHT_INFO), Constant.ALERT_ACTION_ONE).generateAlert();

                }

                break;

            case R.id.BTN_INTER_AIRLINE:

                if(!btnRouOnwAirline){

                    Collections.sort(specificRoundOnwardFlight, new AirFlightSort());
                    listViewRouOnwAllFlight.invalidateViews();
                    btnRouOnwAirline = true;

                    Drawable img = getContext().getResources().getDrawable(R.drawable.up_arrow);
                    img.setBounds(1, 1, 16, 23);
                    btnSortRouOnwAirline.setCompoundDrawables(null, null, img, null);

                }
                else{

                    Collections.reverse(specificRoundOnwardFlight);
                    listViewRouOnwAllFlight.invalidateViews();
                    btnRouOnwAirline = false;

                    Drawable img = getContext().getResources().getDrawable( R.drawable.down_arrow);
                    img.setBounds(1, 1, 16, 23);
                    btnSortRouOnwAirline.setCompoundDrawables(null, null, img, null);
                }
                setRoundOnwardFirstFlightsPrice(rouOnwPosition, 0);
                rouOnwSortButtonHandler(1, btnSortRouOnwAirline );

                break;

            case R.id.BTN_INTER_DEPART:

                if(!btnRouOnwDepart){

                    Collections.sort(specificRoundOnwardFlight, new DepartTimeSort());
                    listViewRouOnwAllFlight.invalidateViews();
                    btnRouOnwDepart = true;

                    Drawable img = getContext().getResources().getDrawable( R.drawable.up_arrow );
                    img.setBounds(1, 1, 16, 23);
                    btnSortRouOnwDepart.setCompoundDrawables(null, null, img, null);
                }
                else{

                    Collections.reverse(specificRoundOnwardFlight);
                    btnRouOnwDepart = false;
                    listViewRouOnwAllFlight.invalidateViews();

                    Drawable img = getContext().getResources().getDrawable( R.drawable.down_arrow);
                    img.setBounds(1, 1, 16, 23);
                    btnSortRouOnwDepart.setCompoundDrawables(null, null, img, null);
                }
                setRoundOnwardFirstFlightsPrice(rouOnwPosition,0);
                rouOnwSortButtonHandler(2, btnSortRouOnwDepart);

                break;
            case R.id.BTN_INTER_ARRIVE:

                if(!btnRouOnwArrive){

                    Collections.sort(specificRoundOnwardFlight, new ArrivalTimeSort());
                    listViewRouOnwAllFlight.invalidateViews();
                    btnRouOnwArrive = true;

                    Drawable img = getContext().getResources().getDrawable( R.drawable.up_arrow );
                    img.setBounds(1, 1, 16, 23);
                    btnSortRouOnwArive.setCompoundDrawables(null, null, img, null);
                }
                else{
                    Collections.reverse(specificRoundOnwardFlight);
                    btnRouOnwArrive = false;
                    listViewRouOnwAllFlight.invalidateViews();

                    Drawable img = getContext().getResources().getDrawable( R.drawable.down_arrow);
                    img.setBounds(1, 1, 16, 23);
                    btnSortRouOnwArive.setCompoundDrawables(null, null, img, null);
                }
                setRoundOnwardFirstFlightsPrice(rouOnwPosition,0);
                rouOnwSortButtonHandler(3, btnSortRouOnwArive);

                break;
            case R.id.BTN_INTER_DURATION:

                if(!btnRouOnwDuration){

                    Collections.sort(specificRoundOnwardFlight, new DurationTimeSort());
                    listViewRouOnwAllFlight.invalidateViews();
                    btnRouOnwDuration = true;

                    Drawable img = getContext().getResources().getDrawable( R.drawable.up_arrow );
                    img.setBounds(1, 1, 16, 23);
                    btnSortRouOnwDuration.setCompoundDrawables(null, null, img, null);
                }
                else{
                    Collections.reverse(specificRoundOnwardFlight);
                    btnRouOnwDuration = false;
                    listViewRouOnwAllFlight.invalidateViews();

                    Drawable img = getContext().getResources().getDrawable( R.drawable.down_arrow);
                    img.setBounds(1, 1, 16, 23);
                    btnSortRouOnwDuration.setCompoundDrawables(null, null, img, null);
                }
                setRoundOnwardFirstFlightsPrice(rouOnwPosition,0);
                rouOnwSortButtonHandler(4, btnSortRouOnwDuration);
                break;
            case R.id.BTN_INTER_PRICE:

                if(!btnRouOnwPrice){

                    Collections.sort(specificRoundOnwardFlight, new FarePriceSort());
                    listViewRouOnwAllFlight.invalidateViews();
                    btnRouOnwPrice = true;

                    Drawable img = getContext().getResources().getDrawable( R.drawable.up_arrow );
                    img.setBounds(1, 1, 16, 23);
                    btnSortRouOnwPrice.setCompoundDrawables(null, null, img, null);
                    btnRouOnwPrice = true;
                }
                else{

                    Collections.reverse(specificRoundOnwardFlight);
                    btnRouOnwPrice = false;
                    listViewRouOnwAllFlight.invalidateViews();

                    Drawable img = getContext().getResources().getDrawable( R.drawable.down_arrow);
                    img.setBounds(1, 1, 16, 23);
                    btnSortRouOnwPrice.setCompoundDrawables(null, null, img, null);
                }
                setRoundOnwardFirstFlightsPrice(rouOnwPosition,0);
                rouOnwSortButtonHandler(5, btnSortRouOnwPrice);
                break;

            // COMMON CLICK LISTENER HANDLING
            case R.id.LAY_ROUND_BOOKING:


                if( specificRoundOnwardFlight.size() > 0 ) {

                    if (!ProjectUtil.checkInternetConnection(mainActivity)) {

                        new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.INTERNET_DOWN), mainActivity.getResources().getString(R.string.NO_INTERNET_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                        //ProjectUtil.showToast(getResources().getString(R.string.INTERNET_DOWN), mainActivity);

                    } else {
                        filterBean = null;
                        mainActivity.isGdsOrInternatioal = true;
                        FlightOnWardDetailBean onWardDetailBeanONW2 = specificRoundOnwardFlight.get(rouOnwPosition);

                        int lOnwUniqID2 = onWardDetailBeanONW2.getFlightUniqueId();//getFlightJson();

                        FlightOnWardDetailBean bOnw2 = AirDataHolder.getListHolder().getList().get(0).getOnwInternationalFlightDetails().get(lOnwUniqID2);

                        AppLogger.e("RoundONW : ", bOnw2.getFlightJson());
                        // CREATE THE XML FOR THE ROUND TRIP FLIGHTS AVAILABILITY
                        createXMLReqAndCallService(bOnw2.getFlightJson(), null);
                    }
                }
                else{
                    new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.FLIGHT_BOOK_ERROR), mainActivity.getResources().getString(R.string.FARE_QUOTE_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                }
                break;
        }
    }

    private void showSelectedFlightDetailsDialog( ArrayList<FlightOnWardDetailBean> lObjalOnwards ) {

//        int positionOnw = lObjalOnwards.get(rouOnwPosition).getFlightUniqueId();
//
//        lObjDialogShowFlightDetails = new Dialog(mainActivity, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
//        mainActivity.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
//        lObjDialogShowFlightDetails.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        lObjDialogShowFlightDetails.getWindow().setBackgroundDrawable(new ColorDrawable(Color.LTGRAY));
//
//        // Set layout
//        lObjDialogShowFlightDetails.setContentView(R.layout.round_trip_gds_flight_info);
//
//
//        ///// INITIALIZE VAR START //////
//        TextView mGdsFlightCodeToHeader,mGdsFlightCodeFromHeader ,mGdsHeaderDuration;
//        ListView lvGdsFlightDetail;
//        GdsFlightInfoAdapter mGdsFlightAdapter;
//        TextView flightBaseFare,flightTaxes,flightTotal;
//
//        //ONWARD
//        lvGdsFlightDetail = (ListView) lObjDialogShowFlightDetails.findViewById(R.id.GDS_LISTVIEW);
//        mGdsFlightCodeFromHeader = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.GDS_HEADER_FROM);
//        mGdsFlightCodeToHeader = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.GDS_HEADER_TO);
//        mGdsHeaderDuration = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.ONWARDS_HEADER_DRATION);
//
//
//        flightBaseFare = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.TXT_FARE_BASE);
//        flightTaxes = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.TXT_FARE_TAXES);
//        flightTotal = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.TXT_FARE_TOTAL);
//
//        Button lObjBtnDone = (Button) lObjDialogShowFlightDetails.findViewById(R.id.BTN_FLIGHT_DETAIL_DONE);
//
//        lObjBtnDone.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                lObjDialogShowFlightDetails.cancel();
//            }
//        });
//
//
//        // FOR ROUND ONWARD SCREEN GDS FLIGHTS
//
//        flightBaseFare.setText(mainActivity.getResources().getString(R.string.Rs) + AirDataHolder.getListHolder().getList().get(0).getOnwInternationalFlightDetails().get(positionOnw).getFlightBaseFare());
//        flightTaxes.setText(mainActivity.getResources().getString(R.string.Rs) + (AirDataHolder.getListHolder().getList().get(0).getOnwInternationalFlightDetails().get(positionOnw).getFlightTaxInFare() + AirDataHolder.getListHolder().getList().get(0).getOnwInternationalFlightDetails().get(positionOnw).getFlightStaxInFare()));
//        flightTotal.setText(mainActivity.getResources().getString(R.string.Rs) + AirDataHolder.getListHolder().getList().get(0).getOnwInternationalFlightDetails().get(positionOnw).getFlightFare());
//
//        mGdsFlightCodeFromHeader.setText(AirDataHolder.getListHolder().getList().get(0).getOnwInternationalFlightDetails().get(positionOnw).getFlightJourneyStartFrom());
//        mGdsFlightCodeToHeader.setText(AirDataHolder.getListHolder().getList().get(0).getOnwInternationalFlightDetails().get(positionOnw).getFlightJourneyEndTo());
//        /**
//         * If inner flight size is 1, show normal duration else find the first flight departure tine and last flight arrival time difference
//         */
//        int innerFlightSize = AirDataHolder.getListHolder().getList().get(0).getOnwInternationalFlightDetails().get(positionOnw).getFlight().size();
//        if(innerFlightSize == 1){
//            mGdsHeaderDuration.setText(AirDataHolder.getListHolder().getList().get(0).getOnwInternationalFlightDetails().get(positionOnw).getFlight().get(0).getFlightDurationTime());
//        }else{
//            mGdsHeaderDuration.setText(getOnWardFlightTotalTime());
//        }
//
//        mGdsFlightAdapter = new GdsFlightInfoAdapter(mainActivity, AirDataHolder.getListHolder().getList().get(0).getOnwInternationalFlightDetails().get(positionOnw).getFlight());
//        lvGdsFlightDetail.setAdapter(mGdsFlightAdapter);
//        ProjectUtil.updateListViewHeight(lvGdsFlightDetail);

        // Show dialog
//        lObjDialogShowFlightDetails.show();


        lObjDialogShowFlightDetails = new Dialog(mainActivity, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
        mainActivity.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        lObjDialogShowFlightDetails.requestWindowFeature(Window.FEATURE_NO_TITLE);
        lObjDialogShowFlightDetails.getWindow().setBackgroundDrawable(new ColorDrawable(Color.LTGRAY));

        // Set layout
        lObjDialogShowFlightDetails.setContentView(R.layout.round_trip_gds_flight_info);


        ///// INITIALIZE VAR START //////
        TextView mOnwardFlightFromToHeader,mOnwardHeaderDuration,mReturnFlightFromToHeader,mReturnHeaderDuration;
        ListView lvOnwardFlightDetail, lvReturnFlightDetail;
        OnwardFlightInfoAdapter mOnwardFlightAdapter, mReturnFlightAdapter;
        TextView flightBaseFare,flightTaxes,flightTotal;
        LinearLayout layReturnHeader;

        TextView txtOnwardRefundable,txtReturnRefundable;
        ImageView imgOnwardRefundable,imgReturnRefundable;




        //ONWARD
        lvOnwardFlightDetail = (ListView) lObjDialogShowFlightDetails.findViewById(R.id.FARE_ONWARD_QUOTE_LISTVIEW);
        mOnwardFlightFromToHeader = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.ONWARDS_HEADER_FROM_TO);
        mOnwardHeaderDuration = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.ONWARDS_HEADER_DRATION);

        imgOnwardRefundable = (ImageView) lObjDialogShowFlightDetails.findViewById(R.id.INTER_ONWARD_REFUNDABLE_IMG);
        txtOnwardRefundable = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.INTER_ONWARD_REFUNDABLE_TXT);




        // RETURN
        lvReturnFlightDetail = (ListView) lObjDialogShowFlightDetails.findViewById(R.id.FARE_RETURN_QUOTE_LISTVIEW);
        mReturnFlightFromToHeader = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.RETURN_HEADER_FROM_TO);
        mReturnHeaderDuration = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.RETURN_HEADER_DRATION);
       // layReturnHeader = (LinearLayout) lObjDialogShowFlightDetails.findViewById(R.id.LAY_RETURN_HEADER);

        imgReturnRefundable = (ImageView) lObjDialogShowFlightDetails.findViewById(R.id.INTER_RETURN_REFUNDABLE_IMG);
        txtReturnRefundable = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.INTER_RETURN_REFUNDABLE_TXT);




        flightBaseFare = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.TXT_FARE_BASE);
        flightTaxes = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.TXT_FARE_TAXES);
        flightTotal = (TextView) lObjDialogShowFlightDetails.findViewById(R.id.TXT_FARE_TOTAL);

        Button lObjBtnDone = (Button) lObjDialogShowFlightDetails.findViewById(R.id.BTN_FLIGHT_DETAIL_DONE);

        lObjBtnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                lObjDialogShowFlightDetails.cancel();
            }
        });

        ArrayList<InnerFlightDetailsBeans> onwGDSFlightDetails = null;
        ArrayList<InnerFlightDetailsBeans> retGDSFlightDetails = null;

        onwGDSFlightDetails = new ArrayList<>();
        retGDSFlightDetails = new ArrayList<>();


        for (int i = 0; i < lObjalOnwards.get(rouOnwPosition).getFlight().size(); i++) {

            InnerFlightDetailsBeans inner =  lObjalOnwards.get(rouOnwPosition).getFlight().get(i);

            if(inner.getFlightOnwRetType().equalsIgnoreCase("onward")){
                onwGDSFlightDetails.add(inner);
            }else if(inner.getFlightOnwRetType().equalsIgnoreCase("return")){
                retGDSFlightDetails.add(inner);
            }
        }


        // FOR ROUND ONWARD SCREEN GDS FLIGHTS

        flightBaseFare.setText(mainActivity.getResources().getString(R.string.Rs) + lObjalOnwards.get(rouOnwPosition).getFlightBaseFare());//getRouOnwGdsLccBean().getFlightLCC()
        flightTaxes.setText(mainActivity.getResources().getString(R.string.Rs) + (lObjalOnwards.get(rouOnwPosition).getFlightTaxInFare()+ lObjalOnwards.get(rouOnwPosition).getFlightStaxInFare()));
        flightTotal.setText(mainActivity.getResources().getString(R.string.Rs) + lObjalOnwards.get(rouOnwPosition).getFlightFare());

        mOnwardFlightFromToHeader.setText(lObjalOnwards.get(rouOnwPosition).getFlightJourneyStartFrom() +" - " + lObjalOnwards.get(rouOnwPosition).getFlightJourneyEndTo());

        mOnwardFlightAdapter = new OnwardFlightInfoAdapter(mainActivity, onwGDSFlightDetails);
        lvOnwardFlightDetail.setAdapter(mOnwardFlightAdapter);
        ProjectUtil.updateListViewHeight(lvOnwardFlightDetail);

        if(onwGDSFlightDetails.get(0).getTkt().contains("Non")){
            imgOnwardRefundable.setImageResource(R.drawable.refundable_red);

            // mOnwardRefundable.setTextColor(mainActivity.getResources().getColor(R.color.TXT_RED));
        }else{
            imgOnwardRefundable.setImageResource(R.drawable.refundable_green);

            // mOnwardRefundable.setTextColor(mainActivity.getResources().getColor(R.color.TXT_GREEN));
        }
        txtOnwardRefundable.setText("" + onwGDSFlightDetails.get(0).getTkt());




        // FOR ROUND RETURN FLIGHTS LCC
        mReturnFlightFromToHeader.setText(lObjalOnwards.get(rouOnwPosition).getFlightJourneyEndTo() +" - "+ lObjalOnwards.get(rouOnwPosition).getFlightJourneyStartFrom());
        mReturnFlightAdapter = new OnwardFlightInfoAdapter(mainActivity, retGDSFlightDetails);
        lvReturnFlightDetail.setAdapter(mReturnFlightAdapter);
        ProjectUtil.updateListViewHeight(lvReturnFlightDetail);

        if(retGDSFlightDetails.get(0).getTkt().contains("Non")){
            imgReturnRefundable.setImageResource(R.drawable.refundable_red);

            // mOnwardRefundable.setTextColor(mainActivity.getResources().getColor(R.color.TXT_RED));
        }else{
            imgReturnRefundable.setImageResource(R.drawable.refundable_green);

            // mOnwardRefundable.setTextColor(mainActivity.getResources().getColor(R.color.TXT_GREEN));
        }
        txtReturnRefundable.setText("" + onwGDSFlightDetails.get(0).getTkt());


        lObjDialogShowFlightDetails.show();

    }

    private void showFilterDialog(FlightFilterBean filterBean) {


        if (!mainActivity.isFinishing()) {

            lObjDialogShowFilterOption = new Dialog(mainActivity, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen);
            mainActivity.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            lObjDialogShowFilterOption.requestWindowFeature(Window.FEATURE_NO_TITLE);
            lObjDialogShowFilterOption.getWindow().setBackgroundDrawable(new ColorDrawable(Color.LTGRAY));
                    // Set layout
            lObjDialogShowFilterOption.setContentView(R.layout.round_trip_filter);

            // PROVIDE ALL NEW REFRESH GDS FLIGHT DATA
            specificRoundOnwardFlight = (ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getOnwInternationalFlightDetails().clone();

           /* new InternationalRoundFilterEventHandler(this ,mainActivity,
                lObjDialogShowFilterOption,
                specificRoundOnwardFlight,
                listViewRouOnwAllFlight,
                IS_ROUND_ONWARD_SCREEN,
                    filterBean
            );*/
            // SHOW THE DAILOG
            lObjDialogShowFilterOption.show();
        }

    }

    private void roundOnwardLayout(){

        rouOnwPosition = 0;
        listViewRouOnwAllFlight.setItemChecked(0, true);
        specificRoundOnwardFlight = new ArrayList<>();

        // HERE WE GET THE GDS FLIGHTS
        ArrayList<FlightOnWardDetailBean> allRoundOnwardFlight = new ArrayList<>();
        if(!mainActivity.isComingFromRoundFilterDialog){

            allRoundOnwardFlight = (ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getOnwInternationalFlightDetails().clone();
        }else{

            allRoundOnwardFlight =  alFliterGDSFlight;
            mainActivity.isComingFromRoundFilterDialog = false;
        }
        //ArrayList<FlightOnWardDetailBean> allRoundOnwardFlight = (ArrayList<FlightOnWardDetailBean>) AirDataHolder.getListHolder().getList().get(0).getRouOnwalAllFlightDetails().clone();

        for (int i = 0; i < allRoundOnwardFlight.size() ; i++) {

            FlightOnWardDetailBean flightDetailBean = allRoundOnwardFlight.get(i);

                specificRoundOnwardFlight.add(flightDetailBean);

        }
        // HERE WE SET THE DATA INTO  ROUND ONWARD VARIABLES FROM MAIN HOLDER CLONE
        setDataInRoundOnwardVarFromHolder();
    }
    public String getOnWardFlightTotalTime(){

        //"" + AirDataHolder.getListHolder().getList().get(0).getmFareQuoteOneWayBean().getFlight().get(0).getDuration()

        return "Working";
    }

    @Override
    public void doPerform(ArrayList<FlightOnWardDetailBean> onwrdClone) {

        alFliterGDSFlight = onwrdClone;
        roundOnwardLayout();
        //Toast.makeText(mainActivity, "Filter Result : " + onwrdClone.size(), Toast.LENGTH_SHORT).show();

    }

    private void createXMLReqAndCallService(String onwJson, String retJson) {

//        AppLogger.enable();
//        AppLogger.e("ONW => ", onwJson);
//        AppLogger.e("RET => ",retJson);

        ArrayList<AirDataModel> mAirMainHolder = AirDataHolder.getListHolder().getList();

        LinkedHashMap<String,String> mapParams = new LinkedHashMap<String,String>();

        mapParams.put("lccp_clientIP",  new SharedPrefrenceAir(mainActivity).getUserUniqueId());
        mapParams.put("lccp_userId", "0004888570");             // Default

        mapParams.put("lccp_domOrInt", mAirMainHolder.get(0).getTripDomOrInter());
        mapParams.put("lccp_tripType", mAirMainHolder.get(0).getTripType());
        mapParams.put("lccp_origin1", mAirMainHolder.get(0).getFromStationCity()+ ", " +mAirMainHolder.get(0).getFromStation());
        mapParams.put("lccp_origin2", "");
        mapParams.put("lccp_origin3", "");
        mapParams.put("lccp_origin4", "");
        mapParams.put("lccp_origin5", "");
        mapParams.put("lccp_origin6", "");

        mapParams.put("lccp_destination1", mAirMainHolder.get(0).getToStationCity()+ ", " +mAirMainHolder.get(0).getToStation());
        mapParams.put("lccp_destination2", "");
        mapParams.put("lccp_destination3", "");
        mapParams.put("lccp_destination4", "");
        mapParams.put("lccp_destination5", "");
        mapParams.put("lccp_destination6", "");

        //// Wed Apr 20 15:58:48 GMT+05:30 2016 ////
        mapParams.put("lccp_departDay1", mAirMainHolder.get(0).getDepDate().split(" ")[2]);
        mapParams.put("lccp_departDay2", "");
        mapParams.put("lccp_departDay3", "");
        mapParams.put("lccp_departDay4", "");
        mapParams.put("lccp_departDay5", "");
        mapParams.put("lccp_departDay6", "");


        mapParams.put("lccp_departMonth1", DateUtility.getMonthInInt(mAirMainHolder.get(0).getDepDate()));
        mapParams.put("lccp_departMonth2", "");
        mapParams.put("lccp_departMonth3", "");
        mapParams.put("lccp_departMonth4", "");
        mapParams.put("lccp_departMonth5", "");
        mapParams.put("lccp_departMonth6", "");

        mapParams.put("lccp_departYear1", mAirMainHolder.get(0).getDepDate().split(" ")[5]);
        mapParams.put("lccp_departYear2", "");
        mapParams.put("lccp_departYear3", "");
        mapParams.put("lccp_departYear4", "");
        mapParams.put("lccp_departYear5", "");
        mapParams.put("lccp_departYear6", "");

        if (mainActivity.isOneWaySelected) {
            mapParams.put("lccp_returnDay", "");
            mapParams.put("lccp_returnMonth", "");
            mapParams.put("lccp_returnYear", "");
        }else {
            mapParams.put("lccp_returnDay", mAirMainHolder.get(0).getReturnDate().split(" ")[2]);
            mapParams.put("lccp_returnMonth", DateUtility.getMonthInInt(mAirMainHolder.get(0).getReturnDate()));
            mapParams.put("lccp_returnYear", mAirMainHolder.get(0).getReturnDate().split(" ")[5]);
        }

        mapParams.put("lccp_noOfAdults", String.valueOf(mAirMainHolder.get(0).getAdultPassNum()));
        mapParams.put("lccp_noOfChildren", String.valueOf(mAirMainHolder.get(0).getChildPassNum()));
        mapParams.put("lccp_noOfInfants", String.valueOf(mAirMainHolder.get(0).getInfantPassNum()));

        mapParams.put("lccp_classType", mAirMainHolder.get(0).getTravelClass());

        mapParams.put("lccp_airlinePreference", "ALL");     // BCZ web _ selectipn pref.
        if(mainActivity.isComingFromSideLTC) {
            mapParams.put("lccp_isLTC", "true");
        }else{
            mapParams.put("lccp_isLTC", "false");
        }
 /*??*/         mapParams.put("lccp_oACCodeString", "");
        mapParams.put("lccp_oACAirlineXML", "");
 /*??*/         mapParams.put("lccp_airLineCodeString", ""); // ??
 /*??*/         mapParams.put("lccp_airPortCodeString", "");

        mapParams.put("lccp_railTransactionID", "");

 /*??*/       mapParams.put("lccp_flightId", "");
        mapParams.put("lccp_flightIdJson", "");
        mapParams.put("lccp_returnFlightId", "");
        mapParams.put("lccp_returnFlightIdJson", "");

        mapParams.put("lccp_jsonstring", "");
        mapParams.put("lccp_jsretstring", "");
        mapParams.put("lccp_roundOnwjson", "");
        mapParams.put("lccp_roundRetjson", "");

        mapParams.put("lccp_searchType", "oneway-return");

          // If special round trip booking  : One way return
        mapParams.put("lccp_fromCache", String.valueOf(AirDataHolder.getListHolder().getList().get(0).isFlightOnWardFromCache()));       // Dynamic from F Search result

        mapParams.put("lccp_isDistress", "false");


        mapParams.put("lccp_isLCCRoundBook", "false");

        String req = new ProjectUtil().convertMapToXml1(mapParams, "WSFareQuote");
        AppLogger.enable();
        AppLogger.e("REQ => ",req);


        // Round Trip FareQuoteService search service call
       /* FareQuoteRoundTripService auth = new FareQuoteRoundTripService(

                mainActivity,
                onwJson,
                retJson,
                new ProjectUtil().convertMapToXml1(mapParams, "WSFareQuote")
        );
        auth.execute();
*/
    }
    @Override
    public void onResume() {
        super.onResume();
        ActivityMain.activeFragment = Constant.FRAGMENT_INTERNATION_ROUND_TRIP;
    }
}
