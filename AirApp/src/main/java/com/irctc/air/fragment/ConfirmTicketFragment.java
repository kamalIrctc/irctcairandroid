package com.irctc.air.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.R;
import com.irctc.air.adapter.ConfirmPassListAdapter;
import com.irctc.air.adapter.ConfirmationFlightandPassInfoAdapter;
import com.irctc.air.header.AirHeader;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.util.Constant;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.ProjectUtil;

public class ConfirmTicketFragment extends Fragment {

    private ActivityMain mainActivity;
    private Button btnSubmit;
    private TextView flightBaseFare,irctcTxnFees,flightTaxes,flightTotal,txtBookingStatus, txtTransactionId, txtErrorMessage;
    private ImageView imgRecentSearch;

    private ListView lvCnfOnwardFlightDetail,lvCnfOnwardPassDetail;
    ConfirmationFlightandPassInfoAdapter mCnfOnwardFlightAdapter;
    ConfirmPassListAdapter mCnfOnwardPassAdapter;
    LinearLayout mCnfOnwardFlightlayoutMain ;
    TextView mCnfOnwardFlightFromToHeader,mCnfOnwardFlightToHeader,mCnfOnwardHeaderDuration,mCnfOnwardRefundable,mCnfOnwardFlightAmount, mCnfOnwardPNR,mCnfOnwardPNRHeader;

    private ListView lvCnfReturnFlightDetail, lvCnfReturnPassDetail;
    ConfirmationFlightandPassInfoAdapter mCnfReturnFlightAdapter;
    ConfirmPassListAdapter mCnfReturnPassAdapter;
    CardView mCnfReturnFlightlayoutMain;
    TextView mCnfReturnFlightFromToHeader,mCnfReturnFlightToHeader, mCnfReturnHeaderDuration, mCnfReturnRefundable, mCnfReturnFlightAmount, mCnfReturnPNR, mCnfReturnPNRHeader;

    ImageView imgCnfReturnRefundable;
    ImageView imgCnfOnwardRefundable;

    public ConfirmTicketFragment(){
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mainActivity = (ActivityMain) activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.confirm_ticket_layout, null);


        // Initialize
        initializeVariable(view);

        // Header visibility
        AirHeader.showRecentSearchIcon(true);

        // Show header text
        AirHeader.showHeaderText(mainActivity, true, "Ticket");

        // Show&Hide toggle and drawer and toolbar
        AirHeader.showDrawerToggleAndToolbar(true, true);

        // Set data in variable from holder
        setdataInVarFromHolder();
        return view;
    }



    private void initializeVariable(View view) {

        imgRecentSearch = (ImageView) mainActivity.findViewById(R.id.RECENT_SEARCH);
        imgRecentSearch.setVisibility(View.VISIBLE);
        imgRecentSearch.setImageResource(R.drawable.recent_search);
        imgRecentSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mainActivity.lastActiveFragment = Constant.CONFIRM_TICKET_FRAGMENT;
                ProjectUtil.replaceFragment(mainActivity, new FragmentRecentSearch(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

            }
        });



        // Onward flights details
        mCnfOnwardFlightlayoutMain = (LinearLayout)view.findViewById(R.id.CONFIRMATION_ONWARD_LAYOUT_MAIN);
        lvCnfOnwardFlightDetail = (ListView) view.findViewById(R.id.CONFIRMATION_ONWARD_QUOTE_LISTVIEW);
        lvCnfOnwardPassDetail = (ListView) view.findViewById(R.id.CONFIRMATION_ONWARD_PASS_LISTVIEW);
        mCnfOnwardFlightFromToHeader = (TextView) view.findViewById(R.id.CONFIRMATION_ONWARDS_HEADER_FROM_TO);
        mCnfOnwardFlightToHeader = (TextView) view.findViewById(R.id.CONFIRMATION_ONWARDS_HEADER_TO_CODE);

        //mCnfOnwardHeaderDuration = (TextView) view.findViewById(R.id.CONFIRMATION_ONWARDS_HEADER_DRATION);
        mCnfOnwardRefundable = (TextView) view.findViewById(R.id.CONFIRMATION_ONWARD_REFUNDABLE_TXT);
        imgCnfOnwardRefundable = (ImageView) view.findViewById(R.id.CONFIRMATION_ONWARD_REFUNDABLE_IMG);
        //mCnfOnwardFlightAmount = (TextView) view.findViewById(R.id.CONFIRMATION_ONWARD_TICKET_AMOUNT);
        /**
         * 21 Oct 2016
         * PNR - PASS STATUS
         */
        mCnfOnwardPNR = (TextView) view.findViewById(R.id.CONFIRMATION_ONWARDS_PNR);
        mCnfOnwardPNRHeader = (TextView) view.findViewById(R.id.CONFIRMATION_ONWARDS_PNR_HEADER);
        //mCnfOnwardFlightAmount = (TextView) view.findViewById(R.id.CONFIRMATION_ONWARD_TICKET_AMOUNT);


        // Return flights details
        mCnfReturnFlightlayoutMain = (CardView)view.findViewById(R.id.CONFIRMATION_RETURN_LAYOUT_MAIN);
        lvCnfReturnFlightDetail = (ListView) view.findViewById(R.id.CONFIRMATION_RETURN_QUOTE_LISTVIEW);
        lvCnfReturnPassDetail = (ListView) view.findViewById(R.id.CONFIRMATION_RETURN_PASS_LISTVIEW);
        mCnfReturnFlightFromToHeader = (TextView) view.findViewById(R.id.CONFIRMATION_RETURN_HEADER_FROM_TO);
        mCnfReturnFlightToHeader = (TextView) view.findViewById(R.id.CONFIRMATION_RETURN_HEADER_TO_CODE);

        //mCnfReturnHeaderDuration = (TextView) view.findViewById(R.id.CONFIRMATION_RETURN_HEADER_DRATION);
        mCnfReturnRefundable = (TextView) view.findViewById(R.id.CONFIRMATION_RETURN_REFUNDABLE_TXT);
        imgCnfReturnRefundable = (ImageView) view.findViewById(R.id.CONFIRMATION_RETURN_REFUNDABLE_IMG);
        /**
         * 21 Oct 2016
         * PNR - PASS STATUS
         */
        mCnfReturnPNR = (TextView) view.findViewById(R.id.CONFIRMATION_RETURN_PNR);
        mCnfReturnPNRHeader = (TextView) view.findViewById(R.id.CONFIRMATION_RETURN_PNR_HEADER);
        //mCnfReturnFlightAmount = (TextView) view.findViewById(R.id.CONFIRMATION_RETURN_TICKET_AMOUNT);


        flightBaseFare = (TextView) view.findViewById(R.id.CONFIRMATION_TXT_FARE_BASE);
        flightTaxes = (TextView) view.findViewById(R.id.CONFIRMATION_TXT_FARE_TAXES);
        /**
         * 07 NOV 2016
         * IRCTC SERVICE CHARGE
         */
        irctcTxnFees = (TextView) view.findViewById(R.id.IRCTC_FARE_TAXES);


        flightTotal = (TextView) view.findViewById(R.id.CONFIRMATION_TXT_FARE_TOTAL);

        txtBookingStatus = (TextView) view.findViewById(R.id.TXT_BOOKING_STATUS_VALUE);
        txtTransactionId = (TextView) view.findViewById(R.id.TXT_TRANSACTION_ID_VALUE);
        txtErrorMessage = (TextView) view.findViewById(R.id.ERROR_MESSAGE);

    }


    private void setdataInVarFromHolder() {

        if(!AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getBookingstatus().equalsIgnoreCase("7")){
            txtErrorMessage.setVisibility(View.VISIBLE);
        }else{
            txtErrorMessage.setVisibility(View.GONE);
        }

        /** CONFIRMATION OUTER LAYOUT DETAILS **/
        flightBaseFare.setText(mainActivity.getResources().getString(R.string.Rs) + AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getAlFareDetail().get(0).getBfare());
        flightTaxes.setText(mainActivity.getResources().getString(R.string.Rs) + (AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getAlFareDetail().get(0).getTax1()));
        /**
         * 07 NOV 2016
         * IRCTC SERVICE CHARGE
         */
        irctcTxnFees.setText(mainActivity.getResources().getString(R.string.Rs) + (AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getAlFareDetail().get(0).getIrctcTxnFees()));

        flightTotal.setText(mainActivity.getResources().getString(R.string.Rs)
                + (Integer.parseInt(AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getAlFareDetail().get(0).getSubTotal())
                + Math.ceil(AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getAlFareDetail().get(0).getIrctcTxnFees())));

        txtBookingStatus.setText(AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getBookingstatusValue());
        txtTransactionId.setText(AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getTrnId());

        /*************************************
         * If one way selected
         *************************************/

            if(AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getAlOnwardFlightConfirmationDetail().get(0).getAlSegmentDetail().get(0).getFaretype().equalsIgnoreCase("Refundable")){
                imgCnfOnwardRefundable.setImageResource(R.drawable.refundable_green);
                //mCnfOnwardRefundable.setTextColor(mainActivity.getResources().getColor(R.color.TXT_GREEN));
            }else{
                imgCnfOnwardRefundable.setImageResource(R.drawable.refundable_red);
               // mCnfOnwardRefundable.setTextColor(mainActivity.getResources().getColor(R.color.bpRed));
            }
            mCnfOnwardRefundable.setText(AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getAlOnwardFlightConfirmationDetail().get(0).getAlSegmentDetail().get(0).getFaretype());

            //mCnfOnwardHeaderDuration.setText(getOnWardFlightTotalTime());
           // mCnfOnwardFlightFromToHeader.setText(AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getOriginCode() + " - " + AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getDestinationCode());

            mCnfOnwardFlightFromToHeader.setText(AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getOriginCode());
            mCnfOnwardFlightToHeader.setText(AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getDestinationCode());


        /**
         * 21 Oct 2016
         * PNR - PASS STATUS
         */
            if(!AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getAlOnwardFlightConfirmationDetail().get(0).getAlSegmentDetail().get(0).getAirpnr().equalsIgnoreCase("")) {
                mCnfOnwardPNR.setText(AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getAlOnwardFlightConfirmationDetail().get(0).getAlSegmentDetail().get(0).getAirpnr());
                mCnfOnwardPNRHeader.setVisibility(View.VISIBLE);
            }

            // ONWARD FLIGHT TICKET CONFIRMATION FLIGHT DETAIL
            mCnfOnwardFlightAdapter = new ConfirmationFlightandPassInfoAdapter(mainActivity, AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getAlOnwardFlightConfirmationDetail());
            lvCnfOnwardFlightDetail.setAdapter(mCnfOnwardFlightAdapter);
            ProjectUtil.updateListViewHeight(lvCnfOnwardFlightDetail);

            // ONWARD PASS PASS DETAIL
            mCnfOnwardPassAdapter = new ConfirmPassListAdapter(mainActivity, AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getAlOnwardFlightConfirmationDetail().get(0).getAlPassengerDetail());
            lvCnfOnwardPassDetail.setAdapter(mCnfOnwardPassAdapter);
            ProjectUtil.updateListViewHeight(lvCnfOnwardPassDetail);


        /*************************************
         * If round trip selected
         *************************************/

        if(!mainActivity.isOneWaySelected){
            // RETURN CONFIRM LAYOUT
            mCnfReturnFlightlayoutMain.setVisibility(View.VISIBLE);

            if(AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getAlReturnFlightConfirmationDetail().get(0).getAlSegmentDetail().get(0).getFaretype().equalsIgnoreCase("Refundable")){
                imgCnfReturnRefundable.setImageResource(R.drawable.refundable_green);
               // mCnfReturnRefundable.setTextColor(mainActivity.getResources().getColor(R.color.TXT_GREEN));
            }else{
                imgCnfReturnRefundable.setImageResource(R.drawable.refundable_red);
                //mCnfReturnRefundable.setTextColor(mainActivity.getResources().getColor(R.color.bpRed));
            }
            mCnfReturnRefundable.setText(AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getAlReturnFlightConfirmationDetail().get(0).getAlSegmentDetail().get(0).getFaretype());

            //mCnfReturnHeaderDuration.setText(getOnWardFlightTotalTime());
            //mCnfReturnFlightFromToHeader.setText(AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getDestinationCode() + " - " + AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getOriginCode());

            mCnfReturnFlightFromToHeader.setText(AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getDestinationCode());
            mCnfReturnFlightToHeader.setText(AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getOriginCode());

            /**
             * 21 Oct 2016
             * PNR - PASS STATUS
             */
            if(!AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getAlReturnFlightConfirmationDetail().get(0).getAlSegmentDetail().get(0).getAirpnr().equalsIgnoreCase("")) {
                mCnfReturnPNR.setText(AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getAlReturnFlightConfirmationDetail().get(0).getAlSegmentDetail().get(0).getAirpnr());
                mCnfReturnPNRHeader.setVisibility(View.VISIBLE);
            }
            // RETURN FLIGHT TICKET CONFIRMATION FLIGHT DETAIL
            mCnfReturnFlightAdapter = new ConfirmationFlightandPassInfoAdapter(mainActivity, AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getAlReturnFlightConfirmationDetail());
            lvCnfReturnFlightDetail.setAdapter(mCnfReturnFlightAdapter);
            ProjectUtil.updateListViewHeight(lvCnfReturnFlightDetail);

            // RETURN PASS DETAIL
            mCnfReturnPassAdapter = new ConfirmPassListAdapter(mainActivity, AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().get(0).getAlReturnFlightConfirmationDetail().get(0).getAlPassengerDetail());
            lvCnfReturnPassDetail.setAdapter(mCnfReturnPassAdapter);
            ProjectUtil.updateListViewHeight(lvCnfReturnPassDetail);
        }else{
            // HIDE RETURN LAYOUT
            mCnfReturnFlightlayoutMain.setVisibility(View.GONE);

        }

    }



    public String getOnWardFlightTotalTime(){

        //"" + AirDataHolder.getListHolder().getList().get(0).getmFareQuoteOneWayBean().getFlight().get(0).getDuration()

        return "Working";
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        ActivityMain.activeFragment = Constant.CONFIRM_TICKET_FRAGMENT;
    }

}
