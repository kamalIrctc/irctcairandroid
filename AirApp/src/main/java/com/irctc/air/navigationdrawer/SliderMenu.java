package com.irctc.air.navigationdrawer;


import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;

import com.irctc.air.Database.AirDatabase;
import com.irctc.air.R;
import com.irctc.air.activity.WebViewActivity;
import com.irctc.air.adapter.NavDrawerExpandableAdapter;
import com.irctc.air.fragment.BookingHistoryFragment;
import com.irctc.air.fragment.FragmentPlanner;
import com.irctc.air.fragment.FragmentLogin;
import com.irctc.air.fragment.FragmentProfile;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.fragment.GenricWebviewFragment;
import com.irctc.air.model.HistoryBean;
//import com.irctc.air.services.LoginServiceAirToTourRest;
import com.irctc.air.networking.Networking;
import com.irctc.air.util.AlertDialogUtil;
import com.irctc.air.util.AppKeys;
import com.irctc.air.util.Constant;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.Pref;
import com.irctc.air.util.ProjectUtil;
import com.irctc.air.Database.SharedPrefrenceAir;

//import com.irctc.tourism.database.SharedPrefrenceTourism;
//import com.irctc.tourism.main.TSplashActivity;

import org.json.JSONObject;

import java.security.Key;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by vivek on 04/13/2016
 */
public class SliderMenu {

    static ListView lvNavDrawer;
    static AppCompatActivity lObjAppCompatActivity;


    static ArrayList<String> listDataHeader;
    static HashMap<String, ArrayList<String>> listDataChild;
    private static ExpandableListView elvNavDrawer;
    private static NavDrawerExpandableAdapter adapterNavDrawer;
    private static ActivityMain mMainActivityCtx;
    public static TextView txtUserId;
    private static DrawerLayout lObjDrawerLayout1;

    public static void initNavigationDrawer(final Context ctx, final DrawerLayout lObjDrawerLayout) {
        lObjAppCompatActivity = ((AppCompatActivity) ctx);
        mMainActivityCtx = (ActivityMain) ctx;
        lObjDrawerLayout1 = lObjDrawerLayout;

        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, ArrayList<String>>();

        // Adding child data
        listDataHeader.add(lObjAppCompatActivity.getString(R.string.SIDE_MENU_ONE));
        // listDataHeader.add(lObjAppCompatActivity.getString(R.string.SIDE_MENU_TWO));
        listDataHeader.add(lObjAppCompatActivity.getString(R.string.SIDE_MENU_THREE));
        listDataHeader.add(lObjAppCompatActivity.getString(R.string.SIDE_MENU_FIVE));
        listDataHeader.add("Cancel Old Tickets");
        listDataHeader.add(lObjAppCompatActivity.getString(R.string.SIDE_MENU_MORE));
        listDataHeader.add(lObjAppCompatActivity.getString(R.string.SIDE_MENU_EIGHT));
        listDataHeader.add(lObjAppCompatActivity.getString(R.string.SIDE_MENU_SIX));
        //listDataHeader.add(lObjAppCompatActivity.getString(R.string.SIDE_MENU_SEVEN));


        ArrayList<String> sideMenuOne = new ArrayList<String>();
        ArrayList<String> sideMenuTwo = new ArrayList<String>();
        ArrayList<String> sideMenuThree = new ArrayList<String>();
        ArrayList<String> sideMenuFive = new ArrayList<String>();
        ArrayList<String> sideMenuEight = new ArrayList<String>();
        ArrayList<String> sideMenuSix = new ArrayList<String>();


        ArrayList<String> alMore = new ArrayList<String>();


        String[] more = lObjAppCompatActivity.getResources().getStringArray(R.array.more);

        for (int i = 0; i < more.length; i++) {
            alMore.add(more[i]);
        }
        listDataChild.put(listDataHeader.get(0), sideMenuOne);      // Header, Child data
        listDataChild.put(listDataHeader.get(1), sideMenuThree);
        listDataChild.put(listDataHeader.get(2), sideMenuFive);
        listDataChild.put(listDataHeader.get(3), sideMenuFive);
        listDataChild.put(listDataHeader.get(4), alMore);
        listDataChild.put(listDataHeader.get(5), sideMenuEight);
        listDataChild.put(listDataHeader.get(6), sideMenuSix);

        txtUserId = (TextView) lObjAppCompatActivity.findViewById(R.id.TXT_USER_ID);
        /*if (Pref.getString(mMainActivityCtx, AppKeys.USER_DETAIL_FIRST_NAME).equals("")) {
            txtUserId.setText("Guest");
        } else {
            txtUserId.setText(Pref.getString(mMainActivityCtx, AppKeys.USER_DETAIL_FIRST_NAME));
        }*/

        ProjectUtil.setUserLogin(ctx);


        elvNavDrawer = (ExpandableListView) lObjAppCompatActivity.findViewById(R.id.elv_nav_drawer);
        adapterNavDrawer = new NavDrawerExpandableAdapter(lObjAppCompatActivity, listDataHeader, listDataChild, lObjDrawerLayout);
        elvNavDrawer.setGroupIndicator(null);
        elvNavDrawer.setAdapter(adapterNavDrawer);

        elvNavDrawer.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                                                 @Override
                                                 public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                                                     Fragment fragment = null;


                                                     if (listDataHeader.get(groupPosition).equals(lObjAppCompatActivity.getString(R.string.SIDE_MENU_ONE))) {
                                                         lObjDrawerLayout.closeDrawers();

                                                         setMAinActivityFlagsToDefaultValue();

                                                         mMainActivityCtx.isComingFromSideLTC = false;

                                                         // Call fragment with LTC flag false
                                                         mMainActivityCtx.lastActiveFragment = Constant.PLANNER_FRAGMENT;
                                                         fragment = new FragmentPlanner();
                                                         Bundle bundle = new Bundle();
                                                         bundle.putString("previousData", "Clear");
                                                         fragment.setArguments(bundle);

                                                     } else if (listDataHeader.get(groupPosition).equals(lObjAppCompatActivity.getString(R.string.SIDE_MENU_TWO))) {
                                                         lObjDrawerLayout.closeDrawers();

                                                         setMAinActivityFlagsToDefaultValue();

                                                         // Call planner fragment with LTC flag true
                                                         mMainActivityCtx.lastActiveFragment = Constant.PLANNER_FRAGMENT;
                                                         fragment = new FragmentPlanner();

                                                         mMainActivityCtx.isComingFromSideLTC = true;


                                                     } else if (listDataHeader.get(groupPosition).equals("Cancel Old Tickets")) {
                                                         lObjDrawerLayout.closeDrawers();
                                                         setMAinActivityFlagsToDefaultValue();
                                                         fragment = null;
                                                         String urlString = Networking.CANCEL_OLD_TICKETS;
                                                         Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlString));
                                                         intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                         intent.setPackage("com.android.chrome");
                                                         try {
                                                             mMainActivityCtx.startActivity(intent);
                                                         } catch (ActivityNotFoundException ex) {
                                                             intent.setPackage(null);
                                                             mMainActivityCtx.startActivity(intent);
                                                         }
                                                     } else if (listDataHeader.get(groupPosition).equals(lObjAppCompatActivity.getString(R.string.SIDE_MENU_THREE))) {
                                                         lObjDrawerLayout.closeDrawers();

                                                         setMAinActivityFlagsToDefaultValue();

                                                         if (Pref.getBoolean(ctx)) {
                                                             mMainActivityCtx.lastActiveFragment = Constant.PLANNER_FRAGMENT;
                                                             fragment = new FragmentProfile();
                                                         } else {
                                                             new AlertDialogUtil(mMainActivityCtx, mMainActivityCtx.getResources().getString(R.string.PROFILE_NOT_LOGIN_MESSAGE), mMainActivityCtx.getResources().getString(R.string.PROFILE_TITLE_TEXT), Constant.ALERT_ACTION_TWO, new FragmentLogin()).generateAlert();
                                                         }

                                                     } else if (listDataHeader.get(groupPosition).equals(lObjAppCompatActivity.getString(R.string.SIDE_MENU_FIVE))) {
                                                         lObjDrawerLayout.closeDrawers();
                                                         if (!Pref.getBoolean(ctx)) {
                                                             mMainActivityCtx.isComingFromFareQuote = false;
                                                             mMainActivityCtx.lastActiveFragment = Constant.PLANNER_FRAGMENT;
                                                             ProjectUtil.replaceFragment(lObjAppCompatActivity, new FragmentLogin(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                                                         } else {
                                                             setMAinActivityFlagsToDefaultValue();
                                                             ProjectUtil.replaceFragment(lObjAppCompatActivity, new BookingHistoryFragment(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                                                         }
/*
                         if(new SharedPrefrenceAir(mMainActivityCtx).getIsLoggedIn()){

                             // Booking History Service  call

                            */
/* if (!ProjectUtil.checkInternetConnection(mMainActivityCtx)) {

                                 new AlertDialogUtil(mMainActivityCtx, mMainActivityCtx.getResources().getString(R.string.INTERNET_DOWN), mMainActivityCtx.getResources().getString(R.string.NO_INTERNET_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();

                             } else {
*//*

                                 mMainActivityCtx.lastActiveFragment = Constant.PLANNER_FRAGMENT;

                                 */
/**
 * 21 Oct 2016
 * PNR - PASS STATUS
 *//*


                                 if(getAllBookedHistory().size() > 0){
                                     // show data from the database
                                     new DatabaseBookingHistoryParser(getAllBookedHistory()).databaseHistoryParser();
                                     ProjectUtil.replaceFragment(lObjAppCompatActivity, new BookingHistoryFragment(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                                     mMainActivityCtx.hideDialog = true;
                                     //BookingHistoryService auth = new BookingHistoryService(mMainActivityCtx);
                                     //auth.execute();
                                 }else{
                                     // show data from the service calling
                                     //BookingHistoryService auth = new BookingHistoryService(mMainActivityCtx);
                                     //auth.execute();
                                 }

  //                           }

                             //ProjectUtil.replaceFragment(lObjAppCompatActivity, new BookedHistoryFragment(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

                         }else{
                             new AlertDialogUtil(mMainActivityCtx, mMainActivityCtx.getResources().getString(R.string.BOOKING_HISTORY_TEXT_MESSAGE), mMainActivityCtx.getResources().getString(R.string.BOOKING_HISTORY_TITLE_TEXT), Constant.ALERT_ACTION_TWO, new FragmentLogin()).generateAlert();
                         }
*/


                                                     } else if (listDataHeader.get(groupPosition).equals(lObjAppCompatActivity.getString(R.string.SIDE_MENU_SIX))) {
                                                         lObjDrawerLayout.closeDrawers();
                                                         setMAinActivityFlagsToDefaultValue();
                                                         if (!Pref.getBoolean(ctx)) {
                                                             mMainActivityCtx.isComingFromFareQuote = false;
                                                             mMainActivityCtx.lastActiveFragment = Constant.PLANNER_FRAGMENT;
                                                             fragment = new FragmentLogin();
                                                         } else {
                                                             mMainActivityCtx.lastActiveFragment = Constant.PLANNER_FRAGMENT;
                                                             Pref.setString(ctx, AppKeys.USER_DETAIL_ADDRESS_1, "");
                                                             Pref.setString(ctx, AppKeys.USER_DETAIL_ADDRESS_2, "");
                                                             Pref.setString(ctx, AppKeys.USER_DETAIL_FIRST_NAME, "");
                                                             Pref.setString(ctx, AppKeys.USER_DETAIL_LAST_NAME, "");
                                                             Pref.setString(ctx, AppKeys.USER_DETAIL_CITY, "");
                                                             Pref.setString(ctx, AppKeys.USER_DETAIL_STATE, "");
                                                             Pref.setString(ctx, AppKeys.USER_DETAIL_COUNTRY, "");
                                                             Pref.setString(ctx, AppKeys.USER_DETAIL_PIN_CODE, "");
                                                             Pref.setString(ctx, AppKeys.USER_DETAIL_EMAIL, "");
                                                             Pref.setString(ctx, AppKeys.USER_DETAIL_MOBILE_NO, "");
                                                             Pref.setBoolean(ctx, false);
                                                             SliderMenu.txtUserId.setText("Guest");
                                                             AirDatabase airDatabase = new AirDatabase(mMainActivityCtx.getApplicationContext());
                                                             airDatabase.removeAuthToken();
                                                             new AlertDialogUtil(mMainActivityCtx, mMainActivityCtx.getResources().getString(R.string.LOGOUT_MESSAGE), mMainActivityCtx.getResources().getString(R.string.LOGIN_ERROR_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_TWO, new FragmentLogin()).generateAlert();
                                                             ProjectUtil.setUserLogin(mMainActivityCtx);
                                                             // delete the booking history
                                                             if (getAllBookedHistory().size() > 0) {
                                                                 deleteBookingHistory();
                                                             }
                                                         }
                                                     }else if (listDataHeader.get(groupPosition).equals(lObjAppCompatActivity.getString(R.string.SIDE_MENU_EIGHT))) {
                                                             lObjDrawerLayout.closeDrawers();
                                                             mMainActivityCtx.isComingFromFareQuote = false;
                                                             Bundle bundle = new Bundle();
                                                             String url = mMainActivityCtx.getString(R.string.olaUrl);
                                                             bundle.putString("OlaUrl",url);
                                                             startNextActivityForResult(bundle, WebViewActivity.class,1);

                                                     }


                                                     if (fragment != null) {

                                                         ProjectUtil.replaceFragment(lObjAppCompatActivity, fragment, R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                                                     }
                                                     return false;
                                                 }
                                             }

        );

    }
    public static void startNextActivityForResult(Bundle bundle,
                                                  Class<? extends Activity> activityClass, int REQ_CODE) {

        Intent i = new Intent(mMainActivityCtx, activityClass);
        if (null != bundle) {
            i.putExtras(bundle);
        }
        mMainActivityCtx.startActivityForResult(i, REQ_CODE);
    }

    private static String makeRequestParameter() {

        JSONObject jsonVoucherInfo = new JSONObject();
        JSONObject jsonUserInfo = new JSONObject();

        try {

            SharedPrefrenceAir pref = new SharedPrefrenceAir(lObjAppCompatActivity);

            if (pref.getIsGuestUser()) {

                // FOR GUEST USER
                jsonUserInfo.put("MOBILE_NUMBER", com.irctc.air.util.AES.decrypt(pref.getGuestUserMobile()));
                jsonUserInfo.put("EMAIL_ID", com.irctc.air.util.AES.decrypt(pref.getGuestUserEmail()));

                jsonUserInfo.put("USER_ID", "");
                jsonUserInfo.put("PASSWORD", "");

            } else {

                // FOR REGISTERED USER
                jsonUserInfo.put("MOBILE_NUMBER", "");
                jsonUserInfo.put("EMAIL_ID", "");

                jsonUserInfo.put("USER_ID", com.irctc.air.util.AES.decrypt(pref.getUserLogInId()));
                jsonUserInfo.put("PASSWORD", com.irctc.air.util.AES.decrypt(pref.getUserLogInPwd()));
            }

            jsonVoucherInfo.put("UserInfo", jsonUserInfo);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return jsonVoucherInfo.toString();
    }

    /***************************************************************************
     * This method sets all value to default if selected side
     * menu features
     ***************************************************************************/
    public static void setMAinActivityFlagsToDefaultValue() {

        mMainActivityCtx.boolBackClose = false;
        mMainActivityCtx.flagIsSliderOpen = false;
        mMainActivityCtx.activeFragment = 0;
        mMainActivityCtx.lastActiveFragment = 0;
        mMainActivityCtx.isDepartureStationSelected = false;
        mMainActivityCtx.isOneWaySelected = true;
        mMainActivityCtx.isDepartureDateSelected = true;
        mMainActivityCtx.isEconomyClassSelected = true;
        mMainActivityCtx.isComingFromRecentFlightSearch = false;

        mMainActivityCtx.isComingFromFilterFrag = false;
        mMainActivityCtx.isComingFromFlightListFrag = false;
        mMainActivityCtx.isComingFromSideLTC = false;
        mMainActivityCtx.isComingFromFareQuote = false;
        mMainActivityCtx.isComingFromRoundFilterDialog = false;
        mMainActivityCtx.userSelectedUniqueIdOfFlight = -1;
        mMainActivityCtx.isComingFromPassengerListPage = false;
        mMainActivityCtx.isAirAsia = false;
        mMainActivityCtx.isGdsOrInternatioal = false;

    }

    private static ArrayList<HistoryBean> getAllBookedHistory() {

        AirDatabase database = new AirDatabase(mMainActivityCtx);
        database.open();
        ArrayList<HistoryBean> historyList = database.getBookingHistory();
        database.close();
        return historyList;
    }

    private static void deleteBookingHistory() {

        AirDatabase database = new AirDatabase(mMainActivityCtx);
        database.open();
        database.clearBookCancelHistory();
        database.close();

    }

}
