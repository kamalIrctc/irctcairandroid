package com.irctc.air.eventhandler;

import android.content.Context;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;

import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.fragment.FragmentPlanner;
import com.irctc.air.model.AirDataModel;
import com.irctc.air.model.RecentFlightSearchBean;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.ListEventHandler;
import com.irctc.air.util.ProjectUtil;

import java.util.ArrayList;

/**
 * Created by vivek on 04/23/2016.
 */
public class ListItemClickListener implements AdapterView.OnItemClickListener {


    AppCompatActivity lObjAppCompatActivity;
    DrawerLayout lObjDrawerLayout;
    private ListEventHandler eventHandler;
    ArrayList<RecentFlightSearchBean> alRecentFlightDetailBean;
    ActivityMain mainActivty;
    ArrayList<AirDataModel> mAirDataHolder;
//    ArrayList<PassDetailbean> mAlPassList;

    public ListItemClickListener(Context ctx, ListEventHandler eventHandler) {


        // alStation = EcateringDataHolder.getListHolder().getList().get(0).getAlTrain();
        lObjAppCompatActivity = (AppCompatActivity) ctx;
        this.eventHandler = eventHandler;


    }

    public ListItemClickListener(ActivityMain ctx, ListEventHandler eventHandler, ArrayList<RecentFlightSearchBean> alRecentFlightBean, ArrayList<AirDataModel> mAirDataHolder) {

        this.alRecentFlightDetailBean = alRecentFlightBean;
        this.mAirDataHolder = mAirDataHolder;
        lObjAppCompatActivity = (AppCompatActivity) ctx;
        this.eventHandler = eventHandler;
        this.mainActivty = ctx;

    }

//    public ListItemClickListener(ActivityMain ctx, ListEventHandler eventHandler, ArrayList<AirDataModel> mAirDataHolder, ArrayList<PassDetailbean> mAlPassListBean, int value ) {
//
//        this.mAirDataHolder = mAirDataHolder;
//        lObjAppCompatActivity = (AppCompatActivity) ctx;
//        this.eventHandler = eventHandler;
//        mAlPassList = mAlPassListBean;
//        this.mainActivty = ctx;
//
//    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


        switch (eventHandler) {
            case RECENT_SEARCH_LIST:

                /**
                 * Vivek 23 aPR 2016
                 * - HANDLE CLICK ON RECENT SEARCH LISTVIEW
                 * - Call Planner fragment
                 */

                // Set value in holder, it will populate recent data in planner fragmnet

                mAirDataHolder.get(0).setFromStation(alRecentFlightDetailBean.get(position).getRecFromStationCode());
                mAirDataHolder.get(0).setFromStationCity(alRecentFlightDetailBean.get(position).getRecFromStationName());
                mAirDataHolder.get(0).setToStation(alRecentFlightDetailBean.get(position).getRecToStationCode());
                mAirDataHolder.get(0).setToStationCity(alRecentFlightDetailBean.get(position).getRecToStationName());

                mAirDataHolder.get(0).setAdultPassNum(Integer.parseInt(alRecentFlightDetailBean.get(position).getRecPassAdoultCount()));
                mAirDataHolder.get(0).setChildPassNum(Integer.parseInt(alRecentFlightDetailBean.get(position).getRecPassChildCount()));
                mAirDataHolder.get(0).setInfantPassNum(Integer.parseInt(alRecentFlightDetailBean.get(position).getRecPassInfantCount()));

                mAirDataHolder.get(0).setDepDate(alRecentFlightDetailBean.get(position).getRecDepDate());
                mAirDataHolder.get(0).setReturnDate(alRecentFlightDetailBean.get(position).getRecReturnDate());

                mAirDataHolder.get(0).setTripType(alRecentFlightDetailBean.get(position).getRecOneWayOrRoundTrip());

                if(alRecentFlightDetailBean.get(position).getRecOneWayOrRoundTrip().equalsIgnoreCase("One way")) {
                    mainActivty.isOneWaySelected = true;
                }else{
                    mainActivty.isOneWaySelected = false;
                }

                // Set main activity variables
                if(alRecentFlightDetailBean.get(position).getRecTripClassEorB().equalsIgnoreCase("E")) {
                    mainActivty.isEconomyClassSelected = true;
                }else{
                    mainActivty.isEconomyClassSelected = false;
                }

                // Set isComingFromRecentFlightSearch = true;
                mainActivty.isComingFromRecentFlightSearch = true;

                // Send back to planner fragment
                 ProjectUtil.replaceFragment(mainActivty, new FragmentPlanner(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);



                break;



//            /*
//            * hANDLING OF MULTI PASSENGER FROM LIST SELECTION
//            * */
//            case MULTI_PASS_SELECTION_LIST:
//
//                // mAlPassList  ???????????
//
//
//                break;

            default:
                break;
        }


    }


}
