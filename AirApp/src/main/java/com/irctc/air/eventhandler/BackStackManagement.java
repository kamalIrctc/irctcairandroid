package com.irctc.air.eventhandler;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.irctc.air.R;
import com.irctc.air.fragment.BookHistoryInfoFragment;
import com.irctc.air.fragment.BookingHistoryFragment;
import com.irctc.air.fragment.CancelTicketFragment;
import com.irctc.air.fragment.ConfirmTicketFragment;
import com.irctc.air.fragment.FragmentReprice;
import com.irctc.air.fragment.FragmentInternationalRoundTrip;
import com.irctc.air.fragment.FragmentOneWayFlight;
import com.irctc.air.fragment.FragmentPlanner;
import com.irctc.air.fragment.FragmentRoundTripDomestic;
import com.irctc.air.fragment.FragmentPassengerList;
import com.irctc.air.fragment.FragmentAddPassengers;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.util.AlertDialogUtil;
import com.irctc.air.util.Constant;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.ProjectUtil;

/**
 * Created by vivek on 04/13/2016
 * This class is use to handle on back press event to check which one is the active fragment and which one need to load.
 */
public class BackStackManagement {

    private Context context;
    private ActivityMain mainActivity;

    public BackStackManagement(Context context) {
        this.context = context;
        this.mainActivity =(ActivityMain) context;
    }


    public void initFragment(int position) {

        switch (position) {
            case 0: //Planner fragment(Default)

                showAlertDialog();
                break;

            case 1: // Planner Fragment
                showAlertDialog();
                break;

            case 2: // Station Search
                ProjectUtil.replaceFragment(context, new FragmentPlanner(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                break;

            case 3: // Calender fragment

                ProjectUtil.replaceFragment(context, new FragmentPlanner(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                break;

            case Constant.RECENT_SEARCH_FRAGMENT: // Recent searches fragment
                if(mainActivity.lastActiveFragment == Constant.ONEWAY_FLIGHT_FRAGMENT){
                    ProjectUtil.replaceFragment(context, new FragmentOneWayFlight(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                }else if(mainActivity.lastActiveFragment == Constant.PLANNER_FRAGMENT){
                    ProjectUtil.replaceFragment(context, new FragmentPlanner(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                }else if(mainActivity.lastActiveFragment == Constant.CONFIRM_TICKET_FRAGMENT){
                    ProjectUtil.replaceFragment(context, new ConfirmTicketFragment(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                }else if(mainActivity.lastActiveFragment == Constant.FRAGMENT_ROUND_TRIP){
                    ProjectUtil.replaceFragment(context, new FragmentRoundTripDomestic(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                }else if(mainActivity.lastActiveFragment == Constant.FRAGMENT_INTERNATION_ROUND_TRIP){
                    ProjectUtil.replaceFragment(context, new FragmentInternationalRoundTrip(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                }
                break;




            case Constant.ONEWAY_FLIGHT_FRAGMENT: // one way flight search fragment

                ProjectUtil.replaceFragment(context, new FragmentPlanner(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                break;
            case Constant.FRAGMENT_ROUND_TRIP:  // Fare Quote round trip fragment

                ProjectUtil.replaceFragment(context, new FragmentRoundTripDomestic(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

                break;
            case Constant.FRAGMENT_INTERNATION_ROUND_TRIP: // one way flight search fragment

                ProjectUtil.replaceFragment(context, new FragmentPlanner(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                break;

            case Constant.FARE_QUOTE_FRAGMENT:  // Fare Quote fragment

                if(mainActivity.FARE_QUOTE_BACK_HANDLER_VAR == Constant.ONEWAY_FLIGHT_FRAGMENT){

                    ProjectUtil.replaceFragment(context, new FragmentOneWayFlight(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

                }else  if(mainActivity.FARE_QUOTE_BACK_HANDLER_VAR == Constant.FRAGMENT_ROUND_TRIP){

                    mainActivity.isGdsOrInternatioal = false;
                    ProjectUtil.replaceFragment(context, new FragmentRoundTripDomestic(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);


                }else  if(mainActivity.FARE_QUOTE_BACK_HANDLER_VAR == Constant.FRAGMENT_INTERNATION_ROUND_TRIP) {

                    mainActivity.isGdsOrInternatioal = false;
                    ProjectUtil.replaceFragment(context, new FragmentInternationalRoundTrip(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

                }

                break;

            case Constant.LOGIN_FRAGMENT: // Login fragment
                if(mainActivity.lastActiveFragment == Constant.FARE_QUOTE_FRAGMENT){
                    mainActivity.lastActiveFragment = Constant.LOGIN_FRAGMENT;
                    ProjectUtil.replaceFragment(context, new FragmentReprice(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                }else{
                    ProjectUtil.replaceFragment(context, new FragmentPlanner(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                }


                break;

            case Constant.ONEWAY_FILTER_FRAGMENT: // Filter fragment

                ProjectUtil.replaceFragment(context, new FragmentOneWayFlight(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

                break;
            case Constant.BOOKING_HISTORY_FRAGMENT:

                ProjectUtil.replaceFragment(context, new FragmentPlanner(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

                break;
            case Constant.ADD_PASS_FRAGMENT: // ADD PASSENGER FRAG

                ProjectUtil.replaceFragment(context, new FragmentReprice(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

                break;
            case Constant.PASS_SIDE_LIST_FRAGMENT: // PASSENGER side list fragment

                ProjectUtil.replaceFragment(context, new FragmentAddPassengers(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

                break;
            case Constant.PAYMENT_WEBVIEW_FRAGMENT: // Contact us fragment

                new AlertDialogUtil(mainActivity, mainActivity.getResources().getString(R.string.payment_to_planner_msge), mainActivity.getResources().getString(R.string.payment_details), Constant.ALERT_ACTION_TWO, new FragmentPlanner()).generateAlert();

                break;
            case Constant.RETURN_FLIGHT_FRAGMENT: // About us fragment

                ProjectUtil.replaceFragment(context, new FragmentPlanner(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

                break;
            case Constant.WEBVIEW_FRAGMENT: // About us fragment

                // added by vivek
               // ProjectUtil.replaceFragment(context, new FragmentPlanner(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

                // new added by asif
                if(ActivityMain.lastActiveFragment == Constant.TERM_AND_CONDITIONS){

                     ProjectUtil.replaceFragment(context, new FragmentAddPassengers(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                }
                else{
                     ProjectUtil.replaceFragment(context, new FragmentPlanner(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

                }

                break;
            case Constant.BOOKING_HISTORY_INFO_FRAGMENT: // About us fragment

                ProjectUtil.replaceFragment(context, new BookingHistoryFragment(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

                break;

            case Constant.MY_PROFILE: // About us fragment

                ProjectUtil.replaceFragment(context, new FragmentPlanner(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

                break;

            case Constant.CONFIRM_TICKET_FRAGMENT:

                showAlertDialogAndRedirctToplanner();

                break;

            case Constant.PAYMENT_FAILED_FRAGMENT:

                showAlertDialogAndRedirctToplanner();

                break;

            case Constant.ADD_MODIFY_PASS_FRAGMENT:

                ProjectUtil.replaceFragment(context, new FragmentPassengerList(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

                break;

            case Constant.CANCEL_TICKET_FRAGMENT:

                // BookHistoryInfoFragment
                Fragment bookHistoryInfofragment = new BookHistoryInfoFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("ClickedBookedFlightPos", CancelTicketFragment.position);
                bundle.putString("commingFrom", "Booking");
                bookHistoryInfofragment.setArguments(bundle);

                ProjectUtil.replaceFragment(mainActivity, bookHistoryInfofragment , R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

                break;
            default:

                //Do nothing
                break;
        }
    }

    private void showAlertDialog() {

        //  AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

        // Setting Dialog Title
        alertDialog.setTitle(R.string.app_name);

        // Setting Dialog Message
        alertDialog.setMessage(R.string.EXIT);

//      // Setting Icon to Dialog
//      alertDialog.setIcon(R.drawable.adult);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                ((AppCompatActivity) context).finish();
                ///((TBaseActivity) context).onFinish();
            }
        });

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });

        // Showing Alert Message
        ProjectUtil.dialogColorAlert(alertDialog);
    }


    private void showAlertDialogAndRedirctToplanner() {

        //  AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

        // Setting Dialog Title
        alertDialog.setTitle(R.string.app_name);

        // Setting Dialog Message
        alertDialog.setMessage(R.string.CONFIRM_BACK_MESSAGE);

        // Setting Icon to Dialog
        alertDialog.setIcon(R.drawable.adult);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {


                mainActivity.lastActiveFragment = Constant.CONFIRM_TICKET_FRAGMENT;
                ProjectUtil.replaceFragment(context, new FragmentPlanner(), R.id.frame_layout, EnumAnimation.LEFT_TO_RIGHT);
            }
        });

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to invoke NO event
                dialog.cancel();
            }
        });

        // Showing Alert Message
        ProjectUtil.dialogColorAlert(alertDialog);
    }
}
