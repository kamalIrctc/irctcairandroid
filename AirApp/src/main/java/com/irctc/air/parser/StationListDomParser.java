package com.irctc.air.parser;

import android.content.Context;

import com.irctc.air.R;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by vivek on 4/22/2016.
 */



public class StationListDomParser {

    //No generics
    ArrayList alStationList;
    Context mObjCtx;


    public StationListDomParser(Context ctx) {
        //create a list to hold the employee objects
        alStationList = new ArrayList();
        mObjCtx = ctx;
    }


    public ArrayList getListFromXML() {
        try {
            InputSource is = new InputSource(mObjCtx.getResources().openRawResource(R.raw.airport_list));
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = null;

            db = dbf.newDocumentBuilder();

            Document doc = null;
            try {
                doc = db.parse(new InputSource(is.getByteStream()));
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            doc.getDocumentElement().normalize();

            String stationName;

//
//            <Airport Code="IXA">
//            <Name>AGARTALA,IN</Name>
//            </Airport>

            NodeList nodeList = doc.getElementsByTagName("Airport");


            for(int i = 0; i<nodeList.getLength();i++)
            {
                //stationName = nodeList.item(0).getAttributes("Code");
                //stationName = nodeList.item(i).getElementsByTagName


                // <Airport Code="IN-BLR"><Name>Bangalore</Name></Airport>

                Node node = nodeList.item(i);
                Element fstElmnt = (Element) node;

                StringBuilder name = new StringBuilder();
                name.append(fstElmnt.getElementsByTagName("Name").item(0).getTextContent().toString().replace(",", " "));
                name.append(" ");

                String code[] = fstElmnt.getAttribute("Code").split("-");


                name.append(code[0]);

                alStationList.add(name+", "+code[1]);



//                for(int i = 0; i<nodeList.getLength();i++)
//                {
//
//                    Node node = nodeList.item(i);
//                    Element fstElmnt = (Element) node;
//
//                    String airportCode = null;
//                    String airportName = null;
//
//                    if(fstElmnt.getAttribute("label").contains("-")) {
//                        String[] lStSplitStations = fstElmnt.getAttribute("label").split("-");
//                        airportCode = lStSplitStations[1];
//                        airportName = lStSplitStations[0].replace(",", " ");
//                    }else{
//                        String[] lStSplitStations = fstElmnt.getAttribute("label").split(",");
//                        airportName = lStSplitStations[0];
//                        airportCode = lStSplitStations[1];
//                    }
//
//                    alStationList.add(airportName+", "+airportCode);
//
//
//                }
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        return alStationList;

    }

}