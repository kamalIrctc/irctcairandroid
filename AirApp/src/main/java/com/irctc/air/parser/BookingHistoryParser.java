package com.irctc.air.parser;

import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.model.BookedHistorySegmentAndPassBean;
import com.irctc.air.model.BookingHistoryBean;
import com.irctc.air.model.FareDetailBean;
import com.irctc.air.model.MainBookedCancledHistoryBean;
import com.irctc.air.model.PassengerDetailBean;
import com.irctc.air.model.SegmentDetailBean;
import com.irctc.air.util.AirlineLogoUtil;
import com.irctc.air.util.AppLogger;
import com.irctc.air.util.DateUtility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BookingHistoryParser {

    JSONObject mJsonObject;

    public BookingHistoryParser(String lresponseStr) {

        try {
            this.mJsonObject  = new JSONObject(lresponseStr);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    public void bookingHistoryResponseParser() {

        //  CLEAR THE BOOKING HISTORY & CANCEL HISTORY
        if(AirDataHolder.getListHolder().getList().get(0).getBookedCancelHistory() != null) {
            AirDataHolder.getListHolder().getList().get(0).setBookedCancelHistory(null);
        }

        try {

            AppLogger.enable();
            JSONArray bookedCancelJsonArray = mJsonObject.getJSONArray("bookingHistory");

            // CREATE INSTANCE OF MainBookedCancledHistoryBean TO SET BOOKED HISTORY
            MainBookedCancledHistoryBean mainBookedCancledHistoryBean = new MainBookedCancledHistoryBean();

                // FOR TOP MOST LOOP
                for (int i = 0; i < bookedCancelJsonArray.length(); i++) {

                    AppLogger.e("bookedCancelJsonArray ",""+i);
                    if(i == 0){

                        JSONObject bookedJsonObj = (JSONObject) bookedCancelJsonArray.get(i);

                        if(bookedJsonObj.has("booked")){

                            JSONArray bookedJsonArray = bookedJsonObj.getJSONArray("booked");

                            // FOR TOP MOST LOOP BOOKED HISTORY
                            for (int bookedIndex = 0; bookedIndex < bookedJsonArray.length(); bookedIndex++) {

                                JSONObject lbookedJson = (JSONObject) bookedJsonArray.get(bookedIndex);

                                // CREATE BookingHistoryBean OBJECT
                                BookingHistoryBean bookingHistoryBean = new BookingHistoryBean();
                                {
                                    // set data in BookingHistoryBean
                                    AppLogger.e("BookingHistoryBean ",""+bookedIndex);

                                    bookingHistoryBean.setBrf(lbookedJson.optString("brf"));
                                    bookingHistoryBean.setOrigin(lbookedJson.optString("origin"));
                                    bookingHistoryBean.setOrig(lbookedJson.optString("orig"));
                                    bookingHistoryBean.setDestination(lbookedJson.optString("destination"));
                                    bookingHistoryBean.setDest(lbookedJson.optString("dest"));
                                    bookingHistoryBean.setTripType(lbookedJson.optString("tripType"));
                                    bookingHistoryBean.setNoofpass(lbookedJson.optString("noofpass"));
                                    bookingHistoryBean.setNoofseg(lbookedJson.optString("noofseg"));
                                    bookingHistoryBean.setTrnId(lbookedJson.optString("trnId"));
                                    bookingHistoryBean.setBookingstatus(lbookedJson.optString("bookingstatus"));
                                    bookingHistoryBean.setBookingstatusValue(lbookedJson.optString("bookingstatusVal"));
                                    bookingHistoryBean.setBookingdate(DateUtility.getDateFromBookingHistoryRes(lbookedJson.optString("bookingdate")));
                                    bookingHistoryBean.setBookeradd1(lbookedJson.optString("bookeradd1"));
                                    bookingHistoryBean.setBookeradd2(lbookedJson.optString("bookeradd2"));
                                    bookingHistoryBean.setBookercity(lbookedJson.optString("bookercity"));
                                    bookingHistoryBean.setBookerstate(lbookedJson.optString("bookerstate"));
                                    bookingHistoryBean.setBookercountry(lbookedJson.optString("bookercountry"));
                                    bookingHistoryBean.setBookerpin(lbookedJson.optString("bookerpin"));
                                    bookingHistoryBean.setBookerphone(lbookedJson.optString("bookerphone"));
                                    bookingHistoryBean.setTktoid(lbookedJson.optString("tktoid"));
                                    bookingHistoryBean.setSegtypecan(lbookedJson.optString("segtypecan"));
                                    bookingHistoryBean.setTranoid(lbookedJson.optString("tranoid"));
                                    bookingHistoryBean.setTotalCharge(lbookedJson.optString("totalCharge"));
                                    bookingHistoryBean.setOrigCharge(lbookedJson.optString("origCharge"));
                                    bookingHistoryBean.setLtcEmpCode(lbookedJson.optString("ltcEmpCode"));
                                    bookingHistoryBean.setNoOfAdult(lbookedJson.optString("noOfAdult"));
                                    bookingHistoryBean.setNoOfChild(lbookedJson.optString("noOfChild"));
                                    bookingHistoryBean.setNoOfInfant(lbookedJson.optString("noOfInfant"));

                                }

                                /**********************************
                                * GETTING ALL THE FARE DETAILS
                                * ********************************/

                                if(lbookedJson.has("fare")){
                                    JSONArray fareJsonArray = lbookedJson.getJSONArray("fare");
                                    // FOR TOP MOST FARE LOOP
                                    for (int fareIndex = 0; fareIndex < fareJsonArray.length(); fareIndex++) {
                                        JSONObject lFareJson  = (JSONObject) fareJsonArray.get(fareIndex);

                                        FareDetailBean fareDetailBean = new FareDetailBean();
                                        {
                                            // set data in FareDetailBean
                                            AppLogger.e("FareDetailBean ",""+fareIndex);

                                            fareDetailBean.setJn(lFareJson.optString("jn"));
                                            fareDetailBean.setBfare(lFareJson.optString("bfare"));
                                            fareDetailBean.setYr(lFareJson.optString("yr"));
                                            fareDetailBean.setYq(lFareJson.optString("yq"));
                                            fareDetailBean.setTax1(lFareJson.optString("tax1"));
                                            fareDetailBean.setSubTotal(lFareJson.optString("subTotal"));
                                            fareDetailBean.setExtra(lFareJson.optString("extra"));
                                            fareDetailBean.setInc(lFareJson.optString("inc"));
                                            fareDetailBean.setWo(lFareJson.optString("wo"));
                                            fareDetailBean.setTax2(lFareJson.optString("tax2"));
                                            fareDetailBean.setDisc(lFareJson.optString("disc"));

                                            /**
                                             * 07 NOV 2016
                                             * IRCTC SERVICE CHARGE
                                             */

                                            //fareDetailBean.setIrctcTxnFees(Double.parseDouble(mJsonObject.optString("irctcTxnFees")));

                                            if (mJsonObject.has("irctcTxnFees")) {
                                                fareDetailBean.setIrctcTxnFees(Double.parseDouble(mJsonObject.optString("irctcTxnFees")));

                                            } else {
                                                fareDetailBean.setIrctcTxnFees(0.0);

                                            }

                                        }

                                        bookingHistoryBean.setAlFareDetail(fareDetailBean);
                                    }
                                }

                                 /**********************************
                                 * GETTING ALL THE ONWARD DETAILS
                                 * ********************************/

                                BookedHistorySegmentAndPassBean  lObjOnwardSegmentPassanger = new BookedHistorySegmentAndPassBean();

                                if(lbookedJson.has("onward")){

                                    JSONArray onwardJsonArray = lbookedJson.getJSONArray("onward");
                                    // FOR TOP MOST ONWARD LOOP
                                    for (int onwardIndex = 0; onwardIndex < onwardJsonArray.length() ; onwardIndex++) {

                                        AppLogger.e("ONWARD ",""+onwardIndex);
                                        JSONObject lonwardJson  = (JSONObject) onwardJsonArray.get(onwardIndex);

                                         /**********************************
                                         * GETTING ALL THE SEGEMNT DETAILS
                                         * ********************************/

                                        if(lonwardJson.has("segment")){


                                            JSONArray lSegmentJsonArray  = lonwardJson.getJSONArray("segment");
                                            // FOR TOP MOST SEGMENT LOOP
                                            for (int segmentIndex = 0; segmentIndex < lSegmentJsonArray.length(); segmentIndex++) {

                                                JSONObject lSegmentJson = (JSONObject) lSegmentJsonArray.get(segmentIndex);
                                                // CREATE INSTANCE OF SegmentDetailBean
                                                SegmentDetailBean lSegmentDetailBean = new SegmentDetailBean();
                                                {
                                                    // set data in SegmentDetailBean
                                                    AppLogger.e("SegmentDetailBean ", "" + segmentIndex);
                                                    /**
                                                     * 21 Oct 2016
                                                     * PNR - PASS STATUS
                                                     */
                                                    //lSegmentDetailBean.setAirpnr(lSegmentJson.optString("disc"));
                                                    lSegmentDetailBean.setAirpnr(lSegmentJson.optString("airpnr"));

                                                    lSegmentDetailBean.setAircontact(lSegmentJson.optString("aircontact"));
                                                    lSegmentDetailBean.setAirline(lSegmentJson.optString("airline"));
                                                    lSegmentDetailBean.setSegdest(lSegmentJson.optString("segdest"));
                                                    lSegmentDetailBean.setArriavlTime(DateUtility.getDateFromBookingHistoryRes(lSegmentJson.optString("arriavlTime")));
                                                    lSegmentDetailBean.setPc(lSegmentJson.optString("pc"));
                                                    lSegmentDetailBean.setVia(lSegmentJson.optString("via"));
                                                    lSegmentDetailBean.setSppnr(lSegmentJson.optString("sppnr"));
                                                    lSegmentDetailBean.setCabinclass(lSegmentJson.optString("cabinclass"));
                                                    lSegmentDetailBean.setOac(lSegmentJson.optString("oac"));
                                                    lSegmentDetailBean.setSegorig(lSegmentJson.optString("segorig"));
                                                    lSegmentDetailBean.setFaretype(lSegmentJson.optString("faretype"));
                                                    lSegmentDetailBean.setDepartTime(DateUtility.getDateFromBookingHistoryRes(lSegmentJson.optString("departTime")));
                                                    lSegmentDetailBean.setFno(lSegmentJson.optString("fno"));
                                                    lSegmentDetailBean.setDuration(lSegmentJson.optString("duration"));

                                                    lSegmentDetailBean.setSegorigC(lSegmentJson.optString("segorigC"));
                                                    lSegmentDetailBean.setSegdestC(lSegmentJson.optString("segdestC"));

                                                    //setFlightIcon(lSegmentJson.optString("pc"));
                                                    lSegmentDetailBean.setFlighticon(setFlightIcon(lSegmentJson.optString("pc")));

                                                    lObjOnwardSegmentPassanger.setAlSegmentDetail(lSegmentDetailBean);
                                                }
                                            }
                                        }

                                         /**********************************
                                         * GETTING ALL THE PASSANGER DETAILS
                                         * ********************************/

                                        if(lonwardJson.has("passanger")) {

                                            JSONArray passangerJsonArray  = lonwardJson.getJSONArray("passanger");
                                            // FOR TOP MOST PASSANGER LOOP
                                            for (int passangerIndex = 0; passangerIndex < passangerJsonArray.length(); passangerIndex++) {

                                                JSONObject lPassangerJson = (JSONObject) passangerJsonArray.get(passangerIndex);

                                                // CREATE INSTANCE OF PassDetailBean
                                                PassengerDetailBean passangerDetailBean = new PassengerDetailBean();
                                                {
                                                   // set data in PassDetailBean
                                                    AppLogger.e("PassDetailBean ",""+passangerIndex);
                                                    passangerDetailBean.setLname(lPassangerJson.optString("lname"));
                                                    passangerDetailBean.setCandetail(lPassangerJson.optString("candetail"));
                                                    passangerDetailBean.setAge(lPassangerJson.optString("age"));
                                                    passangerDetailBean.setFname(lPassangerJson.optString("fname"));
                                                    passangerDetailBean.setCanstatus(lPassangerJson.optString("canstatus"));
                                                    passangerDetailBean.setTicketNo(lPassangerJson.optString("ticketNo"));
                                                    passangerDetailBean.setPasgtype(lPassangerJson.optString("pasgtype"));

                                                    /**
                                                     * 07 nov 2016
                                                     * PNR - PASS STATUS
                                                     */
                                                    passangerDetailBean.setCanstatusText(lPassangerJson.optString("canstatusV"));

                                                    lObjOnwardSegmentPassanger.setAlPassengerDetail(passangerDetailBean);
                                                }
                                            }
                                        }
                                    }
                                }

                                bookingHistoryBean.setAlBookedOnwardDetail(lObjOnwardSegmentPassanger);

                                 /**********************************
                                 * GETTING ALL THE RETURN DETAILS
                                 * ********************************/

                                BookedHistorySegmentAndPassBean  lObjReturnSegmentPassanger = new BookedHistorySegmentAndPassBean();

                                if(lbookedJson.has("returnJrn")){

                                    JSONArray returnJsonArray = lbookedJson.getJSONArray("returnJrn");

                                    // FOR TOP MOST RETURN LOOP
                                    for (int returnIndex = 0; returnIndex < returnJsonArray.length() ; returnIndex++) {

                                        JSONObject lReturnJson  = (JSONObject) returnJsonArray.get(returnIndex);

                                        // GETTING ALL THE SEGEMNT DETAILS
                                        if(lReturnJson.has("segment")){

                                            JSONArray lSegmentJsonArray  = lReturnJson.getJSONArray("segment");
                                            // FOR TOP MOST SEGMENT LOOP
                                            for (int segmentIndex = 0; segmentIndex < lSegmentJsonArray.length(); segmentIndex++) {

                                                JSONObject lSegmentJson = (JSONObject) lSegmentJsonArray.get(segmentIndex);

                                                // CREATE INSTANCE OF SegmentDetailBean
                                                SegmentDetailBean lSegmentDetailBean = new SegmentDetailBean();
                                                {
                                                    // set data in SegmentDetailBean
                                                    AppLogger.e("SegmentDetailBean ",""+segmentIndex);
                                                    /**
                                                     * 21 Oct 2016
                                                     * PNR - PASS STATUS
                                                     */
                                                    //lSegmentDetailBean.setAirpnr(lSegmentJson.optString("disc"));
                                                    lSegmentDetailBean.setAirpnr(lSegmentJson.optString("airpnr"));

                                                    lSegmentDetailBean.setAircontact(lSegmentJson.optString("aircontact"));
                                                    lSegmentDetailBean.setAirline(lSegmentJson.optString("airline"));
                                                    lSegmentDetailBean.setSegdest(lSegmentJson.optString("segdest"));
                                                    lSegmentDetailBean.setArriavlTime(DateUtility.getDateFromBookingHistoryRes(lSegmentJson.optString("arriavlTime")));
                                                    lSegmentDetailBean.setPc(lSegmentJson.optString("pc"));
                                                    lSegmentDetailBean.setVia(lSegmentJson.optString("via"));
                                                    lSegmentDetailBean.setSppnr(lSegmentJson.optString("sppnr"));
                                                    lSegmentDetailBean.setCabinclass(lSegmentJson.optString("cabinclass"));
                                                    lSegmentDetailBean.setOac(lSegmentJson.optString("oac"));
                                                    lSegmentDetailBean.setSegorig(lSegmentJson.optString("segorig"));
                                                    lSegmentDetailBean.setFaretype(lSegmentJson.optString("faretype"));
                                                    lSegmentDetailBean.setDepartTime(DateUtility.getDateFromBookingHistoryRes(lSegmentJson.optString("departTime")));
                                                    lSegmentDetailBean.setFno(lSegmentJson.optString("fno"));
                                                    lSegmentDetailBean.setDuration(lSegmentJson.optString("duration"));
                                                    lSegmentDetailBean.setSegorigC(lSegmentJson.optString("segorigC"));
                                                    lSegmentDetailBean.setSegdestC(lSegmentJson.optString("segdestC"));

                                                    lSegmentDetailBean.setFlighticon(setFlightIcon(lSegmentJson.optString("pc")));

                                                    lObjReturnSegmentPassanger.setAlSegmentDetail(lSegmentDetailBean);
                                                }
                                            }
                                        }

                                        // GETTING ALL THE PASSANGER DETAILS
                                        if(lReturnJson.has("passanger")) {

                                            JSONArray passangerJsonArray  = lReturnJson.getJSONArray("passanger");
                                            // FOR TOP MOST PASSANGER LOOP
                                            for (int passangerIndex = 0; passangerIndex < passangerJsonArray.length(); passangerIndex++) {

                                                JSONObject lPassangerJson = (JSONObject) passangerJsonArray.get(passangerIndex);

                                                // CREATE INSTANCE OF PassDetailBean
                                                PassengerDetailBean passangerDetailBean = new PassengerDetailBean();
                                                {
                                                    // set data in PassDetailBean
                                                    AppLogger.e("PassDetailBean ",""+passangerIndex);
                                                    passangerDetailBean.setLname(lPassangerJson.optString("lname"));
                                                    passangerDetailBean.setCandetail(lPassangerJson.optString("candetail"));
                                                    passangerDetailBean.setAge(lPassangerJson.optString("age"));
                                                    passangerDetailBean.setFname(lPassangerJson.optString("fname"));
                                                    passangerDetailBean.setCanstatus(lPassangerJson.optString("canstatus"));
                                                    passangerDetailBean.setTicketNo(lPassangerJson.optString("ticketNo"));
                                                    passangerDetailBean.setPasgtype(lPassangerJson.optString("pasgtype"));

                                                    /**
                                                     * 07 nov 2016
                                                     * PNR - PASS STATUS
                                                     */
                                                    passangerDetailBean.setCanstatusText(lPassangerJson.optString("canstatusV"));


                                                    lObjReturnSegmentPassanger.setAlPassengerDetail(passangerDetailBean);

                                                }
                                            }
                                        }
                                    }
                                }
                                bookingHistoryBean.setAlBookedReturnDetail(lObjReturnSegmentPassanger);

                                AppLogger.e("INDEX ", "" + bookedIndex);
                                // ADD EACH BOOKED BookingHistoryBean IN MainBookedCancledHistoryBean
                                mainBookedCancledHistoryBean.setBookedHistory(bookingHistoryBean);
                            }
                            // SET MainBookedCancledHistoryBean in MAIN HOLDER (AirDataHolder)
                            AirDataHolder.getListHolder().getList().get(0).setBookedCancelHistory(mainBookedCancledHistoryBean);

                        }
                    }
                    else{
                        // FOR CANCELLATION HISTORY

                        JSONObject cancelledJsonObj = (JSONObject) bookedCancelJsonArray.get(i);

                        if(cancelledJsonObj.has("cancelled")){

                            JSONArray cancelledJsonArray = cancelledJsonObj.getJSONArray("cancelled");

                            // CREATE INSTANCE OF MainBookedCancledHistoryBean TO SET BOOKED HISTORY
                           // MainBookedCancledHistoryBean mainBookedCancledHistoryBean = new MainBookedCancledHistoryBean();
                            // FOR TOP MOST LOOP BOOKED HISTORY
                            for (int cancelIndex = 0; cancelIndex < cancelledJsonArray.length(); cancelIndex++) {

                                JSONObject lcancelledJson = (JSONObject) cancelledJsonArray.get(cancelIndex);

                                // CREATE BookingHistoryBean OBJECT
                                BookingHistoryBean cancelHistoryBean = new BookingHistoryBean();
                                {
                                    // set data in BookingHistoryBean
                                    AppLogger.e("BookingHistoryBean ",""+cancelIndex);

                                    cancelHistoryBean.setBrf(lcancelledJson.optString("brf"));
                                    cancelHistoryBean.setOrigin(lcancelledJson.optString("origin"));
                                    cancelHistoryBean.setOrig(lcancelledJson.optString("orig"));
                                    cancelHistoryBean.setDestination(lcancelledJson.optString("destination"));
                                    cancelHistoryBean.setDest(lcancelledJson.optString("dest"));
                                    cancelHistoryBean.setTripType(lcancelledJson.optString("tripType"));
                                    cancelHistoryBean.setNoofpass(lcancelledJson.optString("noofpass"));
                                    cancelHistoryBean.setNoofseg(lcancelledJson.optString("noofseg"));
                                    cancelHistoryBean.setTrnId(lcancelledJson.optString("trnId"));
                                    cancelHistoryBean.setBookingstatus(lcancelledJson.optString("bookingstatus"));
                                    cancelHistoryBean.setBookingstatusValue(lcancelledJson.optString("bookingstatusVal"));
                                    cancelHistoryBean.setBookingdate(DateUtility.getDateFromBookingHistoryRes(lcancelledJson.optString("bookingdate")));
                                    cancelHistoryBean.setBookeradd1(lcancelledJson.optString("bookeradd1"));
                                    cancelHistoryBean.setBookeradd2(lcancelledJson.optString("bookeradd2"));
                                    cancelHistoryBean.setBookercity(lcancelledJson.optString("bookercity"));
                                    cancelHistoryBean.setBookerstate(lcancelledJson.optString("bookerstate"));
                                    cancelHistoryBean.setBookercountry(lcancelledJson.optString("bookercountry"));
                                    cancelHistoryBean.setBookerpin(lcancelledJson.optString("bookerpin"));
                                    cancelHistoryBean.setBookerphone(lcancelledJson.optString("bookerphone"));
                                    cancelHistoryBean.setTktoid(lcancelledJson.optString("tktoid"));
                                    cancelHistoryBean.setSegtypecan(lcancelledJson.optString("segtypecan"));
                                    cancelHistoryBean.setTranoid(lcancelledJson.optString("tranoid"));
                                    cancelHistoryBean.setTotalCharge(lcancelledJson.optString("totalCharge"));
                                    cancelHistoryBean.setOrigCharge(lcancelledJson.optString("origCharge"));
                                    cancelHistoryBean.setLtcEmpCode(lcancelledJson.optString("ltcEmpCode"));
                                    cancelHistoryBean.setNoOfAdult(lcancelledJson.optString("noOfAdult"));
                                    cancelHistoryBean.setNoOfChild(lcancelledJson.optString("noOfChild"));
                                    cancelHistoryBean.setNoOfInfant(lcancelledJson.optString("noOfInfant"));

                                }

                                /**********************************
                                 * GETTING ALL THE FARE DETAILS
                                 * ********************************/

                                if(lcancelledJson.has("fare")){
                                    JSONArray fareJsonArray = lcancelledJson.getJSONArray("fare");
                                    // FOR TOP MOST FARE LOOP
                                    for (int fareIndex = 0; fareIndex < fareJsonArray.length(); fareIndex++) {
                                        JSONObject lFareJson  = (JSONObject) fareJsonArray.get(fareIndex);

                                        FareDetailBean fareDetailBean = new FareDetailBean();
                                        {
                                            // set data in FareDetailBean
                                            AppLogger.e("FareDetailBean ",""+fareIndex);

                                            fareDetailBean.setJn(lFareJson.optString("jn"));
                                            fareDetailBean.setBfare(lFareJson.optString("bfare"));
                                            fareDetailBean.setYr(lFareJson.optString("yr"));
                                            fareDetailBean.setYq(lFareJson.optString("yq"));
                                            fareDetailBean.setTax1(lFareJson.optString("tax1"));
                                            fareDetailBean.setSubTotal(lFareJson.optString("subTotal"));
                                            fareDetailBean.setExtra(lFareJson.optString("extra"));
                                            fareDetailBean.setInc(lFareJson.optString("inc"));
                                            fareDetailBean.setWo(lFareJson.optString("wo"));
                                            fareDetailBean.setTax2(lFareJson.optString("tax2"));
                                            fareDetailBean.setDisc(lFareJson.optString("disc"));
                                        }
                                        cancelHistoryBean.setAlFareDetail(fareDetailBean);
                                    }
                                }

                                /**********************************
                                 * GETTING ALL THE ONWARD DETAILS
                                 * ********************************/

                                BookedHistorySegmentAndPassBean  lObjOnwardSegmentPassanger = new BookedHistorySegmentAndPassBean();

                                if(lcancelledJson.has("onward")){

                                    JSONArray onwardJsonArray = lcancelledJson.getJSONArray("onward");
                                    // FOR TOP MOST ONWARD LOOP
                                    for (int onwardIndex = 0; onwardIndex < onwardJsonArray.length() ; onwardIndex++) {

                                        AppLogger.e("ONWARD ",""+onwardIndex);
                                        JSONObject lonwardJson  = (JSONObject) onwardJsonArray.get(onwardIndex);

                                        /**********************************
                                         * GETTING ALL THE SEGEMNT DETAILS
                                         * ********************************/

                                        if(lonwardJson.has("segment")){


                                            JSONArray lSegmentJsonArray  = lonwardJson.getJSONArray("segment");
                                            // FOR TOP MOST SEGMENT LOOP
                                            for (int segmentIndex = 0; segmentIndex < lSegmentJsonArray.length(); segmentIndex++) {

                                                JSONObject lSegmentJson = (JSONObject) lSegmentJsonArray.get(segmentIndex);
                                                // CREATE INSTANCE OF SegmentDetailBean
                                                SegmentDetailBean lSegmentDetailBean = new SegmentDetailBean();
                                                {
                                                    // set data in SegmentDetailBean
                                                    AppLogger.e("SegmentDetailBean ",""+segmentIndex);

                                                    /**
                                                     * 21 Oct 2016
                                                     * PNR - PASS STATUS
                                                     */
                                                    //lSegmentDetailBean.setAirpnr(lSegmentJson.optString("disc"));
                                                    lSegmentDetailBean.setAirpnr(lSegmentJson.optString("airpnr"));

                                                    lSegmentDetailBean.setAircontact(lSegmentJson.optString("aircontact"));
                                                    lSegmentDetailBean.setAirline(lSegmentJson.optString("airline"));
                                                    lSegmentDetailBean.setSegdest(lSegmentJson.optString("segdest"));
                                                    lSegmentDetailBean.setArriavlTime(DateUtility.getDateFromBookingHistoryRes(lSegmentJson.optString("arriavlTime")));
                                                    lSegmentDetailBean.setPc(lSegmentJson.optString("pc"));
                                                    lSegmentDetailBean.setVia(lSegmentJson.optString("via"));
                                                    lSegmentDetailBean.setSppnr(lSegmentJson.optString("sppnr"));
                                                    lSegmentDetailBean.setCabinclass(lSegmentJson.optString("cabinclass"));
                                                    lSegmentDetailBean.setOac(lSegmentJson.optString("oac"));
                                                    lSegmentDetailBean.setSegorig(lSegmentJson.optString("segorig"));
                                                    lSegmentDetailBean.setFaretype(lSegmentJson.optString("faretype"));
                                                    lSegmentDetailBean.setDepartTime(DateUtility.getDateFromBookingHistoryRes(lSegmentJson.optString("departTime")));
                                                    lSegmentDetailBean.setFno(lSegmentJson.optString("fno"));
                                                    lSegmentDetailBean.setDuration(lSegmentJson.optString("duration"));

                                                    lSegmentDetailBean.setFlighticon(setFlightIcon(lSegmentJson.optString("pc")));

                                                    lSegmentDetailBean.setSegorigC(lSegmentJson.optString("segorigC"));
                                                    lSegmentDetailBean.setSegdestC(lSegmentJson.optString("segdestC"));


                                                    lObjOnwardSegmentPassanger.setAlSegmentDetail(lSegmentDetailBean);
                                                }
                                            }
                                        }

                                        /**********************************
                                         * GETTING ALL THE PASSANGER DETAILS
                                         * ********************************/

                                        if(lonwardJson.has("passanger")) {

                                            JSONArray passangerJsonArray  = lonwardJson.getJSONArray("passanger");
                                            // FOR TOP MOST PASSANGER LOOP
                                            for (int passangerIndex = 0; passangerIndex < passangerJsonArray.length(); passangerIndex++) {

                                                JSONObject lPassangerJson = (JSONObject) passangerJsonArray.get(passangerIndex);

                                                // CREATE INSTANCE OF PassDetailBean
                                                PassengerDetailBean passangerDetailBean = new PassengerDetailBean();
                                                {
                                                    // set data in PassDetailBean
                                                    AppLogger.e("PassDetailBean ",""+passangerIndex);
                                                    passangerDetailBean.setLname(lPassangerJson.optString("lname"));
                                                    passangerDetailBean.setCandetail(lPassangerJson.optString("candetail"));
                                                    passangerDetailBean.setAge(lPassangerJson.optString("age"));
                                                    passangerDetailBean.setFname(lPassangerJson.optString("fname"));
                                                    passangerDetailBean.setCanstatus(lPassangerJson.optString("canstatus"));
                                                    passangerDetailBean.setTicketNo(lPassangerJson.optString("ticketNo"));
                                                    passangerDetailBean.setPasgtype(lPassangerJson.optString("pasgtype"));

                                                    /**
                                                     * 07 nov 2016
                                                     * PNR - PASS STATUS
                                                     */
                                                    passangerDetailBean.setCanstatusText(lPassangerJson.optString("canstatusV"));


                                                    lObjOnwardSegmentPassanger.setAlPassengerDetail(passangerDetailBean);
                                                }
                                            }
                                        }
                                    }
                                }

                                cancelHistoryBean.setAlBookedOnwardDetail(lObjOnwardSegmentPassanger);

                                /**********************************
                                 * GETTING ALL THE RETURN DETAILS
                                 * ********************************/

                                BookedHistorySegmentAndPassBean  lObjReturnSegmentPassanger = new BookedHistorySegmentAndPassBean();

                                if(lcancelledJson.has("returnJrn")){

                                    JSONArray returnJsonArray = lcancelledJson.getJSONArray("returnJrn");

                                    // FOR TOP MOST RETURN LOOP
                                    for (int returnIndex = 0; returnIndex < returnJsonArray.length() ; returnIndex++) {

                                        JSONObject lReturnJson  = (JSONObject) returnJsonArray.get(returnIndex);

                                        // GETTING ALL THE SEGEMNT DETAILS
                                        if(lReturnJson.has("segment")){

                                            JSONArray lSegmentJsonArray  = lReturnJson.getJSONArray("segment");
                                            // FOR TOP MOST SEGMENT LOOP
                                            for (int segmentIndex = 0; segmentIndex < lSegmentJsonArray.length(); segmentIndex++) {

                                                JSONObject lSegmentJson = (JSONObject) lSegmentJsonArray.get(segmentIndex);

                                                // CREATE INSTANCE OF SegmentDetailBean
                                                SegmentDetailBean lSegmentDetailBean = new SegmentDetailBean();
                                                {
                                                    // set data in SegmentDetailBean
                                                    AppLogger.e("SegmentDetailBean ",""+segmentIndex);
                                                    /**
                                                     * 21 Oct 2016
                                                     * PNR - PASS STATUS
                                                     */
                                                    //lSegmentDetailBean.setAirpnr(lSegmentJson.optString("disc"));
                                                    lSegmentDetailBean.setAirpnr(lSegmentJson.optString("airpnr"));
                                                    
                                                    lSegmentDetailBean.setAircontact(lSegmentJson.optString("aircontact"));
                                                    lSegmentDetailBean.setAirline(lSegmentJson.optString("airline"));
                                                    lSegmentDetailBean.setSegdest(lSegmentJson.optString("segdest"));
                                                    lSegmentDetailBean.setArriavlTime(DateUtility.getDateFromBookingHistoryRes(lSegmentJson.optString("arriavlTime")));
                                                    lSegmentDetailBean.setPc(lSegmentJson.optString("pc"));
                                                    lSegmentDetailBean.setVia(lSegmentJson.optString("via"));
                                                    lSegmentDetailBean.setSppnr(lSegmentJson.optString("sppnr"));
                                                    lSegmentDetailBean.setCabinclass(lSegmentJson.optString("cabinclass"));
                                                    lSegmentDetailBean.setOac(lSegmentJson.optString("oac"));
                                                    lSegmentDetailBean.setSegorig(lSegmentJson.optString("segorig"));
                                                    lSegmentDetailBean.setFaretype(lSegmentJson.optString("faretype"));
                                                    lSegmentDetailBean.setDepartTime(DateUtility.getDateFromBookingHistoryRes(lSegmentJson.optString("departTime")));
                                                    lSegmentDetailBean.setFno(lSegmentJson.optString("fno"));
                                                    lSegmentDetailBean.setDuration(lSegmentJson.optString("duration"));

                                                    lSegmentDetailBean.setFlighticon(setFlightIcon(lSegmentJson.optString("pc")));

                                                    lSegmentDetailBean.setSegorigC(lSegmentJson.optString("segorigC"));
                                                    lSegmentDetailBean.setSegdestC(lSegmentJson.optString("segdestC"));

                                                    lObjReturnSegmentPassanger.setAlSegmentDetail(lSegmentDetailBean);
                                                }
                                            }
                                        }

                                        // GETTING ALL THE PASSANGER DETAILS
                                        if(lReturnJson.has("passanger")) {

                                            JSONArray passangerJsonArray  = lReturnJson.getJSONArray("passanger");
                                            // FOR TOP MOST PASSANGER LOOP
                                            for (int passangerIndex = 0; passangerIndex < passangerJsonArray.length(); passangerIndex++) {

                                                JSONObject lPassangerJson = (JSONObject) passangerJsonArray.get(passangerIndex);

                                                // CREATE INSTANCE OF PassDetailBean
                                                PassengerDetailBean passangerDetailBean = new PassengerDetailBean();
                                                {
                                                    // set data in PassDetailBean
                                                    AppLogger.e("PassDetailBean ",""+passangerIndex);
                                                    passangerDetailBean.setLname(lPassangerJson.optString("lname"));
                                                    passangerDetailBean.setCandetail(lPassangerJson.optString("candetail"));
                                                    passangerDetailBean.setAge(lPassangerJson.optString("age"));
                                                    passangerDetailBean.setFname(lPassangerJson.optString("fname"));
                                                    passangerDetailBean.setCanstatus(lPassangerJson.optString("canstatus"));
                                                    passangerDetailBean.setTicketNo(lPassangerJson.optString("ticketNo"));
                                                    passangerDetailBean.setPasgtype(lPassangerJson.optString("pasgtype"));

                                                    /**
                                                     * 07 nov 2016
                                                     * PNR - PASS STATUS
                                                     */
                                                    passangerDetailBean.setCanstatusText(lPassangerJson.optString("canstatusV"));

                                                    lObjReturnSegmentPassanger.setAlPassengerDetail(passangerDetailBean);
                                                }
                                            }
                                        }
                                    }
                                }
                                cancelHistoryBean.setAlBookedReturnDetail(lObjReturnSegmentPassanger);

                                AppLogger.e("INDEX ", "" + cancelIndex);
                                // ADD EACH BOOKED BookingHistoryBean IN MainBookedCancledHistoryBean
                                mainBookedCancledHistoryBean.setCancelHistory(cancelHistoryBean);
                            }
                            // SET MainBookedCancledHistoryBean in MAIN HOLDER (AirDataHolder)
                            AirDataHolder.getListHolder().getList().get(0).setBookedCancelHistory(mainBookedCancledHistoryBean);
                        }
                    }
                }
        } catch (Exception e) {
            e.printStackTrace();
            AppLogger.e("Exception Occoured ", "" + e);
        }
    }

    private int setFlightIcon(String pc) {

        return AirlineLogoUtil.getAirlineLogo(pc);


//        if(pc.equals("0S")){
//            return R.drawable.spicejet_logo;
//        }
//        else if(pc.equals("G8")){
//            return R.drawable.goair_logo;
//        }
//        else if(pc.equals("6E")){
//            return R.drawable.indigo_logo;
//        }
//        else if(pc.equals("AK")){
//            return R.drawable.airasia;
//        }
//        else if(pc.equals("AI")){
//            return R.drawable.airindia;
//        }
//        else if(pc.equals("UK")){
//            return R.drawable.vistara;
//        }
//        else if(pc.equals("S2")){
//            return R.drawable.ijetkonnect;
//        }
//        else if(pc.equals("9W")){
//            return R.drawable.jetair_logo;
//        }else{
//            return  R.drawable.onward_icon;
//        }
    }

    private void bookingHistoryDatabaseResponseParser(){

    }





}

