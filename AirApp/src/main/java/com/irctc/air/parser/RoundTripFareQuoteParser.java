package com.irctc.air.parser;

import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.model.FareQuoteInnerFlightDetailsBeans;
import com.irctc.air.model.FareQuoteOneWayBean;
import com.irctc.air.model.FareQuoteRoundTripOuterBean;
import com.irctc.air.util.AirlineLogoUtil;
import com.irctc.air.util.DateUtility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by tourism on 4/30/2016.
 */
public class RoundTripFareQuoteParser {

    JSONObject mJsonObject;

    public RoundTripFareQuoteParser(String lresponseStr) {

        try {
            this.mJsonObject  = new JSONObject(lresponseStr);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void fareQuoteResponseParser() {

        try {


            //CLEAR FARE QUOTE DATA FROM HOLDER
            AirDataHolder.getListHolder().getList().get(0).setOnwFlightSecurityToken(null);
            AirDataHolder.getListHolder().getList().get(0).setOnwFlightXMLSegment(null);
            AirDataHolder.getListHolder().getList().get(0).setOnwFlightJsonSegment(null);
            AirDataHolder.getListHolder().getList().get(0).setmFareQuoteOneWayBean(null);
            AirDataHolder.getListHolder().getList().get(0).setmFareQuoteRoundTripOuterBean(null);


            /***********************************************************************************************************
             * FareQuoteRoundTripOuterBean bean is used to save two fareqoute bean for onward flight and return flight
             ***********************************************************************************************************/
            FareQuoteRoundTripOuterBean lObjFareQuoteRoundTripOuterBean = new FareQuoteRoundTripOuterBean();



            // Added to new for security token
            if (mJsonObject.has("securityToken")) {
                AirDataHolder.getListHolder().getList().get(0).setOnwFlightSecurityToken(mJsonObject.getString("securityToken"));
            }

            if (mJsonObject.has("flightSegmentJtoX")) {
                AirDataHolder.getListHolder().getList().get(0).setOnwFlightXMLSegment(mJsonObject.getString("flightSegmentJtoX"));
            }

            if (mJsonObject.has("flightSegmentJson")) {

                AirDataHolder.getListHolder().getList().get(0).setOnwFlightJsonSegment(mJsonObject.getString("flightSegmentJson"));



                /***********************************************************************************************************
                 * Parse date from flightSegmentJson
                 * Set onward fare quote data(FareQuoteOneWayBean) in FareQuoteRoundTripOuterBean
                 ***********************************************************************************************************/
                JSONObject fareQuoteFlightOnwardJson = mJsonObject.getJSONObject("flightSegmentJson");
                FareQuoteOneWayBean lFareQuoteOneWayBean = new FareQuoteOneWayBean();

                // Added by vivek for fare quote header FROm TO
                lFareQuoteOneWayBean.setOrigin(fareQuoteFlightOnwardJson.getString("origin"));
                lFareQuoteOneWayBean.setDestination(fareQuoteFlightOnwardJson.getString("destination"));
                lFareQuoteOneWayBean.setTotal(fareQuoteFlightOnwardJson.getInt("total"));
                lFareQuoteOneWayBean.setTtax(fareQuoteFlightOnwardJson.getInt("ttax"));
                // set the base fare
                lFareQuoteOneWayBean.setBp(fareQuoteFlightOnwardJson.getInt("bp"));
                // set the service tax
                lFareQuoteOneWayBean.setStax(fareQuoteFlightOnwardJson.getInt("stax"));
                lFareQuoteOneWayBean.setAbp(fareQuoteFlightOnwardJson.getInt("abp"));
                lFareQuoteOneWayBean.setFlightAirline(fareQuoteFlightOnwardJson.getString("pc"));
                // lFareQuoteReturnBean.setE_ticket(fareQuoteFlightOnwardJson.getString("e_ticket"));
                lFareQuoteOneWayBean.setFaretype(fareQuoteFlightOnwardJson.getString("faretype"));


                /**
                 * 28 Oct 2016
                 * IRCTC SERVICE CHARGE
                 */
                lFareQuoteOneWayBean.setIrctcBookingCharges(Double.parseDouble(mJsonObject.optString("irctcTxnFee")));
                lFareQuoteOneWayBean.setIrctcAgentServiceCgarges(Double.parseDouble(mJsonObject.optString("irctcTxnFeeSCharge")));



                // flight tag in Fare Quote Response
                JSONArray inFlightJSONArray = fareQuoteFlightOnwardJson.getJSONArray("flight");
                // ArrayList for inner flight key
                ArrayList<FareQuoteInnerFlightDetailsBeans> lInnerFlightAlObj = new ArrayList<>();

                for (int i = 0; i < inFlightJSONArray.length(); i++) {

                    JSONObject lFlightJSONObject = (JSONObject) inFlightJSONArray.get(i);

                    FareQuoteInnerFlightDetailsBeans lFareQuoteInnerFlightDetailsBeans = new FareQuoteInnerFlightDetailsBeans();

                    setAirLineIcon(lFlightJSONObject,lFareQuoteInnerFlightDetailsBeans);

                    lFareQuoteInnerFlightDetailsBeans.setOac(lFlightJSONObject.getString("oac"));
                    lFareQuoteInnerFlightDetailsBeans.setDepartureairport(lFlightJSONObject.getString("departureairport"));
                    lFareQuoteInnerFlightDetailsBeans.setDepart(lFlightJSONObject.getString("depart"));
                    lFareQuoteInnerFlightDetailsBeans.setArrivalairport(lFlightJSONObject.getString("arrivalairport"));
                    lFareQuoteInnerFlightDetailsBeans.setArrive(lFlightJSONObject.getString("arrive"));
                    lFareQuoteInnerFlightDetailsBeans.setTkt(lFlightJSONObject.getString("tkt"));
                    lFareQuoteInnerFlightDetailsBeans.setFno(lFlightJSONObject.getString("fno"));
                    lFareQuoteInnerFlightDetailsBeans.setDeparturetime(DateUtility.formatDate(lFlightJSONObject.getString("departuretime")));//DateUtility.formatDate(lFlightJSONObject.getString("departuretime"))
                    lFareQuoteInnerFlightDetailsBeans.setArrivaltime(DateUtility.formatDate(lFlightJSONObject.getString("arrivaltime")));
                    lFareQuoteInnerFlightDetailsBeans.setDuration(lFlightJSONObject.getString("duration"));
                    // lFareQuoteInnerFlightDetailsBeans.setInfantTicketType(lFlightJSONObject.getString("infantTicketType"));
                    lFareQuoteInnerFlightDetailsBeans.setVia(lFlightJSONObject.getString("via"));
                    lFareQuoteInnerFlightDetailsBeans.setStops(lFlightJSONObject.getString("stops"));
                    lFareQuoteInnerFlightDetailsBeans.setOnwardorreturn(lFlightJSONObject.getString("onwardorreturn"));
                    lInnerFlightAlObj.add(lFareQuoteInnerFlightDetailsBeans);
                }

                lFareQuoteOneWayBean.setFlight(lInnerFlightAlObj);
                // Set fare detail bean in outer bean
                lObjFareQuoteRoundTripOuterBean.setAlRoundTripOnwardFareQuoteBean(lFareQuoteOneWayBean);
                // Here set the FareQuoteRoundTripOuterBean in AirHolder
                AirDataHolder.getListHolder().getList().get(0).setmFareQuoteRoundTripOuterBean(lObjFareQuoteRoundTripOuterBean);










                /***********************************************************************************************************
                 * Parse date from flightSegmentJson
                 * Set onward fare quote data(FareQuoteOneWayBean) in FareQuoteRoundTripOuterBean
                 ***********************************************************************************************************/

                if (mJsonObject.has("flightSegmentRetJtoX")) {
                    AirDataHolder.getListHolder().getList().get(0).setReturnFlightXMLSegment(mJsonObject.getString("flightSegmentRetJtoX"));
                }
                if (mJsonObject.has("returnFlightSegmentJson")) {
                    AirDataHolder.getListHolder().getList().get(0).setReturnFlightJsonSegment(mJsonObject.getString("returnFlightSegmentJson"));
                }



                JSONObject fareQuoteReturnFlightJson = mJsonObject.getJSONObject("returnFlightSegmentJson");

                FareQuoteOneWayBean lFareQuoteReturnBean = new FareQuoteOneWayBean();

                // Added by vivek for fare quote header FROm TO
                lFareQuoteReturnBean.setOrigin(fareQuoteReturnFlightJson.getString("origin"));
                lFareQuoteReturnBean.setDestination(fareQuoteReturnFlightJson.getString("destination"));
                lFareQuoteReturnBean.setTotal(fareQuoteReturnFlightJson.getInt("total"));
                lFareQuoteReturnBean.setTtax(fareQuoteReturnFlightJson.getInt("ttax"));
                // set the base fare
                lFareQuoteReturnBean.setBp(fareQuoteReturnFlightJson.getInt("bp"));
                // set the service tax
                lFareQuoteReturnBean.setStax(fareQuoteReturnFlightJson.getInt("stax"));
                lFareQuoteReturnBean.setAbp(fareQuoteReturnFlightJson.getInt("abp"));
                lFareQuoteReturnBean.setFlightAirline(fareQuoteReturnFlightJson.getString("pc"));
                //lFareQuoteReturnBean.setE_ticket(fareQuoteFlightOnwardJson.getString("e_ticket"));
                lFareQuoteReturnBean.setFaretype(fareQuoteReturnFlightJson.getString("faretype"));

                /**
                 * 28 Oct 2016
                 * IRCTC SERVICE CHARGE
                 */
                lFareQuoteReturnBean.setIrctcBookingCharges(Double.parseDouble(mJsonObject.optString("irctcTxnFee")));
                lFareQuoteReturnBean.setIrctcAgentServiceCgarges(Double.parseDouble(mJsonObject.optString("irctcTxnFeeSCharge")));


                // flight tag in Fare Quote Response
                JSONArray inFlightJsonArray = fareQuoteReturnFlightJson.getJSONArray("flight");
                // ArrayList for inner flight key
                ArrayList<FareQuoteInnerFlightDetailsBeans> lInnerFlightObj = new ArrayList<>();

                for (int i = 0; i < inFlightJsonArray.length(); i++) {

                    JSONObject lFlightJSONObject = (JSONObject) inFlightJsonArray.get(i);
                    FareQuoteInnerFlightDetailsBeans lFareQuoteInnerFlightDetailsBeans = new FareQuoteInnerFlightDetailsBeans();
                    setAirLineIcon(lFlightJSONObject,lFareQuoteInnerFlightDetailsBeans);

                    lFareQuoteInnerFlightDetailsBeans.setOac(lFlightJSONObject.getString("oac"));
                    lFareQuoteInnerFlightDetailsBeans.setDepartureairport(lFlightJSONObject.getString("departureairport"));
                    lFareQuoteInnerFlightDetailsBeans.setDepart(lFlightJSONObject.getString("depart"));
                    lFareQuoteInnerFlightDetailsBeans.setArrivalairport(lFlightJSONObject.getString("arrivalairport"));
                    lFareQuoteInnerFlightDetailsBeans.setArrive(lFlightJSONObject.getString("arrive"));
                    lFareQuoteInnerFlightDetailsBeans.setTkt(lFlightJSONObject.getString("tkt"));
                    lFareQuoteInnerFlightDetailsBeans.setFno(lFlightJSONObject.getString("fno"));
                    lFareQuoteInnerFlightDetailsBeans.setDeparturetime(DateUtility.formatDate(lFlightJSONObject.getString("departuretime")));//DateUtility.formatDate(lFlightJSONObject.getString("departuretime"))
                    lFareQuoteInnerFlightDetailsBeans.setArrivaltime(DateUtility.formatDate(lFlightJSONObject.getString("arrivaltime")));
                    lFareQuoteInnerFlightDetailsBeans.setDuration(lFlightJSONObject.getString("duration"));
                    // lFareQuoteInnerFlightDetailsBeans.setInfantTicketType(lFlightJSONObject.getString("infantTicketType"));
                    lFareQuoteInnerFlightDetailsBeans.setVia(lFlightJSONObject.getString("via"));
                    lFareQuoteInnerFlightDetailsBeans.setStops(lFlightJSONObject.getString("stops"));
                    lFareQuoteInnerFlightDetailsBeans.setOnwardorreturn(lFlightJSONObject.getString("onwardorreturn"));
                    lInnerFlightObj.add(lFareQuoteInnerFlightDetailsBeans);
                }

                lFareQuoteReturnBean.setFlight(lInnerFlightObj);
                // Set fare detail bean in outer bean
                lObjFareQuoteRoundTripOuterBean.setAlRoundTripReturnFareQuoteBean(lFareQuoteReturnBean);
                // Here set the FareQuoteRoundTripOuterBean in AirHolder
                AirDataHolder.getListHolder().getList().get(0).setmFareQuoteRoundTripOuterBean(lObjFareQuoteRoundTripOuterBean);



            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setAirLineIcon(JSONObject lsingleflightJSONObject, FareQuoteInnerFlightDetailsBeans lFlightDetailBean) {

        lFlightDetailBean.setFlightIcon(AirlineLogoUtil.getAirlineLogo(lsingleflightJSONObject.optString("oac")));

//        try {
//            if(lsingleflightJSONObject.getString("oac").equals("0S")){
//                lFlightDetailBean.setFlightIcon(R.drawable.spicejet_logo);
//            }
//            else if(lsingleflightJSONObject.getString("oac").equals("G8")){
//                lFlightDetailBean.setFlightIcon(R.drawable.goair_logo);
//            }
//            else if(lsingleflightJSONObject.getString("oac").equals("6E")){
//                lFlightDetailBean.setFlightIcon(R.drawable.indigo_logo);
//            }
//            else if(lsingleflightJSONObject.getString("oac").equals("S2")){
//                lFlightDetailBean.setFlightIcon(R.drawable.ijetkonnect);
//            }
//            else if(lsingleflightJSONObject.getString("oac").equals("AI")){
//                lFlightDetailBean.setFlightIcon(R.drawable.airindia);
//            }
//            else if(lsingleflightJSONObject.getString("oac").equals("UK")){
//                lFlightDetailBean.setFlightIcon(R.drawable.vistara);//vistara Logo
//            }
//            else if(lsingleflightJSONObject.getString("oac").equals("9W")){
//                lFlightDetailBean.setFlightIcon(R.drawable.jetair_logo);
//            }
//            else if(lsingleflightJSONObject.getString("oac").equals("AK")){
//                lFlightDetailBean.setFlightIcon(R.drawable.airasia);
//            }
//            else {
//                lFlightDetailBean.setFlightIcon(R.drawable.onward_icon);
//            }
//
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

    }

}
