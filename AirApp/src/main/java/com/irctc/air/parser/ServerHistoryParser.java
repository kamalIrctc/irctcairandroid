package com.irctc.air.parser;

import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.model.HistoryBean;
import com.irctc.air.model.SegmentDetailBean;
import com.irctc.air.model.ServerBookedCanceledHistoryBean;
import com.irctc.air.util.AppLogger;
import com.irctc.air.util.DateUtility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by tourism on 6/22/2016.
 */
public class ServerHistoryParser {

    JSONObject mJsonObject;

    public ServerHistoryParser(String lresponseStr) {

        try {
            this.mJsonObject  = new JSONObject(lresponseStr);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    public void bookingHistoryServerResponseParser() {

        //  CLEAR THE BOOKING HISTORY & CANCEL HISTORY
        if(AirDataHolder.getListHolder().getList().get(0).getServerBookedCancelHistory() != null) {
            AirDataHolder.getListHolder().getList().get(0).setServerBookedCancelHistory(null);
        }

        try {

            AppLogger.enable();
            JSONArray bookedCancelJsonArray = mJsonObject.getJSONArray("bookingHistory");

            // CREATE INSTANCE OF MainBookedCancledHistoryBean TO SET BOOKED HISTORY
            ServerBookedCanceledHistoryBean serverBookedCanceledHistoryBean = new ServerBookedCanceledHistoryBean();



            // FOR TOP MOST LOOP
            for (int i = 0; i < bookedCancelJsonArray.length(); i++) {

                AppLogger.e("bookedCancelJsonArray ",""+i);

                if(i == 0){

                    JSONObject bookedJsonObj = (JSONObject) bookedCancelJsonArray.get(i);

                    if(bookedJsonObj.has("booked")){

                        JSONArray bookedJsonArray = bookedJsonObj.getJSONArray("booked");

                        // FOR TOP MOST LOOP BOOKED HISTORY
                        for (int bookedIndex = 0; bookedIndex < bookedJsonArray.length(); bookedIndex++) {

                            JSONObject lbookedJson = (JSONObject) bookedJsonArray.get(bookedIndex);
                            HistoryBean historyBean = new HistoryBean();

                            {
                                // set data in BookingHistoryBean
                                AppLogger.e("BookingHistoryBean ", "" + bookedIndex);

                                historyBean.setTransactionId(lbookedJson.optString("trnId"));
                                historyBean.setBookingStatusValue(lbookedJson.optString("bookingstatusVal"));
                                historyBean.setHistoryJson(lbookedJson.toString());
                                historyBean.setHistoryType("Booked");

                            }

                            if(lbookedJson.has("onward")){

                                JSONArray onwardJsonArray = lbookedJson.getJSONArray("onward");
                                // FOR TOP MOST ONWARD LOOP
                                for (int onwardIndex = 0; onwardIndex < onwardJsonArray.length() ; onwardIndex++) {

                                    AppLogger.e("ONWARD ",""+onwardIndex);
                                    JSONObject lonwardJson  = (JSONObject) onwardJsonArray.get(onwardIndex);

                                    /**********************************
                                     * GETTING ALL THE SEGEMNT DETAILS
                                     * ********************************/

                                    if(lonwardJson.has("segment")){

                                        JSONArray lSegmentJsonArray  = lonwardJson.getJSONArray("segment");
                                        // FOR TOP MOST SEGMENT LOOP
                                        for (int segmentIndex = 0; segmentIndex < lSegmentJsonArray.length(); segmentIndex++) {

                                            JSONObject lSegmentJson = (JSONObject) lSegmentJsonArray.get(segmentIndex);

                                            {
                                                // set data in SegmentDetailBean
                                                AppLogger.e("SegmentDetailBean ", "" + segmentIndex);

                                                historyBean.setDepartureDate(DateUtility.getDateFromBookingHistoryRes(lSegmentJson.optString("departTime")));

                                                if(segmentIndex == lSegmentJsonArray.length()-1){
                                                    historyBean.setArrivalDate(DateUtility.getDateFromBookingHistoryRes(lSegmentJson.optString("arriavlTime")));
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            // set data every time
                            serverBookedCanceledHistoryBean.setServerBookedCanceledHistory(historyBean);

                        }
                    }
                }
                else{
                    // FOR CANCELLATION HISTORY

                    JSONObject cancelledJsonObj = (JSONObject) bookedCancelJsonArray.get(i);

                    if(cancelledJsonObj.has("cancelled")){

                        JSONArray cancelledJsonArray = cancelledJsonObj.getJSONArray("cancelled");

                        // FOR TOP MOST LOOP BOOKED HISTORY
                        for (int cancelIndex = 0; cancelIndex < cancelledJsonArray.length(); cancelIndex++) {

                            JSONObject lcancelledJson = (JSONObject) cancelledJsonArray.get(cancelIndex);

                            // CREATE BookingHistoryBean OBJECT
                            HistoryBean historyBean = new HistoryBean();
                            {
                                // set data in BookingHistoryBean
                                AppLogger.e("BookingHistoryBean ",""+cancelIndex);

                                historyBean.setTransactionId(lcancelledJson.optString("trnId"));
                                historyBean.setBookingStatusValue(lcancelledJson.optString("bookingstatusVal"));
                                historyBean.setHistoryJson(lcancelledJson.toString());
                                historyBean.setHistoryType("Canceled");
                            }

                            if(lcancelledJson.has("onward")){

                                JSONArray onwardJsonArray = lcancelledJson.getJSONArray("onward");
                                // FOR TOP MOST ONWARD LOOP
                                for (int onwardIndex = 0; onwardIndex < onwardJsonArray.length() ; onwardIndex++) {

                                    AppLogger.e("ONWARD ",""+onwardIndex);
                                    JSONObject lonwardJson  = (JSONObject) onwardJsonArray.get(onwardIndex);

                                    /**********************************
                                     * GETTING ALL THE SEGEMNT DETAILS
                                     * ********************************/

                                    if(lonwardJson.has("segment")){

                                        JSONArray lSegmentJsonArray  = lonwardJson.getJSONArray("segment");
                                        // FOR TOP MOST SEGMENT LOOP
                                        for (int segmentIndex = 0; segmentIndex < lSegmentJsonArray.length(); segmentIndex++) {

                                            JSONObject lSegmentJson = (JSONObject) lSegmentJsonArray.get(segmentIndex);
                                            // CREATE INSTANCE OF SegmentDetailBean
                                            SegmentDetailBean lSegmentDetailBean = new SegmentDetailBean();
                                            {
                                                // set data in SegmentDetailBean
                                                AppLogger.e("SegmentDetailBean ", "" + segmentIndex);

                                                historyBean.setDepartureDate(DateUtility.getDateFromBookingHistoryRes(lSegmentJson.optString("departTime")));

                                                if(segmentIndex == lSegmentJsonArray.length()-1){
                                                    historyBean.setArrivalDate(DateUtility.getDateFromBookingHistoryRes(lSegmentJson.optString("arriavlTime")));
                                                }

                                            }
                                        }
                                    }
                                }
                            }

                            // set data every time
                            serverBookedCanceledHistoryBean.setServerBookedCanceledHistory(historyBean);
                        }
                    }
                }
                // SET MainBookedCancledHistoryBean in MAIN HOLDER (AirDataHolder)
                AirDataHolder.getListHolder().getList().get(0).setServerBookedCancelHistory(serverBookedCanceledHistoryBean);


                System.out.println("OBJECT HISTORY "+serverBookedCanceledHistoryBean.getServerBookedCanceledHistory().size());
            }
        } catch (Exception e) {
            e.printStackTrace();
            AppLogger.e("Exception Occoured ", "" + e);
        }
    }

}
