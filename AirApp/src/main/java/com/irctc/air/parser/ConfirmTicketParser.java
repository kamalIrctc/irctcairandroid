package com.irctc.air.parser;

import android.content.Context;

import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.model.ConfirmationTicketBean;
import com.irctc.air.model.FareDetailBean;
import com.irctc.air.model.FlightConfirmationBean;
import com.irctc.air.model.PassengerDetailBean;
import com.irctc.air.model.SegmentDetailBean;
import com.irctc.air.util.AirlineLogoUtil;
import com.irctc.air.util.DateUtility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ConfirmTicketParser {

    JSONObject mJsonObject;
    ActivityMain mainActivity;

    int minAI = 0;

    public ConfirmTicketParser(String lresponseStr, Context ctx) {

        try {
            this.mJsonObject  = new JSONObject(lresponseStr);
            this.mainActivity = (ActivityMain)ctx;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    public void ConfirmTicketParserResponseParser() {

        try {

            // CLEAR CONFIRMATION OLD DATA
            AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().clear();

            //// CONFIRM TICKET PARSER  ////
            if (mJsonObject.has("msg")) {

                if(mJsonObject.getString("msg").equalsIgnoreCase("success")){


                    /******************************************************************************
                     * GET TICKET DETAIL PARSING
                     *******************************************************************************/
                    JSONObject ticketDetail =  (JSONObject) mJsonObject.get("ticketDetail");

                    ConfirmationTicketBean lObjConfirmationTicketBean = new ConfirmationTicketBean();
                    lObjConfirmationTicketBean.setBrf(ticketDetail.optString("brf"));
                    lObjConfirmationTicketBean.setOrigin(ticketDetail.optString("origin"));
                    lObjConfirmationTicketBean.setDestination(ticketDetail.optString("destination"));

                    lObjConfirmationTicketBean.setOriginCode(ticketDetail.optString("orig"));
                    lObjConfirmationTicketBean.setDestinationCode(ticketDetail.optString("dest"));

                    lObjConfirmationTicketBean.setTripType(ticketDetail.optString("tripType"));
                    lObjConfirmationTicketBean.setNoofpass(ticketDetail.optString("noofpass"));
                    lObjConfirmationTicketBean.setNoofseg(ticketDetail.optString("noofseg"));
                    lObjConfirmationTicketBean.setTrnId(ticketDetail.optString("trnId"));
                    lObjConfirmationTicketBean.setBookingstatus(ticketDetail.optString("bookingstatus"));
                    lObjConfirmationTicketBean.setBookingstatusValue(ticketDetail.optString("bookingstatusVal"));
                    lObjConfirmationTicketBean.setBookingdate(DateUtility.getDateFromBookingHistoryRes(ticketDetail.optString("bookingdate")));
                    lObjConfirmationTicketBean.setBookeradd1(ticketDetail.optString("bookeradd1"));
                    lObjConfirmationTicketBean.setBookeradd2(ticketDetail.optString("bookeradd2"));
                    lObjConfirmationTicketBean.setBookercity(ticketDetail.optString("bookercity"));
                    lObjConfirmationTicketBean.setBookerstate(ticketDetail.optString("bookerstate"));
                    lObjConfirmationTicketBean.setBookercountry(ticketDetail.optString("bookercountry"));
                    lObjConfirmationTicketBean.setBookerpin(ticketDetail.optString("bookerpin"));
                    lObjConfirmationTicketBean.setBookerphone(ticketDetail.optString("bookerphone"));
                    lObjConfirmationTicketBean.setTktoid(ticketDetail.optString("tktoid"));
                    lObjConfirmationTicketBean.setSegtypecan(ticketDetail.optString("segtypecan"));
                    lObjConfirmationTicketBean.setTranoid(ticketDetail.optString("tranoid"));
                    lObjConfirmationTicketBean.setTotalCharge(ticketDetail.optString("totalCharge"));
                    lObjConfirmationTicketBean.setOrigCharge(ticketDetail.optString("origCharge"));
                    lObjConfirmationTicketBean.setLtcEmpCode(ticketDetail.optString("ltcEmpCode"));
                    lObjConfirmationTicketBean.setNoOfAdult(ticketDetail.optString("noOfAdult"));
                    lObjConfirmationTicketBean.setNoOfChild(ticketDetail.optString("noOfChild"));
                    lObjConfirmationTicketBean.setNoOfInfant(ticketDetail.optString("noOfInfant"));



                    /******************************************************************************
                     * FARE DETAIL
                     *******************************************************************************/
                    JSONArray fareDetail = ticketDetail.getJSONArray("fare");
                    for (int i=0; i < fareDetail.length(); i++) {

                        FareDetailBean lObjFareDetailBean = new FareDetailBean();
                        JSONObject lObjFareDetail = (JSONObject) fareDetail.get(i);

                        lObjFareDetailBean.setJn(lObjFareDetail.optString("jn"));
                        lObjFareDetailBean.setBfare(lObjFareDetail.optString("bfare"));
                        lObjFareDetailBean.setYr(lObjFareDetail.optString("yr"));
                        lObjFareDetailBean.setYq(lObjFareDetail.optString("yq"));
                        lObjFareDetailBean.setTax1(lObjFareDetail.optString("tax1"));
                        lObjFareDetailBean.setSubTotal(lObjFareDetail.optString("subTotal"));
                        lObjFareDetailBean.setExtra(lObjFareDetail.optString("extra"));
                        lObjFareDetailBean.setInc(lObjFareDetail.optString("inc"));
                        lObjFareDetailBean.setWo(lObjFareDetail.optString("wo"));
                        lObjFareDetailBean.setTax2(lObjFareDetail.optString("tax2"));
                        lObjFareDetailBean.setDisc(lObjFareDetail.optString("disc"));

                        /**
                         * 07 NOV 2016
                         * IRCTC SERVICE CHARGE
                         */
                        lObjFareDetailBean.setIrctcTxnFees(Double.parseDouble(lObjFareDetail.optString("irctcTxnFees")));

                        lObjConfirmationTicketBean.setAlFareDetail(lObjFareDetailBean);
                    }



                    /******************************************************************************
                     * ONWARD DETAIL ARRAY PARSING
                     *******************************************************************************/
                if(ticketDetail.has("onward")) {
                    JSONArray onWardDetailDetail = ticketDetail.getJSONArray("onward");

                    // This contains multiple Segment array and one passenger array for all segment
                    JSONObject lObjDetailDetail = (JSONObject) onWardDetailDetail.get(0);

                    FlightConfirmationBean lObjFlightConfirmationBean = new FlightConfirmationBean();
                    // GET ALL SEGMENT DETAIL
                    JSONArray onWardSegmentDetailDetail = lObjDetailDetail.getJSONArray("segment");
                    for (int m = 0; m < onWardSegmentDetailDetail.length(); m++) {
                        JSONObject lObjSegDetailDetail = (JSONObject) onWardSegmentDetailDetail.get(m);
                        SegmentDetailBean lObjSegmentDetailBean = new SegmentDetailBean();

                        /**
                         * 21 Oct 2016
                         * PNR - PASS STATUS
                         */
                        //lObjSegmentDetailBean.setAirpnr(lObjSegDetailDetail.optString("disc"));
                        lObjSegmentDetailBean.setAirpnr(lObjSegDetailDetail.optString("airpnr"));

                        lObjSegmentDetailBean.setAircontact(lObjSegDetailDetail.optString("aircontact"));
                        lObjSegmentDetailBean.setAirline(lObjSegDetailDetail.optString("airline"));
                        lObjSegmentDetailBean.setSegdest(lObjSegDetailDetail.optString("segdest"));
                        lObjSegmentDetailBean.setArriavlTime(DateUtility.getDateFromBookingHistoryRes(lObjSegDetailDetail.optString("arriavlTime")));
                        lObjSegmentDetailBean.setPc(lObjSegDetailDetail.optString("pc"));
                        lObjSegmentDetailBean.setVia(lObjSegDetailDetail.optString("via"));
                        lObjSegmentDetailBean.setSppnr(lObjSegDetailDetail.optString("sppnr"));
                        lObjSegmentDetailBean.setCabinclass(lObjSegDetailDetail.optString("cabinclass"));
                        lObjSegmentDetailBean.setOac(lObjSegDetailDetail.optString("oac"));
                        lObjSegmentDetailBean.setSegorig(lObjSegDetailDetail.optString("segorig"));
                        lObjSegmentDetailBean.setFaretype(lObjSegDetailDetail.optString("faretype"));
                        lObjSegmentDetailBean.setDepartTime(DateUtility.getDateFromBookingHistoryRes(lObjSegDetailDetail.optString("departTime")));
                        lObjSegmentDetailBean.setFno(lObjSegDetailDetail.optString("fno"));
                        lObjSegmentDetailBean.setDuration(lObjSegDetailDetail.optString("duration"));

                        lObjSegmentDetailBean.setSegdestC(lObjSegDetailDetail.optString("segdestC"));
                        lObjSegmentDetailBean.setSegorigC(lObjSegDetailDetail.optString("segorigC"));

                        setFlightIcon(lObjSegDetailDetail, lObjSegmentDetailBean);

                        lObjFlightConfirmationBean.setAlSegmentDetail(lObjSegmentDetailBean);

                    }

                    // GET SINGLE PASSENGERs DETAIL
                    JSONArray onWardPassDetailDetail = lObjDetailDetail.getJSONArray("passanger");

                    for (int m=0; m < onWardPassDetailDetail.length(); m++) {
                        JSONObject lObjPassDetailDetail = (JSONObject) onWardPassDetailDetail.get(m);
                        PassengerDetailBean lObjPassengerDetailBean = new PassengerDetailBean();

                        lObjPassengerDetailBean.setLname(lObjPassDetailDetail.optString("lname"));
                        lObjPassengerDetailBean.setCandetail(lObjPassDetailDetail.optString("candetail"));
                        lObjPassengerDetailBean.setAge(lObjPassDetailDetail.optString("age"));
                        lObjPassengerDetailBean.setFname(lObjPassDetailDetail.optString("fname"));
                        lObjPassengerDetailBean.setCanstatus(lObjPassDetailDetail.optString("canstatus"));
                        lObjPassengerDetailBean.setTicketNo(lObjPassDetailDetail.optString("ticketNo"));
                        lObjPassengerDetailBean.setPasgtype(lObjPassDetailDetail.optString("pasgtype"));

                        /**
                         * 07 nov 2016
                         * PNR - PASS STATUS
                         */
                        lObjPassengerDetailBean.setCanstatusText(lObjPassDetailDetail.optString("canstatusV"));


                        lObjFlightConfirmationBean.setAlPassengerDetail(lObjPassengerDetailBean);
                    }



                    // Set ONWARD segment and passenger FlightConfirmationBean to ConfirmationTicketBean
                    lObjConfirmationTicketBean.setAlOnwardFlightConfirmationDetail(lObjFlightConfirmationBean);

                }



                    /******************************************************************************
                     * RETURN DETAIL ARRAY PARSING
                     *******************************************************************************/
                    if(ticketDetail.has("returnJrn")) {
                    JSONArray returnDetailDetail = ticketDetail.getJSONArray("returnJrn");
                        JSONObject lObjDetailDetail = (JSONObject) returnDetailDetail.get(0);

                        // This contains Segment array and passenger array
                        FlightConfirmationBean lObjFlightConfirmationBean = new FlightConfirmationBean();
                        // GET ALL SEGMENT DETAIL
                        JSONArray returnSegmentDetailDetail = lObjDetailDetail.getJSONArray("segment");
                        for (int m=0; m < returnSegmentDetailDetail.length(); m++) {
                            JSONObject lObjSegDetailDetail = (JSONObject) returnSegmentDetailDetail.get(m);
                            SegmentDetailBean lObjSegmentDetailBean = new SegmentDetailBean();

                            /**
                             * 21 Oct 2016
                             * PNR - PASS STATUS
                             */
                            //lObjSegmentDetailBean.setAirpnr(lObjSegDetailDetail.optString("disc"));
                            lObjSegmentDetailBean.setAirpnr(lObjSegDetailDetail.optString("airpnr"));

                            lObjSegmentDetailBean.setAircontact(lObjSegDetailDetail.optString("aircontact"));
                            lObjSegmentDetailBean.setAirline(lObjSegDetailDetail.optString("airline"));
                            lObjSegmentDetailBean.setSegdest(lObjSegDetailDetail.optString("segdest"));
                            lObjSegmentDetailBean.setArriavlTime(DateUtility.getDateFromBookingHistoryRes(lObjSegDetailDetail.optString("arriavlTime")));
                            lObjSegmentDetailBean.setPc(lObjSegDetailDetail.optString("pc"));
                            lObjSegmentDetailBean.setVia(lObjSegDetailDetail.optString("via"));
                            lObjSegmentDetailBean.setSppnr(lObjSegDetailDetail.optString("sppnr"));
                            lObjSegmentDetailBean.setCabinclass(lObjSegDetailDetail.optString("cabinclass"));
                            lObjSegmentDetailBean.setOac(lObjSegDetailDetail.optString("oac"));
                            lObjSegmentDetailBean.setSegorig(lObjSegDetailDetail.optString("segorig"));
                            lObjSegmentDetailBean.setFaretype(lObjSegDetailDetail.optString("faretype"));
                            lObjSegmentDetailBean.setDepartTime(DateUtility.getDateFromBookingHistoryRes(lObjSegDetailDetail.optString("departTime")));
                            lObjSegmentDetailBean.setFno(lObjSegDetailDetail.optString("fno"));
                            lObjSegmentDetailBean.setDuration(lObjSegDetailDetail.optString("duration"));

                            lObjSegmentDetailBean.setSegdestC(lObjSegDetailDetail.optString("segdestC"));
                            lObjSegmentDetailBean.setSegorigC(lObjSegDetailDetail.optString("segorigC"));

                            setFlightIcon(lObjSegDetailDetail, lObjSegmentDetailBean);

                            lObjFlightConfirmationBean.setAlSegmentDetail(lObjSegmentDetailBean);

                        }


                        // GET SINGLE PASSENGERS DETAIL
                        JSONArray returnPassDetailDetail = lObjDetailDetail.getJSONArray("passanger");
                        for (int m=0; m < returnPassDetailDetail.length(); m++) {
                            JSONObject lObjPassDetailDetail = (JSONObject) returnPassDetailDetail.get(m);
                            PassengerDetailBean lObjPassengerDetailBean = new PassengerDetailBean();

                            lObjPassengerDetailBean.setLname(lObjPassDetailDetail.optString("lname"));
                            lObjPassengerDetailBean.setCandetail(lObjPassDetailDetail.optString("candetail"));
                            lObjPassengerDetailBean.setAge(lObjPassDetailDetail.optString("age"));
                            lObjPassengerDetailBean.setFname(lObjPassDetailDetail.optString("fname"));
                            lObjPassengerDetailBean.setCanstatus(lObjPassDetailDetail.optString("canstatus"));
                            lObjPassengerDetailBean.setTicketNo(lObjPassDetailDetail.optString("ticketNo"));
                            lObjPassengerDetailBean.setPasgtype(lObjPassDetailDetail.optString("pasgtype"));

                            /**
                             * 07 nov 2016
                             * PNR - PASS STATUS
                             */
                            lObjPassengerDetailBean.setCanstatusText(lObjPassDetailDetail.optString("canstatusV"));

                            lObjFlightConfirmationBean.setAlPassengerDetail(lObjPassengerDetailBean);
                        }



                        // Set RETURN segment and passenger FlightConfirmationBean to ConfirmationTicketBean
                        lObjConfirmationTicketBean.setAlReturnFlightConfirmationDetail(lObjFlightConfirmationBean);

                    }

                    // SET CONFIRMATION TICKET DATA IN HOLDER
                    AirDataHolder.getListHolder().getList().get(0).setConfirmationFlightDetail(lObjConfirmationTicketBean);


                }else{
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setFlightIcon(JSONObject lsingleflightJSONObject, SegmentDetailBean lSegmentDetailBean) {


        lSegmentDetailBean.setFlighticon(AirlineLogoUtil.getAirlineLogo(lsingleflightJSONObject.optString("oac")));


//        try {
//            if(lsingleflightJSONObject.getString("oac").equals("0S")){
//                lSegmentDetailBean.setFlighticon(R.drawable.spicejet_logo);
//            }
//            else if(lsingleflightJSONObject.getString("oac").equals("G8")){
//                lSegmentDetailBean.setFlighticon(R.drawable.goair_logo);
//            }
//            else if(lsingleflightJSONObject.getString("oac").equals("6E")){
//                lSegmentDetailBean.setFlighticon(R.drawable.indigo_logo);
//            }
//            else if(lsingleflightJSONObject.getString("oac").equals("AI")){
//                lSegmentDetailBean.setFlighticon(R.drawable.airindia);
//            }
//            else if(lsingleflightJSONObject.getString("oac").equals("UK")){
//                lSegmentDetailBean.setFlighticon(R.drawable.vistara);//vistara Logo
//            }
//            else if(lsingleflightJSONObject.getString("oac").equals("9W")){
//                lSegmentDetailBean.setFlighticon(R.drawable.jetair_logo);
//            }
//            else if(lsingleflightJSONObject.optString("oac").equals("AK")){
//                lSegmentDetailBean.setFlighticon(R.drawable.airasia);
//            }
//            else if(lsingleflightJSONObject.optString("oac").equals("S2")){
//                lSegmentDetailBean.setFlighticon(R.drawable.ijetkonnect);
//            }
//            else{
//                lSegmentDetailBean.setFlighticon(R.drawable.onward_icon);
//            }
//
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

    }


}


