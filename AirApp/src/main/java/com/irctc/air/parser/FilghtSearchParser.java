package com.irctc.air.parser;

import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.model.FilterCheapFareAirlineBean;
import com.irctc.air.model.FlightFilterBean;
import com.irctc.air.model.FlightOnWardDetailBean;
import com.irctc.air.model.InnerFlightDetailsBeans;
import com.irctc.air.model.RouOnwGdcLccBean;
import com.irctc.air.model.SpecialOfferAirlineBean;
import com.irctc.air.util.AirlineLogoUtil;
import com.irctc.air.util.AppLogger;
import com.irctc.air.util.DateUtility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Asif on 4/22/2016.
 *
 * It is used to parse the Json response
 */
public class FilghtSearchParser {

    private JSONObject mJsonObject;

    private int minAI = 0;
    private int minPrice;
    private int maxPrice;
    private ActivityMain mainActivity;

    private int mHour;
    private int mMinutes;

    /**
     * Oct 18 2016
     * FLIGHT ON/OFF
     */
    String offFlightCode[];
    private static List<String> offFlightCodeArray = null;


    public FilghtSearchParser(ActivityMain mainActivity, String lresponseStr) {

        try {
             this.mainActivity =  mainActivity;
            this.mJsonObject  = new JSONObject(lresponseStr);
        } catch (JSONException e) {
            e.printStackTrace();
            AppLogger.enable();
            AppLogger.e("EXCEPTION in FilghtSearchParser", "");
        }
    }


    public static String  stringFormat1(String mStr){

        StringBuilder builder = new StringBuilder();

        if(mStr.contains("m")) {
            String arr[] = mStr.split(" ");

            if(arr[0].indexOf('h')== 1){

                builder.append("0" + arr[0].substring(0,1)+":");

            }else{

                builder.append(arr[0].substring(0,2)+":");

            }

            if(arr[1].indexOf('m')== 1 ){
                builder.append("0"+arr[1].substring(0,1));
            }else{
                builder.append(arr[1].substring(0,2));
            }
        }
        else{

            if(mStr.indexOf('h')== 1) {

                builder.append("0" + mStr.substring(0,1)+":");

            }
            builder.append("00");
        }
        //System.out.println("FORMATED : "+builder.toString());
        return builder.toString();
    }

    public void flightSearchResponseParser() {

        // Clear onward data
        AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().clear();
        FlightFilterBean lObjFlightFilBean =  AirDataHolder.getListHolder().getList().get(0).getmFlightFilterBean();
        lObjFlightFilBean = null;
        AirDataHolder.getListHolder().getList().get(0).setIsFlightOnWardFromCache(false);
        AirDataHolder.getListHolder().getList().get(0).setOnwFlightJsonSegment(null);
        AirDataHolder.getListHolder().getList().get(0).setOnwFlightXMLSegment(null);
        AirDataHolder.getListHolder().getList().get(0).setOnwFlightSecurityToken(null);
        AirDataHolder.getListHolder().getList().get(0).setOnwFlightFareMax(0);
        AirDataHolder.getListHolder().getList().get(0).setOnwFlightFareMin(0);

        // Clear return data
        AirDataHolder.getListHolder().getList().get(0).getReturnAlAllFlightDetails().clear();
        AirDataHolder.getListHolder().getList().get(0).getRouOnwalAllFlightDetails().clear();
        AirDataHolder.getListHolder().getList().get(0).getRouRetalAllFlightDetails().clear();
        AirDataHolder.getListHolder().getList().get(0).getSpecialOfferFare().clear();
        AirDataHolder.getListHolder().getList().get(0).getCheapestFlightFare().clear();


        // CONFIRM TICKET HOLDER CLEAR
        AirDataHolder.getListHolder().getList().get(0).getConfirmationFlightDetail().clear();

        // INTERNATIONAL ROUND TRIP DATA CLEAR
        AirDataHolder.getListHolder().getList().get(0).getOnwInternationalFlightDetails().clear();
        // TO RESET THE FLAG WHEN USER SEARCH AGAIN
        mainActivity.isGdsOrInternatioal = false;


        /****************************************************************************
        * START FLIGHT DETAIL PARSING
        *****************************************************************************/

        try {


            /**
             * Oct 18 2016
             * FLIGHT ON/OFF
             *
             * - Get offAirlineStr initially
             * - Split coma seperated string
             * - Create list for later use
             * - While parsing flight details, if pc code of flight detail is in offAirlineStr, dont save it in Holder.
             *
             **/

            offFlightCodeArray = new ArrayList<>();

            if(mJsonObject.has("offAirlineStr")) {
                if(mJsonObject.getString("offAirlineStr").toString().contains(",")) {
                    offFlightCode = mJsonObject.getString("offAirlineStr").split(",");
                }/*else{
                    offFlightCode[0] = mJsonObject.getString("offAirlineStr");
                }*/
            }



            if(offFlightCode!= null) {
                for (int i = 0; i < offFlightCode.length; i++) {
                    offFlightCodeArray.add(offFlightCode[i]);
                }
            }






            /**********************************
            * PARSE CHEAPEST AND SPECIAL OFFER
            ************************************/
            if (mJsonObject.has("fareCheapest")) {

                if(mainActivity.isOneWaySelected) {

                    JSONArray cheapestOnewayFareJSONArray = mJsonObject.getJSONArray("fareCheapest");
                    AppLogger.enable();
                    AppLogger.e("Total  flight  in fareOnewayCheapest ", "" + cheapestOnewayFareJSONArray.length());

                    for (int j = 0; j < cheapestOnewayFareJSONArray.length(); j++) {

                        JSONObject cheapflightJSONObj = (JSONObject) cheapestOnewayFareJSONArray.get(j);

                        FilterCheapFareAirlineBean airline = new FilterCheapFareAirlineBean();

                        airline.setmFlightName(cheapflightJSONObj.getString("airline"));
                        airline.setmFlightCode(cheapflightJSONObj.getString("airlineCode"));
                        airline.setmFlightLowestFare(cheapflightJSONObj.getString("fare"));
                        setLowestAirLineIcon(cheapflightJSONObj.getString("airlineCode"), airline);
                        //chepestFlightPrice.add(airline);

                        /**
                         * Oct 18 2016
                         * FLIGHT ON/OFF
                         * Cheapest flight in search
                         **/
                        if(!offFlightCodeArray.contains(cheapflightJSONObj.getString("airlineCode"))) {
                            AirDataHolder.getListHolder().getList().get(0).setCheapestFlightFare(airline);
                        }
                    }
                }
                else{

                    /// PARSING FOR THE SPECIAL AIRLINE OFFER

                    JSONArray specialFareJSONArray = mJsonObject.getJSONArray("fareCheapest");
                    AppLogger.enable();
                    AppLogger.e("Total  flight  in specialFareJSONArray ", "" + specialFareJSONArray.length());

                    for (int j = 0; j < specialFareJSONArray.length(); j++) {

                        JSONObject cheapflightJSONObj = (JSONObject) specialFareJSONArray.get(j);

                        SpecialOfferAirlineBean airline = new SpecialOfferAirlineBean();

                        airline.setFlightName(cheapflightJSONObj.getString("airline"));
                        airline.setFlightCode(cheapflightJSONObj.getString("airlineCode"));
                        airline.setFlightSpecialFare(cheapflightJSONObj.getString("fare"));
                        AirDataHolder.getListHolder().getList().get(0).setSpecialOfferFare(airline);
                    }
                }
            }
            if(mJsonObject.has("fromCache")){
                AirDataHolder.getListHolder().getList().get(0).setIsFlightOnWardFromCache(mJsonObject.getBoolean("fromCache"));

            }else{
                AirDataHolder.getListHolder().getList().get(0).setIsFlightOnWardFromCache(false);
            }



            /************************************
             * ONWARD FLIGHT PARSING
             ************************************/
            if (mJsonObject.has("Onw")) {


                 /***************************
                 * INTERNATIONAL ROUND TRIP
                 ****************************/

                JSONArray lOnwFlightJSONArray = mJsonObject.getJSONArray("Onw");
                AppLogger.enable();
                AppLogger.e("Total  flight  in Onw ", "" + lOnwFlightJSONArray.length());

                if(AirDataHolder.getListHolder().getList().get(0).getTripDomOrInter().equals("International") && AirDataHolder.getListHolder().getList().get(0).getTripType().equals("Round trip")){

                    JSONArray lRouOnwFlightJSONArray = mJsonObject.getJSONArray("Onw");
                    AppLogger.enable();
                    AppLogger.e("Total  flight  in Onw ", "" + lRouOnwFlightJSONArray.length());

                    for (int j=0; j < lRouOnwFlightJSONArray.length(); j++) {

                        FlightOnWardDetailBean lFlightOnwDetailBean = new FlightOnWardDetailBean();
                        JSONObject lsingleflightOnwJSONObject =  (JSONObject) lRouOnwFlightJSONArray.get(j);
                        lFlightOnwDetailBean.setFlightFare(lsingleflightOnwJSONObject.getInt("total"));
                        lFlightOnwDetailBean.setFlightAirline(lsingleflightOnwJSONObject.getString("pc"));

                        // Added by asif for round flight info screen
                        lFlightOnwDetailBean.setFlightStaxInFare(lsingleflightOnwJSONObject.getInt("stax"));

                        // Added by asif for Galilio nad International Flight
                        lFlightOnwDetailBean.setFlightFare(lsingleflightOnwJSONObject.getInt("total") - lsingleflightOnwJSONObject.getInt("stax"));


                        lFlightOnwDetailBean.setFlightTaxInFare(lsingleflightOnwJSONObject.getInt("ttax"));
                        lFlightOnwDetailBean.setFlightBaseFare(lsingleflightOnwJSONObject.getInt("bp"));
                        lFlightOnwDetailBean.setFlightJourneyStartFrom(lsingleflightOnwJSONObject.optString("origin"));
                        lFlightOnwDetailBean.setFlightJourneyEndTo(lsingleflightOnwJSONObject.optString("destination"));
                        lFlightOnwDetailBean.setFlightNumber(lsingleflightOnwJSONObject.optString("id"));



                        // here set the flight json obj as string
                        lFlightOnwDetailBean.setFlightJson(lsingleflightOnwJSONObject.toString());
                        //  lFlightOnwDetailBean.setFlightUniqueId(j);

                        setAirLineIcon(lsingleflightOnwJSONObject, lFlightOnwDetailBean);

                        // flight tag in main lsingleflightJSONObject
                        JSONArray inFlightJSONArray = lsingleflightOnwJSONObject.getJSONArray("flight");
                        //System.out.println("Total  flightJSONArray :  " + inFlightJSONArray.length());
                        // ArrayList for inner flight key
                        ArrayList<InnerFlightDetailsBeans> lInnerFlightAlObj = new ArrayList<>();

                        for (int i = 0; i < inFlightJSONArray.length(); i++) {

                            JSONObject lFlightOnwJSONObject = (JSONObject) inFlightJSONArray.get(i);
                            InnerFlightDetailsBeans lInnerFlightOnwDetailsBeans = new InnerFlightDetailsBeans();
                            lInnerFlightOnwDetailsBeans.setFlightOnwRetType(lFlightOnwJSONObject.getString("onwardorreturn"));


                            lInnerFlightOnwDetailsBeans.setFlightDepartureTime(DateUtility.formatDate(lFlightOnwJSONObject.getString("departuretime")));//(lFlightJSONObject.getString("departuretime").substring(11, 16));
                            lInnerFlightOnwDetailsBeans.setFlightArrivalTime(DateUtility.formatDate(lFlightOnwJSONObject.getString("arrivaltime")));//(lFlightJSONObject.getString("arrivaltime").substring(11, 16));
                            lInnerFlightOnwDetailsBeans.setFlightStops((inFlightJSONArray.length())-1);
                            lInnerFlightOnwDetailsBeans.setFlightNo(lFlightOnwJSONObject.getString("fno"));
                            // lInnerFlightDetailsBeans.setFlightDurationTime(StringFormat.stringFormat(lFlightJSONObject.getString("duration")));
                            lInnerFlightOnwDetailsBeans.setFlightDurationTime(lFlightOnwJSONObject.getString("duration"));
                            // Added by vivek for flight detail screen
                            // lInnerFlightOnwDetailsBeans.setFlightIcon(lFlightOnwJSONObject.optString("fno")); SET TRU METHOD
                            lInnerFlightOnwDetailsBeans.setOac(lFlightOnwJSONObject.optString("oac"));
                            lInnerFlightOnwDetailsBeans.setDepartureairport(lFlightOnwJSONObject.optString("departureairport"));
                            lInnerFlightOnwDetailsBeans.setDepart(lFlightOnwJSONObject.optString("depart"));
                            lInnerFlightOnwDetailsBeans.setArrivalairport(lFlightOnwJSONObject.optString("arrivalairport"));
                            lInnerFlightOnwDetailsBeans.setArrive(lFlightOnwJSONObject.optString("arrive"));
                            lInnerFlightOnwDetailsBeans.setTkt(lFlightOnwJSONObject.optString("tkt"));
                            lInnerFlightOnwDetailsBeans.setInfantTicketType(lFlightOnwJSONObject.optString("infantTicketType"));
                            lInnerFlightOnwDetailsBeans.setVia(lFlightOnwJSONObject.optString("via"));
                            lInnerFlightOnwDetailsBeans.setOnwardorreturn(lFlightOnwJSONObject.optString("onwardorreturn"));

                            // SET FLIGHT ICON IN INNER BEAN
                            setFlightSearchInnerAirLineIcon(lFlightOnwJSONObject, lInnerFlightOnwDetailsBeans);

                            lInnerFlightAlObj.add(lInnerFlightOnwDetailsBeans);
                        }

                            lFlightOnwDetailBean.setFlight(lInnerFlightAlObj);

                            // get the GDS flights
                            getDurationAndStops(lFlightOnwDetailBean, lInnerFlightAlObj, lInnerFlightAlObj.size());

                        /**
                         * Oct 24 2016
                         * UNIQUE ID changed setFlightUniqueId
                         */
                        if(j == 0) {
                            lFlightOnwDetailBean.setFlightUniqueId(j);
                        }else{
                            int lastUniqueId = AirDataHolder.getListHolder().getList().get(0).getOnwInternationalFlightDetails().size();
                            lFlightOnwDetailBean.setFlightUniqueId(lastUniqueId++);
                        }

                            /**
                             * Oct 18 2016
                             * FLIGHT ON/OFF
                             *
                             * - Get offAirlineStr initially
                             * - While parsing flight details, if pc code of flight detail is in offAirlineStr, dont save it in Holder.
                             *
                             */
                            if(!offFlightCodeArray.contains(lsingleflightOnwJSONObject.getString("pc"))){
                                AirDataHolder.getListHolder().getList().get(0).setOnwInternationalFlightDetails(lFlightOnwDetailBean);
                            }


                    }
                    AppLogger.enable();
                    AppLogger.e("Total  flight  after off ", "" + AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().size());


                }
                else
                {

                for (int j=0; j < lOnwFlightJSONArray.length(); j++) {

                    FlightOnWardDetailBean lFlightOnwDetailBean = new FlightOnWardDetailBean();
                    JSONObject lsingleflightOnwJSONObject =  (JSONObject) lOnwFlightJSONArray.get(j);
                    lFlightOnwDetailBean.setFlightFare(lsingleflightOnwJSONObject.getInt("total"));
                    lFlightOnwDetailBean.setFlightAirline(lsingleflightOnwJSONObject.optString("pc"));
                    // Added by vivek for flight info screen
                    lFlightOnwDetailBean.setFlightStaxInFare(lsingleflightOnwJSONObject.getInt("stax"));
                    // Added by asif for Galilio nad International Flight
                    if(lsingleflightOnwJSONObject.getInt("stax") != 0){

                        lFlightOnwDetailBean.setFlightFare(lsingleflightOnwJSONObject.getInt("total") - lsingleflightOnwJSONObject.getInt("stax"));
                    }
                    lFlightOnwDetailBean.setFlightTaxInFare(lsingleflightOnwJSONObject.getInt("ttax"));
                    lFlightOnwDetailBean.setFlightBaseFare(lsingleflightOnwJSONObject.getInt("bp"));
                    lFlightOnwDetailBean.setFlightJourneyStartFrom(lsingleflightOnwJSONObject.optString("origin"));
                    lFlightOnwDetailBean.setFlightJourneyEndTo(lsingleflightOnwJSONObject.optString("destination"));
                    lFlightOnwDetailBean.setFlightNumber(lsingleflightOnwJSONObject.optString("id"));

                    // set the min or max value of onwFlight fare price
                    if(j == 0){
                        minPrice = lsingleflightOnwJSONObject.getInt("total") - lsingleflightOnwJSONObject.getInt("stax");
                        maxPrice = lsingleflightOnwJSONObject.getInt("total") - lsingleflightOnwJSONObject.getInt("stax");
                    }
                    else
                    {
                        if(lsingleflightOnwJSONObject.getInt("total") - lsingleflightOnwJSONObject.getInt("stax") < minPrice ){
                            minPrice = lsingleflightOnwJSONObject.getInt("total") - lsingleflightOnwJSONObject.getInt("stax");
                        }
                        if(lsingleflightOnwJSONObject.getInt("total") - lsingleflightOnwJSONObject.getInt("stax") > maxPrice ){
                            maxPrice = lsingleflightOnwJSONObject.getInt("total") - lsingleflightOnwJSONObject.getInt("stax");
                        }
                    }

                    // Here set the flight json obj as string
                    lFlightOnwDetailBean.setFlightJson(lsingleflightOnwJSONObject.toString());


                    /**
                     * Oct 24 2016
                     * UNIQUE ID changed setFlightUniqueId
                     */
                    if(j == 0) {
                        lFlightOnwDetailBean.setFlightUniqueId(j);
                    }else{
                        int lastUniqueId = AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().size();
                        lFlightOnwDetailBean.setFlightUniqueId(lastUniqueId++);
                    }


                    setAirLineIcon(lsingleflightOnwJSONObject, lFlightOnwDetailBean);

                    // Flight tag in main lsingleflightJSONObject
                    JSONArray inFlightJSONArray = lsingleflightOnwJSONObject.getJSONArray("flight");
                    //System.out.println("Total  flightJSON Array :  " + inFlightJSONArray.length());

                    // ArrayList for inner flight key
                    ArrayList<InnerFlightDetailsBeans> lInnerFlightAlObj = new ArrayList<>();

                    for (int i = 0; i < inFlightJSONArray.length(); i++) {
                        //System.out.println("Flight i :  " + i);
                        JSONObject lFlightOnwJSONObject = (JSONObject) inFlightJSONArray.get(i);
                        InnerFlightDetailsBeans lInnerFlightOnwDetailsBeans = new InnerFlightDetailsBeans();

                        lInnerFlightOnwDetailsBeans.setFlightDepartureTime(DateUtility.formatDate(lFlightOnwJSONObject.optString("departuretime")));//(lFlightJSONObject.getString("departuretime").substring(11, 16));
                        lInnerFlightOnwDetailsBeans.setFlightArrivalTime(DateUtility.formatDate(lFlightOnwJSONObject.optString("arrivaltime")));//(lFlightJSONObject.getString("arrivaltime").substring(11, 16));
                        lInnerFlightOnwDetailsBeans.setFlightStops((inFlightJSONArray.length())-1);
                        lInnerFlightOnwDetailsBeans.setFlightNo(lFlightOnwJSONObject.optString("fno"));
                        // lInnerFlightDetailsBeans.setFlightDurationTime(StringFormat.stringFormat(lFlightJSONObject.optString("duration")));
                       // lInnerFlightOnwDetailsBeans.setFlightDurationTime(stringFormat(lFlightOnwJSONObject.optString("duration")));

                        lInnerFlightOnwDetailsBeans.setFlightDurationTime(lFlightOnwJSONObject.getString("duration"));

                        // Added by vivek for flight detail screen
                        // lInnerFlightOnwDetailsBeans.setFlightIcon(lFlightOnwJSONObject.optString("fno")); SET TRU METHOD
                        lInnerFlightOnwDetailsBeans.setOac(lFlightOnwJSONObject.optString("oac"));
                        lInnerFlightOnwDetailsBeans.setDepartureairport(lFlightOnwJSONObject.optString("departureairport"));
                        lInnerFlightOnwDetailsBeans.setDepart(lFlightOnwJSONObject.optString("depart"));
                        lInnerFlightOnwDetailsBeans.setArrivalairport(lFlightOnwJSONObject.optString("arrivalairport"));
                        lInnerFlightOnwDetailsBeans.setArrive(lFlightOnwJSONObject.optString("arrive"));
                        lInnerFlightOnwDetailsBeans.setTkt(lFlightOnwJSONObject.optString("tkt"));
                        lInnerFlightOnwDetailsBeans.setInfantTicketType(lFlightOnwJSONObject.optString("infantTicketType"));
                        lInnerFlightOnwDetailsBeans.setVia(lFlightOnwJSONObject.optString("via"));
                        lInnerFlightOnwDetailsBeans.setOnwardorreturn(lFlightOnwJSONObject.optString("onwardorreturn"));


                        // SET FLIGHT ICON IN INNER BEAN
                        setFlightSearchInnerAirLineIcon(lFlightOnwJSONObject, lInnerFlightOnwDetailsBeans);

                        lInnerFlightAlObj.add(lInnerFlightOnwDetailsBeans);

                    }

                    lFlightOnwDetailBean.setFlight(lInnerFlightAlObj);
                    getDurationAndStops1(lFlightOnwDetailBean, lInnerFlightAlObj, lInnerFlightAlObj.size());

                    /**
                     * Oct 18 2016
                     * FLIGHT ON/OFF
                     *
                     * - Get offAirlineStr initially
                     * - While parsing flight details, if pc code of flight detail is in offAirlineStr, dont save it in Holder.
                     *
                     */

                    // Issue bcz of uniqueId

                    if(!offFlightCodeArray.contains(lsingleflightOnwJSONObject.optString("pc"))){
                        AirDataHolder.getListHolder().getList().get(0).setOnWalAllFlightDetails(lFlightOnwDetailBean);

                        // If flight is not available then its min/max price is also not used
                        AirDataHolder.getListHolder().getList().get(0).setOnwFlightFareMin(minPrice);
                        AirDataHolder.getListHolder().getList().get(0).setOnwFlightFareMax(maxPrice);
                    }




                }

                    AppLogger.enable();
                    AppLogger.e("Total  flight  after off ", "" + AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().size());
            }

            }



            /************************************
             * RETURN FLIGHT PARSING
             ************************************/
           if (mJsonObject.has("Ret")) {

                JSONArray lOnwFlightJSONArray = mJsonObject.getJSONArray("Ret");
                AppLogger.enable();
                AppLogger.e("Total  flight  in Ret ", "" + lOnwFlightJSONArray.length());

                //  ArrayList<FlightOnWardDetailBean> flightsOnwHolder = new ArrayList<>();

                for (int j=0; j < lOnwFlightJSONArray.length(); j++) {

                    FlightOnWardDetailBean lFlightOnwDetailBean = new FlightOnWardDetailBean();
                    JSONObject lsingleflightOnwJSONObject =  (JSONObject) lOnwFlightJSONArray.get(j);
                    lFlightOnwDetailBean.setFlightFare(lsingleflightOnwJSONObject.getInt("total"));
                    lFlightOnwDetailBean.setFlightAirline(lsingleflightOnwJSONObject.getString("pc"));

                    // Added by asif for Galilio nad International Flight
                    lFlightOnwDetailBean.setFlightFare(lsingleflightOnwJSONObject.getInt("total") - lsingleflightOnwJSONObject.getInt("stax"));


                    // Added by asif for round flight info screen
                    lFlightOnwDetailBean.setFlightTaxInFare(lsingleflightOnwJSONObject.getInt("ttax"));
                    lFlightOnwDetailBean.setFlightBaseFare(lsingleflightOnwJSONObject.getInt("bp"));
                    lFlightOnwDetailBean.setFlightJourneyStartFrom(lsingleflightOnwJSONObject.optString("origin"));
                    lFlightOnwDetailBean.setFlightJourneyEndTo(lsingleflightOnwJSONObject.optString("destination"));
                    lFlightOnwDetailBean.setFlightNumber(lsingleflightOnwJSONObject.optString("id"));

                     // set the min or max value of onwFlight fare price
                    if(j == 0){
                        minPrice = lsingleflightOnwJSONObject.getInt("total") - lsingleflightOnwJSONObject.getInt("stax");
                        maxPrice = lsingleflightOnwJSONObject.getInt("total") - lsingleflightOnwJSONObject.getInt("stax");
                    }
                    else
                    {
                        if(lsingleflightOnwJSONObject.getInt("total") - lsingleflightOnwJSONObject.getInt("stax") < minPrice ){
                            minPrice = lsingleflightOnwJSONObject.getInt("total") - lsingleflightOnwJSONObject.getInt("stax");
                        }
                        if(lsingleflightOnwJSONObject.getInt("total") - lsingleflightOnwJSONObject.getInt("stax") > maxPrice ){
                            maxPrice = lsingleflightOnwJSONObject.getInt("total") - lsingleflightOnwJSONObject.getInt("stax");
                        }
                    }

                    // here set the flight json obj as string
                    lFlightOnwDetailBean.setFlightJson(lsingleflightOnwJSONObject.toString());

                    /**
                     * Oct 24 2016
                     * UNIQUE ID changed setFlightUniqueId
                     */
                    if(j == 0) {
                        lFlightOnwDetailBean.setFlightUniqueId(j);
                    }else{
                        int lastUniqueId = AirDataHolder.getListHolder().getList().get(0).getReturnAlAllFlightDetails().size();
                        lFlightOnwDetailBean.setFlightUniqueId(lastUniqueId++);
                    }


                    setAirLineIcon(lsingleflightOnwJSONObject, lFlightOnwDetailBean);

                    // flight tag in main lsingleflightJSONObject
                    JSONArray inFlightJSONArray = lsingleflightOnwJSONObject.getJSONArray("flight");
                    //System.out.println("Total  flightJSONArray :  " + inFlightJSONArray.length());
                    // ArrayList for inner flight key
                    ArrayList<InnerFlightDetailsBeans> lInnerFlightAlObj = new ArrayList<>();

                    for (int i = 0; i < inFlightJSONArray.length(); i++) {

                        JSONObject lFlightOnwJSONObject = (JSONObject) inFlightJSONArray.get(i);
                        InnerFlightDetailsBeans lInnerFlightOnwDetailsBeans = new InnerFlightDetailsBeans();

                        lInnerFlightOnwDetailsBeans.setFlightDepartureTime(DateUtility.formatDate(lFlightOnwJSONObject.getString("departuretime")));//(lFlightJSONObject.getString("departuretime").substring(11, 16));
                        lInnerFlightOnwDetailsBeans.setFlightArrivalTime(DateUtility.formatDate(lFlightOnwJSONObject.getString("arrivaltime")));//(lFlightJSONObject.getString("arrivaltime").substring(11, 16));
                        lInnerFlightOnwDetailsBeans.setFlightStops((inFlightJSONArray.length())-1);
                        lInnerFlightOnwDetailsBeans.setFlightNo(lFlightOnwJSONObject.getString("fno"));
                        // lInnerFlightDetailsBeans.setFlightDurationTime(StringFormat.stringFormat(lFlightJSONObject.getString("duration")));
                        //lInnerFlightOnwDetailsBeans.setFlightDurationTime(stringFormat(lFlightOnwJSONObject.getString("duration")));
                        lInnerFlightOnwDetailsBeans.setFlightDurationTime(lFlightOnwJSONObject.getString("duration"));
                        // Added by vivek for flight detail screen
                        // lInnerFlightOnwDetailsBeans.setFlightIcon(lFlightOnwJSONObject.optString("fno")); SET TRU METHOD
                        lInnerFlightOnwDetailsBeans.setOac(lFlightOnwJSONObject.optString("oac"));
                        lInnerFlightOnwDetailsBeans.setDepartureairport(lFlightOnwJSONObject.optString("departureairport"));
                        lInnerFlightOnwDetailsBeans.setDepart(lFlightOnwJSONObject.optString("depart"));
                        lInnerFlightOnwDetailsBeans.setArrivalairport(lFlightOnwJSONObject.optString("arrivalairport"));
                        lInnerFlightOnwDetailsBeans.setArrive(lFlightOnwJSONObject.optString("arrive"));
                        lInnerFlightOnwDetailsBeans.setTkt(lFlightOnwJSONObject.optString("tkt"));
                        lInnerFlightOnwDetailsBeans.setInfantTicketType(lFlightOnwJSONObject.optString("infantTicketType"));
                        lInnerFlightOnwDetailsBeans.setVia(lFlightOnwJSONObject.optString("via"));
                        lInnerFlightOnwDetailsBeans.setOnwardorreturn(lFlightOnwJSONObject.optString("onwardorreturn"));

                        // SET FLIGHT ICON IN INNER BEAN
                        setFlightSearchInnerAirLineIcon(lFlightOnwJSONObject, lInnerFlightOnwDetailsBeans);

                        lInnerFlightAlObj.add(lInnerFlightOnwDetailsBeans);
                    }
                    getDurationAndStops1(lFlightOnwDetailBean, lInnerFlightAlObj, lInnerFlightAlObj.size());
                    lFlightOnwDetailBean.setFlight(lInnerFlightAlObj);

                    /**
                     * Oct 18 2016
                     * FLIGHT ON/OFF
                     *
                     * - Get offAirlineStr initially
                     * - While parsing flight details, if pc code of flight detail is in offAirlineStr, dont save it in Holder.
                     *
                     */
                    if(!offFlightCodeArray.contains(lsingleflightOnwJSONObject.getString("pc"))){
                        AirDataHolder.getListHolder().getList().get(0).setReturnAlAllFlightDetails(lFlightOnwDetailBean);

                        AirDataHolder.getListHolder().getList().get(0).setRetFlightFareMin(minPrice);
                        AirDataHolder.getListHolder().getList().get(0).setRetFlightFareMax(maxPrice);
                    }



                }

               AppLogger.enable();
               AppLogger.e("Total  flight  after off ", "" + AirDataHolder.getListHolder().getList().get(0).getReturnAlAllFlightDetails().size());
            }


             /************************************
             * ROUND ONWARD FLIGHT PARSING
             ************************************/
            if (mJsonObject.has("RouOnw")) {

                int lccCounter = 0 ;
                int gdsCounter = 0 ;
                RouOnwGdcLccBean rouOnwGdcLccBean =  new RouOnwGdcLccBean();
                JSONArray lRouOnwFlightJSONArray = mJsonObject.getJSONArray("RouOnw");
                AppLogger.enable();
                AppLogger.e("Total  flight  in RouOnw ", "" + lRouOnwFlightJSONArray.length());

                for (int j=0; j < lRouOnwFlightJSONArray.length(); j++) {

                    FlightOnWardDetailBean lFlightOnwDetailBean = new FlightOnWardDetailBean();
                    JSONObject lsingleflightOnwJSONObject =  (JSONObject) lRouOnwFlightJSONArray.get(j);
                    lFlightOnwDetailBean.setFlightFare(lsingleflightOnwJSONObject.getInt("total"));
                    lFlightOnwDetailBean.setFlightAirline(lsingleflightOnwJSONObject.getString("pc"));

                    // Added by asif for Galilio nad International Flight
                    lFlightOnwDetailBean.setFlightFare(lsingleflightOnwJSONObject.getInt("total") - lsingleflightOnwJSONObject.getInt("stax"));

                    // Added by asif for round flight info screen
                    lFlightOnwDetailBean.setFlightTaxInFare(lsingleflightOnwJSONObject.getInt("ttax"));
                    lFlightOnwDetailBean.setFlightBaseFare(lsingleflightOnwJSONObject.getInt("bp"));
                    lFlightOnwDetailBean.setFlightJourneyStartFrom(lsingleflightOnwJSONObject.optString("origin"));
                    lFlightOnwDetailBean.setFlightJourneyEndTo(lsingleflightOnwJSONObject.optString("destination"));
                    lFlightOnwDetailBean.setFlightNumber(lsingleflightOnwJSONObject.optString("id"));



                    // here set the flight json obj as string
                    lFlightOnwDetailBean.setFlightJson(lsingleflightOnwJSONObject.toString());
                  //  lFlightOnwDetailBean.setFlightUniqueId(j);

                    setAirLineIcon(lsingleflightOnwJSONObject, lFlightOnwDetailBean);

                    // flight tag in main lsingleflightJSONObject
                    JSONArray inFlightJSONArray = lsingleflightOnwJSONObject.getJSONArray("flight");
                    //System.out.println("Total  flightJSONArray :  " + inFlightJSONArray.length());
                    // ArrayList for inner flight key
                    ArrayList<InnerFlightDetailsBeans> lInnerFlightAlObj = new ArrayList<>();

                    for (int i = 0; i < inFlightJSONArray.length(); i++) {

                        JSONObject lFlightOnwJSONObject = (JSONObject) inFlightJSONArray.get(i);
                        InnerFlightDetailsBeans lInnerFlightOnwDetailsBeans = new InnerFlightDetailsBeans();
                        lInnerFlightOnwDetailsBeans.setFlightOnwRetType(lFlightOnwJSONObject.getString("onwardorreturn"));


                        lInnerFlightOnwDetailsBeans.setFlightDepartureTime(DateUtility.formatDate(lFlightOnwJSONObject.getString("departuretime")));//(lFlightJSONObject.getString("departuretime").substring(11, 16));
                        lInnerFlightOnwDetailsBeans.setFlightArrivalTime(DateUtility.formatDate(lFlightOnwJSONObject.getString("arrivaltime")));//(lFlightJSONObject.getString("arrivaltime").substring(11, 16));
                        lInnerFlightOnwDetailsBeans.setFlightStops((inFlightJSONArray.length())-1);
                        lInnerFlightOnwDetailsBeans.setFlightNo(lFlightOnwJSONObject.getString("fno"));
                        // lInnerFlightDetailsBeans.setFlightDurationTime(StringFormat.stringFormat(lFlightJSONObject.getString("duration")));
                       // lInnerFlightOnwDetailsBeans.setFlightDurationTime(stringFormat(lFlightOnwJSONObject.getString("duration")));
                        lInnerFlightOnwDetailsBeans.setFlightDurationTime(lFlightOnwJSONObject.getString("duration"));
                        // Added by vivek for flight detail screen
                        // lInnerFlightOnwDetailsBeans.setFlightIcon(lFlightOnwJSONObject.optString("fno")); SET TRU METHOD
                        lInnerFlightOnwDetailsBeans.setOac(lFlightOnwJSONObject.optString("oac"));
                        lInnerFlightOnwDetailsBeans.setDepartureairport(lFlightOnwJSONObject.optString("departureairport"));
                        lInnerFlightOnwDetailsBeans.setDepart(lFlightOnwJSONObject.optString("depart"));
                        lInnerFlightOnwDetailsBeans.setArrivalairport(lFlightOnwJSONObject.optString("arrivalairport"));
                        lInnerFlightOnwDetailsBeans.setArrive(lFlightOnwJSONObject.optString("arrive"));
                        lInnerFlightOnwDetailsBeans.setTkt(lFlightOnwJSONObject.optString("tkt"));
                        lInnerFlightOnwDetailsBeans.setInfantTicketType(lFlightOnwJSONObject.optString("infantTicketType"));
                        lInnerFlightOnwDetailsBeans.setVia(lFlightOnwJSONObject.optString("via"));
                        lInnerFlightOnwDetailsBeans.setOnwardorreturn(lFlightOnwJSONObject.optString("onwardorreturn"));

                        // SET FLIGHT ICON IN INNER BEAN
                        setFlightSearchInnerAirLineIcon(lFlightOnwJSONObject, lInnerFlightOnwDetailsBeans);

                        lInnerFlightAlObj.add(lInnerFlightOnwDetailsBeans);
                    }

                    lFlightOnwDetailBean.setFlight(lInnerFlightAlObj);


                    if("6E_0S_G8_AK".contains(lsingleflightOnwJSONObject.getString("pc"))){
                        /**
                         * Oct 18 2016
                         * FLIGHT ON/OFF
                         *
                         * - Get offAirlineStr initially
                         * - While parsing flight details, if pc code of flight detail is in offAirlineStr, dont save it in Holder.
                         */
                        if(!offFlightCodeArray.contains(lsingleflightOnwJSONObject.getString("pc"))){
                            // get the LCC flights
                            lFlightOnwDetailBean.setFlightStaxInFare(lsingleflightOnwJSONObject.getInt("stax"));
                            getDurationAndStops1(lFlightOnwDetailBean, lInnerFlightAlObj, lInnerFlightAlObj.size());
                            lFlightOnwDetailBean.setFlightUniqueId(lccCounter);
                            rouOnwGdcLccBean.setFlightLCC(lFlightOnwDetailBean);
                            //AirDataHolder.getListHolder().getList().get(0).setRouOnwGdsLccBean(rouOnwGdcLccBean);
                            lccCounter++;
                        }

                    }else{
                        /**
                         * Oct 18 2016
                         * FLIGHT ON/OFF
                         *
                         * - Get offAirlineStr initially
                         * - While parsing flight details, if pc code of flight detail is in offAirlineStr, dont save it in Holder.
                         */
                        if(!offFlightCodeArray.contains(lsingleflightOnwJSONObject.getString("pc"))) {
                            // get the GDS flights
                            lFlightOnwDetailBean.setFlightStaxInFare(lsingleflightOnwJSONObject.getInt("stax"));
                            getDurationAndStops(lFlightOnwDetailBean, lInnerFlightAlObj, lInnerFlightAlObj.size());
                            lFlightOnwDetailBean.setFlightUniqueId(gdsCounter);
                            rouOnwGdcLccBean.setFlightGDS(lFlightOnwDetailBean);
                            //AirDataHolder.getListHolder().getList().get(0).setRouOnwGdsLccBean(rouOnwGdcLccBean);
                            gdsCounter++;
                        }
                    }
                }



                /**
                 *  CREATE THE OBJ OF RouOnwGdcLccBean
                 *  SET LCC FLIGHTS INTO setFlightLCC
                 *  SET GDS FLIGHTS INTO setFlightGDS
                 *  SET IT INTO THE AMIN HOLDER
                 */

                 AirDataHolder.getListHolder().getList().get(0).setRouOnwGdsLccBean(rouOnwGdcLccBean);


                AppLogger.enable();
                AppLogger.e("Total  flight  after off GDS", "" + AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightGDS().size());
                AppLogger.e("Total  flight  after off LCC", "" + AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightLCC().size());

            }


             /************************************
             * ROUND RETURN FLIGHT PARSING
             ************************************/
            if (mJsonObject.has("RouRet")) {

                JSONArray lOnwFlightJSONArray = mJsonObject.getJSONArray("RouRet");
                AppLogger.enable();
                AppLogger.e("Total  flight  in RouRet ", "" + lOnwFlightJSONArray.length());

                //  ArrayList<FlightOnWardDetailBean> flightsOnwHolder = new ArrayList<>();

                for (int j=0; j < lOnwFlightJSONArray.length(); j++) {

                    FlightOnWardDetailBean lFlightOnwDetailBean = new FlightOnWardDetailBean();
                    JSONObject lsingleflightOnwJSONObject =  (JSONObject) lOnwFlightJSONArray.get(j);
                    lFlightOnwDetailBean.setFlightFare(lsingleflightOnwJSONObject.getInt("total"));
                    lFlightOnwDetailBean.setFlightAirline(lsingleflightOnwJSONObject.getString("pc"));

                    // Added by asif for Galilio nad International Flight
                    if(lsingleflightOnwJSONObject.getInt("stax") != 0){

                        lFlightOnwDetailBean.setFlightFare(lsingleflightOnwJSONObject.getInt("total") - lsingleflightOnwJSONObject.getInt("stax"));
                    }
                    // Added by asif for round flight info screen
                    lFlightOnwDetailBean.setFlightTaxInFare(lsingleflightOnwJSONObject.getInt("ttax"));
                    lFlightOnwDetailBean.setFlightBaseFare(lsingleflightOnwJSONObject.getInt("bp"));
                    lFlightOnwDetailBean.setFlightJourneyStartFrom(lsingleflightOnwJSONObject.optString("origin"));
                    lFlightOnwDetailBean.setFlightJourneyEndTo(lsingleflightOnwJSONObject.optString("destination"));
                    lFlightOnwDetailBean.setFlightNumber(lsingleflightOnwJSONObject.optString("id"));

                    // set the min or max value of onwFlight fare price
//                    if(j == 0){
//                        minPrice = lsingleflightOnwJSONObject.getInt("total");
//                        maxPrice = lsingleflightOnwJSONObject.getInt("total");
//                    }
//                    else
//                    {
//                        if(lsingleflightOnwJSONObject.getInt("total") < minPrice ){
//                            minPrice = lsingleflightOnwJSONObject.getInt("total");
//                        }
//                        if(lsingleflightOnwJSONObject.getInt("total") > maxPrice ){
//                            maxPrice = lsingleflightOnwJSONObject.getInt("total");
//                        }
//                    }

                    // here set the flight json obj as string
                    lFlightOnwDetailBean.setFlightJson(lsingleflightOnwJSONObject.toString());

                    /**
                     * Oct 24 2016
                     * UNIQUE ID changed setFlightUniqueId
                     */
                    if(j == 0) {
                        lFlightOnwDetailBean.setFlightUniqueId(j);
                    }else{
                        int lastUniqueId = AirDataHolder.getListHolder().getList().get(0).getRouRetalAllFlightDetails().size();
                        lFlightOnwDetailBean.setFlightUniqueId(lastUniqueId++);
                    }


                    setAirLineIcon(lsingleflightOnwJSONObject, lFlightOnwDetailBean);

                    // flight tag in main lsingleflightJSONObject
                    JSONArray inFlightJSONArray = lsingleflightOnwJSONObject.getJSONArray("flight");
                    //System.out.println("Total  flightJSONArray :  " + inFlightJSONArray.length());
                    // ArrayList for inner flight key
                    ArrayList<InnerFlightDetailsBeans> lInnerFlightAlObj = new ArrayList<>();

                    for (int i = 0; i < inFlightJSONArray.length(); i++) {

                        JSONObject lFlightOnwJSONObject = (JSONObject) inFlightJSONArray.get(i);
                        InnerFlightDetailsBeans lInnerFlightOnwDetailsBeans = new InnerFlightDetailsBeans();


                        lInnerFlightOnwDetailsBeans.setFlightDepartureTime(DateUtility.formatDate(lFlightOnwJSONObject.getString("departuretime")));//(lFlightJSONObject.getString("departuretime").substring(11, 16));
                        lInnerFlightOnwDetailsBeans.setFlightArrivalTime(DateUtility.formatDate(lFlightOnwJSONObject.getString("arrivaltime")));//(lFlightJSONObject.getString("arrivaltime").substring(11, 16));
                        lInnerFlightOnwDetailsBeans.setFlightStops((inFlightJSONArray.length())-1);
                        lInnerFlightOnwDetailsBeans.setFlightNo(lFlightOnwJSONObject.getString("fno"));
                        // lInnerFlightDetailsBeans.setFlightDurationTime(StringFormat.stringFormat(lFlightJSONObject.getString("duration")));
                      //  lInnerFlightOnwDetailsBeans.setFlightDurationTime(stringFormat(lFlightOnwJSONObject.getString("duration")));
                        lInnerFlightOnwDetailsBeans.setFlightDurationTime(lFlightOnwJSONObject.getString("duration"));
                        // Added by vivek for flight detail screen
                        // lInnerFlightOnwDetailsBeans.setFlightIcon(lFlightOnwJSONObject.optString("fno")); SET TRU METHOD
                        lInnerFlightOnwDetailsBeans.setOac(lFlightOnwJSONObject.optString("oac"));
                        lInnerFlightOnwDetailsBeans.setDepartureairport(lFlightOnwJSONObject.optString("departureairport"));
                        lInnerFlightOnwDetailsBeans.setDepart(lFlightOnwJSONObject.optString("depart"));
                        lInnerFlightOnwDetailsBeans.setArrivalairport(lFlightOnwJSONObject.optString("arrivalairport"));
                        lInnerFlightOnwDetailsBeans.setArrive(lFlightOnwJSONObject.optString("arrive"));
                        lInnerFlightOnwDetailsBeans.setTkt(lFlightOnwJSONObject.optString("tkt"));
                        lInnerFlightOnwDetailsBeans.setInfantTicketType(lFlightOnwJSONObject.optString("infantTicketType"));
                        lInnerFlightOnwDetailsBeans.setVia(lFlightOnwJSONObject.optString("via"));
                        lInnerFlightOnwDetailsBeans.setOnwardorreturn(lFlightOnwJSONObject.optString("onwardorreturn"));

                        // SET FLIGHT ICON IN INNER BEAN
                        setFlightSearchInnerAirLineIcon(lFlightOnwJSONObject, lInnerFlightOnwDetailsBeans);

                        lInnerFlightAlObj.add(lInnerFlightOnwDetailsBeans);
                    }

                    getDurationAndStops1(lFlightOnwDetailBean, lInnerFlightAlObj, lInnerFlightAlObj.size());
                    lFlightOnwDetailBean.setFlight(lInnerFlightAlObj);


                    /**
                     * Oct 18 2016
                     * FLIGHT ON/OFF
                     *
                     * - Get offAirlineStr initially
                     * - While parsing flight details, if pc code of flight detail is in offAirlineStr, dont save it in Holder.
                     *
                     */
                    if(!offFlightCodeArray.contains(lsingleflightOnwJSONObject.getString("pc"))){
                        AirDataHolder.getListHolder().getList().get(0).setRouRetalAllFlightDetails(lFlightOnwDetailBean);
                    }

//                    AirDataHolder.getListHolder().getList().get(0).setOnwFlightFareMin(minPrice);
//                    AirDataHolder.getListHolder().getList().get(0).setOnwFlightFareMax(maxPrice);
                }

                AppLogger.enable();
                AppLogger.e("Total  flight  after off", "" + AirDataHolder.getListHolder().getList().get(0).getRouRetalAllFlightDetails().size());
            }
        } catch (Exception e) {
            e.printStackTrace();

            AppLogger.enable();
            AppLogger.e("EXCEPTION in flightSearchResponseParser","");
        }
    }


    private void getDurationAndStops1(FlightOnWardDetailBean lFlightOnwDetailBean, ArrayList<InnerFlightDetailsBeans> onwardType, int lflightLength) {


        if(lflightLength == 1){

            String onwDepartTime = onwardType.get(0).getFlightDepartureTime();
            String  onwArrivalTime = onwardType.get(onwardType.size()-1).getFlightArrivalTime();
            DateUtility.getTimeDifferenceInTwoDate(onwArrivalTime, onwDepartTime);

            //DateUtility.getTimeDifferenceInTwoDate(onwArrivalTime, onwDepartTime)

            if(onwardType.get(0).getFlightDurationTime().equalsIgnoreCase("") ){

                lFlightOnwDetailBean.setFlightOnwardTotalDuration(DateUtility.getTimeDifferenceInTwoDate(onwArrivalTime, onwDepartTime));
            }
            else{

                lFlightOnwDetailBean.setFlightOnwardTotalDuration(onwardType.get(0).getFlightDurationTime());
            }

            lFlightOnwDetailBean.setFlightOnwardTotalStops(onwardType.size() - 1);
            lFlightOnwDetailBean.setFlightOnwDepartureTime(onwDepartTime);
            lFlightOnwDetailBean.setFlightOnwArrivalTime(onwArrivalTime);

        }else{

            // here to add layover time & duration to get the exact time


            String onwDepartTime = onwardType.get(0).getFlightDepartureTime();
            String  onwArrivalTime = onwardType.get(onwardType.size()-1).getFlightArrivalTime();
            DateUtility.getTimeDifferenceInTwoDate(onwArrivalTime, onwDepartTime);

            lFlightOnwDetailBean.setFlightOnwardTotalDuration(getDurationAccurateTime(lFlightOnwDetailBean,onwardType ));
            lFlightOnwDetailBean.setFlightOnwardTotalStops(onwardType.size() - 1);
            lFlightOnwDetailBean.setFlightOnwDepartureTime(onwDepartTime);
            lFlightOnwDetailBean.setFlightOnwArrivalTime(onwArrivalTime);

            getDurationAccurateTime( lFlightOnwDetailBean,onwardType );

        }
    }

    private String getDurationAccurateTime(FlightOnWardDetailBean lFlightOnwDetailBean, ArrayList<InnerFlightDetailsBeans> onwardType){

        mHour = 0;
        mMinutes = 0;
        int lHour = 0;

        for (int i = 0; i < onwardType.size(); i++) {

            String lStrDuration = onwardType.get(i).getFlightDurationTime();

            if(lStrDuration.contains("days")){

                String str[] = lStrDuration.split(",");
                int day = (int)Double.parseDouble(str[0].replace("days","").trim());
                lHour = day*24;
                lStrDuration = str[1];
            }

            if(lStrDuration.equalsIgnoreCase("")){

                lStrDuration =  DateUtility.getTimeDifferenceInTwoDate(onwardType.get(0).getFlightArrivalTime(), onwardType.get(0).getFlightDepartureTime());
            }

            String lStrDurationFromated;
            if(lStrDuration.contains("h ")) {
                lStrDurationFromated = lStrDuration.replace("h ", ":").replace("m", "");
            }
            else{
                lStrDurationFromated = lStrDuration.replace("h", ":").replace("m", "");
            }

            if(i == 0){

                getAccurateTime(lStrDurationFromated);
            }
            else{

                getAccurateTime(lStrDurationFromated);
                // Call timeDifferenceInTwoDate to get time difference
                getAccurateTime(DateUtility.getTimeDifferenceInTwoDate(onwardType.get(i).getFlightDepartureTime(), onwardType.get(i - 1).getFlightArrivalTime()).replace("h ", ":").replace("m", ""));

            }
        }

      //  lFlightOnwDetailBean.setFlightOnwardTotalDuration(mHour +"h: "+mMinutes+"m");



      return (mHour + lHour) +"h "+mMinutes+"m";
    }

    private void getAccurateTime(String lStrDurationFromated){

        String arr[];
        int minute = 0;;
        int hour = 0;

        if(!lStrDurationFromated.contains(":")){
            minute = Integer.parseInt(lStrDurationFromated.trim());
        }else{

            arr = lStrDurationFromated.split(":");
            hour = Integer.parseInt(arr[0]);

            if(arr.length == 1) {
                minute = 0;
            }else{
                minute = Integer.parseInt(arr[1]);
            }

        }

        mHour += hour;
        mMinutes += minute;

        if(mMinutes >= 60){

            mHour = mHour + mMinutes/60;
            mMinutes = mMinutes%60;
        }

    }

    // SET ROUND ONWARD EXACT DURATION TIME AND STOPS
    private void  getDurationAndStops(FlightOnWardDetailBean lFlightOnwDetailBean, ArrayList<InnerFlightDetailsBeans> alInnerBeans, int lflightLength) {

        ArrayList<InnerFlightDetailsBeans> onwardType = null;
        ArrayList<InnerFlightDetailsBeans> returnType = null ;
        onwardType = new ArrayList<>();
        returnType = new ArrayList<>();

        if(alInnerBeans.size() > 1){

            for (int i = 0; i < lflightLength ; i++) {

                InnerFlightDetailsBeans inner =  alInnerBeans.get(i);

                if(inner.getFlightOnwRetType().equalsIgnoreCase("onward")){
                    onwardType.add(inner);
                }else if(inner.getFlightOnwRetType().equalsIgnoreCase("return")){
                    returnType.add(inner);
                }
            }

            String onwDepartTime = onwardType.get(0).getFlightDepartureTime();
            String  onwArrivalTime = onwardType.get(onwardType.size()-1).getFlightArrivalTime();

            //lFlightOnwDetailBean.setFlightOnwardTotalDuration(DateUtility.getTimeDifferenceInTwoDate(retArrivalTime, retDepartTime));

            lFlightOnwDetailBean.setFlightOnwardTotalDuration(getDurationAccurateTime(lFlightOnwDetailBean, onwardType));
            lFlightOnwDetailBean.setFlightOnwardTotalStops(onwardType.size() - 1);
            lFlightOnwDetailBean.setFlightOnwDepartureTime(onwDepartTime);
            lFlightOnwDetailBean.setFlightOnwArrivalTime(onwArrivalTime);
            lFlightOnwDetailBean.setFlightOnwardFlightNo(onwardType.get(0).getFlightNo());

            String retDepartTime = returnType.get(0).getFlightDepartureTime();
            String retArrivalTime = returnType.get(returnType.size()-1).getFlightArrivalTime();

            //lFlightOnwDetailBean.setFlightReturnTotalDuration(DateUtility.getTimeDifferenceInTwoDate(retArrivalTime, retDepartTime));

            lFlightOnwDetailBean.setFlightReturnTotalDuration(getDurationAccurateTime(lFlightOnwDetailBean, returnType));
            lFlightOnwDetailBean.setFlightreturnTotalStops(returnType.size() - 1);
            lFlightOnwDetailBean.setFlightRetDepartureTime(retDepartTime);
            lFlightOnwDetailBean.setFlightRetArrivalTime(retArrivalTime);
            lFlightOnwDetailBean.setFlightReturnFlightNo(returnType.get(0).getFlightNo());

        }
        else{

            for (int i = 0; i < lflightLength ; i++) {

                InnerFlightDetailsBeans inner =  alInnerBeans.get(i);

                if(inner.getFlightOnwRetType().equalsIgnoreCase("onward")){
                    onwardType.add(inner);
                }
            }

            String onwDepartTime = onwardType.get(0).getFlightDepartureTime();
            String  onwArrivalTime = onwardType.get(onwardType.size()-1).getFlightArrivalTime();
            DateUtility.getTimeDifferenceInTwoDate(onwArrivalTime , onwDepartTime);

            lFlightOnwDetailBean.setFlightOnwardTotalDuration(DateUtility.getTimeDifferenceInTwoDate(onwArrivalTime, onwDepartTime));
            lFlightOnwDetailBean.setFlightOnwardTotalStops(onwardType.size() - 1);
            lFlightOnwDetailBean.setFlightOnwDepartureTime(onwDepartTime);
            lFlightOnwDetailBean.setFlightOnwArrivalTime(onwArrivalTime);
        }
    }


    /************************************
     * SET FLIGHT ICON
     ************************************/
    private void setLowestAirLineIcon(String lsingleflightCode, FilterCheapFareAirlineBean lFlightDetailBean) {


        lFlightDetailBean.setmFlightIcon(AirlineLogoUtil.getAirlineLogo(lsingleflightCode));

//        if(lsingleflightCode.equals("0S")){
//            lFlightDetailBean.setmFlightIcon(R.drawable.spicejet_logo);
//        }
//        else if(lsingleflightCode.equals("G8")){
//            lFlightDetailBean.setmFlightIcon(R.drawable.goair_logo);
//        }
//        else if(lsingleflightCode.equals("6E")){
//            lFlightDetailBean.setmFlightIcon(R.drawable.indigo_logo);
//        }
//        else if(lsingleflightCode.equals("AI")){
//            lFlightDetailBean.setmFlightIcon(R.drawable.airindia);
//        }
//        else if(lsingleflightCode.equals("UK")){
//            lFlightDetailBean.setmFlightIcon(R.drawable.vistara);//vistara Logo
//        }
//        else if(lsingleflightCode.equals("9W")){
//            lFlightDetailBean.setmFlightIcon(R.drawable.jetair_logo);
//        }
//        else if(lsingleflightCode.equals("AK")){
//            lFlightDetailBean.setmFlightIcon(R.drawable.airasia);
//        }
//        else if(lsingleflightCode.equals("S2")){
//            lFlightDetailBean.setmFlightIcon(R.drawable.ijetkonnect);
//        }
//        else{
//            lFlightDetailBean.setmFlightIcon(R.drawable.onward_icon);
//        }
    }

    private void setAirLineIcon(JSONObject lsingleflightJSONObject, FlightOnWardDetailBean lFlightDetailBean) {

        lFlightDetailBean.setFlightIcon(AirlineLogoUtil.getAirlineLogo(lsingleflightJSONObject.optString("pc")));

//        if(lsingleflightJSONObject.optString("pc").equals("0S")){
//                lFlightDetailBean.setFlightIcon(R.drawable.spicejet_logo);
//            }
//            else if(lsingleflightJSONObject.optString("pc").equals("G8")){
//                lFlightDetailBean.setFlightIcon(R.drawable.goair_logo);
//            }
//            else if(lsingleflightJSONObject.optString("pc").equals("6E")){
//                lFlightDetailBean.setFlightIcon(R.drawable.indigo_logo);
//            }
//            else if(lsingleflightJSONObject.optString("pc").equals("AI")){
//                lFlightDetailBean.setFlightIcon(R.drawable.airindia);
//            }
//            else if(lsingleflightJSONObject.optString("pc").equals("UK")){
//                lFlightDetailBean.setFlightIcon(R.drawable.vistara);//vistara Logo
//            }
//            else if(lsingleflightJSONObject.optString("pc").equals("9W")){
//                lFlightDetailBean.setFlightIcon(R.drawable.jetair_logo);
//            }
//            else if(lsingleflightJSONObject.optString("pc").equals("AK")){
//                lFlightDetailBean.setFlightIcon(R.drawable.airasia);
//            }
//            else if(lsingleflightJSONObject.optString("pc").equals("S2")){
//                lFlightDetailBean.setFlightIcon(R.drawable.ijetkonnect);
//            }
//            else{
//                lFlightDetailBean.setFlightIcon(R.drawable.onward_icon);
//            }

    }



    private void setFlightSearchInnerAirLineIcon(JSONObject lsingleflightJSONObject, InnerFlightDetailsBeans lFlightDetailBean) {

        lFlightDetailBean.setFlightIcon(AirlineLogoUtil.getAirlineLogo(lsingleflightJSONObject.optString("mac")));


//        if(lsingleflightJSONObject.optString("mac").equals("0S")){
//                lFlightDetailBean.setFlightIcon(R.drawable.spicejet_logo);
//            }
//            else if(lsingleflightJSONObject.optString("mac").equals("G8")){
//                lFlightDetailBean.setFlightIcon(R.drawable.goair_logo);
//            }
//            else if(lsingleflightJSONObject.optString("mac").equals("6E")){
//                lFlightDetailBean.setFlightIcon(R.drawable.indigo_logo);
//            }
//            else if(lsingleflightJSONObject.optString("mac").equals("AI")){
//                lFlightDetailBean.setFlightIcon(R.drawable.airindia);
//            }
//            else if(lsingleflightJSONObject.optString("mac").equals("UK")){
//                lFlightDetailBean.setFlightIcon(R.drawable.vistara);//vistara Logo
//            }
//            else if(lsingleflightJSONObject.optString("mac").equals("9W")){
//                lFlightDetailBean.setFlightIcon(R.drawable.jetair_logo);
//            }
//            else if(lsingleflightJSONObject.optString("mac").equals("I5")){
//                lFlightDetailBean.setFlightIcon(R.drawable.airasia);
//            }
//            else if(lsingleflightJSONObject.optString("mac").equals("S2")){
//                lFlightDetailBean.setFlightIcon(R.drawable.ijetkonnect);
//            }
//            else{
//                lFlightDetailBean.setFlightIcon(R.drawable.onward_icon);
//            }

    }

}



