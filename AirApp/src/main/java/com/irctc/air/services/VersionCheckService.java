package com.irctc.air.services;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;

import com.irctc.air.Database.ServerDateSharedPrefernce;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.activity.SplashActivity;
import com.irctc.air.activity.UnderMaintenanceActivity;
import com.irctc.air.util.AES;
import com.irctc.air.util.AppLogger;
import com.irctc.air.util.ProjectUtil;
import com.irctc.air.Database.SharedPrefrenceAir;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vivek on 4/20/2016.
 */
public class VersionCheckService{/* extends AsyncTask<Void, Void, String>
{
    private ProgressBar progressBar = null;
    private String mStrResponse;
    private String mStrRequestXml;
    private Context mObjContext;
    private AlertDialog.Builder alertDialog;




    public VersionCheckService(Context context, ProgressBar progressBar){
        this.mObjContext = context;
        this.progressBar = progressBar;
    }



    @Override
    protected void onPreExecute ()
    {
        super.onPreExecute();
        progressBar.setVisibility(View.VISIBLE);
    }





    @Override
    protected String doInBackground ( Void... params )
    {

        int version = 0;
        try {
            version = mObjContext.getPackageManager().getPackageInfo(mObjContext.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        mStrRequestXml = "<versionCheck><version>"+version+"</version></versionCheck>";
        AppLogger.e("Req version : ", mStrRequestXml);
        mStrResponse = SoapService.getInstance2(mObjContext).getResults(mStrRequestXml,null,null,null,null,null,null, AES.decrypt(mObjContext.getResources().getString(R.string.NAMESPACE)), AES.decrypt(mObjContext.getResources().getString(R.string.FLIGHT_SEARCH_URL)), AES.decrypt(mObjContext.getResources().getString(R.string.VERSION_CHECK_METHODNAME)));

        AppLogger.e("Res version : ", mStrResponse);

        return "";
    }


    @Override
    protected void onPostExecute ( String result )
    {

        if (mStrResponse != null)
            {


            if (mStrResponse.contains("ServiceIssueSocketTimeOut"))
            {
                progressBar.setVisibility(View.INVISIBLE);
                alertDialogWithOneActionANdFinishApp(mObjContext.getResources().getString(R.string.GENRIC_ERROR_MESSAGE));
            }
            else
            {
                // PARSE JSON DATA
                if (mStrResponse != null)
                {
                    JSONObject lObjJsonTrainList = getJSONObject(mStrResponse);
                    if (lObjJsonTrainList != null)
                    {
                        if (lObjJsonTrainList.has("ErrorMsg"))
                        {

                            try {
                                alertDialogWithOneActionANdFinishApp( lObjJsonTrainList.get("ErrorMsg").toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        else
                        {

                            try {

                                //{"searchID":"APP7784537","msg":0}
                                progressBar.setVisibility(View.INVISIBLE);

                                JSONObject lObjResponse = new JSONObject(mStrResponse);


                                if (lObjResponse != null && lObjResponse.has("msg")) {

                                    // Save id
                                    if (lObjResponse.has("searchID")) {
                                        new SharedPrefrenceAir(mObjContext).SetUserUniqueId(lObjResponse.optString("searchID"));
                                    }

                                    // Save Server Date
                                    if(lObjResponse.has("serverDate")) {
                                        new ServerDateSharedPrefernce(mObjContext).setServerDate(lObjResponse.optString("serverDate"));

                                    }


                                    if (lObjResponse.optString("msg").equalsIgnoreCase("0")) {

                                        if (lObjResponse.optString("downtime").equalsIgnoreCase("TRUE")) {

                                            *//***************************************************
                                            * SHOW SITE IS UNDER MAINTENANCE
                                            ****************************************************//*

                                            Intent intent = new Intent(new Intent(mObjContext, UnderMaintenanceActivity.class));
                                            intent.putExtra("DOWN_MSG", lObjResponse.optString("imgpath"));

                                            mObjContext.startActivity(intent);
                                            ((AppCompatActivity) mObjContext).finish();

                                        }else {

                                            mObjContext.startActivity(new Intent(mObjContext, ActivityMain.class));
                                            ((AppCompatActivity) mObjContext).finish();
                                        }

                                    } else if (lObjResponse.optString("msg").equalsIgnoreCase("1")) {

                                        //minor update


                                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mObjContext);
                                        alertDialogBuilder.setTitle(mObjContext.getString(R.string.new_update_available));
                                        alertDialogBuilder.setMessage(mObjContext.getString(R.string.wish_to_update)).setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {

                                                Uri marketUri = Uri.parse("market://details?id=com.irctc.air");

                                                Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);

                                                mObjContext.startActivity(marketIntent);
                                            }
                                        })

                                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {

                                                        mObjContext.startActivity(new Intent(mObjContext, ActivityMain.class));

                                                        ((AppCompatActivity) mObjContext).finish();

                                                        *//*mObjContext.startActivity(new Intent(mObjContext, ActivityMain.class));

                                                        ((AppCompatActivity) mObjContext).finish();
*//*

                                                    }
                                                });

                                        //alertDialogBuilder.show();
                                        ((SplashActivity)mObjContext).startActivity(new Intent(mObjContext,ActivityMain.class));
                                        ((SplashActivity)mObjContext).finish();
                                    } else if (lObjResponse.optString("msg").equalsIgnoreCase("2")) {
                                        // major update

                                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mObjContext);
                                        alertDialogBuilder.setTitle(mObjContext.getString(R.string.new_update_available));
                                        alertDialogBuilder.setMessage(mObjContext.getString(R.string.update_your_app)).setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                Uri marketUri = Uri.parse("market://details?id=com.irctc.air");

                                                Intent marketIntent = new Intent(Intent.ACTION_VIEW, marketUri);

                                                mObjContext.startActivity(marketIntent);

                                            }
                                        })

                                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {

                                                        ((AppCompatActivity) mObjContext).finish();
                                                    }
                                                });

                                        //alertDialogBuilder.show();

                                        ((SplashActivity)mObjContext).startActivity(new Intent(mObjContext,ActivityMain.class));
                                        ((SplashActivity)mObjContext).finish();
                                    }

                                }else{
                                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mObjContext);
                                    alertDialogBuilder.setTitle("Version Check");
                                    alertDialogBuilder.setMessage(mObjContext.getString(R.string.ERROR_MESSAGE)).setCancelable(false).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            ((AppCompatActivity) mObjContext).finish();
                                        }
                                    });
                                }


                            }catch (Exception e){
                                AppLogger.enable();
                                AppLogger.e("VErsion Excp :", String.valueOf(e));
                            }




                        }
                    }
                    else
                    {
                        alertDialogWithOneActionANdFinishApp(mObjContext.getResources().getString(R.string.GENRIC_ERROR_MESSAGE));
                    }

                }
                else
                {
                    alertDialogWithOneActionANdFinishApp( mObjContext.getResources().getString(R.string.GENRIC_ERROR_MESSAGE));
                }
            }
        }
        else
        {
            alertDialogWithOneActionANdFinishApp(mObjContext.getResources().getString(R.string.GENRIC_ERROR_MESSAGE));
        }

        progressBar.setVisibility(View.INVISIBLE);
    }



    public JSONObject getJSONObject ( final String JSON_DATA )
    {
        JSONObject lObjJSON = null;
        try
        {
            lObjJSON = new JSONObject(JSON_DATA);
        }
        catch (JSONException e)
        {
            lObjJSON = null;
        }
        return lObjJSON;
    }


    public void alertDialogWithOneActionANdFinishApp(final String message) {

        try {
            alertDialog = new AlertDialog.Builder(mObjContext);

            alertDialog.setTitle("Version Check");

            if (message == null)
            {
                alertDialog.setMessage(R.string.ERROR_MESSAGE);
            } else if (message.isEmpty())
            {
                alertDialog.setMessage(R.string.ERROR_MESSAGE);
            }
            else{
                alertDialog.setMessage(message);
            }

            // Setting OK Button
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    dialog.cancel();
                    ((AppCompatActivity) mObjContext).finish();

                    // Write your code here to execute after dialog closed
                    //Toast.makeText(getApplicationContext(), "You clicked on OK", Toast.LENGTH_SHORT).show();
                }
            });
            // Showing Alert Message
            ProjectUtil.dialogColorAlert(alertDialog);
        }

        catch (Exception e)
        {
            ProjectUtil.showToast(mObjContext.getString(R.string.ERROR_MESSAGE), mObjContext);
        }

    }
*/}