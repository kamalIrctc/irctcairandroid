package com.irctc.air.services;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.fragment.ConfirmTicketFragment;
import com.irctc.air.parser.ConfirmTicketParser;
import com.irctc.air.util.AES;
import com.irctc.air.util.AlertDialogUtil;
import com.irctc.air.util.AppLogger;
import com.irctc.air.util.Constant;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.ProjectUtil;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vivek on 4/20/2016.
 */
public class ConfirmationTicketService{/* extends AsyncTask<Void, Void, String>
{
    private ProgressDialog dialog = null;
    private String mStrResponse;
    private Context mObjContext;
    private String mStrRequestXml;
    private ActivityMain mainActivity;


    public ConfirmationTicketService(Context context, String requestXml){
        this.mObjContext = context;
        this.mStrRequestXml = requestXml;
        this.mainActivity = (ActivityMain)context;
    }



    @Override
    protected void onPreExecute ()
    {
        super.onPreExecute();

        dialog = new ProgressDialog(mObjContext);
        dialog.setTitle("Please Wait");
        dialog.setMessage("Booking under process...");
        dialog.setCancelable(false);
        dialog.show();
        ProjectUtil.dialogColor(dialog);
    }





    @Override
    protected String doInBackground ( Void... params )
    {
        AppLogger.enable();
        AppLogger.e("Req ConfirmationTicketService ", mStrRequestXml);

        mStrResponse = SoapService.getInstance(mObjContext).getResults(mStrRequestXml,null,null,null,null,null,null, AES.decrypt(mObjContext.getResources().getString(R.string.NAMESPACE)), AES.decrypt(mObjContext.getResources().getString(R.string.FLIGHT_SEARCH_URL)), AES.decrypt(mObjContext.getResources().getString(R.string.BOOKING_CONFIRMATION_METHODNAME)));

        AppLogger.e("Res ConfirmationTicketService : ", mStrResponse);

        return "";
    }





    @Override
    protected void onPostExecute ( String result )
    {

        if (mStrResponse != null)
            {


            if (mStrResponse.contains("ServiceIssueSocketTimeOut"))
            {
                finishDialog();
                new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.SOCKET_TIME_OUT), mObjContext.getResources().getString(R.string.LOGIN_ERROR_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
            }
            else
            {
                // PARSE JSON DATA
                if (mStrResponse != null)
                {
                    JSONObject lObjJsonTrainList = SoapService.getInstance(mObjContext).getJSONObject(mStrResponse);
                    if (lObjJsonTrainList != null)
                    {
                        if (lObjJsonTrainList.has("ErrorMsg"))
                        {

                            try {
                                new AlertDialogUtil(mObjContext, lObjJsonTrainList.get("ErrorMsg").toString(), mObjContext.getResources().getString(R.string.payment_details), Constant.ALERT_ACTION_ONE).generateAlert();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        else
                        {

                            try {

                                // PARSE CONFIRM TICKET DATA
                                new ConfirmTicketParser(lObjJsonTrainList.toString(), mObjContext).ConfirmTicketParserResponseParser();
                                // Send user to next screen
                                mainActivity.lastActiveFragment = Constant.PAYMENT_WEBVIEW_FRAGMENT;
                                ProjectUtil.replaceFragment(mObjContext, new ConfirmTicketFragment(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

                            }catch (Exception e){
                                AppLogger.enable();
                                AppLogger.e("Login Excp :", String.valueOf(e));
                            }




                        }
                    }
                    else
                    {
                        new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.GENRIC_ERROR_MESSAGE), mObjContext.getResources().getString(R.string.payment_details), Constant.ALERT_ACTION_ONE).generateAlert();
                    }

                }
                else
                {
                    new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.GENRIC_ERROR_MESSAGE), mObjContext.getResources().getString(R.string.payment_details), Constant.ALERT_ACTION_ONE).generateAlert();
                }

            }

        }
        else
        {
            new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.GENRIC_ERROR_MESSAGE), mObjContext.getResources().getString(R.string.payment_details), Constant.ALERT_ACTION_ONE).generateAlert();
        }
        // Vivek : Progress Dialog was appreaing after network dialog
        // if( dialog != null)
        finishDialog();
    }





    // Finish Dialog
    public void finishDialog ()
    {

        try
        {
            if ((dialog != null) && dialog.isShowing())
            {
                dialog.dismiss();
            }
        }
        catch (final IllegalArgumentException e)
        {
            // Handle or log or ignore
        }
        catch (final Exception e)
        {
            // Handle or log or ignore
        }
        finally
        {
            dialog = null;
        }

    }
*/
}