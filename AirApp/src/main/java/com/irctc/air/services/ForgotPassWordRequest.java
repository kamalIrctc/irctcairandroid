package com.irctc.air.services;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.fragment.BookingHistoryFragment;
import com.irctc.air.util.AES;
import com.irctc.air.util.AlertDialogUtil;
import com.irctc.air.util.AppLogger;
import com.irctc.air.util.Constant;
import com.irctc.air.util.ProjectUtil;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by tourism on 5/15/2016.
 */
public class ForgotPassWordRequest{/* extends AsyncTask<Void, Void, String>
{
    private ProgressDialog dialog = null;
    private String mStrResponse;
    private ActivityMain mObjContext;
    private String mStrRequestXml;
    private ActivityMain mainActivity;

    public ForgotPassWordRequest(ActivityMain context, String requestXml){
        this.mObjContext = context;
        this.mStrRequestXml = requestXml;
        this.mainActivity = context;
    }


    @Override
    protected void onPreExecute ()
    {
        super.onPreExecute();

        dialog = new ProgressDialog(mObjContext);
        dialog.setTitle("Please Wait");
        dialog.setMessage("Generating password...");
        dialog.setCancelable(false);
        dialog.show();
        ProjectUtil.dialogColor(dialog);
    }

    @Override
    protected String doInBackground ( Void... params )
    {

        AppLogger.enable();
        AppLogger.e("inputXml Request : ", mStrRequestXml);

        mStrResponse = SoapService.getInstance(mObjContext).getResults(mStrRequestXml,null,null,null,null,null,null, AES.decrypt(mObjContext.getResources().getString(R.string.NAMESPACE)), AES.decrypt(mObjContext.getResources().getString(R.string.LOGIN_URL)), AES.decrypt(mObjContext.getResources().getString(R.string.FORGOT_PASSWORD_METHODNAME)));

        AppLogger.e("inputXml Response : ", mStrResponse);

        return "";
    }





    @Override
    protected void onPostExecute ( String result )
    {

        if (mStrResponse != null)

        {
            AppLogger.enable();;
            AppLogger.e("RESPONSE ::: ",mStrResponse);

            if (mStrResponse.contains("ServiceIssueSocketTimeOut"))
            {
                finishDialog();
                new AlertDialogUtil(mObjContext, mStrResponse, mObjContext.getResources().getString(R.string.FORGOT_PASS_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
            }
            else
            {
                // PARSE JSON DATA
                if (mStrResponse != null)
                {
                    JSONObject lObjJsonTrainList = SoapService.getInstance(mObjContext).getJSONObject(mStrResponse);
                    if (lObjJsonTrainList != null)
                    {
                        if (lObjJsonTrainList.has("ErrorMsg"))
                        {

                            // {"ErrorCode":"FlightSearch06","ErrorMsg":"Departure date is not valid"}
                            try {
                                new AlertDialogUtil(mainActivity, lObjJsonTrainList.get("ErrorMsg").toString() + mainActivity.getResources().getString(R.string.CANCEL_TICKET_ERROR_MESSAGE), mainActivity.getResources().getString(R.string.CANCEL_TICKET_TITLE), Constant.ALERT_ACTION_TWO, new BookingHistoryFragment()).generateAlert();
                               // new AlertDialogUtil(mObjContext, lObjJsonTrainList.get("ErrorMsg").toString(), mObjContext.getResources().getString(R.string.CANCEL_TICKET_TITLE), Constant.ALERT_ACTION_ONE).generateAlert();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        else
                        {








                        }
                    }
                    else
                    {
                        new AlertDialogUtil(mObjContext, mStrResponse, mObjContext.getResources().getString(R.string.CANCEL_TICKET_TITLE), Constant.ALERT_ACTION_ONE).generateAlert();
                    }
                }
                else
                {
                    new AlertDialogUtil(mObjContext, mStrResponse, mObjContext.getResources().getString(R.string.CANCEL_TICKET_TITLE), Constant.ALERT_ACTION_ONE).generateAlert();
                }
            }
        }
        else
        {
            new AlertDialogUtil(mObjContext, mStrResponse, mObjContext.getResources().getString(R.string.CANCEL_TICKET_TITLE), Constant.ALERT_ACTION_ONE).generateAlert();
        }
        // Vivek : Progress Dialog was appreaing after network dialog
        // if( dialog != null)
        finishDialog();
    }





    // Finish Dialog
    public void finishDialog ()
    {
        try
        {
            if ((dialog != null) && dialog.isShowing())
            {
                dialog.dismiss();
            }
        }
        catch (final IllegalArgumentException e)
        {
            // Handle or log or ignore
        }
        catch (final Exception e)
        {
            // Handle or log or ignore
        }
        finally
        {
            dialog = null;
        }
    }
*/
}