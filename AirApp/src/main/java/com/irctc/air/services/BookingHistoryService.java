package com.irctc.air.services;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;

import com.irctc.air.Database.AirDatabase;
import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.fragment.BookingHistoryFragment;
import com.irctc.air.model.HistoryBean;
import com.irctc.air.parser.DatabaseBookingHistoryParser;
import com.irctc.air.parser.ServerHistoryParser;
import com.irctc.air.util.AES;
import com.irctc.air.util.AlertDialogUtil;
import com.irctc.air.util.AppLogger;
import com.irctc.air.util.Constant;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.ProjectUtil;
import com.irctc.air.Database.SharedPrefrenceAir;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by vivek on 4/20/2016.
 */
public class BookingHistoryService{/* extends AsyncTask<Void, Void, String>
{
    private ProgressDialog dialog = null;
    private String mStrResponse;
    private Context mObjContext;
    private ActivityMain mainActivity;
    boolean isChangeData;


    public BookingHistoryService(Context context){
        this.mObjContext = context;
        this.mainActivity = (ActivityMain)context;
    }



    @Override
    protected void onPreExecute ()
    {
        super.onPreExecute();

        dialog = new ProgressDialog(mObjContext);
        dialog.setTitle("Please Wait");
        dialog.setMessage("Fetching History...");
        dialog.setCancelable(false);

        if( mainActivity.hideDialog == true){
            dialog.dismiss();
        }else{
            dialog.show();
        }
        ProjectUtil.dialogColor(dialog);
    }





    @Override
    protected String doInBackground ( Void... params )
    {
        String mStrRequestXml;

        // If user is logged in as guest user
        if( new SharedPrefrenceAir(mObjContext).getIsGuestUser()){

            // Call service with guest credentials
            mStrRequestXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><visitor><name>bvairguest</name><guestEmail>"+  AES.decrypt(new SharedPrefrenceAir(mObjContext).getGuestUserEmail()) + "</guestEmail><guestMobile>" + AES.decrypt(new SharedPrefrenceAir(mObjContext).getGuestUserMobile()) + "</guestMobile></visitor>";

        }
        // If user is logged in as normal user
        else{

            // Call service with user registered credentials
            mStrRequestXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><visitor><name>"+AES.decrypt(new SharedPrefrenceAir(mObjContext).getUserLogInId())+"</name><guestEmail></guestEmail><guestMobile></guestMobile></visitor>";
            // mStrRequestXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><visitor><name>"+new SharedPrefrenceAir(mObjContext).getUserLogInId()+"</name><password>"+new SharedPrefrenceAir(mObjContext).getUserLogInPwd()+"</password><guestEmail></guestEmail><guestMobile></guestMobile><serviceName>IndianRailways</serviceName><loginType>1</loginType></visitor>";
        }




        AppLogger.enable();
        AppLogger.e("Req Booking history ", mStrRequestXml);

        mStrResponse = SoapService.getInstance(mObjContext).getResults(mStrRequestXml,null,null,null,null,null,null, AES.decrypt(mObjContext.getResources().getString(R.string.NAMESPACE)), AES.decrypt(mObjContext.getResources().getString(R.string.LOGIN_URL)), AES.decrypt(mObjContext.getResources().getString(R.string.BOOKING_HISTORY_METHODNAME)));

        AppLogger.e("Res Booking history : ", mStrResponse);

         return "";
    }





    @Override
    protected void onPostExecute ( String result )
    {
        try {
            JSONArray jArray = new JSONArray(mStrResponse);
            JSONObject jObj = new JSONObject();
            jObj.putOpt("TEST1", jArray.get(0));
            jObj.putOpt("TEST2", jArray.get(1));
            mStrResponse = jObj.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        if (mStrResponse != null)
        {

            if (mStrResponse.contains("ServiceIssueSocketTimeOut"))
            {
                finishDialog();
                AirDatabase database = new AirDatabase(mObjContext);
                database.open();
                ArrayList<HistoryBean> historyList = database.getBookingHistory();
                database.close();
                if(historyList.size()==0){
                new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.SOCKET_TIME_OUT), mObjContext.getResources().getString(R.string.BOOKING_HISTORY_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                }
             }
            else
            {
                // PARSE JSON DATA
                if (mStrResponse != null)
                {
                    //JSONObject lObjJsonTrainList = SoapService.getInstance(mObjContext).getJSONObject(mStrResponse);

                    JSONObject lObjJsonTrainList = SoapService.getInstance(mObjContext).getJSONObject(mStrResponse);

                    if (lObjJsonTrainList != null)
                    {
                        if (lObjJsonTrainList.has("errorMsg") || lObjJsonTrainList.has("ErrorMsg"))
                        {

                            // {"ErrorCode": "UserAuthentication11","ErrorMsg": "Invalid sign-in ID or password." }

                            try {
                                if(lObjJsonTrainList.has("errorMsg")){
                                    new AlertDialogUtil(mObjContext, lObjJsonTrainList.get("errorMsg").toString(), mObjContext.getResources().getString(R.string.BOOKING_HISTORY_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                                }else{
                                    new AlertDialogUtil(mObjContext, lObjJsonTrainList.get("ErrorMsg").toString(), mObjContext.getResources().getString(R.string.BOOKING_HISTORY_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        else
                        {

//                          // Parse
//                          new BookingHistoryParser(lObjJsonTrainList.toString()).bookingHistoryResponseParser();
//
//                          // Send user to next screen
//                          mainActivity.lastActiveFragment = Constant.PLANNER_FRAGMENT;
//                          ProjectUtil.replaceFragment(mObjContext, new BookedHistoryFragment(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);


                            if( getAllBookedHistory().size() > 0 ){

                                new ServerHistoryParser(lObjJsonTrainList.toString()).bookingHistoryServerResponseParser();
                                ArrayList<HistoryBean> serverRes = AirDataHolder.getListHolder().getList().get(0).getServerBookedCancelHistory().getServerBookedCanceledHistory();
                                ArrayList<HistoryBean> databaseRes  = getAllBookedHistory();

                                if(serverRes.size() != databaseRes.size()){

                                    // 0. to give the popup for new information on BookedHistoryFragment
                                    isChangeData = true;
                                    // 1. clear the database
                                    clearDatabeHistory();
                                    // 2. insert the new server response
                                    insertHistoryInDatabase(serverRes);
                                    new DatabaseBookingHistoryParser(getAllBookedHistory()).databaseHistoryParser();
                                    showPopupDialog();

                                } else if(updateHistoryBasedOnCancelStatus(serverRes, databaseRes)) {

                                    // 0. to give the popup for new information on BookedHistoryFragment
                                    isChangeData = true;
                                    // 1. clear the database
                                    clearDatabeHistory();
                                    // 2. insert the new server response
                                    insertHistoryInDatabase(serverRes);
                                    new DatabaseBookingHistoryParser(getAllBookedHistory()).databaseHistoryParser();
                                    showPopupDialog();

                                } else {
                                    compareServerAndDatabseResponses(serverRes,databaseRes);

                                    if(isChangeData == true) {
                                        new DatabaseBookingHistoryParser(getAllBookedHistory()).databaseHistoryParser();
                                        showPopupDialog();
                                    }
                                }
                                mainActivity.hideDialog = false;

                            }else {
                                new ServerHistoryParser(lObjJsonTrainList.toString()).bookingHistoryServerResponseParser();
                                insertHistoryInDatabase(AirDataHolder.getListHolder().getList().get(0).getServerBookedCancelHistory().getServerBookedCanceledHistory());
                                new DatabaseBookingHistoryParser(getAllBookedHistory()).databaseHistoryParser();
                                ProjectUtil.replaceFragment(mObjContext, new BookingHistoryFragment(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                            }
                        }
                    }
                    else
                    {
                        new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.GENRIC_ERROR_MESSAGE), mObjContext.getResources().getString(R.string.BOOKING_HISTORY_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                    }

                }
                else
                {
                    new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.GENRIC_ERROR_MESSAGE), mObjContext.getResources().getString(R.string.BOOKING_HISTORY_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                }
            }
        }
        else
        {
            new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.GENRIC_ERROR_MESSAGE), mObjContext.getResources().getString(R.string.BOOKING_HISTORY_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
        }

        finishDialog();
    }




    // Finish Dialog
    public void finishDialog ()
    {

        try
        {
            if ((dialog != null) && dialog.isShowing())
            {
                dialog.dismiss();
            }
        }
        catch (final IllegalArgumentException e)
        {
            // Handle or log or ignore
        }
        catch (final Exception e)
        {
            // Handle or log or ignore
        }
        finally
        {
            dialog = null;
        }

    }

    private void insertHistoryInDatabase(ArrayList<HistoryBean> history) {

        //ArrayList<HistoryBean> history = AirDataHolder.getListHolder().getList().get(0).getServerBookedCancelHistory().getServerBookedCanceledHistory();
        AirDatabase database = new AirDatabase(mainActivity);
        database.open();
        database.insertBookingHistory(history);
        database.close();

    }

    private ArrayList<HistoryBean> getAllBookedHistory() {

        AirDatabase database = new AirDatabase(mainActivity);
        database.open();
        ArrayList<HistoryBean> historyList = database.getBookingHistory();
        database.close();
        return historyList;
    }

    private void clearDatabeHistory() {

        AirDatabase database = new AirDatabase(mainActivity);
        database.open();
        database.clearBookCancelHistory();
        database.close();
    }

    private void compareServerAndDatabseResponses(ArrayList<HistoryBean> serverRes, ArrayList<HistoryBean> databaseRes) {

        AirDatabase database = new AirDatabase(mainActivity);
        database.open();

        for (int i = 0; i < serverRes.size(); i++) {

           HistoryBean serverBean = serverRes.get(i);
           String transactionId =  serverBean.getTransactionId();
            HistoryBean databaseBean = database.selectSpecificBookCancelHistory(transactionId);

            if(!serverBean.getHistoryJson().equalsIgnoreCase(databaseBean.getHistoryJson())){

                isChangeData = true;
               // update the record in database
               boolean bool = database.updateRecord(transactionId, serverBean.getHistoryJson());

            }
        }
        database.close();
    }

    private void  showPopupDialog(){

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mainActivity);
        alertDialog.setTitle("Booking History");
        alertDialog.setMessage(R.string.UPDATE_HISTORY_MESSAGE);

        // Setting OK Button
        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                isChangeData = false;
                ProjectUtil.replaceFragment(mObjContext, new BookingHistoryFragment(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
            }
        });

//        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
//
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                isChangeData = false;
//                dialog.cancel();
//            }
//        });
        // Showing Alert Message
        ProjectUtil.dialogColorAlert(alertDialog);
    }

    private boolean updateHistoryBasedOnCancelStatus(ArrayList<HistoryBean> serverRes, ArrayList<HistoryBean> databaseRes){

        checkTicketIsCanceled(serverRes);
        checkTicketIsCanceled(databaseRes);

        if(checkTicketIsCanceled(serverRes) != checkTicketIsCanceled(databaseRes)){
            return true;
        }
        return false;
    }

    private int checkTicketIsCanceled(ArrayList<HistoryBean> lAList){

        int counter = 0;

        for (int i = 0; i < lAList.size(); i++) {

            HistoryBean historyBean = lAList.get(i);

            if(!historyBean.getHistoryType().equalsIgnoreCase("Booked")){
                counter++;
            }
        }

       return counter;
    }*/
}