package com.irctc.air.services;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.fragment.PaymentWebviewFragment;
import com.irctc.air.util.AES;
import com.irctc.air.util.AlertDialogUtil;
import com.irctc.air.util.AppLogger;
import com.irctc.air.util.Constant;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.ProjectUtil;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vivek on 4/28/2016.
 */
public class SubmitPassengerService{/* extends AsyncTask<Void, Void, String>
{
    private ProgressDialog dialog = null;
    private String mStrResponse;
    private Context mObjContext;
    private String mStrRequestXml1;
    private String mStrRequestXml2;
    private String mStrRequestXml3;
    private String mStrRequestXml4;
    private String mStrRequestXml5;
    private String mStrRequestXml6;
    private String mStrRequestXml7;
    private ActivityMain mainActivity;



    public SubmitPassengerService(Context context, String requestXml1, String requestXml2, String requestXml3,String requestXml4,String requestXml5, String requestXml6, String requestXml7){
        this.mObjContext = context;
        this.mStrRequestXml1 = requestXml1;
        this.mStrRequestXml2 = requestXml2;
        this.mStrRequestXml3 = requestXml3;
        this.mStrRequestXml4 = requestXml4;
        this.mStrRequestXml5 = requestXml5;
        this.mStrRequestXml6 = requestXml6;
        this.mStrRequestXml7 = requestXml7;
        this.mainActivity = (ActivityMain)context;
    }



    @Override
    protected void onPreExecute ()
    {
        super.onPreExecute();

        dialog = new ProgressDialog(mObjContext);
        dialog.setTitle("Please Wait");
        dialog.setMessage("Redirecting to payment screen...");
        dialog.setCancelable(false);
        dialog.show();
        ProjectUtil.dialogColor(dialog);
    }





    @Override
    protected String doInBackground ( Void... params )
    {

        AppLogger.enable();
        AppLogger.e("Req Add pass ", mStrRequestXml1);
        AppLogger.e("Req Add pass ", mStrRequestXml2);
        AppLogger.e("Req Add pass ", mStrRequestXml3);

          *//*
    * in0 ------input planner,
         * in1 ------ Onwardjson ,
         * in2 -----onward xml,
         * in3 ----- traveller xml,
         * in4 ------- security token ---
         * in5 ------- return json, ---
         * in6 --------- return xml.
     *//*
        mStrResponse = SoapService.getInstance(mObjContext).getResults(
                mStrRequestXml1,
                mStrRequestXml2,
                mStrRequestXml3,
                mStrRequestXml4,
                mStrRequestXml5,
                mStrRequestXml6,
                mStrRequestXml7, AES.decrypt(mObjContext.getResources().getString(R.string.NAMESPACE)), AES.decrypt(mObjContext.getResources().getString(R.string.FLIGHT_SEARCH_URL)), AES.decrypt(mObjContext.getResources().getString(R.string.ADD_PASS_METHODNAME)));

        AppLogger.e("Res Add pass : ", mStrResponse);

        return "";
    }





    @Override
    protected void onPostExecute ( String result )
    {

        if (mStrResponse != null)

        {

            if (mStrResponse.contains("ServiceIssueSocketTimeOut"))
            {
                finishDialog();
                new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.SOCKET_TIME_OUT), mObjContext.getResources().getString(R.string.ADD_PASS_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
            }
            else
            {
                // PARSE JSON DATA
                if (mStrResponse != null)
                {
                    JSONObject lObjJsonTrainList = SoapService.getInstance(mObjContext).getJSONObject(mStrResponse);
                    if (lObjJsonTrainList != null)
                    {
                        if (lObjJsonTrainList.has("ErrorMsg"))
                        {

                            try {
                                new AlertDialogUtil(mObjContext, lObjJsonTrainList.get("ErrorMsg").toString(), mObjContext.getResources().getString(R.string.ADD_PASS_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        else
                        {
                            // {"TransactionID":"5100001187"}

                                if(lObjJsonTrainList.has("TransactionID")){
                                    try {
                                        Fragment fragment = new PaymentWebviewFragment();
                                        Bundle bundle = new Bundle();
                                        bundle.putString("TransactionID", lObjJsonTrainList.getString("TransactionID"));
                                        fragment.setArguments(bundle);

                                        mainActivity.lastActiveFragment = Constant.ADD_PASS_FRAGMENT;
                                        ProjectUtil.replaceFragment(mObjContext, fragment, R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                                else{
                                    new AlertDialogUtil(mObjContext, mStrResponse, mObjContext.getResources().getString(R.string.ADD_PASS_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();


                                }


                        }
                    }
                    else
                    {
                        new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.GENRIC_ERROR_MESSAGE), mObjContext.getResources().getString(R.string.ADD_PASS_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                    }

                }
                else
                {
                    new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.GENRIC_ERROR_MESSAGE), mObjContext.getResources().getString(R.string.ADD_PASS_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                }

            }

        }
        else
        {
            new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.GENRIC_ERROR_MESSAGE), mObjContext.getResources().getString(R.string.ADD_PASS_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
        }
        // Vivek : Progress Dialog was appreaing after network dialog
        // if( dialog != null)
        finishDialog();
    }





    // Finish Dialog
    public void finishDialog ()
    {

        try
        {
            if ((dialog != null) && dialog.isShowing())
            {
                dialog.dismiss();
            }
        }
        catch (final IllegalArgumentException e)
        {
            // Handle or log or ignore
        }
        catch (final Exception e)
        {
            // Handle or log or ignore
        }
        finally
        {
            dialog = null;
        }

    }
*/
}