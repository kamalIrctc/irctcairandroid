package com.irctc.air.services;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.fragment.FragmentReprice;
import com.irctc.air.parser.OneWayFareQuoteParser;
import com.irctc.air.util.AES;
import com.irctc.air.util.AlertDialogUtil;
import com.irctc.air.util.AppLogger;
import com.irctc.air.util.Constant;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.ProjectUtil;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vivek on 4/20/2016.
 */
public class FareQuoteOneWayService{/* extends AsyncTask<Void, Void, String>
{
    private ProgressDialog dialog = null;
    private String mStrResponse;
    private Context mObjContext;
    private String jsonFlightString;
    private String jsonFlightReturnString;
    private String mStrRequestXml;
    private ActivityMain mainActivity;

    public FareQuoteOneWayService(Context context, String jsonFlightString, String requestXml, String jsonFlightReturnString){
        this.mObjContext = context;
        this.mStrRequestXml = requestXml;
        this.jsonFlightString = jsonFlightString;
        this.jsonFlightReturnString = jsonFlightReturnString;
        this.mainActivity = (ActivityMain)context;
    }



    @Override
    protected void onPreExecute ()
    {
        super.onPreExecute();

        dialog = new ProgressDialog(mObjContext);
        dialog.setTitle(mObjContext.getString(R.string.please_wait));
        dialog.setMessage(mObjContext.getString(R.string.reconfirming_avl));
        dialog.setCancelable(false);
        dialog.show();
        ProjectUtil.dialogColor(dialog);
    }

    @Override
    protected String doInBackground ( Void... params )
    {

        AppLogger.enable();
        AppLogger.e("Req Fare quote : ", mStrRequestXml);

        mStrResponse = SoapService.getInstance(mObjContext).getResults(mStrRequestXml,jsonFlightString,"",null,null,null,null, AES.decrypt(mObjContext.getResources().getString(R.string.NAMESPACE)), AES.decrypt(mObjContext.getResources().getString(R.string.FLIGHT_SEARCH_URL)), AES.decrypt(mObjContext.getResources().getString(R.string.FARE_QUOTE_METHODNAME)));

        AppLogger.e("Res Fare quote : ", mStrResponse);

        return "";
    }





    @Override
    protected void onPostExecute ( String result )
    {

        if (mStrResponse != null)

        {

            if (mStrResponse.contains("ServiceIssueSocketTimeOut"))
            {
                finishDialog();
                new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.SOCKET_TIME_OUT), mObjContext.getResources().getString(R.string.FARE_QUOTE_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
            }
            else
            {
                // PARSE JSON DATA
                if (mStrResponse != null)
                {
                    JSONObject lObjJsonTrainList = SoapService.getInstance(mObjContext).getJSONObject(mStrResponse);
                    if (lObjJsonTrainList != null)
                    {
                        if (lObjJsonTrainList.has("ErrorMsg"))
                        {
                            try {
                                new AlertDialogUtil(mObjContext, lObjJsonTrainList.get("ErrorMsg").toString(), mObjContext.getResources().getString(R.string.FARE_QUOTE_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        else
                        {
                           // {"flightSegmentJson":{"mf":0,"id":"1530S","tp":3756,"bp":2740,"total":3756,"stax":0,"astax":0,"cstax":0,"istax":0,"ttax":1016,"abp":2740,"cbp":0,"ibp":0,"pc":"0S","marktype":"true","classtype":"E","cabinType":"E","discount":0,"e_ticket":"E-TICKET","faretype":"public","disInFare":"0.0","jsellkey":"SG~ 153~ ~~DEL~11-23-2015 07:35~BOM~11-23-2015 09:45~","faresellkey":"0~E~~ESAVER~7701~~2~X","fareclass":"ESAVER","flight":[{"mac":"0S","oac":"0S","departureairport":"DEL","depart":"Delhi","arrivalairport":"BOM","arrive":"Mumbai","tkt":"Refundable","fno":"153","departuretime":"23-11-2015T07:35","arrivaltime":"23-11-2015T09:45","duration":"2h 10m","afc":"E","afbc":"E","afareBasis":"ESAVER","cfc":"E","cfbc":"E","ifc":"E","ifbc":"E","infantTicketType":"0","amf":0,"cmf":0,"imf":0,"cbp":0,"ibp":0,"atax":1016,"ctax":0,"itax":0,"abp":[2740,2740],"ataxdetails":[{"name":"TRF","value":50},{"name":"UDF","value":664},{"name":"GST","value":156},{"name":"PSF","value":146}],"ctaxdetails":[],"itaxdetails":[],"via":"Delhi to Mumbai","stops":0,"onwardorreturn":"onward"}]},"travellerXML":"<traveller><adult><counter>0<\/counter><sno>1<\/sno><title>passenger/Adult/Title<\/title><titleId>adult_title_0<\/titleId><age>passenger/Adult/Age<\/age><ageId>adult_age_0<\/ageId><fname>passenger/Adult/FName<\/fname><fnameId>adult_fname_0<\/fnameId><lname>passenger/Adult/LName<\/lname><lnameId>adult_lname_0<\/lnameId><tlnameId>adult_tlname_0<\/tlnameId><\/adult><\/traveller>"}
                                if(lObjJsonTrainList.has("flightSegmentJson"))
                                {

                                    // Parse
                                    new OneWayFareQuoteParser(lObjJsonTrainList.toString()).fareQuoteResponseParser();

                                    // Send user to next screen
                                    mainActivity.lastActiveFragment = Constant.ONEWAY_FLIGHT_FRAGMENT;

                                    mainActivity.FARE_QUOTE_BACK_HANDLER_VAR = Constant.ONEWAY_FLIGHT_FRAGMENT;

                                    ProjectUtil.replaceFragment(mObjContext, new DReprice(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

                                }else{
                                    new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.GENRIC_ERROR_MESSAGE), mObjContext.getResources().getString(R.string.FARE_QUOTE_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                                }

                        }
                    }
                    else
                    {
                        new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.GENRIC_ERROR_MESSAGE), mObjContext.getResources().getString(R.string.FARE_QUOTE_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                    }

                }
                else
                {
                    new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.GENRIC_ERROR_MESSAGE), mObjContext.getResources().getString(R.string.FARE_QUOTE_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                }

            }

        }
        else
        {
            new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.GENRIC_ERROR_MESSAGE), mObjContext.getResources().getString(R.string.FARE_QUOTE_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
        }

        finishDialog();
    }





    // Finish Dialog
    public void finishDialog ()
    {

        try
        {
            if ((dialog != null) && dialog.isShowing())
            {
                dialog.dismiss();
            }
        }
        catch (final IllegalArgumentException e)
        {
            // Handle or log or ignore
        }
        catch (final Exception e)
        {
            // Handle or log or ignore
        }
        finally
        {
            dialog = null;
        }

    }
*/
}