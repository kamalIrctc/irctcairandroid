package com.irctc.air.services;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class MyJobService extends JobService {
   private static final String TAG = MyJobService.class.getSimpleName();
   boolean isWorking = false;
   boolean jobCancelled = false;

   // Called by the Android system when it's time to run the job
   @Override
   public boolean onStartJob(JobParameters jobParameters) {
       Log.d(TAG, "Job started!");
       isWorking = true;
       // We need 'jobParameters' so we can call 'jobFinished'
       startWorkOnNewThread(jobParameters); // Services do NOT run on a separate thread

       return isWorking;
   }

   private void startWorkOnNewThread(final JobParameters jobParameters) {
       new Thread(new Runnable() {
           public void run() {
               doWork(jobParameters);
           }
       }).start();
   }

   private void doWork(JobParameters jobParameters) {
       // 10 seconds of working (1000*10ms)
       for (int i = 0; i < 1000; i++) {
           // If the job has been cancelled, stop working; the job will be rescheduled.
           if (jobCancelled)
               return;

           try { Thread.sleep(30); } catch (Exception e) { }
       }

       Log.d(TAG, "Job finished!");
       isWorking = false;
       boolean needsReschedule = false;
       if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
           jobFinished(jobParameters, needsReschedule);
       }
   }

   // Called if the job was cancelled before being finished
   @Override
   public boolean onStopJob(JobParameters jobParameters) {
       Log.d(TAG, "Job cancelled before being completed.");
       jobCancelled = true;
       boolean needsReschedule = isWorking;
       if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
           jobFinished(jobParameters, needsReschedule);
       }
       return needsReschedule;
   }
}