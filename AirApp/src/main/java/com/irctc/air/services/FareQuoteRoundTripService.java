package com.irctc.air.services;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.fragment.FragmentReprice;
import com.irctc.air.parser.RoundTripFareQuoteParser;
import com.irctc.air.util.AES;
import com.irctc.air.util.AlertDialogUtil;
import com.irctc.air.util.AppLogger;
import com.irctc.air.util.Constant;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.ProjectUtil;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by tourism on 5/11/2016.
 */
public class FareQuoteRoundTripService{/* extends AsyncTask<Void, Void, String> {

    private ProgressDialog dialog = null;
    private String mStrResponse;
    private Context mObjContext;
    private String mStrRequestXml;
    private String jsonOnwardFlightString;
    private String jsonReturnFlightString;
    private ActivityMain mainActivity;

    public FareQuoteRoundTripService(Context context, String jsonStrOnward, String jsonStrReturn, String requestXml){
        this.mObjContext = context;
        this.mStrRequestXml = requestXml;
        this.mainActivity = (ActivityMain) context;
        this.jsonOnwardFlightString = jsonStrOnward;
        this.jsonReturnFlightString = jsonStrReturn;
    }

    @Override
    protected void onPreExecute ()
    {
        super.onPreExecute();

        dialog = new ProgressDialog(mObjContext);
        dialog.setTitle("Please Wait");
        dialog.setMessage("Reconfirming availability...");
        dialog.setCancelable(false);
        dialog.show();
        ProjectUtil.dialogColor(dialog);
    }

    @Override
    protected String doInBackground ( Void... params )
    {

        AppLogger.enable();
        AppLogger.e("Req Fare quote : ", mStrRequestXml);

        mStrResponse = SoapService.getInstance(mObjContext).getResults(mStrRequestXml, jsonOnwardFlightString, jsonReturnFlightString,null , null, null, null, AES.decrypt(mObjContext.getResources().getString(R.string.NAMESPACE)), AES.decrypt(mObjContext.getResources().getString(R.string.FLIGHT_SEARCH_URL)), AES.decrypt(mObjContext.getResources().getString(R.string.FARE_QUOTE_METHODNAME)));

        AppLogger.e("Res Fare quote : ", mStrResponse);

        return "";
    }


    @Override
    protected void onPostExecute ( String result )
    {

        if (mStrResponse != null)

        {

            if (mStrResponse.contains("ServiceIssueSocketTimeOut"))
            {
                finishDialog();
                new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.SOCKET_TIME_OUT), mObjContext.getResources().getString(R.string.FARE_QUOTE_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
            }
            else
            {
                // PARSE JSON DATA
                if (mStrResponse != null)
                {
                    JSONObject lObjJsonTrainList = SoapService.getInstance(mObjContext).getJSONObject(mStrResponse);
                    if (lObjJsonTrainList != null)
                    {
                        if (lObjJsonTrainList.has("ErrorMsg"))
                        {
                            try {
                                new AlertDialogUtil(mObjContext, lObjJsonTrainList.get("ErrorMsg").toString(), mObjContext.getResources().getString(R.string.FARE_QUOTE_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        else
                        {

                            if(lObjJsonTrainList.has("flightSegmentJson"))
                            {

                                // Parse
                                new RoundTripFareQuoteParser(lObjJsonTrainList.toString()).fareQuoteResponseParser();

                                if(AirDataHolder.getListHolder().getList().get(0).getTripDomOrInter().equalsIgnoreCase("International")){

                                    mainActivity.lastActiveFragment = Constant.FRAGMENT_INTERNATION_ROUND_TRIP;
                                    mainActivity.FARE_QUOTE_BACK_HANDLER_VAR = Constant.FRAGMENT_INTERNATION_ROUND_TRIP;

                                }
                                else {
                                    mainActivity.lastActiveFragment = Constant.FRAGMENT_ROUND_TRIP;
                                    mainActivity.FARE_QUOTE_BACK_HANDLER_VAR = Constant.FRAGMENT_ROUND_TRIP;
                                }
                                // Send user to next screen
                                ProjectUtil.replaceFragment(mObjContext, new DReprice(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);

                            }else{
                                new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.GENRIC_ERROR_MESSAGE), mObjContext.getResources().getString(R.string.FARE_QUOTE_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                            }

                        }
                    }
                    else
                    {
                        new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.GENRIC_ERROR_MESSAGE), mObjContext.getResources().getString(R.string.FARE_QUOTE_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                    }

                }
                else
                {
                    new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.GENRIC_ERROR_MESSAGE), mObjContext.getResources().getString(R.string.FARE_QUOTE_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                }

            }
        }
        else
        {
            new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.GENRIC_ERROR_MESSAGE), mObjContext.getResources().getString(R.string.FARE_QUOTE_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
        }
        finishDialog();
    }





    // Finish Dialog
    public void finishDialog ()
    {
        try
        {
            if ((dialog != null) && dialog.isShowing())
            {
                dialog.dismiss();
            }
        }
        catch (final IllegalArgumentException e)
        {
            // Handle or log or ignore
        }
        catch (final Exception e)
        {
            // Handle or log or ignore
        }
        finally
        {
            dialog = null;
        }

    }
*/
}
