package com.irctc.air.services;

import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.irctc.air.Database.AirDatabase;
import com.irctc.air.model.search_airports.PojoAirportList;
import com.irctc.air.util.AssetsReader;

/**
 * Created by vivek on 4/20/2016.
 */
public class ServiceCreateAirportList extends IntentService {
    Context context;
    Gson gson;
    AirDatabase airDatabase;
    public ServiceCreateAirportList() {
        super(ServiceCreateAirportList.class.getName());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        context = getApplicationContext();
        gson = new Gson();
        airDatabase = new AirDatabase(context);
        if(airDatabase.isAirportDataExist()<3950){
        new Thread(new Runnable() {
            @Override
            public void run() {
                String airportsData = AssetsReader.loadJSONFromAsset(context);
                PojoAirportList pojoAirportList = gson.fromJson(airportsData,PojoAirportList.class);
                airDatabase.insertAirportData(pojoAirportList.getData());
            }
        }).start();}else{
            stopSelf();
        }
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

    }
}