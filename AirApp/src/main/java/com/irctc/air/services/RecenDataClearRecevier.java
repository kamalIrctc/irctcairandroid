package com.irctc.air.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.irctc.air.Database.AirDatabase;
import com.irctc.air.util.Pref;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.prefs.Preferences;

public class RecenDataClearRecevier extends BroadcastReceiver {
   private String TAG="RecenDataClearRecevier";

   @Override
   public void onReceive(Context context, Intent intent) {
       Log.i(TAG,"RecentDataClear receiver called..");
           clearData(context);
   }

    private void clearData(Context context) {
        AirDatabase airDatabase = new AirDatabase(context);
        airDatabase.removeRecentData();
        Pref.saveData(Pref.IS_RECENT_DATA_CLEAR, true);
    }
}