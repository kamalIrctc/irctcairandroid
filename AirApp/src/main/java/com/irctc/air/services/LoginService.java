package com.irctc.air.services;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.irctc.air.R;
import com.irctc.air.fragment.FragmentAddPassengers;
import com.irctc.air.fragment.FragmentPlanner;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.util.AES;
import com.irctc.air.util.AlertDialogUtil;
import com.irctc.air.util.AppLogger;
import com.irctc.air.util.Constant;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.ProjectUtil;
import com.irctc.air.Database.SharedPrefrenceAir;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vivek on 4/20/2016.
 */
public class LoginService{/* extends AsyncTask<Void, Void, String>
{
    private ProgressDialog dialog = null;
    private String mStrResponse;
    private Context mObjContext;
    private String mStrRequestXml;
    private ActivityMain mActivity;
    private int userType;


    public LoginService(Context context, String requestXml, int userType){
        this.mObjContext = context;
        this.mStrRequestXml = requestXml;
        this.mActivity = (ActivityMain)context;
        this.userType = userType;
    }



    @Override
    protected void onPreExecute ()
    {
        super.onPreExecute();

        dialog = new ProgressDialog(mObjContext);
        dialog.setTitle("Please Wait");
        if(new SharedPrefrenceAir(mActivity).getIsGuestUser()) {
            dialog.setMessage("Authenticating...");
        }else{
            dialog.setMessage("Processing...");
        }
        dialog.setCancelable(false);
        dialog.show();
        ProjectUtil.dialogColor(dialog);
    }





    @Override
    protected String doInBackground ( Void... params )
    {

        AppLogger.enable();
        AppLogger.e("Req Login ", mStrRequestXml);

        mStrResponse = SoapService.getInstance(mObjContext).getResults(mStrRequestXml,null,null,null,null,null,null,AES.decrypt(mObjContext.getResources().getString(R.string.NAMESPACE)), AES.decrypt(mObjContext.getResources().getString(R.string.LOGIN_URL)), AES.decrypt(mObjContext.getResources().getString(R.string.LOGIN_METHODNAME)));

        AppLogger.e("Res Login : ", mStrResponse);

        return "";
    }





    @Override
    protected void onPostExecute ( String result )
    {

        if (mStrResponse != null)
            {


            if (mStrResponse.contains("ServiceIssueSocketTimeOut"))
            {
                finishDialog();
                new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.SOCKET_TIME_OUT), mObjContext.getResources().getString(R.string.LOGIN_ERROR_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
            }
            else
            {
                // PARSE JSON DATA
                if (mStrResponse != null)
                {
                    JSONObject lObjJsonTrainList = SoapService.getInstance(mObjContext).getJSONObject(mStrResponse);
                    if (lObjJsonTrainList != null)
                    {
                        if (lObjJsonTrainList.has("ErrorMsg"))
                        {

                            try {
                                new AlertDialogUtil(mObjContext, lObjJsonTrainList.get("ErrorMsg").toString(), mObjContext.getResources().getString(R.string.LOGIN_ERROR_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        else
                        {

                            try {
                                // {"VisitorSuccess":"true","VisitorEmail":"vivekadhikari44@gmail.com","Visitorid":70004222,"VisitorProfile":"VIVEK ADHIKARI~VIVEK~ADHIKARI~IRCTC IRCA NEAR DRM~~NEW DELHI~DELHI~110055~INDIA~vivekadhikari44@gmail.com~8899889988~","screen":"travellerLogin"}

                                if (lObjJsonTrainList.has("VisitorSuccess") && lObjJsonTrainList.get("VisitorSuccess").toString().equalsIgnoreCase("true")) {
                                    // Set if guest is guest user
                                    if (userType == 1) {
                                        new SharedPrefrenceAir(mObjContext).setIsGuestUser(true);
                                    } else {
                                        new SharedPrefrenceAir(mObjContext).setIsGuestUser(false);
                                        // new SharedPrefrenceAir(mObjContext).SetUserLogInId(sasasasasasa);
                                    }

                                    // Save login status in AIR HOLDER
                                    new SharedPrefrenceAir(mObjContext).setIsLoggedIn(true);

                                    // Set user data in shared preference
                                    //VisitorProfile": "VIVEK ADHIKARI~
                                    // VIVEK~ADHIKARI~
                                    // IRCTC IRCA NEAR DRM~~
                                    // NEW DELHI~
                                    // DELHI~110055~
                                    // INDIA~
                                    // vivekadhikari44@gmail.com~
                                    // 8899889988~"

                                    new SharedPrefrenceAir(mObjContext).SetUserIdNumber(AES.encrypt(lObjJsonTrainList.optString("Visitorid")));


                                    if(lObjJsonTrainList.has("VisitorProfile")) {
                                        String userDetail[] = lObjJsonTrainList.optString("VisitorProfile").split("~");
                                        new SharedPrefrenceAir(mObjContext).SetUserName(AES.encrypt(userDetail[0]));
                                        new SharedPrefrenceAir(mObjContext).SetUserAddress(AES.encrypt(userDetail[3] + "\n" + userDetail[4] + "\n" + userDetail[5] + " " + userDetail[6] + "\n" + userDetail[7]));
                                        new SharedPrefrenceAir(mObjContext).SetGuestUserEmail(AES.encrypt(userDetail[9]));
                                        new SharedPrefrenceAir(mObjContext).SetGuestUserMobile(AES.encrypt(userDetail[10]));
                                    }

                                    if(mActivity.isComingFromFareQuote){
                                        mActivity.isComingFromFareQuote = false;
                                        // Send user to next screen
                                        mActivity.lastActiveFragment = Constant.FARE_QUOTE_FRAGMENT;
                                        ProjectUtil.replaceFragment(mObjContext, new FragmentAddPassengers(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                                    }else{
                                        // Send user to next screen
                                        mActivity.lastActiveFragment = Constant.PLANNER_FRAGMENT;
                                        ProjectUtil.replaceFragment(mObjContext, new FragmentPlanner(), R.id.frame_layout, EnumAnimation.NEITHER_LEFT_NOR_RIGHT);
                                    }

                                    ProjectUtil.setUserLogin(mObjContext);
                                }
                            }catch (Exception e){
                                AppLogger.enable();
                                AppLogger.e("Login Excp :", String.valueOf(e));
                            }




                        }
                    }
                    else
                    {
                        new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.GENRIC_ERROR_MESSAGE), mObjContext.getResources().getString(R.string.LOGIN_ERROR_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                    }

                }
                else
                {
                    new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.GENRIC_ERROR_MESSAGE), mObjContext.getResources().getString(R.string.LOGIN_ERROR_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                }

            }

        }
        else
        {
            new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.GENRIC_ERROR_MESSAGE), mObjContext.getResources().getString(R.string.LOGIN_ERROR_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
        }
        // Vivek : Progress Dialog was appreaing after network dialog
        // if( dialog != null)
        finishDialog();
    }





    // Finish Dialog
    public void finishDialog ()
    {

        try
        {
            if ((dialog != null) && dialog.isShowing())
            {
                dialog.dismiss();
            }
        }
        catch (final IllegalArgumentException e)
        {
            // Handle or log or ignore
        }
        catch (final Exception e)
        {
            // Handle or log or ignore
        }
        finally
        {
            dialog = null;
        }

    }
*/
}