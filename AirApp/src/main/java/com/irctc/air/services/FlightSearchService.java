package com.irctc.air.services;

import android.app.Dialog;
import android.os.AsyncTask;
import android.widget.TextView;

import com.irctc.air.Dataholder.AirDataHolder;
import com.irctc.air.R;
import com.irctc.air.activity.ActivityMain;
import com.irctc.air.fragment.FragmentInternationalRoundTrip;
import com.irctc.air.fragment.FragmentOneWayFlight;
import com.irctc.air.fragment.FragmentRoundTripDomestic;
import com.irctc.air.parser.FilghtSearchParser;
import com.irctc.air.util.AES;
import com.irctc.air.util.AlertDialogUtil;
import com.irctc.air.util.AppLogger;
import com.irctc.air.util.Constant;
import com.irctc.air.util.EnumAnimation;
import com.irctc.air.util.ProjectUtil;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vivek on 4/20/2016.
 */
public class FlightSearchService{/* extends AsyncTask<Void, Void, String>
{
    private Dialog dialog = null;
    private String mStrResponse;
    private ActivityMain mObjContext;
    private String mStrRequestXml;
    private String from,to;
        private ActivityMain mainActivity;
        public FlightSearchService(ActivityMain context, String requestXml, String from, String to, Dialog dialog){
                this.mObjContext = context;
                this.mStrRequestXml = requestXml;
                this.mainActivity = (ActivityMain)context;
                this.from=from;
                this.to=to;
                this.dialog=dialog;
        }


    @Override
    protected void onPreExecute ()
    {
        super.onPreExecute();
        TextView textViewFrom = (TextView) dialog.findViewById(R.id.textViewFrom);
        TextView textViewTo = (TextView) dialog.findViewById(R.id.textViewTo);
        textViewFrom.setText(from);
        textViewTo.setText(to);
        dialog.show();
        ProjectUtil.dialogColor(dialog);


    }





    @Override
    protected String doInBackground ( Void... params )
    {

        AppLogger.enable();
        AppLogger.e("inputXmlLoginRequest : ", mStrRequestXml);

        mStrResponse = SoapService.getInstance(mObjContext).getResults(mStrRequestXml,null,null,null,null,null,null, AES.decrypt(mObjContext.getResources().getString(R.string.NAMESPACE)), AES.decrypt(mObjContext.getResources().getString(R.string.FLIGHT_SEARCH_URL)), AES.decrypt(mObjContext.getResources().getString(R.string.FLIGHT_SEARCH_METHODNAME)));

        AppLogger.e("inputXmlLoginResponse : ", mStrResponse);

        return "";
    }





    @Override
    protected void onPostExecute ( String result )
    {

        if (mStrResponse != null)

        {
            AppLogger.enable();;
            AppLogger.e("RESPONSE ::: ",mStrResponse);

            if (mStrResponse.contains("ServiceIssueSocketTimeOut"))
            {
                finishDialog();
                new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.SOCKET_TIME_OUT), mObjContext.getResources().getString(R.string.FLIGHT_SEARCH_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
            }
            else
            {
                // PARSE JSON DATA
                if (mStrResponse != null)
                {
                    JSONObject lObjJsonTrainList = SoapService.getInstance(mObjContext).getJSONObject(mStrResponse);
                    if (lObjJsonTrainList != null)
                    {
                        if (lObjJsonTrainList.has("ErrorMsg"))
                        {

                            // {"ErrorCode":"FlightSearch06","ErrorMsg":"Departure date is not valid"}
                            try {
                                new AlertDialogUtil(mObjContext, lObjJsonTrainList.get("ErrorMsg").toString(), mObjContext.getResources().getString(R.string.FLIGHT_SEARCH_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        else
                        {
                           // new AlertDialogUtil(mObjContext, "SUCCESS RESPONSE", mObjContext.getResources().getString(R.string.FLIGHT_SEARCH_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();

                            new FilghtSearchParser(mainActivity,mStrResponse).flightSearchResponseParser();

                                if (mObjContext.isOneWaySelected) {
                                        // Check if holder got data else stay on planner page
                                        if(AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().size() > 0)
                                        {
                                                mainActivity.lastActiveFragment = Constant.PLANNER_FRAGMENT;
                                                ProjectUtil.replaceFragment(mObjContext, new FragmentOneWayFlight(), R.id.frame_layout, EnumAnimation.DOWN_TO_TOP);
                                        }else{
                                                new AlertDialogUtil(mObjContext, "No flight found.", mObjContext.getResources().getString(R.string.FLIGHT_SEARCH_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                                        }
                                } else {

                                    if(AirDataHolder.getListHolder().getList().get(0).getOnwInternationalFlightDetails().size() > 0 &&
                                            AirDataHolder.getListHolder().getList().get(0).getReturnAlAllFlightDetails().size() == 0 &&
                                            AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightGDS().size() == 0 &&
                                            AirDataHolder.getListHolder().getList().get(0).getRouOnwGdsLccBean().getFlightLCC().size() == 0 &&
                                            AirDataHolder.getListHolder().getList().get(0).getRouRetalAllFlightDetails().size() == 0 )
                                    {
                                        mainActivity.lastActiveFragment = Constant.PLANNER_FRAGMENT;
                                        ProjectUtil.replaceFragment(mObjContext, new FragmentInternationalRoundTrip(), R.id.frame_layout, EnumAnimation.DOWN_TO_TOP);
                                    }
                                    // Check if holder got data else stay on planner page
                                        else if(AirDataHolder.getListHolder().getList().get(0).getReturnAlAllFlightDetails().size() > 0 &&
                                                AirDataHolder.getListHolder().getList().get(0).getOnWalAllFlightDetails().size() > 0)
                                        {
                                                mainActivity.lastActiveFragment = Constant.PLANNER_FRAGMENT;
                                                ProjectUtil.replaceFragment(mObjContext, new FragmentRoundTripDomestic(), R.id.frame_layout, EnumAnimation.DOWN_TO_TOP);

                                        }
                                        else{
                                                new AlertDialogUtil(mObjContext, "No flight found.", mObjContext.getResources().getString(R.string.FLIGHT_SEARCH_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                                        }





                                }
                        }
                    }
                    else
                    {
                        new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.GENRIC_ERROR_MESSAGE), mObjContext.getResources().getString(R.string.FLIGHT_SEARCH_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                    }

                }
                else
                {
                    new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.GENRIC_ERROR_MESSAGE), mObjContext.getResources().getString(R.string.FLIGHT_SEARCH_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
                }

            }

        }
        else
        {
            new AlertDialogUtil(mObjContext, mObjContext.getResources().getString(R.string.GENRIC_ERROR_MESSAGE), mObjContext.getResources().getString(R.string.FLIGHT_SEARCH_ERROR_TITLE_TEXT), Constant.ALERT_ACTION_ONE).generateAlert();
        }
        // Vivek : Progress Dialog was appreaing after network dialog
        // if( dialog != null)
        finishDialog();
    }





    // Finish Dialog
    public void finishDialog ()
    {

        try
        {
            if ((dialog != null) && dialog.isShowing())
            {
                dialog.dismiss();
            }
        }
        catch (final IllegalArgumentException e)
        {
            // Handle or log or ignore
        }
        catch (final Exception e)
        {
            // Handle or log or ignore
        }
        finally
        {
            dialog = null;
        }

    }

*/}