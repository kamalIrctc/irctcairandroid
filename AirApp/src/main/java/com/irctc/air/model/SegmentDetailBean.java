package com.irctc.air.model;

/**
 * Created by vivek on 5/9/2016.
 */
public class SegmentDetailBean {

    private String airpnr;
    private String aircontact;
    private String airline;
    private String segdest;
    private String arriavlTime;
    private String pc;
    private String via;
    private String sppnr;
    private String cabinclass;
    private String oac;
    private String segorig;
    private String faretype;
    private String departTime;
    private String fno;
    private String duration;
    private int flighticon;


    private String segorigC;
    private String segdestC;

    public String getSegorigC() {
        return segorigC;
    }

    public void setSegorigC(String segorigC) {
        this.segorigC = segorigC;
    }

    public String getSegdestC() {
        return segdestC;
    }

    public void setSegdestC(String segdestC) {
        this.segdestC = segdestC;
    }

    public int getFlighticon() {
        return flighticon;
    }

    public void setFlighticon(int flighticon) {
        this.flighticon = flighticon;
    }

    public String getAirpnr() {
        return airpnr;
    }

    public void setAirpnr(String airpnr) {
        this.airpnr = airpnr;
    }

    public String getAircontact() {
        return aircontact;
    }

    public void setAircontact(String aircontact) {
        this.aircontact = aircontact;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getSegdest() {
        return segdest;
    }

    public void setSegdest(String segdest) {
        this.segdest = segdest;
    }

    public String getArriavlTime() {
        return arriavlTime;
    }

    public void setArriavlTime(String arriavlTime) {
        this.arriavlTime = arriavlTime;
    }

    public String getPc() {
        return pc;
    }

    public void setPc(String pc) {
        this.pc = pc;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    public String getSppnr() {
        return sppnr;
    }

    public void setSppnr(String sppnr) {
        this.sppnr = sppnr;
    }

    public String getCabinclass() {
        return cabinclass;
    }

    public void setCabinclass(String cabinclass) {
        this.cabinclass = cabinclass;
    }

    public String getOac() {
        return oac;
    }

    public void setOac(String oac) {
        this.oac = oac;
    }

    public String getSegorig() {
        return segorig;
    }

    public void setSegorig(String segorig) {
        this.segorig = segorig;
    }

    public String getFaretype() {
        return faretype;
    }

    public void setFaretype(String faretype) {
        this.faretype = faretype;
    }

    public String getDepartTime() {
        return departTime;
    }

    public void setDepartTime(String departTime) {
        this.departTime = departTime;
    }

    public String getFno() {
        return fno;
    }

    public void setFno(String fno) {
        this.fno = fno;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
