package com.irctc.air.model.book_ticket;

public class Data
{
    private String payment_URL;

    /*private null totalCost;

    private null error;
*/
    private String order_ID;

    private String cust_ID;

  /*  private null status;

    private null pnr;

    private null balDue;
*/
    private String callback_URL;

  //  private null flightNumber;

    private String app_CODE;

    private String txn_AMOUNT;
    private String txnType;
/*

    private null ticketNo;

    private null carrier;
*/

    public String getPayment_URL ()
    {
        return payment_URL;
    }

    public void setPayment_URL (String payment_URL)
    {
        this.payment_URL = payment_URL;
    }

/*
    public null getTotalCost ()
    {
        return totalCost;
    }

    public void setTotalCost (null totalCost)
    {
        this.totalCost = totalCost;
    }

    public null getError ()
    {
        return error;
    }

    public void setError (null error)
    {
        this.error = error;
    }
*/

    public String getOrder_ID ()
    {
        return order_ID;
    }

    public void setOrder_ID (String order_ID)
    {
        this.order_ID = order_ID;
    }

    public String getCust_ID ()
    {
        return cust_ID;
    }

    public void setCust_ID (String cust_ID)
    {
        this.cust_ID = cust_ID;
    }

/*
    public null getStatus ()
    {
        return status;
    }

    public void setStatus (null status)
    {
        this.status = status;
    }

    public null getPnr ()
    {
        return pnr;
    }

    public void setPnr (null pnr)
    {
        this.pnr = pnr;
    }

    public null getBalDue ()
    {
        return balDue;
    }

    public void setBalDue (null balDue)
    {
        this.balDue = balDue;
    }
*/

    public String getCallback_URL ()
    {
        return callback_URL;
    }

    public void setCallback_URL (String callback_URL)
    {
        this.callback_URL = callback_URL;
    }

/*
    public null getFlightNumber ()
    {
        return flightNumber;
    }

    public void setFlightNumber (null flightNumber)
    {
        this.flightNumber = flightNumber;
    }
*/

    public String getApp_CODE ()
    {
        return app_CODE;
    }

    public void setApp_CODE (String app_CODE)
    {
        this.app_CODE = app_CODE;
    }

    public String getTxn_AMOUNT ()
    {
        return txn_AMOUNT;
    }

    public void setTxn_AMOUNT (String txn_AMOUNT)
    {
        this.txn_AMOUNT = txn_AMOUNT;
    }

    public String getTxnType() {
        return txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    /*
    public null getTicketNo ()
    {
        return ticketNo;
    }

    public void setTicketNo (null ticketNo)
    {
        this.ticketNo = ticketNo;
    }

    public null getCarrier ()
    {
        return carrier;
    }

    public void setCarrier (null carrier)
    {
        this.carrier = carrier;
    }
*/

    @Override
    public String toString() {
        return "Data{" +
                "payment_URL='" + payment_URL + '\'' +
                ", order_ID='" + order_ID + '\'' +
                ", cust_ID='" + cust_ID + '\'' +
                ", callback_URL='" + callback_URL + '\'' +
                ", app_CODE='" + app_CODE + '\'' +
                ", txn_AMOUNT='" + txn_AMOUNT + '\'' +
                ", txnType='" + txnType + '\'' +
                '}';
    }
    /*
    @Override
    public String toString()
    {
        //totalCost = "+totalCost+", error = "+error+",
        //, status = "+status+"
        //, ticketNo = "+ticketNo+", carrier = "+carrier+"
        //, pnr = "+pnr+", balDue = "+balDue+"
        // flightNumber = "+flightNumber+",
        return "ClassPojo [payment_URL = "+payment_URL+",  order_ID = "+order_ID+", cust_ID = "+cust_ID+", callback_URL = "+callback_URL+", app_CODE = "+app_CODE+", txn_AMOUNT = "+txn_AMOUNT+"]";
    }*/
}

