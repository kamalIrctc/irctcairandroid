package com.irctc.air.model;

/**
 * Created by vivek on 4/26/2016.
 */
public class PassDetailbean {
    private String passTitle = ""; // added by asif
    private String passFirstName = ""; // added by asif
    private String passLastName = ""; // added by asif
    private String passAge = ""; // added by asif
    // Adult infant or child
    private String passType = ""; // added by asif

    private int passIndex = 0; // added by asif

    private boolean isValidation = false;


    // FOR INTERNATIONAL PERSON
    private String passPassportNum = ""; // added by asif
    private String passNationalty = ""; // added by asif
    private String passPassportIssueDate = ""; // added by asif
    private String passPassportValidUpto = ""; // added by asif


    private boolean isSelected = false;

    public int getPassIndex() {
        return passIndex;
    }

    public void setPassIndex(int passIndex) {
        this.passIndex = passIndex;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public String getPassTitle() {
        return passTitle;
    }

    public void setPassTitle(String passTitle) {
        this.passTitle = passTitle;
    }

    public String getPassFirstName() {
        return passFirstName;
    }

    public void setPassFirstName(String passFirstName) {
        this.passFirstName = passFirstName;
    }

    public String getPassLastName() {
        return passLastName;
    }

    public void setPassLastName(String passLastName) {
        this.passLastName = passLastName;
    }

    public String getPassAge() {
        return passAge;
    }

    public void setPassAge(String passAge) {
        this.passAge = passAge;
    }

    public String getPassType() {
        return passType;
    }

    public void setPassType(String passType) {
        this.passType = passType;
    }

    public String getPassPassportNum() {
        return passPassportNum;
    }

    public void setPassPassportNum(String passPassportNum) {
        this.passPassportNum = passPassportNum;
    }

    public String getPassNationalty() {
        return passNationalty;
    }

    public void setPassNationalty(String passNationalty) {
        this.passNationalty = passNationalty;
    }

    public String getPassPassportIssueDate() {
        return passPassportIssueDate;
    }

    public void setPassPassportIssueDate(String passPassportIssueDate) {
        this.passPassportIssueDate = passPassportIssueDate;
    }

    public String getPassPassportValidUpto() {
        return passPassportValidUpto;
    }

    public void setPassPassportValidUpto(String passPassportValidUpto) {
        this.passPassportValidUpto = passPassportValidUpto;
    }

    public boolean isValidation() {
        return isValidation;
    }

    public void setIsValidation(boolean isValidation) {
        this.isValidation = isValidation;
    }
}
