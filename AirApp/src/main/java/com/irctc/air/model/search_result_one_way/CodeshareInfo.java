package com.irctc.air.model.search_result_one_way;

public class CodeshareInfo
{
   // private null operatingFlightNumber;

   // private null operatingCarrier;

    private String value;

   /* public null getOperatingFlightNumber ()
    {
        return operatingFlightNumber;
    }

    public void setOperatingFlightNumber (null operatingFlightNumber)
    {
        this.operatingFlightNumber = operatingFlightNumber;
    }

    public null getOperatingCarrier ()
    {
        return operatingCarrier;
    }

    public void setOperatingCarrier (null operatingCarrier)
    {
        this.operatingCarrier = operatingCarrier;
    }
*/
    public String getValue ()
    {
        return value;
    }

    public void setValue (String value)
    {
        this.value = value;
    }

    @Override
    public String toString()
    {
        //operatingFlightNumber = "+operatingFlightNumber+", operatingCarrier = "+operatingCarrier+",
        return "ClassPojo [value = "+value+"]";
    }
}