package com.irctc.air.model;

import java.util.ArrayList;

/**
 * Created by tourism on 4/30/2016.
 */
public class FareQuoteOneWayBean {


    private String origin;
    private String destination;
    private int total = 0;  // total price = bp + ttax + stax
    private int ttax = 0;   // total tax
    private int stax = 0;   // service tax
    private int bp = 0;     // base fare
    private int abp = 0;

    /**
     * 28 Oct 2016
     * IRCTC SERVICE CHARGE
     */
    private double irctcBookingCharges = 0;            // IRCTC Booking charges: 	50
    private double irctcAgentServiceCgarges = 0; // Agent Service Tax: 	8

    private String flightAirline;
    private String e_ticket;
    private String faretype;

    ArrayList<FareQuoteInnerFlightDetailsBeans> flight = new ArrayList<>();




    /*********************************
     * Getter Setter
     *********************************/

    public String getFlightAirline() {
        return flightAirline;
    }

    public void setFlightAirline(String flightAirline) {
        this.flightAirline = flightAirline;
    }

    public int getStax() {
        return stax;
    }

    public void setStax(int stax) {
        this.stax = stax;
    }

    public int getBp() {
        return bp;
    }

    public void setBp(int bp) {
        this.bp = bp;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getTtax() {
        return ttax;
    }

    public void setTtax(int ttax) {
        this.ttax = ttax;
    }

    public int getAbp() {
        return abp;
    }

    public void setAbp(int abp) {
        this.abp = abp;
    }

    public String getE_ticket() {
        return e_ticket;
    }

    public void setE_ticket(String e_ticket) {
        this.e_ticket = e_ticket;
    }

    public String getFaretype() {
        return faretype;
    }

    public void setFaretype(String faretype) {
        this.faretype = faretype;
    }

    public ArrayList<FareQuoteInnerFlightDetailsBeans> getFlight() {
        return flight;
    }

    public void setFlight(ArrayList<FareQuoteInnerFlightDetailsBeans> flight) {
        this.flight = flight;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public double getIrctcBookingCharges() {
        return irctcBookingCharges;
    }

    public void setIrctcBookingCharges(double irctcBookingCharges) {
        this.irctcBookingCharges = irctcBookingCharges;
    }

    public double getIrctcAgentServiceCgarges() {
        return irctcAgentServiceCgarges;
    }

    public void setIrctcAgentServiceCgarges(double irctcAgentServiceCgarges) {
        this.irctcAgentServiceCgarges = irctcAgentServiceCgarges;
    }
}
