package com.irctc.air.model.search_result_round_trip;

public class Data
{
    private IntlFlights[][] intlFlights;

    //private null flights;

    private String searchKey;

    private String isInternational;

    public IntlFlights[][] getIntlFlights ()
    {
        return intlFlights;
    }

    public void setIntlFlights (IntlFlights[][] intlFlights)
    {
        this.intlFlights = intlFlights;
    }

    /*public null getFlights ()
    {
        return flights;
    }

    public void setFlights (null flights)
    {
        this.flights = flights;
    }
*/
    public String getSearchKey ()
    {
        return searchKey;
    }

    public void setSearchKey (String searchKey)
    {
        this.searchKey = searchKey;
    }

    public String getIsInternational ()
    {
        return isInternational;
    }

    public void setIsInternational (String isInternational)
    {
        this.isInternational = isInternational;
    }

  /*  public null getLstLowFareFlight ()
    {
        return lstLowFareFlight;
    }

    public void setLstLowFareFlight (null lstLowFareFlight)
    {
        this.lstLowFareFlight = lstLowFareFlight;
    }
*/
    @Override
    public String toString()
    {
        //, flights = "+flights+"
        //, lstLowFareFlight = "+lstLowFareFlight+"
        return "ClassPojo [intlFlights = "+intlFlights+", searchKey = "+searchKey+", isInternational = "+isInternational+"]";
    }
}