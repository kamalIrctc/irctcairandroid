package com.irctc.air.model.low_fare_calendar;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rajnikant Kumar on 12/19/2018.
 */

public class LowFareCalModel {
    private String status, message;
    private List<LowFareModel> data = new ArrayList<>();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<LowFareModel> getData() {
        return data;
    }

    public void setData(List<LowFareModel> data) {
        this.data = data;
    }

   public class LowFareModel implements Parcelable {
        private String carrierName, price, date, num;

        protected LowFareModel(Parcel in) {
            carrierName = in.readString();
            price = in.readString();
            date = in.readString();
            num = in.readString();
        }

        public final Creator<LowFareModel> CREATOR = new Creator<LowFareModel>() {
            @Override
            public LowFareModel createFromParcel(Parcel in) {
                return new LowFareModel(in);
            }

            @Override
            public LowFareModel[] newArray(int size) {
                return new LowFareModel[size];
            }
        };

        public String getCarrierName() {
            return carrierName;
        }

        public void setCarrierName(String carrierName) {
            this.carrierName = carrierName;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getNum() {
            return num;
        }

        public void setNum(String num) {
            this.num = num;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(carrierName);
            dest.writeString(price);
            dest.writeString(date);
            dest.writeString(num);
        }
    }

}
