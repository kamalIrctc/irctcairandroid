package com.irctc.air.model.gst;

/**
 * Created by Rajnikant Kumar on 11/4/2018.
 */

public class ModelGstDetails {
    public boolean isGstflag() {
        return gstflag;
    }

    public void setGstflag(boolean gstflag) {
        this.gstflag = gstflag;
    }

    public String getGstNumber() {
        return gstNumber;
    }

    public void setGstNumber(String gstNumber) {
        this.gstNumber = gstNumber;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    boolean gstflag;
    String gstNumber;
    String companyName;
    String emailid;
}
