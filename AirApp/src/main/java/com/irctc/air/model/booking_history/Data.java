package com.irctc.air.model.booking_history;

public class Data
{
    private String creationtime;

    private String onretround;

    private String segmentorigin;

    private String currentbookingstatus;

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public String getOrg() {
        return org;
    }

    public void setOrg(String org) {
        this.org = org;
    }

    private String departuretime;

    private String segmentdestination;

    private String txnid;

    private String transactionid;

    private String value,dest,org;

/*
    private null airlinepnrno;
*/

    public String getCreationtime ()
    {
        return creationtime;
    }

    public void setCreationtime (String creationtime)
    {
        this.creationtime = creationtime;
    }

    public String getOnretround ()
    {
        return onretround;
    }

    public void setOnretround (String onretround)
    {
        this.onretround = onretround;
    }

    public String getSegmentorigin ()
    {
        return segmentorigin;
    }

    public void setSegmentorigin (String segmentorigin)
    {
        this.segmentorigin = segmentorigin;
    }

    public String getCurrentbookingstatus ()
    {
        return currentbookingstatus;
    }

    public void setCurrentbookingstatus (String currentbookingstatus)
    {
        this.currentbookingstatus = currentbookingstatus;
    }

    public String getDeparturetime ()
    {
        return departuretime;
    }

    public void setDeparturetime (String departuretime)
    {
        this.departuretime = departuretime;
    }

    public String getSegmentdestination ()
    {
        return segmentdestination;
    }

    public void setSegmentdestination (String segmentdestination)
    {
        this.segmentdestination = segmentdestination;
    }

    public String getTxnid ()
    {
        return txnid;
    }

    public void setTxnid (String txnid)
    {
        this.txnid = txnid;
    }

    public String getTransactionid ()
    {
        return transactionid;
    }

    public void setTransactionid (String transactionid)
    {
        this.transactionid = transactionid;
    }

    public String getValue ()
    {
        return value;
    }

    public void setValue (String value)
    {
        this.value = value;
    }

/*
    public null getAirlinepnrno ()
    {
        return airlinepnrno;
    }

    public void setAirlinepnrno (null airlinepnrno)
    {
        this.airlinepnrno = airlinepnrno;
    }
*/

/*    @Override
    public String toString()
    {
        return "ClassPojo [creationtime = "+creationtime+", onretround = "+onretround+", segmentorigin = "+segmentorigin+", currentbookingstatus = "+currentbookingstatus+", departuretime = "+departuretime+", segmentdestination = "+segmentdestination+", txnid = "+txnid+", transactionid = "+transactionid+", value = "+value+", airlinepnrno = "+airlinepnrno+"]";
    }*/
}

			