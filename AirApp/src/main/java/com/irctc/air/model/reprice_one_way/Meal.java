package com.irctc.air.model.reprice_one_way;

public class Meal
{
    private String paxType;

    private String index;

    private String dest;

    private String price;

    private String deptDate;

    private String paxNum;

    private String origin;

    private String flightNumber;

    private String i;

    private String mealType;

    public String getPaxType ()
    {
        return paxType;
    }

    public void setPaxType (String paxType)
    {
        this.paxType = paxType;
    }

        public String getIndex ()
        {
            return index;
        }

        public void setIndex (String index)
        {
            this.index = index;
        }
    public String getDest ()
    {
        return dest;
    }

    public void setDest (String dest)
    {
        this.dest = dest;
    }

    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }

    public String getDeptDate ()
    {
        return deptDate;
    }

    public void setDeptDate (String deptDate)
    {
        this.deptDate = deptDate;
    }

    public String getPaxNum ()
    {
        return paxNum;
    }

    public void setPaxNum (String paxNum)
    {
        this.paxNum = paxNum;
    }
    public String getOrigin ()
    {
        return origin;
    }

    public void setOrigin (String origin)
    {
        this.origin = origin;
    }

    public String getFlightNumber ()
    {
        return flightNumber;
    }

    public void setFlightNumber (String flightNumber)
    {
        this.flightNumber = flightNumber;
    }

    public String getI ()
    {
        return i;
    }

    public void setI (String i)
    {
        this.i = i;
    }
    public String getMealType ()
    {
        return mealType;
    }

    public void setMealType (String mealType)
    {
        this.mealType = mealType;
    }

    @Override
    public String toString()
    {
        //paxType = "+paxType+",
        //index = "+index+",
        //, paxNum = "+paxNum+"
        //, i = "+i+"
        return "ClassPojo [dest = "+dest+", price = "+price+", deptDate = "+deptDate+", origin = "+origin+", flightNumber = "+flightNumber+", mealType = "+mealType+"]";
    }
}

			