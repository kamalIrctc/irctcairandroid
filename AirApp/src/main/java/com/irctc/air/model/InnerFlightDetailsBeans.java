package com.irctc.air.model;

/**
 * Created by tourism on 4/22/2016.
 */
public class InnerFlightDetailsBeans {

    private String flightArrivalTime;
    private String flightDepartureTime;
    private String flightDurationTime;
    private String flightNo;
    int flightStops;


    // Added by vivek for flight detail screen
    private String oac;
    private int flightIcon;
    private String departureairport;
    private String depart;
    private String arrivalairport;
    private String arrive;
    private String tkt;
    private String infantTicketType;
    private String via;
    private String onwardorreturn;


    private String flightOnwRetType;


    public String getFlightOnwRetType() {
        return flightOnwRetType;
    }

    public void setFlightOnwRetType(String flightOnwRetType) {
        this.flightOnwRetType = flightOnwRetType;
    }

    public int getFlightStops() {
        return flightStops;
    }

    public void setFlightStops(int flightStops) {
        this.flightStops = flightStops;
    }

    public String getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }

    public String getFlightArrivalTime() {
        return flightArrivalTime;
    }

    public void setFlightArrivalTime(String flightArrivalTime) {
        this.flightArrivalTime = flightArrivalTime;
    }

    public String getFlightDepartureTime() {
        return flightDepartureTime;
    }

    public void setFlightDepartureTime(String flightDepartureTime) {
        this.flightDepartureTime = flightDepartureTime;
    }

    public String getFlightDurationTime() {
        return flightDurationTime;
    }

    public void setFlightDurationTime(String flightDurationTime) {
        this.flightDurationTime = flightDurationTime;
    }



    // Added by vivek for flight detail screen


    public String getOac() {
        return oac;
    }

    public void setOac(String oac) {
        this.oac = oac;
    }

    public int getFlightIcon() {
        return flightIcon;
    }

    public void setFlightIcon(int flightIcon) {
        this.flightIcon = flightIcon;
    }

    public String getDepartureairport() {
        return departureairport;
    }

    public void setDepartureairport(String departureairport) {
        this.departureairport = departureairport;
    }

    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public String getArrivalairport() {
        return arrivalairport;
    }

    public void setArrivalairport(String arrivalairport) {
        this.arrivalairport = arrivalairport;
    }

    public String getArrive() {
        return arrive;
    }

    public void setArrive(String arrive) {
        this.arrive = arrive;
    }

    public String getTkt() {
        return tkt;
    }

    public void setTkt(String tkt) {
        this.tkt = tkt;
    }

    public String getInfantTicketType() {
        return infantTicketType;
    }

    public void setInfantTicketType(String infantTicketType) {
        this.infantTicketType = infantTicketType;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    public String getOnwardorreturn() {
        return onwardorreturn;
    }

    public void setOnwardorreturn(String onwardorreturn) {
        this.onwardorreturn = onwardorreturn;
    }
}


