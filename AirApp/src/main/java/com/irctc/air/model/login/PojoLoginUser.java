package com.irctc.air.model.login;

public class PojoLoginUser
{
    private String message;

    private String status;

    private String data;

    private UserDetails userDetails;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getData ()
    {
        return data;
    }

    public void setData (String data)
    {
        this.data = data;
    }

    public UserDetails getUserDetails ()
    {
        return userDetails;
    }

    public void setUserDetails (UserDetails userDetails)
    {
        this.userDetails = userDetails;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", status = "+status+", data = "+data+", userDetails = "+userDetails+"]";
    }
}
