package com.irctc.air.model;

/**
 * Created by tourism on 5/8/2016.
 */
public class SpecialOfferAirlineBean {

    private String flightName;
    private String flightCode;
    private String flightSpecialFare;


    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

    public String getFlightCode() {
        return flightCode;
    }

    public void setFlightCode(String flightCode) {
        this.flightCode = flightCode;
    }

    public String getFlightSpecialFare() {
        return flightSpecialFare;
    }

    public void setFlightSpecialFare(String flightSpecialFare) {
        this.flightSpecialFare = flightSpecialFare;
    }
}
