package com.irctc.air.model;

import java.util.ArrayList;

/**
 * Created by vivek on 4/26/2016.
 */
public class BookingHistoryBean {
    // TICKET DETAIL //
    private String brf;
    private String origin;
    private String orig;
    private String destination;
    private String dest;
    private String tripType;
    private String noofpass;
    private String noofseg;
    private String trnId;
    private String bookingstatus;
    private String bookingstatusValue;
    private String bookingdate;
    private String bookeradd1;
    private String bookeradd2;
    private String bookercity;
    private String bookerstate;
    private String bookercountry;
    private String bookerpin;
    private String bookerphone;
    private String tktoid;
    private String segtypecan;
    private String tranoid;
    private String totalCharge;
    private String origCharge;
    private String ltcEmpCode;
    private String noOfAdult;
    private String noOfChild;
    private String noOfInfant;

    // FARE DETAIL ARRAYLIST
    private ArrayList<FareDetailBean> alFareDetail = new ArrayList<>();

    // BOOKED HISTORY  ONWARD SEGMENT DETAIL ARRAYLIST
    private ArrayList<BookedHistorySegmentAndPassBean> alBookedOnwardDetail = new ArrayList<>();

    // BOOKED HISTORY  RETURN SEGMENT DETAIL ARRAYLIST
    private ArrayList<BookedHistorySegmentAndPassBean> alBookedReturnDetail = new ArrayList<>();

    /* GETTER SETTER */

    public String getBrf() {
        return brf;
    }

    public void setBrf(String brf) {
        this.brf = brf;
    }

    public String getOrigin() {
        return origin;
    }

    public String getOrig() {
        return orig;
    }

    public void setOrig(String orig) {
        this.orig = orig;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }
    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getNoofpass() {
        return noofpass;
    }

    public void setNoofpass(String noofpass) {
        this.noofpass = noofpass;
    }

    public String getNoofseg() {
        return noofseg;
    }

    public void setNoofseg(String noofseg) {
        this.noofseg = noofseg;
    }

    public String getTrnId() {
        return trnId;
    }

    public void setTrnId(String trnId) {
        this.trnId = trnId;
    }

    public String getBookingstatus() {
        return bookingstatus;
    }

    public void setBookingstatus(String bookingstatus) {
        this.bookingstatus = bookingstatus;
    }

    public String getBookingstatusValue() {
        return bookingstatusValue;
    }

    public void setBookingstatusValue(String bookingstatusValue) {
        this.bookingstatusValue = bookingstatusValue;
    }

    public String getBookingdate() {
        return bookingdate;
    }

    public void setBookingdate(String bookingdate) {
        this.bookingdate = bookingdate;
    }

    public String getBookeradd1() {
        return bookeradd1;
    }

    public void setBookeradd1(String bookeradd1) {
        this.bookeradd1 = bookeradd1;
    }

    public String getBookeradd2() {
        return bookeradd2;
    }

    public void setBookeradd2(String bookeradd2) {
        this.bookeradd2 = bookeradd2;
    }

    public String getBookercity() {
        return bookercity;
    }

    public void setBookercity(String bookercity) {
        this.bookercity = bookercity;
    }

    public String getBookerstate() {
        return bookerstate;
    }

    public void setBookerstate(String bookerstate) {
        this.bookerstate = bookerstate;
    }

    public String getBookercountry() {
        return bookercountry;
    }

    public void setBookercountry(String bookercountry) {
        this.bookercountry = bookercountry;
    }

    public String getBookerpin() {
        return bookerpin;
    }

    public void setBookerpin(String bookerpin) {
        this.bookerpin = bookerpin;
    }

    public String getBookerphone() {
        return bookerphone;
    }

    public void setBookerphone(String bookerphone) {
        this.bookerphone = bookerphone;
    }

    public String getTktoid() {
        return tktoid;
    }

    public void setTktoid(String tktoid) {
        this.tktoid = tktoid;
    }

    public String getSegtypecan() {
        return segtypecan;
    }

    public void setSegtypecan(String segtypecan) {
        this.segtypecan = segtypecan;
    }

    public String getTranoid() {
        return tranoid;
    }

    public void setTranoid(String tranoid) {
        this.tranoid = tranoid;
    }

    public String getTotalCharge() {
        return totalCharge;
    }

    public void setTotalCharge(String totalCharge) {
        this.totalCharge = totalCharge;
    }

    public String getOrigCharge() {
        return origCharge;
    }

    public void setOrigCharge(String origCharge) {
        this.origCharge = origCharge;
    }

    public String getLtcEmpCode() {
        return ltcEmpCode;
    }

    public void setLtcEmpCode(String ltcEmpCode) {
        this.ltcEmpCode = ltcEmpCode;
    }

    public String getNoOfAdult() {
        return noOfAdult;
    }

    public void setNoOfAdult(String noOfAdult) {
        this.noOfAdult = noOfAdult;
    }

    public String getNoOfChild() {
        return noOfChild;
    }

    public void setNoOfChild(String noOfChild) {
        this.noOfChild = noOfChild;
    }

    public String getNoOfInfant() {
        return noOfInfant;
    }

    public void setNoOfInfant(String noOfInfant) {
        this.noOfInfant = noOfInfant;
    }

    public ArrayList<FareDetailBean> getAlFareDetail() {
        return alFareDetail;
    }

    public void setAlFareDetail(FareDetailBean alFareDetail) {
        this.alFareDetail.add(alFareDetail);
    }

    public ArrayList<BookedHistorySegmentAndPassBean> getAlBookedOnwardDetail() {
        return alBookedOnwardDetail;
    }

    public void setAlBookedOnwardDetail(BookedHistorySegmentAndPassBean alBookedOnwardDetail) {
        this.alBookedOnwardDetail.add(alBookedOnwardDetail);
    }

    public ArrayList<BookedHistorySegmentAndPassBean> getAlBookedReturnDetail() {
        return alBookedReturnDetail;
    }

    public void setAlBookedReturnDetail(BookedHistorySegmentAndPassBean alBookedReturnDetail) {
        this.alBookedReturnDetail.add(alBookedReturnDetail);
    }

}
