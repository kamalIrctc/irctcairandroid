package com.irctc.air.model;

/**
 * Created by Rajnikant Kumar on 11/7/2018.
 */

public class ModelCancelTicket {
  String transId,pnr,paxNo,segNo,oid,airlineCode, paxType, ticketNo, tarvelClass;

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public String getPaxNo() {
        return paxNo;
    }

    public void setPaxNo(String paxNo) {
        this.paxNo = paxNo;
    }

    public String getSegNo() {
        return segNo;
    }

    public void setSegNo(String segNo) {
        this.segNo = segNo;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public String getPaxType() {
        return paxType;
    }

    public void setPaxType(String paxType) {
        this.paxType = paxType;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public String getTarvelClass() {
        return tarvelClass;
    }

    public void setTarvelClass(String tarvelClass) {
        this.tarvelClass = tarvelClass;
    }
}
