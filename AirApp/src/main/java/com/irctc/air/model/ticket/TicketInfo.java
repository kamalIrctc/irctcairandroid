package com.irctc.air.model.ticket;

public class TicketInfo
{
    private String destinationCity;

    private String airlineName;

    private boolean isFreeMeal;
    private boolean isBaggageAllowed;

    public String getOriginAirport() {
        return originAirport;
    }

    public void setOriginAirport(String originAirport) {
        this.originAirport = originAirport;
    }

    public String getDestinationAirport() {
        return destinationAirport;
    }

    public void setDestinationAirport(String destinationAirport) {
        this.destinationAirport = destinationAirport;
    }

    private String originAirport;

    private String origin;

    private String departureTime;

    private String destinationAirport;

    private String pnr;

    private String flightNumber;

    private String destination;

    private Passengers[] passengers;

    private String duration;

    private String segmentType;

    private String arrivalTime;

    private String tarvelClass;

    private String originCity;

    private String airline;

    private String departureTimeAnd;
    private String arrivalTimeAnd;

    public String getSegmentType() {
        return segmentType;
    }

    public void setSegmentType(String segmentType) {
        this.segmentType = segmentType;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
    }

    public String getOriginCity() {
        return originCity;
    }

    public void setOriginCity(String originCity) {
        this.originCity = originCity;
    }


    public Passengers[] getPassengers ()
    {
        return passengers;
    }

    public void setPassengers (Passengers[] passengers)
    {
        this.passengers = passengers;
    }

    public String getDuration ()
    {
        return duration;
    }

    public void setDuration (String duration)
    {
        this.duration = duration;
    }

    public String getAirlineName ()
    {
        return airlineName;
    }

    public void setAirlineName (String airlineName)
    {
        this.airlineName = airlineName;
    }

    public String getArrivalTime ()
    {
        return arrivalTime;
    }

    public void setArrivalTime (String arrivalTime)
    {
        this.arrivalTime = arrivalTime;
    }

    public String getDepartureTime ()
    {
        return departureTime;
    }

    public void setDepartureTime (String departureTime)
    {
        this.departureTime = departureTime;
    }

    public String getOrigin ()
    {
        return origin;
    }

    public void setOrigin (String origin)
    {
        this.origin = origin;
    }

    public String getPnr ()
    {
        return pnr;
    }

    public void setPnr (String pnr)
    {
        this.pnr = pnr;
    }

    public String getFlightNumber ()
    {
        return flightNumber;
    }

    public void setFlightNumber (String flightNumber)
    {
        this.flightNumber = flightNumber;
    }

    public String getAirline ()
    {
        return airline;
    }

    public void setAirline (String airline)
    {
        this.airline = airline;
    }

    public String getDestination ()
    {
        return destination;
    }

    public void setDestination (String destination)
    {
        this.destination = destination;
    }

    public String getTarvelClass() {
        return tarvelClass;
    }

    public void setTarvelClass(String tarvelClass) {
        this.tarvelClass = tarvelClass;
    }

    public String getDepartureTimeAnd() {
        return departureTimeAnd;
    }

    public void setDepartureTimeAnd(String departureTimeAnd) {
        this.departureTimeAnd = departureTimeAnd;
    }

    public String getArrivalTimeAnd() {
        return arrivalTimeAnd;
    }

    public void setArrivalTimeAnd(String arrivalTimeAnd) {
        this.arrivalTimeAnd = arrivalTimeAnd;
    }

    public boolean isFreeMeal() {
        return isFreeMeal;
    }

    public void setFreeMeal(boolean freeMeal) {
        isFreeMeal = freeMeal;
    }

    public boolean isBaggageAllowed() {
        return isBaggageAllowed;
    }

    public void setBaggageAllowed(boolean baggageAllowed) {
        isBaggageAllowed = baggageAllowed;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [passengers = "+passengers+", duration = "+duration+", airlineName = "+airlineName+", arrivalTime = "+arrivalTime+", departureTime = "+departureTime+", origin = "+origin+", pnr = "+pnr+", flightNumber = "+flightNumber+", airline = "+airline+", destination = "+destination+", tarvelClass = "+tarvelClass+", arrivalTimeAnd = "+arrivalTimeAnd+", departureTimeAnd = "+departureTimeAnd+"]";
    }
}

		