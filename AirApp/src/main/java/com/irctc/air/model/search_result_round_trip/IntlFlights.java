package com.irctc.air.model.search_result_round_trip;

import android.text.TextUtils;

import com.irctc.air.round.trip.domastic.model.LstBaggageDetails;

public class IntlFlights
{
    private LstFareDetails[] lstFareDetails;

    //private null segments;
    private boolean isFreeMeal;
    private boolean isBaggageAllowed;
    private LstBaggageDetails[] lstBaggageDetails;
    private String serviceProvider;

    private String departureDate;

    private String departureCityWithCode;

    private String departureTime;

    private String cacheKeyIntl;

    private String flightNumber;

    private String arrivalCityWithCode;

    private String stops;

    private String cacheKey;

    private AdditionalTypeBaseAirSegment[] additionalTypeBaseAirSegment;

    private String duration;

    private String arrivalTime;

    private String price;

    private String carrierName;

    private String carrier;

    private String arrivalDate;

    private String segmentType;

    private LstFlightDetails[] lstFlightDetails;

    //private null serviceProvider;

    private String key;

    public LstFareDetails[] getLstFareDetails ()
    {
        return lstFareDetails;
    }

    public void setLstFareDetails (LstFareDetails[] lstFareDetails)
    {
        this.lstFareDetails = lstFareDetails;
    }

    /*public null getSegments ()
    {
        return segments;
    }

    public void setSegments (null segments)
    {
        this.segments = segments;
    }*/

    public boolean isBaggageAllowed() {
        return isBaggageAllowed;
    }

    public void setBaggageAllowed(boolean baggageAllowed) {
        isBaggageAllowed = baggageAllowed;
    }

    public String getDepartureDate ()
    {
        return departureDate;
    }

    public void setDepartureDate (String departureDate)
    {
        this.departureDate = departureDate;
    }

    public String getDepartureCityWithCode ()
    {
        return departureCityWithCode;
    }

    public void setDepartureCityWithCode (String departureCityWithCode)
    {
        this.departureCityWithCode = departureCityWithCode;
    }

    public String getDepartureTime ()
    {
        return departureTime;
    }

    public void setDepartureTime (String departureTime)
    {
        this.departureTime = departureTime;
    }

    public String getCacheKeyIntl ()
    {
        return cacheKeyIntl;
    }

    public void setCacheKeyIntl (String cacheKeyIntl)
    {
        this.cacheKeyIntl = cacheKeyIntl;
    }

    public String getFlightNumber ()
    {
        return flightNumber;
    }

    public void setFlightNumber (String flightNumber)
    {
        this.flightNumber = flightNumber;
    }

    public String getArrivalCityWithCode ()
    {
        return arrivalCityWithCode;
    }

    public void setArrivalCityWithCode (String arrivalCityWithCode)
    {
        this.arrivalCityWithCode = arrivalCityWithCode;
    }

    public String getStops ()
    {
        return stops;
    }

    public void setStops (String stops)
    {
        this.stops = stops;
    }

    public String getCacheKey ()
    {
        return cacheKey;
    }

    public void setCacheKey (String cacheKey)
    {
        this.cacheKey = cacheKey;
    }

    public AdditionalTypeBaseAirSegment[] getAdditionalTypeBaseAirSegment ()
    {
        return additionalTypeBaseAirSegment;
    }

    public void setAdditionalTypeBaseAirSegment (AdditionalTypeBaseAirSegment[] additionalTypeBaseAirSegment)
    {
        this.additionalTypeBaseAirSegment = additionalTypeBaseAirSegment;
    }

    public String getDuration ()
    {
        return duration;
    }

    public void setDuration (String duration)
    {
        this.duration = duration;
    }

    public String getArrivalTime ()
    {
        return arrivalTime;
    }

    public void setArrivalTime (String arrivalTime)
    {
        this.arrivalTime = arrivalTime;
    }

    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }

    public String getCarrierName ()
    {
        return carrierName;
    }

    public void setCarrierName (String carrierName)
    {
        this.carrierName = carrierName;
    }

    public String getCarrier ()
    {
        return carrier;
    }

    public void setCarrier (String carrier)
    {
        this.carrier = carrier;
    }

    public String getArrivalDate ()
    {
        return arrivalDate;
    }

    public void setArrivalDate (String arrivalDate)
    {
        this.arrivalDate = arrivalDate;
    }

    public String getSegmentType ()
    {
        return segmentType;
    }

    public void setSegmentType (String segmentType)
    {
        this.segmentType = segmentType;
    }

    public LstFlightDetails[] getLstFlightDetails ()
    {
        return lstFlightDetails;
    }

    public void setLstFlightDetails (LstFlightDetails[] lstFlightDetails)
    {
        this.lstFlightDetails = lstFlightDetails;
    }

   /* public null getServiceProvider ()
    {
        return serviceProvider;
    }

    public void setServiceProvider (null serviceProvider)
    {
        this.serviceProvider = serviceProvider;
    }*/

    public boolean isFreeMeal() {
        return isFreeMeal;
    }

    public void setFreeMeal(boolean freeMeal) {
        isFreeMeal = freeMeal;
    }

    public String getKey ()
    {
        return key;
    }

    public void setKey (String key)
    {
        this.key = key;
    }

    public String getServiceProvider() {
        if (!TextUtils.isEmpty(serviceProvider)){
            return serviceProvider;
        }
        return "";

    }

    public void setServiceProvider(String serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public LstBaggageDetails[] getLstBaggageDetails() {
        return lstBaggageDetails;
    }

    public void setLstBaggageDetails(LstBaggageDetails[] lstBaggageDetails) {
        this.lstBaggageDetails = lstBaggageDetails;
    }

    @Override
    public String toString()
    {
        //, segments = "+segments+", isBaggageAllowed = "+isBaggageAllowed+"
        //, serviceProvider = "+serviceProvider+"
        return "ClassPojo [lstFareDetails = "+lstFareDetails+", departureDate = "+departureDate+", departureCityWithCode = "+departureCityWithCode+", departureTime = "+departureTime+", cacheKeyIntl = "+cacheKeyIntl+", flightNumber = "+flightNumber+", arrivalCityWithCode = "+arrivalCityWithCode+", stops = "+stops+", cacheKey = "+cacheKey+", additionalTypeBaseAirSegment = "+additionalTypeBaseAirSegment+", duration = "+duration+", arrivalTime = "+arrivalTime+", price = "+price+", carrierName = "+carrierName+", carrier = "+carrier+", arrivalDate = "+arrivalDate+", segmentType = "+segmentType+", lstFlightDetails = "+lstFlightDetails+", key = "+key+"]";
    }
}

			