package com.irctc.air.model.login;

public class UserInfo
{
    private String country;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    /* private null postoffice;

    private null street;

    private null panNumber;*/

    private String pinCode;

   /* private null employerName;*/

    private String phoneNumber;

 /*   private null motherMaidenName;*/

    private String userInfoId;

    private String userId;

    private String gender;

  /*  private null otherCountryName;*/

   /* private null preferredMailingFlag;

    private null offOtherCountryName;

    private null offPostoffice;*/

    private String middleName;

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    private String district;

    private String lastName;

   /* private null occupation;

    private null offPhoneNumber;

    private null offStreet;

    private null answer;

    private null signupSource;

    private null colony;

    private null promMailSmsOption;

    private null idCardType;

    private null offFax;

    private null softMemberid;
*/
    private String dateOfBirth;

   /* private null offOtherStateName;

    private null questionId;

    private null deptDesignation;*/

    private String otherStateName;

   /* private null offState;

    private null moblienoChangeDate;

    private null profileUpdateTime;*/

    private String maritalStatus;

   /* private null signupDate;

    private null faxNumber;

    private null oldLiveTxnExist;

    private null migrationDate;

    private null offOtherCityName;*/

    private String travelName;

    private String firstName;

    private String address;

    private String stateName;

    private String otherCityName;

    public String getPinCode ()
    {
        return pinCode;
    }

    public void setPinCode (String pinCode)
    {
        this.pinCode = pinCode;
    }

    public String getPhoneNumber ()
    {
        return phoneNumber;
    }

    public void setPhoneNumber (String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getUserInfoId ()
    {
        return userInfoId;
    }

    public void setUserInfoId (String userInfoId)
    {
        this.userInfoId = userInfoId;
    }

    public String getUserId ()
    {
        return userId;
    }

    public void setUserId (String userId)
    {
        this.userId = userId;
    }

    public String getGender ()
    {
        return gender;
    }

    public void setGender (String gender)
    {
        this.gender = gender;
    }

    public String getMiddleName ()
    {
        return middleName;
    }

    public void setMiddleName (String middleName)
    {
        this.middleName = middleName;
    }

    public String getLastName ()
    {
        return lastName;
    }

    public void setLastName (String lastName)
    {
        this.lastName = lastName;
    }

    public String getDateOfBirth ()
    {
        return dateOfBirth;
    }

    public void setDateOfBirth (String dateOfBirth)
    {
        this.dateOfBirth = dateOfBirth;
    }

    public String getOtherStateName ()
    {
        return otherStateName;
    }

    public void setOtherStateName (String otherStateName)
    {
        this.otherStateName = otherStateName;
    }

    public String getMaritalStatus ()
    {
        return maritalStatus;
    }

    public void setMaritalStatus (String maritalStatus)
    {
        this.maritalStatus = maritalStatus;
    }

    public String getTravelName ()
    {
        return travelName;
    }

    public void setTravelName (String travelName)
    {
        this.travelName = travelName;
    }

    public String getFirstName ()
    {
        return firstName;
    }

    public void setFirstName (String firstName)
    {
        this.firstName = firstName;
    }

    public String getAddress ()
    {
        return address;
    }

    public void setAddress (String address)
    {
        this.address = address;
    }

    public String getStateName ()
    {
        return stateName;
    }

    public void setStateName (String stateName)
    {
        this.stateName = stateName;
    }

    public String getOtherCityName ()
    {
        return otherCityName;
    }

    public void setOtherCityName (String otherCityName)
    {
        this.otherCityName = otherCityName;
    }

}

			