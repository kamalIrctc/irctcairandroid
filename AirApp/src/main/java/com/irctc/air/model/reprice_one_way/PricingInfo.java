package com.irctc.air.model.reprice_one_way;

/**
 * Created by Rajnikant Kumar on 9/5/2018.
 */

public class PricingInfo {
    private String paxType;

    private String total;

    private String refundable;

    private String fareBasis;

    private String origin;

    private String carrier;

    private TaxDetails taxDetails;

    private String taxes;

    private String basePrice;

    private String destination;

    public String getPaxType ()
    {
        return paxType;
    }

    public void setPaxType (String paxType)
    {
        this.paxType = paxType;
    }

    public String getTotal ()
    {
        return total;
    }

    public void setTotal (String total)
    {
        this.total = total;
    }

    public String getRefundable ()
    {
        return refundable;
    }

    public void setRefundable (String refundable)
    {
        this.refundable = refundable;
    }

    public String getFareBasis ()
    {
        return fareBasis;
    }

    public void setFareBasis (String fareBasis)
    {
        this.fareBasis = fareBasis;
    }

    public String getOrigin ()
    {
        return origin;
    }

    public void setOrigin (String origin)
    {
        this.origin = origin;
    }

    public String getCarrier ()
    {
        return carrier;
    }

    public void setCarrier (String carrier)
    {
        this.carrier = carrier;
    }

    public TaxDetails getTaxDetails ()
    {
        return taxDetails;
    }

    public void setTaxDetails (TaxDetails taxDetails)
    {
        this.taxDetails = taxDetails;
    }

    public String getTaxes ()
    {
        return taxes;
    }

    public void setTaxes (String taxes)
    {
        this.taxes = taxes;
    }

    public String getBasePrice ()
    {
        return basePrice;
    }

    public void setBasePrice (String basePrice)
    {
        this.basePrice = basePrice;
    }

    public String getDestination ()
    {
        return destination;
    }

    public void setDestination (String destination)
    {
        this.destination = destination;
    }

}
