package com.irctc.air.model.ticket;

public class Passengers
{
    private String lastName;

    private String ticketNo;

    private String status;

    private String oid;

    private String firstName;

    private String segmentNo;

    private String paxNo;

    public String getPaxType() {
        return paxType;
    }

    public void setPaxType(String paxType) {
        this.paxType = paxType;
    }

    private String paxType;

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLastName ()
    {
        return lastName;
    }

    public void setLastName (String lastName)
    {
        this.lastName = lastName;
    }

    public String getTicketNo ()
    {
        return ticketNo;
    }

    public void setTicketNo (String ticketNo)
    {
        this.ticketNo = ticketNo;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getOid ()
    {
        return oid;
    }

    public void setOid (String oid)
    {
        this.oid = oid;
    }

    public String getFirstName ()
    {
        return firstName;
    }

    public void setFirstName (String firstName)
    {
        this.firstName = firstName;
    }

    public String getSegmentNo ()
    {
        return segmentNo;
    }

    public void setSegmentNo (String segmentNo)
    {
        this.segmentNo = segmentNo;
    }

    public String getPaxNo ()
    {
        return paxNo;
    }

    public void setPaxNo (String paxNo)
    {
        this.paxNo = paxNo;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [lastName = "+lastName+", ticketNo = "+ticketNo+", status = "+status+", oid = "+oid+", firstName = "+firstName+", segmentNo = "+segmentNo+", paxNo = "+paxNo+"]";
    }
}

		