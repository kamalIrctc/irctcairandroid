package com.irctc.air.model.ticket;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IrFlightsBook {
    @SerializedName("oid")
    @Expose
    private Integer oid;
    @SerializedName("airUserAlias")
    @Expose
    private String airUserAlias;
    @SerializedName("bookerAddress1")
    @Expose
    private String bookerAddress1;
    @SerializedName("bookerAddress2")
    @Expose
    private Object bookerAddress2;
    @SerializedName("bookerCity")
    @Expose
    private String bookerCity;
    @SerializedName("bookerCountry")
    @Expose
    private String bookerCountry;
    @SerializedName("bookerEmail")
    @Expose
    private String bookerEmail;
    @SerializedName("bookerName")
    @Expose
    private String bookerName;
    @SerializedName("bookerPhone")
    @Expose
    private String bookerPhone;
    @SerializedName("bookerPincode")
    @Expose
    private String bookerPincode;
    @SerializedName("bookerState")
    @Expose
    private String bookerState;
    @SerializedName("bookingStatus")
    @Expose
    private Integer bookingStatus;
    @SerializedName("creationTime")
    @Expose
    private long creationTime;
    @SerializedName("deleted")
    @Expose
    private Integer deleted;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("errorMessage")
    @Expose
    private Object errorMessage;
    @SerializedName("guestEmailId")
    @Expose
    private Object guestEmailId;
    @SerializedName("guestMobileNo")
    @Expose
    private Object guestMobileNo;
    @SerializedName("irFlBookingRefNo")
    @Expose
    private String irFlBookingRefNo;
    @SerializedName("lastModTime")
    @Expose
    private long lastModTime;
    @SerializedName("ltcEmpCode")
    @Expose
    private Object ltcEmpCode;
    @SerializedName("noOfChild")
    @Expose
    private Object noOfChild;
    @SerializedName("noOfInfant")
    @Expose
    private Object noOfInfant;
    @SerializedName("noOfPassengers")
    @Expose
    private Integer noOfPassengers;
    @SerializedName("noOfSegments")
    @Expose
    private Integer noOfSegments;
    @SerializedName("origin")
    @Expose
    private String origin;
    @SerializedName("paymentGatewayId")
    @Expose
    private Object paymentGatewayId;
    @SerializedName("segmentTypeCan")
    @Expose
    private Object segmentTypeCan;
    @SerializedName("serviceProvider")
    @Expose
    private Integer serviceProvider;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("storeId")
    @Expose
    private Integer storeId;
    @SerializedName("transactionId")
    @Expose
    private String transactionId;
    @SerializedName("transactionIdIrctc")
    @Expose
    private Object transactionIdIrctc;
    @SerializedName("tripType")
    @Expose
    private Integer tripType;
    @SerializedName("userId")
    @Expose
    private String userId;

    public Integer getOid() {
        return oid;
    }

    public void setOid(Integer oid) {
        this.oid = oid;
    }

    public String getAirUserAlias() {
        return airUserAlias;
    }

    public void setAirUserAlias(String airUserAlias) {
        this.airUserAlias = airUserAlias;
    }

    public String getBookerAddress1() {
        return bookerAddress1;
    }

    public void setBookerAddress1(String bookerAddress1) {
        this.bookerAddress1 = bookerAddress1;
    }

    public Object getBookerAddress2() {
        return bookerAddress2;
    }

    public void setBookerAddress2(Object bookerAddress2) {
        this.bookerAddress2 = bookerAddress2;
    }

    public String getBookerCity() {
        return bookerCity;
    }

    public void setBookerCity(String bookerCity) {
        this.bookerCity = bookerCity;
    }

    public String getBookerCountry() {
        return bookerCountry;
    }

    public void setBookerCountry(String bookerCountry) {
        this.bookerCountry = bookerCountry;
    }

    public String getBookerEmail() {
        return bookerEmail;
    }

    public void setBookerEmail(String bookerEmail) {
        this.bookerEmail = bookerEmail;
    }

    public String getBookerName() {
        return bookerName;
    }

    public void setBookerName(String bookerName) {
        this.bookerName = bookerName;
    }

    public String getBookerPhone() {
        return bookerPhone;
    }

    public void setBookerPhone(String bookerPhone) {
        this.bookerPhone = bookerPhone;
    }

    public String getBookerPincode() {
        return bookerPincode;
    }

    public void setBookerPincode(String bookerPincode) {
        this.bookerPincode = bookerPincode;
    }

    public String getBookerState() {
        return bookerState;
    }

    public void setBookerState(String bookerState) {
        this.bookerState = bookerState;
    }

    public Integer getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(Integer bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public Long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Integer creationTime) {
        this.creationTime = creationTime;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Object getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(Object errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Object getGuestEmailId() {
        return guestEmailId;
    }

    public void setGuestEmailId(Object guestEmailId) {
        this.guestEmailId = guestEmailId;
    }

    public Object getGuestMobileNo() {
        return guestMobileNo;
    }

    public void setGuestMobileNo(Object guestMobileNo) {
        this.guestMobileNo = guestMobileNo;
    }

    public String getIrFlBookingRefNo() {
        return irFlBookingRefNo;
    }

    public void setIrFlBookingRefNo(String irFlBookingRefNo) {
        this.irFlBookingRefNo = irFlBookingRefNo;
    }

    public Long getLastModTime() {
        return lastModTime;
    }

    public void setLastModTime(Integer lastModTime) {
        this.lastModTime = lastModTime;
    }

    public Object getLtcEmpCode() {
        return ltcEmpCode;
    }

    public void setLtcEmpCode(Object ltcEmpCode) {
        this.ltcEmpCode = ltcEmpCode;
    }

    public Object getNoOfChild() {
        return noOfChild;
    }

    public void setNoOfChild(Object noOfChild) {
        this.noOfChild = noOfChild;
    }

    public Object getNoOfInfant() {
        return noOfInfant;
    }

    public void setNoOfInfant(Object noOfInfant) {
        this.noOfInfant = noOfInfant;
    }

    public Integer getNoOfPassengers() {
        return noOfPassengers;
    }

    public void setNoOfPassengers(Integer noOfPassengers) {
        this.noOfPassengers = noOfPassengers;
    }

    public Integer getNoOfSegments() {
        return noOfSegments;
    }

    public void setNoOfSegments(Integer noOfSegments) {
        this.noOfSegments = noOfSegments;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public Object getPaymentGatewayId() {
        return paymentGatewayId;
    }

    public void setPaymentGatewayId(Object paymentGatewayId) {
        this.paymentGatewayId = paymentGatewayId;
    }

    public Object getSegmentTypeCan() {
        return segmentTypeCan;
    }

    public void setSegmentTypeCan(Object segmentTypeCan) {
        this.segmentTypeCan = segmentTypeCan;
    }

    public Integer getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(Integer serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Object getTransactionIdIrctc() {
        return transactionIdIrctc;
    }

    public void setTransactionIdIrctc(Object transactionIdIrctc) {
        this.transactionIdIrctc = transactionIdIrctc;
    }

    public Integer getTripType() {
        return tripType;
    }

    public void setTripType(Integer tripType) {
        this.tripType = tripType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

//    @Override
//    public String toString()
//    {
//        return "ClassPojo [tripType = "+tripType+"]";
//    }
}
