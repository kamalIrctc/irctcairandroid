package com.irctc.air.model;

import java.util.ArrayList;

/**
 * Created by tourism on 4/22/2016.
 */
public class OneWayFlightsDetailsBeans {

    private int mf;
    private String id;
    private int tp;
    private int bp;
    private int total;
    private int stax;
    private int astax;
    private int cstax;
    private int istax;
    private int ttax;
    private int abp;
    private int cbp;
    private int ibp;
    private String pc;
    private String marktype;
    private String classtype;
    private String cabinType;
    private int discount;
    private String e_ticket;
    private String faretype;
    private String disInFare;
    private String jsellkey;
    private String faresellkey;
    private String fareclass;
    // private ArrayList<FlightBean> flight;

    public int getMf() {
        return mf;
    }

    public void setMf(int mf) {
        this.mf = mf;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getTp() {
        return tp;
    }

    public void setTp(int tp) {
        this.tp = tp;
    }

    public int getBp() {
        return bp;
    }

    public void setBp(int bp) {
        this.bp = bp;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getStax() {
        return stax;
    }

    public void setStax(int stax) {
        this.stax = stax;
    }

    public int getAstax() {
        return astax;
    }

    public void setAstax(int astax) {
        this.astax = astax;
    }

    public int getCstax() {
        return cstax;
    }

    public void setCstax(int cstax) {
        this.cstax = cstax;
    }

    public int getIstax() {
        return istax;
    }

    public void setIstax(int istax) {
        this.istax = istax;
    }

    public int getTtax() {
        return ttax;
    }

    public void setTtax(int ttax) {
        this.ttax = ttax;
    }

    public int getAbp() {
        return abp;
    }

    public void setAbp(int abp) {
        this.abp = abp;
    }

    public int getCbp() {
        return cbp;
    }

    public void setCbp(int cbp) {
        this.cbp = cbp;
    }

    public int getIbp() {
        return ibp;
    }

    public void setIbp(int ibp) {
        this.ibp = ibp;
    }

    public String getPc() {
        return pc;
    }

    public void setPc(String pc) {
        this.pc = pc;
    }

    public String getMarktype() {
        return marktype;
    }

    public void setMarktype(String marktype) {
        this.marktype = marktype;
    }

    public String getClasstype() {
        return classtype;
    }

    public void setClasstype(String classtype) {
        this.classtype = classtype;
    }

    public String getCabinType() {
        return cabinType;
    }

    public void setCabinType(String cabinType) {
        this.cabinType = cabinType;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public String getE_ticket() {
        return e_ticket;
    }

    public void setE_ticket(String e_ticket) {
        this.e_ticket = e_ticket;
    }

    public String getFaretype() {
        return faretype;
    }

    public void setFaretype(String faretype) {
        this.faretype = faretype;
    }

    public String getDisInFare() {
        return disInFare;
    }

    public void setDisInFare(String disInFare) {
        this.disInFare = disInFare;
    }

    public String getJsellkey() {
        return jsellkey;
    }

    public void setJsellkey(String jsellkey) {
        this.jsellkey = jsellkey;
    }

    public String getFaresellkey() {
        return faresellkey;
    }

    public void setFaresellkey(String faresellkey) {
        this.faresellkey = faresellkey;
    }

    public String getFareclass() {
        return fareclass;
    }

    public void setFareclass(String fareclass) {
        this.fareclass = fareclass;
    }

//    public ArrayList<FlightBean> getFlight() {
//        return flight;
//    }
//
//    public void setFlight(ArrayList<FlightBean> flight) {
//        this.flight = flight;
//    }



}
