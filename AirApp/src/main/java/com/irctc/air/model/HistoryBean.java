package com.irctc.air.model;

/**
 * Created by tourism on 6/22/2016.
 */
public class HistoryBean {

    private String transactionId;
    private String bookingStatusValue;
    private String DepartureDate;
    private String ArrivalDate;
    private String historyType;
    private String historyJson;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getBookingStatusValue() {
        return bookingStatusValue;
    }

    public void setBookingStatusValue(String bookingStatusValue) {
        this.bookingStatusValue = bookingStatusValue;
    }

    public String getDepartureDate() {
        return DepartureDate;
    }

    public void setDepartureDate(String departureDate) {
        DepartureDate = departureDate;
    }

    public String getArrivalDate() {
        return ArrivalDate;
    }

    public void setArrivalDate(String arricalDate) {
        ArrivalDate = arricalDate;
    }

    public String getHistoryType() {
        return historyType;
    }

    public void setHistoryType(String historyType) {
        this.historyType = historyType;
    }

    public String getHistoryJson() {
        return historyJson;
    }

    public void setHistoryJson(String historyJson) {
        this.historyJson = historyJson;
    }
}
