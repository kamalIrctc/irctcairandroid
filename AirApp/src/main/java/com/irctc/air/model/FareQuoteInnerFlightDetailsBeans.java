package com.irctc.air.model;

/**
 * Created by tourism on 4/30/2016.
 */
public class FareQuoteInnerFlightDetailsBeans {



    private String oac;
    private int flightIcon;
    private String departureairport;
    private String depart;
    private String arrivalairport;
    private String arrive;
    private String tkt;
    private String fno;
    private String departuretime;
    private String arrivaltime;
    private String duration;
    private String infantTicketType;
    private String via;
    private String stops;
    private String onwardorreturn;


    public String getOac() {
        return oac;
    }

    public void setOac(String oac) {
        this.oac = oac;
    }

    public int getFlightIcon() {
        return flightIcon;
    }

    public void setFlightIcon(int flightIcon) {
        this.flightIcon = flightIcon;
    }

    public String getDepartureairport() {
        return departureairport;
    }

    public void setDepartureairport(String departureairport) {
        this.departureairport = departureairport;
    }

    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public String getArrivalairport() {
        return arrivalairport;
    }

    public void setArrivalairport(String arrivalairport) {
        this.arrivalairport = arrivalairport;
    }

    public String getArrive() {
        return arrive;
    }

    public void setArrive(String arrive) {
        this.arrive = arrive;
    }

    public String getTkt() {
        return tkt;
    }

    public void setTkt(String tkt) {
        this.tkt = tkt;
    }

    public String getFno() {
        return fno;
    }

    public void setFno(String fno) {
        this.fno = fno;
    }

    public String getDeparturetime() {
        return departuretime;
    }

    public void setDeparturetime(String departuretime) {
        this.departuretime = departuretime;
    }

    public String getArrivaltime() {
        return arrivaltime;
    }

    public void setArrivaltime(String arrivaltime) {
        this.arrivaltime = arrivaltime;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getInfantTicketType() {
        return infantTicketType;
    }

    public void setInfantTicketType(String infantTicketType) {
        this.infantTicketType = infantTicketType;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }

    public String getStops() {
        return stops;
    }

    public void setStops(String stops) {
        this.stops = stops;
    }

    public String getOnwardorreturn() {
        return onwardorreturn;
    }

    public void setOnwardorreturn(String onwardorreturn) {
        this.onwardorreturn = onwardorreturn;
    }
}
