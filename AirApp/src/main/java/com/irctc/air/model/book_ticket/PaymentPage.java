package com.irctc.air.model.book_ticket;

/**
 * Created by Rajnikant Kumar on 9/1/2018.
 */

public class PaymentPage {
    public static String get(String orderId,String txnAmount,String paymentUrl,String callbackUrl,String custId,String appCode, String txnType){
        //region : Payment Page
        String str="\n" +
                "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">\n" +
                "<html>\n" +
                "<head>\n" +
                "    <title>Payment in progress...</title>\n" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\">\n" +
                "    <script type=\"text/javascript\">\n" +
                "        function replace(id, newContent)\n" +
                "        {\n" +
                "            document.getElementById(id).innerHTML = newContent;\n" +
                "        }\n" +
                "    </script>\n" +
                "</head>\n" +
                "<body onload=\"document.form1.submit()\">\n" +
                "<h1>Payment in progress...</h1>\n" +
                "<pre>\n" +
                "</pre>\n" +
                "<form name=\"form1\" style=\"visibility:hidden\" method=\"post\" action=\""+paymentUrl+"\">\n" +
                "    <table border=\"1\">\n" +
                "        <tbody>\n" +
                "        <tr>\n" +
                "            <th>S.No</th>\n" +
                "            <th>Label</th>\n" +
                "            <th>Value</th>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "            <td>1</td>\n" +
                "            <td><label>Trans ID::*</label></td>\n" +
                "            <td><input id=\"ORDER_ID\" tabindex=\"1\" maxlength=\"20\" size=\"20\"\n" +
                "                       name=\"ORDER_ID\" autocomplete=\"off\"\n" +
                "                       value=\""+orderId+"\">\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "            <td>2</td>\n" +
                "            <td><label>CUSTID ::*</label></td>\n" +
                "            <td><input id=\"CUST_ID\" tabindex=\"2\" maxlength=\"30\" size=\"12\" name=\"CUST_ID\" autocomplete=\"off\" value=\""+custId+"\"></td>\n" +
                "        </tr>\n" +
                "        <!-- <tr>\n" +
                "        <td>3</td>\n" +
                "        <td><label>INDUSTRY_TYPE_ID ::*</label></td>\n" +
                "        <td><input id=\"INDUSTRY_TYPE_ID\" tabindex=\"4\" maxlength=\"12\" size=\"12\" name=\"INDUSTRY_TYPE_ID\" autocomplete=\"off\" value=\"Retail\"></td>\n" +
                "        </tr> -->\n" +
                "        <!-- <tr>\n" +
                "        <td>4</td>\n" +
                "        <td><label>Channel ::*</label></td>\n" +
                "        <td><input id=\"CHANNEL_ID\" tabindex=\"4\" maxlength=\"12\"\n" +
                "        size=\"12\" name=\"CHANNEL_ID\" autocomplete=\"off\" value=\"WEB\">\n" +
                "        </td>\n" +
                "        </tr> -->\n" +
                "        <tr>\n" +
                "            <td>5</td>\n" +
                "            <td><label>txnAmount*</label></td>\n" +
                "            <td><input id=\"ID_TXN_AMOUNT\" title=\"TXN_AMOUNT\" tabindex=\"10\"\n" +
                "                       type=\"text\" name=\"TXN_AMOUNT\"\n" +
                "                       value=\""+txnAmount+"\">\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "            <td>5</td>\n" +
                "            <td><label>txnType*</label></td>\n" +
                "            <td><input id=\"ID_TXN_TYPE\" title=\"TXN_TYPE\" tabindex=\"10\"\n" +
                "                       type=\"text\" name=\"TXN_TYPE\"\n" +
                "                       value=\""+txnType+"\">\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "            <td>6</td>\n" +
                "            <td><label>Callback URL*</label></td>\n" +
                "            <td><input id=\"cb_url\" title=\"Callback URL\" tabindex=\"10\"\n" +
                "                       type=\"text\" name=\"CALLBACK_URL\"\n" +
                "                       value=\""+callbackUrl+"\">\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "        <!-- <tr>\n" +
                "        <td>6</td>\n" +
                "        <td><label>Payment Option</label></td>\n" +
                "        <td>\n" +
                "        <select><option value=\"paytm\" selected>Paytm</option>\n" +
                "        <option value=\"citrus\">Citrus</option>\n" +
                "        <option value=\"test\">Test</option>\n" +
                "        </select>\n" +
                "\n" +
                "        </td>\n" +
                "        </tr> -->\n" +
                "        <tr>\n" +
                "            <td>6</td>\n" +
                "            <td><label>Application name</label></td>\n" +
                "            <td>\n" +
                "                <select name=\"APP_CODE\"><option value=\""+appCode+"\" selected>Air ticketing</option>\n" +
                "                    <option value=\""+appCode+"\">E-Catering</option>\n" +
                "                </select>\n" +
                "\n" +
                "            </td>\n" +
                "        </tr>\n" +
                "\n" +
                "        <tr>\n" +
                "            <td></td>\n" +
                "            <td></td>\n" +
                "            <td><input value=\"CheckOut\" type=\"submit\" onclick=\"\"></td>\n" +
                "        </tr>\n" +
                "        </tbody>\n" +
                "    </table>\n" +
                "    * - Mandatory Fields\n" +
                "</form>\n" +
                "</body>\n" +
                "</html>";
        //endregion
        return str;
    }
}
