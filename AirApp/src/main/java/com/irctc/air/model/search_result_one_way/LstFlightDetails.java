package com.irctc.air.model.search_result_one_way;

public class LstFlightDetails
{
    private String flightTime;
    private boolean isFreeMeal;

    private String originTerminal;

    private String originAirportName;

    private String departureDate;

    private String origin;

    private String originCity;

    private String destinationCity;

    private String departureTime;

    private String flightNumber;

    private String destinationTerminal;

    private String destination;

    private String arrivalTime;

    private String airlineCode;

    private String destinationAirportName;

    private String arrivalDate;

    private String halt;

   // private null key;

    private String airline;

    public String getOriginCity() {
        return originCity;
    }

    public void setOriginCity(String originCity) {
        this.originCity = originCity;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
    }


    public String getFlightTime ()
    {
        return flightTime;
    }

    public void setFlightTime (String flightTime)
    {
        this.flightTime = flightTime;
    }

   public String getOriginTerminal ()
    {
        return originTerminal;
    }

    public void setOriginTerminal (String originTerminal)
    {
        this.originTerminal = originTerminal;
    }

    public String getOriginAirportName ()
    {
        return originAirportName;
    }

    public void setOriginAirportName (String originAirportName)
    {
        this.originAirportName = originAirportName;
    }

    public String getDepartureDate ()
    {
        return departureDate;
    }

    public void setDepartureDate (String departureDate)
    {
        this.departureDate = departureDate;
    }

    public String getOrigin ()
    {
        return origin;
    }

    public void setOrigin (String origin)
    {
        this.origin = origin;
    }

    public String getDepartureTime ()
    {
        return departureTime;
    }

    public void setDepartureTime (String departureTime)
    {
        this.departureTime = departureTime;
    }

    public String getFlightNumber ()
    {
        return flightNumber;
    }

    public void setFlightNumber (String flightNumber)
    {
        this.flightNumber = flightNumber;
    }

   public String getDestinationTerminal ()
    {
        return destinationTerminal;
    }

    public void setDestinationTerminal (String destinationTerminal)
    {
        this.destinationTerminal = destinationTerminal;
    }

    public String getDestination ()
    {
        return destination;
    }

    public void setDestination (String destination)
    {
        this.destination = destination;
    }

    public String getArrivalTime ()
    {
        return arrivalTime;
    }

    public void setArrivalTime (String arrivalTime)
    {
        this.arrivalTime = arrivalTime;
    }

    public String getAirlineCode ()
    {
        return airlineCode;
    }

    public void setAirlineCode (String airlineCode)
    {
        this.airlineCode = airlineCode;
    }

    public String getDestinationAirportName ()
    {
        return destinationAirportName;
    }

    public void setDestinationAirportName (String destinationAirportName)
    {
        this.destinationAirportName = destinationAirportName;
    }

    public String getArrivalDate ()
    {
        return arrivalDate;
    }

    public void setArrivalDate (String arrivalDate)
    {
        this.arrivalDate = arrivalDate;
    }

  public String getHalt ()
    {
        return halt;
    }

    public void setHalt (String halt)
    {
        this.halt = halt;
    }
  /*
    public null getKey ()
    {
        return key;
    }

    public void setKey (null key)
    {
        this.key = key;
    }*/

    public String getAirline ()
    {
        return airline;
    }

    public void setAirline (String airline)
    {
        this.airline = airline;
    }

    public boolean isFreeMeal() {
        return isFreeMeal;
    }

    public void setFreeMeal(boolean freeMeal) {
        isFreeMeal = freeMeal;
    }

    @Override
    public String toString()
    {
        //, originTerminal = "+originTerminal+"
        //, destinationTerminal = "+destinationTerminal+"
        //, destinationAirportName = "+destinationAirportName+"
        //, halt = "+halt+", key = "+key+"
        return "ClassPojo [flightTime = "+flightTime+", originAirportName = "+originAirportName+", departureDate = "+departureDate+", origin = "+origin+", departureTime = "+departureTime+", flightNumber = "+flightNumber+", destination = "+destination+", arrivalTime = "+arrivalTime+", airlineCode = "+airlineCode+", arrivalDate = "+arrivalDate+", airline = "+airline+"]";
    }
}