package com.irctc.air.model.ticket;

public class LstIrFbPassengerDetail
{
/*
    private null dateOfBirth;
*/

    private String lastName;
/*

    private null seatPreference;

    private null passportNo;

    private null passengerNo;
*/

    private String ticketBookingStatus;

/*
    private null countryOfIssue;
*/

    private String passengerType;
/*

    private null expiryDate;

    private null placeOfIssue;
*/

    private Id id;

/*
    private null mealPreference;

    private null nationality;
*/

    private String titleType;

/*
    private null age;

    private null otherRemark;
*/

    private String gender;

/*
    private null cancelCurrentStatus;

    private null paxTicketNo;

*/
    private String firstName;

/*
    private null spRemark;

    private null dateOfIssue;

    public null getDateOfBirth ()
    {
        return dateOfBirth;
    }

    public void setDateOfBirth (null dateOfBirth)
    {
        this.dateOfBirth = dateOfBirth;
    }
*/

    public String getLastName ()
    {
        return lastName;
    }

    public void setLastName (String lastName)
    {
        this.lastName = lastName;
    }

/*
    public null getSeatPreference ()
    {
        return seatPreference;
    }

    public void setSeatPreference (null seatPreference)
    {
        this.seatPreference = seatPreference;
    }

    public null getPassportNo ()
    {
        return passportNo;
    }

    public void setPassportNo (null passportNo)
    {
        this.passportNo = passportNo;
    }

    public null getPassengerNo ()
    {
        return passengerNo;
    }

    public void setPassengerNo (null passengerNo)
    {
        this.passengerNo = passengerNo;
    }

*/
    public String getTicketBookingStatus ()
    {
        return ticketBookingStatus;
    }

    public void setTicketBookingStatus (String ticketBookingStatus)
    {
        this.ticketBookingStatus = ticketBookingStatus;
    }

/*
    public null getCountryOfIssue ()
    {
        return countryOfIssue;
    }

    public void setCountryOfIssue (null countryOfIssue)
    {
        this.countryOfIssue = countryOfIssue;
    }
*/

    public String getPassengerType ()
    {
        return passengerType;
    }

    public void setPassengerType (String passengerType)
    {
        this.passengerType = passengerType;
    }

/*
    public null getExpiryDate ()
    {
        return expiryDate;
    }

    public void setExpiryDate (null expiryDate)
    {
        this.expiryDate = expiryDate;
    }

    public null getPlaceOfIssue ()
    {
        return placeOfIssue;
    }

    public void setPlaceOfIssue (null placeOfIssue)
    {
        this.placeOfIssue = placeOfIssue;
    }
*/

    public Id getId ()
    {
        return id;
    }

    public void setId (Id id)
    {
        this.id = id;
    }

/*
    public null getMealPreference ()
    {
        return mealPreference;
    }

    public void setMealPreference (null mealPreference)
    {
        this.mealPreference = mealPreference;
    }

    public null getNationality ()
    {
        return nationality;
    }

    public void setNationality (null nationality)
    {
        this.nationality = nationality;
    }
*/

    public String getTitleType ()
    {
        return titleType;
    }

    public void setTitleType (String titleType)
    {
        this.titleType = titleType;
    }

/*
    public null getAge ()
    {
        return age;
    }

    public void setAge (null age)
    {
        this.age = age;
    }

    public null getOtherRemark ()
    {
        return otherRemark;
    }

    public void setOtherRemark (null otherRemark)
    {
        this.otherRemark = otherRemark;
    }

*/
    public String getGender ()
    {
        return gender;
    }

    public void setGender (String gender)
    {
        this.gender = gender;
    }

/*
    public null getCancelCurrentStatus ()
    {
        return cancelCurrentStatus;
    }

    public void setCancelCurrentStatus (null cancelCurrentStatus)
    {
        this.cancelCurrentStatus = cancelCurrentStatus;
    }

    public null getPaxTicketNo ()
    {
        return paxTicketNo;
    }

    public void setPaxTicketNo (null paxTicketNo)
    {
        this.paxTicketNo = paxTicketNo;
    }
*/

    public String getFirstName ()
    {
        return firstName;
    }

    public void setFirstName (String firstName)
    {
        this.firstName = firstName;
    }

/*
    public null getSpRemark ()
    {
        return spRemark;
    }

    public void setSpRemark (null spRemark)
    {
        this.spRemark = spRemark;
    }

    public null getDateOfIssue ()
    {
        return dateOfIssue;
    }

    public void setDateOfIssue (null dateOfIssue)
    {
        this.dateOfIssue = dateOfIssue;
    }

*/
/*
    @Override
    public String toString()
    {
        return "ClassPojo [dateOfBirth = "+dateOfBirth+", lastName = "+lastName+", seatPreference = "+seatPreference+", passportNo = "+passportNo+", passengerNo = "+passengerNo+", ticketBookingStatus = "+ticketBookingStatus+", countryOfIssue = "+countryOfIssue+", passengerType = "+passengerType+", expiryDate = "+expiryDate+", placeOfIssue = "+placeOfIssue+", id = "+id+", mealPreference = "+mealPreference+", nationality = "+nationality+", titleType = "+titleType+", age = "+age+", otherRemark = "+otherRemark+", gender = "+gender+", cancelCurrentStatus = "+cancelCurrentStatus+", paxTicketNo = "+paxTicketNo+", firstName = "+firstName+", spRemark = "+spRemark+", dateOfIssue = "+dateOfIssue+"]";
    }
*/
}

			