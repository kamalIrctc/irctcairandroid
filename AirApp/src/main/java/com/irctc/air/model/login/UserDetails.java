package com.irctc.air.model.login;

public class UserDetails
{
   // private null dateOfBirth;

    private String lastName;

    //private null panCard;

    private String state;

    private String firstname;

    private String password;

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    private UserInfo userInfo;

    private String city;

    private String country;

   // private null id;

    private String userState;

    private String pincode;

    private String username;

    private String email;

    private String address;

  //  private null role;

//    private null gender;

    private String mobileNumber;

  /*  public null getDateOfBirth ()
    {
        return dateOfBirth;
    }

    public void setDateOfBirth (null dateOfBirth)
    {
        this.dateOfBirth = dateOfBirth;
    }
*/
    public String getLastName ()
    {
        return lastName;
    }

    public void setLastName (String lastName)
    {
        this.lastName = lastName;
    }

  /*  public null getPanCard ()
    {
        return panCard;
    }

    public void setPanCard (null panCard)
    {
        this.panCard = panCard;
    }
*/
    public String getState ()
    {
        return state;
    }

    public void setState (String state)
    {
        this.state = state;
    }

    public String getFirstname ()
    {
        return firstname;
    }

    public void setFirstname (String firstname)
    {
        this.firstname = firstname;
    }

    public String getPassword ()
    {
        return password;
    }

    public void setPassword (String password)
    {
        this.password = password;
    }

    public String getCity ()
{
    return city;
}

    public void setCity (String city)
    {
        this.city = city;
    }

    public String getCountry ()
{
    return country;
}

    public void setCountry (String country)
    {
        this.country = country;
    }

  /*  public null getUserType ()
    {
        return userType;
    }

    public void setUserType (null userType)
    {
        this.userType = userType;
    }





    public null getId ()
    {
        return id;
    }

    public void setId (null id)
    {
        this.id = id;
    }

  */  public String getUserState ()
    {
        return userState;
    }

    public void setUserState (String userState)
    {
        this.userState = userState;
    }

    public String getPincode ()
    {
        return pincode;
    }

    public void setPincode (String pincode)
    {
        this.pincode = pincode;
    }
    public String getUsername ()
    {
        return username;
    }

    public void setUsername (String username)
    {
        this.username = username;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }


    public String getAddress ()
    {
        return address;
    }

    public void setAddress (String address)
    {
        this.address = address;
    }
/*
    public null getRole ()
    {
        return role;
    }

    public void setRole (null role)
    {
        this.role = role;
    }

    public null getGender ()
    {
        return gender;
    }

    public void setGender (null gender)
    {
        this.gender = gender;
    }
*/

    public String getMobileNumber ()
    {
        return mobileNumber;
    }

    public void setMobileNumber (String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    @Override
    public String toString()
    {
        //dateOfBirth = "+dateOfBirth+",
        //, panCard = "+panCard+", state = "+state+"
        //, userType = "+userType+", city = "+city+", country = "+country+", id = "+id+"
        //, pincode = "+pincode+"
        // address = "+address+", role = "+role+", gender = "+gender+",
        return "ClassPojo [lastName = "+lastName+", firstname = "+firstname+", password = "+password+", userState = "+userState+", username = "+username+", email = "+email+", mobileNumber = "+mobileNumber+"]";
    }
}
