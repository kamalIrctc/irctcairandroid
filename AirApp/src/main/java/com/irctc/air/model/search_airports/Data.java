package com.irctc.air.model.search_airports;

public class Data
{
    private String Index;

    private String searchKey;

    private String cityName;

    private String countryCode;

    private String label;

    private String code;

    public String getIndex ()
    {
        return Index;
    }

    public void setIndex (String Index)
    {
        this.Index = Index;
    }

    public String getSearchKey ()
    {
        return searchKey;
    }

    public void setSearchKey (String searchKey)
    {
        this.searchKey = searchKey;
    }

    public String getCityName ()
    {
        return cityName;
    }

    public void setCityName (String cityName)
    {
        this.cityName = cityName;
    }

    public String getCountryCode ()
    {
        return countryCode;
    }

    public void setCountryCode (String countryCode)
    {
        this.countryCode = countryCode;
    }

    public String getLabel ()
    {
        return label;
    }

    public void setLabel (String label)
    {
        this.label = label;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [Index = "+Index+", searchKey = "+searchKey+", cityName = "+cityName+", countryCode = "+countryCode+", label = "+label+", code = "+code+"]";
    }
}

			