package com.irctc.air.model.reprice_one_way;

public class TaxDetails
{
    private String airportDevFee;

    private String gst;

    private String other;

    private String paxServiceFee;

    private String fuelFee;

    private String yq;


    public String getYq() {
        return yq;
    }

    public void setYq(String yq) {
        this.yq = yq;
    }

    public String getAirportDevFee ()
    {
        return airportDevFee;
    }

    public void setAirportDevFee (String airportDevFee)
    {
        this.airportDevFee = airportDevFee;
    }

    public String getGst ()
    {
        return gst;
    }

    public void setGst (String gst)
    {
        this.gst = gst;
    }

    public String getOther ()
    {
        return other;
    }

    public void setOther (String other)
    {
        this.other = other;
    }

    public String getPaxServiceFee ()
    {
        return paxServiceFee;
    }

    public void setPaxServiceFee (String paxServiceFee)
    {
        this.paxServiceFee = paxServiceFee;
    }

    public String getFuelFee ()
    {
        return fuelFee;
    }

    public void setFuelFee (String fuelFee)
    {
        this.fuelFee = fuelFee;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [airportDevFee = "+airportDevFee+", gst = "+gst+", other = "+other+", paxServiceFee = "+paxServiceFee+", fuelFee = "+fuelFee+"]";
    }
}

			