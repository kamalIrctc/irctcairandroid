package com.irctc.air.model.search_result_one_way;

import java.util.Arrays;

public class Data
{
    //private null intlFlights;

    private Flights[] flights;

    private String searchKey;

    private String isInternational;

    //private null lstLowFareFlight;

   /* public null getIntlFlights ()
    {
        return intlFlights;
    }

    public void setIntlFlights (null intlFlights)
    {
        this.intlFlights = intlFlights;
    }*/

    public Flights[] getFlights ()
    {
        return flights;
    }

    public void setFlights (Flights[] flights)
    {
        this.flights = flights;
    }

    public String getSearchKey ()
    {
        return searchKey;
    }

    public void setSearchKey (String searchKey)
    {
        this.searchKey = searchKey;
    }

    public String getIsInternational ()
    {
        return isInternational;
    }

    public void setIsInternational (String isInternational)
    {
        this.isInternational = isInternational;
    }

   /* public null getLstLowFareFlight ()
    {
        return lstLowFareFlight;
    }

    public void setLstLowFareFlight (null lstLowFareFlight)
    {
        this.lstLowFareFlight = lstLowFareFlight;
    }*/

    /*@Override
    public String toString() {
        return "ClassPojo[" +
                "flights=" + Arrays.toString(flights) +
                ", searchKey='" + searchKey + '\'' +
                ", isInternational='" + isInternational + '\'' +
                ", isFreeMeal=" + isFreeMeal +
                ", isBaggageAllowed=" + isBaggageAllowed +
                ']';
    }*/

   @Override
    public String toString()
    {
        //intlFlights = "+intlFlights+",
        //, lstLowFareFlight = "+lstLowFareFlight+"
        return "ClassPojo [flights = "+flights+", searchKey = "+searchKey+", isInternational = "+isInternational+"]";
    }
}