package com.irctc.air.model.recent_search;

/**
 * Created by Rajnikant Kumar on 9/18/2018.
 */

public class ModelRecentSearch {
    String stationCodeFrom;
    String stationNameFrom;
    String stationCodeTo;
    String stationNameTo;
    String tripType;
    String tripClass;
    String passAdultCount;
    String passChildCount;
    String passInfantCount;
    String depertureDay;
    String depertureMonth;
    String depertureYear;
    String depertureDayName;
    String depertureMonthName;
    String returnDay;
    String returnMonth;
    String returnYear;
    String returnDayName;
    String returnMonthName;
    String isLTC;


    public String getIsLTC() {
        return isLTC;
    }

    public void setIsLTC(String isLTC) {
        this.isLTC = isLTC;
    }

    public String getStationCodeFrom() {
        return stationCodeFrom;
    }

    public void setStationCodeFrom(String stationCodeFrom) {
        this.stationCodeFrom = stationCodeFrom;
    }

    public String getStationNameFrom() {
        return stationNameFrom;
    }

    public void setStationNameFrom(String stationNameFrom) {
        this.stationNameFrom = stationNameFrom;
    }

    public String getStationCodeTo() {
        return stationCodeTo;
    }

    public void setStationCodeTo(String stationCodeTo) {
        this.stationCodeTo = stationCodeTo;
    }

    public String getStationNameTo() {
        return stationNameTo;
    }

    public void setStationNameTo(String stationNameTo) {
        this.stationNameTo = stationNameTo;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getTripClass() {
        return tripClass;
    }

    public void setTripClass(String tripClass) {
        this.tripClass = tripClass;
    }

    public String getPassAdultCount() {
        return passAdultCount;
    }

    public void setPassAdultCount(String passAdultCount) {
        this.passAdultCount = passAdultCount;
    }

    public String getPassChildCount() {
        return passChildCount;
    }

    public void setPassChildCount(String passChildCount) {
        this.passChildCount = passChildCount;
    }

    public String getPassInfantCount() {
        return passInfantCount;
    }

    public void setPassInfantCount(String passInfantCount) {
        this.passInfantCount = passInfantCount;
    }

    public String getDepertureDay() {
        return depertureDay;
    }

    public void setDepertureDay(String depertureDay) {
        this.depertureDay = depertureDay;
    }

    public String getDepertureMonth() {
        return depertureMonth;
    }

    public void setDepertureMonth(String depertureMonth) {
        this.depertureMonth = depertureMonth;
    }

    public String getDepertureYear() {
        return depertureYear;
    }

    public void setDepertureYear(String depertureYear) {
        this.depertureYear = depertureYear;
    }

    public String getDepertureDayName() {
        return depertureDayName;
    }

    public void setDepertureDayName(String depertureDayName) {
        this.depertureDayName = depertureDayName;
    }

    public String getDepertureMonthName() {
        return depertureMonthName;
    }

    public void setDepertureMonthName(String depertureMonthName) {
        this.depertureMonthName = depertureMonthName;
    }

    public String getReturnDay() {
        return returnDay;
    }

    public void setReturnDay(String returnDay) {
        this.returnDay = returnDay;
    }

    public String getReturnMonth() {
        return returnMonth;
    }

    public void setReturnMonth(String returnMonth) {
        this.returnMonth = returnMonth;
    }

    public String getReturnYear() {
        return returnYear;
    }

    public void setReturnYear(String returnYear) {
        this.returnYear = returnYear;
    }

    public String getReturnDayName() {
        return returnDayName;
    }

    public void setReturnDayName(String returnDayName) {
        this.returnDayName = returnDayName;
    }

    public String getReturnMonthName() {
        return returnMonthName;
    }

    public void setReturnMonthName(String returnMonthName) {
        this.returnMonthName = returnMonthName;
    }
}
