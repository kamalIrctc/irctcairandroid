package com.irctc.air.model.search_result_one_way;

import com.irctc.air.round.trip.domastic.model.LstBaggageDetails;

public class Flights {
    private LstFareDetails[] lstFareDetails;

    // private null segments;
    private LstBaggageDetails[] lstBaggageDetails;

    private String departureDate;

    private String departureCityWithCode;

    private String departureTime;

    private String flightNumber;

    private String arrivalCityWithCode;

    private String stops;

    private String cacheKey;

    private AdditionalTypeBaseAirSegment[] additionalTypeBaseAirSegment;

    private String duration;

    private String arrivalTime;

    private String price;

    private String carrierName;

    private String carrier;

    private String arrivalDate;

    private String segmentType;

    private LstFlightDetails[] lstFlightDetails;

    private String serviceProvider;

    private String key;

    private boolean isBaggageAllowed;
    private boolean isFreeMeal;

    public LstFareDetails[] getLstFareDetails() {
        return lstFareDetails;
    }

    public void setLstFareDetails(LstFareDetails[] lstFareDetails) {
        this.lstFareDetails = lstFareDetails;
    }

   /* public null getSegments ()
    {
        return segments;
    }

    public void setSegments (null segments)
    {
        this.segments = segments;
    }

    public null getIsBaggageAllowed ()
    {
        return isBaggageAllowed;
    }

    public void setIsBaggageAllowed (null isBaggageAllowed)
    {
        this.isBaggageAllowed = isBaggageAllowed;
    }*/

    public boolean isFreeMeal() {
        return isFreeMeal;
    }

    public void setFreeMeal(boolean freeMeal) {
        isFreeMeal = freeMeal;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getDepartureCityWithCode() {
        return departureCityWithCode;
    }

    public void setDepartureCityWithCode(String departureCityWithCode) {
        this.departureCityWithCode = departureCityWithCode;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getArrivalCityWithCode() {
        return arrivalCityWithCode;
    }

    public void setArrivalCityWithCode(String arrivalCityWithCode) {
        this.arrivalCityWithCode = arrivalCityWithCode;
    }

    public String getStops() {
        return stops;
    }

    public void setStops(String stops) {
        this.stops = stops;
    }

    public String getCacheKey() {
        return cacheKey;
    }

    public void setCacheKey(String cacheKey) {
        this.cacheKey = cacheKey;
    }

    public AdditionalTypeBaseAirSegment[] getAdditionalTypeBaseAirSegment() {
        return additionalTypeBaseAirSegment;
    }

    public void setAdditionalTypeBaseAirSegment(AdditionalTypeBaseAirSegment[] additionalTypeBaseAirSegment) {
        this.additionalTypeBaseAirSegment = additionalTypeBaseAirSegment;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getSegmentType() {
        return segmentType;
    }

    public void setSegmentType(String segmentType) {
        this.segmentType = segmentType;
    }

    public LstFlightDetails[] getLstFlightDetails() {
        return lstFlightDetails;
    }

    public void setLstFlightDetails(LstFlightDetails[] lstFlightDetails) {
        this.lstFlightDetails = lstFlightDetails;
    }

    public String getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(String serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean isBaggageAllowed() {
        return isBaggageAllowed;
    }

    public void setBaggageAllowed(boolean baggageAllowed) {
        isBaggageAllowed = baggageAllowed;
    }

    public LstBaggageDetails[] getLstBaggageDetails() {
        return lstBaggageDetails;
    }

    public void setLstBaggageDetails(LstBaggageDetails[] lstBaggageDetails) {
        this.lstBaggageDetails = lstBaggageDetails;
    }



    @Override
    public String toString() {
        //, segments = "+segments+", isBaggageAllowed = "+isBaggageAllowed+"
        //, segmentType = "+segmentType+"
        return "ClassPojo [lstFareDetails = " + lstFareDetails + ", departureDate = " + departureDate + ", departureCityWithCode = " + departureCityWithCode + ", departureTime = " + departureTime + ", flightNumber = " + flightNumber + ", arrivalCityWithCode = " + arrivalCityWithCode + ", stops = " + stops + ", cacheKey = " + cacheKey + ", additionalTypeBaseAirSegment = " + additionalTypeBaseAirSegment + ", duration = " + duration + ", arrivalTime = " + arrivalTime + ", price = " + price + ", carrierName = " + carrierName + ", carrier = " + carrier + ", arrivalDate = " + arrivalDate + ", lstFlightDetails = " + lstFlightDetails + ", serviceProvider = " + serviceProvider + ", key = " + key + ", isBaggageAllowed = " + isBaggageAllowed + "]";
    }
}