package com.irctc.air.model.search_result_round_trip;

public class FlightDetailsRef
{
    private String key;

    public String getKey ()
    {
        return key;
    }

    public void setKey (String key)
    {
        this.key = key;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [key = "+key+"]";
    }
}
