package com.irctc.air.model.search_result_round_trip;

public class CodeshareInfo
{
    private String operatingFlightNumber;

    private String operatingCarrier;

    private String value;

    public String getOperatingFlightNumber ()
    {
        return operatingFlightNumber;
    }

    public void setOperatingFlightNumber (String operatingFlightNumber)
    {
        this.operatingFlightNumber = operatingFlightNumber;
    }

    public String getOperatingCarrier ()
    {
        return operatingCarrier;
    }

    public void setOperatingCarrier (String operatingCarrier)
    {
        this.operatingCarrier = operatingCarrier;
    }

    public String getValue ()
    {
        return value;
    }

    public void setValue (String value)
    {
        this.value = value;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [operatingFlightNumber = "+operatingFlightNumber+", operatingCarrier = "+operatingCarrier+", value = "+value+"]";
    }
}

			