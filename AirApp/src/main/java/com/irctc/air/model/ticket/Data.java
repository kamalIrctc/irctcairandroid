package com.irctc.air.model.ticket;

import com.irctc.air.round.trip.domastic.model.LstBaggageDetails;

public class Data {
    private LstBaggageDetails[] lstBaggageDetails;
    private String serviceProvider;
    private LstIrFbFareDetail[] lstIrFbFareDetail;

    private LstIrFbExtraInfo lstIrFbExtraInfo;

    private LstIrFbPassengerDetail[] lstIrFbPassengerDetail;

    private IrFbServiceCharge irFbServiceCharge;

    private IrFlightsBook irFlightsBook;

    private TicketInfo[] ticketInfo;

    private LstIrFbFlightDetail[] lstIrFbFlightDetail;

    private String isSpl;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private String status;

    private boolean cancelAllowed;

    public String getIsSpl() {
        return isSpl;
    }

    public void setIsSpl(String isSpl) {
        this.isSpl = isSpl;
    }

    public IrFbServiceCharge getIrFbServiceCharge() {
        return irFbServiceCharge;
    }

    public void setIrFbServiceCharge(IrFbServiceCharge irFbServiceCharge) {
        this.irFbServiceCharge = irFbServiceCharge;
    }


    public LstIrFbFareDetail[] getLstIrFbFareDetail() {
        return lstIrFbFareDetail;
    }

    public void setLstIrFbFareDetail(LstIrFbFareDetail[] lstIrFbFareDetail) {
        this.lstIrFbFareDetail = lstIrFbFareDetail;
    }

    public LstIrFbExtraInfo getLstIrFbExtraInfo() {
        return lstIrFbExtraInfo;
    }

    public void setLstIrFbExtraInfo(LstIrFbExtraInfo lstIrFbExtraInfo) {
        this.lstIrFbExtraInfo = lstIrFbExtraInfo;
    }

    public LstIrFbPassengerDetail[] getLstIrFbPassengerDetail() {
        return lstIrFbPassengerDetail;
    }

    public void setLstIrFbPassengerDetail(LstIrFbPassengerDetail[] lstIrFbPassengerDetail) {
        this.lstIrFbPassengerDetail = lstIrFbPassengerDetail;
    }

    public TicketInfo[] getTicketInfo() {
        return ticketInfo;
    }

    public void setTicketInfo(TicketInfo[] ticketInfo) {
        this.ticketInfo = ticketInfo;
    }

    public LstIrFbFlightDetail[] getLstIrFbFlightDetail() {
        return lstIrFbFlightDetail;
    }

    public void setLstIrFbFlightDetail(LstIrFbFlightDetail[] lstIrFbFlightDetail) {
        this.lstIrFbFlightDetail = lstIrFbFlightDetail;
    }

    public boolean isCancelAllowed() {
        return cancelAllowed;
    }

    public void setCancelAllowed(boolean cancelAllowed) {
        this.cancelAllowed = cancelAllowed;
    }


    public IrFlightsBook getIrFlightsBook() {
        return irFlightsBook;
    }

    public void irFlightsBook(IrFlightsBook irFlightsBook) {
        this.irFlightsBook = irFlightsBook;
    }

    public LstBaggageDetails[] getLstBaggageDetails() {
        return lstBaggageDetails;
    }

    public void setLstBaggageDetails(LstBaggageDetails[] lstBaggageDetails) {
        this.lstBaggageDetails = lstBaggageDetails;
    }

    public String getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(String serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public void setIrFlightsBook(IrFlightsBook irFlightsBook) {
        this.irFlightsBook = irFlightsBook;
    }

    @Override
    public String toString() {
        return "ClassPojo [lstIrFbFareDetail = " + lstIrFbFareDetail + ", lstIrFbExtraInfo = " + lstIrFbExtraInfo + ", lstIrFbPassengerDetail = " + lstIrFbPassengerDetail +
                ", ticketInfo = " + ticketInfo + ", lstIrFbFlightDetail = " + lstIrFbFlightDetail +
                ", cancelAllowed = " + cancelAllowed + ", irFlightsBook = "+irFlightsBook +"]";
    }
}

			