package com.irctc.air.model.reprice_one_way;

public class Inf
{
    private String paxType;

    private String dest;

    private String price;

    private String deptDate;

    private String name;

    private String paxNum;

    private String origin;

    private String flightNumber;

    public String getPaxType ()
    {
        return paxType;
    }

    public void setPaxType (String paxType)
    {
        this.paxType = paxType;
    }


    public String getDest ()
    {
        return dest;
    }

    public void setDest (String dest)
    {
        this.dest = dest;
    }

    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }

    public String getDeptDate ()
    {
        return deptDate;
    }

    public void setDeptDate (String deptDate)
    {
        this.deptDate = deptDate;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getPaxNum ()
    {
        return paxNum;
    }

    public void setPaxNum (String paxNum)
    {
        this.paxNum = paxNum;
    }

    public String getOrigin ()
    {
        return origin;
    }

    public void setOrigin (String origin)
    {
        this.origin = origin;
    }

    public String getFlightNumber ()
    {
        return flightNumber;
    }

    public void setFlightNumber (String flightNumber)
    {
        this.flightNumber = flightNumber;
    }

    @Override
    public String toString()
    {
        //paxType = "+paxType+",
        //, paxNum = "+paxNum+"
        return "ClassPojo [dest = "+dest+", price = "+price+", deptDate = "+deptDate+", name = "+name+", origin = "+origin+", flightNumber = "+flightNumber+"]";
    }
}

			