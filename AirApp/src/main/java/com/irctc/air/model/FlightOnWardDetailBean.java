package com.irctc.air.model;

import java.util.ArrayList;

/**
 * Created by tourism on 4/11/2016.
 */
public class FlightOnWardDetailBean {

   // Asif code
    private String flightOnwardFlightNo;
    private String flightOnwardTotalDuration;
    private int flightOnwardTotalStops;
    private String flightOnwDepartureTime;
    private String flightOnwArrivalTime;


    private String flightOnwardNumTotalDuration;


    private String flightReturnFlightNo;
    private String flightReturnTotalDuration;
    private int flightreturnTotalStops;
    private String flightRetDepartureTime;
    private String flightRetArrivalTime;


    private String flightAirline;
    private String flightJourneyStartFrom;
    private String flightJourneyEndTo;
    private int flightFare;
    private int flightBaseFare;
    private int flightTaxInFare;



    private int flightStaxInFare;
    private int flightIcon;
    private int flightUniqueId;
    private String flightJson;

    // Added by asif
    public int getFlightStaxInFare() {
        return flightStaxInFare;
    }

    public void setFlightStaxInFare(int flightStaxInFare) {
        this.flightStaxInFare = flightStaxInFare;
    }

    public String getFlightOnwardNumTotalDuration() {
        return flightOnwardNumTotalDuration;
    }

    public void setFlightOnwardNumTotalDuration(String flightOnwardNumTotalDuration) {
        this.flightOnwardNumTotalDuration = flightOnwardNumTotalDuration;
    }


    // Added by vivek
    private String flightNumber;

    ArrayList<InnerFlightDetailsBeans> flight;


    public String getFlightJson() {
        return flightJson;
    }

    public void setFlightJson(String flightJson) {
        this.flightJson = flightJson;
    }


    public int getFlightIcon() {
        return flightIcon;
    }

    public void setFlightIcon(int flightIcon) {
        this.flightIcon = flightIcon;
    }


    public ArrayList<InnerFlightDetailsBeans> getFlight() {
        return flight;
    }

    public void setFlight(ArrayList<InnerFlightDetailsBeans> flight) {
        this.flight = flight;
    }

    public int getFlightUniqueId() {
        return flightUniqueId;
    }

    public void setFlightUniqueId(int flightUniqueId) {
        this.flightUniqueId = flightUniqueId;
    }

    public String getFlightAirline() {
        return flightAirline;
    }

    public void setFlightAirline(String flightAirline) {
        this.flightAirline = flightAirline;
    }

    public int getFlightFare() {
        return flightFare;
    }

    public void setFlightFare(int flightFare) {
        this.flightFare = flightFare;
    }

    public int getFlightBaseFare() {
        return flightBaseFare;
    }

    public void setFlightBaseFare(int flightBaseFare) {
        this.flightBaseFare = flightBaseFare;
    }

    public int getFlightTaxInFare() {
        return flightTaxInFare;
    }

    public void setFlightTaxInFare(int flightTaxInFare) {
        this.flightTaxInFare = flightTaxInFare;
    }

    public String getFlightJourneyStartFrom() {
        return flightJourneyStartFrom;
    }

    public void setFlightJourneyStartFrom(String flightJourneyStartFrom) {
        this.flightJourneyStartFrom = flightJourneyStartFrom;
    }

    public String getFlightJourneyEndTo() {
        return flightJourneyEndTo;
    }

    public void setFlightJourneyEndTo(String flightJourneyEndTo) {
        this.flightJourneyEndTo = flightJourneyEndTo;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }


// Asif setter getter

    public String getFlightOnwardFlightNo() {
        return flightOnwardFlightNo;
    }

    public void setFlightOnwardFlightNo(String flightOnwardFlightNo) {
        this.flightOnwardFlightNo = flightOnwardFlightNo;
    }

    public String getFlightReturnFlightNo() {
        return flightReturnFlightNo;
    }

    public void setFlightReturnFlightNo(String flightReturnFlightNo) {
        this.flightReturnFlightNo = flightReturnFlightNo;
    }

    public String getFlightOnwDepartureTime() {
        return flightOnwDepartureTime;
    }

    public void setFlightOnwDepartureTime(String flightOnwDepartureTime) {
        this.flightOnwDepartureTime = flightOnwDepartureTime;
    }

    public String getFlightOnwArrivalTime() {
        return flightOnwArrivalTime;
    }

    public void setFlightOnwArrivalTime(String flightOnwArrivalTime) {
        this.flightOnwArrivalTime = flightOnwArrivalTime;
    }

    public String getFlightRetDepartureTime() {
        return flightRetDepartureTime;
    }

    public void setFlightRetDepartureTime(String flightRetDepartureTime) {
        this.flightRetDepartureTime = flightRetDepartureTime;
    }

    public String getFlightRetArrivalTime() {
        return flightRetArrivalTime;
    }

    public void setFlightRetArrivalTime(String flightRetArrivalTime) {
        this.flightRetArrivalTime = flightRetArrivalTime;
    }

    public String getFlightReturnTotalDuration() {
        return flightReturnTotalDuration;
    }

    public void setFlightReturnTotalDuration(String flightReturnTotalDuration) {
        this.flightReturnTotalDuration = flightReturnTotalDuration;
    }

    public int getFlightreturnTotalStops() {
        return flightreturnTotalStops;
    }

    public void setFlightreturnTotalStops(int flightreturnTotalStops) {
        this.flightreturnTotalStops = flightreturnTotalStops;
    }

    public String getFlightOnwardTotalDuration() {
        return flightOnwardTotalDuration;
    }

    public void setFlightOnwardTotalDuration(String flightOnwardTotalDuration) {
        this.flightOnwardTotalDuration = flightOnwardTotalDuration;
    }

    public int getFlightOnwardTotalStops() {
        return flightOnwardTotalStops;
    }

    public void setFlightOnwardTotalStops(int flightOnwardTotalStops) {
        this.flightOnwardTotalStops = flightOnwardTotalStops;
    }



}
