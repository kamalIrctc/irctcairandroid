package com.irctc.air.model.search_result_round_trip.model_combo;

import com.irctc.air.model.search_result_round_trip.IntlFlights;

import java.util.ArrayList;

/**
 * Created by Rajnikant Kumar on 9/27/2018.
 */

public class ModelFlightsCombo {
    ArrayList<IntlFlights> onwardFlightsList;
    ArrayList<IntlFlights> returnFlightsList;

    public ModelFlightsCombo(){
        onwardFlightsList=new ArrayList<>();
        returnFlightsList=new ArrayList<>();
    }
    public  ArrayList<IntlFlights>  getOnwardFlightsList() {
        return onwardFlightsList;
    }

    public void setOnwardFlightsList(IntlFlights intlFlight) {
        onwardFlightsList.add(intlFlight);
    }

    public  ArrayList<IntlFlights>  getReturnFlightsList() {
        return returnFlightsList;
    }

    public void setReturnFlightsList(IntlFlights intlFlight) {
        returnFlightsList.add(intlFlight);
    }
}
