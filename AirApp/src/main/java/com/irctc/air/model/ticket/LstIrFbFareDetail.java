package com.irctc.air.model.ticket;

public class LstIrFbFareDetail
{
    private String param4Received;

    private String fareBasisCode;

    ///private null serviceTax;

    private String baseFare;
    private String total;

    /*private null tax2;

    private null tax1;

    private null exchangeRate;
*/
    private String param2Received;

    private String tax3;

  /*  private null lccRetRound;

    private null airlineTransactionFee;

    private null paidAmountCurrency;

    private null irctcDiscount;
*/
    private String airlinePnr;

    private String param5Received;

  /*  private null passengerTicketType;

    private null reconciled;

    private null ffn;

    private null mlPrice;

    private null commission;

  */  private String param1Received;

    private String subTotalFare;

    /*private null segmentNoForFare;

    private null bgPrice;

    private null baseFareDiscount;

    private null raaNo;
*/
    private String fareType;

  /*  private null onRetRoundFare;

    private null baseFareReceived;

    private null binDiscount;

  */  private String paramExtra;

//    private null bgCode;

    private Id id;
/*

    private null totalRecievedCommisionable;

    private null mlCode;

    private null markedAmount;

    private null airRefundStatus;

    private null tdsOnCommission;

    private null originalTaxDesc;

    private null edmNo;

    private null taxDesc;

    private null airlineDiscount;

    private null markedType;

    private null paidByCurrency;

    private null spPnr;
*/

    private String tktBookingStatusFare;

//    private null reconciliationDate;

    private String param3Received;

  /*  private null passengerNoForFare;
*/
    private String ticketNo;

    private String totalParamsRecieved;

    private String totalTax;

    public String getParam4Received ()
    {
        return param4Received;
    }

    public void setParam4Received (String param4Received)
    {
        this.param4Received = param4Received;
    }

    public String getFareBasisCode ()
    {
        return fareBasisCode;
    }

    public void setFareBasisCode (String fareBasisCode)
    {
        this.fareBasisCode = fareBasisCode;
    }

/*
    public null getServiceTax ()
    {
        return serviceTax;
    }

    public void setServiceTax (null serviceTax)
    {
        this.serviceTax = serviceTax;
    }
*/

    public String getBaseFare ()
    {
        return baseFare;
    }

    public void setBaseFare (String baseFare)
    {
        this.baseFare = baseFare;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    /*
        public null getTax2 ()
        {
            return tax2;
        }

        public void setTax2 (null tax2)
        {
            this.tax2 = tax2;
        }

        public null getTax1 ()
        {
            return tax1;
        }

        public void setTax1 (null tax1)
        {
            this.tax1 = tax1;
        }

        public null getExchangeRate ()
        {
            return exchangeRate;
        }

        public void setExchangeRate (null exchangeRate)
        {
            this.exchangeRate = exchangeRate;
        }

    */
    public String getParam2Received ()
    {
        return param2Received;
    }

    public void setParam2Received (String param2Received)
    {
        this.param2Received = param2Received;
    }

    public String getTax3 ()
    {
        return tax3;
    }

    public void setTax3 (String tax3)
    {
        this.tax3 = tax3;
    }
/*

    public null getLccRetRound ()
    {
        return lccRetRound;
    }

    public void setLccRetRound (null lccRetRound)
    {
        this.lccRetRound = lccRetRound;
    }

    public null getAirlineTransactionFee ()
    {
        return airlineTransactionFee;
    }

    public void setAirlineTransactionFee (null airlineTransactionFee)
    {
        this.airlineTransactionFee = airlineTransactionFee;
    }

    public null getPaidAmountCurrency ()
    {
        return paidAmountCurrency;
    }

    public void setPaidAmountCurrency (null paidAmountCurrency)
    {
        this.paidAmountCurrency = paidAmountCurrency;
    }

    public null getIrctcDiscount ()
    {
        return irctcDiscount;
    }

    public void setIrctcDiscount (null irctcDiscount)
    {
        this.irctcDiscount = irctcDiscount;
    }

*/
    public String getAirlinePnr ()
    {
        return airlinePnr;
    }

    public void setAirlinePnr (String airlinePnr)
    {
        this.airlinePnr = airlinePnr;
    }

    public String getParam5Received ()
    {
        return param5Received;
    }

    public void setParam5Received (String param5Received)
    {
        this.param5Received = param5Received;
    }

/*
    public null getPassengerTicketType ()
    {
        return passengerTicketType;
    }

    public void setPassengerTicketType (null passengerTicketType)
    {
        this.passengerTicketType = passengerTicketType;
    }

    public null getReconciled ()
    {
        return reconciled;
    }

    public void setReconciled (null reconciled)
    {
        this.reconciled = reconciled;
    }

    public null getFfn ()
    {
        return ffn;
    }

    public void setFfn (null ffn)
    {
        this.ffn = ffn;
    }

    public null getMlPrice ()
    {
        return mlPrice;
    }

    public void setMlPrice (null mlPrice)
    {
        this.mlPrice = mlPrice;
    }

    public null getCommission ()
    {
        return commission;
    }

    public void setCommission (null commission)
    {
        this.commission = commission;
    }
*/

    public String getParam1Received ()
    {
        return param1Received;
    }

    public void setParam1Received (String param1Received)
    {
        this.param1Received = param1Received;
    }

    public String getSubTotalFare ()
    {
        return subTotalFare;
    }

    public void setSubTotalFare (String subTotalFare)
    {
        this.subTotalFare = subTotalFare;
    }

/*
    public null getSegmentNoForFare ()
    {
        return segmentNoForFare;
    }

    public void setSegmentNoForFare (null segmentNoForFare)
    {
        this.segmentNoForFare = segmentNoForFare;
    }

    public null getBgPrice ()
    {
        return bgPrice;
    }

    public void setBgPrice (null bgPrice)
    {
        this.bgPrice = bgPrice;
    }

    public null getBaseFareDiscount ()
    {
        return baseFareDiscount;
    }

    public void setBaseFareDiscount (null baseFareDiscount)
    {
        this.baseFareDiscount = baseFareDiscount;
    }

    public null getRaaNo ()
    {
        return raaNo;
    }

    public void setRaaNo (null raaNo)
    {
        this.raaNo = raaNo;
    }
*/

    public String getFareType ()
    {
        return fareType;
    }

    public void setFareType (String fareType)
    {
        this.fareType = fareType;
    }

/*
    public null getOnRetRoundFare ()
    {
        return onRetRoundFare;
    }

    public void setOnRetRoundFare (null onRetRoundFare)
    {
        this.onRetRoundFare = onRetRoundFare;
    }

    public null getBaseFareReceived ()
    {
        return baseFareReceived;
    }

    public void setBaseFareReceived (null baseFareReceived)
    {
        this.baseFareReceived = baseFareReceived;
    }

    public null getBinDiscount ()
    {
        return binDiscount;
    }

    public void setBinDiscount (null binDiscount)
    {
        this.binDiscount = binDiscount;
    }

*/
    public String getParamExtra ()
    {
        return paramExtra;
    }

    public void setParamExtra (String paramExtra)
    {
        this.paramExtra = paramExtra;
    }
/*

    public null getBgCode ()
    {
        return bgCode;
    }

    public void setBgCode (null bgCode)
    {
        this.bgCode = bgCode;
    }

*/
    public Id getId ()
    {
        return id;
    }

    public void setId (Id id)
    {
        this.id = id;
    }
/*

    public null getTotalRecievedCommisionable ()
    {
        return totalRecievedCommisionable;
    }

    public void setTotalRecievedCommisionable (null totalRecievedCommisionable)
    {
        this.totalRecievedCommisionable = totalRecievedCommisionable;
    }

    public null getMlCode ()
    {
        return mlCode;
    }

    public void setMlCode (null mlCode)
    {
        this.mlCode = mlCode;
    }

    public null getMarkedAmount ()
    {
        return markedAmount;
    }

    public void setMarkedAmount (null markedAmount)
    {
        this.markedAmount = markedAmount;
    }

    public null getAirRefundStatus ()
    {
        return airRefundStatus;
    }

    public void setAirRefundStatus (null airRefundStatus)
    {
        this.airRefundStatus = airRefundStatus;
    }

    public null getTdsOnCommission ()
    {
        return tdsOnCommission;
    }

    public void setTdsOnCommission (null tdsOnCommission)
    {
        this.tdsOnCommission = tdsOnCommission;
    }

    public null getOriginalTaxDesc ()
    {
        return originalTaxDesc;
    }

    public void setOriginalTaxDesc (null originalTaxDesc)
    {
        this.originalTaxDesc = originalTaxDesc;
    }

    public null getEdmNo ()
    {
        return edmNo;
    }

    public void setEdmNo (null edmNo)
    {
        this.edmNo = edmNo;
    }

    public null getTaxDesc ()
    {
        return taxDesc;
    }

    public void setTaxDesc (null taxDesc)
    {
        this.taxDesc = taxDesc;
    }

    public null getAirlineDiscount ()
    {
        return airlineDiscount;
    }

    public void setAirlineDiscount (null airlineDiscount)
    {
        this.airlineDiscount = airlineDiscount;
    }

    public null getMarkedType ()
    {
        return markedType;
    }

    public void setMarkedType (null markedType)
    {
        this.markedType = markedType;
    }

    public null getPaidByCurrency ()
    {
        return paidByCurrency;
    }

    public void setPaidByCurrency (null paidByCurrency)
    {
        this.paidByCurrency = paidByCurrency;
    }

    public null getSpPnr ()
    {
        return spPnr;
    }

    public void setSpPnr (null spPnr)
    {
        this.spPnr = spPnr;
    }

*/
    public String getTktBookingStatusFare ()
    {
        return tktBookingStatusFare;
    }

    public void setTktBookingStatusFare (String tktBookingStatusFare)
    {
        this.tktBookingStatusFare = tktBookingStatusFare;
    }

/*
    public null getReconciliationDate ()
    {
        return reconciliationDate;
    }

    public void setReconciliationDate (null reconciliationDate)
    {
        this.reconciliationDate = reconciliationDate;
    }

*/
    public String getParam3Received ()
    {
        return param3Received;
    }

    public void setParam3Received (String param3Received)
    {
        this.param3Received = param3Received;
    }

/*
    public null getPassengerNoForFare ()
    {
        return passengerNoForFare;
    }

    public void setPassengerNoForFare (null passengerNoForFare)
    {
        this.passengerNoForFare = passengerNoForFare;
    }
*/

    public String getTicketNo ()
    {
        return ticketNo;
    }

    public void setTicketNo (String ticketNo)
    {
        this.ticketNo = ticketNo;
    }

    public String getTotalParamsRecieved ()
    {
        return totalParamsRecieved;
    }

    public void setTotalParamsRecieved (String totalParamsRecieved)
    {
        this.totalParamsRecieved = totalParamsRecieved;
    }

    public String getTotalTax ()
    {
        return totalTax;
    }

    public void setTotalTax (String totalTax)
    {
        this.totalTax = totalTax;
    }

   /* @Override
    public String toString()
    {
        return "ClassPojo [param4Received = "+param4Received+", fareBasisCode = "+fareBasisCode+", serviceTax = "+serviceTax+", baseFare = "+baseFare+", tax2 = "+tax2+", tax1 = "+tax1+", exchangeRate = "+exchangeRate+", param2Received = "+param2Received+", tax3 = "+tax3+", lccRetRound = "+lccRetRound+", airlineTransactionFee = "+airlineTransactionFee+", paidAmountCurrency = "+paidAmountCurrency+", irctcDiscount = "+irctcDiscount+", airlinePnr = "+airlinePnr+", param5Received = "+param5Received+", passengerTicketType = "+passengerTicketType+", reconciled = "+reconciled+", ffn = "+ffn+", mlPrice = "+mlPrice+", commission = "+commission+", param1Received = "+param1Received+", subTotalFare = "+subTotalFare+", segmentNoForFare = "+segmentNoForFare+", bgPrice = "+bgPrice+", baseFareDiscount = "+baseFareDiscount+", raaNo = "+raaNo+", fareType = "+fareType+", onRetRoundFare = "+onRetRoundFare+", baseFareReceived = "+baseFareReceived+", binDiscount = "+binDiscount+", paramExtra = "+paramExtra+", bgCode = "+bgCode+", id = "+id+", totalRecievedCommisionable = "+totalRecievedCommisionable+", mlCode = "+mlCode+", markedAmount = "+markedAmount+", airRefundStatus = "+airRefundStatus+", tdsOnCommission = "+tdsOnCommission+", originalTaxDesc = "+originalTaxDesc+", edmNo = "+edmNo+", taxDesc = "+taxDesc+", airlineDiscount = "+airlineDiscount+", markedType = "+markedType+", paidByCurrency = "+paidByCurrency+", spPnr = "+spPnr+", tktBookingStatusFare = "+tktBookingStatusFare+", reconciliationDate = "+reconciliationDate+", param3Received = "+param3Received+", passengerNoForFare = "+passengerNoForFare+", ticketNo = "+ticketNo+", totalParamsRecieved = "+totalParamsRecieved+", totalTax = "+totalTax+"]";
    }*/
}

			