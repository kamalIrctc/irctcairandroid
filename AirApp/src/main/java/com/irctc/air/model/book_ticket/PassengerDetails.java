package com.irctc.air.model.book_ticket;

/**
 * Created by Rajnikant Kumar on 8/31/2018.
 */

public class PassengerDetails {
    String firstName;
    String lastName;
    String gender;
    String dob;
    String titleType;
    PassportInfo passportInfo;
    String passengerType;
    public PassportInfo getPassportinfo() {
        return passportInfo;
    }

    public void setPassportinfo(PassportInfo passportinfo) {
        this.passportInfo = passportinfo;
    }

    public String getTitleType() {
        return titleType;
    }

    public void setTitleType(String titleType) {
        this.titleType = titleType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPassengerType() {
        return passengerType;
    }

    public void setPassengerType(String passengerType) {
        this.passengerType = passengerType;
    }
}
