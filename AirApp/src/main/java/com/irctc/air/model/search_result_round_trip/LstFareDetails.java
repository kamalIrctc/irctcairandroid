package com.irctc.air.model.search_result_round_trip;

public class LstFareDetails
{
    private String paxType;

    //private null difference;

    private String total;

    //private null originalTotalFare;

    private String baseFare;

    private String flightChangePenalty;

    private String tax;

    private String baseType;

    private String cancellationFee;

    private String flightCancelPenalty;

    public String getPaxType ()
    {
        return paxType;
    }

    public void setPaxType (String paxType)
    {
        this.paxType = paxType;
    }

    /*public null getDifference ()
    {
        return difference;
    }

    public void setDifference (null difference)
    {
        this.difference = difference;
    }
*/
    public String getTotal ()
    {
        return total;
    }

    public void setTotal (String total)
    {
        this.total = total;
    }

  /*  public null getOriginalTotalFare ()
    {
        return originalTotalFare;
    }

    public void setOriginalTotalFare (null originalTotalFare)
    {
        this.originalTotalFare = originalTotalFare;
    }
*/
    public String getBaseFare ()
    {
        return baseFare;
    }

    public void setBaseFare (String baseFare)
    {
        this.baseFare = baseFare;
    }

    public String getFlightChangePenalty ()
    {
        return flightChangePenalty;
    }

    public void setFlightChangePenalty (String flightChangePenalty)
    {
        this.flightChangePenalty = flightChangePenalty;
    }

    public String getTax ()
    {
        return tax;
    }

    public void setTax (String tax)
    {
        this.tax = tax;
    }

    public String getBaseType ()
    {
        return baseType;
    }

    public void setBaseType (String baseType)
    {
        this.baseType = baseType;
    }

    public String getCancellationFee ()
    {
        return cancellationFee;
    }

    public void setCancellationFee (String cancellationFee)
    {
        this.cancellationFee = cancellationFee;
    }

    public String getFlightCancelPenalty ()
    {
        return flightCancelPenalty;
    }

    public void setFlightCancelPenalty (String flightCancelPenalty)
    {
        this.flightCancelPenalty = flightCancelPenalty;
    }

    @Override
    public String toString()
    {
        //, difference = "+difference+"
        //, originalTotalFare = "+originalTotalFare+"
        return "ClassPojo [paxType = "+paxType+", total = "+total+", baseFare = "+baseFare+", flightChangePenalty = "+flightChangePenalty+", tax = "+tax+", baseType = "+baseType+", cancellationFee = "+cancellationFee+", flightCancelPenalty = "+flightCancelPenalty+"]";
    }
}

			