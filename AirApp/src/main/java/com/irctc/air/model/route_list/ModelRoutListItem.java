package com.irctc.air.model.route_list;

import android.text.TextUtils;

import com.irctc.air.fragment.FragmentOneWayFlight;
import com.irctc.air.model.passenger_list.ModelPassengerListItem;
import com.irctc.air.round.trip.domastic.model.LstBaggageDetails;

import java.util.ArrayList;

/**
 * Created by Rajnikant Kumar on 8/29/2018.
 */

public class ModelRoutListItem {
    String flightCode;
    String fromAirportCode;
    String fromAirportName;
    String fromAirportTerminal;
    String toAirportCode;
    String toAirportName;
    String toAirportTerminal;
    String travelDuration;
    String departureDate;
    String departureTime;
    String arrivalDate;
    String arrivalTime;
    String halt;
    String carrear;
    String pnr;
    String serviceProvider;
    String tarvelClass;
    String departureTimeAnd;
    String arrivalTimeAnd;
    String departureTimeOnlyAnd;
    String arrivalTimeOnlyAnd;
    String departureDateOnlyAnd;
    String arrivalDateOnlyAnd;
    LstBaggageDetails lstBaggageDetails;
    private boolean isFreeMeal;
    private boolean isBaggageAllowed;

    public String getTarvelClass() {
        return tarvelClass;
    }

    public void setTarvelClass(String tarvelClass) {
        this.tarvelClass = tarvelClass;
    }
    ArrayList<ModelPassengerListItem> passengersList;

    public String getServiceProvider() {
        if (!TextUtils.isEmpty(serviceProvider)){
            return serviceProvider;
        }
        return "";
    }


    public void setServiceProvider(String serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public ArrayList<ModelPassengerListItem> getPassengersList() {
        return passengersList;
    }

    public void setPassengersList(ArrayList<ModelPassengerListItem> passengersList) {
        this.passengersList = passengersList;
    }

    public String getHalt() {
        return halt;
    }

    public void setHalt(String halt) {
        this.halt = halt;
    }

    public String getFromAirportCode() {
        return fromAirportCode;
    }

    public void setFromAirportCode(String fromAirportCode) {
        this.fromAirportCode = fromAirportCode;
    }

    public String getFromAirportName() {
        return fromAirportName;
    }

    public void setFromAirportName(String fromAirportName) {
        this.fromAirportName = fromAirportName;
    }

    public String getFromAirportTerminal() {
        return fromAirportTerminal;
    }

    public void setFromAirportTerminal(String fromAirportTerminal) {
        this.fromAirportTerminal = fromAirportTerminal;
    }

    public String getToAirportCode() {
        return toAirportCode;
    }

    public void setToAirportCode(String toAirportCode) {
        this.toAirportCode = toAirportCode;
    }

    public String getToAirportName() {
        return toAirportName;
    }

    public void setToAirportName(String toAirportName) {
        this.toAirportName = toAirportName;
    }

    public String getToAirportTerminal() {
        return toAirportTerminal;
    }

    public void setToAirportTerminal(String toAirportTerminal) {
        this.toAirportTerminal = toAirportTerminal;
    }

    public String getTravelDuration() {
        return travelDuration;
    }

    public void setTravelDuration(String travelDuration) {
        this.travelDuration = travelDuration;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getFlightCode() {
        return flightCode;
    }

    public void setFlightCode(String flightCode) {
        this.flightCode = flightCode;
    }

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public String getCarrear() {
        return carrear;
    }

    public void setCarrear(String carrear) {
        this.carrear = carrear;
    }

    public String getDepartureTimeAnd() {
        return departureTimeAnd;
    }

    public void setDepartureTimeAnd(String departureTimeAnd) {
        this.departureTimeAnd = departureTimeAnd;
    }

    public String getArrivalTimeAnd() {
        return arrivalTimeAnd;
    }

    public void setArrivalTimeAnd(String arrivalTimeAnd) {
        this.arrivalTimeAnd = arrivalTimeAnd;
    }

    public String setDepartureTimeOnlyAnd(String text) {
        if (!TextUtils.isEmpty(text)){
            return  this.departureTimeOnlyAnd = text.split(", ")[0];
        }
        return "";
    }

    public String setArrivalTimeOnlyAnd(String text) {
        if (!TextUtils.isEmpty(text)){
            return  this.arrivalTimeOnlyAnd = text.split(", ")[0];
        }
        return "";
    }

    public String setDepartureDateOnlyAnd(String text) {
        if (!TextUtils.isEmpty(text)){
            return  this.departureDateOnlyAnd = text.split(", ")[1];
        }
        return "";
    }


    public String setArrivalDateOnlyAnd(String text) {
        if (!TextUtils.isEmpty(text)){
            return  this.arrivalDateOnlyAnd = text.split(", ")[1];
        }
        return "";
    }

    public boolean isFreeMeal() {
        return isFreeMeal;
    }

    public void setFreeMeal(boolean freeMeal) {
        isFreeMeal = freeMeal;
    }

    public boolean isBaggageAllowed() {
        return isBaggageAllowed;
    }

    public void setBaggageAllowed(boolean baggageAllowed) {
        isBaggageAllowed = baggageAllowed;
    }

    public String getDepartureTimeOnlyAnd() {
        return departureTimeOnlyAnd;
    }

    public String getArrivalTimeOnlyAnd() {
        return arrivalTimeOnlyAnd;
    }

    public String getDepartureDateOnlyAnd() {
        return departureDateOnlyAnd;
    }

    public String getArrivalDateOnlyAnd() {
        return arrivalDateOnlyAnd;
    }

    public LstBaggageDetails getLstBaggageDetails() {
        return lstBaggageDetails;
    }

    public void setLstBaggageDetails(LstBaggageDetails lstBaggageDetails) {
        this.lstBaggageDetails = lstBaggageDetails;
    }
}
