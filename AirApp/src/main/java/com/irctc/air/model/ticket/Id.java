package com.irctc.air.model.ticket;

public class Id
{
    private String passengerNo;

    private String oid;

    public String getPassengerNo ()
    {
        return passengerNo;
    }

    public void setPassengerNo (String passengerNo)
    {
        this.passengerNo = passengerNo;
    }

    public String getOid ()
    {
        return oid;
    }

    public void setOid (String oid)
    {
        this.oid = oid;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [passengerNo = "+passengerNo+", oid = "+oid+"]";
    }
}

			