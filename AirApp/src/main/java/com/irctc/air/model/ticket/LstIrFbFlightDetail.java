package com.irctc.air.model.ticket;

public class LstIrFbFlightDetail
{
    private String via;

    /*private null segmentsNo;

    private null lccRetRoundFlight;
*/
    private String onRetRound;

    private String operatingAirlineName;

    private String operatingAirline;

    private Id id;

    private String deptTermial;

  /*  private null segmentCount;
*/
    private String flightNo;

  /*  private null plattingCarrier;
*/
    private String airline;

    private String cabinClass;

    private String segmentOrigin;

  //  private null spNumber;

    private String departureTime;

    //private null baggageAllowance;

//    private null fareClass;

    private String duration;

    private String arrivalTime;

    private String arrivalTerminal;

    private String segmentDestination;

    private String airlinePnrNo;

    private String spPnrNo;

//    private null segmentType;

    public String getVia ()
    {
        return via;
    }

    public void setVia (String via)
    {
        this.via = via;
    }

  /*  public null getSegmentsNo ()
    {
        return segmentsNo;
    }

    public void setSegmentsNo (null segmentsNo)
    {
        this.segmentsNo = segmentsNo;
    }

    public null getLccRetRoundFlight ()
    {
        return lccRetRoundFlight;
    }

    public void setLccRetRoundFlight (null lccRetRoundFlight)
    {
        this.lccRetRoundFlight = lccRetRoundFlight;
    }
*/
    public String getOnRetRound ()
    {
        return onRetRound;
    }

    public void setOnRetRound (String onRetRound)
    {
        this.onRetRound = onRetRound;
    }

    public String getOperatingAirlineName ()
    {
        return operatingAirlineName;
    }

    public void setOperatingAirlineName (String operatingAirlineName)
    {
        this.operatingAirlineName = operatingAirlineName;
    }

    public String getOperatingAirline ()
    {
        return operatingAirline;
    }

    public void setOperatingAirline (String operatingAirline)
    {
        this.operatingAirline = operatingAirline;
    }

    public Id getId ()
    {
        return id;
    }

    public void setId (Id id)
    {
        this.id = id;
    }

    public String getDeptTermial ()
    {
        return deptTermial;
    }

    public void setDeptTermial (String deptTermial)
    {
        this.deptTermial = deptTermial;
    }

  /*  public null getSegmentCount ()
    {
        return segmentCount;
    }

    public void setSegmentCount (null segmentCount)
    {
        this.segmentCount = segmentCount;
    }
*/
    public String getFlightNo ()
    {
        return flightNo;
    }

    public void setFlightNo (String flightNo)
    {
        this.flightNo = flightNo;
    }

  /*  public null getPlattingCarrier ()
    {
        return plattingCarrier;
    }

    public void setPlattingCarrier (null plattingCarrier)
    {
        this.plattingCarrier = plattingCarrier;
    }
*/
    public String getAirline ()
    {
        return airline;
    }

    public void setAirline (String airline)
    {
        this.airline = airline;
    }

    public String getCabinClass ()
    {
        return cabinClass;
    }

    public void setCabinClass (String cabinClass)
    {
        this.cabinClass = cabinClass;
    }

    public String getSegmentOrigin ()
    {
        return segmentOrigin;
    }

    public void setSegmentOrigin (String segmentOrigin)
    {
        this.segmentOrigin = segmentOrigin;
    }

  /*  public null getSpNumber ()
    {
        return spNumber;
    }

    public void setSpNumber (null spNumber)
    {
        this.spNumber = spNumber;
    }
*/
    public String getDepartureTime ()
    {
        return departureTime;
    }

    public void setDepartureTime (String departureTime)
    {
        this.departureTime = departureTime;
    }

  /*  public null getBaggageAllowance ()
    {
        return baggageAllowance;
    }

    public void setBaggageAllowance (null baggageAllowance)
    {
        this.baggageAllowance = baggageAllowance;
    }

    public null getFareClass ()
    {
        return fareClass;
    }

    public void setFareClass (null fareClass)
    {
        this.fareClass = fareClass;
    }

  */  public String getDuration ()
    {
        return duration;
    }

    public void setDuration (String duration)
    {
        this.duration = duration;
    }

    public String getArrivalTime ()
    {
        return arrivalTime;
    }

    public void setArrivalTime (String arrivalTime)
    {
        this.arrivalTime = arrivalTime;
    }

    public String getArrivalTerminal ()
    {
        return arrivalTerminal;
    }

    public void setArrivalTerminal (String arrivalTerminal)
    {
        this.arrivalTerminal = arrivalTerminal;
    }

    public String getSegmentDestination ()
    {
        return segmentDestination;
    }

    public void setSegmentDestination (String segmentDestination)
    {
        this.segmentDestination = segmentDestination;
    }

    public String getAirlinePnrNo ()
    {
        return airlinePnrNo;
    }

    public void setAirlinePnrNo (String airlinePnrNo)
    {
        this.airlinePnrNo = airlinePnrNo;
    }

    public String getSpPnrNo ()
    {
        return spPnrNo;
    }

    public void setSpPnrNo (String spPnrNo)
    {
        this.spPnrNo = spPnrNo;
    }

/*
    public null getSegmentType ()
    {
        return segmentType;
    }

    public void setSegmentType (null segmentType)
    {
        this.segmentType = segmentType;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [via = "+via+", segmentsNo = "+segmentsNo+", lccRetRoundFlight = "+lccRetRoundFlight+", onRetRound = "+onRetRound+", operatingAirlineName = "+operatingAirlineName+", operatingAirline = "+operatingAirline+", id = "+id+", deptTermial = "+deptTermial+", segmentCount = "+segmentCount+", flightNo = "+flightNo+", plattingCarrier = "+plattingCarrier+", airline = "+airline+", cabinClass = "+cabinClass+", segmentOrigin = "+segmentOrigin+", spNumber = "+spNumber+", departureTime = "+departureTime+", baggageAllowance = "+baggageAllowance+", fareClass = "+fareClass+", duration = "+duration+", arrivalTime = "+arrivalTime+", arrivalTerminal = "+arrivalTerminal+", segmentDestination = "+segmentDestination+", airlinePnrNo = "+airlinePnrNo+", spPnrNo = "+spPnrNo+", segmentType = "+segmentType+"]";
    }
*/
}

			