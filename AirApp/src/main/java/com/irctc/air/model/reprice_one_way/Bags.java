package com.irctc.air.model.reprice_one_way;

public class Bags
{
    //private null paxType;

    private String baggage;

    //private null index;

    private String price;

    private String dest;

    private String deptDate;

    //private null paxNum;

    private String origin;

    private String flightNumber;

    //private null i;

    /*public null getPaxType ()
    {
        return paxType;
    }

    public void setPaxType (null paxType)
    {
        this.paxType = paxType;
    }
*/
    public String getBaggage ()
    {
        return baggage;
    }

    public void setBaggage (String baggage)
    {
        this.baggage = baggage;
    }

  /*  public null getIndex ()
    {
        return index;
    }

    public void setIndex (null index)
    {
        this.index = index;
    }
*/
    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }

    public String getDest ()
    {
        return dest;
    }

    public void setDest (String dest)
    {
        this.dest = dest;
    }

    public String getDeptDate ()
    {
        return deptDate;
    }

    public void setDeptDate (String deptDate)
    {
        this.deptDate = deptDate;
    }

/*
    public null getPaxNum ()
    {
        return paxNum;
    }

    public void setPaxNum (null paxNum)
    {
        this.paxNum = paxNum;
    }
*/

    public String getOrigin ()
    {
        return origin;
    }

    public void setOrigin (String origin)
    {
        this.origin = origin;
    }

    public String getFlightNumber ()
    {
        return flightNumber;
    }

    public void setFlightNumber (String flightNumber)
    {
        this.flightNumber = flightNumber;
    }

/*
    public null getI ()
    {
        return i;
    }

    public void setI (null i)
    {
        this.i = i;
    }
*/

    @Override
    public String toString()
    {
        //paxType = "+paxType+",
        //, index = "+index+"
        //, paxNum = "+paxNum+"
        //, i = "+i+"
        return "ClassPojo [baggage = "+baggage+", price = "+price+", dest = "+dest+", deptDate = "+deptDate+", origin = "+origin+", flightNumber = "+flightNumber+"]";
    }
}

			