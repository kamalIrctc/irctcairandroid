package com.irctc.air.model.search_result_round_trip;

public class PojoRoundTrip
{
    private String message;

    private String status;

    private Data data;

  /*  private null userDetails;*/

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public Data getData ()
    {
        return data;
    }

    public void setData (Data data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        //, userDetails = "+userDetails+"
        return "ClassPojo [message = "+message+", status = "+status+", data = "+data+"]";
    }
}

			