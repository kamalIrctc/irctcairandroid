package com.irctc.air.model;

import java.util.ArrayList;

/**
 * Created by tourism on 6/22/2016.
 */
public class ServerBookedCanceledHistoryBean {

    // BOOKED HISTORY DETAIL ARRAYLIST
    ArrayList<HistoryBean> serverBookedCanceledHistory = new ArrayList<>();

    public ArrayList<HistoryBean> getServerBookedCanceledHistory() {
        return serverBookedCanceledHistory;
    }

    public void setServerBookedCanceledHistory(HistoryBean bookedCancelHistory) {
        this.serverBookedCanceledHistory.add(bookedCancelHistory);
    }
}
