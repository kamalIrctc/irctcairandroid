package com.irctc.air.model.search_result_one_way;

public class Segments
{
    private String flightTime;

    //private null originTerminal;

    private String journeySellKey;

    private String originAirportName;

    private String departureDate;

    private String origin;

    private String departureTime;

    private String flightNumber;

    //private null destinationTerminal;

    private String destination;

    private String arrivalTime;

    private String airlineCode;

    private String destinationAirportName;

    private String fareSellKey;

    private String arrivalDate;

    //private null segmentType;

    //private null halt;

    //private null key;

    private String airline;

    public String getFlightTime ()
    {
        return flightTime;
    }

    public void setFlightTime (String flightTime)
    {
        this.flightTime = flightTime;
    }

    /*public null getOriginTerminal ()
    {
        return originTerminal;
    }

    public void setOriginTerminal (null originTerminal)
    {
        this.originTerminal = originTerminal;
    }
*/
    public String getJourneySellKey ()
    {
        return journeySellKey;
    }

    public void setJourneySellKey (String journeySellKey)
    {
        this.journeySellKey = journeySellKey;
    }

    public String getOriginAirportName ()
    {
        return originAirportName;
    }

    public void setOriginAirportName (String originAirportName)
    {
        this.originAirportName = originAirportName;
    }

    public String getDepartureDate ()
    {
        return departureDate;
    }

    public void setDepartureDate (String departureDate)
    {
        this.departureDate = departureDate;
    }

    public String getOrigin ()
    {
        return origin;
    }

    public void setOrigin (String origin)
    {
        this.origin = origin;
    }

    public String getDepartureTime ()
    {
        return departureTime;
    }

    public void setDepartureTime (String departureTime)
    {
        this.departureTime = departureTime;
    }

    public String getFlightNumber ()
    {
        return flightNumber;
    }

    public void setFlightNumber (String flightNumber)
    {
        this.flightNumber = flightNumber;
    }

  /*  public null getDestinationTerminal ()
    {
        return destinationTerminal;
    }

    public void setDestinationTerminal (null destinationTerminal)
    {
        this.destinationTerminal = destinationTerminal;
    }
*/
    public String getDestination ()
    {
        return destination;
    }

    public void setDestination (String destination)
    {
        this.destination = destination;
    }

    public String getArrivalTime ()
    {
        return arrivalTime;
    }

    public void setArrivalTime (String arrivalTime)
    {
        this.arrivalTime = arrivalTime;
    }

    public String getAirlineCode ()
    {
        return airlineCode;
    }

    public void setAirlineCode (String airlineCode)
    {
        this.airlineCode = airlineCode;
    }

    public String getDestinationAirportName ()
    {
        return destinationAirportName;
    }

    public void setDestinationAirportName (String destinationAirportName)
    {
        this.destinationAirportName = destinationAirportName;
    }

    public String getFareSellKey ()
    {
        return fareSellKey;
    }

    public void setFareSellKey (String fareSellKey)
    {
        this.fareSellKey = fareSellKey;
    }

    public String getArrivalDate ()
    {
        return arrivalDate;
    }

    public void setArrivalDate (String arrivalDate)
    {
        this.arrivalDate = arrivalDate;
    }

  /*  public null getSegmentType ()
    {
        return segmentType;
    }

    public void setSegmentType (null segmentType)
    {
        this.segmentType = segmentType;
    }

    public null getHalt ()
    {
        return halt;
    }

    public void setHalt (null halt)
    {
        this.halt = halt;
    }

    public null getKey ()
    {
        return key;
    }

    public void setKey (null key)
    {
        this.key = key;
    }
*/
    public String getAirline ()
    {
        return airline;
    }

    public void setAirline (String airline)
    {
        this.airline = airline;
    }

    @Override
    public String toString()
    {
        //, originTerminal = "+originTerminal+"
        //, segmentType = "+segmentType+", halt = "+halt+", key = "+key+"
        //, destinationTerminal = "+destinationTerminal+"
        return "ClassPojo [flightTime = "+flightTime+", journeySellKey = "+journeySellKey+", originAirportName = "+originAirportName+", departureDate = "+departureDate+", origin = "+origin+", departureTime = "+departureTime+", flightNumber = "+flightNumber+", destination = "+destination+", arrivalTime = "+arrivalTime+", airlineCode = "+airlineCode+", destinationAirportName = "+destinationAirportName+", fareSellKey = "+fareSellKey+", arrivalDate = "+arrivalDate+", airline = "+airline+"]";
    }
}