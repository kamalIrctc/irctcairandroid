package com.irctc.air.model.ticket;

public class Baggages
{
    //private null paxType;

    private String baggage;

    //private null index;

    private String price;

    //private null dest;

    private String description;

    //private null deptDate;

    //private null paxNum;

    //private null origin;

    //private null flightNumber;

    //private null i;
/*

    public null getPaxType ()
    {
        return paxType;
    }

    public void setPaxType (null paxType)
    {
        this.paxType = paxType;
    }
*/

    public String getBaggage ()
    {
        return baggage;
    }

    public void setBaggage (String baggage)
    {
        this.baggage = baggage;
    }
/*

    public null getIndex ()
    {
        return index;
    }

    public void setIndex (null index)
    {
        this.index = index;
    }
*/

    public String getPrice ()
    {
        return price;
    }

    public void setPrice (String price)
    {
        this.price = price;
    }

/*
    public null getDest ()
    {
        return dest;
    }

    public void setDest (null dest)
    {
        this.dest = dest;
    }
*/

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

/*
    public null getDeptDate ()
    {
        return deptDate;
    }

    public void setDeptDate (null deptDate)
    {
        this.deptDate = deptDate;
    }

    public null getPaxNum ()
    {
        return paxNum;
    }

    public void setPaxNum (null paxNum)
    {
        this.paxNum = paxNum;
    }

    public null getOrigin ()
    {
        return origin;
    }

    public void setOrigin (null origin)
    {
        this.origin = origin;
    }

    public null getFlightNumber ()
    {
        return flightNumber;
    }

    public void setFlightNumber (null flightNumber)
    {
        this.flightNumber = flightNumber;
    }

    public null getI ()
    {
        return i;
    }

    public void setI (null i)
    {
        this.i = i;
    }
*/

    @Override
    public String toString()
    {
        //paxType = "+paxType+",
        //, index = "+index+"
        //, dest = "+dest+"
        //, deptDate = "+deptDate+", paxNum = "+paxNum+", origin = "+origin+", flightNumber = "+flightNumber+", i = "+i+"
        return "ClassPojo [baggage = "+baggage+", price = "+price+", description = "+description+"]";
    }
}

	