package com.irctc.air.model.refresh_token;

/**
 * Created by Rajnikant Kumar on 12/26/2018.
 */

public class RefreshTokenModel {
    String status, message, data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
