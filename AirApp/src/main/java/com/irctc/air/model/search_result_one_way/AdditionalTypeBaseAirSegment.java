package com.irctc.air.model.search_result_one_way;

public class AdditionalTypeBaseAirSegment
{
    //private null travelTime;

    private String optionalServicesIndicator;

    //private null elStat;

    private String equipment;

    private CodeshareInfo codeshareInfo;

    //private null marriageGroup;

    //private null numberInParty;

    //private null seamless;

    private String polledAvailabilityOption;

    private String carrier;

    private String key;

    private FlightDetailsRef[] flightDetailsRef;

    //private null providerReservationInfoRef;

    //private null cabinClass;

    //private null apisrequirementsRef;

    private String flightTime;

    //private null connection;

    private String flownSegment;

    private String availabilitySource;

    //private null status;

    //private null hostTokenRef;

    private String departureTime;

    //private null sponsoredFltInfo;

    private String destination;

    private String[] sellMessage;

    //private null passiveProviderReservationInfoRef;

    //private null openSegment;

    //private null passive;

    private String availabilityDisplayType;

    //private null operationalStatus;

    //private null brandIndicator;

    private String[] flightDetails;

    private String[] railCoachDetails;

    private String[] alternateLocationDistanceRef;

    private AirAvailInfo[] airAvailInfo;

    private String flightNumber;

    //private null railCoachNumber;

    private String distance;

    //private null classOfService;

    //private null providerSegmentOrder;

    //private null keyOverride;

    private String scheduleChange;

    //private null bookingDate;

    private String changeOfPlane;

    private String eticketability;

    private String linkAvailability;

    private String origin;

    //private null supplierCode;

    //private null numberOfStops;

    //private null blackListed;

    //private null providerCode;

    //private null travelOrder;

    private String arrivalTime;

    private String participantLevel;

    private String group;

    private String[] segmentRemark;

    //private null guaranteedPaymentCarrier;

    /*public null getTravelTime ()
    {
        return travelTime;
    }

    public void setTravelTime (null travelTime)
    {
        this.travelTime = travelTime;
    }
*/
    public String getOptionalServicesIndicator ()
    {
        return optionalServicesIndicator;
    }

    public void setOptionalServicesIndicator (String optionalServicesIndicator)
    {
        this.optionalServicesIndicator = optionalServicesIndicator;
    }

  /*  public null getElStat ()
    {
        return elStat;
    }

    public void setElStat (null elStat)
    {
        this.elStat = elStat;
    }
*/
    public String getEquipment ()
    {
        return equipment;
    }

    public void setEquipment (String equipment)
    {
        this.equipment = equipment;
    }

    public CodeshareInfo getCodeshareInfo ()
    {
        return codeshareInfo;
    }

    public void setCodeshareInfo (CodeshareInfo codeshareInfo)
    {
        this.codeshareInfo = codeshareInfo;
    }

  /*  public null getMarriageGroup ()
    {
        return marriageGroup;
    }

    public void setMarriageGroup (null marriageGroup)
    {
        this.marriageGroup = marriageGroup;
    }

    public null getNumberInParty ()
    {
        return numberInParty;
    }

    public void setNumberInParty (null numberInParty)
    {
        this.numberInParty = numberInParty;
    }

    public null getSeamless ()
    {
        return seamless;
    }

    public void setSeamless (null seamless)
    {
        this.seamless = seamless;
    }
*/
    public String getPolledAvailabilityOption ()
    {
        return polledAvailabilityOption;
    }

    public void setPolledAvailabilityOption (String polledAvailabilityOption)
    {
        this.polledAvailabilityOption = polledAvailabilityOption;
    }

    public String getCarrier ()
    {
        return carrier;
    }

    public void setCarrier (String carrier)
    {
        this.carrier = carrier;
    }

    public String getKey ()
    {
        return key;
    }

    public void setKey (String key)
    {
        this.key = key;
    }

    public FlightDetailsRef[] getFlightDetailsRef ()
    {
        return flightDetailsRef;
    }

    public void setFlightDetailsRef (FlightDetailsRef[] flightDetailsRef)
    {
        this.flightDetailsRef = flightDetailsRef;
    }

  /*  public null getProviderReservationInfoRef ()
    {
        return providerReservationInfoRef;
    }

    public void setProviderReservationInfoRef (null providerReservationInfoRef)
    {
        this.providerReservationInfoRef = providerReservationInfoRef;
    }

    public null getCabinClass ()
    {
        return cabinClass;
    }

    public void setCabinClass (null cabinClass)
    {
        this.cabinClass = cabinClass;
    }

    public null getApisrequirementsRef ()
    {
        return apisrequirementsRef;
    }

    public void setApisrequirementsRef (null apisrequirementsRef)
    {
        this.apisrequirementsRef = apisrequirementsRef;
    }
*/
    public String getFlightTime ()
    {
        return flightTime;
    }

    public void setFlightTime (String flightTime)
    {
        this.flightTime = flightTime;
    }

  /*  public null getConnection ()
    {
        return connection;
    }

    public void setConnection (null connection)
    {
        this.connection = connection;
    }
*/
    public String getFlownSegment ()
    {
        return flownSegment;
    }

    public void setFlownSegment (String flownSegment)
    {
        this.flownSegment = flownSegment;
    }

    public String getAvailabilitySource ()
    {
        return availabilitySource;
    }

    public void setAvailabilitySource (String availabilitySource)
    {
        this.availabilitySource = availabilitySource;
    }

  /*  public null getStatus ()
    {
        return status;
    }

    public void setStatus (null status)
    {
        this.status = status;
    }

    public null getHostTokenRef ()
    {
        return hostTokenRef;
    }

    public void setHostTokenRef (null hostTokenRef)
    {
        this.hostTokenRef = hostTokenRef;
    }
*/
    public String getDepartureTime ()
    {
        return departureTime;
    }

    public void setDepartureTime (String departureTime)
    {
        this.departureTime = departureTime;
    }

  /*  public null getSponsoredFltInfo ()
    {
        return sponsoredFltInfo;
    }

    public void setSponsoredFltInfo (null sponsoredFltInfo)
    {
        this.sponsoredFltInfo = sponsoredFltInfo;
    }
*/
    public String getDestination ()
    {
        return destination;
    }

    public void setDestination (String destination)
    {
        this.destination = destination;
    }

    public String[] getSellMessage ()
    {
        return sellMessage;
    }

    public void setSellMessage (String[] sellMessage)
    {
        this.sellMessage = sellMessage;
    }

  /*  public null getPassiveProviderReservationInfoRef ()
    {
        return passiveProviderReservationInfoRef;
    }

    public void setPassiveProviderReservationInfoRef (null passiveProviderReservationInfoRef)
    {
        this.passiveProviderReservationInfoRef = passiveProviderReservationInfoRef;
    }

    public null getOpenSegment ()
    {
        return openSegment;
    }

    public void setOpenSegment (null openSegment)
    {
        this.openSegment = openSegment;
    }

    public null getPassive ()
    {
        return passive;
    }

    public void setPassive (null passive)
    {
        this.passive = passive;
    }
*/
    public String getAvailabilityDisplayType ()
    {
        return availabilityDisplayType;
    }

    public void setAvailabilityDisplayType (String availabilityDisplayType)
    {
        this.availabilityDisplayType = availabilityDisplayType;
    }

  /*  public null getOperationalStatus ()
    {
        return operationalStatus;
    }

    public void setOperationalStatus (null operationalStatus)
    {
        this.operationalStatus = operationalStatus;
    }

    public null getBrandIndicator ()
    {
        return brandIndicator;
    }

    public void setBrandIndicator (null brandIndicator)
    {
        this.brandIndicator = brandIndicator;
    }

  */  public String[] getFlightDetails ()
    {
        return flightDetails;
    }

    public void setFlightDetails (String[] flightDetails)
    {
        this.flightDetails = flightDetails;
    }

    public String[] getRailCoachDetails ()
    {
        return railCoachDetails;
    }

    public void setRailCoachDetails (String[] railCoachDetails)
    {
        this.railCoachDetails = railCoachDetails;
    }

    public String[] getAlternateLocationDistanceRef ()
    {
        return alternateLocationDistanceRef;
    }

    public void setAlternateLocationDistanceRef (String[] alternateLocationDistanceRef)
    {
        this.alternateLocationDistanceRef = alternateLocationDistanceRef;
    }

    public AirAvailInfo[] getAirAvailInfo ()
    {
        return airAvailInfo;
    }

    public void setAirAvailInfo (AirAvailInfo[] airAvailInfo)
    {
        this.airAvailInfo = airAvailInfo;
    }

    public String getFlightNumber ()
    {
        return flightNumber;
    }

    public void setFlightNumber (String flightNumber)
    {
        this.flightNumber = flightNumber;
    }

    /*public null getRailCoachNumber ()
    {
        return railCoachNumber;
    }

    public void setRailCoachNumber (null railCoachNumber)
    {
        this.railCoachNumber = railCoachNumber;
    }
*/
    public String getDistance ()
    {
        return distance;
    }

    public void setDistance (String distance)
    {
        this.distance = distance;
    }

  /*  public null getClassOfService ()
    {
        return classOfService;
    }

    public void setClassOfService (null classOfService)
    {
        this.classOfService = classOfService;
    }

    public null getProviderSegmentOrder ()
    {
        return providerSegmentOrder;
    }

    public void setProviderSegmentOrder (null providerSegmentOrder)
    {
        this.providerSegmentOrder = providerSegmentOrder;
    }

    public null getKeyOverride ()
    {
        return keyOverride;
    }

    public void setKeyOverride (null keyOverride)
    {
        this.keyOverride = keyOverride;
    }
*/
    public String getScheduleChange ()
    {
        return scheduleChange;
    }

    public void setScheduleChange (String scheduleChange)
    {
        this.scheduleChange = scheduleChange;
    }

  /*  public null getBookingDate ()
    {
        return bookingDate;
    }

    public void setBookingDate (null bookingDate)
    {
        this.bookingDate = bookingDate;
    }

  */  public String getChangeOfPlane ()
    {
        return changeOfPlane;
    }

    public void setChangeOfPlane (String changeOfPlane)
    {
        this.changeOfPlane = changeOfPlane;
    }

    public String getEticketability ()
    {
        return eticketability;
    }

    public void setEticketability (String eticketability)
    {
        this.eticketability = eticketability;
    }

    public String getLinkAvailability ()
    {
        return linkAvailability;
    }

    public void setLinkAvailability (String linkAvailability)
    {
        this.linkAvailability = linkAvailability;
    }

    public String getOrigin ()
    {
        return origin;
    }

    public void setOrigin (String origin)
    {
        this.origin = origin;
    }

/*
    public null getSupplierCode ()
    {
        return supplierCode;
    }

    public void setSupplierCode (null supplierCode)
    {
        this.supplierCode = supplierCode;
    }

    public null getNumberOfStops ()
    {
        return numberOfStops;
    }

    public void setNumberOfStops (null numberOfStops)
    {
        this.numberOfStops = numberOfStops;
    }

    public null getBlackListed ()
    {
        return blackListed;
    }

    public void setBlackListed (null blackListed)
    {
        this.blackListed = blackListed;
    }

    public null getProviderCode ()
    {
        return providerCode;
    }

    public void setProviderCode (null providerCode)
    {
        this.providerCode = providerCode;
    }

    public null getTravelOrder ()
    {
        return travelOrder;
    }

    public void setTravelOrder (null travelOrder)
    {
        this.travelOrder = travelOrder;
    }
*/

    public String getArrivalTime ()
    {
        return arrivalTime;
    }

    public void setArrivalTime (String arrivalTime)
    {
        this.arrivalTime = arrivalTime;
    }

    public String getParticipantLevel ()
    {
        return participantLevel;
    }

    public void setParticipantLevel (String participantLevel)
    {
        this.participantLevel = participantLevel;
    }

    public String getGroup ()
    {
        return group;
    }

    public void setGroup (String group)
    {
        this.group = group;
    }

    public String[] getSegmentRemark ()
    {
        return segmentRemark;
    }

    public void setSegmentRemark (String[] segmentRemark)
    {
        this.segmentRemark = segmentRemark;
    }

/*
    public null getGuaranteedPaymentCarrier ()
    {
        return guaranteedPaymentCarrier;
    }

    public void setGuaranteedPaymentCarrier (null guaranteedPaymentCarrier)
    {
        this.guaranteedPaymentCarrier = guaranteedPaymentCarrier;
    }
*/

    @Override
    public String toString()
    {
        //travelTime = "+travelTime+",
        //, elStat = "+elStat+"
        //, marriageGroup = "+marriageGroup+", numberInParty = "+numberInParty+", seamless = "+seamless+"
        // providerReservationInfoRef = "+providerReservationInfoRef+", cabinClass = "+cabinClass+", apisrequirementsRef = "+apisrequirementsRef+",
        //, guaranteedPaymentCarrier = "+guaranteedPaymentCarrier+"
        //, supplierCode = "+supplierCode+", numberOfStops = "+numberOfStops+", blackListed = "+blackListed+", providerCode = "+providerCode+", travelOrder = "+travelOrder+"
        //, bookingDate = "+bookingDate+"
        // classOfService = "+classOfService+", providerSegmentOrder = "+providerSegmentOrder+", keyOverride = "+keyOverride+",
        //, connection = "+connection+"
        // operationalStatus = "+operationalStatus+", brandIndicator = "+brandIndicator+",
        //, status = "+status+", hostTokenRef = "+hostTokenRef+"
        //, railCoachNumber = "+railCoachNumber+"
        // passiveProviderReservationInfoRef = "+passiveProviderReservationInfoRef+", openSegment = "+openSegment+", passive = "+passive+",
        //, sponsoredFltInfo = "+sponsoredFltInfo+"
        return "ClassPojo [optionalServicesIndicator = "+optionalServicesIndicator+", equipment = "+equipment+", codeshareInfo = "+codeshareInfo+", polledAvailabilityOption = "+polledAvailabilityOption+", carrier = "+carrier+", key = "+key+", flightDetailsRef = "+flightDetailsRef+", flightTime = "+flightTime+", flownSegment = "+flownSegment+", availabilitySource = "+availabilitySource+", departureTime = "+departureTime+", destination = "+destination+", sellMessage = "+sellMessage+", availabilityDisplayType = "+availabilityDisplayType+", flightDetails = "+flightDetails+", railCoachDetails = "+railCoachDetails+", alternateLocationDistanceRef = "+alternateLocationDistanceRef+", airAvailInfo = "+airAvailInfo+", flightNumber = "+flightNumber+", distance = "+distance+", scheduleChange = "+scheduleChange+", changeOfPlane = "+changeOfPlane+", eticketability = "+eticketability+", linkAvailability = "+linkAvailability+", origin = "+origin+", arrivalTime = "+arrivalTime+", participantLevel = "+participantLevel+", group = "+group+", segmentRemark = "+segmentRemark+"]";
    }
}