package com.irctc.air.model.ticket;

public class IrFbServiceCharge
{
    private String total;

    private String id;

    private String tax;

    private String percentage;

    private String serviceCharge;

    public String getTotal ()
    {
        return total;
    }

    public void setTotal (String total)
    {
        this.total = total;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getTax ()
    {
        return tax;
    }

    public void setTax (String tax)
    {
        this.tax = tax;
    }

    public String getPercentage ()
    {
        return percentage;
    }

    public void setPercentage (String percentage)
    {
        this.percentage = percentage;
    }

    public String getServiceCharge ()
    {
        return serviceCharge;
    }

    public void setServiceCharge (String serviceCharge)
    {
        this.serviceCharge = serviceCharge;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [total = "+total+", id = "+id+", tax = "+tax+", percentage = "+percentage+", serviceCharge = "+serviceCharge+"]";
    }
}

			