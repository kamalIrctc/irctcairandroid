package com.irctc.air.model.flight_icon;

public class PojoFlightIcon
{
    private String message;

    private String status;

    private String data;

    /*private null userDetails;*/

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getData ()
    {
        return data;
    }

    public void setData (String data)
    {
        this.data = data;
    }

/*
    public null getUserDetails ()
    {
        return userDetails;
    }

    public void setUserDetails (null userDetails)
    {
        this.userDetails = userDetails;
    }
*/

    @Override
    public String toString()
    {
        //, userDetails = "+userDetails+"
        return "ClassPojo [message = "+message+", status = "+status+", data = "+data+"]";
    }
}

			