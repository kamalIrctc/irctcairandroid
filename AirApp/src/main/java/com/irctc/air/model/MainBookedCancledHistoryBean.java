package com.irctc.air.model;

import java.util.ArrayList;

/**
 * Created by tourism on 5/14/2016.
 */
public class MainBookedCancledHistoryBean {

    // BOOKED HISTORY DETAIL ARRAYLIST
    ArrayList<BookingHistoryBean> bookedHistory = new ArrayList<>();
    // CANCELLATION HISTORY DETAIL ARRAYLIST
    ArrayList<BookingHistoryBean> cancelHistory = new ArrayList<>();

    public ArrayList<BookingHistoryBean> getBookedHistory() {
        return bookedHistory;
    }

    public void setBookedHistory(BookingHistoryBean bookedHistory) {
        this.bookedHistory.add(bookedHistory);
    }

    public ArrayList<BookingHistoryBean> getCancelHistory() {
        return cancelHistory;
    }

    public void setCancelHistory(BookingHistoryBean cancelHistory) {
        this.cancelHistory.add(cancelHistory);
    }



}
