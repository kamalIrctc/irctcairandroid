package com.irctc.air.model.reprice_one_way;

public class Data
{
    private LstFareDetails[] lstFareDetails;

    private String issoldOut;

    private Inf[] inf;

    private String isPriceChange;

    private String priceChangeFlag;

    private Bags[] bags;

    private PricingInfo[] pricingInfo;

    private Meal[] meal;

    public LstFareDetails[] getLstFareDetails ()
    {
        return lstFareDetails;
    }

    public void setLstFareDetails (LstFareDetails[] lstFareDetails)
    {
        this.lstFareDetails = lstFareDetails;
    }

    public String getIssoldOut(){
    return issoldOut;
    }

    public void setIssoldOut(String issoldOut){
        this.issoldOut = issoldOut;
    }

    public Inf[] getInf ()
    {
        return inf;
    }

    public void setInf (Inf[] inf)
    {
        this.inf = inf;
    }

    public String getIsPriceChange ()
    {
        return isPriceChange;
    }

    public void setIsPriceChange (String isPriceChange)
    {
        this.isPriceChange = isPriceChange;
    }

    public String getPriceChangeFlag ()
    {
        return priceChangeFlag;
    }

    public void setPriceChangeFlag (String priceChangeFlag)
    {
        this.priceChangeFlag = priceChangeFlag;
    }

    public Bags[] getBags ()
    {
        return bags;
    }

    public void setBags (Bags[] bags)
    {
        this.bags = bags;
    }

    public PricingInfo[] getPricingInfo ()
    {
        return pricingInfo;
    }

    public void setPricingInfo (PricingInfo[] pricingInfo)
    {
        this.pricingInfo = pricingInfo;
    }

    public Meal[] getMeal ()
    {
        return meal;
    }

    public void setMeal (Meal[] meal)
    {
        this.meal = meal;
    }

    @Override
    public String toString()
    {
        //, issoldOut = "+issoldOut+"
        return "ClassPojo [lstFareDetails = "+lstFareDetails+", inf = "+inf+", isPriceChange = "+isPriceChange+", priceChangeFlag = "+priceChangeFlag+", bags = "+bags+", pricingInfo = "+pricingInfo+", meal = "+meal+"]";
    }
}

			