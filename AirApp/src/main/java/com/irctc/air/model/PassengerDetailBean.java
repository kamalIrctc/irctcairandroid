package com.irctc.air.model;

/**
 * Created by vivek on 5/9/2016.
 */
public class PassengerDetailBean {

    private String lname;
    private String candetail;
    private String age;
    private String fname;
    private String canstatus;
    private String canstatusText;
    private String ticketNo;
    private String pasgtype;


    private boolean isSelected;


    public boolean isSelected() {
        return isSelected;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }


    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getCandetail() {
        return candetail;
    }

    public void setCandetail(String candetail) {
        this.candetail = candetail;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getCanstatus() {
        return canstatus;
    }

    public void setCanstatus(String canstatus) {
        this.canstatus = canstatus;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public String getPasgtype() {
        return pasgtype;
    }

    public void setPasgtype(String pasgtype) {
        this.pasgtype = pasgtype;
    }

    public String getCanstatusText() {
        return canstatusText;
    }

    public void setCanstatusText(String canstatusText) {
        this.canstatusText = canstatusText;
    }
}
