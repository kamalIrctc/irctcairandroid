package com.irctc.air.model;

import java.util.ArrayList;

/**
 * Created by vivek on 5/9/2016.
 */
public class FareQuoteRoundTripOuterBean {


    // ONWARD - ROUND TRIP FARE QUOTE DETAIL ARRAYLIST
    private ArrayList<FareQuoteOneWayBean> alRoundTripOnwardFareQuoteBean = new ArrayList<>();

    // RETURN - ROUND TRIP FARE QUOTE DETAIL ARRAYLIST
    private ArrayList<FareQuoteOneWayBean> alRoundTripReturnFareQuoteBean = new ArrayList<>();


    /***************************************************************************
     ***************************************************************************/
    public ArrayList<FareQuoteOneWayBean> getAlRoundTripOnwardFareQuoteBean() {
        return alRoundTripOnwardFareQuoteBean;
    }

    public void setAlRoundTripOnwardFareQuoteBean(FareQuoteOneWayBean alRoundTripOnwardFareQuoteBean) {
        this.alRoundTripOnwardFareQuoteBean.add(alRoundTripOnwardFareQuoteBean);
    }

    public ArrayList<FareQuoteOneWayBean> getAlRoundTripReturnFareQuoteBean() {
        return alRoundTripReturnFareQuoteBean;
    }

    public void setAlRoundTripReturnFareQuoteBean(FareQuoteOneWayBean alRoundTripReturnFareQuoteBean) {
        this.alRoundTripReturnFareQuoteBean.add(alRoundTripReturnFareQuoteBean);
    }

}
