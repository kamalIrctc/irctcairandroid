package com.irctc.air.model;

/**
 * Created by vivek on 5/9/2016.
 */
public class FareDetailBean {

    private String jn;
    private String bfare;
    private String yr;
    private String yq;
    private String tax1;
    private String subTotal;
    private String extra;
    private String inc;
    private String wo;
    private String tax2;
    private String disc;
    /**
     * 07 NOV 2016
     * IRCTC SERVICE CHARGE
     */
    private double irctcTxnFees;

    public double getIrctcTxnFees() {
        return irctcTxnFees;
    }

    public void setIrctcTxnFees(double irctcTxnFees) {
        this.irctcTxnFees = irctcTxnFees;
    }

    public String getJn() {
        return jn;
    }

    public void setJn(String jn) {
        this.jn = jn;
    }

    public String getBfare() {
        return bfare;
    }

    public void setBfare(String bfare) {
        this.bfare = bfare;
    }

    public String getYr() {
        return yr;
    }

    public void setYr(String yr) {
        this.yr = yr;
    }

    public String getYq() {
        return yq;
    }

    public void setYq(String yq) {
        this.yq = yq;
    }

    public String getTax1() {
        return tax1;
    }

    public void setTax1(String tax1) {
        this.tax1 = tax1;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public String getInc() {
        return inc;
    }

    public void setInc(String inc) {
        this.inc = inc;
    }

    public String getWo() {
        return wo;
    }

    public void setWo(String wo) {
        this.wo = wo;
    }

    public String getTax2() {
        return tax2;
    }

    public void setTax2(String tax2) {
        this.tax2 = tax2;
    }

    public String getDisc() {
        return disc;
    }

    public void setDisc(String disc) {
        this.disc = disc;
    }
}
