package com.irctc.air.model.search_result_one_way;

public class AirAvailInfo
{
    private String[] bookingCodeInfo;

    //private null hostTokenRef;

    private String[] fareTokenInfo;

    private String providerCode;

    public String[] getBookingCodeInfo ()
    {
        return bookingCodeInfo;
    }

    public void setBookingCodeInfo (String[] bookingCodeInfo)
    {
        this.bookingCodeInfo = bookingCodeInfo;
    }

    /*public null getHostTokenRef ()
    {
        return hostTokenRef;
    }

    public void setHostTokenRef (null hostTokenRef)
    {
        this.hostTokenRef = hostTokenRef;
    }
*/
    public String[] getFareTokenInfo ()
    {
        return fareTokenInfo;
    }

    public void setFareTokenInfo (String[] fareTokenInfo)
    {
        this.fareTokenInfo = fareTokenInfo;
    }

    public String getProviderCode ()
    {
        return providerCode;
    }

    public void setProviderCode (String providerCode)
    {
        this.providerCode = providerCode;
    }

    @Override
    public String toString()
    {
        //, hostTokenRef = "+hostTokenRef+"
        return "ClassPojo [bookingCodeInfo = "+bookingCodeInfo+", fareTokenInfo = "+fareTokenInfo+", providerCode = "+providerCode+"]";
    }
}