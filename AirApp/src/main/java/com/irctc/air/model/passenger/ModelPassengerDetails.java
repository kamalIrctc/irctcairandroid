package com.irctc.air.model.passenger;

/**
 * Created by Rajnikant Kumar on 8/30/2018.
 */

public class ModelPassengerDetails {
    String firstName;
    String passengerType;

    public String getPassengerTypeString() {
        return passengerTypeString;
    }

    public void setPassengerTypeString(String passengerTypeString) {
        this.passengerTypeString = passengerTypeString;
    }

    String passengerTypeString;
    String lastName;
    String gender;
    String titleType;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getTitleType() {
        return titleType;
    }

    public void setTitleType(String titleType) {
        this.titleType = titleType;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    String dob;




    public String getPassengerType() {
        return passengerType;
    }

    public void setPassengerType(String passengerType) {
        this.passengerType = passengerType;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

}
