package com.irctc.air.model;

import com.irctc.air.calander.CalenderDate;

import java.util.ArrayList;

public class AirDataModel {

    // Planner screen model //

    // User detail
//    private boolean isLoggedIn = false;
//    private boolean isGuestUser = false;
//    private String userName;
//    private String userAddress;
//
//    private String userLogInId;
//    private String userLogInGuestEmail;
//    private String userLogInGuestMobNum;

    // Search param
    private String fromStation = "";
    private String toStation = "";
    private String fromStationCity = "";
    private String toStationCity = "";


    // If dep date is blank, show current date
    private String depDate = "";
    // If return date is blank, show current date
    private String returnDate = "";

    //One Way, Round Trip -  Multi City we are not using in app,
    private String tripType = "";

    //Domestic or international
    private String tripDomOrInter = "";

    // E or B
    private String travelClass = "";

    // Travelling passengers count
    private int adultPassNum = 0;
    private int childPassNum = 0;
    private int infantPassNum = 0;


    // Flight list bean for 4 type of flights
    private FlightFilterBean mFlightFilterBean = new FlightFilterBean();
//    FlightOnWardDetailBean
//    FlightReturnDetailBean
//    FlightRoundOnWardDetailBean
//    FlightRoundOnWardDetailBean

    // Flag for to check if result coming from cache
    private boolean isFlightOnWardFromCache;
//    isFlightReturnFromCache
//    isFlightRoundOnWardFromCache
//    isFlightRoundOnWardFromCache


    /*************************************
     * ONE WAY TRIP FARE QUOTE VARIABLE
     *************************************/
    // Onward FareQuote JSON Segment
    private String onwFlightJsonSegment;
    // Onward FareQuote XML Segment
    private String onwFlightXMLSegment;
    // Onward FareQuote Security Token
    private String onwFlightSecurityToken;
    // min or max price of Flight Fare
    private int onwFlightFareMin;
    private int onwFlightFareMax;
    // min or max price of Ret Flight Fare
    private int retFlightFareMin;
    private int retFlightFareMax;


    /*************************************
     * CUSTOM CALANDER VARIABLE
     *************************************/

    private CalenderDate firstMonthInfo;
    private CalenderDate lastMonthInfo;

    private String onwardDate;
    private String retDate;

    private boolean isOnwardSelect;
    private int onewayMonthCounter;


    public int getOnewayMonthCounter() {
        return onewayMonthCounter;
    }

    public void setOnewayMonthCounter(int onewayMonthCounter) {
        this.onewayMonthCounter = onewayMonthCounter;
    }



    public CalenderDate getFirstMonthInfo() {
        return firstMonthInfo;
    }

    public void setFirstMonthInfo(CalenderDate firstMonthInfo) {
        this.firstMonthInfo = firstMonthInfo;
    }

    public CalenderDate getLastMonthInfo() {
        return lastMonthInfo;
    }

    public void setLastMonthInfo(CalenderDate lastMonthInfo) {
        this.lastMonthInfo = lastMonthInfo;
    }

    public String getOnwardDate() {
        return onwardDate;
    }

    public void setOnwardDate(String onwardDate) {
        this.onwardDate = onwardDate;
    }

    public String getRetDate() {
        return retDate;
    }

    public void setRetDate(String retDate) {
        this.retDate = retDate;
    }

    public boolean isOnwardSelect() {
        return isOnwardSelect;
    }

    public void setOnwardSelect(boolean onwardSelect) {
        isOnwardSelect = onwardSelect;
    }


//////////////////////////////////////////////////////////////////////////////////////////////
    /***********************************
     * SERVER BOOKED AND CANCEL HISTORY
     * **********************************/

    private ServerBookedCanceledHistoryBean serverBookedCancelHistory;

    public ServerBookedCanceledHistoryBean getServerBookedCancelHistory() {
        return serverBookedCancelHistory;
    }

    public void setServerBookedCancelHistory(ServerBookedCanceledHistoryBean serverBookedCancelHistory) {
        this.serverBookedCancelHistory = serverBookedCancelHistory;
    }

    /***********************************
    * MAIN BOOKED AND CANCEL HISTORY
    * **********************************/

    private MainBookedCancledHistoryBean bookedCancelHistory;

    public MainBookedCancledHistoryBean getBookedCancelHistory() {
        return bookedCancelHistory;
    }

    public void setBookedCancelHistory(MainBookedCancledHistoryBean bookedCancelHistory) {
        this.bookedCancelHistory = bookedCancelHistory;
    }

    /*************************************
     * ROUND TRIP FARE QUOTE VARIABLE
     *************************************/
    // Round trip FareQuote JSON Segment
    private String returnFlightJsonSegment;
    // Round trip FareQuote XML segment
    private String returnFlightXMLSegment;




    /************************************************
     *  RETURUR TRIP INTERNATIONAL SEARCH ARRAYLIST
     ************************************************/

    private ArrayList<FlightOnWardDetailBean> onwInternationalFlightDetails = new ArrayList<>();

    public ArrayList<FlightOnWardDetailBean> getOnwInternationalFlightDetails() {
        return onwInternationalFlightDetails;
    }

    public void setOnwInternationalFlightDetails(FlightOnWardDetailBean onwInternationalFlightDetails) {
        this.onwInternationalFlightDetails.add(onwInternationalFlightDetails);
    }

    /************************************************
     *  FLIGHT SEARCH ARRAYLIST
     ************************************************/
    private ArrayList<FlightOnWardDetailBean> onWalAllFlightDetails = new ArrayList<>();

    public ArrayList<FlightOnWardDetailBean> getOnWalAllFlightDetails() {
        return onWalAllFlightDetails;
    }

    public void setOnWalAllFlightDetails(FlightOnWardDetailBean sm) {
        this.onWalAllFlightDetails.add(sm);
    }



    /************************************************
     *  FLIGHT RETURN SEARCH ARRAYLIST
     ************************************************/
    private ArrayList<FlightOnWardDetailBean> RetalAllFlightDetails = new ArrayList<>();

    public ArrayList<FlightOnWardDetailBean> getReturnAlAllFlightDetails() {
        return RetalAllFlightDetails;
    }

    public void setReturnAlAllFlightDetails(FlightOnWardDetailBean sm) {
        this.RetalAllFlightDetails.add(sm);
    }




    /****************************************************
     *  FLIGHT ROUND ONWARD GDC AND LCC FLIGHT ARRAYLIST
     ****************************************************/

    private RouOnwGdcLccBean rouOnwGdsLccBean = new RouOnwGdcLccBean();

    public RouOnwGdcLccBean getRouOnwGdsLccBean() {
        return rouOnwGdsLccBean;
    }

    public void setRouOnwGdsLccBean(RouOnwGdcLccBean rouOnwGdsLccBean) {
        this.rouOnwGdsLccBean = rouOnwGdsLccBean;
    }



    private ArrayList<FlightOnWardDetailBean> RouOnwalAllFlightDetails = new ArrayList<>();

    public ArrayList<FlightOnWardDetailBean> getRouOnwalAllFlightDetails() {
        return RouOnwalAllFlightDetails;
    }

    public void setRouOnwalAllFlightDetails(FlightOnWardDetailBean sm) {
        this.RouOnwalAllFlightDetails.add(sm);
    }


    private ArrayList<FlightOnWardDetailBean> RouRetalAllFlightDetails = new ArrayList<>();

    public ArrayList<FlightOnWardDetailBean> getRouRetalAllFlightDetails() {
        return RouRetalAllFlightDetails;
    }

    public void setRouRetalAllFlightDetails(FlightOnWardDetailBean sm) {
        this.RouRetalAllFlightDetails.add(sm);
    }



    /************************************************
    *  BOOKING HISTORY ARRAYLIST
     ************************************************/
    private ArrayList<BookingHistoryBean> mObjBookingHistory = new ArrayList<>();

    public ArrayList<BookingHistoryBean> getmObjBookingHistory() {
        return mObjBookingHistory;
    }
    public void setmObjBookingHistory(BookingHistoryBean mObjBookingHistory) {
        this.mObjBookingHistory.add(mObjBookingHistory);
    }



    /************************************************
     *  CANCELLATION HISTORY ARRAYLIST
     ************************************************/
    private ArrayList<BookingHistoryBean> mObjCancellationHistory = new ArrayList<>();

    public ArrayList<BookingHistoryBean> getmObjCancellationHistory() {
        return mObjCancellationHistory;
    }

    public void setmObjCancellationHistory(BookingHistoryBean mObjCancellationHistory) {
        this.mObjCancellationHistory.add(mObjCancellationHistory);
    }



    /************************************************
     *  CHEAPEST FLIGHT ARRAYLIST
     ************************************************/
    ArrayList<FilterCheapFareAirlineBean> cheapestFlightFare = new ArrayList<>();

    public ArrayList<FilterCheapFareAirlineBean> getCheapestFlightFare() {
        return cheapestFlightFare;
    }

    public void setCheapestFlightFare(FilterCheapFareAirlineBean cheapestFlightFare) {
        this.cheapestFlightFare.add(cheapestFlightFare);
    }



    /************************************************
     *  SPECIAL OFFER FLIGHT ARRAYLIST
     ************************************************/
    ArrayList<SpecialOfferAirlineBean> specialOfferFare = new ArrayList<>();


    public ArrayList<SpecialOfferAirlineBean> getSpecialOfferFare() {
        return specialOfferFare;
    }

    public void setSpecialOfferFare(SpecialOfferAirlineBean specialOfferFare) {
        this.specialOfferFare.add(specialOfferFare);
    }



    /************************************************
     *  CONFIRMATION FLIGHT ARRAYLIST
     ************************************************/
    ArrayList<ConfirmationTicketBean> confirmationFlightDetail = new ArrayList<>();

    public ArrayList<ConfirmationTicketBean> getConfirmationFlightDetail() {
        return confirmationFlightDetail;
    }

    public void setConfirmationFlightDetail(ConfirmationTicketBean confirmationFlightDetail) {
        this.confirmationFlightDetail.add(confirmationFlightDetail);
    }







    /************************************************
     *  FARE QUOTE ONE WAY BEAN
     ************************************************/
    private FareQuoteOneWayBean mFareQuoteOneWayBean = new FareQuoteOneWayBean();

    public FareQuoteOneWayBean getmFareQuoteOneWayBean() {
        return mFareQuoteOneWayBean;
    }

    public void setmFareQuoteOneWayBean(FareQuoteOneWayBean mFareQuoteOneWayBean) {
        this.mFareQuoteOneWayBean = mFareQuoteOneWayBean;
    }


    /************************************************
     *  FARE QUOTE ROUND TRIP BEAN
     ************************************************/
    private FareQuoteRoundTripOuterBean mFareQuoteRoundTripOuterBean = new FareQuoteRoundTripOuterBean();

    public FareQuoteRoundTripOuterBean getmFareQuoteRoundTripOuterBean() {
        return mFareQuoteRoundTripOuterBean;
    }

    public void setmFareQuoteRoundTripOuterBean(FareQuoteRoundTripOuterBean mFareQuoteRoundTripOuterBean) {
        this.mFareQuoteRoundTripOuterBean = mFareQuoteRoundTripOuterBean;
    }






    /*******************************************************************************************
     *******************************************************************************************/

    public FlightFilterBean getmFlightFilterBean() {
        return mFlightFilterBean;
    }

    public void setmFlightFilterBean(FlightFilterBean mFlightFilterBean) {
        this.mFlightFilterBean = mFlightFilterBean;
    }

    public int getOnwFlightFareMin() {
        return onwFlightFareMin;
    }

    public void setOnwFlightFareMin(int onwFlightFareMin) {
        this.onwFlightFareMin = onwFlightFareMin;
    }

    public int getOnwFlightFareMax() {
        return onwFlightFareMax;
    }

    public void setOnwFlightFareMax(int onwFlightFareMax) {
        this.onwFlightFareMax = onwFlightFareMax;
    }

    public String getOnwFlightJsonSegment() {
        return onwFlightJsonSegment;
    }

    public void setOnwFlightJsonSegment(String onwFlightJsonSegment) {
        this.onwFlightJsonSegment = onwFlightJsonSegment;
    }

    public String getOnwFlightXMLSegment() {
        return onwFlightXMLSegment;
    }

    public void setOnwFlightXMLSegment(String onwFlightXMLSegment) {
        this.onwFlightXMLSegment = onwFlightXMLSegment;
    }

    public String getOnwFlightSecurityToken() {
        return onwFlightSecurityToken;
    }

    public void setOnwFlightSecurityToken(String onwFlightSecurityToken) {
        this.onwFlightSecurityToken = onwFlightSecurityToken;
    }

    public String getReturnFlightJsonSegment() {
        return returnFlightJsonSegment;
    }

    public void setReturnFlightJsonSegment(String returnFlightJsonSegment) {
        this.returnFlightJsonSegment = returnFlightJsonSegment;
    }

    public String getReturnFlightXMLSegment() {
        return returnFlightXMLSegment;
    }

    public void setReturnFlightXMLSegment(String returnFlightXMLSegment) {
        this.returnFlightXMLSegment = returnFlightXMLSegment;
    }

    public boolean isFlightOnWardFromCache() {
        return isFlightOnWardFromCache;
    }

    public void setIsFlightOnWardFromCache(boolean isFlightOnWardFromCache) {
        this.isFlightOnWardFromCache = isFlightOnWardFromCache;
    }

    public String getTripDomOrInter() {
        return tripDomOrInter;
    }

    public void setTripDomOrInter(String tripDomOrInter) {
        this.tripDomOrInter = tripDomOrInter;
    }

    public String getFromStation() {
        return fromStation;
    }

    public void setFromStation(String fromStation) {
        this.fromStation = fromStation;
    }

    public String getToStation() {
        return toStation;
    }

    public void setToStation(String toStation) {
        this.toStation = toStation;
    }

    public String getDepDate() {
        return depDate;
    }

    public void setDepDate(String depDate) {
        this.depDate = depDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getTravelClass() {
        return travelClass;
    }

    public void setTravelClass(String travelClass) {
        this.travelClass = travelClass;
    }

    public int getAdultPassNum() {
        return adultPassNum;
    }

    public void setAdultPassNum(int adultPassNum) {
        this.adultPassNum = adultPassNum;
    }

    public int getChildPassNum() {
        return childPassNum;
    }

    public void setChildPassNum(int childPassNum) {
        this.childPassNum = childPassNum;
    }

    public int getInfantPassNum() {
        return infantPassNum;
    }

    public void setInfantPassNum(int infantPassNum) {
        this.infantPassNum = infantPassNum;
    }

    public String getToStationCity() {
        return toStationCity;
    }

    public void setToStationCity(String toStationCity) {
        this.toStationCity = toStationCity;
    }

    public String getFromStationCity() {
        return fromStationCity;
    }

    public void setFromStationCity(String fromStationCity) {
        this.fromStationCity = fromStationCity;
    }

    public int getRetFlightFareMin() {
        return retFlightFareMin;
    }

    public void setRetFlightFareMin(int retFlightFareMin) {
        this.retFlightFareMin = retFlightFareMin;
    }

    public int getRetFlightFareMax() {
        return retFlightFareMax;
    }

    public void setRetFlightFareMax(int retFlightFareMax) {
        this.retFlightFareMax = retFlightFareMax;
    }

}
