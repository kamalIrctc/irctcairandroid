package com.irctc.air.model.booking_history;

public class PojoBookingHistory
{
    private String message;

    private String status;

    private Data[] data;

    //private null userDetails;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public Data[] getData ()
    {
        return data;
    }

    public void setData (Data[] data)
    {
        this.data = data;
    }

/*
    public null getUserDetails ()
    {
        return userDetails;
    }

    public void setUserDetails (null userDetails)
    {
        this.userDetails = userDetails;
    }
*/

/*
    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", status = "+status+", data = "+data+", userDetails = "+userDetails+"]";
    }
*/
}

			