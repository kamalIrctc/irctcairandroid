package com.irctc.air.model;

/**
 * Created by Rajnikant Kumar on 11/8/2018.
 */

public class ModelPnrPax {
    String pnr,paxNo;

    public String getPnr() {
        return pnr;
    }

    public void setPnr(String pnr) {
        this.pnr = pnr;
    }

    public String getPaxNo() {
        return paxNo;
    }

    public void setPaxNo(String paxNo) {
        this.paxNo = paxNo;
    }
}
