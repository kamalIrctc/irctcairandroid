package com.irctc.air.model;

/**
 * Created by tourism on 6/14/2016.
 */
public class PassengerOperationBean {

    private int imageId;
    private String operationName;

    public String getOperationName() {
        return operationName;
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
}
