package com.irctc.air.model;

/**
 * Created by vivek on 4/23/2016.
 */
public class RecentFlightSearchBean {

    public int id;
    private String recFromStationCode;
    private String recFromStationName;
    private String recToStationCode;
    private String recToStationName;
    private String recTripTypeDomOrInter;
    private String recTripClassEorB;
    private String recOneWayOrRoundTrip;
    private String recPassAdoultCount;
    private String recPassChildCount;
    private String recPassInfantCount;
    private String recDepDate;
    private String recReturnDate;




    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRecFromStationCode() {
        return recFromStationCode;
    }

    public void setRecFromStationCode(String recFromStationCode) {
        this.recFromStationCode = recFromStationCode;
    }

    public String getRecFromStationName() {
        return recFromStationName;
    }

    public void setRecFromStationName(String recFromStationName) {
        this.recFromStationName = recFromStationName;
    }

    public String getRecToStationCode() {
        return recToStationCode;
    }

    public void setRecToStationCode(String recToStationCode) {
        this.recToStationCode = recToStationCode;
    }

    public String getRecToStationName() {
        return recToStationName;
    }

    public void setRecToStationName(String recToStationName) {
        this.recToStationName = recToStationName;
    }

    public String getRecTripTypeDomOrInter() {
        return recTripTypeDomOrInter;
    }

    public void setRecTripTypeDomOrInter(String recTripTypeDomOrInter) {
        this.recTripTypeDomOrInter = recTripTypeDomOrInter;
    }

    public String getRecTripClassEorB() {
        return recTripClassEorB;
    }

    public void setRecTripClassEorB(String recTripClassEorB) {
        this.recTripClassEorB = recTripClassEorB;
    }

    public String getRecOneWayOrRoundTrip() {
        return recOneWayOrRoundTrip;
    }

    public void setRecOneWayOrRoundTrip(String recOneWayOrRoundTrip) {
        this.recOneWayOrRoundTrip = recOneWayOrRoundTrip;
    }

    public String getRecPassAdoultCount() {
        return recPassAdoultCount;
    }

    public void setRecPassAdoultCount(String recPassAdoultCount) {
        this.recPassAdoultCount = recPassAdoultCount;
    }

    public String getRecPassChildCount() {
        return recPassChildCount;
    }

    public void setRecPassChildCount(String recPassChildCount) {
        this.recPassChildCount = recPassChildCount;
    }

    public String getRecPassInfantCount() {
        return recPassInfantCount;
    }

    public void setRecPassInfantCount(String recPassInfantCount) {
        this.recPassInfantCount = recPassInfantCount;
    }

    public String getRecDepDate() {
        return recDepDate;
    }

    public void setRecDepDate(String recDepDate) {
        this.recDepDate = recDepDate;
    }

    public String getRecReturnDate() {
        return recReturnDate;
    }

    public void setRecReturnDate(String recReturnDate) {
        this.recReturnDate = recReturnDate;
    }
}
