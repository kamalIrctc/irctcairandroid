package com.irctc.air.model;

/**
 * Created by tourism on 4/25/2016.
 */
public class FilterCheapFareAirlineBean {

    private String mFlightCode;
    private String mFlightName;
    private int mFlightIcon;
    private String mFlightLowestFare;


    public String getmFlightLowestFare() {
        return mFlightLowestFare;
    }

    public void setmFlightLowestFare(String mFlightLowestFare) {
        this.mFlightLowestFare = mFlightLowestFare;
    }

    public String getmFlightCode() {
        return mFlightCode;
    }

    public void setmFlightCode(String mFlightCode) {
        this.mFlightCode = mFlightCode;
    }

    public boolean ismFlightSelected() {
        return mFlightSelected;
    }

    public void setmFlightSelected(boolean mFlightSelected) {
        this.mFlightSelected = mFlightSelected;
    }

    private boolean mFlightSelected;

    public String getmFlightName() {
        return mFlightName;
    }

    public void setmFlightName(String mFlightName) {
        this.mFlightName = mFlightName;
    }

    public int getmFlightIcon() {
        return mFlightIcon;
    }

    public void setmFlightIcon(int mFlightIcon) {
        this.mFlightIcon = mFlightIcon;
    }
}
