package com.irctc.air.model;

import java.util.ArrayList;

/**
 * Created by tourism on 4/25/2016.
 */
public class FlightFilterBean {

    private int stopZero = 0;
    private int stopOne = 0;
    private int stopTwoMore = 0;

    private int onwardOption1 = 0;
    private int onwardOption2 = 0;
    private int onwardOption3 = 0;
    private int onwardOption4 = 0;

    private int isChange = 0;
    private int minPrice = 0;
    private int maxPrice = 0;

    private int returnOption1 = 0;
    private int returnOption2 = 0;
    private int returnOption3 = 0;
    private int returnOption4 = 0;

    public int getIsChange() {
        return isChange;
    }

    public void setIsChange(int isChange) {
        this.isChange = isChange;
    }


    public int getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(int maxPrice) {
        this.maxPrice = maxPrice;
    }

    public int getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(int minPrice) {
        this.minPrice = minPrice;
    }

    private ArrayList<String> preferredAirline= new ArrayList<>();

    public int getOnwardOption1() {
        return onwardOption1;
    }

    public void setOnwardOption1(int onwardOption1) {
        this.onwardOption1 = onwardOption1;
    }

    public int getOnwardOption2() {
        return onwardOption2;
    }

    public void setOnwardOption2(int onwardOption2) {
        this.onwardOption2 = onwardOption2;
    }

    public int getOnwardOption3() {
        return onwardOption3;
    }

    public void setOnwardOption3(int onwardOption3) {
        this.onwardOption3 = onwardOption3;
    }

    public int getOnwardOption4() {
        return onwardOption4;
    }

    public void setOnwardOption4(int onwardOption4) {
        this.onwardOption4 = onwardOption4;
    }

    public int getReturnOption1() {
        return returnOption1;
    }

    public void setReturnOption1(int returnOption1) {
        this.returnOption1 = returnOption1;
    }

    public int getReturnOption2() {
        return returnOption2;
    }

    public void setReturnOption2(int returnOption2) {
        this.returnOption2 = returnOption2;
    }

    public int getReturnOption3() {
        return returnOption3;
    }

    public void setReturnOption3(int returnOption3) {
        this.returnOption3 = returnOption3;
    }

    public int getReturnOption4() {
        return returnOption4;
    }

    public void setReturnOption4(int returnOption4) {
        this.returnOption4 = returnOption4;
    }

    public int getStopZero() {
        return stopZero;
    }

    public void setStopZero(int stopZero) {
        this.stopZero = stopZero;
    }

    public int getStopOne() {
        return stopOne;
    }

    public void setStopOne(int stopOne) {
        this.stopOne = stopOne;
    }

    public int getStopTwoMore() {
        return stopTwoMore;
    }

    public void setStopTwoMore(int stopTwoMore) {
        this.stopTwoMore = stopTwoMore;
    }

    public ArrayList<String> getPreferredAirline() {
        return preferredAirline;
    }

    public void setPreferredAirline(ArrayList<String> preferredAirline) {
        this.preferredAirline = preferredAirline;
    }

}