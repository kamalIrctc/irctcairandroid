package com.irctc.air.model;

import java.util.ArrayList;

/**
 * Created by vivek on 5/9/2016.
 */
public class FlightConfirmationBean {

    // SEGMENT ARRAYLIST
    private ArrayList<SegmentDetailBean> alSegmentDetail = new ArrayList();

    // PASSENGER ARRAYLIST
    private ArrayList<PassengerDetailBean> alPassengerDetail = new ArrayList();


    public ArrayList<SegmentDetailBean> getAlSegmentDetail() {
        return alSegmentDetail;
    }

    public void setAlSegmentDetail(SegmentDetailBean alSegmentDetail) {
        this.alSegmentDetail.add(alSegmentDetail);
    }

    public ArrayList<PassengerDetailBean> getAlPassengerDetail() {
        return alPassengerDetail;
    }

    public void setAlPassengerDetail(PassengerDetailBean alPassengerDetail) {
        this.alPassengerDetail.add(alPassengerDetail);
    }
}
