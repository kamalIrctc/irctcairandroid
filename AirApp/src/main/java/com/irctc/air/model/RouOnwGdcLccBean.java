package com.irctc.air.model;

import java.util.ArrayList;

/**
 * Created by tourism on 5/10/2016.
 */
public class RouOnwGdcLccBean {

    ArrayList<FlightOnWardDetailBean> flightLCC = new ArrayList<FlightOnWardDetailBean>();
    ArrayList<FlightOnWardDetailBean> flightGDS = new ArrayList<FlightOnWardDetailBean>();;

    public ArrayList<FlightOnWardDetailBean> getFlightLCC() {
        return flightLCC;
    }

    public void setFlightLCC(FlightOnWardDetailBean flightLCC) {
        this.flightLCC.add(flightLCC);
    }

    public ArrayList<FlightOnWardDetailBean> getFlightGDS() {
        return flightGDS;
    }

    public void setFlightGDS(FlightOnWardDetailBean flightGDS) {
        this.flightGDS.add(flightGDS);
    }
}
