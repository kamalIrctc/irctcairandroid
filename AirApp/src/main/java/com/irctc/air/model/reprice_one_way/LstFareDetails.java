package com.irctc.air.model.reprice_one_way;

public class LstFareDetails {
    private String total;

    private String irctcCharge;

    private String baseFare;

    private String sgst;

    private String gst;

    private String flightCancelPenalty;

    //private null paxType;

    private String addCharge;

    private String difference;

    private String originalTotalFare;

    private String gstTotal;

    private String tax;

    private String flightChangePenalty;

    private String baseType;

    //private null fareRuleKey;

    private String cgst;

    private TaxDetails taxDetails;

    private String cancellationFee;

    public String getTotal ()
    {
        return total;
    }

    public void setTotal (String total)
    {
        this.total = total;
    }

    public String getIrctcCharge ()
    {
        return irctcCharge;
    }

    public void setIrctcCharge (String irctcCharge)
    {
        this.irctcCharge = irctcCharge;
    }

    public String getBaseFare ()
    {
        return baseFare;
    }

    public void setBaseFare (String baseFare)
    {
        this.baseFare = baseFare;
    }

    /*public null getFareInfoRef ()
    {
        return fareInfoRef;
    }

    public void setFareInfoRef (null fareInfoRef)
    {
        this.fareInfoRef = fareInfoRef;
    }
*/
    public String getSgst ()
    {
        return sgst;
    }

    public void setSgst (String sgst)
    {
        this.sgst = sgst;
    }

    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }

    public String getFlightCancelPenalty ()
    {
        return flightCancelPenalty;
    }

    public void setFlightCancelPenalty (String flightCancelPenalty)
    {
        this.flightCancelPenalty = flightCancelPenalty;
    }

    /*  public null getPaxType ()
      {
          return paxType;
      }

      public void setPaxType (null paxType)
      {
          this.paxType = paxType;
      }
*/
      public String getAddCharge ()
      {
          return addCharge;
      }

      public void setAddCharge (String addCharge)
      {
          this.addCharge = addCharge;
      }

    public String getDifference ()
    {
        return difference;
    }

    public void setDifference (String difference)
    {
        this.difference = difference;
    }

    public String getOriginalTotalFare ()
    {
        return originalTotalFare;
    }

    public void setOriginalTotalFare (String originalTotalFare)
    {
        this.originalTotalFare = originalTotalFare;
    }

    public String getGstTotal ()
    {
        return gstTotal;
    }

    public void setGstTotal (String gstTotal)
    {
        this.gstTotal = gstTotal;
    }

    public String getTax ()
    {
        return tax;
    }

    public void setTax (String tax)
    {
        this.tax = tax;
    }

    public String getFlightChangePenalty ()
    {
        return flightChangePenalty;
    }

    public void setFlightChangePenalty (String flightChangePenalty)
    {
        this.flightChangePenalty = flightChangePenalty;
    }

    public String getBaseType ()
    {
        return baseType;
    }

    public void setBaseType (String baseType)
    {
        this.baseType = baseType;
    }

  /*  public null getFareRuleKey ()
    {
        return fareRuleKey;
    }

    public void setFareRuleKey (null fareRuleKey)
    {
        this.fareRuleKey = fareRuleKey;
    }
*/
    public String getCgst ()
    {
        return cgst;
    }

    public void setCgst (String cgst)
    {
        this.cgst = cgst;
    }

    public TaxDetails getTaxDetails ()
    {
        return taxDetails;
    }

    public void setTaxDetails (TaxDetails taxDetails)
    {
        this.taxDetails = taxDetails;
    }

  public String getCancellationFee ()
    {
        return cancellationFee;
    }

    public void setCancellationFee (String cancellationFee)
    {
        this.cancellationFee = cancellationFee;
    }

    @Override
    public String toString()
    {
        // fareRuleKey = "+fareRuleKey+",
        //, fareInfoRef = "+fareInfoRef+"
        //, cancellationFee = "+cancellationFee+"
        //, flightCancelPenalty = "+flightCancelPenalty+", paxType = "+paxType+", addCharge = "+addCharge+"
        //flightChangePenalty = "+flightChangePenalty+",
        return "ClassPojo [total = "+total+", irctcCharge = "+irctcCharge+", baseFare = "+baseFare+", sgst = "+sgst+", difference = "+difference+", originalTotalFare = "+originalTotalFare+", gstTotal = "+gstTotal+", tax = "+tax+",  baseType = "+baseType+", cgst = "+cgst+", taxDetails = "+taxDetails+"]";
    }
}

		