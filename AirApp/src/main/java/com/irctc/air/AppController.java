package com.irctc.air;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Intent;
import android.text.TextUtils;
import android.text.format.DateUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;
import com.irctc.air.services.RecenDataClearRecevier;
import com.irctc.air.session.Waiter;
import com.irctc.air.util.DateUtility;
import com.irctc.air.util.Pref;

import java.util.Calendar;

import io.fabric.sdk.android.Fabric;

import static com.android.volley.VolleyLog.TAG;

/**
 * Created by Rajnikant Kumar on 7/31/2018.
 */

public class AppController extends Application {
    private static AppController mInstance;
    private RequestQueue mRequestQueue;
    //region : Global Switch
    public static boolean autoFillByRecentSearch=false;
    public static int autoFillByRecentSearchValueIndex=0;
    //endregion
    public static String routeType;
    public static boolean isComingFromFareQuote=false;
    private Waiter waiter;  //Thread which controls idle time
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        mInstance = this;
        //startThread();
        AnalyticsTrackers.initialize(this);
        AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);
        sheduleJob();
    }

    private void sheduleJob() {
        Calendar calender = DateUtility.getCalender("11:59 PM");
        Intent intent = new Intent(this, RecenDataClearRecevier.class);
        final int _id = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, _id, intent, 0);
        AlarmManager alarmManager = (AlarmManager) this.getSystemService(ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calender.getTimeInMillis(), DateUtils.DAY_IN_MILLIS, pendingIntent);
    }

    public static synchronized AppController getInstance(){
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    /*    private Waiter waiter;  //Thread which controls idle time

    private static MyApp instance;

    public static boolean isSessionExpired =false;
    // only lazy initializations here!
    @Override
    public void onCreate()
    {
        super.onCreate();
        instance = this;
        //Log.d(TAG, "Starting application"+this.toString());
        waiter=new Waiter(1*30*1000); //15 mins
        startThread();
    }

    public static MyApp getApp(){return instance;}

    public void startThread(){
        if (waiter.isAlive())
            waiter.interrupt();
        waiter=new Waiter(1*30*1000); //15 mins

        waiter.start();
    }

    public void touch(){
        waiter.touch();
    }*/

    public void startThread(){
        //if (waiter.isAlive())
        //    waiter.interrupt();
        //waiter=new Waiter(15*60*1000); //15 mins
        waiter=new Waiter(1*10*1000); //10 Sec
        waiter.start();
    }
    public void touch(){
        waiter.touch();
    }


    public synchronized Tracker getGoogleAnalyticsTracker() {
        AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
        return analyticsTrackers.get(AnalyticsTrackers.Target.APP);
    }

    /***
     * Tracking screen view
     *
     * @param screenName screen name to be displayed on GA dashboard
     */
    public void trackScreenView(String screenName) {
        Tracker t = getGoogleAnalyticsTracker();

        // Set screen name.
        t.setScreenName(screenName);

        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        GoogleAnalytics.getInstance(this).dispatchLocalHits();
    }

    /***
     * Tracking exception
     *
     * @param e exception to be tracked
     */
    public void trackException(Exception e) {
        if (e != null) {
            Tracker t = getGoogleAnalyticsTracker();

            t.send(new HitBuilders.ExceptionBuilder()
                    .setDescription(
                            new StandardExceptionParser(this, null)
                                    .getDescription(Thread.currentThread().getName(), e))
                    .setFatal(false)
                    .build()
            );
        }
    }

    /***
     * Tracking event
     *
     * @param category event category
     * @param action   action of the event
     * @param label    label
     */
    public void trackEvent(String category, String action, String label) {
        Tracker t = getGoogleAnalyticsTracker();

        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).build());
    }

}
