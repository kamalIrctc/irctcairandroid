package com.irctc.air.networking;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.irctc.air.AppController;
import com.irctc.air.Database.SharedPrefrenceAir;
import com.irctc.air.model.ModelCancelTicket;
import com.irctc.air.model.book_ticket.PassengerDetails;
import com.irctc.air.model.book_ticket.UserDetails;
import com.irctc.air.model.gst.ModelGstDetails;
import com.irctc.air.model.reprice_one_way.Inf;
import com.irctc.air.model.reprice_one_way.LstFareDetails;
import com.irctc.air.model.reprice_one_way.Meal;
import com.irctc.air.model.reprice_one_way.PricingInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.android.volley.Request.Method.GET;
import static com.android.volley.Request.Method.POST;

/**
 * Created by Rajnikant Kumar on 7/28/2018.
 */

//lakdeepak     123

public class Networking {
    //Server list
    private static final String rohit_negi = "10.34.42.26:18081";
    private static final String testing = "10.34.14.47:8003";
    private static final String developement = "10.34.14.35:7003";
    private static final String live = "www.air.irctc.co.in";
    private static final String UAT = "uat.irctc.co.in";
    private static final String AIR_AIR = "/air/air/";
    private static final String AIR_STQC = "/airstqc/air/";


    //region : Base host Url
    public static final String BASE_URL_IP = testing;
//    private static final String BASE_URL_LOGIN = "http://" + BASE_URL_IP + "/airstqc/";
    private static final String BASE_URL_LOGIN = "http://" + BASE_URL_IP + "/air/";
//private static final String BASE_URL_LOGIN = "https://" + BASE_URL_IP + "/air/";
//    private static final String BASE_URL = "https://" + BASE_URL_IP + AIR_AIR;
private static final String BASE_URL = "http://" + BASE_URL_IP + AIR_AIR;
//    private static final String BASE_URL = "http://" + BASE_URL_IP + AIR_STQC;
    //endregion

    //region : Static Links
    public static final String AIRLINE_AGREMENT = BASE_URL + "mobile/mobile/airline-agreement.component.html";
    public static final String USER_AGREMENT = BASE_URL + "mobile/mobile/agreement.component.html";
    public static final String FAQ = BASE_URL + "mobile/mobile/faq.html";
    //public static final String FAQ = "http://www.air.irctc.co.in/IndianRailways/irctcair/flights_faqs.html";
    public static final String CANCEL_OLD_TICKETS = "http://oldair.irctc.co.in/";
    //endregion

    //region : Close payment page when payment settled
    public static final String CLOSE_PAYMENT_PAGE = "/#/";
    //endregion

    //region : Request Queue TAG
    public static final String LOGIN_REQUEST = "loginRequest";

    public static final String SEARCH_AIRPORT_REQUEST = "searchAirportRequest";
    public static final String SEARCH_FLIGHTS_REQUEST = "searchFlightsRequest";
    public static final String REPRICE_REQUEST = "repriceRequest";
    public static final String BOOK_FLIGHT_REQUEST = "bookFlightRequest";
    public static final String TRANSACTION_STATUS = "transactionStatus";
    public static final String FLIGHT_ICON_PATH = "flightIconPath";
    public static final String BOOKING_HISTORY = "bookingHistory";
    public static final String CANCEL_TICKET = "cancelTicket";
    public static final String SEND_MAIL = "sendMail";
    public static final String LOW_FARE_CAL_REQUEST = "lowFareCalRequest";
    public static final String REFRESH_TOKEN_REQUEST = "refreshTokenRequest";
    public static final String CHECHK_APP_VERSION = "checkAppVersion";
    //endregion

    //region : Request Timeout
    public static final int REQUEST_TIME_OUT = 120000;
    //endregion

    //region : Search Airport
    public static void searchAirport(String searchKey, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        AppController.getInstance().cancelPendingRequests(SEARCH_AIRPORT_REQUEST);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(GET, BASE_URL + "airportsByKey?searchKey=" + searchKey, null, listener, errorListener);
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, SEARCH_AIRPORT_REQUEST);
    }
    //endregion

    public static void checkAppVersion(Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        AppController.getInstance().cancelPendingRequests(CHECHK_APP_VERSION);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(GET, BASE_URL + "version/AND", null, listener, errorListener);
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, CHECHK_APP_VERSION);
//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(GET, "http://10.34.14.47:8003/airstqc/air/version/AND", null, listener, errorListener);
//        AppController.getInstance().addToRequestQueue(jsonObjectRequest, CHECHK_APP_VERSION);
    }

    //region : Search Flights
    public static void searchFlights(final String tripType,
                                     final String departureDate,
                                     final String returnDate,
                                     final String noOfAdults,
                                     final String noOfChildren,
                                     final String noOfInfants,
                                     final String origin,
                                     final String destination,
                                     final String destinationCity,
                                     final String originCity,
                                     final String classOfTravel,
                                     final String preferedAirline,
                                     final boolean ltc,
                                     Response.Listener<JSONObject> listener,
                                     Response.ErrorListener errorListener) {
        AppController.getInstance().cancelPendingRequests(SEARCH_FLIGHTS_REQUEST);
        Log.e("URL", BASE_URL + "search");
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(POST, BASE_URL + "search", null, listener, errorListener) {
            @Override
            public byte[] getBody() {
                JSONObject params = new JSONObject();
                try {
                    params.put("tripType", tripType);
                    params.put("departureDate", departureDate);
                    params.put("returnDate", returnDate);
                    params.put("noOfAdults", noOfAdults);
                    params.put("noOfChildren", noOfChildren);
                    params.put("noOfInfants", noOfInfants);
                    params.put("origin", origin);
                    params.put("destination", destination);
                    params.put("destinationCity", destinationCity);
                    params.put("originCity", originCity);
                    params.put("classOfTravel", classOfTravel);
                    params.put("airline", preferedAirline);
                    params.put("ltc", ltc);
                    params.put("src", "and");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                final String mRequestBody = params.toString();
                try {
                    Log.e("Request", mRequestBody);
                    return mRequestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> map = new HashMap<String, String>();
                //map.put("Accept-Encoding","application/gzip");
                return map;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                REQUEST_TIME_OUT,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, SEARCH_FLIGHTS_REQUEST);
    }
    //endregion

    //region : DReprice OneWay Trip
    public static void repriceOneWay(
            final String totalPrice,
            final String searchKey,
            final String[] flightKeys,
            final String isInternational,
            final ModelGstDetails modelGstDetails,
            Response.Listener<JSONObject> listener,
            Response.ErrorListener errorListener) {
        AppController.getInstance().cancelPendingRequests(REPRICE_REQUEST);
        Log.e("AirPriceURL", BASE_URL + "airprice");
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(POST, BASE_URL + "airprice", null, listener, errorListener) {
            @Override
            public byte[] getBody() {
                String flightKeysString = new Gson().toJson(flightKeys, String[].class);
                String mRequestBody = "{" +
                        "\"totalPrice\":\"" + totalPrice + "\"," +
                        "\"searchKey\":\"" + searchKey + "\"," +
                        "\"serviceProvider\":\"null\"," +
                        "\"flightKeys\":" + flightKeysString + "," +
                        "\"gstflag\":" + modelGstDetails.isGstflag() + "," +
                        "\"gstNumber\":\"" + modelGstDetails.getGstNumber() + "\"," +
                        "\"companyName\":\"" + modelGstDetails.getCompanyName() + "\"," +
                        "\"emailid\":\"" + modelGstDetails.getEmailid() + "\"," +
                        "\"isInternational\":\"" + isInternational + "\"" +
                        "}";
                Log.e("Request", mRequestBody);
                try {
                    return mRequestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                REQUEST_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, REPRICE_REQUEST);
    }
    //endregion

    //region : Reprice Special Fare
    public static void repriceSpecialFare(
            final String totalPrice,
            final String searchKey,
            final String[] flightKeys,
            final String isInternational,
            final String[] gdsKeys,
            final String[] segKeys,
            final String serviceProvider,
            final String noOfAdult,
            final String noOfChild,
            final String noOfInfant,
            final ModelGstDetails modelGstDetails,
            Response.Listener<JSONObject> listener,
            Response.ErrorListener errorListener) {
        AppController.getInstance().cancelPendingRequests(REPRICE_REQUEST);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(POST, BASE_URL + "airprice", null, listener, errorListener) {
            @Override
            public byte[] getBody() {
                StringBuilder flightKeysString = new StringBuilder();
                if (flightKeys.length > 0) {
                    flightKeysString.append("[");
                    for (int i = 0; i < flightKeys.length; i++) {
                        flightKeysString.append("\"" + flightKeys[i] + "\"");
                        if (i < flightKeys.length - 1) {
                            flightKeysString.append(",");
                        }
                    }
                    flightKeysString.append("]");
                }
                StringBuilder gdsKeysString = new StringBuilder();
                if (gdsKeys.length > 0) {
                    gdsKeysString.append("[");
                    for (int i = 0; i < gdsKeys.length; i++) {
                        gdsKeysString.append("\"" + gdsKeys[i] + "\"");
                        if (i < gdsKeys.length - 1) {
                            gdsKeysString.append(",");
                        }
                    }
                    gdsKeysString.append("]");
                }
                StringBuilder segKeysString = new StringBuilder();
                if (segKeys.length > 0) {
                    segKeysString.append("[");
                    for (int i = 0; i < segKeys.length; i++) {
                        segKeysString.append("\"" + segKeys[i] + "\"");
                        if (i < segKeys.length - 1) {
                            segKeysString.append(",");
                        }
                    }
                    segKeysString.append("]");
                }
                String mRequestBody = "{" +
                        "\"totalPrice\":\"" + totalPrice + "\"," +
                        "\"searchKey\":\"" + searchKey + "\"," +
                        "\"serviceProvider\":\"" + serviceProvider + "\"," +
                        "\"flightKeys\":" + flightKeysString.toString() + "," +
                        "\"isInternational\":\"" + isInternational + "\"," +
                        "\"isSpl\":true," +
                        "\"gdsKey\":" + gdsKeysString.toString() + "," +
                        "\"segKey\":" + segKeysString.toString() + "," +
                        "\"noOfAdult\":\"" + noOfAdult + "\"," +
                        " \"noOfChild\":\"" + noOfChild + "\"," +
                        " \"noOfInfant\":\"" + noOfInfant + "\"," +
                        " \"gstflag\":" + modelGstDetails.isGstflag() + "," +
                        " \"gstNumber\": \"" + modelGstDetails.getGstNumber() + "\"," +
                        " \"companyName\": \"" + modelGstDetails.getCompanyName() + "\"," +
                        " \"emailid\": \"" + modelGstDetails.getEmailid() + "\" " +
                        "}";
                Log.e("Request", mRequestBody);
                try {
                    return mRequestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                REQUEST_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, REPRICE_REQUEST);
    }
    //endregion

    //region : IReprice Round Trip
    public static void repriceRoundTripInternational(
            final String totalPrice,
            final String searchKey,
            final String[] flightKeys,
            final String noOdAd,
            final String noOdCh,
            final String noOdIn,
            final ModelGstDetails modelGstDetails,
            final String[] gdsKeys,
            Response.Listener<JSONObject> listener,
            Response.ErrorListener errorListener) {
        AppController.getInstance().cancelPendingRequests(REPRICE_REQUEST);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(POST, BASE_URL + "airprice", null, listener, errorListener) {
            @Override
            public byte[] getBody() {
                String flightKeysString = new Gson().toJson(flightKeys, String[].class);
                //String gdsKeyArray = "[\"" + gdsKeys[0] + "\",\"" + gdsKeys[1] + "\"]";
                String gdsKeyArray = new Gson().toJson(gdsKeys,String[].class);
                String mRequestBody = "{\n" +
                        "\"totalPrice\":\"" + totalPrice + "\"," +
                        "\"searchKey\":\"" + searchKey + "\"," +
                        "\"serviceProvider\":null," +
                        "\"flightKeys\":" + flightKeysString + "," +
                        "\"gdsKey\":" + gdsKeyArray + "," +
                        "\"isInternational\":true," +
                        "\"noOfAdult\": \"" + noOdAd + "\"," +
                        "\"noOfChild\": \"" + noOdCh + "\"," +
                        "\"noOfInfant\": \"" + noOdIn + "\"," +
                        " \"gstflag\":" + modelGstDetails.isGstflag() + "," +
                        " \"gstNumber\": \"" + modelGstDetails.getGstNumber() + "\"," +
                        " \"companyName\": \"" + modelGstDetails.getCompanyName() + "\"," +
                        " \"emailid\": \"" + modelGstDetails.getEmailid() + "\" " +
                        "}";
                Log.e("Request", mRequestBody);
                try {
                    return mRequestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                REQUEST_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, REPRICE_REQUEST);
    }
    //endregion

    //region : User Login
    public static void loginUser(
            final String username,
            final String password,
            Response.Listener<JSONObject> listener,
            Response.ErrorListener errorListener) {
        AppController.getInstance().cancelPendingRequests(LOGIN_REQUEST);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(POST, BASE_URL_LOGIN + "user/login", null, listener, errorListener) {
            @Override
            public byte[] getBody() {
                String mRequestBody = "{" +
                        "\"username\":\"" + username + "\"," +
                        "\"password\":\"" + password + "\"" +
                        "}";

                Log.e("URL", BASE_URL_LOGIN + "user/login");
                Log.e("Request", mRequestBody);
                try {
                    return mRequestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                REQUEST_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, LOGIN_REQUEST);
    }
    //endregion

    //region : Guest Login
    public static void loginGuest(
            final String username,
            final String password,
            Response.Listener<JSONObject> listener,
            Response.ErrorListener errorListener) {
        AppController.getInstance().cancelPendingRequests(LOGIN_REQUEST);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(POST, BASE_URL_LOGIN + "user/guestlogin", null, listener, errorListener) {
            @Override
            public byte[] getBody() {
                String mRequestBody = "{" +
                        "\"emailId\":\"" + username + "\"," +
                        "\"phoneNo\":\"" + password + "\"" +
                        "}";
                Log.e("Request", mRequestBody);
                try {
                    return mRequestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                REQUEST_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, LOGIN_REQUEST);
    }
    //endregion

    //region : Book Flight
    public static void bookFlight(
            final boolean isBaggageAllowed,
            final String signMap,
            final String authToken,
            final UserDetails userDetails,
            final String segmentType,
            final LstFareDetails[] lstFareDetails,
            final PricingInfo[] pricingInfo,
            final ArrayList<PassengerDetails> passengerDetails,
            final String searchKey,
            final String[] flightKeys,
            final String isInternational,
            final Inf[] inf,
            final String totalAmount,
            final String[] segKeys,
            final boolean isSpecialFare,
            final boolean isPriceChange,
            final ModelGstDetails modelGstDetails,
            final String[] gdsKeys,
            Response.Listener<JSONObject> listener,
            Response.ErrorListener errorListener) {
        AppController.getInstance().cancelPendingRequests(BOOK_FLIGHT_REQUEST);
        Log.e("URL", BASE_URL + "book");
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(POST, BASE_URL + "book", null, listener, errorListener) {
            @Override
            public byte[] getBody() {
/*                StringBuilder sb=new StringBuilder();
                for(int i=0;i<passengerDetails.size();i++){
                    String dobA=null;
                    if(passengerDetails.get(i).getDob()!=null){
                        dobA="\""+passengerDetails.get(i).getDob()+"\"";
                    }
                    sb.append("{\"titleType\":" + passengerDetails.get(i).getTitleType() +",\"firstName\": \""+passengerDetails.get(i).getFirstName()+"\",\"lastName\": \""+passengerDetails.get(i).getLastName()+"\",\"gender\": "+passengerDetails.get(i).getGender()+",\"dob\": "+dobA+",\"passengerType\": \""+passengerDetails.get(i).getType()+"\"}");
                    if(i>=0&&i<passengerDetails.size()-1){
                        sb.append(",");
                    }
                }*/
                String gdsKeyArray=null;
                if(gdsKeys!=null){
                 gdsKeyArray = new Gson().toJson(gdsKeys,String[].class);
                }
                PassengerDetails[] passDet = new PassengerDetails[passengerDetails.size()];
                passDet = passengerDetails.toArray(passDet);
                String passengerListJsonString = new Gson().toJson(passDet, PassengerDetails[].class);
                String infString = new Gson().toJson(inf, Inf[].class);
                String fareDetailsString = new Gson().toJson(lstFareDetails, LstFareDetails[].class);
                String strPricingInfo = new Gson().toJson(pricingInfo, PricingInfo[].class);
                String flightKeyString = new Gson().toJson(flightKeys, String[].class);
                String passengerDetailsString = passengerListJsonString.toString();
                StringBuilder segKeysString = new StringBuilder();
                segKeysString.append("[");
                if (segKeys.length > 0) {
                    for (int i = 0; i < segKeys.length; i++) {
                        segKeysString.append("\"" + segKeys[i] + "\"");
                        if (i < segKeys.length - 1) {
                            segKeysString.append(",");
                        }
                    }
                }
                segKeysString.append("]");

                String tempFirstName =userDetails.getFirstName();
                String tempLastName =userDetails.getLastName();
                if(tempFirstName.equals("")){
                    tempFirstName=passengerDetails.get(0).getFirstName();
                }
                if(tempLastName.equals("")){
                    tempLastName=passengerDetails.get(0).getLastName();
                }


                String mRequestBody = "{" +
                        "\"src\": \"app:And\"," +
                        "\"address1\": \"" + userDetails.getAddress1() + "\"," +
                        "\"address2\": \"" + userDetails.getAddress2() + "\"," +
                        "\"firstName\": \"" + tempFirstName + "\"," +
                        "\"lastName\": \"" + tempLastName + "\"," +
                        "\"city\": \"" + userDetails.getCity() + "\"," +
                        "\"state\": \"" + userDetails.getState() + "\"," +
                        "\"country\": \"" + userDetails.getCountry() + "\"," +
                        "\"pinCode\": \"" + userDetails.getPinCode() + "\"," +
                        "\"email\": \"" + userDetails.getEmail() + "\"," +
                        "\"empCode\": \"" + userDetails.getEmpCode() + "\"," +
                        "\"mobileNo\": \"" + userDetails.getMobileNo() + "\"," +
                        "\"transactionId\": \"\"," +
                        "\"gdsKeys\":" + gdsKeyArray + "," +
                        "\"gstflag\":" + modelGstDetails.isGstflag() + "," +
                        "\"gstNumber\":\"" + modelGstDetails.getGstNumber() + "\"," +
                        "\"companyName\":\"" + modelGstDetails.getCompanyName() + "\"," +
                        "\"emailid\":\"" + modelGstDetails.getEmailid() + "\"," +
                        "\"segmentType\": \"" + segmentType + "\"," +
                        "\"lstFlightDetails\": []," +
                        "\"totalAmount\":" + totalAmount + "," +
                        "\"lstFareDetails\": " + fareDetailsString + "," +
                        "\"lstPassengerDetails\":" + passengerDetailsString + "," +
                        "\"ltcEmpCode\": \"\"," +
                        "\"bags\": []," +
                        "\"meal\": []," +
                        "\"inf\": " + infString + "," +
                        "\"pricingInfo\":" + strPricingInfo + "," +
                        "\"searchKey\": \"" + searchKey + "\"," +
                        "\"flightKeys\":" + flightKeyString + "," +
                        "\"isInternational\": " + isInternational + "," +
                        "\"segKey\":" + segKeysString.toString() + "," +
                        "\"isSpl\":" + isSpecialFare + "," +
                        "\"signMap\":" + signMap + "," +
                        "\"isBaggageAllowed\":" + isBaggageAllowed + "," +
                        "\"isPriceOk\":" + isPriceChange +
                        "}";
                Log.e("Request", mRequestBody);
                try {
                    return mRequestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=utf-8");
                params.put("Authorization", "Bearer " + authToken);
                Log.e("RequestToken", "Bearer " + authToken);
                return params;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                REQUEST_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, BOOK_FLIGHT_REQUEST);
    }
    //endregion

    //region : Check Transaction Status
    public static void checkTransactionStatus(final String authToken, String transactionId, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        AppController.getInstance().cancelPendingRequests(TRANSACTION_STATUS);
        Log.e("URL", BASE_URL + "bookingconfirmation?transactionId=" + transactionId);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(GET, BASE_URL + "bookingconfirmation?transactionId=" + transactionId, null, listener, errorListener) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=utf-8");
                params.put("Authorization", "Bearer " + authToken);
                Log.e("RequestToken", "Bearer " + authToken);
                return params;
            }
        };
        Log.e("REQUEST", "transactionId=" + transactionId);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, TRANSACTION_STATUS);
    }
    //endregion

    //region : Get Flight Icon path
    public static void getFileIconPath(Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        AppController.getInstance().cancelPendingRequests(FLIGHT_ICON_PATH);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(GET, BASE_URL + "flighticon", null, listener, errorListener);
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, FLIGHT_ICON_PATH);
    }
    //endregion

    //region : Get Booking History
    public static void getBookingHistory(final String authToken, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        AppController.getInstance().cancelPendingRequests(BOOKING_HISTORY);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(POST, BASE_URL + "bookings?type=BOOKED&USER_ID=", null, listener, errorListener) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=utf-8");
                params.put("Authorization", "Bearer " + authToken);
                Log.e("RequestToken", "Bearer " + authToken);
                return params;
            }
        };
        Log.e("REQUEST", jsonObjectRequest.getUrl());
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, BOOKING_HISTORY);
    }
    //endregion

    //region : Get Cancellation History
    public static void getCancellationHistory(final String authToken, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        AppController.getInstance().cancelPendingRequests(BOOKING_HISTORY);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(POST, BASE_URL + "bookings?type=CANCELLED&USER_ID=", null, listener, errorListener) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=utf-8");
                params.put("Authorization", "Bearer " + authToken);
                Log.e("RequestToken", "Bearer " + authToken);
                return params;
            }
        };
        Log.e("REQUEST", jsonObjectRequest.getUrl());
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, BOOKING_HISTORY);
    }
    //endregion

    //region : Cancel Ticket
    public static void cancelTicket(final String authToken, final String cancelReq, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        AppController.getInstance().cancelPendingRequests(CANCEL_TICKET);
        Log.e("Cancel Ticket URL", BASE_URL + "cancel");
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(POST, BASE_URL + "cancel", null, listener, errorListener) {
            @Override
            public byte[] getBody() {
                String mRequestBody = cancelReq;
                Log.e("Request", mRequestBody);
                try {
                    return mRequestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=utf-8");
                params.put("Authorization", "Bearer " + authToken);
                Log.e("RequestToken", "Bearer " + authToken);
                return params;
            }
        };
        Log.e("REQUEST", jsonObjectRequest.getUrl());
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                REQUEST_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, CANCEL_TICKET);
    }
    //endregion

    //region : Check Session
    public static void checkSession(
            Response.Listener<JSONObject> listener,
            Response.ErrorListener errorListener) {
        AppController.getInstance().cancelPendingRequests(LOGIN_REQUEST);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(POST, BASE_URL + "getServerDate", null, listener, errorListener);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                REQUEST_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, LOGIN_REQUEST);
    }
    //endregion

    //region : Low Fare Calendar
    public static void lowFareCalendar(
            final String org,
            final String dest,
            Response.Listener<JSONObject> listener,
            Response.ErrorListener errorListener) {
        AppController.getInstance().cancelPendingRequests(LOW_FARE_CAL_REQUEST);

        Log.e("URL", BASE_URL + "lowfare?org=" + org + "&dest=" + dest);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(POST, BASE_URL + "lowfare?org=" + org + "&dest=" + dest, null, listener, errorListener) {
           /* @Override
            public byte[] getBody() {
                String mRequestBody = "{" +
                        "\"org\":\"" + org + "\"," +
                        "\"dest\":\"" + dest + "\"" +
                        "}";

                Log.e("URL", BASE_URL + "user/login");
                Log.e("Request", mRequestBody);
                try {
                    return mRequestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }*/

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                REQUEST_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, LOW_FARE_CAL_REQUEST);
    }


    public static void refreshToken(
            final String authToken,
            Response.Listener<JSONObject> listener,
            Response.ErrorListener errorListener) {
        //AppController.getInstance().cancelPendingRequests(REFRESH_TOKEN_REQUEST);
        Log.e("URL", BASE_URL + "user/refreshToken");
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(POST, BASE_URL + "user/refreshToken", null, listener, errorListener) {


            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=utf-8");
                params.put("Authorization", "Bearer " + authToken);
                Log.e("RequestToken", "Bearer " + authToken);
                return params;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                REQUEST_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, REFRESH_TOKEN_REQUEST);
    }

    //region : Cancel Ticket
    public static void sendMail(final String authToken,String txnId, Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        AppController.getInstance().cancelPendingRequests(SEND_MAIL);
        Log.e("Send Mail URL", BASE_URL + "confirmMail?txnId="+txnId);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(POST, BASE_URL + "confirmMail?txnId="+txnId, null, listener, errorListener) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=utf-8");
                params.put("Authorization", "Bearer " + authToken);
                Log.e("RequestToken", "Bearer " + authToken);
                return params;
            }
        };
        Log.e("REQUEST", jsonObjectRequest.getUrl());
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                REQUEST_TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjectRequest, SEND_MAIL);
    }
    //endregion
}
