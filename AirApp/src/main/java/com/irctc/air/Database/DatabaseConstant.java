package com.irctc.air.Database;

/**
 * Created by pc_u on 23/10/15.
 */
public class DatabaseConstant {

            public static final String KEY_ID = "_id";
            public static final String DATA_TYPE_VAR = " VARCHAR ";
            public static final String DATA_TYPE_PRIMARY = " INTEGER PRIMARY KEY AUTOINCREMENT";



            // TABLE_RECENT_SEARCH
            public static final String _STATION_CODE_FROM = "stationCodeFrom";
            public static final String _STATION_NAME_FROM = "stationNameFrom";
            public static final String _STATION_CODE_TO = "stationCodeTo";
            public static final String _STATION_NAME_TO = "stationNameTo";
            public static final String _TRIP_TYPE = "tripType";
            public static final String _TRIP_CLASS = "tripClass";
            public static final String _PASS_ADULT_COUNT = "passAdultCount";
            public static final String _PASS_CHILD_COUNT = "passChildCount";
            public static final String _PASS_INFANT_COUNT = "passInfantCount";
            public static final String _DEPARTURE_DAY = "depertureDay";
            public static final String _DEPARTURE_MONTH = "depertureMonth";
            public static final String _DEPARTURE_YEAR = "depertureYear";
            public static final String _DEPARTURE_DAY_NAME = "depertureDayName";
            public static final String _DEPARTURE_MONTH_NAME = "depertureMonthName";
            public static final String _RETURN_DAY = "returnDay";
            public static final String _RETURN_MONTH = "returnMonth";
            public static final String _RETURN_YEAR = "returnYear";
            public static final String _RETURN_DAY_NAME = "returnDayName";
            public static final String _RETURN_MONTH_NAME = "returnMonthName";
            public static final String _IS_LTC = "isLTC";

            //public static final String PRIMARY_KEY_SORT = "UNIQUE("+_STATION_CODE_FROM+", "+_STATION_CODE_TO+", "+_IS_LTC+", "+_DEPARTURE_DAY+", "+_DEPARTURE_MONTH+", "+_DEPARTURE_YEAR+", "+_RETURN_DAY+", "+_RETURN_MONTH+", "+_RETURN_YEAR+", "+_TRIP_TYPE+", "+_PASS_ADULT_COUNT+", "+_PASS_CHILD_COUNT+", "+_PASS_INFANT_COUNT+") ON CONFLICT REPLACE";
            public static final String PRIMARY_KEY_SORT = "PRIMARY KEY("+_STATION_CODE_FROM+", "+_STATION_CODE_TO+", "+_IS_LTC+", "+_DEPARTURE_DAY+", "+_DEPARTURE_MONTH+", "+_DEPARTURE_YEAR+", "+_RETURN_DAY+", "+_RETURN_MONTH+", "+_RETURN_YEAR+", "+_TRIP_TYPE+", "+_PASS_ADULT_COUNT+", "+_PASS_CHILD_COUNT+", "+_PASS_INFANT_COUNT+")";

            // fOR UNIQUE DATA PRIMARY KEY
            public static final String RETURN_DAY_AND_MONTH = "RETURN_DAY_AND_MONTH";
            public static final String DEPARTURE_DAY_AND_MONTH = "DEPARTURE_DAY_AND_MONTH";

            public static final String ONE_WAY_OR_ROUND_TRIP = "ONE_WAY_OR_ROUND_TRIP";

            // TABLE_RECENT_SEARCH is used for saving user searches
            public static final String TABLE_RECENT_SEARCH = "RECENT_FLIGHT_SEARCH";

            public static final String CREATE_TABLE_RECENT_SEARCH = "CREATE TABLE " + TABLE_RECENT_SEARCH + "("
            + _STATION_CODE_FROM + DATA_TYPE_VAR + ","
            + _STATION_NAME_FROM + DATA_TYPE_VAR + ","
            + _STATION_CODE_TO + DATA_TYPE_VAR + ","
            + _STATION_NAME_TO + DATA_TYPE_VAR + ","
            + _TRIP_TYPE + DATA_TYPE_VAR + ","
            + _TRIP_CLASS + DATA_TYPE_VAR + ","
            + _PASS_ADULT_COUNT + DATA_TYPE_VAR + ","
            + _PASS_CHILD_COUNT + DATA_TYPE_VAR + ","
            + _PASS_INFANT_COUNT + DATA_TYPE_VAR + ","
            + _DEPARTURE_DAY + DATA_TYPE_VAR + ","
            + _DEPARTURE_MONTH + DATA_TYPE_VAR + ","
            + _DEPARTURE_YEAR + DATA_TYPE_VAR + ","
            + _DEPARTURE_DAY_NAME + DATA_TYPE_VAR + ","
            + _DEPARTURE_MONTH_NAME + DATA_TYPE_VAR + ","
            + _RETURN_DAY + DATA_TYPE_VAR + ","
            + _RETURN_MONTH + DATA_TYPE_VAR + ","
            + _RETURN_YEAR + DATA_TYPE_VAR + ","
            + _RETURN_DAY_NAME + DATA_TYPE_VAR + ","
            + _IS_LTC + DATA_TYPE_VAR + ","
            + _RETURN_MONTH_NAME + DATA_TYPE_VAR + ","
            + PRIMARY_KEY_SORT + ")";



            // TABLE_PASS_DETAIL
            public static final String PASS_TITLE = "PASS_TITLE";
            public static final String PASS_FIRST_NAME = "PASS_FIRST_NAME";
            public static final String PASS_LAST_NAME = "PASS_LAST_NAME";
            public static final String PASS_AGE = "PASS_AGE";
            public static final String PASS_TYPE = "PASS_TYPE";
            public static final String PASS_PASSPORT_NUM = "PASS_PASSPORT_NUM";
            public static final String PASS_PASSPORT_NATIONALITY = "PASS_PASSPORT_NATIONALITY";
            public static final String PASS_PASSPORT_ISSUE_DATE = "PASS_PASSPORT_ISSUE_DATE";
            public static final String PASS_PASSPORT_VALID_DATE = "PASS_PASSPORT_VALID_DATE";
            public static final String PRIMARY_KEY_PASS_DETAIL = "PRIMARY KEY(PASS_TITLE, PASS_FIRST_NAME,PASS_LAST_NAME,PASS_AGE,PASS_TYPE)";
            public static final String TABLE_PASS_DETAIL = "TABLE_PASS_DETAIL";

            public static final String CREATE_TABLE_PASS_DETAIL = "CREATE TABLE " + TABLE_PASS_DETAIL + "("

            + PASS_TITLE + DATA_TYPE_VAR + ","
            + PASS_FIRST_NAME + DATA_TYPE_VAR + ","
            + PASS_LAST_NAME + DATA_TYPE_VAR + ","
            + PASS_AGE + DATA_TYPE_VAR + ","
            + PASS_TYPE + DATA_TYPE_VAR +","
            + PASS_PASSPORT_NUM + DATA_TYPE_VAR +","
            + PASS_PASSPORT_NATIONALITY + DATA_TYPE_VAR +","
            + PASS_PASSPORT_ISSUE_DATE + DATA_TYPE_VAR +","
            + PASS_PASSPORT_VALID_DATE + DATA_TYPE_VAR +","
            + PRIMARY_KEY_PASS_DETAIL + ")";


            // TABLE BOOKING & CANCELLATION HISTORY
            public static final String HISTORY_ID = "HISTORY_ID";
            public static final String HISTORY_TRN_ID = "HISTORY_TRN_ID";
            public static final String HISTORY_BOOK_STATUS_VALUE = "HISTORY_BOOK_STATUS_VALUE";
            public static final String HISTORY_DEPART_DATE = "HISTORY_DEPART_DATE";
            public static final String HISTORY_ARRIVE_DATE = "HISTORY_ARRIVE_DATE";
            public static final String HISTORY_TYPE = "HISTORY_TYPE";
            public static final String HISTORY_JSON = "HISTORY_JSON";
            public static final String TABLE_HISTORY_DETAIL = "TABLE_HISTORY_DETAIL";

            public static final String CREATE_TABLE_HISTORY_DETAIL = "CREATE TABLE " + TABLE_HISTORY_DETAIL + "("

            + HISTORY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT " + ","
            + HISTORY_TRN_ID + DATA_TYPE_VAR + ","
            + HISTORY_BOOK_STATUS_VALUE + DATA_TYPE_VAR + ","
            + HISTORY_DEPART_DATE + DATA_TYPE_VAR + ","
            + HISTORY_ARRIVE_DATE + DATA_TYPE_VAR +","
            + HISTORY_TYPE + DATA_TYPE_VAR +","
            + HISTORY_JSON + " TEXT " + ")";


    //region : TABLE USER TOKEN
    public static final String AUTH_TOKEN = "token";
    public static final String TABLE_AUTH_TOKEN = "authToken";
    public static final String CREATE_AUTH_TOKEN = "CREATE TABLE " + TABLE_AUTH_TOKEN + "(" + AUTH_TOKEN + " TEXT " + ")";
    //endregion



    //region : TABLE PASSENGER LIST
    public static final String TABLE_PASSENGER_LIST = "passengerList";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String GENDER = "gender";
    public static final String DOB = "dob";
    public static final String TYPE = "type";
    public static final String TITLE_TYPE = "titleType";
/*
    public static final String PASSPORT_NUMBER = "passportNumber";
    public static final String PASSPORT_ISSUE_DATE = "passportIssueDate";
    public static final String PASSPORT_EXP_DATE = "passportExpDate";
*/
    public static final String PRIMARY_KEY_PASS_LIST = "PRIMARY KEY("+FIRST_NAME+", "+LAST_NAME+", "+GENDER+", "+TYPE+")";
    public static final String CREATE_TABLE_PASSENGER_LIST = "CREATE TABLE " + TABLE_PASSENGER_LIST +
            "(" +
            FIRST_NAME + " TEXT, " +
            LAST_NAME + " TEXT, " +
            GENDER + " TEXT, " +
            DOB + " TEXT, " +
            TYPE + " TEXT, " +
            TITLE_TYPE + " TEXT, " +
/*
            PASSPORT_NUMBER + " TEXT, " +
            PASSPORT_ISSUE_DATE + " TEXT, " +
            PASSPORT_EXP_DATE + " TEXT, " +
*/
            PRIMARY_KEY_PASS_LIST +
            ")";
    //endregion


//region : Airport Table
    public static final String _TABLE_AIRPORTS = "tableAirports";
    public static final String _AIRPORT_SEARCH_KEY = "airportSearchKey";
    public static final String _AIRPORT_INDEX = "airportIndex";
    public static final String _AIRPORT_CODE = "airportCode";
    public static final String _AIRPORT_CITY_NAME = "airportCityName";
    public static final String _AIRPORT_COUNTRY_CODE = "airportCountryCode";
    public static final String _AIRPORT_LABEL = "airportLabel";
    public static final String _CREATE_TABLE_AIRPORTS = "CREATE TABLE " + _TABLE_AIRPORTS +
            "(" +
            _AIRPORT_SEARCH_KEY + " TEXT NOT NULL, " +
            _AIRPORT_INDEX + " TEXT, " +
            _AIRPORT_CODE + " TEXT, " +
            _AIRPORT_CITY_NAME + " TEXT, " +
            _AIRPORT_COUNTRY_CODE + " TEXT, " +
            _AIRPORT_LABEL + " TEXT, " +
            "PRIMARY KEY("+_AIRPORT_SEARCH_KEY+")" +
            ")";
    public static final String _DROP_TABLE_AIRPORTS = "DROP TABLE IF EXISTS " + DatabaseConstant._TABLE_AIRPORTS;
//endregion

}


