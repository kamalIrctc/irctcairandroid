package com.irctc.air.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.irctc.air.model.HistoryBean;
import com.irctc.air.model.PassDetailbean;
import com.irctc.air.model.RecentFlightSearchBean;
import com.irctc.air.model.book_ticket.Data;
import com.irctc.air.model.book_ticket.PassengerDetails;
import com.irctc.air.model.recent_search.ModelRecentSearch;
import com.irctc.air.util.AES;
import com.irctc.air.util.AppLogger;
import com.irctc.air.util.DateUtility;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;

public class AirDatabase extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "AIR_DATABASE";
    public static final int DATABASE_VERSION = 6;

    private SQLiteDatabase db;

    public AirDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void open() {
        try {
            db = getWritableDatabase();
        } catch (Exception e) {
        }
    }


    public void close() {
        db.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

//        try {

        AppLogger.enable();
        AppLogger.e("Create table : ", DatabaseConstant.CREATE_TABLE_RECENT_SEARCH);
        AppLogger.e("Create table : ", DatabaseConstant.CREATE_TABLE_PASS_DETAIL);
        AppLogger.e("Create table : ", DatabaseConstant.CREATE_TABLE_HISTORY_DETAIL);

        db.execSQL(DatabaseConstant.CREATE_TABLE_RECENT_SEARCH);
        db.execSQL(DatabaseConstant.CREATE_TABLE_PASS_DETAIL);
        db.execSQL(DatabaseConstant.CREATE_TABLE_HISTORY_DETAIL);
        db.execSQL(DatabaseConstant.CREATE_AUTH_TOKEN);
        db.execSQL(DatabaseConstant.CREATE_TABLE_PASSENGER_LIST);
        db.execSQL(DatabaseConstant._CREATE_TABLE_AIRPORTS);

//        } catch (Exception e) {
//                e.printStackTrace();
//        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //try {
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseConstant.TABLE_RECENT_SEARCH);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseConstant.TABLE_PASS_DETAIL);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseConstant.TABLE_HISTORY_DETAIL);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseConstant.TABLE_AUTH_TOKEN);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseConstant.TABLE_PASSENGER_LIST);
        db.execSQL(DatabaseConstant._DROP_TABLE_AIRPORTS);
        onCreate(db);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    /****************************************************
     * TABLE_RECENT_SEARCH
     * Used to save recent user's flight searches
     ****************************************************/

    //region : Auth Token Insertion in database
    public void insertAuthToken(String token) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues cv = new ContentValues();
            cv.put(DatabaseConstant.AUTH_TOKEN, token);
            db.insert(DatabaseConstant.TABLE_AUTH_TOKEN, null, cv);
        } catch (Exception e) {
        }
        db.close();
    }

    //region : RecentData from Database
    public void removeRecentData() {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.delete(DatabaseConstant.TABLE_RECENT_SEARCH, null, null);
        } catch (Exception e) {
        }
        db.close();
    }


    //endregion

    //region : Remove Auth Token from Database
    public void removeAuthToken() {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.delete(DatabaseConstant.TABLE_AUTH_TOKEN, null, null);
        } catch (Exception e) {
        }
        db.close();
    }
    //endregion

    //region : Get Auth Token from database
    public String getAuthToken() {
        String authToken = "";
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            Cursor cursor = db.rawQuery("select * from " + DatabaseConstant.TABLE_AUTH_TOKEN, null);
            if (cursor.moveToFirst()) {
                do {
                    authToken = cursor.getString(0);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
        }
        db.close();
        return authToken;
    }
    //endregion

    //region : Passenger List Insertion in database
    public void insertPassengerList(ArrayList<PassengerDetails> passengerDetailsList) {
        for (PassengerDetails passengerDetails : passengerDetailsList) {
            SQLiteDatabase db = this.getWritableDatabase();
            try {
                ContentValues cv = new ContentValues();
                cv.put(DatabaseConstant.FIRST_NAME, passengerDetails.getFirstName());
                cv.put(DatabaseConstant.LAST_NAME, passengerDetails.getLastName());
                cv.put(DatabaseConstant.GENDER, passengerDetails.getGender());
                if (passengerDetails.getDob() != null) {
                    cv.put(DatabaseConstant.DOB, new SimpleDateFormat("dd/MM/yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(passengerDetails.getDob())));
                }
                cv.put(DatabaseConstant.TYPE, passengerDetails.getPassengerType());
                cv.put(DatabaseConstant.TITLE_TYPE, passengerDetails.getTitleType());
/*
            cv.put(DatabaseConstant.PASSPORT_NUMBER,passengerDetails.getPassportinfo().getNo());
            cv.put(DatabaseConstant.PASSPORT_ISSUE_DATE,passengerDetails.getPassportinfo().getIssueDate());
            cv.put(DatabaseConstant.PASSPORT_EXP_DATE,passengerDetails.getPassportinfo().getExpiryDate());
*/
                db.insert(DatabaseConstant.TABLE_PASSENGER_LIST, null, cv);
            } catch (Exception e) {
                Log.e("Database Error", e.toString());
            }
            db.close();
        }
    }
    //endregion

    //region : Passenger List Insertion in database
    public ArrayList<PassengerDetails> getPassengerList() {
        ArrayList<PassengerDetails> passengerDetailsList = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            Cursor cursor = db.rawQuery("SELECT * FROM " + DatabaseConstant.TABLE_PASSENGER_LIST, null);
            if (cursor.moveToFirst()) {
                do {
                    PassengerDetails passengerDetails = new PassengerDetails();
                    passengerDetails.setFirstName(cursor.getString(cursor.getColumnIndex(DatabaseConstant.FIRST_NAME)));
                    passengerDetails.setLastName(cursor.getString(cursor.getColumnIndex(DatabaseConstant.LAST_NAME)));
                    passengerDetails.setGender(cursor.getString(cursor.getColumnIndex(DatabaseConstant.GENDER)));
                    passengerDetails.setDob(cursor.getString(cursor.getColumnIndex(DatabaseConstant.DOB)));
                    passengerDetails.setPassengerType(cursor.getString(cursor.getColumnIndex(DatabaseConstant.TYPE)));
                    passengerDetails.setTitleType(cursor.getString(cursor.getColumnIndex(DatabaseConstant.TITLE_TYPE)));
                  /*  passengerDetails.setPassportNumber(cursor.getString(cursor.getColumnIndex(DatabaseConstant.PASSPORT_NUMBER)));
                    passengerDetails.setPassportIssueDate(cursor.getString(cursor.getColumnIndex(DatabaseConstant.PASSPORT_ISSUE_DATE)));
                    passengerDetails.setPassportExpDate(cursor.getString(cursor.getColumnIndex(DatabaseConstant.PASSPORT_EXP_DATE)));
                    passengerDetails.setCountry(cursor.getString(cursor.getColumnIndex(DatabaseConstant.COUNTRY)));
                 */
                    passengerDetailsList.add(passengerDetails);
                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
        }
        db.close();
        return passengerDetailsList;
    }
    //endregion


    public boolean saveSearch(ModelRecentSearch recentSearch) {
        boolean inserted = false;
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            ContentValues cv = new ContentValues();
            cv.put(DatabaseConstant._STATION_CODE_FROM, recentSearch.getStationCodeFrom());
            cv.put(DatabaseConstant._STATION_NAME_FROM, recentSearch.getStationNameFrom());
            cv.put(DatabaseConstant._STATION_CODE_TO, recentSearch.getStationCodeTo());
            cv.put(DatabaseConstant._STATION_NAME_TO, recentSearch.getStationNameTo());
            cv.put(DatabaseConstant._TRIP_TYPE, recentSearch.getTripType());
            cv.put(DatabaseConstant._TRIP_CLASS, recentSearch.getTripClass());
            cv.put(DatabaseConstant._PASS_ADULT_COUNT, recentSearch.getPassAdultCount());
            cv.put(DatabaseConstant._PASS_CHILD_COUNT, recentSearch.getPassChildCount());
            cv.put(DatabaseConstant._PASS_INFANT_COUNT, recentSearch.getPassInfantCount());
            cv.put(DatabaseConstant._DEPARTURE_DAY, recentSearch.getDepertureDay());
            cv.put(DatabaseConstant._DEPARTURE_MONTH, recentSearch.getDepertureMonth());
            cv.put(DatabaseConstant._DEPARTURE_YEAR, recentSearch.getDepertureYear());
            cv.put(DatabaseConstant._DEPARTURE_DAY_NAME, recentSearch.getDepertureDayName());
            cv.put(DatabaseConstant._DEPARTURE_MONTH_NAME, recentSearch.getDepertureMonthName());
            cv.put(DatabaseConstant._RETURN_DAY, recentSearch.getReturnDay());
            cv.put(DatabaseConstant._RETURN_MONTH, recentSearch.getReturnMonth());
            cv.put(DatabaseConstant._RETURN_YEAR, recentSearch.getReturnYear());
            cv.put(DatabaseConstant._RETURN_DAY_NAME, recentSearch.getReturnDayName());
            cv.put(DatabaseConstant._RETURN_MONTH_NAME, recentSearch.getReturnMonthName());
            cv.put(DatabaseConstant._IS_LTC, recentSearch.getIsLTC());

            long insrtedValue = db.insert(DatabaseConstant.TABLE_RECENT_SEARCH, null, cv);
            if (insrtedValue == -1) {
                inserted = true;
                //performRecentRearchOnTop(list);
            }
        } catch (Exception e) {
            //      AppLogger.enable();
            //       AppLogger.e("EXP in DB - insertRecentSearches", e.toString());
        }
        db.close();
        return inserted;
    }


    public ArrayList<ModelRecentSearch> getRecentSearchList() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + DatabaseConstant.TABLE_RECENT_SEARCH, null);
        ArrayList<ModelRecentSearch> recentSearchList = new ArrayList<ModelRecentSearch>();

        try {
            cursor.moveToFirst();
            for (int i = 0; i < cursor.getCount(); i++) {
                ModelRecentSearch recentSearch = new ModelRecentSearch();
                recentSearch.setStationCodeFrom(cursor.getString(cursor.getColumnIndex(DatabaseConstant._STATION_CODE_FROM)));
                recentSearch.setStationNameFrom(cursor.getString(cursor.getColumnIndex(DatabaseConstant._STATION_NAME_FROM)));
                recentSearch.setStationCodeTo(cursor.getString(cursor.getColumnIndex(DatabaseConstant._STATION_CODE_TO)));
                recentSearch.setStationNameTo(cursor.getString(cursor.getColumnIndex(DatabaseConstant._STATION_NAME_TO)));
                recentSearch.setTripType(cursor.getString(cursor.getColumnIndex(DatabaseConstant._TRIP_TYPE)));
                recentSearch.setTripClass(cursor.getString(cursor.getColumnIndex(DatabaseConstant._TRIP_CLASS)));
                recentSearch.setPassAdultCount(cursor.getString(cursor.getColumnIndex(DatabaseConstant._PASS_ADULT_COUNT)));
                recentSearch.setPassChildCount(cursor.getString(cursor.getColumnIndex(DatabaseConstant._PASS_CHILD_COUNT)));
                recentSearch.setPassInfantCount(cursor.getString(cursor.getColumnIndex(DatabaseConstant._PASS_INFANT_COUNT)));
                recentSearch.setDepertureDay(cursor.getString(cursor.getColumnIndex(DatabaseConstant._DEPARTURE_DAY)));
                recentSearch.setDepertureMonth(cursor.getString(cursor.getColumnIndex(DatabaseConstant._DEPARTURE_MONTH)));
                recentSearch.setDepertureYear(cursor.getString(cursor.getColumnIndex(DatabaseConstant._DEPARTURE_YEAR)));
                recentSearch.setDepertureDayName(cursor.getString(cursor.getColumnIndex(DatabaseConstant._DEPARTURE_DAY_NAME)));
                recentSearch.setDepertureMonthName(cursor.getString(cursor.getColumnIndex(DatabaseConstant._DEPARTURE_MONTH_NAME)));
                recentSearch.setReturnDay(cursor.getString(cursor.getColumnIndex(DatabaseConstant._RETURN_DAY)));
                recentSearch.setReturnMonth(cursor.getString(cursor.getColumnIndex(DatabaseConstant._RETURN_MONTH)));
                recentSearch.setReturnYear(cursor.getString(cursor.getColumnIndex(DatabaseConstant._RETURN_YEAR)));
                recentSearch.setReturnDayName(cursor.getString(cursor.getColumnIndex(DatabaseConstant._RETURN_DAY_NAME)));
                recentSearch.setReturnMonthName(cursor.getString(cursor.getColumnIndex(DatabaseConstant._RETURN_MONTH_NAME)));
                recentSearch.setIsLTC(cursor.getString(cursor.getColumnIndex(DatabaseConstant._IS_LTC)));
                recentSearchList.add(recentSearch);
                cursor.moveToNext();
            }
        } finally {
            db.close();
        }
        Collections.reverse(recentSearchList);
        return recentSearchList;
    }
    /****************************************************
     * TABLE_RECENT_SEARCH
     * Getting data from recent user's flight searches
     ****************************************************/


    /****************************************************
     * TABLE_RECENT_SEARCH
     * Clear data from recent user's flight searches
     ****************************************************/

    public void clearRecentSearch() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + DatabaseConstant.TABLE_RECENT_SEARCH);
    }


    /****************************************************
     * TABLE_PASS_DETAIL
     * Used to save previous added user
     ****************************************************/
    public void insertPassengerInDB(ArrayList<PassDetailbean> list) {

        SQLiteDatabase db = this.getWritableDatabase();
        try {

            for (int i = 0; i < list.size(); i++) {
                ContentValues cv = new ContentValues();
                cv.put(DatabaseConstant.PASS_TITLE, AES.encrypt(list.get(i).getPassTitle()));
                cv.put(DatabaseConstant.PASS_FIRST_NAME, AES.encrypt(list.get(i).getPassFirstName()));
                cv.put(DatabaseConstant.PASS_LAST_NAME, AES.encrypt(list.get(i).getPassLastName()));
                if ((list.get(i).getPassAge() != null) && (!list.get(i).getPassAge().equalsIgnoreCase(""))) {
                    cv.put(DatabaseConstant.PASS_AGE, AES.encrypt(list.get(i).getPassAge()));
                } else {
                    cv.put(DatabaseConstant.PASS_AGE, AES.encrypt("NA"));
                }
                cv.put(DatabaseConstant.PASS_TYPE, AES.encrypt(list.get(i).getPassType()));

                if (list.get(i).getPassPassportNum() != null && !list.get(i).getPassPassportNum().equalsIgnoreCase("")) {
                    cv.put(DatabaseConstant.PASS_PASSPORT_NUM, AES.encrypt(list.get(i).getPassPassportNum()));
                } else {
                    cv.put(DatabaseConstant.PASS_PASSPORT_NUM, "");
                }
                if (list.get(i).getPassNationalty() != null && !list.get(i).getPassNationalty().equalsIgnoreCase("")) {
                    cv.put(DatabaseConstant.PASS_PASSPORT_NATIONALITY, AES.encrypt(list.get(i).getPassNationalty()));
                } else {
                    cv.put(DatabaseConstant.PASS_PASSPORT_NATIONALITY, "");
                }
                if (list.get(i).getPassPassportIssueDate() != null && !list.get(i).getPassPassportIssueDate().equalsIgnoreCase("")) {
                    cv.put(DatabaseConstant.PASS_PASSPORT_ISSUE_DATE, AES.encrypt(list.get(i).getPassPassportIssueDate()));
                } else {
                    cv.put(DatabaseConstant.PASS_PASSPORT_ISSUE_DATE, "");
                }
                if (list.get(i).getPassPassportValidUpto() != null && !list.get(i).getPassPassportValidUpto().equalsIgnoreCase("")) {
                    cv.put(DatabaseConstant.PASS_PASSPORT_VALID_DATE, AES.encrypt(list.get(i).getPassPassportValidUpto()));
                } else {
                    cv.put(DatabaseConstant.PASS_PASSPORT_VALID_DATE, "");
                }


                db.insert(DatabaseConstant.TABLE_PASS_DETAIL, null, cv);

            }

        } catch (Exception e) {
            AppLogger.enable();
            AppLogger.e("EXP in DB - insertPassengerInDB", e.toString());
        }
        db.close();
    }

    /****************************************************
     * TABLE_RECENT_SEARCH
     * Clear data from recent user's flight searches
     ****************************************************/

    public void clearPassengerList() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + DatabaseConstant.TABLE_PASS_DETAIL);
    }


    /****************************************************
     * TABLE_PASS_DETAIL
     * Getting all passengers from TABLE_PASS_DETAIL
     ****************************************************/
    public ArrayList<PassDetailbean> getAllPassengerList() {


        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + DatabaseConstant.TABLE_PASS_DETAIL, null);
        ArrayList<PassDetailbean> alObjPassList = new ArrayList<PassDetailbean>();

        try {

            cursor.moveToFirst();

            for (int i = 0; i < cursor.getCount(); i++) {

                PassDetailbean lObjPassListBean = new PassDetailbean();

                lObjPassListBean.setPassIndex(i);
                lObjPassListBean.setPassTitle(AES.decrypt(cursor.getString(cursor.getColumnIndex(DatabaseConstant.PASS_TITLE))));
                lObjPassListBean.setPassFirstName(AES.decrypt(cursor.getString(cursor.getColumnIndex(DatabaseConstant.PASS_FIRST_NAME))));
                lObjPassListBean.setPassLastName(AES.decrypt(cursor.getString(cursor.getColumnIndex(DatabaseConstant.PASS_LAST_NAME))));
                lObjPassListBean.setPassType(AES.decrypt(cursor.getString(cursor.getColumnIndex(DatabaseConstant.PASS_TYPE))));

                if (!AES.decrypt(cursor.getString(cursor.getColumnIndex(DatabaseConstant.PASS_AGE)).toString()).equalsIgnoreCase("NA")) {
                    lObjPassListBean.setPassAge(AES.decrypt(cursor.getString(cursor.getColumnIndex(DatabaseConstant.PASS_AGE))));
                } else {
                    lObjPassListBean.setPassAge("");
                }

                if (cursor.getString(cursor.getColumnIndex(DatabaseConstant.PASS_PASSPORT_NUM)) != null && !cursor.getString(cursor.getColumnIndex(DatabaseConstant.PASS_PASSPORT_NUM)).equalsIgnoreCase("")) {
                    lObjPassListBean.setPassPassportNum(AES.decrypt(cursor.getString(cursor.getColumnIndex(DatabaseConstant.PASS_PASSPORT_NUM))));
                } else {
                    lObjPassListBean.setPassPassportNum("");
                }

                if (cursor.getString(cursor.getColumnIndex(DatabaseConstant.PASS_PASSPORT_NATIONALITY)) != null && !cursor.getString(cursor.getColumnIndex(DatabaseConstant.PASS_PASSPORT_NATIONALITY)).equalsIgnoreCase("")) {
                    lObjPassListBean.setPassNationalty(AES.decrypt(cursor.getString(cursor.getColumnIndex(DatabaseConstant.PASS_PASSPORT_NATIONALITY))));
                } else {
                    lObjPassListBean.setPassNationalty("");
                }

                if (cursor.getString(cursor.getColumnIndex(DatabaseConstant.PASS_PASSPORT_ISSUE_DATE)) != null && !cursor.getString(cursor.getColumnIndex(DatabaseConstant.PASS_PASSPORT_ISSUE_DATE)).equalsIgnoreCase("")) {
                    lObjPassListBean.setPassPassportIssueDate(AES.decrypt(cursor.getString(cursor.getColumnIndex(DatabaseConstant.PASS_PASSPORT_ISSUE_DATE))));
                } else {
                    lObjPassListBean.setPassPassportIssueDate("");
                }

                if (cursor.getString(cursor.getColumnIndex(DatabaseConstant.PASS_PASSPORT_VALID_DATE)) != null && !cursor.getString(cursor.getColumnIndex(DatabaseConstant.PASS_PASSPORT_VALID_DATE)).equalsIgnoreCase("")) {
                    lObjPassListBean.setPassPassportValidUpto(AES.decrypt(cursor.getString(cursor.getColumnIndex(DatabaseConstant.PASS_PASSPORT_VALID_DATE))));
                } else {
                    lObjPassListBean.setPassPassportValidUpto("");
                }

                alObjPassList.add(lObjPassListBean);
                cursor.moveToNext();
            }
        } finally {
            db.close();
        }

        return alObjPassList;
    }

/*
    public void performRecentRearchOnTop(RecentFlightSearchBean list){

        ArrayList<RecentFlightSearchBean> lAlRecentSearch =  getRecentSearchList(0);

        for (int i = 0; i < lAlRecentSearch.size(); i++) {

            RecentFlightSearchBean lRecentSearch = lAlRecentSearch.get(i);

            if( lRecentSearch.getRecFromStationCode().equalsIgnoreCase(list.getRecFromStationCode()) &&
                     lRecentSearch.getRecToStationCode().equalsIgnoreCase(list.getRecToStationCode()) &&
                     DateUtility.getMonthAndDayAndYear(lRecentSearch.getRecReturnDate()).equalsIgnoreCase(DateUtility.getMonthAndDayAndYear(list.getRecReturnDate())) &&
                     DateUtility.getMonthAndDayAndYear(lRecentSearch.getRecDepDate()).equalsIgnoreCase(DateUtility.getMonthAndDayAndYear(list.getRecDepDate())) &&
                     lRecentSearch.getRecOneWayOrRoundTrip().equalsIgnoreCase(list.getRecOneWayOrRoundTrip()) &&
                     lRecentSearch.getRecPassAdoultCount().equalsIgnoreCase(list.getRecPassAdoultCount()) &&
                     lRecentSearch.getRecPassChildCount().equalsIgnoreCase(list.getRecPassChildCount()) &&
                     lRecentSearch.getRecPassInfantCount().equalsIgnoreCase(list.getRecPassInfantCount())

                    )
            {
                lAlRecentSearch.remove(i);
                lAlRecentSearch.add(list);
                break;
            }
        }

        clearRecentSearch();

        for (int i = 0; i < lAlRecentSearch.size(); i++) {

            insertRecentSearches(lAlRecentSearch.get(i));
        }
    }
*/


    /****************************************************
     * TABLE_HISTORY_DETAIL
     * Used to save booking history details
     ****************************************************/
    public void insertBookingHistory(ArrayList<HistoryBean> historyList) {

        for (int i = 0; i < historyList.size(); i++) {

            HistoryBean historyBean = historyList.get(i);

            ContentValues cv = new ContentValues();
            cv.put(DatabaseConstant.HISTORY_TRN_ID, historyBean.getTransactionId());
            cv.put(DatabaseConstant.HISTORY_BOOK_STATUS_VALUE, historyBean.getBookingStatusValue());
            cv.put(DatabaseConstant.HISTORY_DEPART_DATE, historyBean.getDepartureDate());
            cv.put(DatabaseConstant.HISTORY_ARRIVE_DATE, historyBean.getArrivalDate());
            cv.put(DatabaseConstant.HISTORY_TYPE, historyBean.getHistoryType());
            cv.put(DatabaseConstant.HISTORY_JSON, historyBean.getHistoryJson());

            long insrtedValue = db.insert(DatabaseConstant.TABLE_HISTORY_DETAIL, null, cv);

        }

    }

    public ArrayList<HistoryBean> getBookingHistory() {

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + DatabaseConstant.TABLE_HISTORY_DETAIL, null);
        ArrayList<HistoryBean> historyList = new ArrayList<HistoryBean>();

        try {

            cursor.moveToFirst();

            for (int i = 0; i < cursor.getCount(); i++) {

                HistoryBean historyBean = new HistoryBean();

                historyBean.setTransactionId(cursor.getString(cursor.getColumnIndex(DatabaseConstant.HISTORY_TRN_ID)));
                historyBean.setBookingStatusValue(cursor.getString(cursor.getColumnIndex(DatabaseConstant.HISTORY_BOOK_STATUS_VALUE)));
                historyBean.setDepartureDate(cursor.getString(cursor.getColumnIndex(DatabaseConstant.HISTORY_DEPART_DATE)));
                historyBean.setArrivalDate(cursor.getString(cursor.getColumnIndex(DatabaseConstant.HISTORY_ARRIVE_DATE)));
                historyBean.setHistoryType(cursor.getString(cursor.getColumnIndex(DatabaseConstant.HISTORY_TYPE)));
                historyBean.setHistoryJson(cursor.getString(cursor.getColumnIndex(DatabaseConstant.HISTORY_JSON)));

                historyList.add(historyBean);
                cursor.moveToNext();
            }
        } catch (Exception e) {
            db.close();
        } finally {
            db.close();
        }

        return historyList;
    }

    /****************************************************
     * TABLE_HISTORY_DETAIL
     * Clear data from  user's booking history details
     ****************************************************/

    public void clearBookCancelHistory() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + DatabaseConstant.TABLE_HISTORY_DETAIL);
    }

    public HistoryBean selectSpecificBookCancelHistory(String transactionId) {

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + DatabaseConstant.TABLE_HISTORY_DETAIL + " WHERE " + DatabaseConstant.HISTORY_TRN_ID + " = " + transactionId, null);
        HistoryBean historyBean = null;

        try {

            cursor.moveToFirst();

            for (int i = 0; i < cursor.getCount(); i++) {

                historyBean = new HistoryBean();

                historyBean.setTransactionId(cursor.getString(cursor.getColumnIndex(DatabaseConstant.HISTORY_TRN_ID)));
                historyBean.setBookingStatusValue(cursor.getString(cursor.getColumnIndex(DatabaseConstant.HISTORY_BOOK_STATUS_VALUE)));
                historyBean.setDepartureDate(cursor.getString(cursor.getColumnIndex(DatabaseConstant.HISTORY_DEPART_DATE)));
                historyBean.setArrivalDate(cursor.getString(cursor.getColumnIndex(DatabaseConstant.HISTORY_ARRIVE_DATE)));
                historyBean.setHistoryType(cursor.getString(cursor.getColumnIndex(DatabaseConstant.HISTORY_TYPE)));
                historyBean.setHistoryJson(cursor.getString(cursor.getColumnIndex(DatabaseConstant.HISTORY_JSON)));
                cursor.moveToNext();
            }
        } catch (Exception e) {
            db.close();
        } finally {
            db.close();
        }

        return historyBean;
    }

    public boolean updateRecord(String transactionId, String historyJson) {

        ContentValues cv = new ContentValues();
        cv.put(DatabaseConstant.HISTORY_JSON, historyJson);

        SQLiteDatabase db = this.getWritableDatabase();

        int id = db.update(DatabaseConstant.TABLE_HISTORY_DETAIL, cv, DatabaseConstant.HISTORY_TRN_ID + " = " + transactionId, null);

        if (id != -1) {
            return true;
        }
        return false;
    }


    //region : Airport Table
    public void insertAirportData(com.irctc.air.model.search_airports.Data[] airportList){
        int i =0;
        for(com.irctc.air.model.search_airports.Data data: airportList){
            SQLiteDatabase db = this.getWritableDatabase();
            try {
            ContentValues cv = new ContentValues();
            cv.put(DatabaseConstant._AIRPORT_SEARCH_KEY, data.getSearchKey());
            cv.put(DatabaseConstant._AIRPORT_INDEX, data.getIndex());
            cv.put(DatabaseConstant._AIRPORT_LABEL, data.getLabel());
            cv.put(DatabaseConstant._AIRPORT_CODE, data.getCode());
            cv.put(DatabaseConstant._AIRPORT_CITY_NAME, data.getCityName());
            cv.put(DatabaseConstant._AIRPORT_COUNTRY_CODE, data.getCountryCode());
            db.insert(DatabaseConstant._TABLE_AIRPORTS, null, cv);
            }catch (Exception e) {
            }
            db.close();
        }
    }

    public int isAirportDataExist(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + DatabaseConstant._TABLE_AIRPORTS+ "", null);
        int count=cursor.getCount();
        cursor.close();
        db.close();
        return count;
    }

    public ArrayList<com.irctc.air.model.search_airports.Data> getAirportsByKeyword(String keyword){
        ArrayList<com.irctc.air.model.search_airports.Data> arrayList = new ArrayList<>();
        com.irctc.air.model.search_airports.Data data;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + DatabaseConstant._TABLE_AIRPORTS+ " WHERE "+DatabaseConstant._AIRPORT_SEARCH_KEY+" LIKE '%"+keyword+"%' LIMIT 15", null);
        if(cursor.moveToFirst()){
            do{
                data = new com.irctc.air.model.search_airports.Data();
                data.setIndex(cursor.getString(cursor.getColumnIndex(DatabaseConstant._AIRPORT_INDEX)));
                data.setCityName(cursor.getString(cursor.getColumnIndex(DatabaseConstant._AIRPORT_CITY_NAME)));
                data.setCode(cursor.getString(cursor.getColumnIndex(DatabaseConstant._AIRPORT_CODE)));
                data.setCountryCode(cursor.getString(cursor.getColumnIndex(DatabaseConstant._AIRPORT_COUNTRY_CODE)));
                data.setLabel(cursor.getString(cursor.getColumnIndex(DatabaseConstant._AIRPORT_LABEL)));
                data.setSearchKey(cursor.getString(cursor.getColumnIndex(DatabaseConstant._AIRPORT_SEARCH_KEY)));
                arrayList.add(data);
            }while(cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return arrayList;
    }

    //endregion
}
