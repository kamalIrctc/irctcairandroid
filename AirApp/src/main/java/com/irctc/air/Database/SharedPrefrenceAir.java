package com.irctc.air.Database;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by BVTeam on 11/6/2015.
 */
public class SharedPrefrenceAir {

    private SharedPreferences.Editor editor;
    private Context context;
    private SharedPreferences sharedPreference;

    public SharedPrefrenceAir(Context context) {
        this.context = context;
        sharedPreference = context.getSharedPreferences("AIR_PREFERENCE", Context.MODE_PRIVATE);

    }

    private String signMap;

    public String getSignMap() {
        return sharedPreference.getString("signMap", null);
    }

    public void setSignMap(String signMap) {
        editor = sharedPreference.edit();
        editor.putString("signMap", signMap);
        editor.commit();
    }
    /*
    * SET USER DETAIL IN PREF
    * */

    // Is user logged in
    public void setIsLoggedIn(boolean flag) {
        editor = sharedPreference.edit();
        editor.putBoolean("IsLoggedIn", flag);
        editor.commit();
    }

    // Is user logged in
    public boolean getIsLoggedIn() {
        return sharedPreference.getBoolean("IsLoggedIn", false);
    }

    //////////////////////////////////

    // Is guest user
    public void setIsGuestUser(boolean flag) {
        editor = sharedPreference.edit();
        editor.putBoolean("IsGuestUser", flag);
        editor.commit();
    }

    // Is guest user
    public boolean getIsGuestUser() {
        return sharedPreference.getBoolean("IsGuestUser", false);
    }

    /////////////////////////////////////

    // User name
    public void SetUserName(String userName) {
        editor = sharedPreference.edit();
        editor.putString("UserName", userName);
        editor.commit();
    }

    // User name
    public String getUserName() {
        return sharedPreference.getString("UserName", "");
    }

    //////////////////////////////////////

    /////////////////////////////////////

    // User name
    public void SetUserUniqueId(String userName) {
        editor = sharedPreference.edit();
        editor.putString("UserUniqueId", userName);
        editor.commit();
    }


    // User name
    public String getUserUniqueId() {
        return sharedPreference.getString("UserUniqueId", "");
    }

    //////////////////////////////////////


    // Guest user mobile
    public void SetGuestUserMobile(String mobile) {
        editor = sharedPreference.edit();
        editor.putString("GuestUserMobile", mobile);
        editor.commit();
    }

    // Guest user mobile
    public String getGuestUserMobile() {
        return sharedPreference.getString("GuestUserMobile", "");
    }

    /////////////////////////////////


    // Guest user mobile number
    public void SetGuestUserEmail(String email) {
        editor = sharedPreference.edit();
        editor.putString("GuestUserEmail", email);
        editor.commit();
    }

    // Guest user mobile number
    public String getGuestUserEmail() {
        return sharedPreference.getString("GuestUserEmail", "");
    }
    //////////////////////////////////


    // User log in user id
    public void SetUserLogInId(String id) {
        editor = sharedPreference.edit();
        editor.putString("UserLogInId", id);
        editor.commit();
    }

    // User log in user id
    public String getUserLogInId() {
        return sharedPreference.getString("UserLogInId", "");
    }


    /////////////////////////////


    public void setUserLogInPwd(String pwd) {
        editor = sharedPreference.edit();
        editor.putString("UserLogInPwd", pwd);
        editor.commit();
    }

    // User log in user id
    public String getUserLogInPwd() {
        return sharedPreference.getString("UserLogInPwd", "");
    }

    ////////////////////////////////


    // User log in user id
    public void SetUserIdNumber(String id) {
        editor = sharedPreference.edit();
        editor.putString("UserLogInIdNumber", id);
        editor.commit();
    }

    // User log in user id
    public String getUserIdNumber() {
        return sharedPreference.getString("UserLogInIdNumber", "");
    }


    // USER ADDRESS
    public void SetUserAddress(String address) {
        editor = sharedPreference.edit();
        editor.putString("UserAddress", address);
        editor.commit();
    }


    // USER ADDRESS
    public String getUserAddress() {
        return sharedPreference.getString("UserAddress", "");
    }





/*
* DELETE SHARE PREF
* */

    public void deleteSharePreferenceData() {
        SharedPreferences sharedPreference = context.getSharedPreferences("AIR_PREFERENCE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreference.edit();
        editor.clear();
        editor.commit();
    }


}
