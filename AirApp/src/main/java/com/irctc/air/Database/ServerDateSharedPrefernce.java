package com.irctc.air.Database;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by tourism on 6/20/2016.
 */
public class ServerDateSharedPrefernce {

    private SharedPreferences.Editor editor;
    private Context context;
    private SharedPreferences sharedPreference;

    public ServerDateSharedPrefernce(Context context) {
        this.context = context;
        sharedPreference = context.getSharedPreferences("SERVER_DATE_PREFERENCE", Context.MODE_PRIVATE);
    }

    // Server Date
    public void setServerDate(String serverDate) {
        editor = sharedPreference.edit();
        editor.putString("ServerDate", serverDate);
        editor.commit();
    }

    public String getServerDate() {
        return sharedPreference.getString("ServerDate", "");
    }

}
