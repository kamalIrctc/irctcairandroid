package com.irctc.air.Dataholder;


import com.irctc.air.model.PassDetailbean;

import java.io.Serializable;
import java.util.ArrayList;


public class PassDataHolder implements Serializable {


    private static PassDataHolder mObjPassDataHolder;
    private ArrayList<PassDetailbean> mList;

    private PassDataHolder() {

        mList = new ArrayList<PassDetailbean>();
    }

    public synchronized static PassDataHolder getListHolder() {

        if (mObjPassDataHolder == null) {
            mObjPassDataHolder = new PassDataHolder();
        }
        return mObjPassDataHolder;
    }

    public ArrayList<PassDetailbean> getList() {

        return mList;
    }

    public void setList(ArrayList<PassDetailbean> pList) {

        mList = pList;
    }

}
