package com.irctc.air.Dataholder;


import com.irctc.air.model.PassDetailbean;

import java.io.Serializable;
import java.util.ArrayList;


public class PassListDataHolder implements Serializable {


    private static PassListDataHolder mObjPassListDataHolder;
    private ArrayList<PassDetailbean> mList;

    private PassListDataHolder() {

        mList = new ArrayList<PassDetailbean>();
    }

    public synchronized static PassListDataHolder getListHolder() {

        if (mObjPassListDataHolder == null) {
            mObjPassListDataHolder = new PassListDataHolder();
        }
        return mObjPassListDataHolder;
    }

    public ArrayList<PassDetailbean> getList() {

        return mList;
    }

    public void setList(ArrayList<PassDetailbean> pList) {

        mList = pList;
    }
}
