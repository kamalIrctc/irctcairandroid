package com.irctc.air.Dataholder;


import com.irctc.air.model.AirDataModel;
import java.io.Serializable;
import java.util.ArrayList;


public class AirDataHolder implements Serializable {


    private static AirDataHolder mObjAirDataHolder;
    private ArrayList<AirDataModel> mList;

    private AirDataHolder() {

        mList = new ArrayList<AirDataModel>();
    }

    public synchronized static AirDataHolder getListHolder() {

        if (mObjAirDataHolder == null) {
            mObjAirDataHolder = new AirDataHolder();
        }
        return mObjAirDataHolder;
    }

    public ArrayList<AirDataModel> getList() {

        return mList;
    }

    public void setList(ArrayList<AirDataModel> pList) {

        mList = pList;
    }
}
